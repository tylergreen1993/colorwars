﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MuseumViewModel : BaseViewModel
    {
        public List<MuseumDisplay> MuseumDisplays { get; set; }
        public List<Item> Items { get; set; }
        public Accomplishment RarestAccomplishment { get; set; }
        public UserStatistic TopAttentionHallPoster { get; set; }
        public UserStatistic TopAttentionHallCommenter { get; set; }
        public Item OldestItem { get; set; }
        public Item LongestOwnedItem { get; set; }
        public bool EastWingUnlocked { get; set; }
        public bool WestWingUnlocked { get; set; }
        public bool SouthWingUnlocked { get; set; }
        public bool HasStreakAccomplishment { get; set; }
        public bool CanBuyBoutiqueItem { get; set; }
        public MuseumWing CurrentWing { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}