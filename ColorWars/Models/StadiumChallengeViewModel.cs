﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StadiumChallengeViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public bool IsDeckAllowed { get; set; }
        public bool HasDoubleStadiumCPUReward { get; set; }
        public int Reward { get; set; }
        public int MatchLevel { get; set; }
        public CPUChallenge Challenge { get; set; }
        public DateTime ChallengeExpiryDate { get; set; }
    }
}