﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TowerViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public List<TowerRanking> TowerRankings { get; set; }
        public List<TowerRanking> RecentTowerRankings { get; set; }
        public int CurrentStreak { get; set; }
        public int RecordStreak { get; set; }
    }
}