﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class NewsViewModel : BaseViewModel
    {
        public List<News> News { get; set; }
        public News Article { get; set; }
        public HoroscopeType Horoscope { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int LuckLevel { get; set; }
        public DateTime HoroscopeExpiryDate { get; set; }
    }

    public enum HoroscopeType
    {
        Bad = -1,
        None = 0,
        Good = 1
    }
}