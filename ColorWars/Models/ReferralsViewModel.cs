﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ReferralsViewModel : BaseViewModel
    {
        public List<Referral> Referrals { get; set; }
        public List<TopReferrer> TopReferrers { get; set; }
        public List<Item> Rewards { get; set; }
        public string UniqueLink { get; set; }
        public int TotalEarned { get; set; }
        public bool HasAdditionalReferralsPower { get; set; }
    }
}