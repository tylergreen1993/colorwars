﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SecretGifterViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public string SecretGiftUsername { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}