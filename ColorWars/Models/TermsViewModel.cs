﻿namespace ColorWars.Models
{
    public class TermsViewModel : BaseViewModel
    {
        public bool HasClaimedReward { get; set; }
    }
}