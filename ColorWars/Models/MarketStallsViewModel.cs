﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MarketStallsViewModel : BaseViewModel
    {
        public List<MarketStall> NewMarketStalls { get; set; }
        public List<MarketStallItem> MarketStallItems { get; set; }
        public MarketStall CurrentMarketStall { get; set; }
        public List<Item> UserItems { get; set; }
        public List<MarketStallHistory> MarketStallHistories { get; set; }
        public bool IsUserMarketStall { get; set; }
        public bool OpenLedger { get; set; }
        public bool MatchExactSearch { get; set; }
        public int UserMarketStallId { get; set; }
        public int MaxMarketStallItems { get; set; }
        public int RepairCost { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public string SearchQuery { get; set; }
        public ItemCategory SearchCategory { get; set; }
        public ItemCondition SearchCondition { get; set; }

    }
}
