﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SwapDeckViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public bool CanSwapDeck { get; set; }
        public bool CanAccessMaze { get; set; }
        public bool CanEnterMaze { get; set; }
        public bool HasBetterSwapDeckLuck { get; set; }
        public int MazeLevel { get; set; }
        public List<SwapDeckHistory> SwapDeckHistories { get; set; }
        public DateTime MazeEntryDate { get; set; }
    }

    public enum MazeDirection
    {
        None = 0,
        Left = 1,
        Right = 2
    }
}