﻿namespace ColorWars.Models
{
    public class HelpViewModel : BaseViewModel
    {
        public Item Card { get; set; }
        public Item Enhancer { get; set; }
        public PowerMove PowerMove { get; set; }
    }
}