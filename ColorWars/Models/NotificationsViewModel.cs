﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class NotificationsViewModel : BaseViewModel
    {
        public List<Notification> Notifications { get; set; }
        public int UnreadNotificationsCount { get; set; }
    }
}