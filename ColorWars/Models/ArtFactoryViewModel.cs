﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ArtFactoryViewModel : BaseViewModel
    {
        public List<Custom> RecentCards { get; set; }
        public Item CustomCard { get; set; }
    }
}