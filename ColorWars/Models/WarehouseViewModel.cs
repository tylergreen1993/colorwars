﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class WarehouseViewModel : BaseViewModel
    {
        public List<StoreItem> BulkItems { get; set; }
        public List<Item> Companions { get; set; }
        public bool CanBuy { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}