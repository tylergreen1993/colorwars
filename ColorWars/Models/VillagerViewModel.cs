﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class VillagerViewModel : BaseViewModel
    {
        public List<Card> Cards { get; set; }
        public Item ThemeCard { get; set; }
        public int CardsGiven { get; set; }
        public bool CanTakeThemeCard { get; set; }
        public DateTime ThemeFinishedDate { get; set; }
    }
}