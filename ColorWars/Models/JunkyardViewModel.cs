﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class JunkyardViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public List<Item> SellItems { get; set; }
        public int DiscountPercent { get; set; }
    }
}