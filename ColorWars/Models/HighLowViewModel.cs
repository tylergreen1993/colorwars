﻿namespace ColorWars.Models
{
    public class HighLowViewModel : BaseViewModel
    {
        public Card Card { get; set; }
        public int Pot { get; set; }
        public int Number { get; set; }
        public int Streak { get; set; }
        public int TopStreak { get; set; }
        public bool IsInGame { get; set; }
    }
}