﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class BeyondWorldViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public List<Beyond> BeyondCards { get; set; }
        public List<User> HonorPathUsers { get; set; }
        public List<User> GreedPathUsers { get; set; }
        public List<Item> Items { get; set; }
        public Item BeyondCard { get; set; }
        public int Level { get; set; }
        public bool ShowAnimation { get; set; }
        public ColorWarsColor Color { get; set; }
        public BeyondWorldPath Path { get; set; }
    }

    public enum BeyondWorldPath
    {
        None = 0,
        Honor = 1,
        Greed = 2
    }
}