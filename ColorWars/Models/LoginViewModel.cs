﻿using System;
namespace ColorWars.Models
{
    public class LoginViewModel : BaseViewModel
    {
        public string ReturnUrl { get; set; }
    }
}
