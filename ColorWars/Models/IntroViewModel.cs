﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class IntroViewModel : BaseViewModel
    {
        public List<StarterItems> StarterItems { get; set; }
        public int AllottedBudget { get; set; }
        public IntroStage Stage { get; set; }
        public bool IsBetaEnabled { get; set; }
        public string EmailAddress { get; set; }
        public string Referral { get; set; }
        public string BetaCode { get; set; }
    }

    public enum IntroStage
    {
        First = 0,
        Second = 1,
        Third = 2,
        Fourth = 3,
        Completed = 4
    }
}