﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ItemsGuideViewModel : BaseViewModel
    {
        public List<Guid> ItemIds { get; set; }
        public List<ItemSection> ItemSections { get; set; }
        public bool HasShowroom { get; set; }
    }
}