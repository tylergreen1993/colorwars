﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class DeoJackViewModel : BaseViewModel
    {
        public int Points { get; set; }
        public int Total { get; set; }
        public int DealerTotal { get; set; }
        public DeoJackGameState GameState { get; set; }
        public List<Card> Cards { get; set; }
        public List<Card> DealerCards { get; set; }
    }

    public enum DeoJackGameState
    {
        Lost = -1,
        NotFinished = 0,
        Draw = 1,
        Won = 2
    }
}