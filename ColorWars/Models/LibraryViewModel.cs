﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class LibraryViewModel : BaseViewModel
    {
        public List<LibraryHistory> LibraryHistories { get; set; }
        public List<LibraryCard> LibraryCards { get; set; }
        public List<VoidMessage> VoidMessages { get; set; }
        public List<Card> Deck { get; set; }
        public Item UnknownItem { get; set; }
        public int PowerLevel { get; set; }
        public int OpportunityLevel { get; set; }
        public int WealthLevel { get; set; }
        public bool CanUseLibrary { get; set; }
        public bool HasGoldenKnowledge { get; set; }
        public bool CanOpenPassage { get; set; }
        public bool CanPullLever { get; set; }
        public bool CanSeeUnknown { get; set; }
        public bool CanChallengeUnknown { get; set; }
        public bool HasTakenUnknownItem { get; set; }
    }
}