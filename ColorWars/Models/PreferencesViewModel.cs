﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class PreferencesViewModel : BaseViewModel
    {
        public Settings Settings { get; set; }
        public List<Blocked> BlockedUsers { get; set; }
        public List<ActiveSession> ActiveSessions { get; set; }
        public List<HiddenFeedCategory> HiddenFeedCategories { get; set; }
        public List<Badge> AvailableBadges { get; set; }
    }

    public enum PointsLocation
    {
        [Display(Name = "Items")]
        Items = 0,
        [Display(Name = "Bank")]
        Bank = 1,
        [Display(Name = "Groups")]
        Groups = 2,
        [Display(Name = "Plaza")]
        Plaza = 3,
        [Display(Name = "Preferences")]
        Preferences = 4,
        [Display(Name = "Profile")]
        Profile = 5,
        [Display(Name = "Stadium")]
        Stadium = 6,
        [Display(Name = "StockMarket/Portfolio")]
        Portfolio = 7,
        [Display(Name = "MarketStalls")]
        MarketStalls = 8,
        [Display(Name = "AttentionHall")]
        AttentionHall = 9,
        [Display(Name = "Club")]
        Club = 10,
        [Display(Name = "Profile/Showroom")]
        Showroom = 11,
        [Display(Name = "Fair")]
        Fair = 12
    }
}