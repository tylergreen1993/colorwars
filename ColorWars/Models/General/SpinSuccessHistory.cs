﻿using System;
namespace ColorWars.Models
{
    public class SpinSuccessHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public SpinSuccessType Type { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum SpinSuccessType
    {
        Negative = -1,
        None = 0,
        Positive = 1
    }
}
