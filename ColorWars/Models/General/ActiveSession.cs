﻿using System;

namespace ColorWars.Models
{
    public class ActiveSession
    {
        public Guid AccessToken { get; set; }
        public string Username { get; set; }
        public string IPAddress { get; set; }
        public string UserAgent { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}