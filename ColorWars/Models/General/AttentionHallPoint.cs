﻿using System;

namespace ColorWars.Models
{
    public class AttentionHallPoint
    {
        public int PostId { get; set; }
        public string Username { get; set; }
        public AttentionHallPointType Points { get; set; }
        public int TotalPoints { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum AttentionHallPointType
    {
        Negative = -1,
        None = 0,
        Positive = 1
    }
}