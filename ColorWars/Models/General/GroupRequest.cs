﻿using System;

namespace ColorWars.Models
{
    public class GroupRequest
    {
        public int GroupId { get; set; }
        public string Username { get; set; }
        public bool Accepted { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum GroupRequestStatus
    {
        None = 0,
        Pending = 1,
        Accepted = 2
    }
}
