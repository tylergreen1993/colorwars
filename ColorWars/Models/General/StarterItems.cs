﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StarterItems
    {
        public List<Item> Items { get; set; }
        public int Index { get; set; }
        public int Balance { get; set; }
        public int Power { get; set; }
        public int Enhancer { get; set; }
        public string Title { get; set; }
    }
}
