﻿using System;

namespace ColorWars.Models
{
    public class ColorWarsSubmission
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public int Amount { get; set; }
        public ColorWarsColor TeamColor { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum ColorWarsColor
    {
        None = 0,
        Orange = 1,
        Blue = 2
    }
}
