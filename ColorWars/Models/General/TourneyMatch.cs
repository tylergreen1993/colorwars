﻿using System;

namespace ColorWars.Models
{
	public class TourneyMatch
	{
        public Guid Id { get; set; }
		public int TourneyId { get; set; }
        public int Round { get; set; }
        public int MatchesPerRound { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public string Username1 { get; set; }
        public string Username2 { get; set; }
        public string Winner { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string GetWinner()
        {
            if (!string.IsNullOrEmpty(Winner))
            {
                return Winner;
            }

            int winsNeeded = MatchesPerRound / 2 + 1;
            if (Wins == winsNeeded)
            {
                return Username1;
            }
            if (Losses == winsNeeded)
            {
                return Username2;
            }

            return null;
        }

        public bool HasMoreMatches()
        {
            if (string.IsNullOrEmpty(GetWinner()))
            {
                return true;
            }

            return false;
        }
    }
}
