﻿namespace ColorWars.Models
{
    public class Reward
    {
        public RewardId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Info { get; set; }
        public AccomplishmentCategory Category { get; set; }
        public int Cost { get; set; }
        public bool IsHidden { get; set; }
        public string ImageUrl => $"Accomplishments/{Category}-Trophy.png";
    }

    public enum RewardId
    {
        None = 0,
        OneMonthClub = 1,
        ExtraWizardVisit = 2,
        ExtraLibraryVisit = 3,
        StadiumBonus = 4,
        ExtraSwapDeckVisit = 5,
        RepairAllDeck = 6,
        ImproveDeckCard = 7,
        ExtraWishingStoneVisit = 8,
        MysteryPointsBag = 9,
        MysteryStockBag = 10,
        ThemeChange = 11,
        MaxBagSize = 12,
        DoubleBankInterest = 13,
        DoubleCPUReward = 14,
        UnlockPartnership = 15
    }
}