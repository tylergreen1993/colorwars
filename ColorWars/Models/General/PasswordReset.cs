﻿using System;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class PasswordReset
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
