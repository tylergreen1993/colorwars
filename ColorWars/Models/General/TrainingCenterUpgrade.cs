﻿namespace ColorWars.Models
{
    public class TrainingCenterUpgrade
    {
        public TrainingCenterUpgradeId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Tokens { get; set; }
        public string ImageUrl { get; set; }
    }

    public enum TrainingCenterUpgradeId
    {
        None = 0,
        ExtraPowerMoves = 1,
        ThemeBonusIncrease = 2,
        ExtraDeckSlot = 3,
        StadiumBonus = 4
    }
}