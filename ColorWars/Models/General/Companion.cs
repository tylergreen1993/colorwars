﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ColorWars.Classes;
using ColorWars.Repositories;

namespace ColorWars.Models
{
    public class Companion
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public CompanionType Type { get; set; }
        public string Name { get; set; }
        public CompanionColor Color { get; set; }
        public int Level { get; set; }
        public int Position { get; set; }
        public int Points { get; set; }
        public DateTime LastInteractedDate { get; set; }
        public DateTime HappinessDate { get; set; }
        public DateTime LastGiftGivenDate { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public string GetImageUrl()
        {
            return $"Companions/{Type}/{Color}{(IsAsleep() ? "_Asleep" : "")}.png";
        }

        public string Interact(User user, out bool shouldUpdate)
        {
            shouldUpdate = false;

            if (IsAsleep())
            {
                return $"{Name} is asleep. Try again later.";
            }

            if (user?.Username == Username)
            {
                if ((DateTime.UtcNow - LastInteractedDate).TotalHours >= 1)
                {
                    LastInteractedDate = DateTime.UtcNow;
                    HappinessDate = HappinessDate.AddHours(100) > DateTime.UtcNow ? DateTime.UtcNow : HappinessDate.AddHours(100);
                    if (GetHappiness() == 0)
                    {
                        HappinessDate = DateTime.UtcNow.AddHours(-300);
                    }
                    shouldUpdate = true;
                }
            }

            return Speak(user);
        }

        public int GetHappiness()
        {
            return Math.Max(0, (int)Math.Round(100 - (DateTime.UtcNow - HappinessDate).TotalHours / 4));
        }

        public bool CanGiveGift(User user)
        {
            if (user == null)
            {
                return false;
            }

            if (user.Username == Username)
            {
                if (IsAsleep())
                {
                    return false;
                }

                if (GetHappiness() >= 80 && (DateTime.UtcNow - LastGiftGivenDate).TotalDays >= 1)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsAsleep()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalHours % 3 == 1;
        }

        private string Speak(User user)
        {
            List<Location> locations = ServicesLocator.GetService<ILocationsRepository>().GetLocations(new Settings(), new List<Accomplishment>(), LocationArea.Plaza, true);
            Random rnd = new Random();

            List<string> choices = new List<string>()
            {
                $"{Name} wants to visit the Stadium one day.",
                $"{Name} has heard about the Deep Caverns.",
                $"{Name} is feeling sleepy.",
                $"{Name} wants to be bragged about at the Attention Hall.",
                $"{Name} wonders which item is being showcased at the Timed Showcase.",
                $"{Name} thinks you should make a wish at the Wishing Stone.",
                $"{Name} has heard some stories about the Young Wizard.",
                $"{Name} thinks the Stock Market is pretty cool.",
                $"{Name} has a favorite {Resources.DeckCard}, \"{Math.Max(1, (int)Math.Round(CreatedDate.Minute * 1.66))}\".",
                $"{Name} wants to make more friends.",
                $"{Name} is fascinated by the Museum.",
                $"{Name} wants to visit the Fair.",
                $"{Name} wants you to defeat your next Checkpoint CPU at the Stadium.",
                $"{Name} thinks you should play Streak Cards.",
                $"{Name} has a favorite location: {locations[CreatedDate.Minute % locations.Count].Name}!",
                $"{Name} thinks you should enter a tourney!",
            };

            if (CreatedDate.DayOfYear == DateTime.UtcNow.DayOfYear && CreatedDate.Year != DateTime.UtcNow.Year)
            {
                choices.Add($"{Name} is celebrating their birthday!");
            }
            else if ((int)(DateTime.UtcNow - CreatedDate).TotalDays == 180)
            {
                choices.Add($"{Name} is celebrating 6 months with {(user?.Username == Username ? "you" : Username)}!");
            }
            if (GetHappiness() < 50)
            {
                choices.Add($"{Name} feels very lonely these days.");
            }
            if (GetHappiness() < 80)
            {
                choices.Add($"{Name} needs a little affection from {(user?.Username == Username ? "you" : Username)}.");
            }
            else
            {
                if (user?.Username == Username)
                {
                    choices.Add($"{Name} stares at you affectionately.");
                }
                else
                {
                    choices.Add($"{Name} is very happy.");
                }
            }

            if (Type == CompanionType.Cat)
            {
                choices.Add($"{Name} stares at you curiously.");
                choices.Add($"{Name} meows at you.");
                choices.Add($"{Name} softly purrs.");
            }
            else if (Type == CompanionType.Dog)
            {
                choices.Add($"{Name} stares at you excitedly.");
                choices.Add($"{Name} barks at you.");
                choices.Add($"{Name} howls.");
            }
            else if (Type == CompanionType.Fish)
            {
                choices.Add($"{Name} stares at you and swims away.");
                choices.Add($"{Name} loves the water.");
                choices.Add($"{Name} floats around.");
            }
            else if (Type == CompanionType.Dragon)
            {
                choices.Add($"{Name} sometimes misses the Right-Side Up World.");
                choices.Add($"{Name} flies around.");
                choices.Add($"{Name} lets out a mighty roar.");
            }
            else if (Type == CompanionType.Panda)
            {
                choices.Add($"{Name} loves {Resources.Currency}");
                choices.Add($"{Name} chews on bamboo.");
                choices.Add($"{Name} sits around lazily.");
            }
            else if (Type == CompanionType.Monkey)
            {
                choices.Add($"{Name} makes a silly face at you.");
                choices.Add($"{Name} jumps up and down.");
                choices.Add($"{Name} loves bananas.");
            }
            else if (Type == CompanionType.Penguin)
            {
                choices.Add($"{Name} is hunting for fish.");
                choices.Add($"{Name} loves to swim.");
                choices.Add($"{Name} squawks.");
            }
            else if (Type == CompanionType.Unicorn)
            {
                choices.Add($"{Name} is feeling magical.");
                choices.Add($"{Name} cares for you very much.");
                choices.Add($"{Name} smiles majestically.");
            }

            if (user != null)
            {
                if (user.Points >= 10000)
                {
                    choices.Add($"{Name} admires all your {Resources.Currency} on hand.");
                }
                if (user.Role >= UserRole.Default)
                {
                    choices.Add($"{Name} think it's cool that you're part of the {Resources.SiteName} team.");
                }
                if (user.Username == Username)
                {
                    if (GetHappiness() >= 85)
                    {
                        choices.Add($"{Name} is glad to have you as an owner.");
                    }
                    int daysSinceActivation = (int)(DateTime.UtcNow - CreatedDate).TotalDays;
                    if (daysSinceActivation >= 3)
                    {
                        choices.Add($"{Name} has had you as an owner for {Helper.FormatNumber(daysSinceActivation)} days already.");
                    }
                }
                else
                {
                    choices.Add($"{Name} thinks you should be friends with {Username}.");
                }
                choices.Add($"{Name} has heard that you're user #{Helper.FormatNumber(user.Id)}.");
            }

            return choices[rnd.Next(choices.Count)];
        }
    }

    public enum CompanionColor
    {
        [Display(Name = "Primary")]
        Default = 0,
        [Display(Name = "Secondary")]
        One = 1,
        [Display(Name = "Tertiary")]
        Two = 2,
        [Display(Name = "Quaternary")]
        Three = 3,
        [Display(Name = "Quinary")]
        Four = 4,
        [Display(Name = "Senary")]
        Five = 5
    }
}
