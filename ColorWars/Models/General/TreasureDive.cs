﻿using System;
using System.Drawing;

namespace ColorWars.Models
{
    public class TreasureDive
    {
        public string Username { get; set; }
        public int Attempts { get; set; }
        public string TreasureCoordinates { get; set; }
        public string LastGuessCoordinates { get; set; }

        public bool IsMatch()
        {
            int distance = GetDistanceBetweenTreasureAndGuess();
            if (distance == -1)
            {
                return false;
            }

            return distance <= 15;
        }

        public Point GetTreasureCoordinates()
        {
            if (string.IsNullOrEmpty(TreasureCoordinates))
            {
                return Point.Empty;
            }

            string[] treasureCoordinates = TreasureCoordinates.Split("-");

            return new Point
            {
                X = int.Parse(treasureCoordinates[0]),
                Y = int.Parse(treasureCoordinates[1]),
            };
        }

        public Point GetLastGuessCoordinates()
        {
            if (string.IsNullOrEmpty(LastGuessCoordinates))
            {
                return Point.Empty;
            }

            string[] lastGuessCoordinates = LastGuessCoordinates.Split("-");
            if (lastGuessCoordinates.Length <= 1)
            {
                return Point.Empty;
            }

            int xPoint, yPoint;
            if (!int.TryParse(lastGuessCoordinates[0], out xPoint))
            {
                return Point.Empty;
            }
            if (!int.TryParse(lastGuessCoordinates[1], out yPoint))
            {
                return Point.Empty;
            }

            return new Point
            {
                X = xPoint,
                Y = yPoint
            };
        }

        public int GetDistanceBetweenTreasureAndGuess()
        {
            Point p1 = GetLastGuessCoordinates();
            Point p2 = GetTreasureCoordinates();
            if (p1 == Point.Empty || p2 == Point.Empty)
            {
                return -1;
            }

            return (int)Math.Round(Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2)));
        }

        public string GetDistanceHint()
        {
            int distance = GetDistanceBetweenTreasureAndGuess();
            if (distance == -1)
            {
                return "Tap somewhere on the map!";
            }
            if (distance > 300)
            {
                return "Incredibly Far!";
            }
            if (distance > 150)
            {
                return "Very Far!";
            }
            if (distance > 100)
            {
                return "Far!";
            }
            if (distance > 50)
            {
                return "Somewhat Close!";
            }
            if (distance > 30)
            {
                return "Close!";
            }
            if (distance > 15)
            {
                return "Very Close!";
            }
            return "Found!";
        }

        public void UpdateLastGuessCoordinates(Point treasureDiveCoordinate)
        {
            LastGuessCoordinates = $"{treasureDiveCoordinate.X}-{treasureDiveCoordinate.Y}";
        }
    }
}
