﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ColorWars.Models
{
    public class Tourney
    {
        public int Id { get; set; }
        public int EntranceCost { get; set; }
        public int MatchesPerRound { get; set; } = 3;
        public int RequiredParticipants { get; set; } = 8;
        public string Creator { get; set; }
        public string Name { get; set; }
        public TourneyCaliber TourneyCaliber { get; set; }
        public List<TourneyMatch> TourneyMatches { get; set; }
        public bool IsSponsored { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastRetrievedDate { get; set; }

        public TourneyStatus GetStatus()
        {
            List<TourneyMatch> firstRoundMatches = TourneyMatches.FindAll(x => x.Round == 0);
            if (firstRoundMatches.Count == RequiredParticipants/2 && firstRoundMatches.All(x => !string.IsNullOrEmpty(x.Username2)))
            {
                if (string.IsNullOrEmpty(GetWinner()))
                {
                    return TourneyStatus.InProgress;
                }

                return TourneyStatus.Completed;
            }

            return TourneyStatus.NotStarted;
        }

        public string GetWinner()
        {
            int finalRound = RequiredParticipants == 4 ? 1 : 2;

            TourneyMatch finalRoundMatch = TourneyMatches.Find(x => x.Round == finalRound);
            if (finalRoundMatch == null)
            {
                return null;
            }

            return finalRoundMatch.GetWinner();
        }

        public int GetEntrantsCount()
        {
            List<TourneyMatch> firstRoundMatches = TourneyMatches.FindAll(x => x.Round == 0);
            return firstRoundMatches.Count + firstRoundMatches.Count(x => !string.IsNullOrEmpty(x.Username2));
        }

        public List<string> GetEntrants(int round)
        {
            List<TourneyMatch> roundMatches = TourneyMatches.FindAll(x => x.Round == round);
            List<string> usernames = roundMatches.Select(x => x.Username1).ToList();
            usernames.AddRange(roundMatches.Select(x => x.Username2).ToList());
            usernames.RemoveAll(x => x == null);
            return usernames;
        }

        public bool IsParticipant(User user)
        {
            if (user == null)
            {
                return false;
            }

            return TourneyMatches.Any(x => x.Username1 == user.Username || x.Username2 == user.Username);
        }
    }

    public enum TourneyCaliber
    {
        [Display(Name = "Everyone")]
        All = 0,
        [Display(Name = "Elite Zone Only")]
        EliteOnly = 1,
        [Display(Name = "Non-Elite Zone Only")]
        NonEliteOnly = 2
    }

    public enum TourneyStatus
    {
        [Display(Name = "Not Started")]
        NotStarted = 0,
        [Display(Name = "In Progress")]
        InProgress = 1,
        [Display(Name = "Completed")]
        Completed = 2
    }
}
