﻿namespace ColorWars.Models
{
    public class QuarryStone
    {
        public int Position { get; set; }
        public QuarryStoneType Type { get; set; }
        public int MineAmount { get; set; }
        public bool IsRevealed { get; set; }
        public string Description => $"A stone that is {Type.ToString().ToLower()}.";
        public string ImageUrl => $"Quarry/Stone_{Type}.png";
    }

    public enum QuarryStoneType
    {
        Gray = 0,
        Red = 1,
        Yellow = 2,
        Green = 3,
        Blue = 4,
        Purple = 5
    }
}