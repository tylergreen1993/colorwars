﻿using System;

namespace ColorWars.Models
{
    public class StockHistory
    {
        public StockTicker Id { get; set; }
        public Guid SelectionId { get; set; }
        public string Username { get; set; }
        public int UserCost { get; set; }
        public int UserAmount { get; set; }
        public StockHistoryType HistoryType { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum StockHistoryType
    {
        None =  0,
        Buy = 1,
        Sold = 2
    }
}
