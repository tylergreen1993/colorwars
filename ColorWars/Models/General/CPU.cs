﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class CPU
    {
        public string Name { get; set; }
        public List<Card> Deck { get; set; }
        public List<Enhancer> Enhancers { get; set; }
        public int Level { get; set; }
        public int Reward { get; set; }
        public int Intensity { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsCheckpoint => !IsSpecial && (Level + 1) % 5 == 0;
    }

    public enum CPUChallenge
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "No Enhancers")]
        NoEnhancers = 1,
        [Display(Name = "Only Red Deck Cards")]
        OnlyRedDeckCards = 2,
        [Display(Name = "Only Even Numbered Deck Cards")]
        OnlyEvenDeckCards = 3,
        [Display(Name = "Only Odd Numbered Deck Cards")]
        OnlyOddDeckCards = 4,
        [Display(Name = "Only Addition Enhancers")]
        OnlyAdditionEnhancers = 5,
        [Display(Name = "Only Multiplication Enhancers")]
        OnlyMultiplicationEnhancers = 6,
        [Display(Name = "Only Same Colored Deck Cards")]
        OnlySameColorDeckCards = 7,
        [Display(Name = "Enhancers Worth Double")]
        EnhancersWorthDouble = 8,
        [Display(Name = "Deck Cards Worth Double")]
        DeckCardsWorthDouble = 9
    }
}
