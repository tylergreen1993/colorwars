﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class AdminHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string AdminUser { get; set; }
        public string Reason { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
