﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Stock
    {
        public StockTicker Id { get; set; }
        public Guid SelectionId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TopOwner { get; set; }
        public double CurrentCost { get; set; }
        public double DividendsPercent { get; set; }
        public int PurchaseCost { get; set; }
        public int UserAmount { get; set; }
        public int TotalAmount { get; set; }
        public DateTime PurchaseDate { get; set; }
    }

    public enum StockTicker
    {
        None =  0,
        DI = 1,
        STS = 2,
        COCO = 3,
        BZI = 4,
        IDE = 5,
        GCO = 6,
        CD = 7,
        BBA = 8,
        WS = 9,
        GS = 10,
        LALA = 11,
        STC = 12,
        PLPR = 13,
        BC = 14,
        NSL = 15,
        NN = 16,
        CHEC = 17,
        DD = 18
    }

    public enum StockSort
    {
        [Display(Name = "Name")]
        Name = 0,
        [Display(Name = "Lowest Cost")]
        Lowest = 1,
        [Display(Name = "Highest Cost")]
        Highest = 2,
        [Display(Name = "Highest Dividends")]
        HighestDividends = 3
    }

    public enum PortfolioSort
    {
        [Display(Name = "Date")]
        Date = 0,
        [Display(Name = "Stock")]
        Stock = 1,
    }
}
