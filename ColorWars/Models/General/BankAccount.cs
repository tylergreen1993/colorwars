﻿using System;

namespace ColorWars.Models
{
    public class BankAccount
    {
        public string Name { get; set; }
        public int Points { get; set; }

        private double _interest;
        public double Interest
        {
            get => _interest / 50;
            set => _interest = value;
        }
    }
}
