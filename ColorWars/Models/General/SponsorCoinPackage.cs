﻿namespace ColorWars.Models
{
    public class SponsorCoinPackage
    {
        public int CoinAmount { get; set; }
        public double MoneyAmount { get; set; }
        public string ImageUrl { get; set; }
        public string Label { get; set; }
    }
}