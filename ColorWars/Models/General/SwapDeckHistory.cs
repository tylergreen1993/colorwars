﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class SwapDeckHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public SwapDeckType Type { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum SwapDeckType
    {
        [Display(Name = "Disadvantage")]
        Negative = -1,
        [Display(Name = "N/A")]
        None = 0,
        [Display(Name = "Improvement")]
        Positive = 1
    }
}
