﻿namespace ColorWars.Models
{
    public class Status
    {
        public Status()
        {
            LoggedMessage = Classes.Messages.GetMessage();
        }

        private bool _success;
        public bool Success
        {
            get => _success;
            set => _success = string.IsNullOrEmpty(LoggedMessage) && value;
        }
        public object Object { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string InfoMessage { get; set; }
        public string DangerMessage { get; set; }
        public string Id { get; set; }
        public string ReturnUrl { get; set; }
        public string Points { get; set; }
        public string LoggedMessage { get; set; }
        public string ButtonText { get; set; }
        public string ButtonUrl { get; set; }
        public string SoundUrl { get; set; }
        public int Amount { get; set; }
        public bool StopExecuting { get; set; }
        public bool ShouldRefresh { get; set; }
        public bool ShowSpecialMessage { get; set; }
        public bool HideMessage { get; set; }
        public bool ScrollToTop { get; set; }
        public bool CanPlaySound { get; set; }
    }
}
