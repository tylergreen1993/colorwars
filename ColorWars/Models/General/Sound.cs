﻿namespace ColorWars.Models
{
    public class Sound
    {
        public SoundType Type { get; set; }
        public string Url { get; set; }
    }

    public enum SoundType
    {
        None = 0,
        Plop = 1,
        Victory = 2,
        Defeat = 3,
        Success = 4,
        Negative = 5,
        StadiumIntro = 6,
        Notification = 7,
        VictoryAlt = 8,
        Coins = 9,
        NegativeAlt = 10,
        SuccessShort = 11,
        NegativeShort = 12,
        Mine = 13,
        Scissors = 14,
        Shred = 15,
        SuccessShortAlt = 16
    }
}