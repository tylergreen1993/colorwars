﻿using System;

namespace ColorWars.Models
{
    public class ColorWarsHistory
    {
        public Guid Id { get; set; }
        public ColorWarsColor TeamColor { get; set; }
        public int Amount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}