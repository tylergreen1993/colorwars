﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class MatchState
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Opponent { get; set; }
        public Guid CardId { get; set; }
        public Guid EnhancerId { get; set; }
        public Guid CPUCardId { get; set; }
        public Guid CPUEnhancerId { get; set; }
        public Guid TourneyMatchId { get; set; }
        public ItemTheme CPUCardTheme { get; set; }
        public int Position { get; set; }
        public int Points { get; set; }
        public MatchStatus Status { get; set; }
        public PowerMoveType PowerMove { get; set; }
        public PowerMoveType CPUPowerMove { get; set; }
        public string ImageUrl { get; set; }
        public bool IsInSelection { get; set; }
        public bool IsCPU { get; set; }
        public bool IsCPUChallenge { get; set; }
        public bool IsSpecialCPU { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int GetSecondsLeft()
        {
            int secondsElapsed = (int)(DateTime.UtcNow - UpdatedDate).TotalSeconds;
            return Helper.GetMaxMatchSeconds() - secondsElapsed;
        }
    }

    public enum MatchStatus
    {
        Lost = -1,
        NotPlayed = 0,
        Won = 1,
        Draw = 2
    }

    public enum SpecialOpponents
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "Showroom Owner")]
        ShowroomOwner = 1,
        [Display(Name = "The Guard")]
        Guard = 2,
        [Display(Name = "The Unknown")]
        Unknown = 3,
        [Display(Name = "Challenger Dome")]
        ChallengerDome = 4,
        [Display(Name = "Phantom Spirit")]
        PhantomSpirit = 5,
        [Display(Name = "Evena")]
        Evena = 6,
        [Display(Name = "Oddna")]
        Oddna = 7,
        [Display(Name = "Phantom Boss")]
        PhantomBoss = 8,
        [Display(Name = "Guardian")]
        Guardian = 9,
        [Display(Name = "Beyond Creature")]
        BeyondCreature = 10
    }
}
