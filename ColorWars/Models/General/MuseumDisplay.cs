﻿namespace ColorWars.Models
{
    public class MuseumDisplay
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public double WidthToHeightRatio { get; set; }
        public int PinCode { get; set; }
        public MuseumWing MuseumWing { get; set; }
    }

    public enum MuseumWing
    {
        North = 0,
        East = 1,
        West = 2,
        South = 3,
        Boutique = 4,
        Records = 5
    }
}
