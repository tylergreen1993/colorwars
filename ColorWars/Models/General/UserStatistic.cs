﻿namespace ColorWars.Models
{
    public class UserStatistic
    {
        public string Username { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public int Amount { get; set; }
    }
}
