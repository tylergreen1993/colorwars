﻿using System;

namespace ColorWars.Models
{
    public class Report
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string ReportedUser { get; set; }
        public string IPAddress { get; set; }
        public string ClaimedUser { get; set; }
        public int AttentionHallPostId { get; set; }
        public string Reason { get; set; }
        public string Outcome { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
