﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MarketStall
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<MarketStallItem> Items { get; set; }
        public int Discount { get; set; }
        public DateTime CreatedDate { get; set; }

        private string _color;
        public string Color
        {
            get => string.IsNullOrEmpty(_color) ? "#337ab7" : _color;
            set => _color = value;
        }
    }
}
