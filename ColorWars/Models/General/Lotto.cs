﻿using System;

namespace ColorWars.Models
{
    public class Lotto
    {
        public int Id { get; set; }
        public int CurrentPot { get; set; }
        public int WinningNumber { get; set; }
        public string WinnerUsername { get; set; }
        public DateTime WonDate { get; set; }
    }
}
