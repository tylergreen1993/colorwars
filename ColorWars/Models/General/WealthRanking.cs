﻿namespace ColorWars.Models
{
    public class WealthRanking
    {
        public string Username { get; set; }
        public string DisplayPicUrl { get; set; }
        public string Color { get; set; }
        public int Rank { get; set; }
        public double Percentile { get; set; }
        public int TotalPoints { get; set; }
    }
}
