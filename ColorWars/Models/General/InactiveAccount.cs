﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class InactiveAccount
    {
        public string Username { get; set; }
        public InactiveAccountReason Reason { get; set; }
        public InactiveAccountType Type { get; set; }
        public string Info { get; set; }
        public DateTime InactiveDate { get; set; }
    }

    public enum InactiveAccountType
    {
        None = 0,
        Disabled = 1,
        Deleted = 2
    }

    public enum InactiveAccountReason
    {
        [Display(Name = "---")]
        None = -1,
        [Display(Name = "Concerned About Privacy")]
        ConcernedAboutPrivacy = 1,
        [Display(Name = "Too Boring")]
        TooBoring = 2,
        [Display(Name = "Too Confusing")]
        TooConfusing = 3,
        [Display(Name = "Too Difficult")]
        TooDifficult = 4,
        [Display(Name = "Too Distracting")]
        TooDistracting = 5,
        [Display(Name = "Unused Account")]
        UnusedAccount = 6,
        [Display(Name = "Taking a Break")]
        TakingABreak = 7,
        [Display(Name = "Other")]
        Other = 0
    }
}
