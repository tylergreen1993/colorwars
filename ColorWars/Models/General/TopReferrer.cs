﻿namespace ColorWars.Models
{
    public class TopReferrer
    {
        public int Rank { get; set; }
        public string Referrer { get; set; }
        public int Amount { get; set; }
    }
}
