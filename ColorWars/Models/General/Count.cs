﻿namespace ColorWars.Models
{
    public class Count
    {
        public int Amount { get; set; }
        public double TotalPercent { get; set; }
    }
}
