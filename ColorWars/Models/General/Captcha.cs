﻿namespace ColorWars.Models
{
    public class Captcha
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
        public int Answer { get; set; }
    }
}
