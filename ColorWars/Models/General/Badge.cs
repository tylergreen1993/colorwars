﻿using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Badge
    {
        public string ShortName { get; set; }
        public string Label { get; set; }
        public string Icon { get; set; }
        public BadgeType Type { get; set; }
    }

    public enum BadgeType
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "DeoDeck Club")]
        Club = 1,
        [Display(Name = "Sponsor Society")]
        Donor = 2,
        [Display(Name = "Ambassador")]
        Ambassador = 3,
        [Display(Name = "Beyond Honor")]
        BeyondHonor = 4,
        [Display(Name = "Beyond Greed")]
        BeyondGreed = 5,
        [Display(Name = "Community Contributor")]
        Contributor = 6,
        [Display(Name = "Orange Team")]
        OrangeTeam = 7,
        [Display(Name = "Blue Team")]
        BlueTeam = 8,
        [Display(Name = "Clubhouse Member")]
        Clubhouse = 9,
        [Display(Name = "Helper")]
        Helper = 100,
        [Display(Name = "Staff")]
        Staff = 101
    }
}