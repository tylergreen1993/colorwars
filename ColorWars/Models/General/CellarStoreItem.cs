﻿using System;

namespace ColorWars.Models
{
    public class CellarStoreItem
    {
        public CellarStoreItemId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public string ImageUrl => "CellarStore/Box.png";
    }

    public enum CellarStoreItemId
    {
        None = 0,
        MazeAccess = 1,
        ThemeSpeedUp = 2,
        CompanionColor = 3,
        CompanionLevel = 4,
        SummonMuseum = 5,
        ExtraNoticeBoardDay = 6,
        ExtendedStreakCardsStreakShort = 7,
        ExtendedStreakCardsStreakLong = 8,
        SummonRewardsStore = 9
    }
}
