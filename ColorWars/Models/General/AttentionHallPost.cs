﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class AttentionHallPost
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public AccomplishmentId? AccomplishmentId { get; set; }
        public Guid ItemId { get; set; }
        public ItemTheme? ItemTheme { get; set; }
        public Accomplishment Accomplishment { get; set; }
        public Item Item { get; set; }
        public MatchTag MatchTag { get; set; }
        public string MatchTagUsername { get; set; }
        public string ItemName { get; set; }
        public string ItemImageUrl { get; set; }
        public int Points { get; set; }
        public int TotalComments { get; set; }
        public AttentionHallPointType UserVote { get; set; }
        public AttentionHallPostType Type { get; set; }
        public AttentionHallFlair Flair { get; set; }
        public Guid JobSubmissionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime LastRetrievedFeedDate { get; set; }
        public bool IsModified => CreatedDate != ModifiedDate;
    }

    public enum AttentionHallPostsFilter
    {
        [Display(Name = "Popular")]
        Popular = 0,
        [Display(Name = "Newest")]
        Newest = 1,
        [Display(Name = "All-Time")]
        AllTime = 2,
        [Display(Name = "Your Posts")]
        UserCreated = 3
    }

    public enum AttentionHallPostType
    {
        [Display(Name = "All")]
        None = 0,
        [Display(Name = "General")]
        General = 1,
        [Display(Name = "Accomplishment")]
        Accomplishment = 2,
        [Display(Name = "Item")]
        Item = 3,
        [Display(Name = "Match Tag")]
        MatchTag = 4
    }

    public enum AttentionHallFlair
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "Wanted Notice")]
        Wanted = 1,
        [Display(Name = "Boast")]
        Boast = 2,
        [Display(Name = "Bug")]
        Bug = 3,
        [Display(Name = "Idea")]
        Idea = 4,
        [Display(Name = "Opinion")]
        Opinion = 5,
        [Display(Name = "Introduction")]
        Introduction = 6,
        [Display(Name = "Question")]
        Question = 7
    }
}