﻿namespace ColorWars.Models
{
    public class PrizeCup
    {
        public int Position { get; set; }
        public CardColor Color { get; set; }
        public bool IsRevealed => Color != CardColor.None;
        public string Description => IsRevealed ? $"It's a {Color} Marble!" : $"Flip it over!";
        public string ImageUrl => IsRevealed ? $"PrizeCup/{Color}Marble.png" : "Locations/PrizeCup.png";
    }
}