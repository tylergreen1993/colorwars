﻿using System;
namespace ColorWars.Models
{
    public class WizardRecord
    {
        public Guid Id { get; set; }
        public int Rank { get; set; }
        public string Username { get; set; }
        public int Amount { get; set; }
        public WizardRecordType Type { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum WizardRecordType
    {
        None = 0,
        PointsLoss = 1
    }
}
