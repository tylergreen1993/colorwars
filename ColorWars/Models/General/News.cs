﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<NewsComment> Comments { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
