﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class Bank
    {
        public string Username { get; set; }
        public int Points { get; set; }
        public DateTime UpdatedDate { get; set; }

        public BankAccount GetAccountType()
        {
            return GetBankAccounts().FindAll(x => x.Points <= Points).OrderByDescending(x => x.Points).First();
        }

        public BankAccount GetNextAccountType()
        {
            return GetBankAccounts().FindAll(x => x.Points > Points)?.OrderBy(x => x.Points)?.FirstOrDefault();
        }

        public int GetDailyInterest()
        {
            return Points == 0 ? 0 : (int)Math.Max(Math.Round(GetAccountType().Interest * Points), 1);
        }

        private List<BankAccount> GetBankAccounts()
        {
            return new List<BankAccount>
            {
                new BankAccount
                {
                    Name = "Beginner's Luck",
                    Interest = 0.01,
                    Points = 0
                },
                new BankAccount
                {
                    Name = "Smart Saver",
                    Interest = 0.015,
                    Points = 500
                },
                new BankAccount
                {
                    Name = "Junior Investor",
                    Interest = 0.02,
                    Points = 1000
                },
                new BankAccount
                {
                    Name = "Silver Savings",
                    Interest = 0.025,
                    Points = 2500
                },
                new BankAccount
                {
                    Name = "Gold Earner",
                    Interest = 0.03,
                    Points = 5000
                },
                new BankAccount
                {
                    Name = $"{Resources.Currency} Connoisseur",
                    Interest = 0.035,
                    Points = 7500
                },
                new BankAccount
                {
                    Name = "Investment Luminary",
                    Interest = 0.04,
                    Points = 10000
                },
                new BankAccount
                {
                    Name = "Exemplary Earnings",
                    Interest = 0.045,
                    Points = 15000
                },
                new BankAccount
                {
                    Name = "Rising Wealth",
                    Interest = 0.05,
                    Points = 20000
                },
                new BankAccount
                {
                    Name = "Silver Celebrations",
                    Interest = 0.055,
                    Points = 25000
                },
                new BankAccount
                {
                    Name = "Pearl Promises",
                    Interest = 0.06,
                    Points = 30000
                },
                new BankAccount
                {
                    Name = "Ruby Riches",
                    Interest = 0.065,
                    Points = 40000
                },
                new BankAccount
                {
                    Name = "Golden Slumbers",
                    Interest = 0.07,
                    Points = 50000
                },
                new BankAccount
                {
                    Name = "Diamond Desires",
                    Interest = 0.075,
                    Points = 60000
                },
                new BankAccount
                {
                    Name = "Rising Tycoon",
                    Interest = 0.08,
                    Points = 75000
                },
                new BankAccount
                {
                    Name = "Small Fortune",
                    Interest = 0.085,
                    Points = 100000
                },
                new BankAccount
                {
                    Name = "Medium Fortune",
                    Interest = 0.09,
                    Points = 150000
                },
                new BankAccount
                {
                    Name = "Large Fortune",
                    Interest = 0.095,
                    Points = 200000
                },
                new BankAccount
                {
                    Name = "Extreme Earner",
                    Interest = 0.1,
                    Points = 250000
                },
                new BankAccount
                {
                    Name = "Friends' Envy",
                    Interest = 0.105,
                    Points = 500000
                },
                new BankAccount
                {
                    Name = "Millionaire",
                    Interest = 0.11,
                    Points = 1000000
                },
                new BankAccount
                {
                    Name = "Multimillionaire",
                    Interest = 0.115,
                    Points = 2000000
                },
                new BankAccount
                {
                    Name = "Mega Wealth",
                    Interest = 0.12,
                    Points = 5000000
                },
                new BankAccount
                {
                    Name = "Wealthiest Wealth",
                    Interest = 0.125,
                    Points = 10000000
                }
            };
        }
    }
}
