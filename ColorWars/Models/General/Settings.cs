﻿using System;

namespace ColorWars.Models
{
    public class Settings
    {
        public int MaxBagSize { get; set; } = 20;
        public int MatchLevel { get; set; } = 0;
        public int SeekerLevel { get; set; } = 0;
        public int LibraryPowerLevel { get; set; } = 1;
        public int LibraryOpportunityLevel { get; set; } = 1;
        public int LibraryWealthLevel { get; set; } = 1;
        public int BuyersClubPoints { get; set; } = 0;
        public int AccomplishmentPointsSpent { get; set; } = 0;
        public int TrainingTokens { get; set; } = 0;
        public int StadiumChallengeWins { get; set; } = 0;
        public int MaxMarketStallSize { get; set; } = 15;
        public int MaxStorageSize { get; set; } = 50;
        public int MaxQuarryStonesMined { get; set; } = 10;
        public int SecretGifterRank { get; set; } = 0;
        public int MaxShowroomSize { get; set; } = 15;
        public int MaxCompanions { get; set; } = 6;
        public int BankLoanAmountOwed { get; set; } = 0;
        public int SponsorCoins { get; set; } = 0;
        public int PhantomDoorLevel { get; set; } = 0;
        public int MaxPowerMoves { get; set; } = 8;
        public int ThemeBonusBoost { get; set; } = 0;
        public int BeyondWorldLevel { get; set; } = 0;
        public DateTime BankInterestDate { get; set; } = DateTime.MinValue;
        public DateTime CandyExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime CurseExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime StadiumBonusExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime DonationsTakeDate { get; set; } = DateTime.MinValue;
        public DateTime TimedShowcasePurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime LastWishDate { get; set; } = DateTime.MinValue;
        public DateTime LastWizardDate { get; set; } = DateTime.MinValue;
        public DateTime CourierMessageSentDate { get; set; } = DateTime.MinValue;
        public DateTime SpecialCourierMessageSentDate { get; set; } = DateTime.MinValue;
        public DateTime DividendsCollectDate { get; set; } = DateTime.MinValue;
        public DateTime LastLottoEntry { get; set; } = DateTime.MinValue;
        public DateTime LastStreakCardsPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastDailyStreakCardsPlayDate { get; set; } = DateTime.MinValue;
        public DateTime StreakCardsNoExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime StadiumChallengeExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime SeekerChallengeExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime MerchantsExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime RewardsStoreExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime NoticeBoardExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime WarehouseExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime DeepCavernsExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime RightSideUpWorldExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime SecretGifterExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime MuseumExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime SurveyExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime LastSwapDeckDate { get; set; } = DateTime.MinValue;
        public DateTime LastGeneralStoreBuyDate { get; set; } = DateTime.MinValue;
        public DateTime LastBuyersClubBuyDate { get; set; } = DateTime.MinValue;
        public DateTime LastLottoBuyDate { get; set; } = DateTime.MinValue;
        public DateTime LastLibraryKnowledgeUseDate { get; set; } = DateTime.MinValue;
        public DateTime LastAttentionHallPostDate { get; set; } = DateTime.MinValue;
        public DateTime LastClubBonusDate { get; set; } = DateTime.MinValue;
        public DateTime IncubationStartDate { get; set; } = DateTime.MinValue;
        public DateTime MerchantsContestEnteredDate { get; set; } = DateTime.MinValue;
        public DateTime LastNewLocationDate { get; set; } = DateTime.MinValue;
        public DateTime StealthProtectExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime StealthGrabStolenLastDate { get; set; } = DateTime.MinValue;
        public DateTime FurnaceDefrostDate { get; set; } = DateTime.MinValue;
        public DateTime HoroscopeExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime SeedCardTakeDate { get; set; } = DateTime.MinValue;
        public DateTime RewardsStoreMaxClaimedDate { get; set; } = DateTime.MinValue;
        public DateTime WarehouseMaxPurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime BagAddedLastDate { get; set; } = DateTime.MinValue;
        public DateTime LastSorceressEffectDate { get; set; } = DateTime.MinValue;
        public DateTime SorceressStadiumCurseProtectExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime LastRightSideUpWorldItemPurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime LastQuarryStoneMineDate { get; set; } = DateTime.MinValue;
        public DateTime LastBoutiquePurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime LastEliteShopPurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime LastSecretShrineItemGivenDate { get; set; } = DateTime.MinValue;
        public DateTime LastUnknownStadiumChallengeDate { get; set; } = DateTime.MinValue;
        public DateTime LavenderStopExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime Crystal4thExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime LastCrystal4thPointsAddDate { get; set; } = DateTime.MinValue;
        public DateTime LastMazeEntryDate { get; set; } = DateTime.MinValue;
        public DateTime LastRepairShopDiscountCourierDate { get; set; } = DateTime.MinValue;
        public DateTime LastMatchingColorsPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastColorWarsContributeDate { get; set; } = DateTime.MinValue;
        public DateTime LastSpinSuccessDate { get; set; } = DateTime.MinValue;
        public DateTime LastTakeOfferPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastPrizeCupPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastCrystalBallPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastTreasureDivePlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastShowroomAssessmentDate { get; set; } = DateTime.MinValue;
        public DateTime PurchaseMoreStocksDate { get; set; } = DateTime.MinValue;
        public DateTime LastLoginBonusDate { get; set; } = DateTime.MinValue;
        public DateTime ThemeCardSqueezeDate { get; set; } = DateTime.MinValue;
        public DateTime BagProtectExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime StoreMagicianExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime LastWealthyDonorsTakeDate { get; set; } = DateTime.MinValue;
        public DateTime LastOnTheHourDate { get; set; } = DateTime.MinValue;
        public DateTime LastChallengerDomeMatchDate { get; set; } = DateTime.MinValue;
        public DateTime ChallengerDomeStreakDate { get; set; } = DateTime.MinValue;
        public DateTime LastPhantomBossStadiumChallengeDate { get; set; } = DateTime.MinValue;
        public DateTime LastFeedVisitDate { get; set; } = DateTime.MinValue;
        public DateTime LastTopCounterPlayDate { get; set; } = DateTime.MinValue;
        public DateTime CraftingLuckExpiryDate { get; set; } = DateTime.MinValue;
        public DateTime CellarStoreLastPurchaseDate { get; set; } = DateTime.MinValue;
        public DateTime LuckyGuessLastPlayDate { get; set; } = DateTime.MinValue;
        public DateTime LastPrimaryGroupChangeDate { get; set; } = DateTime.MinValue;
        public DateTime LastClubhouseBuyDate { get; set; } = DateTime.MinValue;
        public int StreakCardsStreak { get; set; } = 0;
        public int CandyPercent { get; set; } = 0;
        public int CursePercent { get; set; } = 0;
        public int FillerPercent { get; set; } = 0;
        public int StadiumBonusPercent { get; set; } = 0;
        public int OpponentFillerPercent { get; set; } = 0;
        public int StadiumSelfDestructMultiplier { get; set; } = 0;
        public int OpponentStadiumSelfDestructMultiplier { get; set; } = 0;
        public int StadiumRevivalRound { get; set; } = -1;
        public int OpponentStadiumRevivalRound { get; set; } = -1;
        public int StadiumPlannedStrikeMultiplier { get; set; } = 0;
        public int OpponentStadiumPlannedStrikeMultiplier { get; set; } = 0;
        public int StadiumPoisonTouchCurse { get; set; } = 0;
        public int OpponentStadiumPoisonTouchCurse { get; set; } = 0;
        public int StadiumPsychUpCurse { get; set; } = 0;
        public int StadiumOpponentPsychUpCurse { get; set; } = 0;
        public int StadiumCPUBonus { get; set; } = 0;
        public int JunkyardCouponPercent { get; set; } = 0;
        public int RepairCouponPercent { get; set; } = 0;
        public int GeneralStoreCouponPercent { get; set; } = 0;
        public int ExtraReferralPointsEarned { get; set; } = 0;
        public int GeneralStoreRecentBuyCount { get; set; } = 0;
        public int BuyersClubRecentBuyCount { get; set; } = 0;
        public int DaysUntilNextMerchantsVisit { get; set; } = 0;
        public int MerchantsJewelsSold { get; set; } = 0;
        public int NoticeBoardTasksCompleted { get; set; } = 0;
        public int RewardsStoreTotalClaimed { get; set; } = 0;
        public int WarehouseRecentBuyCount { get; set; } = 0;
        public int GoldBriefcasePinCode { get; set; } = 0;
        public int MatchStartGlobalRanking { get; set; } = 0;
        public int StadiumTutorialLevel { get; set; } = -1;
        public int HighLowPot { get; set; } = 0;
        public int HighLowNumber { get; set; } = 0;
        public int HighLowCardNumber { get; set; } = 0;
        public int HighLowStreak { get; set; } = 0;
        public int HighLowTopStreak { get; set; } = 0;
        public int LuckLevel { get; set; } = 0;
        public int MazeLevel { get; set; } = 0;
        public int CrystalBallStreak { get; set; } = 0;
        public int DisposerTrainingTokenProgress { get; set; } = 0;
        public int LoginBonusStreak { get; set; } = 0;
        public int TowerFloorLevel { get; set; } = 0;
        public int TowerFloorRecordLevel { get; set; } = 0;
        public int VillagerCardsGiven { get; set; } = 0;
        public int ChallengerDomeStreak { get; set; } = 0;
        public int TotalTourneysWon { get; set; } = 0;
        public int DonatedItems { get; set; } = 0;
        public int DeoJackPoints { get; set; } = 0;
        public int SorceressGuardiansDefeated { get; set; } = 0;
        public int BatchNotificationEmailMinutes { get; set; } = 30;
        public int DoubtItWins { get; set; } = 0;
        public int StadiumEnhancerUses { get; set; } = 0;
        public double MerchantsJewelCardMultiplier { get; set; } = 0;
        public double StadiumRewardLuckBonus { get; set; } = 0;
        public bool ShowUserMatchesFirst { get; set; } = false;
        public bool DisableMessages { get; set; } = false;
        public bool OnlyShowUserMatchesStatistics { get; set; } = false;
        public bool EnableReadReceipts { get; set; } = true;
        public bool ShowGroupOnProfile { get; set; } = true;
        public bool ShowMarketStallOnProfile { get; set; } = true;
        public bool NotificationsForNewGroupMembers { get; set; } = true;
        public bool ShowGroupMessagesFirst { get; set; } = false;
        public bool ReceiveStadiumChallenges { get; set; } = true;
        public bool CheckedStadiumChallenge { get; set; } = false;
        public bool CheckedMerchants { get; set; } = false;
        public bool CheckedRewardsStore { get; set; } = false;
        public bool CheckedNoticeBoard { get; set; } = false;
        public bool CheckedWarehouse { get; set; } = false;
        public bool CheckedSecretGifter { get; set; } = false;
        public bool CheckedSurvey { get; set; } = false;
        public bool CheckedMuseum { get; set; } = false;
        public bool EmailsForMarketStallPurchases { get; set; } = false;
        public bool ShowNewsOnFeed { get; set; } = true;
        public bool HasClaimedTermsReward { get; set; } = false;
        public bool ReceiveAttentionHallPostsCommentsNotifications { get; set; } = true;
        public bool ShowTrendingAttentionHallPostsOnFeed { get; set; } = true;
        public bool HasWizardBetterLuck { get; set; } = false;
        public bool HasWizardGoldWand { get; set; } = false;
        public bool HasLibraryGoldenKnowledge { get; set; } = false;
        public bool HasBetterSpinLuck { get; set; } = false;
        public bool HasBetterTreasureDiveLuck { get; set; } = false;
        public bool HasDoubleQuarryHaul { get; set; } = false;
        public bool HasBetterSwapDeckLuck { get; set; } = false;
        public bool NotificationsForGroupJoinRequests { get; set; } = true;
        public bool HasDoubleBankInterest { get; set; } = false;
        public bool HasDoubleStadiumCPUReward { get; set; } = false;
        public bool StadiumHasLuckUp { get; set; } = false;
        public bool OpponentStadiumHasLuckUp { get; set; } = false;
        public bool ShowCompanionsOnProfile { get; set; } = true;
        public bool ShowStadiumFireworksAnimation { get; set; } = true;
        public bool HasReceivedAccomplishmentsCourierMessage { get; set; } = false;
        public bool HasReceived35AccomplishmentsCourierMessage { get; set; } = false;
        public bool HasReceived50AccomplishmentsCourierMessage { get; set; } = false;
        public bool HasReceivedShowroomCourierMessage { get; set; } = false;
        public bool HasReceivedPhantomDoorCourierMessage { get; set; } = false;
        public bool HasReceivedSecondMatchCourierMessage { get; set; } = false;
        public bool HasReceivedBeyondWorldCourierMessage { get; set; } = false;
        public bool HasReceivedAllChecklistCourierMessage { get; set; } = false;
        public bool HasReceivedStadiumChallengesCourierMessage { get; set; } = false;
        public bool HasReceivedFiveStadiumWinsCourierMessage { get; set; } = false;
        public bool HasReceivedTenStadiumWinsCourierMessage { get; set; } = false;
        public bool HasReceivedFifteenStadiumWinsCourierMessage { get; set; } = false;
        public bool HasReceivedTwentyStadiumWinsCourierMessage { get; set; } = false;
        public bool HasReceivedFiveStadiumUserWinsCourierMessage { get; set; } = false;
        public bool HasReceivedFiftyStadiumUserWinsCourierMessage { get; set; } = false;
        public bool HasReceivedAllMuseumOpenedCourierMessage { get; set; } = false;
        public bool HasReceivedArtShowroomCourierMessage { get; set; } = false;
        public bool HasReceivedCourierPinCodeMessage { get; set; } = false;
        public bool HasReceived10AccomplishmentsCourierMessage { get; set; } = false;
        public bool HasReceivedClubTrialCourierMessage { get; set; } = false;
        public bool HasReceivedSecretCourierMessage { get; set; } = false;
        public bool ShowUnreadGroupMessagesOnFeed { get; set; } = true;
        public bool ReceiveAuctionBidNotifications { get; set; } = true;
        public bool HasTakenRightSideUpGuardItem { get; set; } = false;
        public bool HasTakenUnknownItem { get; set; } = false;
        public bool AllowPartnershipRequests { get; set; } = true;
        public bool PartnershipEnabled { get; set; } = false;
        public bool AllowUserMentionNotifications { get; set; } = true;
        public bool ShowDisposerConfirmation { get; set; } = true;
        public bool HasSeenTutorial { get; set; } = false;
        public bool HasSeenBagTutorial { get; set; } = false;
        public bool HasSeenStadiumTutorial { get; set; } = false;
        public bool HasSeenStadiumRelicsTutorial { get; set; } = false;
        public bool HasSeenRelicRequirementTutorial { get; set; } = false;
        public bool HasSeenStadiumCheckpointTutorial { get; set; } = false;
        public bool HasSeenStadiumSuddenDeathTutorial { get; set; } = false;
        public bool HasSeenEliteZoneTutorial { get; set; } = false;
        public bool HasSeenProfileAccomplishmentTutorial { get; set; } = false;
        public bool HasSeenEnableNotifications { get; set; } = false;
        public bool HasSeenLocationsTutorial { get; set; } = false;
        public bool HasSeenFairTutorial { get; set; } = false;
        public bool HasSeenReferUserTutorial { get; set; } = false;
        public bool HasSeenSponsorTutorial { get; set; } = false;
        public bool HasSeenLocationFavoritesTutorial { get; set; } = false;
        public bool HasSeenStadiumPowerMovesTutorial { get; set; } = false;
        public bool HasSeenStadiumCheckpointDefeatedTutorial { get; set; } = false;
        public bool HasSeenSecondMatchTutorial { get; set; } = false;
        public bool HasSeenEnhancerLimitedUsesTutorial { get; set; } = false;
        public bool HasSeenBagPowerMovesTutorial { get; set; } = false;
        public bool StadiumIsConfused { get; set; } = false;
        public bool OpponentStadiumIsConfused { get; set; } = false;
        public bool AllowUserChallenges { get; set; } = true;
        public bool FinishedPrizeCupGame { get; set; } = false;
        public bool ExpandLocations { get; set; } = false;
        public bool HasReceivedReturnMessage { get; set; } = false;
        public bool EnableSoundEffects { get; set; } = false;
        public bool HasLearntTranslation { get; set; } = false;
        public bool HasDefeatedEvena { get; set; } = false;
        public bool HasDefeatedOddna { get; set; } = false;
        public bool ReceivedPhantomDoorLanguageHint { get; set; } = false;
        public bool EnableCPUChallengingDifficulty { get; set; } = false;
        public bool HasSeenEnableSoundEffects { get; set; } = false;
        public bool DoubtItCheatMessageSuccess { get; set; } = false;
        public bool UnlockedExtraDeck { get; set; } = false;
        public bool HasSeenMessagesPopup { get; set; } = false;
        public bool CanShowMessagesTab { get; set; } = true;
        public bool HasReceivedStoreMagicianTrialMessage { get; set; } = false;
        public bool VerifiedEmail { get; set; }
        public bool HasSeenStadiumTipTutorial { get; set; }
        public string QuarryStonePositions { get; set; } = string.Empty;
        public string SecretGiftUsername { get; set; } = string.Empty;
        public string SurveyQuestionIds { get; set; } = string.Empty;
        public string ShowroomTitle { get; set; } = string.Empty;
        public string ShowroomDescription { get; set; } = string.Empty;
        public string ShowroomColor { get; set; } = string.Empty;
        public string SecretQuarryStonePattern { get; set; } = string.Empty;
        public string PartnershipUsername { get; set; } = string.Empty;
        public string RedeemedGuideIds { get; set; } = string.Empty;
        public string MatchingColorsPositions { get; set; } = string.Empty;
        public string CrystalBallColors { get; set; } = string.Empty;
        public string PrizeCupPositions { get; set; } = string.Empty;
        public string PhantomDoorSpeltWord { get; set; } = string.Empty;
        public string TakenPhantomItems { get; set; } = string.Empty;
        public string TakenBeyondWorldItems { get; set; } = string.Empty;
        public string DeoJackCards { get; set; } = string.Empty;
        public string DeoJackDealerCards { get; set; } = string.Empty;
        public string TopCounterCards { get; set; } = string.Empty;
        public string PastLottoGuesses { get; set; } = string.Empty;
        public string DoubtItLastPlayed { get; set; } = string.Empty;
        public string DoubtItCheatMessage { get; set; } = string.Empty;
        public string Notes { get; set; } = string.Empty;
        public string ReferralItemsClaimed { get; set; } = string.Empty;
        public string ArtFactoryPreviewImage { get; set; } = string.Empty;
        public string EmailVerificationCode { get; set; } = string.Empty;
        public JobTitle JobTitle { get; set; } = JobTitle.Recruit;
        public PointsLocation PointsLocation { get; set; } = PointsLocation.Bank;
        public StockSort StockMarketSort { get; set; } = StockSort.Name;
        public PortfolioSort StockMarketPortfolioSort { get; set; } = PortfolioSort.Stock;
        public ItemSort ItemSort { get; set; } = ItemSort.Category;
        public ItemSort StorageSort { get; set; } = ItemSort.Manual;
        public AccomplishmentSort AccomplishmentSort { get; set; } = AccomplishmentSort.Easiest;
        public AttentionHallPostsFilter AttentionHallPostsFilter { get; set; } = AttentionHallPostsFilter.Popular;
        public AttentionHallPostType AttentionHallPostsTypeFilter { get; set; } = AttentionHallPostType.None;
        public CPUChallenge StadiumChallenge { get; set; } = CPUChallenge.None;
        public HoroscopeType ActiveHoroscope { get; set; } = HoroscopeType.None;
        public CardColor PlantedItemSeedColor { get; set; } = CardColor.None;
        public AccomplishmentId LastReceivedCourierChecklistAccomplishmentId { get; set; } = AccomplishmentId.None;
        public IntroStage CurrentIntroStage { get; set; } = IntroStage.First;
        public ChecklistRank LastCheckedChecklistRank { get; set; } = ChecklistRank.Beginner;
        public BadgeType ActiveBadge { get; set; } = BadgeType.None;
        public ShowroomCertification ShowroomCertification { get; set; } = ShowroomCertification.None;
        public ItemTheme ThemeSqueezeType { get; set; } = ItemTheme.Default;
        public BeyondWorldPath BeyondWorldPath { get; set; } = BeyondWorldPath.None;
        public DoubtItOwner DoubtItTurn { get; set; } = DoubtItOwner.You;
        public DeckType DefaultDeckType { get; set; } = DeckType.Primary;
    }

    public enum ShowroomCertification
    {
        None = 0,
        Bronze = 1,
        Silver = 2,
        Gold = 3,
        Platinum = 4
    }
}
