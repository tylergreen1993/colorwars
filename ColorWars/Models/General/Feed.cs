﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class Feed
    {
        public int Id { get; set; }
        public Guid Guid { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public string Object { get; set; }
        public string OnClick { get; set; }
        public FeedCategory Category { get; set; }
        public string Href { get; set; }
        public DateTime CreatedDate { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get => string.IsNullOrEmpty(_imageUrl) ? GetImageUrl(Category) : Helper.GetImageUrl(_imageUrl);
            set => _imageUrl = value;
        }

        private string GetImageUrl(FeedCategory category)
        {
            switch (category)
            {
                case FeedCategory.News:
                    return Helper.GetImageUrl("Locations/News.png");
                case FeedCategory.Accomplishments:
                    return Helper.GetImageUrl("Accomplishments/Gray-Trophy.png");
                case FeedCategory.Referrals:
                    return Helper.GetImageUrl("Locations/Referral.png");
                case FeedCategory.Auctions:
                    return Helper.GetImageUrl("Locations/Auction.png");
                case FeedCategory.Trades:
                    return Helper.GetImageUrl("Locations/Trading.png");
                case FeedCategory.Jobs:
                    return Helper.GetImageUrl("Locations/JobCenter.png");
                case FeedCategory.Groups:
                    return Helper.GetImageUrl("Locations/Groups.png");
                case FeedCategory.Stocks:
                    return Helper.GetImageUrl("Locations/Stocks.png");
                case FeedCategory.Bank:
                    return Helper.GetImageUrl("Locations/Bank.png");
                case FeedCategory.YoungWizard:
                    return Helper.GetImageUrl("Locations/Wizard.png");
                case FeedCategory.WishingStone:
                    return Helper.GetImageUrl("Locations/WishingStone.png");
                case FeedCategory.Lotto:
                    return Helper.GetImageUrl("Locations/Lotto.png");
                case FeedCategory.StreakCards:
                    return Helper.GetImageUrl("Locations/StreakCards.png");
                case FeedCategory.RepairShop:
                    return Helper.GetImageUrl("Locations/Repair.png");
                case FeedCategory.Stadium:
                    return Helper.GetImageUrl("Locations/Stadium.png");
                case FeedCategory.SwapDeck:
                    return Helper.GetImageUrl("Locations/SwapDeck.png");
                case FeedCategory.MarketStalls:
                    return Helper.GetImageUrl("Locations/MarketStalls.png");
                case FeedCategory.Library:
                    return Helper.GetImageUrl("Locations/Library.png");
                case FeedCategory.AttentionHall:
                    return Helper.GetImageUrl("Locations/AttentionHall.png");
                case FeedCategory.Seeker:
                    return Helper.GetImageUrl("Locations/Seeker.png");
                case FeedCategory.Club:
                    return Helper.GetImageUrl("Locations/Club.png");
                case FeedCategory.Merchants:
                    return Helper.GetImageUrl("Locations/Merchants.png");
                case FeedCategory.Showcase:
                    return Helper.GetImageUrl("Locations/Showcase.png");
                case FeedCategory.RewardsStore:
                    return Helper.GetImageUrl("Locations/RewardsStore.png");
                case FeedCategory.NoticeBoard:
                    return Helper.GetImageUrl("Locations/NoticeBoard.png");
                case FeedCategory.Effects:
                    return Helper.GetImageUrl("Locations/ActiveEffects.png");
                case FeedCategory.Crafting:
                    return Helper.GetImageUrl("Locations/Crafting.png");
                case FeedCategory.Store:
                    return Helper.GetImageUrl("Locations/Store.png");
                case FeedCategory.Warehouse:
                    return Helper.GetImageUrl("Locations/Warehouse.png");
                case FeedCategory.Companions:
                    return Helper.GetImageUrl("Global/Logo.png");
                case FeedCategory.DeepCaverns:
                    return Helper.GetImageUrl("Locations/Caverns.png");
                case FeedCategory.RightSideUpWorld:
                    return Helper.GetImageUrl("Locations/Portal.png");
                case FeedCategory.DonationZone:
                    return Helper.GetImageUrl("Locations/Donate.png");
                case FeedCategory.SecretGifter:
                    return Helper.GetImageUrl("Locations/SecretGifter.png");
                case FeedCategory.Survey:
                    return Helper.GetImageUrl("Locations/Survey.png");
                case FeedCategory.Museum:
                    return Helper.GetImageUrl("Locations/Museum.png");
                case FeedCategory.SecretShrine:
                    return Helper.GetImageUrl("Locations/SecretShrine.png");
                case FeedCategory.Partnership:
                    return Helper.GetImageUrl("Locations/Partnership.png");
                case FeedCategory.BuyersClub:
                    return Helper.GetImageUrl("Locations/Buyers.png");
                case FeedCategory.MatchingColors:
                    return Helper.GetImageUrl("Locations/MatchingColors.png");
                case FeedCategory.ColorWars:
                    return Helper.GetImageUrl("Locations/ColorWars.png");
                case FeedCategory.SpinSuccess:
                    return Helper.GetImageUrl("Locations/SpinSuccess.png");
                case FeedCategory.MatchTags:
                    return Helper.GetImageUrl("Stadium/MatchTag.png");
                case FeedCategory.Checklist:
                    return Helper.GetImageUrl("Home/Checklist.png");
                case FeedCategory.TakeOffer:
                    return Helper.GetImageUrl("Locations/TakeOffer.png");
                case FeedCategory.Fair:
                    return Helper.GetImageUrl("Locations/Fair.png");
                case FeedCategory.CrystalBall:
                    return Helper.GetImageUrl("Locations/CrystalBall.png");
                case FeedCategory.TrainingCenter:
                    return Helper.GetImageUrl("Locations/TrainingCenter.png");
                case FeedCategory.PrizeCup:
                    return Helper.GetImageUrl("Locations/PrizeCup.png");
                case FeedCategory.TreasureDive:
                    return Helper.GetImageUrl("Locations/TreasureDive.png");
                case FeedCategory.DailyBonus:
                    return Helper.GetImageUrl("DailyBonus/TopPrize.png");
                case FeedCategory.StadiumLobby:
                    return Helper.GetImageUrl("Locations/Stadium.png");
                case FeedCategory.Tower:
                    return Helper.GetImageUrl("Locations/Tower.png");
                case FeedCategory.SponsorSociety:
                    return Helper.GetImageUrl("Locations/SponsorSociety.png");
                case FeedCategory.WearyVillager:
                    return Helper.GetImageUrl("Locations/Villager.png");
                case FeedCategory.StoreMagician:
                    return Helper.GetImageUrl("Locations/StoreMagician.png");
                case FeedCategory.WealthyDonors:
                    return Helper.GetImageUrl("Locations/WealthyDonors.png");
                case FeedCategory.ChallengerDome:
                    return Helper.GetImageUrl("Locations/ChallengerDome.png");
                case FeedCategory.PhantomDoor:
                    return Helper.GetImageUrl("Locations/PhantomDoor.png");
                case FeedCategory.Tourney:
                    return Helper.GetImageUrl("Locations/Tourney.png");
                case FeedCategory.TopCounter:
                    return Helper.GetImageUrl("Locations/TopCounter.png");
                case FeedCategory.BeyondWorld:
                    return Helper.GetImageUrl("Locations/BeyondWorld.png");
                case FeedCategory.TopAdoptions:
                    return Helper.GetImageUrl("Locations/TopAdoptions.png");
                case FeedCategory.LuckyGuess:
                    return Helper.GetImageUrl("Locations/LuckyGuess.png");
                case FeedCategory.ArtFactory:
                    return Helper.GetImageUrl("Locations/ArtFactory.png");
                default:
                    return Helper.GetImageUrl("Global/Logo.png");
            }
        }
    }

    public enum FeedCategory
    {
        [Display(Name = "None")]
        None = -1,
        [Display(Name = "All")]
        All = 0,
        [Display(Name = "News")]
        News = 1,
        [Display(Name = "Accomplishments")]
        Accomplishments = 2,
        [Display(Name = "Referrals")]
        Referrals = 3,
        [Display(Name = "Auction House")]
        Auctions = 4,
        [Display(Name = "Trading Square")]
        Trades = 5,
        [Display(Name = "Job Center")]
        Jobs = 6,
        [Display(Name = "Groups")]
        Groups = 7,
        [Display(Name = "Stock Market")]
        Stocks = 8,
        [Display(Name = "Bank")]
        Bank = 9,
        [Display(Name = "Young Wizard")]
        YoungWizard = 10,
        [Display(Name = "Wishing Stone")]
        WishingStone = 11,
        [Display(Name = "Lotto")]
        Lotto = 12,
        [Display(Name = "Streak Cards")]
        StreakCards = 13,
        [Display(Name = "Repair Shop")]
        RepairShop = 14,
        [Display(Name = "Stadium")]
        Stadium = 15,
        [Display(Name = "Swap Deck")]
        SwapDeck = 16,
        [Display(Name = "Market Stalls")]
        MarketStalls = 17,
        [Display(Name = "Library")]
        Library = 18,
        [Display(Name = "Attention Hall")]
        AttentionHall = 19,
        [Display(Name = "Seeker's Hut")]
        Seeker = 20,
        [Display(Name = "DeoDeck Club")]
        Club = 21,
        [Display(Name = "Traveling Merchants")]
        Merchants = 22,
        [Display(Name = "Timed Showcase")]
        Showcase = 23,
        [Display(Name = "Rewards Store")]
        RewardsStore = 24,
        [Display(Name = "Notice Board")]
        NoticeBoard = 25,
        [Display(Name = "Active Effects")]
        Effects = 26,
        [Display(Name = "Crafting")]
        Crafting = 27,
        [Display(Name = "General Store")]
        Store = 28,
        [Display(Name = "Warehouse")]
        Warehouse = 29,
        [Display(Name = "Companions")]
        Companions = 30,
        [Display(Name = "Deep Caverns")]
        DeepCaverns = 31,
        [Display(Name = "Right-Side Up World")]
        RightSideUpWorld = 32,
        [Display(Name = "Donation Zone")]
        DonationZone = 33,
        [Display(Name = "Secret Gifter")]
        SecretGifter = 34,
        [Display(Name = "Survey")]
        Survey = 35,
        [Display(Name = "Museum")]
        Museum = 36,
        [Display(Name = "Secret Shrine")]
        SecretShrine = 37,
        [Display(Name = "Partnership")]
        Partnership = 38,
        [Display(Name = "Buyers Club")]
        BuyersClub = 39,
        [Display(Name = "Matching Colors")]
        MatchingColors = 40,
        [Display(Name = "Color Wars")]
        ColorWars = 41,
        [Display(Name = "Spin of Success")]
        SpinSuccess = 42,
        [Display(Name = "Match Tags")]
        MatchTags = 43,
        [Display(Name = "Checklist")]
        Checklist = 44,
        [Display(Name = "Take the Offer")]
        TakeOffer = 45,
        [Display(Name = "Fair")]
        Fair = 46,
        [Display(Name = "Crystal Ball")]
        CrystalBall = 47,
        [Display(Name = "Training Center")]
        TrainingCenter = 48,
        [Display(Name = "Prize Cup")]
        PrizeCup = 49,
        [Display(Name = "Treasure Dive")]
        TreasureDive = 50,
        [Display(Name = "Daily Bonus")]
        DailyBonus = 51,
        [Display(Name = "Stadium Lobby")]
        StadiumLobby = 52,
        [Display(Name = "The Tower")]
        Tower = 53,
        [Display(Name = "Sponsor Society")]
        SponsorSociety = 54,
        [Display(Name = "Weary Villager")]
        WearyVillager = 55,
        [Display(Name = "Store Magician")]
        StoreMagician = 56,
        [Display(Name = "Wealthy Donors")]
        WealthyDonors = 57,
        [Display(Name = "Challenger Dome")]
        ChallengerDome = 58,
        [Display(Name = "Phantom Door")]
        PhantomDoor = 59,
        [Display(Name = "Tourney")]
        Tourney = 60,
        [Display(Name = "Top Counter")]
        TopCounter = 61,
        [Display(Name = "Beyond World")]
        BeyondWorld = 62,
        [Display(Name = "Top Adoptions")]
        TopAdoptions = 63,
        [Display(Name = "Lucky Guess")]
        LuckyGuess = 64,
        [Display(Name = "Art Factory")]
        ArtFactory = 65,
        [Display(Name = "Clubhouse")]
        Clubhouse = 66
    }
}