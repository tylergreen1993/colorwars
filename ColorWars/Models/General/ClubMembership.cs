﻿using System;

namespace ColorWars.Models
{
    public class ClubMembership
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public bool ClaimedItem { get; set; }
        public int Month { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}