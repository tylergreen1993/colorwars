﻿using System;

namespace ColorWars.Models
{
    public class BannedIPAddress
    {
        public string IPAddress { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}