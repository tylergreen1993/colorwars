﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Location
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Url { get; set; }
        public LocationArea LocationArea { get; set; }
        public LocationId LocationId { get; set; }
        public LocationType LocationType { get; set; }
        public double WidthToHeightRatio { get; set; }
        public int Position { get; set; }
        public LocationCategory LocationCategory { get; set; }
        public bool IsFavorited { get; set; }
        public bool IsFaded { get; set; }
        public List<string> Tags { get; set; } = new List<string>();
        public DateTime CreatedDate { get; set; }
    }

    public enum LocationId
    {
        None = 0,
        Bank = 1,
        AuctionHouse = 2,
        GeneralStore = 3,
        Junkyard = 4,
        RepairShop = 5,
        Crafting = 6,
        StockMarket = 7,
        Storage = 8,
        News = 9,
        TradingSquare = 10,
        DonationZone = 11,
        Lotto = 12,
        SeekersHut = 13,
        WishingStone = 14,
        YoungWizard = 15,
        Groups = 16,
        Rankings = 17,
        TimedShowcase = 18,
        JobCenter = 19,
        BuyersClub = 20,
        Referrals = 21,
        ActiveEffects = 22,
        PromoCodes = 23,
        Fair = 24,
        TrainingCenter = 25,
        MarketStalls = 26,
        Disposer = 27,
        Library = 28,
        AttentionHall = 29,
        Club = 30,
        Merchants = 31,
        RewardsStore = 32,
        NoticeBoard = 33,
        Warehouse = 34,
        Caverns = 35,
        RightSideUpWorld = 36,
        SecretGifter = 37,
        Survey = 38,
        Museum = 39,
        SecretShrine = 40,
        HallOfHelpers = 41,
        UnusableStore = 42,
        Tower = 43,
        SponsorSociety = 44,
        Villager = 45,
        StoreMagician = 46,
        PhantomDoor = 47,
        Tourney  = 48,
        BeyondWorld = 49,
        TopAdoptions = 50,
        ArtFactory = 51,
        CrystalBall = 52,
        DeoJack = 53,
        DoubtIt = 54,
        LuckyGuess = 55,
        SpinSuccess = 56,
        StreakCards = 57,
        TakeOffer = 58,
        TreasureDive = 59,
        ChallengerDome = 60,
        ColorWars = 61,
        HighLow = 62,
        MatchingColors = 63,
        PrizeCup = 64,
        SwapDeck = 65,
        TopCounter = 66,
        WealthyDonors = 67,
        Clubhouse = 68
    }

    public enum LocationArea
    {
        None = 0,
        Plaza = 1,
        Profile = 2,
        Fair = 3
    }

    public enum LocationCategory
    {
        [Display(Name = "More")]
        More = 0,
        [Display(Name = "Limited")]
        Limited = 1,
        [Display(Name = "Secret")]
        Secret = 2,
        [Display(Name = "Special")]
        Special = 3,
        [Display(Name = "No Limit")]
        NoLimit = 4,
        [Display(Name = "Popular")]
        Popular = 5
    }

    public enum LocationType
    {
        [Display(Name = "Favorites")]
        Favorites = 0,
        [Display(Name = "Games & Freebies")]
        Games = 1,
        [Display(Name = "Shops & Trading")]
        Shops = 2,
        [Display(Name = "Upgrades & Management")]
        Improvements = 3,
        [Display(Name = "Social & Knowledge")]
        Knowledge = 4
    }
}
