﻿using System;

namespace ColorWars.Models
{
    public class SeekerChallenge
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public ItemTheme ItemTheme { get; set; }
        public Guid RewardId { get; set; }
        public ItemTheme RewardTheme { get; set; }
    }
}
