﻿using System;

namespace ColorWars.Models
{
    public class Message
    {
        public Guid Id { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Content { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public bool IsRead { get; set; }
        public bool IsRecentlyActive => (DateTime.UtcNow - LastActive).TotalMinutes <= 30;
        public DateTime CreatedDate { get; set; }
        public DateTime LastActive { get; set; }
    }
}
