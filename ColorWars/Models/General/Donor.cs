﻿using System;

namespace ColorWars.Models
{
    public class Donor
    {
        public string Username { get; set; }
        public int Amount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
