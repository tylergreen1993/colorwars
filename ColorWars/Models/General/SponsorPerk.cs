﻿namespace ColorWars.Models
{
    public class SponsorPerk
    {
        public SponsorPerkId Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public int Coins { get; set; }
    }

    public enum SponsorPerkId
    {
        None = 0,
        ElegantTheme = 1,
        StoreMagician = 2,
        CompanionUpgrade = 3,
        SpecialArtCard = 4,
        CoinGift = 5,
        MaximumStorageSize = 6,
        UnicornCompanion = 7,
        RemoveAds = 8,
        SponsorCardPack  = 9
    }
}