﻿using System;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class Notification
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public NotificationLocation Location { get; set; }
        public string Href { get; set; }
        public DateTime SeenDate { get; set; }
        public DateTime CreatedDate { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get => string.IsNullOrEmpty(_imageUrl) ? GetImageUrl(Location) : Helper.GetImageUrl(_imageUrl);
            set => _imageUrl = value;
        }

        private string GetImageUrl(NotificationLocation location)
        {
            switch (location)
            {
                case NotificationLocation.Auction:
                    return Helper.GetImageUrl("Locations/Auction.png");
                case NotificationLocation.Trade:
                    return Helper.GetImageUrl("Locations/Trading.png");
                case NotificationLocation.News:
                    return Helper.GetImageUrl("Locations/News.png");
                case NotificationLocation.Jobs:
                    return Helper.GetImageUrl("Locations/JobCenter.png");
                case NotificationLocation.Stocks:
                    return Helper.GetImageUrl("Locations/Stocks.png");
                case NotificationLocation.Groups:
                    return Helper.GetImageUrl("Locations/Groups.png");
                case NotificationLocation.Accomplishment:
                    return Helper.GetImageUrl("Accomplishments/Gray-Trophy.png");
                case NotificationLocation.Stadium:
                    return Helper.GetImageUrl("Locations/Stadium.png");
                case NotificationLocation.Referral:
                    return Helper.GetImageUrl("Locations/Referral.png");
                case NotificationLocation.MarketStalls:
                    return Helper.GetImageUrl("Locations/MarketStalls.png");
                case NotificationLocation.AttentionHall:
                    return Helper.GetImageUrl("Locations/AttentionHall.png");
                case NotificationLocation.Merchants:
                    return Helper.GetImageUrl("Locations/Merchants.png");
                case NotificationLocation.RewardsStore:
                    return Helper.GetImageUrl("Locations/RewardsStore.png");
                case NotificationLocation.NoticeBoard:
                    return Helper.GetImageUrl("Locations/NoticeBoard.png");
                case NotificationLocation.Warehouse:
                    return Helper.GetImageUrl("Locations/Warehouse.png");
                case NotificationLocation.SecretGifter:
                    return Helper.GetImageUrl("Locations/SecretGifter.png");
                case NotificationLocation.Survey:
                    return Helper.GetImageUrl("Locations/Survey.png");
                case NotificationLocation.Museum:
                    return Helper.GetImageUrl("Locations/Museum.png");
                case NotificationLocation.Partnership:
                    return Helper.GetImageUrl("Locations/Partnership.png");
                case NotificationLocation.ColorWars:
                    return Helper.GetImageUrl("Locations/ColorWars.png");
                case NotificationLocation.SponsorSociety:
                    return Helper.GetImageUrl("Locations/SponsorSociety.png");
                case NotificationLocation.DonationZone:
                    return Helper.GetImageUrl("Locations/Donate.png");
                case NotificationLocation.Tourney:
                    return Helper.GetImageUrl("Locations/Tourney.png");
                case NotificationLocation.StadiumChallenge:
                    return Helper.GetImageUrl("CPU/RandomChallenger.png");
                default:
                    return Helper.GetImageUrl("Global/Logo.png");
            }
        }
    }

    public enum NotificationLocation
    {
        General = 0,
        Auction = 1,
        Trade = 2,
        News = 3,
        Jobs = 4,
        Stocks = 5,
        Groups = 6,
        Accomplishment = 7,
        Stadium = 8,
        Referral = 9,
        MarketStalls = 10,
        AttentionHall = 11,
        Merchants = 12,
        RewardsStore = 13,
        NoticeBoard = 14,
        Warehouse = 15,
        SecretGifter = 16,
        Survey = 17,
        Museum = 18,
        Partnership = 19,
        ColorWars = 20,
        SponsorSociety = 21,
        DonationZone = 22,
        Tourney = 23,
        StadiumChallenge = 24
    }
}
