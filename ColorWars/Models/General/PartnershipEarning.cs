﻿using System;

namespace ColorWars.Models
{
    public class PartnershipEarning
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string PartnershipUsername { get; set; }
        public int Amount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}