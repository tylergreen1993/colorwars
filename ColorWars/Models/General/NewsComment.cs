﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class NewsComment
    {
        public Guid Id { get; set; }
        public Guid ParentCommentId { get; set; }
        public int NewsId { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public string LikedUsernames { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public int Level { get; set; }
        public int TotalLikes { get; set; }
        public bool UserLiked { get; set; }
        public DateTime CommentDate { get; set; }

        public string GetLikedUsernamesString()
        {
            if (string.IsNullOrEmpty(LikedUsernames))
            {
                return string.Empty;
            }

            List<string> likedUsernamesList = LikedUsernames.Split(',').ToList();
            if (likedUsernamesList.Count > 5)
            {
                return $"Liked by {string.Join(",", likedUsernamesList.Take(5))}, and {Helper.FormatNumber(likedUsernamesList.Count - 5)} more.";
            }

            return $"Liked by {string.Join(",", likedUsernamesList)}";
        }
    }
}
