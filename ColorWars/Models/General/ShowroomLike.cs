﻿using System;

namespace ColorWars.Models
{
    public class ShowroomLike
    {
        public string Username { get; set; }
        public string ShowroomUsername { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}