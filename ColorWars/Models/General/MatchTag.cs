﻿using System;

namespace ColorWars.Models
{
    public class MatchTag
    {
        public string Username { get; set; }
        public string TagUsername { get; set; }
        public string Color { get; set; }
        public int Wins { get; set; }
        public DateTime FirstWinDate { get; set; }
    }
}
