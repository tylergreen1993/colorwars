﻿namespace ColorWars.Models
{
    public class DisposerRecord
    {
        public int Rank { get; set; }
        public string Username { get; set; }
        public int Amount { get; set; }
    }
}
