﻿using System;
namespace ColorWars.Models
{
    public class Wish
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Content { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public bool IsGranted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
