﻿using System;

namespace ColorWars.Models
{
    public class HallOfFameEntry
    {
        public string Username { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
