﻿using System;
namespace ColorWars.Models
{
    public class WizardHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public WizardType Type { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum WizardType
    {
        Negative = -1,
        None = 0,
        Positive = 1
    }
}
