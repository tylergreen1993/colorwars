﻿namespace ColorWars.Models
{
    public class WealthyDonor
    {
        public int Rank { get; set; }
        public string Username { get; set; }
        public int Amount { get; set; }
    }
}
