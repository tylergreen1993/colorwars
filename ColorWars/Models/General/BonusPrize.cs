﻿namespace ColorWars.Models
{
    public class BonusPrize
    {
        public int Day { get; set; }
        public int Reward { get; set; }
        public bool IsActive { get; set; }
        public bool IsRedeemed { get; set; }
        public bool IsExpired { get; set; }
        public string ImageUrl { get; set; }
        public string Description => $"Earned on day {Day}.";
    }
}
