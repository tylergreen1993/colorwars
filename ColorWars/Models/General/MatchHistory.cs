﻿using System;

namespace ColorWars.Models
{
    public class MatchHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Opponent { get; set; }
        public string OpponentColor { get; set; }
        public int Points { get; set; }
        public int Level { get; set; }
        public int Intensity { get; set; }
        public Guid TourneyMatchId { get; set; }
        public MatchHistoryResult Result { get; set; }
        public CPUType CPUType { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum CPUType
    {
        None = 0,
        Normal = 1,
        Challenge = 2,
        Special = 3
    }

    public enum MatchHistoryResult
    {
        Lost = -1,
        Draw = 0,
        Won = 1
    }
}
