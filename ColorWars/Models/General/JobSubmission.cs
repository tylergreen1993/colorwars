﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class JobSubmission
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public JobPosition Position { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int AttentionHallPostId { get; set; }
        public AttentionHallPost AttentionHallPost { get; set; }
        public JobState State { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public enum JobPosition
    {
        [Display(Name = "All")]
        None = 0,
        [Display(Name = "Attention Hog")]
        Attention = 1,
        [Display(Name = "Bug Catcher")]
        Bugs = 2,
        [Display(Name = "DeoDeck Visionary")]
        Visionary = 3,
        [Display(Name = "Design Scholar")]
        Design = 4,
        [Display(Name = "News Writer")]
        News = 5,
        [Display(Name = "Helper")]
        Helper = 6
    }

    public enum JobState
    {
        [Display(Name = "Not for Now")]
        Declined = -1,
        [Display(Name = "Open")]
        Open = 0,
        [Display(Name = "Accepted")]
        Accepted = 1
    }
}
