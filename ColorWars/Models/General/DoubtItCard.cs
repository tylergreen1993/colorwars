﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class DoubtItCard
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public int Amount { get; set; }
        public string ImageUrl => $"/Cards/{Amount}.png";
        public DoubtItOwner Owner { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public enum DoubtItOwner
    {
        [Display(Name = "Pile")]
        Pile = 0,
        [Display(Name = "You")]
        You = 1,
        [Display(Name = "Sly Steve")]
        Opponent1 = 2,
        [Display(Name = "Sappy Sandy")]
        Opponent2 = 3,
        [Display(Name = "Silly Sam")]
        Opponent3 = 4
    }
}