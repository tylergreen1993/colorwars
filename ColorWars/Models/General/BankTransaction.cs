﻿using System;

namespace ColorWars.Models
{
    public class BankTransaction
    {
        public string Username { get; set; }
        public BankTransactionType Type { get; set; }
        public int Amount { get; set; }
        public DateTime TransactionDate { get; set; }
    }

    public enum BankTransactionType
    {
        Withdraw = 0,
        Deposit = 1,
        Interest = 2
    }
}
