﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ItemSection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AmountOwned { get; set; }
        public int RequiredAmount { get; set; }
        public List<Item> Items { get; set; }
        public bool IsRedeemed { get; set; }
        public bool IsCompleted => AmountOwned >= RequiredAmount;
    }
}
