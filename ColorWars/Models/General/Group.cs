﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string Whiteboard { get; set; }
        public string DisplayPicUrl { get; set; }
        public GroupPower Power { get; set; }
        public int MembersCount { get; set; }
        public int UnreadMessages { get; set; }
        public List<GroupMember> Members { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsUserMember { get; set; }
        public bool IsUserBanned { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsPrizeMember { get; set; }
        public GroupRole Role { get; set; }
        public DateTime LastPrizeDate { get; set; }
        public DateTime LastPowerChangeDate { get; set; }
        public DateTime LastReadMessageDate { get; set; }
        public DateTime LastGroupMessageDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

public enum GroupPower
{
    [Display(Name = "Unset")]
    Unset = -1,
    [Display(Name = "None")]
    None = 0,
    [Display(Name = "5% Stock Market Discount")]
    StockDiscount = 1,
    [Display(Name = "30% Timed Showcase Discount")]
    BiggerTimedShowcaseDiscount = 2,
    [Display(Name = "Additional Referrals Bonus")]
    AdditionalReferralsBonus = 3,
    [Display(Name = "Better Seeker's Hut Rewards")]
    BetterSeekerOffers = 4,
    [Display(Name = "Better Streak Cards Luck")]
    BetterStreakCardsLuck = 5,
    [Display(Name = "Better Wishing Stone Luck")]
    BetterWishingStoneLuck = 6
}
