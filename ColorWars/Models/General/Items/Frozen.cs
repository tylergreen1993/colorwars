﻿namespace ColorWars.Models
{
    public class Frozen : Item
    {
        public FrozenType Type { get; set; }
    }

    public enum FrozenType
    {
        None = 0,
        Light = 1,
        Normal = 2,
        Extra = 3
    }
}