﻿using System;
using Newtonsoft.Json;

namespace ColorWars.Models
{
    public class StoreItem : Item
    {
        [JsonProperty("Cost")]
        private int _cost { get; set; }
        [JsonIgnore]
        public int Cost
        {
            get => _cost;
            set
            {
                _cost = Math.Max(1, value);
            }
        }

        [JsonProperty("MinCost")]
        private int _minCost { get; set; }
        [JsonIgnore]
        public int MinCost
        {
            get => _minCost;
            set
            {
                _minCost = Math.Max(1, value);
            }
        }
    }
}
