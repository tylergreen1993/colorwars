﻿namespace ColorWars.Models
{
    public class Enhancer : Item
    {
        public bool IsMultiplied { get; set; }
        public bool IsPercent { get; set; }
        public bool IsFiller { get; set; }
        public EnhancerSpecialType SpecialType { get; set; }
        public int Amount { get; set; }
    }

    public enum EnhancerSpecialType
    {
        None = 0,
        Blocker = 1,
        Predictor = 2,
        Dark = 3,
        Luck = 4
    }
}