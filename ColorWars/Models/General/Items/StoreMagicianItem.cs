﻿namespace ColorWars.Models
{
    public class StoreMagicianItem : Item
    {
        public int Cost { get; set; }
        public string Location { get; set; }
        public string Url { get; set; }

        public void Copy(Item item)
        {
            Id = item.Id;
            SelectionId = item.SelectionId;
            Name = item.Name;
            Description = item.Description;
            ImageUrl = item.ImageUrl;
            Points = item.Points;
            Category = item.Category;
            Uses = item.Uses;
            TotalUses = item.TotalUses;
            IsExclusive = item.IsExclusive;
            CreatedDate = item.CreatedDate;
            RepairedDate = item.RepairedDate;
        }
    }
}
