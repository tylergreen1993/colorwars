﻿namespace ColorWars.Models
{
    public class Egg : Item
    {
        public EggType Type { get; set; }
    }

    public enum EggType
    {
        Normal = 0,
        Enhanced = 1,
        Club = 2
    }
}
