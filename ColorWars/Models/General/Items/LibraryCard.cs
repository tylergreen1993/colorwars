﻿namespace ColorWars.Models
{
    public class LibraryCard : Item
    {
        public CardColor Color { get; set; }
    }
}
