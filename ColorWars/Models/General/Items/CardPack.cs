﻿namespace ColorWars.Models
{
    public class CardPack : Item
    {
        public CardPackType Type { get; set; }
    }

    public enum CardPackType
    {
        Normal = 0,
        Enhanced = 1,
        Rare = 2,
        Club = 3
    }
}
