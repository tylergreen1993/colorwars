﻿namespace ColorWars.Models
{
    public class Sleeve : Item
    {
        public SleeveType Type { get; set; }
    }

    public enum SleeveType
    {
        Normal = 0,
        Club = 1,
        Enhanced = 2
    }
}
