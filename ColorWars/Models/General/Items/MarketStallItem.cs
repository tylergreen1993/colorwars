﻿namespace ColorWars.Models
{
    public class MarketStallItem : Item
    {
        public int Cost { get; set; }
        public int MarketStallId { get; set; }
    }
}
