﻿namespace ColorWars.Models
{
    public class Theme : Item
    {
        public ThemeType Type { get; set; }
    }

    public enum ThemeType
    {
        Card = 0,
        Deck = 1
    }
}