﻿namespace ColorWars.Models
{
    public class Seed : Item
    {
        public CardColor Color { get; set; }
    }
}
