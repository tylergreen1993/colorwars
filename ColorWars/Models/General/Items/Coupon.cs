﻿namespace ColorWars.Models
{
    public class Coupon : Item
    {
        public CouponType Type { get; set; }
        public int Amount { get; set; }
    }

    public enum CouponType
    {
        Junkyard = 0,
        Repair = 1,
        Store = 2
    }
}
