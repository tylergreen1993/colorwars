﻿namespace ColorWars.Models
{
    public class Jewel : Item
    {
        public JewelColor Color { get; set; }
    }

    public enum JewelColor
    {
        Red = 0,
        Orange = 1,
        Yellow = 2,
        Green = 3,
        Blue = 4,
        Purple = 5,
        Rainbow = 6,
        Eternal = 7
    }
}