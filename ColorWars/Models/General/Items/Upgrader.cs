﻿namespace ColorWars.Models
{
    public class Upgrader : Item
    {
        public UpgraderType Type { get; set; }
        public int Amount { get; set; }
    }

    public enum UpgraderType
    {
        Bag = 0,
        Market = 1,
        Storage = 2,
        Red = 3,
        Orange = 4,
        Yellow = 5,
        Green = 6,
        Blue = 7,
        Purple = 8,
        Negative = 9
    }
}
