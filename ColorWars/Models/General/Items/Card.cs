﻿namespace ColorWars.Models
{
    public class Card : Item
    {
        public int Amount { get; set; }
        public CardColor Color { get; set; }
    }

    public enum CardColor
    {
        None = -1,
        Red = 0,
        Orange = 1,
        Yellow = 2,
        Green = 3,
        Blue = 4,
        Purple = 5,
        Pink = 6,
        Club = 7,
        Gold = 8
    }
}
