﻿namespace ColorWars.Models
{
    public class Candy : Item
    {
        public int Amount { get; set; }
        public int TotalTime { get; set; }
        public CandyType Type { get; set; }
    }

    public enum CandyType
    {
        Chocolate = 0,
        Strawberry = 1,
        Sour = 2,
        Club = 4,
        Wizard = 5,
        Light = 6,
        Lavender = 7,
        Wall = 8,
        Quarry = 9,
        SwapDeck = 10,
        Bubble = 11,
        Companion = 12,
        Crafting = 13,
        Spin = 14,
        CompanionDye = 15,
        CinnamonTreasure = 16
    }
}
