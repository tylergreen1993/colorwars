﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ColorWars.Classes;
using Newtonsoft.Json;

namespace ColorWars.Models
{
    [Serializable]
    public class Item
    {
        public Guid Id { get; set; }
        public Guid SelectionId { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public ItemCategory Category { get; set; }
        public int OriginalPoints { get; set; }
        public DeckType DeckType { get; set; }
        public int Uses { get; set; }
        public int TotalUses { get; set; }
        public int Position { get; set; }
        public ItemTheme Theme { get; set; }
        public bool IsExclusive { get; set; }
        public RelicId Tier { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime RepairedDate { get; set; }
        public DateTime AddedDate { get; set; }

        [JsonProperty("Name")]
        private string _name { get; set; }
        [JsonIgnore]
        public string Name
        {
            get
            {
                if (Theme == ItemTheme.Default || Category == ItemCategory.Theme)
                {
                    return _name;
                }

                return $"{_name} - {Theme} Theme";
            }
            set
            {
                _name = value;
            }
        }

        [JsonProperty("ImageUrl")]
        private string _imageUrl { get; set; }
        [JsonIgnore]
        public string ImageUrl
        {
            get
            {
                if (Theme == ItemTheme.Default || Category == ItemCategory.Theme)
                {
                    return _imageUrl;
                }

                if (_imageUrl.Contains("/"))
                {
                    return _imageUrl.Insert(_imageUrl.LastIndexOf("/", StringComparison.Ordinal) + 1, $"{Theme}/");
                }

                return _imageUrl;
            }
            set
            {
                _imageUrl = value;
            }
        }

        [JsonProperty("Points")]
        private int _points { get; set; }
        [JsonIgnore]
        public int Points
        {
            get => _points;
            set
            {
                _points = Math.Max(1, ModifyValueBasedOnUse(value));
                OriginalPoints = value;
            }
        }

        public ItemCondition Condition
        {
            get
            {
                if (RepairedDate == DateTime.MinValue)
                {
                    return ItemCondition.New;
                }

                int days = GetRepairedDateAgeInDays();
                return Helper.GetItemCondition(days);
            }
        }

        public int GetRepairedDateAgeInDays()
        {
            return (int)(DateTime.UtcNow - RepairedDate).TotalDays;
        }

        public int GetAgeInDays()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalDays;
        }

        public int GetAgeInHours()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalHours;
        }

        public int GetAgeInMinutes()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalMinutes;
        }

        public int GetAgeInSeconds()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalSeconds;
        }

        public int GetSecondsSinceAddedToBag()
        {
            return (int)(DateTime.UtcNow - AddedDate).TotalSeconds;
        }

        public void UpdatePoints(int amount)
        {
            _points = amount;
        }

        public Item DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                return (Item)formatter.Deserialize(stream);
            }
        }

        protected int ModifyValueBasedOnUse(int value)
        {
            if (SelectionId == Guid.Empty)
            {
                return value;
            }

            double usesRatio = (double)Uses / TotalUses;
            value = (int)(value * usesRatio);

            if (Condition == ItemCondition.New)
            {
                return value;
            }
            if (Condition == ItemCondition.Good)
            {
                return (int)(Math.Round((double)value * 9 / 10));
            }
            if (Condition == ItemCondition.Poor)
            {
                return (int)(Math.Round((double)value * 2 / 3));
            }
            return (int)(Math.Round((double)value / 15));
        }
    }

    public enum ItemCondition
    {
        All = -1,
        New = 0,
        Good = 1,
        Poor = 2,
        Unusable = 4
    }

    public enum ItemCategory
    {
        [Display(Name = "None")]
        None = -2,
        [Display(Name = "All")]
        All = -1,
        [Display(Name = "Deck Card")]
        Deck = 0,
        [Display(Name = "Enhancer")]
        Enhancer = 1,
        [Display(Name = "Card Pack")]
        Pack = 2,
        [Display(Name = "Crafting Card")]
        Crafting = 3,
        [Display(Name = "Candy Card")]
        Candy = 4,
        [Display(Name = "Coupon Card")]
        Coupon = 5,
        [Display(Name = "Egg Card")]
        Egg = 6,
        [Display(Name = "Library Card")]
        Library = 7,
        [Display(Name = "Theme Card")]
        Theme = 8,
        [Display(Name = "Sleeve")]
        Sleeve = 9,
        [Display(Name = "Upgrader")]
        Upgrader = 10,
        [Display(Name = "Jewel Card")]
        Jewel = 11,
        [Display(Name = "Stealth Card")]
        Stealth = 12,
        [Display(Name = "Frozen Item")]
        Frozen = 13,
        [Display(Name = "Companion")]
        Companion = 14,
        [Display(Name = "Seed Card")]
        Seed = 15,
        [Display(Name = "Art Card")]
        Art = 16,
        [Display(Name = "Fairy Card")]
        Fairy = 17,
        [Display(Name = "Beyond Card")]
        Beyond = 18,
        [Display(Name = "Key Card")]
        Key = 19,
        [Display(Name = "Custom Card")]
        Custom = 20
    }

    public enum ItemTheme
    {
        Default = 0,
        Outline = 1,
        Striped = 2,
        Retro = 3,
        Squared = 4,
        Astral = 5,
        Club = 6,
        Elegant = 7,
        Neon = 8
    }

    public enum ItemSort
    {
        [Display(Name = "Category")]
        Category = 0,
        [Display(Name = "Recently Added")]
        Newest = 1,
        [Display(Name = "Highest Value")]
        HighestValue = 2,
        [Display(Name = "Lowest Value")]
        LowestValue = 3,
        [Display(Name = "Name")]
        Name = 4,
        [Display(Name = "Manual")]
        Manual = 5
    }

    public enum DeckType
    {
        None = 0,
        Primary = 1,
        Secondary = 2
    }
}