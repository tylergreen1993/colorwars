﻿namespace ColorWars.Models
{
    public class Crafting : Item
    {
        public CardColor Color { get; set; }
        public bool IsEnhanced { get; set; }
        public CraftingType Type { get; set; }
    }

    public enum CraftingType
    {
        Normal = 0,
        Partial = 1
    }
}
