﻿namespace ColorWars.Models
{
    public class Stealth : Item
    {
        public StealthType Type { get; set; }
        public StealthQuality Quality { get; set; }
    }

    public enum StealthType
    {
        None = 0,
        Protect = 1,
        Grab = 2
    }

    public enum StealthQuality
    {
        Normal = 0,
        Diamond = 1
    }
}