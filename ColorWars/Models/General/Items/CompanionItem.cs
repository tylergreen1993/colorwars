﻿namespace ColorWars.Models
{
    public class CompanionItem : Item
    {
        public CompanionType Type { get; set; }
    }

    public enum CompanionType
    {
        None = -1,
        Dog = 0,
        Cat = 1,
        Fish = 2,
        Dragon = 3,
        Panda = 4,
        Monkey = 5,
        Penguin = 6,
        Unicorn = 7
    }
}
