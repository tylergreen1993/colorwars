﻿namespace ColorWars.Models
{
    public class SpiritSister
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ChallengeUrl { get; set; }
        public bool CanChallenge { get; set; }
        public bool HasDefeated { get; set; }
    }
}