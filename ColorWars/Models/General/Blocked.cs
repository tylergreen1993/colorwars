﻿using System;

namespace ColorWars.Models
{
    public class Blocked
    {
        public string Username { get; set; }
        public string BlockedUser { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
