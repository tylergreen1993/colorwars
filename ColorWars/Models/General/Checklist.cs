﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Checklist
    {
        public AccomplishmentId AccomplishmentId { get; set; }
        public string Href { get; set; }
        public string Description { get; set; }
        public ChecklistRank Rank { get; set; }
        public bool Completed { get; set; }
        public DateTime EarnedDate { get; set; }
    }

    public enum ChecklistRank
    {
        [Display(Name = "Beginner")]
        Beginner = 0,
        [Display(Name = "Novice")]
        Novice = 1,
        [Display(Name = "Apprentice")]
        Apprentice = 2,
        [Display(Name = "Intermediate")]
        Intermediate = 3,
        [Display(Name = "Advanced")]
        Advanced = 4,
        [Display(Name = "Expert")]
        Expert = 5,
        [Display(Name = "Elite")]
        Elite = 6,
    }
}
