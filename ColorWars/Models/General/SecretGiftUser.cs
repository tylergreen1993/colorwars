﻿namespace ColorWars.Models
{
    public class SecretGiftUser
    {
        public string Username { get; set; }
        public int SecretGifterRank { get; set; }
        public int Score { get; set; }
    }
}
