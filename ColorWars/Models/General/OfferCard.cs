﻿using System;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class OfferCard
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public int Position { get; set; }
        public int Points { get; set; }
        public string Description => IsHidden ? "Flip it over and see what's underneath!" : $"The card's worth {Helper.GetFormattedPoints(Points)}!";
        public string ImageUrl => "/Cards/CardWon.png";
        public bool IsHidden => FlipDate == DateTime.MinValue;
        public DateTime FlipDate { get; set; }
    }
}