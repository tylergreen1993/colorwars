﻿using System;

namespace ColorWars.Models
{
    public class PowerMove
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool CanBeNegative { get; set; }
        public bool IsNegative { get; set; }
        public bool IsActive { get; set; }
        public bool IsDisabled { get; set; }
        public PowerMoveType Type { get; set; }
        public int MinCPULevel { get; set; }
        public int Tokens { get; set; }
        public DateTime CreatedDate { get; set; }

        private string _imageUrl;
        public string ImageUrl {
            get => IsDisabled ? $"PowerMoves/Lock.png" : _imageUrl;
            set => _imageUrl = value;
        }
    }

    public enum PowerMoveType
    {
        BlockersBaneNegative = -17,
        BigShotNegative = -16,
        UnderdogRiseNegative = -15,
        LadyLuckNegative = -2,
        AllOutAttackNegative = -1,
        None = 0,
        AllOutAttack = 1,
        LadyLuck = 2,
        SilencedEnhancers = 3,
        DoubledEnhancers = 4,
        ColorBoost = 5,
        PowerStop = 6,
        FlowingPower = 7,
        SelfDestruct = 8,
        PlannedStrike = 9,
        EnhancerSwap = 10,
        PoisonTouch = 11,
        MirrorMirror = 12,
        ColorReduce = 13,
        LoseControl = 14,
        WinningLoser = 15,
        BigShot = 16,
        BlockersBane = 17,
        DeckSwap = 18,
        Confusion = 19,
        TopsyTurvy = 20,
        PsychUp = 21,
        LuckUp = 22,
        Revival = 23,
        CounterStop = 24,
        DoubleDip = 25
    }
}
