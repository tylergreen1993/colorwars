﻿using System;

namespace ColorWars.Models
{
    public class SorceressEffect
    {
        public SorceressEffectId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public string ImageUrl { get; set; }
    }

    public enum SorceressEffectId
    {
        None = 0,
        PositiveHoroscope = 1,
        CompanionHappiness = 2,
        CurseUser = 3,
        ProtectUser = 4,
        RenameCompanion = 5,
        CrystalBall = 6,
        ReduceFurnace = 7,
        LearnTranslation = 8
    }
}