﻿using System;

namespace ColorWars.Models
{
    public class LibraryHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public LibraryKnowledgeType Type { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum LibraryKnowledgeType
    {
        None = 0,
        Power = 1,
        Opportunity = 2,
        Wealth = 3
    }
}
