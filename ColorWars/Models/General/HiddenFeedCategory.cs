﻿using System;

namespace ColorWars.Models
{
    public class HiddenFeedCategory
    {
        public string Username { get; set; }
        public FeedCategory FeedId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
