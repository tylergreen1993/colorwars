﻿using System;

namespace ColorWars.Models
{
    public class GroupMessage
    {
        public Guid Id { get; set; }
        public int GroupId { get; set; }
        public string Sender { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
