﻿using System;
namespace ColorWars.Models
{
    public class PromoCodeHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}