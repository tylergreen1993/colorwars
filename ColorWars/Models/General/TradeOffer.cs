﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TradeOffer
    {
        public Guid Id { get; set; }
        public int TradeId { get; set; }
        public string Username { get; set; }
        public Guid Item1Id { get; set; }
        public Guid Item2Id { get; set; }
        public Guid Item3Id { get; set; }
        public List<Item> Items { get; set; }
        public bool IsAccepted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
