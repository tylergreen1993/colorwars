﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class Auction
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string TopBidder { get; set; }
        public int StartPoints { get; set; }
        public Item AuctionItem { get; set; }
        public List<Bid> Bids { get; set; }
        public int MinBidIncrement { get; set; }
        public int BidCount { get; set; }
        public bool CreatorCollected { get; set; }
        public bool WinnerCollected { get; set; }
        public bool SentNotification { get; set; }
        public DateTime EndDate { get; set; }

        private int _highestBid;
        public int HighestBid
        {
            get => _highestBid > 0 ? _highestBid : Bids == null ? StartPoints : Bids.Count == 0 ? StartPoints : Bids.First().Points;
            set => _highestBid = value;
        }
        public string HighestBidder => Bids.Count == 0 ? null : Bids.First().Username;

        public bool IsFinished()
        {
            return EndDate <= DateTime.UtcNow;
        }
    }
}
