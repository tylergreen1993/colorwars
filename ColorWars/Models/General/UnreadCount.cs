﻿namespace ColorWars.Models
{
    public class UnreadCount
    {
        public int Messages { get; set; }
        public int Notifications { get; set; }
    }
}