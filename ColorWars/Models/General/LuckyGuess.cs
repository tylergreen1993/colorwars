﻿using System;

namespace ColorWars.Models
{
    public class LuckyGuess
    {
        public int Position { get; set; }
        public string Username { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
