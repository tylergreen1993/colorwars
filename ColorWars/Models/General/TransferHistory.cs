﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class TransferHistory
    {
        public Guid Id { get; set; }
        public int TransferId { get; set; }
        public TransferType Type { get; set; }
        public string Transferer { get; set; }
        public string Transferee { get; set; }
        public int DifferenceAmount { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum TransferType
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "Market Stall")]
        MarketStalls = 1,
        [Display(Name = "Trade")]
        Trading = 2,
        [Display(Name = "Auction House")]
        Auction = 3
    }
}