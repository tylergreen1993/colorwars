﻿namespace ColorWars.Models
{
    public class Statistics
    {
        public int UsersCount { get; set; }
        public int ItemsCount { get; set; }
        public int StadiumMatchesCount { get; set; }
        public int PointsAmount { get; set; }
    }
}
