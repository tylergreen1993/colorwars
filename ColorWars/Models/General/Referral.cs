﻿using System;

namespace ColorWars.Models
{
    public class Referral
    {
        public string Username { get; set; }
        public int InterestTimes { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
