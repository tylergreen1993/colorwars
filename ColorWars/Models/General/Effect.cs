﻿using System;

namespace ColorWars.Models
{
    public class Effect
    {
        public string Name { get; set; }
        public int Amount { get; set; }
        public int Uses { get; set; }
        public bool IsPercent { get; set; }
        public string Url { get; set; }
        public EffectType Type { get; set; }
        public DateTime ExpiryDate { get; set; }
    }

    public enum EffectType
    {
        Negative = -1,
        None = 0,
        Positive = 1
    }
}
