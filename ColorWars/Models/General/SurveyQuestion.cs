﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SurveyQuestion
    {
        public SurveyQuestionId Id { get; set; }
        public string Question { get; set; }
        public List<string> Options { get; set; }
    }

    public enum SurveyQuestionId
    {
        None = 0,
        Age = 1,
        VisitFrequency = 2,
        FirstHearAbout = 3,
        JoinReason = 4,
        FavoriteLocation = 5,
        MobileOperatingSystem = 6,
        LeastFavoriteLocation = 7,
        PayToUse = 8,
        FavoriteMoment = 9,
        SuggestedLocation = 10,
        SuggestedItem = 11,
        RateOverall = 12,
        RateFun = 13,
        NextFeature = 14,
        EntertainmentSpending = 15,
        MostAttachedToFeature = 16,
        YoungWizardEffect = 17,
        SomethingThatCanBeImproved = 18,
        RecommendedToOthers = 19,
        FaveGames = 20,
        FaveInterests = 21,
        GamesPlayedPerWeek = 22,
        BooksReadPerYear = 23,
        TvWatchedPerDay = 24,
        GamePurchases = 25,
        ComputerTime = 26,
        PhoneTime = 27,
        AnythingElse = 28,
        CurrentJob = 29,
        FaveGamingSystem = 30,
        NextGoal = 31,
        EverSubmittedJobCenter = 32,
        MoneySpentOnGamesYearly = 33,
        DeviceUsage = 34,
        ThoughtsOnAds = 35,
        DiscoverySource = 36
    }
}
