﻿namespace ColorWars.Models
{
    public class TowerRanking
    {
        public string Username { get; set; }
        public int Rank { get; set; }
        public double Percentile { get; set; }
        public int TopFloorLevel { get; set; }
    }
}