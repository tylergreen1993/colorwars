﻿using System;

namespace ColorWars.Models
{
    public class NoticeBoardTask
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public Guid ItemId { get; set; }
        public ItemCondition ItemCondition { get; set; }
        public bool IsExactCondition { get; set; }
        public bool HasTotalUses { get; set; }
        public int Reward { get; set; }
        public DateTime ItemCreatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}