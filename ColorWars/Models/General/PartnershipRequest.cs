﻿using System;

namespace ColorWars.Models
{
    public class PartnershipRequest
    {
        public string RequestUsername { get; set; }
        public string Username { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}