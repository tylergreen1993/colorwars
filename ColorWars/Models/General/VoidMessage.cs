﻿using System;

namespace ColorWars.Models
{
    public class VoidMessage
    {
        public string Username { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
