﻿namespace ColorWars.Models
{
    public class MatchRanking
    {
        public string Username { get; set; }
        public string DisplayPicUrl { get; set; }
        public string Color { get; set; }
        public int Rank { get; set; }
        public double Percentile { get; set; }
        public int Score { get; set; }
        public int TotalMatches { get; set; }
        public int WonMatches { get; set; }
    }
}
