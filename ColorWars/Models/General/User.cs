﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public Guid AccessToken { get; set; }
        public string FirstName { get; set; }
        public string EmailAddress { get; set; }
        public string Location { get; set; }
        public string Bio { get; set; }
        public Gender Gender { get; set; }
        public bool AllowNotifications { get; set; }
        public bool FlushSession { get; set; }
        public string Referral { get; set; }
        public string IPAddress { get; set; }
        public string Color { get; set; }
        public string DisplayPicUrl { get; set; }
        public int LoginAttempts { get; set; }
        public UserRole Role { get; set; }
        public InactiveAccountType InactiveType { get; set; }
        public DateTime LoginAttemptDate { get; set; }
        public DateTime ForgotEmailDate { get; set; }
        public DateTime SuspendedDate { get; set; }
        public DateTime PremiumExpiryDate { get; set; }
        public DateTime AdFreeExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Points { get; set; }
        public bool IsSuspended => DateTime.UtcNow < SuspendedDate;

        public int GetAgeInDays()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalDays;
        }
        public int GetAgeInHours()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalHours;
        }
        public int GetAgeInMinutes()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalMinutes;
        }
        public int GetAgeInSeconds()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalSeconds;
        }

        public string GetFriendlyAge()
        {
            int days = GetAgeInDays();
            if (days >= 365)
            {
                int years = days / 365;
                return $"{Helper.FormatNumber(years)} year{(years == 1 ? "" : "s")}";
            }
            if (days >= 30)
            {
                int months = days / 30;
                return $"{Helper.FormatNumber(months)} month{(months == 1 ? "" : "s")}";
            }
            return $"{days} day{(days == 1 ? "" : "s")}";
        }

        public string GetFriendlyName()
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                return Username;
            }

            return FirstName;
        }

        public string GetFormattedPoints()
        {
            return Helper.GetFormattedPoints(Points);
        }
    }

    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2,
        Other = 3
    }

    public enum UserRole
    {
        [Display(Name = "User")]
        Default = 0,
        [Display(Name = "Helper")]
        Helper = 1,
        [Display(Name = "Staff")]
        Admin = 2,
        [Display(Name = "Staff")]
        SuperAdmin = 3
    }
}
