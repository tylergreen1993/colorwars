﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SuspensionHistory
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string SuspendingUser { get; set; }
        public string Reason { get; set; }
        public DateTime SuspendedDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
