﻿using System;

namespace ColorWars.Models
{
    public class MatchWaitlist
    {
        public Guid Id { get; set; }
        public Guid TourneyMatchId { get; set; }
        public string Username { get; set; }
        public string Username2 { get; set; }
        public string ChallengeUser { get; set; }
        public int Points { get; set; }
        public bool IsElite { get; set; }
        public DateTime CreatedDate { get; set; }

        public bool IsExpired()
        {
            return (int)(DateTime.UtcNow - CreatedDate).TotalSeconds > 90;
        }
    }
}
