﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class MatchingCard
    {
        public CardColor Color { get; set; }
        public int Position { get; set; }
        public string Description => IsHidden ? "Flip it over and see what's underneath!" : $"It's a {Color} Card!";
        public string ImageUrl => IsHidden ? "/Cards/CardBack.png" : $"/Cards/{Color}.png";
        public bool IsHidden => Color == CardColor.None;
    }
}