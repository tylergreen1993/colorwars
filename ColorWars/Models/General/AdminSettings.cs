﻿using System;

namespace ColorWars.Models
{
    public class AdminSettings
    {
        public bool IsBetaEnabled { get; set; } = false;
        public bool EnableAds { get; set; } = true;
        public string BetaCode { get; set; } = string.Empty;
        public double TotalSponsorAmount { get; set; } = 30;
        public int WealthyDonorsPotTaken { get; set; } = 0;
        public DateTime LastDonationZoneItemAdded { get; set; } = DateTime.MinValue;
        public DateTime LastJunkyardItemAdded { get; set; } = DateTime.MinValue;
    }
}