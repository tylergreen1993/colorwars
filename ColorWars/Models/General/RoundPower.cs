﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class RoundPower
    {
        public int Power { get; set; }
        public List<bool> IsFaded { get; set; }
        public List<bool> HasBackfired { get; set; }
        public RoundPowerStage Stage { get; set; }
    }

    public enum RoundPowerStage
    {
        Deck = 0,
        Enhancers = 1,
        Final = 2
    }
}