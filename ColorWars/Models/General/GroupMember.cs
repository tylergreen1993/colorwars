﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class GroupMember
    {
        public int GroupId { get; set; }
        public string Username { get; set; }
        public GroupRole Role { get; set; }
        public bool IsPrizeMember { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsActive => (DateTime.UtcNow - ModifiedDate).TotalDays <= 7;
        public DateTime LastReadMessageDate { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public enum GroupRole
    {
        [Display(Name = "Banned")]
        Banned = -1,
        [Display(Name = "Member")]
        Standard = 0,
        [Display(Name = "Leader")]
        Leader = 1,
        [Display(Name = "Founder")]
        Creator = 2
    }
}
