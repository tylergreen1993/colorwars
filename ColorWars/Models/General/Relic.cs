﻿namespace ColorWars.Models
{
    public class Relic
    {
        public RelicId Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl => $"Relics/{Name}.png";
        public int Level { get; set; }
        public bool Earned { get; set; }
    }

    public enum RelicId
    {
        None = 0,
        Moon = 1,
        Sun = 2,
        Fire = 3,
        Wind = 4,
        Water = 5,
        Shadow = 6,
        Eternal = 7
    }
}