﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class Contact
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string ClaimedUser { get; set; }
        public string EmailAddress { get; set; }
        public string IPAddress { get; set; }
        public string Message { get; set; }
        public ContactCategory Category { get; set; }
        public bool ReplyViaMessages { get; set; }
        public string Outcome { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum ContactCategory
    {
        [Display(Name = "---")]
        None = -1,
        [Display(Name = "Other")]
        Other = 0,
        [Display(Name = "Advertising Opportunities")]
        AdvertisingOpportunities = 1,
        [Display(Name = "Account Issue")]
        AccountIssue = 2,
        [Display(Name = "Copyright Issue")]
        CopyrightIssue = 3,
        [Display(Name = "General Help")]
        GeneralHelp = 4,
        [Display(Name = "General Question")]
        GeneralQuestion = 5,
        [Display(Name = "Helper/Staff Complaint")]
        StaffComplaint = 6,
        [Display(Name = "Suspended Account")]
        SuspendedAccount = 7,
        [Display(Name = "Payment Issue")]
        PaymentIssue = 8
    }
}
