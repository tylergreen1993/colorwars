﻿using System;
using Newtonsoft.Json;

namespace ColorWars.Models
{
    public class MarketStallHistory
    {
        public Guid Id { get; set; }
        public int MarketStallId { get; set; }
        public string Username { get; set; }
        public Guid ItemId { get; set; }
        public int Cost { get; set; }
        public ItemTheme Theme { get; set; }
        public DateTime CreatedDate { get; set; }

        [JsonProperty("ItemName")]
        private string _itemName { get; set; }
        [JsonIgnore]
        public string ItemName
        {
            get
            {
                if (Theme == ItemTheme.Default)
                {
                    return _itemName;
                }

                return $"{_itemName} - {Theme} Theme";
            }
            set
            {
                _itemName = value;
            }
        }
    }
}
