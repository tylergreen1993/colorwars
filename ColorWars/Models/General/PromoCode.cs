﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class PromoCode
    {
        public string Username { get; set; }
        public string Creator { get; set; }
        public string Code { get; set; }
        public PromoEffect Effect { get; set; }
        public bool OneUserUse { get; set; }
        public bool IsSponsor { get; set; }
        public Guid ItemId { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime RedeemedDate { get; set; }
    }

    public enum PromoEffect
    {
        [Display(Name = "No effect")]
        None = 0,
        [Display(Name = "Free CD")]
        GainPoints = 1,
        [Display(Name = "Collect daily Bank interest again")]
        ResetBankVisit = 2,
        [Display(Name = "Free Card Pack")]
        CardPack = 3,
        [Display(Name = "Free item")]
        RandomItem = 4,
        [Display(Name = "Maximum bag size increased")]
        IncreaseBagSize = 5,
        [Display(Name = "Stadium Bonus")]
        AddStadiumBonus = 6,
        [Display(Name = "The Young Wizard's powers are recharged")]
        ResetWizardVist = 7,
        [Display(Name = "Free CD")]
        GainBigPoints = 8,
        [Display(Name = "Junkyard discount")]
        JunkyardDiscount = 9,
        [Display(Name = "Secret Gift")]
        SecretGift = 10,
        [Display(Name = "Repair Shop discount")]
        RepairShopDiscount = 11,
        [Display(Name = "Visit the Spin of Success again")]
        ResetSpinOfSuccessVisit = 12,
        [Display(Name = "One free month of DeoDeck Club")]
        ClubTrial = 13,
        [Display(Name = "Visit Treasure Dive again")]
        ResetTreasureDiveVisit = 14,
        [Display(Name = "Free item")]
        BigItem = 15,
        [Display(Name = "Free Card Pack - Rare")]
        RareCardPack = 16,
        [Display(Name = "Store Magician free trial")]
        StoreMagicianTrial = 17,
        [Display(Name = "Maximum Market Stall size increased")]
        IncreaseMarketStallSize = 18
    }
}
