﻿using System;
using ColorWars.Classes;

namespace ColorWars.Models
{
    public class Bid
    {
        public Guid Id { get; set; }
        public int AuctionId { get; set; }
        public int Points { get; set; }
        public string Username { get; set; }
        public DateTime BidDate { get; set; }
    }
}
