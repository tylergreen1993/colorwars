﻿using System;

namespace ColorWars.Models
{
    public class ShowroomEnhancement
    {
        public ShowroomEnhancementId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Info { get; set; }
        public int Points { get; set; }
        public string ImageUrl { get; set; }
    }

    public enum ShowroomEnhancementId
    {
        None = 0,
        ShowroomAssessment = 1,
        IncreaseSize = 2,
        RechargeItems = 3,
        RepairItems = 4,
        RandomArtCard = 5
    }
}