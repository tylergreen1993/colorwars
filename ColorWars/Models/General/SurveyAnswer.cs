﻿using System;

namespace ColorWars.Models
{
    public class SurveyAnswer
    {
        public string Username { get; set; }
        public SurveyQuestionId SurveyQuestionId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
