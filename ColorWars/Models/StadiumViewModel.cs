﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StadiumViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public List<Enhancer> Enhancers { get; set; }
        public List<Card> AvailableDeck { get; set; }
        public List<MatchStatus> Results { get; set; }
        public List<Card> OpponentDeck { get; set; }
        public List<PowerMove> PowerMoves { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public List<RoundPower> RoundPowers { get; set; }
        public List<RoundPower> OpponentRoundPowers { get; set; }
        public List<MatchTag> MatchTags { get; set; }
        public List<MatchHistory> MatchHistories { get; set; }
        public List<MatchWaitlist> UserLobby { get; set; }
        public MatchState MatchState { get; set; }
        public MatchState OpponentMatchState { get; set; }
        public Card NextCard { get; set; }
        public Enhancer NextEnhancer { get; set; }
        public PowerMove NextPowerMove { get; set; }
        public Card OpponentCard { get; set; }
        public Enhancer OpponentEnhancer { get; set; }
        public PowerMove OpponentPowerMove { get; set; }
        public Item LossEnhancer { get; set; }
        public CPU CPU { get; set; }
        public Settings UserSettings { get; set; }
        public Settings OpponentSettings { get; set; }
        public MatchStatus RoundResult { get; set; }
        public MatchStatus FinalStatus { get; set; }
        public MatchRanking MatchRanking { get; set; }
        public StadiumTab StadiumTab { get; set; }
        public MatchWaitlist MatchWaitlist { get; set; }
        public Relic Relic { get; set; }
        public Relic NextRelic { get; set; }
        public Tourney Tourney { get; set; }
        public TourneyMatch TourneyMatch { get; set; }
        public bool IsCPU { get; set; }
        public bool IsCPUChallenge { get; set; }
        public bool IsSpecialCPU { get; set; }
        public bool IsInWaitlist { get; set; }
        public bool IsElite { get; set; }
        public bool FromChallenge { get; set; }
        public bool CanChallengeCPU { get; set; }
        public bool ShowTutorial { get; set; }
        public bool ShowRelicsTutorial { get; set; }
        public bool ShowCheckpointTutorial { get; set; }
        public bool ShowSuddenDeathTutorial { get; set; }
        public bool ShowEliteZoneTutorial { get; set; }
        public bool ShowStadiumMatchIntro { get; set; }
        public bool ShowPowerMovesTutorial { get; set; }
        public bool ShowFirstCheckpointDefeatedTutorial { get; set; }
        public bool ShowEnhancerLimitedUsesTutorial { get; set; }
        public bool ShowSecondMatchTutorial { get; set; }
        public bool ShowRelicRequirementTutorial { get; set; }
        public bool ShowStadiumTipTutorial { get; set; }
        public int Points { get; set; }
        public int SecondsLeft { get; set; }
        public int CPUMatchCount { get; set; }
        public int CPUWinStreak { get; set; }
        public int UserWinStreak { get; set; }
        public int TotalCPUs { get; set; }
        public int WinsNeeded { get; set; }
        public double UserWinPercent { get; set; }
        public double ThemeBoost { get; set; }
        public double OpponentThemeBoost { get; set; }
        public string OpponentName { get; set; }
        public string OpponentColor { get; set; }
        public string OpponentDisplayPicUrl { get; set; }
        public string CheckpointCPURequirement { get; set; }
        public string Tip { get; set; }
    }

    public enum StadiumTab
    {
        None = 0,
        CPU = 1,
        User = 2
    }
}