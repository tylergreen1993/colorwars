﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class CrystalBallViewModel : BaseViewModel
    {
        public List<CardColor> Colors { get; set; }
        public int Streak { get; set; }
        public bool HasSeenPattern { get; set; }
        public bool CanPlayCrystalBall { get; set; }
    }
}