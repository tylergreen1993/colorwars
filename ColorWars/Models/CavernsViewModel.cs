﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class CavernsViewModel : BaseViewModel
    {
        public List<SorceressEffect> Effects { get; set; }
        public List<Companion> Companions { get; set; }
        public List<Item> SeedCards { get; set; }
        public List<Seed> UserSeedCards { get; set; }
        public bool CanCastEffect { get; set; }
        public SorceressEffect SelectedEffect { get; set; }
        public Item PlantedItem { get; set; }
        public DateTime SeedCardTakeDate { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}