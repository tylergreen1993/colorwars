﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class EliteShopViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public bool CanBuyEliteShopItem { get; set; }
    }
}