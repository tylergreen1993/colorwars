﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SponsorSocietyViewModel : BaseViewModel
    {
        public List<Donor> RecentDonors { get; set; }
        public List<SponsorCoinPackage> SponsorCoinPackages { get; set; }
        public List<SponsorPerk> SponsorPerks { get; set; }
        public List<PromoCode> ActivePromoCodes { get; set; }
        public List<Item> Cards { get; set; }
        public ItemTheme Theme { get; set; }
        public CompanionType Companion { get; set; }
        public int SponsorCoins { get; set; }
        public double WeeklyAmount { get; set; }
        public double WeeklyTotal { get; set; }
        public string StripePublishableKey { get; set; }
    }
}