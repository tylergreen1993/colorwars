﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class DonationsViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public bool CanTakeDonation { get; set; }
    }
}