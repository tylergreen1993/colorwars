﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TakeOfferViewModel : BaseViewModel
    {
        public List<OfferCard> OfferCards { get; set; }
        public int HighestValueRemaining { get; set; }
        public int OfferTaken { get; set; }
        public int Offer { get; set; }
        public bool CanPlay { get; set; }
    }
}