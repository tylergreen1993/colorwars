﻿using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class AdminViewModel : BaseViewModel
    {
        public List<Contact> ClaimedContactSubmissions { get; set; }
        public List<Contact> ContactSubmissions { get; set; }
        public List<JobSubmission> JobSubmissions { get; set; }
        public List<JobSubmission> HelperJobSubmissions { get; set; }
        public List<string> UserLogs { get; set; }
        public List<InactiveAccount> InactiveAccounts { get; set; }
        public List<Report> Reports { get; set; }
        public List<Report> ClaimedReports { get; set; }
        public List<Report> CompletedReports { get; set; }
        public List<PromoCode> PreviousPromoCodes { get; set; }
        public List<Item> Items { get; set; }
        public List<SuspensionHistory> SuspensionHistories { get; set; }
        public List<AdminHistory> AdminHistories { get; set; }
        public List<TransferHistory> TransferHistories { get; set; }
        public List<ActiveSession> ActiveSessions { get; set; }
        public List<User> Users { get; set; }
        public List<User> RecentUsers { get; set; }
        public List<User> ReturningUsers { get; set; }
        public List<User> AllUsers { get; set; }
        public List<User> ActiveUsers { get; set; }
        public List<User> RecentlyReturningUsers { get; set; }
        public List<User> NonReturningUsers { get; set; }
        public List<User> SameIPUsers { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public Dictionary<User, List<Message>> LookUpMessages { get; set; }
        public List<IGrouping<string, SurveyAnswer>> SurveyAnswers { get; set; }
        public ActiveAdminPage ActivePage { get; set; }
        public ActiveAdminPage Type { get; set; }
        public News Article { get; set; }
        public JobPosition JobPosition { get; set; }
        public InactiveAccountType InactiveAccountType { get; set; }
        public Gender Gender { get; set; }
        public User LookUpUser { get; set; }
        public Bank LookUpBank { get; set; }
        public Group LookUpGroup { get; set; }
        public MarketStall LookUpMarketStall { get; set; }
        public AdminSettings AdminSettings { get; set; }
        public Statistics Statistics { get; set; }
        public bool IsHelper { get; set; }
        public bool IsIPAddressBanned { get; set; }
        public string SearchQuery { get; set; }
        public string IPAddress { get; set; }
        public int SurveyScore { get; set; }
        public double DAUMAUPercent { get; set; }
        public Settings LookUpSettings { get; set; }
    }

    public enum ActiveAdminPage
    {
        Users = 0,
        Reports = 1,
        Jobs = 2,
        Contact = 3,
        News = 4,
        Promos = 5,
        Claims = 6,
        CompletedReports = 7,
        CompletedJobs = 8,
        CompletedContact = 9,
        InactiveAccounts = 10,
        Surveys = 11,
        Settings = 12,
        TransferHistory = 13,
        Members = 14,
        UserStatistics = 15
    }
}