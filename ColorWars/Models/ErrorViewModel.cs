using System;
using System.Net;

namespace ColorWars.Models
{
    public class ErrorViewModel : BaseViewModel
    {
        public HttpStatusCode StatusCode { get; set; }
    }
}