﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class EffectsViewModel : BaseViewModel
    {
        public List<Effect> ActiveEffects { get; set; }
        public GroupPower ActiveGroupPower { get; set; }
    }
}