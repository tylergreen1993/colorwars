﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class WealthyDonorsViewModel : BaseViewModel
    {
        public List<WealthyDonor> TopDonors { get; set; }
        public int CurrentPot { get; set; }
        public bool CanTake { get; set; }
    }
}