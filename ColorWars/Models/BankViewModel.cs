﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class BankViewModel : BaseViewModel
    {
        public List<BankTransaction> BankTransactions { get; set; }
        public Bank Bank { get; set; }
        public BankAccount BankAccountType { get; set; }
        public BankAccount NextBankAccountType { get; set; }
        public int BankInterest { get; set; }
        public int MaxLoanAmount { get; set; }
        public int LoanAmountOwed { get; set; }
        public bool CanGetInterest { get; set; }
        public bool HasDoubleBankInterest { get; set; }
    }
}
