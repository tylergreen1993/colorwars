﻿using System;

namespace ColorWars.Models
{
    public class WallViewModel : BaseViewModel
    {
        public bool CanAddPoints { get; set; }
        public int StadiumBonus { get; set; }
        public bool HasStealthProtect { get; set; }
        public bool HasCurseProtect { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}