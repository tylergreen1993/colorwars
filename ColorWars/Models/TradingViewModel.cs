﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TradingViewModel : BaseViewModel
    {
        public List<Trade> Trades { get; set; }
        public Trade CurrentTrade { get; set; }
        public List<Item> Items { get; set; }
        public string SearchQuery { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public bool CanTrade { get; set; }
        public bool MatchExactSearch { get; set; }
        public ItemCategory SearchCategory { get; set; }
        public ItemCondition SearchCondition { get; set; }
    }
}
