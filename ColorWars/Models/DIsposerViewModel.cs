﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class DisposerViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public List<DisposerRecord> DisposerRecords { get; set; }
        public bool ShowDisposerConfirmation { get; set; }
        public int DisposedPoints { get; set; }
    }
}