﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class AboutViewModel : BaseViewModel
    {
        public List<Item> DisplayItems { get; set; }
        public Statistics Statistics { get; set; }
    }
}