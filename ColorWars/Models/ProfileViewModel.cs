﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ProfileViewModel : BaseViewModel
    {
        public User Profile { get; set; }
        public List<MatchHistory> MatchHistories { get; set; }
        public MatchRanking MatchRanking { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public List<Accomplishment> AllAccomplishments { get; set; }
        public List<Companion> Companions { get; set; }
        public List<MatchTag> MatchTags { get; set; }
        public List<Relic> Relics { get; set; }
        public List<Item> ShowroomItems { get; set; }
        public List<PartnershipRequest> PartnershipRequests { get; set; }
        public List<ShowroomLike> ShowroomLikes { get; set; }
        public List<Item> Items { get; set; }
        public List<Badge> AllBadges { get; set; }
        public Badge ActiveBadge { get; set; }
        public Group Group { get; set; }
        public MarketStall MarketStall { get; set; }
        public Settings ProfileSettings { get; set; }
        public int MaxShowroomSize { get; set; }
        public int AttentionHallPoints { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsJoinDay { get; set; }
        public bool IsElite { get; set; }
        public bool HasShowroom { get; set; }
        public bool IsUserProfile { get; set; }
        public bool IsCollapsed { get; set; }
        public bool CanRequestPartnership { get; set; }
        public bool PartnershipRequestPending { get; set; }
        public bool HasLikedShowroom { get; set; }
        public bool ShowAccomplishmentsTutorial { get; set; }
        public string ShowroomColor { get; set; }
        public AccomplishmentSort Sort { get; set; }
    }
}