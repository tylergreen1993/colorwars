﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ClubhouseViewModel : BaseViewModel
    {
        public List<Item> SampleItems { get; set; }
        public Item DailyItem { get; set; }
        public int TrainingTokens { get; set; }
        public int RewardPoints { get; set; }
        public bool IsMember { get; set; }
        public bool CanBuy { get; set; }
    }
}