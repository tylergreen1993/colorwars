﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StorageViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public bool CanAddStorage { get; set; }
        public bool ShowDisposerConfirmation { get; set; }
        public bool IsWithdraw { get; set; }
        public int MaxStorageSize { get; set; }
        public int MaxBagSize { get; set; }
        public int RepairAllCost { get; set; }
        public ItemSort Sort { get; set; }
    }
}
