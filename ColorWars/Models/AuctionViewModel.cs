﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class AuctionViewModel : BaseViewModel
    {
        public List<Auction> Auctions { get; set; }
        public Auction CurrentAuction { get; set; }
        public List<Item> Items { get; set; }
        public Item SelectedItem { get; set; }
        public string SearchQuery { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int CollectPoints { get; set; }
        public List<Item> CollectItems { get; set; }
        public bool UncollectedEarnings { get; set; }
        public bool MatchExactSearch { get; set; }
        public ItemCategory SearchCategory { get; set; }
        public ItemCondition SearchCondition { get; set; }
    }
}