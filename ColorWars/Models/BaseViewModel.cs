﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace ColorWars.Models
{
    public class BaseViewModel
    {
        public string Title { get; set; }
        public User User { get; set; }
        public LocationId? LocationId { get; set; }
        public string Parent { get; set; }
        public string ParentUrl { get; set; }
        public string TopParent { get; set; }
        public bool Authenticated => User != null;
        public bool ShowTitle { get; set; } = true;
        public bool ShowFooter { get; set; } = true;

        public void UpdateViewData(ViewDataDictionary viewData)
        {
            viewData["Title"] = Title;
            viewData["Authenticated"] = Authenticated;
            viewData["Points"] = User?.GetFormattedPoints();
            viewData["NumericalPoints"] = User?.Points ?? 0;
            viewData["Username"] = User?.Username;
            viewData["UserImage"] = User?.DisplayPicUrl;
            viewData["ShowTitle"] = ShowTitle;
            viewData["ShowFooter"] = ShowFooter;
            viewData["LocationId"] = LocationId;
            viewData["Parent"] = Parent;
            viewData["ParentUrl"] = ParentUrl;
            viewData["TopParent"] = TopParent;
            viewData["UserRole"] = User?.Role ?? UserRole.Default;
        }
    }
}
