﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class PromoCodesViewModel : BaseViewModel
    {
        public List<PromoCodeHistory> PromoCodeHistories { get; set; }
        public string Code { get; set; }
    }
}
