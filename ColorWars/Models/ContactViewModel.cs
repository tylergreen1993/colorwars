﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ContactViewModel : BaseViewModel
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
    }
}