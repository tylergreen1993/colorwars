﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SurveyViewModel : BaseViewModel
    {
        public List<SurveyQuestion> SurveyQuestions { get; set; }
        public int Reward { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}