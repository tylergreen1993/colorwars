﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TopCounterViewModel : BaseViewModel
    {
        public List<Card> Cards { get; set; }
        public bool CanPlay { get; set; }
        public DateTime LastPlayDate { get;set; }
    }
}