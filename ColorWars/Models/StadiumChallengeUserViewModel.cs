﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StadiumChallengeUserViewModel : BaseViewModel
    {
        public User ChallengeUser { get; set; }
        public List<Card> Deck { get; set; }
        public int MatchLevel { get; set; }
        public bool IsDeckAllowed { get; set; }
        public bool HasPendingRequest { get; set; }
    }
}