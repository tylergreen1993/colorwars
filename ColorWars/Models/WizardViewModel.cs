﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class WizardViewModel : BaseViewModel
    {
        public List<WizardWandColor> Wands { get; set; }
        public List<WizardHistory> WizardHistories { get; set; }
        public List<WizardRecord> WizardRecords { get; set; }
        public List<Card> Deck { get; set; }
        public int GuardiansDefeated { get; set; }
        public bool CanSeeWizard { get; set; }
        public bool HasBetterLuck { get; set; }
        public bool CanSeeSecretCavern { get; set; }
    }

    public enum WizardWandColor
    {
        None = 0,
        Red = 1,
        Orange = 2,
        Yellow = 3,
        Green = 4,
        Blue = 5,
        Purple = 6,
        Gold = 7
    }
}