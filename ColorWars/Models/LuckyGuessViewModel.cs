﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class LuckyGuessViewModel : BaseViewModel
    {
        public List<LuckyGuess> Chests { get; set; }
        public List<LuckyGuess> PastWinners { get; set; }
        public bool CanPlay { get; set; }
    }
}