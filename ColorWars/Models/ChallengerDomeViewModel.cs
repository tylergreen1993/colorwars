﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ChallengerDomeViewModel : BaseViewModel
    {
        public List<Card> Deck { get; set; }
        public int ChallengerDomeStreak { get; set; }
        public int MatchLevel { get; set; }
        public bool CanPlay { get; set; }
    }
}