﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class RightSideUpWorldViewModel : BaseViewModel
    {
        public List<Enhancer> Items { get; set; }
        public List<HallOfFameEntry> HallOfFame { get; set; }
        public List<Card> Deck { get; set; }
        public Item Item { get; set; }
        public bool CanBuyItem { get; set; }
        public bool HasDefeatedGuard { get; set; }
        public bool HasTakenItem { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}