﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SecretShrineViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public bool CanLeaveItem { get; set; }
        public DateTime LeaveItemDate { get; set; }
    }
}