﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StreakCardsViewModel : BaseViewModel
    {
        public List<StreakCard> StreakCards { get; set; }
        public int TotalEarned { get; set; }
        public int Streak { get; set; }
        public bool HasBetterLuck { get; set; }
        public bool CanPlayStreakCards { get; set; }
    }
}