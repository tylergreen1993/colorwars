﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class LottoViewModel : BaseViewModel
    {
        public Lotto CurrentLotto { get; set; }
        public List<Lotto> Lottos { get; set; }
        public List<Item> EggCards { get; set; }
        public List<int> PastGuesses { get; set; }
        public int TaxPercent { get; set; }
        public bool CanEnterLotto { get; set; }
        public bool CanBuyLottoItem { get; set; }
    }
}