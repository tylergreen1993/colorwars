﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SpinSuccessViewModel : BaseViewModel
    {
        public List<SpinSuccessHistory> SpinSuccessHistories { get; set; }
        public bool CanSpin { get; set; }
        public bool HasBetterLuck { get; set; }
    }

    public enum SpinSuccessCategory
    {
        DeckChange = 0,
        RepairedItem = 1,
        StolenItem = 2,
        PointsChange = 3,
        StadiumChange = 4,
        NewItem = 5
    }
}