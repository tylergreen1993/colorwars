﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StoreMagicianViewModel : BaseViewModel
    {
        public List<StoreMagicianItem> Items { get; set; }
        public string SearchQuery { get; set; }
        public bool MatchExactSearch { get; set; }
        public ItemCategory SearchCategory { get; set; }
        public ItemCondition SearchCondition { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}