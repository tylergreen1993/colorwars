﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TrainingCenterViewModel : BaseViewModel
    {
        public List<Card> Cards { get; set; }
        public List<Relic> Relics { get; set; }
        public List<PowerMove> PowerMoves { get; set; }
        public List<PowerMove> AllPowerMoves { get; set; }
        public List<TrainingCenterUpgrade> Upgrades { get; set; }
        public Relic NextRelic { get; set; }
        public DeckType DeckType { get; set; }
        public int Tokens { get; set; }
        public bool IsInMatch { get; set; }
        public bool HasUnlocked { get; set; }
        public bool ExpandAll { get; set; }
        public DateTime PortalExpiryDate { get; set; }
    }
}