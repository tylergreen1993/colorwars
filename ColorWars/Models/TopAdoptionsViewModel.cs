﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TopAdoptionsViewModel : BaseViewModel
    {
        public List<Companion> Companions { get; set; }
        public List<KeyCard> KeyCards { get; set; }
        public List<CellarStoreItem> CellarStoreItems { get; set; }
        public bool IsCellarUnlocked { get; set; }
        public bool CanBuyCellarStoreItem { get; set; }
    }
}