﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class PasswordResetViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public PasswordReset PasswordReset { get; set; }
        public bool CanResetPassword { get; set; }
        public int TimeLeftInMinutes { get; set; }
    }
}