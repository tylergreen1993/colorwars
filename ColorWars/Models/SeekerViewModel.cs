﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class SeekerViewModel : BaseViewModel
    {
        public List<Item> ChallengeItems { get; set; }
        public Item RewardItem { get; set; }
        public int Level { get; set; }
        public bool HasSeekerGroupPower { get; set; }
        public bool HasChallengeExpired { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}