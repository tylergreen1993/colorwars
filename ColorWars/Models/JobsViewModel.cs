﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorWars.Models
{
    public class JobsViewModel : BaseViewModel
    {
        public List<JobSubmission> JobSubmissions { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public JobPosition SelectedPosition { get; set; }
        public JobTitle JobTitle { get; set; }
        public int JobTitlePoints { get; set; }
        public bool Expand { get; set; }
        public bool CanSubmitHelperApplication { get; set; }
        public bool HasClaimedTermsReward { get; set; }
    }

    public enum JobTitle
    {
        [Display(Name = "Recruit")]
        Recruit = 0,
        [Display(Name = "Junior")]
        Junior = 1,
        [Display(Name = "Intermediate")]
        Intermediate = 2,
        [Display(Name = "Professional")]
        Professional = 3,
        [Display(Name = "Senior")]
        Senior = 4,
        [Display(Name = "Team Lead")]
        TeamLead = 5,
        [Display(Name = "Assistant to the Manager")]
        AssistantToTheManager = 6,
        [Display(Name = "Jr. Manager")]
        JrManager = 7,
        [Display(Name = "Sr. Manager")]
        SrManager = 8,
        [Display(Name = "Chief Officer")]
        Officer = 9,
        [Display(Name = "President")]
        President = 10,
    }
}