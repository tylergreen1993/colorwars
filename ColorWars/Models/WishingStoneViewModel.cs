﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class WishingStoneViewModel : BaseViewModel
    {
        public List<Wish> Wishes { get; set; }
        public List<QuarryStone> QuarryStones { get; set; }
        public int MaxQuarryStonesCanMine { get; set; }
        public int QuarryStonesLeft { get; set; }
        public int TotalEarned { get; set; }
        public bool CanMakeWish { get; set; }
        public bool HasBetterLuck { get; set; }
        public bool CanMine { get; set; }
    }
}