﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class BuyersClubViewModel : BaseViewModel
    {
        public List<StoreItem> StoreItems { get; set; }
        public List<ShowroomEnhancement> ShowroomEnhancements { get; set; }
        public int BuyersClubPoints { get; set; }
        public int RemainingItems { get; set; }
        public bool CanBuy { get; set; }
        public bool CanAccessShowroom { get; set; }
        public bool CanChallengeShowroomOwner { get; set; }
        public bool ShowDiscount { get; set; }
        public bool ShowshowShowroomTitleChange { get; set; }
        public string ShowroomTitle { get; set; }
        public string ShowroomDescription { get; set; }
        public string ShowroomColor { get; set; }
        public DateTime LastBuyDate { get; set; }
    }
}