﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class HallViewModel : BaseViewModel
    {
        public List<User> Helpers { get; set; }
        public List<User> Staff { get; set; }
    }
}