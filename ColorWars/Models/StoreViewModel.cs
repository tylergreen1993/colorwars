﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StoreViewModel : BaseViewModel
    {
        public List<StoreItem> StoreItems { get; set; }
        public StoreItem SelectedItem { get; set; }
        public bool CanBuy { get; set; }
        public int DiscountPercent { get; set; }
        public int RemainingItems { get; set; }
        public ItemCategory DiscountItemCategory { get; set; }
        public DateTime LastBuyDate { get; set; }
    }
}