﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class TourneyViewModel : BaseViewModel
    {
        public List<Tourney> Tourneys { get; set; }
        public Tourney CurrentTourney { get; set; }
        public User TourneyOpponent { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
    }
}