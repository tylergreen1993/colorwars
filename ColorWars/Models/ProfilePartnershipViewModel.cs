﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ProfilePartnershipViewModel : BaseViewModel
    {
        public User PartnershipUser { get; set; }
        public List<PartnershipEarning> PartnershipEarnings { get; set; }
    }
}