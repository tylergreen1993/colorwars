﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class HomeViewModel : BaseViewModel
    {
        public List<Feed> Feed { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public List<Checklist> Checklist { get; set; }
        public List<Blocked> BlockedUsers { get; set; }
        public List<BonusPrize> BonusPrizes { get; set; }
        public List<Location> FavoritedLocations { get; set; }
        public AdminSettings AdminSettings { get; set; }
        public CPU NextChallenger { get; set; }
        public int UnreadNotifications { get; set; }
        public int UnreadMessages { get; set; }
        public bool ShowTutorial { get; set; }
        public bool ShowEnableNotifications { get; set; }
        public bool ShowReferUserTutorial { get; set; }
        public bool ShowSponsorTutorial { get; set; }
        public bool ShowChecklistAnimation { get; set; }
        public bool ShowEnableSFX { get; set; }
        public bool ShowMessagesPopup { get; set; }
        public bool HighlightChecklist { get; set; }
        public bool CanHighlightFeeds { get; set; }
        public bool IsJoinDay { get; set; }
        public Badge ActiveBadge { get; set; }
        public DateTime LastFeedVisitDate { get; set; }
    }
}