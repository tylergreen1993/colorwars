﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class RankingsViewModel : BaseViewModel
    {
        public List<MatchRanking> MatchRankings { get; set; }
        public List<WealthRanking> WealthRankings { get; set; }
        public bool WealthRankingsFirst { get; set; }
    }
}