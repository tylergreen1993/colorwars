﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class NoticeBoardViewModel : BaseViewModel
    {
        public NoticeBoardTask Task { get; set; }
        public Item Item { get; set; }
        public List<Item> Items { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}