﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class PrizeCupViewModel : BaseViewModel
    {
        public List<PrizeCup> PrizeCups { get; set; }
        public int Pot { get; set; }
        public bool CanPlay { get; set; }
    }
}