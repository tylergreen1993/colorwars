﻿using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class FairViewModel : BaseViewModel
    {
        public IEnumerable<IGrouping<LocationCategory, Location>> Locations { get; set; }
        public string Tag { get; set; }
        public bool ShowFairTutorial { get; set; }
    }
}
