﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class CraftingViewModel : BaseViewModel
    {
        public List<IGrouping<CardColor, Crafting>> GroupedCraftingCards { get; set; }
        public List<IGrouping<CardColor, Crafting>> GroupedPartialCraftingCards { get; set; }
        public List<IGrouping<ItemTheme, Theme>> GroupedThemeCards { get; set; }
        public List<IGrouping<string, Item>> GroupedOtherCards { get; set; }
        public List<Frozen> FrozenItems { get; set; }
        public Item FurnaceItem { get; set; }
        public DateTime DefrostDate { get; set; }
        public DateTime BetterCraftingLuckExpiryDate { get; set; }
    }
}