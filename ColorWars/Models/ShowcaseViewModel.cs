﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ShowcaseViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public Item Item { get; set; }
        public int TimeLeft { get; set; }
        public bool CanBuyItem { get; set; }
        public bool HasBiggerDiscount { get; set; }
    }
}