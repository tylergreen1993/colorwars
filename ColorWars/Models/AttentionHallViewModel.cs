﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class AttentionHallViewModel : BaseViewModel
    {
        public List<AttentionHallPost> AttentionHallPosts { get; set; }
        public AttentionHallPost AttentionHallPost { get; set; }
        public List<AttentionHallComment> AttentionHallComments { get; set; }
        public List<Item> UserItems { get; set; }
        public List<Accomplishment> UserAccomplishments { get; set; }
        public List<MatchTag> UserMatchTags { get; set; }
        public JobState? JobState { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public AttentionHallPostsFilter CurrentFilter { get; set; }
        public AttentionHallPostType TypeFilter { get; set; }
    }
}