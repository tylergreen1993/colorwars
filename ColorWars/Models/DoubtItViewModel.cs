﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class DoubtItViewModel : BaseViewModel
    {
        public List<DoubtItCard> DoubtItCards { get; set; }
        public List<DoubtItCard> UserCards { get; set; }
        public List<DoubtItOwner> Owners { get; set; }
        public List<int> PossibleAmounts { get; set; }
        public DoubtItOwner CurrentTurn { get; set; }
        public DoubtItOwner Winner { get; set; }
        public (int amount, int type) LastMove { get; set; }
        public int TotalWins { get; set; }
        public string DoubtItMessage { get; set; }
        public bool DoubtItMessageSuccess { get; set; }
    }
}