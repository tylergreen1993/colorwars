﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MessagesViewModel : BaseViewModel
    {
        public User Correspondent { get; set; }
        public List<Message> AllMessages { get; set; }
        public List<Message> Conversation { get; set; }
        public bool IsBlocked { get; set; }
        public bool CanSendMessage { get; set; }
        public bool ShowReadReceipts { get; set; }
        public bool IsMessageBox { get; set; }
        public string SearchQuery { get; set; }
    }
}