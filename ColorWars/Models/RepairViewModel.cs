﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class RepairViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public DeckType DeckType { get; set; }
        public int DiscountPercent { get; set; }
        public int RepairAllCost { get; set; }
        public bool IsRecharge { get; set; }
        public bool FromStadium { get; set; }
    }
}