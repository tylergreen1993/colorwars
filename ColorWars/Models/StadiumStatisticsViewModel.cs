﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StadiumStatisticsViewModel : BaseViewModel
    {
        public List<MatchHistory> MatchHistories { get; set; }
        public User Profile { get; set; }
        public CPU CPU { get; set; }
        public MatchRanking MatchRanking { get; set; }
        public int CPULevel { get; set; }
        public int TotalCPULevels { get; set; }
        public int Intensity { get; set; }
        public int TotalCPUs { get; set; }
        public double CPULevelPercent { get; set; }
        public double UserWinPercent { get; set; }
        public bool OnlyShowUserMatches { get; set; }
        public bool IsElite { get; set; }
    }
}