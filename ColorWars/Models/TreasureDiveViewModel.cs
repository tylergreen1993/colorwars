﻿namespace ColorWars.Models
{
    public class TreasureDiveViewModel : BaseViewModel
    {
        public TreasureDive TreasureDive { get; set; }
        public bool CanPlay { get; set; }
        public bool IsOnTheHour { get; set; }
        public bool HasTreasureHint { get; set; }
    }
}