﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MerchantsViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public List<Jewel> Jewels { get; set; }
        public bool CanEnterContest { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}