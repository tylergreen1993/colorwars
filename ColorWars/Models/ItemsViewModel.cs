﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ItemsViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public List<Card> Deck { get; set; }
        public List<PowerMove> PowerMoves { get; set; }
        public List<Companion> Companions { get; set; }
        public List<CompanionColor> CompanionColors { get; set; }
        public List<Guid> UnearnedCards { get; set; }
        public Guid SelectionId { get; set; }
        public bool IsInMatch { get; set; }
        public bool HasShowroom { get; set; }
        public bool UnlockedExtraDeck { get; set; }
        public bool ShowTutorial { get; set; }
        public bool ShowPowerMovesTutorial { get; set; }
        public bool ShowRelicRequirementTutorial { get; set; }
        public bool ShowPowerMovesFirst { get; set; }
        public bool IsFirstCardPack { get; set; }
        public int MaxBagSize { get; set; }
        public int TotalItems { get; set; }
        public int MaxPowerMoves { get; set; }
        public int MaxItemsCount { get; set; }
        public Theme SelectedTheme { get; set; }
        public Sleeve SelectedSleeve { get; set; }
        public Stealth SelectedStealth { get; set; }
        public Fairy SelectedFairy { get; set; }
        public CompanionItem SelectedCompanion { get; set; }
        public Companion SelectedActivatedCompanion { get; set; }
        public Upgrader SelectedUpgrader { get; set; }
        public Candy SelectedCandy { get; set; }
        public DeckType DeckType { get; set; }
        public ItemSort Sort { get; set; }
        public DateTime BagAddedLastDate { get; set; }
    }
}