﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class PhantomDoorViewModel : BaseViewModel
    {
        public List<Card> Cards { get; set; }
        public List<Relic> Relics { get; set; }
        public List<SpiritSister> SpiritSisters { get; set; }
        public List<Accomplishment> Accomplishments { get; set; }
        public List<Item> Items { get; set; }
        public Item Item { get; set; }
        public int Level { get; set; }
        public bool CanOpenDoor { get; set; }
        public bool CanReadNote { get; set; }
        public bool HasReceivedLanguageHint { get; set; }
        public string SpeltWord { get; set; }
    }
}