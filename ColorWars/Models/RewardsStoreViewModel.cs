﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class RewardsStoreViewModel : BaseViewModel
    {
        public int RewardPoints { get; set; }
        public List<Reward> Rewards { get; set; }
        public bool CanRedeem { get; set; }
        public bool IsPartnershipEnabled { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}