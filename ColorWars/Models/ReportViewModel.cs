﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ReportViewModel : BaseViewModel
    {
        public User ReportedUser { get; set; }
        public int AttentionHallPostId { get; set; }
        public bool IsUserBlocked { get; set; }
        public int Number1 { get; set; }
        public int Number2 { get; set; }
    }
}