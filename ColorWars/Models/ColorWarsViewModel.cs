﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ColorWarsViewModel : BaseViewModel
    {
        public ColorWarsColor TeamColor { get; set; }
        public int Team1Points { get; set; }
        public int Team2Points { get; set; }
        public int PointsGoal { get; set; }
        public List<ColorWarsSubmission> Team1Submissions { get; set; }
        public List<ColorWarsSubmission> Team2Submissions { get; set; }
        public List<ColorWarsHistory> ColorWarsHistories { get; set; }
        public bool CanContribute { get; set; }
    }
}