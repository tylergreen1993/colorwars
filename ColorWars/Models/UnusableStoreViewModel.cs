﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class UnusableStoreViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
    }
}