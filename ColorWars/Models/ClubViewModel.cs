﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class ClubViewModel : BaseViewModel
    {
        public List<Item> Items { get; set; }
        public List<ClubMembership> ClubMemberships { get; set; }
        public List<Enhancer> Enhancers { get; set; }
        public Item NextItem { get; set; }
        public Item IncubatedItem { get; set; }
        public bool IsMember { get; set; }
        public bool CanUseBoost { get; set; }
        public int IncubationDays { get; set; }
        public DateTime MembershipEndDate { get; set; }
    }
}
