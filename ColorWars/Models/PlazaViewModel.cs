﻿using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Models
{
    public class PlazaViewModel : BaseViewModel
    {
        public List<IGrouping<LocationType, Location>> Locations { get; set; }
        public string SearchQuery { get; set; }
        public string SuccessMessage { get; set; }
        public Location MatchedUnavailableLocation { get; set; }
        public LocationId? LastLocation { get; set; }
        public bool LocationNotAvailable { get; set; }
        public bool ShowLocationsTutorial { get; set; }
        public bool ShowFavoritesTutorial { get; set; }
        public bool ExpandLocations  { get; set; }
        public bool CanShowAds { get; set; }
        public FeedCategory ShowHiddenLocation { get; set; }
    }
}
