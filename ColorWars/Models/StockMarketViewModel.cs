﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class StockMarketViewModel : BaseViewModel
    {
        public List<Stock> Portfolio { get; set; }
        public List<Stock> Market { get; set; }
        public List<StockHistory> RecentTransactions { get; set; }
        public Stock Stock { get; set; }
        public int TotalPortfolioValue { get; set; }
        public int OriginalTotalPortfolioValue { get; set; }
        public int TotalPortfolioChanged { get; set; }
        public int CollectableDividends { get; set; }
        public double PortfolioValuePercentChanged { get; set; }
        public double StockPercentChanged { get; set; }
        public bool CanBuyShares { get; set; }
        public bool CanCollectDividends { get; set; }
        public bool HasStockDiscount { get; set; }
        public StockSort Sort { get; set; }
        public PortfolioSort PortfolioSort { get; set; }
    }
}