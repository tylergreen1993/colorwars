﻿using System;
using System.Collections.Generic;

namespace ColorWars.Models
{
    public class GroupsViewModel : BaseViewModel
    {
        public Group CurrentGroup { get; set; }
        public List<Group> AllGroups { get; set; }
        public GroupMember UserMember { get; set; }
        public GroupMember PrizeMember { get; set; }
        public List<Group> UserGroups { get; set; }
        public List<GroupMessage> Messages { get; set; }
        public List<GroupRequest> Requests { get; set; }
        public GroupRequestStatus RequestStatus { get; set; }
        public int UnreadGroupMessages { get; set; }
        public string UniqueLink { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public string SearchQuery { get; set; }
    }
}