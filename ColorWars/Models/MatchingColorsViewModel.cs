﻿using System.Collections.Generic;

namespace ColorWars.Models
{
    public class MatchingColorsViewModel : BaseViewModel
    {
        public List<MatchingCard> MatchingCards { get; set; }
        public CardColor MatchingColor { get; set; }
        public bool CanPlayMatchingCards { get; set; }
    }
}