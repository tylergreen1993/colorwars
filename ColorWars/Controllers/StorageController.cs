﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StorageController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISafetyDepositRepository _safetyDepositRepository;
        private IDisposerRepository _disposerRepository;
        private IRepairRepository _repairRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private ISoundsRepository _soundsRepository;

        public StorageController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        ISafetyDepositRepository safetyDepositRepository, IDisposerRepository disposerRepository, IRepairRepository repairRepository,
        IPointsRepository pointsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _safetyDepositRepository = safetyDepositRepository;
            _disposerRepository = disposerRepository;
            _repairRepository = repairRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(bool addItems, ItemSort? sort)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Storage" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (sort.HasValue)
            {
                if (settings.StorageSort != sort)
                {
                    settings.StorageSort = sort.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.StorageSort), settings.StorageSort.ToString());
                }
            }
            else
            {
                sort = settings.StorageSort;
            }

            StorageViewModel storageViewModel = GenerateModel(user, !addItems, sort.Value);
            storageViewModel.UpdateViewData(ViewData);
            return View(storageViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StoreItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item no longer exists in your bag." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (_safetyDepositRepository.GetSafetyDepositItems(user).Count >= settings.MaxStorageSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Storage is full." });
            }

            _itemsRepository.RemoveItem(user, item);
            _safetyDepositRepository.AddSafetyDepositItem(user, item);

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your Storage!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _safetyDepositRepository.GetSafetyDepositItems(user);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item no longer exists in your Storage." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            Settings settings = _settingsRepository.GetSettings(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_safetyDepositRepository.DeleteSafetyDepositItem(user, item))
            {
                foreach (Item updateItem in items.FindAll(x => x.Position > item.Position))
                {
                    updateItem.Position--;
                    _safetyDepositRepository.UpdateSafetyDepositItem(updateItem);
                }
            }
            _itemsRepository.AddItem(user, item);

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your bag!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DisposeItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _safetyDepositRepository.GetSafetyDepositItems(user);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item no longer exists in your Storage." });
            }

            string infoMessage = string.Empty;
            string soundUrl = string.Empty;
            if (_safetyDepositRepository.DeleteSafetyDepositItem(user, item))
            {
                _disposerRepository.DisposeItem(user, item, false, out bool recordQualifier);
                if (recordQualifier)
                {
                    infoMessage = $"You qualified as one of the top disposers of the past month!";
                }

                foreach (Item updateItem in items.FindAll(x => x.Position > item.Position))
                {
                    updateItem.Position--;
                    _safetyDepositRepository.UpdateSafetyDepositItem(updateItem);
                }
                Settings settings = _settingsRepository.GetSettings(user);
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Shred, settings);
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully disposed of the item \"{item.Name}\"!", InfoMessage = infoMessage, SoundUrl = soundUrl });
        }

        [HttpPost]
        public IActionResult UpdatePosition(Guid selectionId, int newPosition)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> storageItems = _safetyDepositRepository.GetSafetyDepositItems(user);

            Item item = storageItems.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have this item." });
            }

            if (newPosition < 0 || newPosition >= storageItems.Count)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't a valid position." });
            }

            if (newPosition == item.Position)
            {
                return Json(new Status { Success = false });
            }

            if (newPosition < item.Position)
            {
                foreach (Item updateItem in storageItems.FindAll(x => x.Position >= newPosition && x.Position < item.Position))
                {
                    updateItem.Position++;
                    _safetyDepositRepository.UpdateSafetyDepositItem(updateItem);
                }
            }
            else
            {
                foreach (Item updateItem in storageItems.FindAll(x => x.Position <= newPosition && x.Position > item.Position))
                {
                    updateItem.Position--;
                    _safetyDepositRepository.UpdateSafetyDepositItem(updateItem);
                }
            }

            item.Position = newPosition;
            _safetyDepositRepository.UpdateSafetyDepositItem(item);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RepairAll()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int cost = GetRepairCost(user, null, settings.RepairCouponPercent);
            if (cost == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have no items in your Storage to repair." });
            }

            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                if (_safetyDepositRepository.RepairAllItems(user))
                {
                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully repaired all the items in your Storage for {Helper.GetFormattedPoints(cost)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"The items in your Storage could not be repaired." });
        }


        [HttpGet]
        public IActionResult GetStoragePartialView(bool isWithdraw)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            StorageViewModel storageViewModel = GenerateModel(user, isWithdraw, settings.StorageSort);
            if (isWithdraw)
            {
                return PartialView("_StorageMainPartial", storageViewModel);
            }

            return PartialView("_StorageAddPartial", storageViewModel);
        }

        private StorageViewModel GenerateModel(User user, bool isWithdraw, ItemSort sort)
        {
            StorageViewModel storageViewModel = new StorageViewModel
            {
                Title = "Storage",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Storage,
                IsWithdraw = isWithdraw,
                Sort = sort
            };

            Settings settings = _settingsRepository.GetSettings(user);
            if (isWithdraw)
            {
                storageViewModel.Items = _safetyDepositRepository.GetSafetyDepositItems(user);
                if (storageViewModel.Items.Count > 1 && storageViewModel.Items.GroupBy(x => x.Position).Any(x => x.Count() > 1))
                {
                    for (int i = 0; i < storageViewModel.Items.Count; i++)
                    {
                        storageViewModel.Items[i].Position = i;
                        _safetyDepositRepository.UpdateSafetyDepositItem(storageViewModel.Items[i]);
                    }
                }
                if (sort == ItemSort.Manual)
                {
                    storageViewModel.Items = storageViewModel.Items.OrderBy(x => x.Position).ToList();
                }
                storageViewModel.MaxStorageSize = settings.MaxStorageSize;
                storageViewModel.ShowDisposerConfirmation = settings.ShowDisposerConfirmation;
                storageViewModel.RepairAllCost = GetRepairCost(user, storageViewModel.Items, settings.RepairCouponPercent);
            }
            else
            {
                List<Item> userItems = _itemsRepository.GetItems(user);
                storageViewModel.Items = userItems.FindAll(x => x.DeckType == DeckType.None);
                storageViewModel.CanAddStorage = _safetyDepositRepository.GetSafetyDepositItems(user).Count < settings.MaxStorageSize;
                storageViewModel.MaxBagSize = settings.MaxBagSize - userItems.FindAll(x => x.DeckType != DeckType.None).Count;
            }

            return storageViewModel;
        }

        private int GetRepairCost(User user, List<Item> items, int discountPercent)
        {
            items ??= _safetyDepositRepository.GetSafetyDepositItems(user);
            items = items.FindAll(x => x.Condition != ItemCondition.New);
            int totalCost = 0;
            foreach (Item item in items)
            {
                totalCost += _repairRepository.GetRepairPoints(item, discountPercent);
            }
            return totalCost;
        }

    }
}
