﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StreakCardsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IStreakCardsRepository _streakCardsRepository;
        private IGroupsRepository _groupsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public StreakCardsController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IStreakCardsRepository streakCardsRepository, IGroupsRepository groupsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _streakCardsRepository = streakCardsRepository;
            _groupsRepository = groupsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "StreakCards" });
            }

            StreakCardsViewModel streakCardsViewModel = GenerateModel(user);
            streakCardsViewModel.UpdateViewData(ViewData);
            return View(streakCardsViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastStreakCardsPlayDate).TotalHours;
            if (hoursSinceLastGame < 8)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Streak Cards recently. Try again in {8 - hoursSinceLastGame} hour{(8 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            if (position > 9 || position < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Streak Card can't be revealed." });
            }

            List<StreakCard> streakCards = _streakCardsRepository.GetStreakCards(user);
            if (streakCards.Count >= 3)
            {
                streakCards = new List<StreakCard>();
            }
            if (streakCards.Any(x => x.Position == position))
            {
                return Json(new Status { Success = false, ErrorMessage = "This Streak Card has already been revealed." });
            }

            GroupPower groupPower = _groupsRepository.GetGroupPower(user);

            Random rnd = new Random();
            int odds = groupPower == GroupPower.BetterStreakCardsLuck ? 12 : 10;
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds += 1;
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    odds -= 1;
                }
            }

            bool firstTime = settings.LastStreakCardsPlayDate == DateTime.MinValue;

            int choice = rnd.Next(firstTime ? 4 : 0, odds);
            int points = 0;

            if (choice >= 9)
            {
                points = 500;
            }
            else if (choice >= 6)
            {
                points = rnd.Next(250, 500);
            }
            else if (choice >= 3)
            {
                points = rnd.Next(0, 250);
            }

            string soundUrl = string.Empty;

            if (_streakCardsRepository.AddStreakCard(user, position, points))
            {
                if (streakCards.Count == 2)
                {
                    settings.LastStreakCardsPlayDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastStreakCardsPlayDate), settings.LastStreakCardsPlayDate.ToString());

                    double hoursSinceLastDailyStreakCards = (DateTime.UtcNow - settings.LastDailyStreakCardsPlayDate).TotalHours;
                    if (hoursSinceLastDailyStreakCards >= 24)
                    {
                        settings.LastDailyStreakCardsPlayDate = DateTime.UtcNow;
                        _settingsRepository.SetSetting(user, nameof(settings.LastDailyStreakCardsPlayDate), settings.LastDailyStreakCardsPlayDate.ToString());

                        if (hoursSinceLastDailyStreakCards >= 48 && settings.StreakCardsNoExpiryDate <= DateTime.UtcNow)
                        {
                            settings.StreakCardsStreak = 0;
                        }

                        settings.StreakCardsStreak++;
                        _settingsRepository.SetSetting(user, nameof(settings.StreakCardsStreak), settings.StreakCardsStreak.ToString());
                    }

                    int totalEarned = Math.Min(100000, (streakCards.Sum(x => x.Points) + points) * settings.StreakCardsStreak);
                    _pointsRepository.AddPoints(user, totalEarned);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    AddAccomplishments(user, settings.StreakCardsStreak, accomplishments);

                    if (totalEarned == 0)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCardsLoss))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCardsLoss);
                        }

                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                        return Json(new Status { Success = true, InfoMessage = $"You revealed 3 Streak Cards but earned {Helper.GetFormattedPoints(0)}!", SoundUrl = soundUrl });
                    }

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You revealed 3 Streak Cards and successfully earned {Helper.GetFormattedPoints(totalEarned)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
                }
                if (streakCards.Count >= 3)
                {
                    _streakCardsRepository.DeleteStreakCards(user);
                }
            }

            if (points > 0)
            {
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);
            }

            return Json(new Status { Success = true, SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetStreakCardsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            StreakCardsViewModel streakCardsViewModel = GenerateModel(user);
            return PartialView("_StreakCardsMainPartial", streakCardsViewModel);
        }

        private StreakCardsViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            bool canPlay = (DateTime.UtcNow - settings.LastStreakCardsPlayDate).TotalHours >= 8;
            GroupPower groupPower = _groupsRepository.GetGroupPower(user);

            List<StreakCard> streakCards = GetStreakCards(user, canPlay);
            if ((DateTime.UtcNow - settings.LastDailyStreakCardsPlayDate).TotalHours >= 48 && settings.StreakCardsNoExpiryDate <= DateTime.UtcNow)
            {
                settings.StreakCardsStreak = 0;
            }
            int streak = settings.StreakCardsStreak;
            int totalEarned = Math.Min(100000, streakCards.Sum(x => x.Points) * streak);

            StreakCardsViewModel streakCardsViewModel = new StreakCardsViewModel
            {
                Title = "Streak Cards",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                StreakCards = streakCards,
                Streak = streak,
                TotalEarned = totalEarned,
                HasBetterLuck = groupPower == GroupPower.BetterStreakCardsLuck,
                CanPlayStreakCards = streakCards.FindAll(x => !x.IsHidden).Count < 3
            };
            return streakCardsViewModel;
        }

        private List<StreakCard> GetStreakCards(User user, bool newGame)
        {
            List<StreakCard> streakCards = _streakCardsRepository.GetStreakCards(user);
            if (streakCards.Count > 3)
            {
                _streakCardsRepository.DeleteStreakCards(user);
            }
            if (streakCards.Count >= 3 && newGame)
            {
                streakCards = new List<StreakCard>();
            }
            for (int i = 0; i < 10; i++)
            {
                StreakCard streakCard = streakCards.Find(x => x.Position == i);
                if (streakCard == null)
                {
                    streakCards.Add(new StreakCard
                    {
                        Position = i
                    });
                }
            }

            return streakCards.OrderBy(x => x.Position).ToList();
        }

        private void AddAccomplishments(User user, int streak, List<Accomplishment> accomplishments)
        {
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCardsWin))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCardsWin);
            }
            if (streak >= 5)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCards5DayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCards5DayStreak);
                }
            }
            if (streak >= 10)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCards10DayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCards10DayStreak);
                }
            }
            if (streak >= 25)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCards25DayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCards25DayStreak);
                }
            }
            if (streak >= 50)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCards50DayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCards50DayStreak);
                }
            }
            if (streak >= 100)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StreakCards100DayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StreakCards100DayStreak);
                }
            }
        }
    }
}
