﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class AttentionHallController : Controller
    {
        private ILoginRepository _loginRepository;
        private IAttentionHallRepository _attentionHallRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IUserRepository _userRepository;
        private IBlockedRepository _blockedRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IJobsRepository _jobsRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private INotificationsRepository _notificationsRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public AttentionHallController(ILoginRepository loginRepository, IAttentionHallRepository attentionHallRepository, IItemsRepository itemsRepository,
        ISettingsRepository settingsRepository, IUserRepository userRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IJobsRepository jobsRepository, IMatchHistoryRepository matchHistoryRepository, IBlockedRepository blockedRepository, INotificationsRepository notificationsRepository,
        IAdminHistoryRepository adminHistoryRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _attentionHallRepository = attentionHallRepository;
            _settingsRepository = settingsRepository;
            _userRepository = userRepository;
            _blockedRepository = blockedRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _jobsRepository = jobsRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _notificationsRepository = notificationsRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public IActionResult Index(int id, int page, AttentionHallPostsFilter? filter, AttentionHallPostType? type)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"AttentionHall?id={id}&page={page}&filter={(filter.HasValue ? filter : AttentionHallPostsFilter.Popular)}&type={(type.HasValue ? type : AttentionHallPostType.None)}" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (filter.HasValue)
            {
                if (settings.AttentionHallPostsFilter != filter)
                {
                    settings.AttentionHallPostsFilter = filter.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.AttentionHallPostsFilter), settings.AttentionHallPostsFilter.ToString());
                }
            }
            else
            {
                filter = settings.AttentionHallPostsFilter;
            }
            if (type.HasValue)
            {
                if (settings.AttentionHallPostsTypeFilter != type)
                {
                    settings.AttentionHallPostsTypeFilter = type.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.AttentionHallPostsTypeFilter), settings.AttentionHallPostsTypeFilter.ToString());
                }
            }
            else
            {
                type = settings.AttentionHallPostsTypeFilter;
            }

            AttentionHallViewModel attentionHallViewModel = GenerateModel(user, id, page, false, filter.Value, type.Value);
            attentionHallViewModel.UpdateViewData(ViewData);
            return View(attentionHallViewModel);
        }

        public IActionResult Create(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"AttentionHall/Create" });
            }

            AttentionHallViewModel attentionHallViewModel = GenerateModel(user, id, 0, true);
            attentionHallViewModel.UpdateViewData(ViewData);
            return View(attentionHallViewModel);
        }

        [HttpPost]
        public IActionResult Vote(int id, AttentionHallPointType vote)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
            if (attentionHallPost == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This post does not exist." });
            }

            if (_attentionHallRepository.SetAttentionHallPoint(user, attentionHallPost, vote))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    if (attentionHallPost.Points == 4 && vote == AttentionHallPointType.Positive && user.Username != attentionHallPost.Username)
                    {
                        User postUser = _userRepository.GetUser(attentionHallPost.Username);
                        if (postUser != null)
                        {
                            _notificationsRepository.AddNotification(postUser, "Your Attention Hall post received 5 votes!", NotificationLocation.AttentionHall, $"/AttentionHall?id={attentionHallPost.Id}", string.Empty, domain);
                        }
                    }
                    if (attentionHallPost.Points == 19 && vote == AttentionHallPointType.Positive)
                    {
                        User postUser = _userRepository.GetUser(attentionHallPost.Username);
                        if (postUser != null)
                        {
                            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(postUser, false);
                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.PopularAttentionHallPost))
                            {
                                _accomplishmentsRepository.AddAccomplishment(postUser, AccomplishmentId.PopularAttentionHallPost, false, domain);
                            }
                        }
                    }
                });
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(string title, string post, int editId, AttentionHallPostType postType, Guid selectedAccomplishment, Guid selectedItem, string selectedMatchTag, AttentionHallFlair flair)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(title))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to write a title for your post." });
            }

            if (title.Length > 50)
            {
                return Json(new Status { Success = false, ErrorMessage = "You post title is too long." });
            }

            if (string.IsNullOrEmpty(post))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to write a post." });
            }

            if (post.Length > 2500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your post cannot be more than {Helper.FormatNumber(2500)} characters." });
            }

            AttentionHallPost attentionHallPost = null;
            if (editId > 0)
            {
                attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, editId);
                if (attentionHallPost != null && attentionHallPost.Username != user.Username)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot edit this Attention Hall post." });
                }
            }

            post = HttpUtility.HtmlEncode(Regex.Replace(post, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine));

            if (attentionHallPost == null)
            {
                if (postType == AttentionHallPostType.None)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You need to choose what your post is about." });
                }

                Settings settings = _settingsRepository.GetSettings(user);

                int totalMinutesSinceLastPost = (int)(DateTime.UtcNow - settings.LastAttentionHallPostDate).TotalMinutes;
                if (totalMinutesSinceLastPost < 5)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You've made a post too recently. Try again in {5 - totalMinutesSinceLastPost} minute{(5 - totalMinutesSinceLastPost == 1 ? "" : "s")}." });
                }

                Accomplishment accomplishment = null;
                Item item = null;
                MatchTag matchTag = null;
                if (postType == AttentionHallPostType.Accomplishment)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user).FindAll(x => x.Category > AccomplishmentCategory.Orange);
                    accomplishment = accomplishments.Find(x => x.SelectionId == selectedAccomplishment);
                    if (accomplishment == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You didn't choose an accomplishment." });
                    }
                }
                else if (postType == AttentionHallPostType.Item)
                {
                    List<Item> items = _itemsRepository.GetItems(user);
                    item = items.Find(x => x.SelectionId == selectedItem);
                    if (item == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You didn't choose an item." });
                    }
                }
                else if (postType == AttentionHallPostType.MatchTag)
                {
                    List<MatchTag> matchTags = _matchHistoryRepository.GetMatchTags(user);
                    matchTag = matchTags.Find(x => x.TagUsername == selectedMatchTag);
                    if (matchTag == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You didn't choose a Match Tag." });
                    }
                }

                if (_attentionHallRepository.AddAttentionHallPost(user, title, post, accomplishment, item, matchTag, Guid.Empty, flair, out attentionHallPost))
                {
                    settings.LastAttentionHallPostDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastAttentionHallPostDate), settings.LastAttentionHallPostDate.ToString());

                    string domain = Helper.GetDomain();
                    if (postType == AttentionHallPostType.MatchTag)
                    {
                        Task.Run(() =>
                        {
                            User matchTagUser = _userRepository.GetUser(matchTag.TagUsername);
                            if (matchTagUser != null)
                            {
                                _notificationsRepository.AddNotification(matchTagUser, $"@{user.Username} made an Attention Hall post about your Match Tag!", NotificationLocation.AttentionHall, $"/AttentionHall?id={attentionHallPost.Id}", domain: domain);
                            }
                        });
                    }

                    Task.Run(() =>
                    {
                        Helper.ExtractUserReply(post, out List<string> userMentions);
                        foreach (string username in userMentions)
                        {
                            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                            {
                                continue;
                            }
                            User userMention = _userRepository.GetUser(username);
                            if (userMention != null)
                            {
                                Settings userMentionSettings = _settingsRepository.GetSettings(userMention, false);
                                if (userMentionSettings.AllowUserMentionNotifications)
                                {
                                    _notificationsRepository.AddNotification(userMention, $"@{user.Username} mentioned you in their Attention Hall post!", NotificationLocation.AttentionHall, $"/AttentionHall?id={attentionHallPost.Id}", domain: domain);
                                }
                            }
                        }
                    });

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstAttentionHallPost))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstAttentionHallPost);
                    }

                    return Json(new Status { Success = true, Id = attentionHallPost.Id.ToString() });
                }
            }
            else
            {
                attentionHallPost.Title = title;
                attentionHallPost.Content = post;

                if (_attentionHallRepository.UpdateAttentionHallPost(attentionHallPost))
                {
                    return Json(new Status { Success = true, Id = attentionHallPost.Id.ToString() });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your post could not be {(attentionHallPost == null ? "created" : "edited")}. Please try again." });
        }

        [HttpPost]
        public IActionResult RemovePost(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
            if (attentionHallPost == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This post does not exist." });
            }

            if (attentionHallPost.Username != user.Username && user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to remove this post." });
            }

            if (_attentionHallRepository.DeleteAttentionHallPost(attentionHallPost))
            {
                if (attentionHallPost.Username != user.Username)
                {
                    User postUser = _userRepository.GetUser(attentionHallPost.Username);
                    if (postUser != null)
                    {
                        _adminHistoryRepository.AddAdminHistory(postUser, user, $"Deleted Attention Hall post \"{attentionHallPost.Title}\".");
                    }
                }

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This post could not be removed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Comment(int id, string comment, Guid parentCommentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(comment))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your comment cannot be empty." });
            }

            if (comment.Length > 750)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your comment must be 750 characters or less." });
            }

            AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
            if (attentionHallPost == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This post doesn't exist." });
            }

            User postUser = null;
            if (attentionHallPost.Username != user.Username)
            {
                postUser = _userRepository.GetUser(attentionHallPost.Username);
                if (postUser != null)
                {
                    List<Blocked> blockedUsers = _blockedRepository.GetBlocked(postUser, false);
                    if (blockedUsers.Any(x => x.BlockedUser == user.Username))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You are not allowed to comment on this post." });
                    }
                }
            }

            AttentionHallComment parentAttentionHallComment = null;
            if (parentCommentId != Guid.Empty)
            {
                List<AttentionHallComment> attentionHallComments = _attentionHallRepository.GetAllAttentionHallComments(user, attentionHallPost, true);
                parentAttentionHallComment = attentionHallComments.Find(x => x.Id == parentCommentId);
                if (parentAttentionHallComment?.Level >= 6)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You are not allowed to reply to this comment." });
                }
            }

            User parentCommentUser = null;
            if (parentAttentionHallComment != null && user.Username != parentAttentionHallComment.Username)
            {
                parentCommentUser = _userRepository.GetUser(parentAttentionHallComment.Username);
            }

            if (parentCommentUser != null && _blockedRepository.HasBlockedUser(parentCommentUser, user, false))
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to reply to this comment." });
            }

            comment = Regex.Replace(comment, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            if (_attentionHallRepository.AddAttentionHallComment(user, parentAttentionHallComment, attentionHallPost, HttpUtility.HtmlEncode(comment), out AttentionHallComment attentionHallComment))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    if (postUser != null && user.Username != attentionHallPost.Username && parentAttentionHallComment == null)
                    {
                        Settings postUserSettings = _settingsRepository.GetSettings(postUser, false);
                        if (postUserSettings.ReceiveAttentionHallPostsCommentsNotifications)
                        {
                            _notificationsRepository.AddNotification(postUser, $"@{user.Username} commented on your Attention Hall post!", NotificationLocation.AttentionHall, $"/AttentionHall?id={attentionHallPost.Id}#{attentionHallComment.Id}", domain: domain);
                        }
                    }

                    string url = $"/AttentionHall?id={attentionHallPost.Id}#{attentionHallComment.Id}";

                    if (parentCommentUser != null)
                    {
                        _notificationsRepository.AddNotification(parentCommentUser, $"@{user.Username} replied to your comment on an Attention Hall post!", NotificationLocation.AttentionHall, url, domain: domain);
                    }

                    Helper.ExtractUserReply(comment, out List<string> userMentions);
                    foreach (string username in userMentions)
                    {
                        if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                        if (parentCommentUser != null && parentCommentUser.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                        User userMention = _userRepository.GetUser(username);
                        if (userMention != null)
                        {
                            Settings userMentionSettings = _settingsRepository.GetSettings(userMention, false);
                            if (userMentionSettings.AllowUserMentionNotifications)
                            {
                                if (!_blockedRepository.HasBlockedUser(userMention, user, false))
                                {
                                    _notificationsRepository.AddNotification(userMention, $"@{user.Username} mentioned you in their comment on an Attention Hall post!", NotificationLocation.AttentionHall, url, domain: domain);
                                }
                            }
                        }
                    }
                });

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your comment could not be posted." });
        }

        [HttpPost]
        public IActionResult RemoveComment(int id, Guid commentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
            if (attentionHallPost == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This post doesn't exist." });
            }

            List<AttentionHallComment> attentionHallComments = _attentionHallRepository.GetAllAttentionHallComments(user, attentionHallPost);
            AttentionHallComment attentionHallComment = attentionHallComments.Find(x => x.Id == commentId);
            if (attentionHallComment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This comment doesn't exist." });
            }

            if (attentionHallComment.Username != user.Username && user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to remove this comment." });
            }

            if (_attentionHallRepository.DeleteAttentionHallComment(attentionHallComment))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your comment could not be removed." });
        }

        [HttpPost]
        public IActionResult LikeComment(int id, Guid commentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
            if (attentionHallPost == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This post doesn't exist." });
            }

            List<AttentionHallComment> attentionHallComments = _attentionHallRepository.GetAllAttentionHallComments(user, attentionHallPost);
            AttentionHallComment attentionHallComment = attentionHallComments.Find(x => x.Id == commentId);
            if (attentionHallComment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This comment doesn't exist." });
            }

            if (_attentionHallRepository.SetAttentionHallCommentLike(user, attentionHallComment))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    if (attentionHallComment.TotalLikes == 4 && user.Username != attentionHallComment.Username)
                    {
                        User commentUser = _userRepository.GetUser(attentionHallComment.Username);
                        if (commentUser != null)
                        {
                            _notificationsRepository.AddNotification(commentUser, "Your Attention Hall comment received 5 likes!", NotificationLocation.AttentionHall, $"/AttentionHall?id={attentionHallPost.Id}#{attentionHallComment.Id}", string.Empty, domain);
                        }
                    }
                });
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The comment could not be liked." });
        }

        [HttpGet]
        public IActionResult GetAttentionHallMainPartialView(int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            AttentionHallViewModel attentionHallViewModel = GenerateModel(user, 0, page, false, settings.AttentionHallPostsFilter, settings.AttentionHallPostsTypeFilter);
            return PartialView("_AttentionHallAllPartial", attentionHallViewModel);
        }

        [HttpGet]
        public IActionResult GetAttentionHallPostPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            AttentionHallViewModel attentionHallViewModel = GenerateModel(user, id, 0, false);
            return PartialView("_AttentionHallPostPartial", attentionHallViewModel);
        }

        private AttentionHallViewModel GenerateModel(User user, int id, int page, bool isCreate, AttentionHallPostsFilter filter = AttentionHallPostsFilter.Popular, AttentionHallPostType type = AttentionHallPostType.None)
        {
            AttentionHallViewModel attentionHallViewModel = new AttentionHallViewModel
            {
                Title = "Attention Hall",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.AttentionHall,
                CurrentPage = page
            };

            if (isCreate)
            {
                if (id > 0)
                {
                    attentionHallViewModel.AttentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
                    attentionHallViewModel.AttentionHallPost.Content = HttpUtility.HtmlDecode(attentionHallViewModel.AttentionHallPost.Content);
                    if (attentionHallViewModel.AttentionHallPost != null && attentionHallViewModel.AttentionHallPost.Username != user.Username)
                    {
                        attentionHallViewModel.AttentionHallPost = null;
                    }
                }
                attentionHallViewModel.UserAccomplishments = _accomplishmentsRepository.GetAccomplishments(user).FindAll(x => x.Category > AccomplishmentCategory.Orange).OrderByDescending(x => x.Category).ThenBy(x => x.Name).ToList();
                attentionHallViewModel.UserItems = _itemsRepository.GetItems(user).OrderBy(x => x.Category).ThenByDescending(x => x.Points).ToList();
                attentionHallViewModel.UserMatchTags = _matchHistoryRepository.GetMatchTags(user);

                return attentionHallViewModel;
            }

            if (id > 0)
            {
                AttentionHallPost attentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, id);
                if (attentionHallPost == null)
                {
                    Messages.AddSnack("This Attention Hall post no longer exists.", true);
                }
                else
                {
                    attentionHallViewModel.AttentionHallPost = attentionHallPost;
                    attentionHallViewModel.AttentionHallComments = _attentionHallRepository.GetAllAttentionHallComments(user, attentionHallPost, true);

                    if (attentionHallPost.JobSubmissionId != Guid.Empty)
                    {
                        JobSubmission jobSubmission = _jobsRepository.GetJobSubmission(attentionHallPost.JobSubmissionId);
                        attentionHallViewModel.JobState = jobSubmission?.State;
                    }
                }
            }
            if (attentionHallViewModel.AttentionHallPost == null)
            {
                List<AttentionHallPost> attentionHallPosts = _attentionHallRepository.GetAllAttentionHallPosts(user, filter, type);
                int resultsPerPage = 10;
                attentionHallViewModel.TotalPages = (int)Math.Ceiling((double)attentionHallPosts.Count / resultsPerPage);
                if (page < 0 || page >= attentionHallViewModel.TotalPages)
                {
                    page = 0;
                }
                attentionHallViewModel.AttentionHallPosts = attentionHallPosts.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                attentionHallViewModel.CurrentFilter = filter;
                attentionHallViewModel.TypeFilter = type;
            }

            return attentionHallViewModel;
        }
    }
}
