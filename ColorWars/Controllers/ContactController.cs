﻿using System;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ContactController : Controller
    {
        private ILoginRepository _loginRepository;
        private IContactsRepository _contactsRepository;
        private ICaptchaRepository _captchaRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public ContactController(ILoginRepository loginRepository, IContactsRepository contactsRepository, 
        ICaptchaRepository captchaRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _contactsRepository = contactsRepository;
            _captchaRepository = captchaRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();

            ContactViewModel contactViewModel = GenerateModel(user);
            contactViewModel.UpdateViewData(ViewData);
            return View(contactViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Contact(ContactCategory category, string emailAddress, string isReplyViaMessages, string message, int mathAnswer)
        {
            User user = _loginRepository.AuthenticateUser();

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(message))
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your email address and message cannot be empty." });
            }
            if (message.Length > 2500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your message is too long." });
            }

            if (category == ContactCategory.None)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must select a reason for contacting us." });
            }

            if (user == null && !_captchaRepository.VerifyCaptcha("contact", mathAnswer))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your answer to the math problem isn't quite right." });
            }

            bool isReplyViaMessagesOn = isReplyViaMessages == "on";

            _contactsRepository.AddContact(user, emailAddress, message, category, isReplyViaMessagesOn);

            Messages.AddSnack("Thank you for contacting us! If needed, we will get back to you as soon as possible.");

            return Json(new Status { Success = true });
        }

        private ContactViewModel GenerateModel(User user)
        {
            Captcha captcha = _captchaRepository.GenerateCaptcha("contact");

            ContactViewModel contactViewModel = new ContactViewModel
            {
                Title = "Contact",
                User = user,
                Number1 = captcha.Number1,
                Number2 = captcha.Number2
            };

            return contactViewModel;
        }
    }
}
