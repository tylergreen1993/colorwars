﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StockMarketController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IStocksRepository _stocksRepository;
        private IGroupsRepository _groupsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISettingsRepository _settingsRepository;
        private ISoundsRepository _soundsRepository;

        public StockMarketController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IStocksRepository stocksRepository,
        IGroupsRepository groupsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _stocksRepository = stocksRepository;
            _groupsRepository = groupsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(StockTicker id, StockSort? sort, string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "StockMarket" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Portfolio", "StockMarket");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (sort.HasValue)
            {
                if (settings.StockMarketSort != sort)
                {
                    settings.StockMarketSort = sort.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.StockMarketSort), settings.StockMarketSort.ToString());
                }
            }
            else
            {
                sort = settings.StockMarketSort;
            }

            StockMarketViewModel stockMarketViewModel = GenerateModel(user, id, false, sort.Value);
            stockMarketViewModel.UpdateViewData(ViewData);
            return View(stockMarketViewModel);
        }

        public IActionResult Portfolio(PortfolioSort? sort)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "StockMarket/Portfolio" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (sort.HasValue)
            {
                if (settings.StockMarketPortfolioSort != sort)
                {
                    settings.StockMarketPortfolioSort = sort.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.StockMarketPortfolioSort), settings.StockMarketPortfolioSort.ToString());
                }
            }
            else
            {
                sort = settings.StockMarketPortfolioSort;
            }

            StockMarketViewModel stockMarketViewModel = GenerateModel(user, StockTicker.None, true, portfolioSort: sort.Value);
            stockMarketViewModel.UpdateViewData(ViewData);
            return View(stockMarketViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Buy(StockTicker stockId, int shares)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (shares < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must buy at least one share." });
            }

            List<Stock> allStocks = _stocksRepository.GetAllStocks();
            Stock stock = allStocks.Find(x => x.Id == stockId);
            if (stock == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This stock does not exist." });
            }

            GroupPower groupPower = _groupsRepository.GetGroupPower(user);
            bool hasStockDiscount = groupPower == GroupPower.StockDiscount;
            int totalCost = (int)Math.Round(Math.Round(stock.CurrentCost) * shares * (hasStockDiscount ? 0.95 : 1));
            if (user.Points < totalCost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Stock> userTransactions = _stocksRepository.GetStocks(user).FindAll(x => x.PurchaseCost > 1 && (DateTime.UtcNow - x.PurchaseDate).TotalDays < 1);
            int totalShares = userTransactions.Sum(x => x.UserAmount);
            if (totalShares + shares > Helper.MaxSharesPerDay())
            {
                return Json(new Status { Success = false, ErrorMessage = $"You are only allowed to buy {Helper.MaxSharesPerDay()} total shares per day. You can buy {Math.Max(0, Helper.MaxSharesPerDay() - totalShares)} more today." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (totalShares + shares == Helper.MaxSharesPerDay())
            {
                settings.PurchaseMoreStocksDate = (userTransactions.FirstOrDefault()?.PurchaseDate ?? DateTime.UtcNow).AddDays(1);
                _settingsRepository.SetSetting(user, nameof(settings.PurchaseMoreStocksDate), settings.PurchaseMoreStocksDate.ToString());
            }

            if (_pointsRepository.SubtractPoints(user, totalCost))
            {
                _stocksRepository.AddStock(user, stock, shares, hasStockDiscount, false);
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StockBuy))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StockBuy);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully bought {Helper.FormatNumber(shares)} share{(shares == 1 ? "" : "s")} of {stock.Name}!", ButtonText = "Head to your Portfolio!", ButtonUrl = "/StockMarket/Portfolio", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Sell(Guid selectionId, StockTicker stockId, int shares, PortfolioSort sort)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            double currentCost;
            int purchaseCost;
            int earnings;
            string stockName;
            List<Stock> userStocks = _stocksRepository.GetStocks(user);
            if (sort == PortfolioSort.Date)
            {
                Stock stock = userStocks.Find(x => x.SelectionId == selectionId);
                if (stock == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This stock does not exist." });
                }

                int hoursSincePurchased = (int)(DateTime.UtcNow - stock.PurchaseDate).TotalHours;
                if (hoursSincePurchased < 24)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You must wait {24 - hoursSincePurchased} hour{(24 - hoursSincePurchased == 1 ? "" : "s")} before you can sell any of these shares." });
                }

                if (shares <= 0 || shares > stock.UserAmount)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You can't sell that many shares." });
                }

                earnings = shares * (int)Math.Round(stock.CurrentCost);
                if (hoursSincePurchased < 30 * 24)
                {
                    earnings = (int)Math.Round(earnings * 0.85);
                }

                if (_stocksRepository.RemoveStock(user, stock, shares, out bool isCrashed))
                {
                    _pointsRepository.AddPoints(user, earnings);
                }

                currentCost = stock.CurrentCost;
                purchaseCost = stock.PurchaseCost;
                stockName = stock.Name;
            }
            else
            {
                userStocks = userStocks.FindAll(x => x.Id == stockId);
                if (userStocks.Count == 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This stock does not exist." });
                }

                userStocks = userStocks.FindAll(x => (DateTime.UtcNow - x.PurchaseDate).TotalHours >= 24);
                if (shares <= 0 || userStocks.Sum(x => x.UserAmount) < shares)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot sell this many shares. Shares can only be sold if you've owned them for at least 24 hours." });
                }

                userStocks = userStocks.OrderBy(x => x.PurchaseDate).ToList();
                earnings = 0;
                int totalShares = 0;
                currentCost = 0;
                foreach (Stock userStock in userStocks)
                {
                    currentCost = userStock.CurrentCost;
                    if ((DateTime.UtcNow - userStock.PurchaseDate).TotalDays < 30)
                    {
                        currentCost *= 0.85;
                    }
                    if (userStock.UserAmount + totalShares <= shares)
                    {
                        totalShares += userStock.UserAmount;
                        if (_stocksRepository.RemoveStock(user, userStock, userStock.UserAmount, out bool isCrashed))
                        {
                            earnings += userStock.UserAmount * (int)Math.Round(currentCost);
                            if (isCrashed)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        int sharesNeeded = shares - totalShares;
                        totalShares = shares;
                        if (_stocksRepository.RemoveStock(user, userStock, sharesNeeded, out bool isCrashed))
                        {
                            earnings += sharesNeeded * (int)Math.Round(currentCost);
                            if (isCrashed)
                            {
                                break;
                            }
                        }
                        break;
                    }
                }

                _pointsRepository.AddPoints(user, earnings);

                currentCost = userStocks.First().CurrentCost;
                purchaseCost = (int)Math.Round(userStocks.Average(x => x.PurchaseCost));
                stockName = userStocks.First().Name;
            }

            if (currentCost > purchaseCost)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StockSellProfit))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StockSellProfit);
                }
                if (earnings - (shares * purchaseCost) >= 50000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigStockEarnings))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigStockEarnings);
                    }
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully sold {Helper.FormatNumber(shares)} share{(shares == 1 ? "" : "s")} of {stockName} for {Helper.GetFormattedPoints(earnings)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CollectDividends()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastDividendsCollect = (int)(DateTime.UtcNow - settings.DividendsCollectDate).TotalHours;
            if (hoursSinceLastDividendsCollect < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You already collected your daily dividends today. Try again in {24 - hoursSinceLastDividendsCollect} hour{(24 - hoursSinceLastDividendsCollect == 1 ? "" : "s")}." });
            }

            List<Stock> portfolio = _stocksRepository.GetStocks(user);
            if (portfolio.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You currently own no shares and have no dividends to collect." });
            }

            int collectableDividends = portfolio.Sum(x => (int)Math.Round(x.UserAmount * (x.CurrentCost * x.DividendsPercent / 100)));
            if (collectableDividends == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot collect {Helper.GetFormattedPoints(0)}." });
            }

            if (_pointsRepository.AddPoints(user, collectableDividends))
            {
                settings.DividendsCollectDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.DividendsCollectDate), settings.DividendsCollectDate.ToString());
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully collected {Helper.GetFormattedPoints(collectableDividends)} for your daily dividends!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetStockMarketPartialView(StockTicker id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            StockMarketViewModel stockMarketViewModel = GenerateModel(user, id, false, settings.StockMarketSort);

            if (id == StockTicker.None)
            {
                return PartialView("_StockMarketMainPartial", stockMarketViewModel);
            }
            return PartialView("_StockMarketIdPartial", stockMarketViewModel);
        }

        [HttpGet]
        public IActionResult GetPortfolioPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            StockMarketViewModel stockMarketViewModel = GenerateModel(user, StockTicker.None, true, portfolioSort: settings.StockMarketPortfolioSort);

            return PartialView("_StockMarketPortfolioPartial", stockMarketViewModel);
        }


        private StockMarketViewModel GenerateModel(User user, StockTicker id, bool isPortfolio, StockSort sort = StockSort.Name, PortfolioSort portfolioSort = PortfolioSort.Stock)
        {
            GroupPower groupPower = _groupsRepository.GetGroupPower(user);
            StockMarketViewModel stockMarketViewModel = new StockMarketViewModel
            {
                Title = "Stock Market",
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.StockMarket,
                User = user,
                Sort = sort,
                HasStockDiscount = groupPower == GroupPower.StockDiscount
            };

            List<Stock> allStocks = _stocksRepository.GetAllStocks();
            if (id == StockTicker.None)
            {
                if (isPortfolio)
                {
                    stockMarketViewModel.Portfolio = _stocksRepository.GetStocks(user);
                    stockMarketViewModel.TotalPortfolioValue = stockMarketViewModel.Portfolio.Sum(x => x.UserAmount * (int)Math.Round(x.CurrentCost));
                    if (stockMarketViewModel.TotalPortfolioValue > 0)
                    {
                        stockMarketViewModel.PortfolioSort = portfolioSort;
                        stockMarketViewModel.OriginalTotalPortfolioValue = stockMarketViewModel.Portfolio.Sum(x => x.UserAmount * x.PurchaseCost);
                        stockMarketViewModel.PortfolioValuePercentChanged = Math.Round((double)stockMarketViewModel.TotalPortfolioValue / stockMarketViewModel.OriginalTotalPortfolioValue * 100 - 100, 2);
                        stockMarketViewModel.TotalPortfolioChanged = stockMarketViewModel.TotalPortfolioValue - stockMarketViewModel.OriginalTotalPortfolioValue;
                        stockMarketViewModel.CollectableDividends = stockMarketViewModel.Portfolio.Sum(x => (int)Math.Round(x.UserAmount * (x.CurrentCost * x.DividendsPercent / 100)));
                        if (stockMarketViewModel.CollectableDividends > 0)
                        {
                            Settings settings = _settingsRepository.GetSettings(user);
                            stockMarketViewModel.CanCollectDividends = (DateTime.UtcNow - settings.DividendsCollectDate).TotalHours >= 24;
                        }
                        if (portfolioSort == PortfolioSort.Stock)
                        {
                            List<Stock> stocks = new List<Stock>();
                            IEnumerable<IGrouping<StockTicker, Stock>> groupedStocks = stockMarketViewModel.Portfolio.GroupBy(x => x.Id);
                            foreach (IGrouping<StockTicker, Stock> groupedStock in groupedStocks)
                            {
                                stocks.Add(new Stock
                                {
                                    Id = groupedStock.Key,
                                    Name = groupedStock.First().Name,
                                    CurrentCost = groupedStock.First().CurrentCost,
                                    PurchaseCost = (int)Math.Round(groupedStock.Average(x => x.PurchaseCost)),
                                    TotalAmount = groupedStock.Sum(x => x.UserAmount),
                                    UserAmount = groupedStock.Where(x => (DateTime.UtcNow - x.PurchaseDate).TotalHours >= 24).Sum(x => x.UserAmount)
                                });
                            }
                            stockMarketViewModel.Portfolio = stocks;
                        }
                    }
                }
                else
                {
                    stockMarketViewModel.Market = SortStocks(allStocks, sort);
                }
            }
            else
            {
                stockMarketViewModel.Stock = allStocks.Find(x => x.Id == id);
                stockMarketViewModel.RecentTransactions = _stocksRepository.GetStockHistory(id).Take(100).ToList();
                if (stockMarketViewModel.RecentTransactions.Count > 0)
                {
                    stockMarketViewModel.StockPercentChanged = Math.Round(Math.Round(stockMarketViewModel.Stock.CurrentCost) / stockMarketViewModel.RecentTransactions.Last().UserCost * 100 - 100, 2);
                }
                List<Stock> userTransactions = _stocksRepository.GetStocks(user).FindAll(x => x.PurchaseCost > 1 && (DateTime.UtcNow - x.PurchaseDate).TotalDays < 1);
                int totalShares = userTransactions.Sum(x => x.UserAmount);
                stockMarketViewModel.CanBuyShares = totalShares < Helper.MaxSharesPerDay();
            }

            return stockMarketViewModel;
        }

        private List<Stock> SortStocks(List<Stock> stocks, StockSort sort)
        {
            if (sort == StockSort.Lowest)
            {
                return stocks.OrderBy(x => x.CurrentCost).ToList();
            }
            if (sort == StockSort.Highest)
            {
                return stocks.OrderByDescending(x => x.CurrentCost).ToList();
            }
            if (sort == StockSort.HighestDividends)
            {
                return stocks.OrderByDescending(x => x.DividendsPercent).ToList();
            }

            return stocks;
        }
    }
}
