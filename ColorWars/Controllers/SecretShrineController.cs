﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SecretShrineController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public SecretShrineController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SecretShrine" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SecretShrineFound))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SecretShrineFound);
            }

            SecretShrineViewModel secretShrineViewModel = GenerateModel(user);
            secretShrineViewModel.UpdateViewData(ViewData);
            return View(secretShrineViewModel);
        }

        [HttpGet]
        public IActionResult GetSecretShrinePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            SecretShrineViewModel secretShrineViewModel = GenerateModel(user);
            return PartialView("_SecretShrineMainPartial", secretShrineViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastShrineItemLeft = (int)(DateTime.UtcNow - settings.LastSecretShrineItemGivenDate).TotalHours;
            if (hoursSinceLastShrineItemLeft < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've gave an item to the Secret Shrine too recently. Come back in {24 - hoursSinceLastShrineItemLeft} hour{(24 - hoursSinceLastShrineItemLeft == 1 ? "" : "s")}." });
            }

            Item item = _itemsRepository.GetItems(user, true).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            _itemsRepository.RemoveItem(user, item);

            int odds = 4;
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds = 5;
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    odds = 3;
                }
            }

            settings.LastSecretShrineItemGivenDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastSecretShrineItemGivenDate), settings.LastSecretShrineItemGivenDate.ToString());

            Random rnd = new Random();
            if (rnd.Next(odds) == 1)
            {
                return Json(new Status { Success = true, InfoMessage = $"You gave the item \"{item.Name}\" but the Secret Shrine had nothing to give you in return. Better luck next time!" });
            }

            List<Item> allItems = _itemsRepository.GetAllItems().FindAll(x => x.Points <= Math.Max(item.Points * 1.75, 10) && x.Points >= item.Points / 4 && x.Id != item.Id);
            Item chosenItem = allItems[rnd.Next(allItems.Count)];

            _itemsRepository.AddItem(user, chosenItem);

            if (chosenItem.Points > item.Points)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SecretShrineBetterItem))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SecretShrineBetterItem);
                }
            }

            return Json(new Status { Success = true, SuccessMessage = $"The Secret Shrine took your item and successfully gave you the item \"{chosenItem.Name}\" in return!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
        }

        private SecretShrineViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            SecretShrineViewModel secretShrineViewModel = new SecretShrineViewModel
            {
                Title = "Secret Shrine",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.SecretShrine,
                CanLeaveItem = (DateTime.UtcNow - settings.LastSecretShrineItemGivenDate).TotalDays >= 1,
                LeaveItemDate = settings.LastSecretShrineItemGivenDate
            };

            if (secretShrineViewModel.CanLeaveItem)
            {
                secretShrineViewModel.Items = _itemsRepository.GetItems(user, true);
            }

            return secretShrineViewModel;
        }
    }
}
