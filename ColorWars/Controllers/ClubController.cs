﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ClubController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IClubRepository _clubRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ClubController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IClubRepository clubRepository,
        IItemsRepository itemsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _clubRepository = clubRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Club" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Incubator", "Club");
            }

            ClubViewModel clubViewModel = GenerateModel(user, false);
            clubViewModel.UpdateViewData(ViewData);
            return View(clubViewModel);
        }

        public IActionResult Incubator()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Club/Incubator" });
            }

            ClubViewModel clubViewModel = GenerateModel(user, true);
            if (!clubViewModel.IsMember)
            {
                return RedirectToAction("Index", "Club");
            }
            clubViewModel.UpdateViewData(ViewData);
            return View(clubViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetMembership(int months)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            ClubMembership clubMembership = _clubRepository.GetCurrentMembership(user);
            if (clubMembership != null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You already have an active {Resources.SiteName} Club membership." });
            }

            bool isMembershipPurchaseSuccessful = false;

            if (months == 1)
            {
                if (user.Points < 15000)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to choose this membership option." });
                }

                if (_pointsRepository.SubtractPoints(user, 15000))
                {
                    isMembershipPurchaseSuccessful = true;
                }
            }
            else if (months == 12)
            {
                if (user.Points < 150000)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to choose this membership option." });
                }

                if (_pointsRepository.SubtractPoints(user, 150000))
                {
                    isMembershipPurchaseSuccessful = true;
                }
            }

            if (isMembershipPurchaseSuccessful)
            {
                _clubRepository.AddClubMembership(user, months, DateTime.UtcNow);

                Settings settings = _settingsRepository.GetSettings(user);
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully joined the {Resources.SiteName} Club for {months} month{(months == 1 ? "" : "s")}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You didn't choose a valid membership option." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClaimItems()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _clubRepository.GetUnclaimedItems(user);
            if (items.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have no unclaimed items." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_clubRepository.GetCurrentMembership(user) == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have an active {Resources.SiteName} Club membership." });
            }

            if (_clubRepository.ClaimItems(user))
            {
                foreach (Item item in items)
                {
                    _itemsRepository.AddItem(user, item);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully claimed the item{(items.Count == 1 ? $" \"{items.First().Name}\"" : "s")}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You were unable to claim the item{(items.Count == 1 ? $" \"{items.First().Name}\"" : "s")}." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetBonus()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastClubBoost = (int)(DateTime.UtcNow - settings.LastClubBonusDate).TotalHours;
            if (hoursSinceLastClubBoost < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've used your Daily Club Bonus too recently. Try again in {24 - hoursSinceLastClubBoost} hour{(24 - hoursSinceLastClubBoost == 1 ? "" : "s")}." });
            }

            if (settings.StadiumBonusExpiryDate > DateTime.UtcNow && settings.StadiumBonusPercent > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an active Stadium Bonus." });
            }

            if (_clubRepository.GetCurrentMembership(user) == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have an active {Resources.SiteName} Club membership." });
            }

            Random rnd = new Random();
            settings.StadiumBonusPercent = rnd.Next(9, 25) + 1;
            settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(10);
            settings.LastClubBonusDate = DateTime.UtcNow;

            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.LastClubBonusDate), settings.LastClubBonusDate.ToString());

            return Json(new Status { Success = true, SuccessMessage = $"You successfully gained a {settings.StadiumBonusPercent}% Stadium Bonus during CPU matches for the next 10 minutes!", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ExtendMembership(bool oneMonth)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<ClubMembership> activeClubMemberships = _clubRepository.GetAllClubMemberships(user).FindAll(x => x.EndDate > DateTime.UtcNow);
            if (activeClubMemberships.Count == 0)
            {
                return GetMembership(12);
            }

            if (activeClubMemberships.Count == 1)
            {
                if (oneMonth)
                {
                    if (user.Points < 15000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to choose this membership option." });
                    }

                    if (_pointsRepository.SubtractPoints(user, 15000))
                    {
                        _clubRepository.AddClubMembership(user, 1, activeClubMemberships.First().EndDate);

                        Settings settings = _settingsRepository.GetSettings(user);
                        string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                        return Json(new Status { Success = true, SuccessMessage = $"You successfully extended your {Resources.SiteName} Club membership by 1 more month!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
                    }
                }
                else
                {
                    if (user.Points < 135000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to choose this membership option." });
                    }

                    if (_pointsRepository.SubtractPoints(user, 135000))
                    {
                        _clubRepository.AddClubMembership(user, 11, activeClubMemberships.First().EndDate);

                        Settings settings = _settingsRepository.GetSettings(user);
                        string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                        return Json(new Status { Success = true, SuccessMessage = $"You successfully extended your {Resources.SiteName} Club membership by 11 more months!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
                    }
                }

                return Json(new Status { Success = false, ErrorMessage = "Your membership was not able to be extended. Please try again." });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You have more than 1 month of your {Resources.SiteName} Club membership remaining. It does not need to be extended at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Incubate(Guid enhancerId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (enhancerId == Guid.Empty)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't chosen an Enhancer to incubate." });
            }

            if (_clubRepository.GetIncubatorItem(user) != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are already incubating an Enhancer." });
            }

            Enhancer enhancer = _itemsRepository.GetEnhancers(user).Find(x => x.SelectionId == enhancerId);
            if (enhancer == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Enhancer." });
            }

            if (enhancer.TotalUses >= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can only incubate an Enhancer that has unlimited uses." });
            }

            if (_clubRepository.GetCurrentMembership(user) == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have an active {Resources.SiteName} Club membership." });
            }

            if (_clubRepository.AddIncubatorItem(user, enhancer))
            {
                _itemsRepository.RemoveItem(user, enhancer);

                Settings settings = _settingsRepository.GetSettings(user);
                settings.IncubationStartDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.IncubationStartDate), settings.IncubationStartDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully incubated your Enhancer!" });
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your Enhancer couldn't be incubated." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StopIncubator()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (_clubRepository.GetCurrentMembership(user) == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have an active {Resources.SiteName} Club membership." });
            }

            Item incubatedItem = _clubRepository.GetIncubatorItem(user);
            if (incubatedItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You currently aren't incubating an Enhancer." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int totalDaysSinceIncubation = (int)(DateTime.UtcNow - settings.IncubationStartDate).TotalDays;

            int uses = totalDaysSinceIncubation >= 3 ? Math.Min(2 * totalDaysSinceIncubation - 5, 5) : 0;

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count + (uses > 0 ? 1 : 0) >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your bag is too full to add {(uses > 0 ? "your Enhancer and its copy" : "another item")}." });
            }

            if (_clubRepository.DeleteIncubatorItem(incubatedItem))
            {
                _itemsRepository.AddItem(user, incubatedItem);
            }

            settings.IncubationStartDate = DateTime.MinValue;
            _settingsRepository.SetSetting(user, nameof(settings.IncubationStartDate), settings.IncubationStartDate.ToString());

            if (uses > 0)
            {
                Enhancer enhancerCopy = _itemsRepository.GetAllEnhancers(true).Find(x => x.ImageUrl == incubatedItem.ImageUrl && x.TotalUses > 0);
                if (enhancerCopy != null)
                {
                    enhancerCopy.Uses = uses;
                    _itemsRepository.AddItem(user, enhancerCopy);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.IncubatedItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.IncubatedItem);
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully finished the incubation and your item \"{incubatedItem.Name}\" and its copy were added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
                }
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully ended the incubation and your item \"{incubatedItem.Name}\" was added back to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
        }

        [HttpGet]
        public IActionResult GetClubPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ClubViewModel clubViewModel = GenerateModel(user, false);
            if (clubViewModel.IsMember)
            {
                return PartialView("_ClubMemberPartial", clubViewModel);
            }

            return PartialView("_ClubSignUpPartial", clubViewModel);
        }

        [HttpGet]
        public IActionResult GetClubIncubatorPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ClubViewModel clubViewModel = GenerateModel(user, true);
            if (!clubViewModel.IsMember)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You do not have a {Resources.SiteName} Club membership." });
            }

            return PartialView("_ClubIncubatorPartial", clubViewModel);
        }

        private ClubViewModel GenerateModel(User user, bool isIncubator)
        {
            ClubMembership clubMembership = _clubRepository.GetCurrentMembership(user);

            ClubViewModel clubViewModel = new ClubViewModel
            {
                Title = $"{Resources.SiteName} Club",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Club,
                IsMember = clubMembership != null
            };

            Settings settings;
            if (isIncubator)
            {
                settings = _settingsRepository.GetSettings(user);

                clubViewModel.Enhancers = _itemsRepository.GetEnhancers(user).FindAll(x => x.TotalUses < 0);
                clubViewModel.IncubatedItem = _clubRepository.GetIncubatorItem(user);
                clubViewModel.IncubationDays = (int)(DateTime.UtcNow - settings.IncubationStartDate).TotalDays;
            }
            else if (clubViewModel.IsMember)
            {
                settings = _settingsRepository.GetSettings(user);

                clubViewModel.Items = _clubRepository.GetUnclaimedItems(user);
                clubViewModel.NextItem = _clubRepository.GetNextItem(user);
                clubViewModel.ClubMemberships = _clubRepository.GetAllClubMemberships(user).FindAll(x => x.EndDate > DateTime.UtcNow);
                clubViewModel.MembershipEndDate = _clubRepository.GetMembershipEndDate(user);
                clubViewModel.CanUseBoost = (DateTime.UtcNow - settings.LastClubBonusDate).TotalHours >= 24;

                if (clubMembership.Month >= 6)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ClubSixMonths))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ClubSixMonths);
                    }
                    if (clubMembership.Month >= 12)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ClubTwelveMonths))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ClubTwelveMonths);
                        }
                    }
                }
            }
            else
            {
                Random rnd = new Random();
                clubViewModel.Items = _itemsRepository.GetAllItems(ItemCategory.All, null, true).FindAll(x => x.IsExclusive && x.Name.Contains("Club") && x.TotalUses == 1).GroupBy(x => x.Category).Select(x => x.First()).OrderBy(x => rnd.Next()).Take(4).ToList();
            }

            return clubViewModel;
        }
    }
}
