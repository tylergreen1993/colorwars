﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using PayPal.Api;
using Item = ColorWars.Models.Item;

namespace ColorWars.Controllers
{
    public class SponsorSocietyController : Controller
    {
        private ILoginRepository _loginRepository;
        private IDonorRepository _donorRepository;
        private IItemsRepository _itemsRepository;
        private IUserRepository _userRepository;
        private IMatchStateRepository _matchStateRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationsRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private ICompanionsRepository _companionsRepository;
        private ISoundsRepository _soundsRepository;
        private IMessagesRepository _messagesRepository;
        private IPromoCodesRepository _promoCodesRepository;

        public SponsorSocietyController(ILoginRepository loginRepository, IDonorRepository donorRepository, IItemsRepository itemsRepository,
        IMatchStateRepository matchStateRepository, IUserRepository userRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, IEmailRepository emailRepository, INotificationsRepository notificationsRepository,
        IAdminSettingsRepository adminSettingsRepository, ICompanionsRepository companionsRepository, ISoundsRepository soundsRepository,
        IMessagesRepository messagesRepository, IPromoCodesRepository promoCodesRepository)
        {
            _loginRepository = loginRepository;
            _donorRepository = donorRepository;
            _itemsRepository = itemsRepository;
            _userRepository = userRepository;
            _matchStateRepository = matchStateRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _notificationsRepository = notificationsRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _companionsRepository = companionsRepository;
            _soundsRepository = soundsRepository;
            _messagesRepository = messagesRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("CoinCatalog", "SponsorSociety");
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user);
            sponsorSocietyViewModel.UpdateViewData(ViewData);
            return View(sponsorSocietyViewModel);
        }

        public IActionResult CoinCatalog()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety/CoinCatalog" });
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user);
            sponsorSocietyViewModel.UpdateViewData(ViewData);
            return View(sponsorSocietyViewModel);
        }

        public IActionResult ActivateCompanion(CompanionType type)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"SponsorSociety/ActivateCompanion?type={type}" });
            }

            if (type != CompanionType.Unicorn)
            {
                return RedirectToAction("CoinCatalog", "SponsorSociety");
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user, companionType: type);
            sponsorSocietyViewModel.UpdateViewData(ViewData);
            return View(sponsorSocietyViewModel);
        }

        public IActionResult Success(string paymentId, string payerId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety" });
            }

            Dictionary<string, string> config = ConfigManager.Instance.GetProperties();
            string accessToken = new OAuthTokenCredential(config).GetAccessToken();
            APIContext apiContext = new APIContext(accessToken)
            {
                Config = ConfigManager.Instance.GetProperties()
            };

            PaymentExecution paymentExecution = new PaymentExecution
            {
                payer_id = payerId
            };

            Payment payment = new Payment
            {
                id = paymentId
            };

            Payment executedPayment = payment.Execute(apiContext, paymentExecution);

            SponsorCoinPackage sponsorCoinPackage = _donorRepository.GetSponsorCoinPackages().Find(x => x.MoneyAmount.ToString() == executedPayment.transactions.First().amount.total);
            AddCoins(user, (int)Math.Round(sponsorCoinPackage.MoneyAmount * 100), sponsorCoinPackage);

            Messages.AddSnack($"You successfully purchased {Helper.FormatNumber(sponsorCoinPackage.CoinAmount)} Sponsor Coins!");

            return RedirectToAction("Index", "SponsorSociety");
        }

        public IActionResult Cancel()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety" });
            }

            Messages.AddSnack("Your purchase was cancelled!", true);

            return RedirectToAction("Index", "SponsorSociety");
        }

        public IActionResult ChooseCard(ItemTheme theme)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety/CoinCatalog" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SponsorCoins == 0 || theme != ItemTheme.Elegant)
            {
                return RedirectToAction("Index", "SponsorSociety");
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user, theme);
            sponsorSocietyViewModel.UpdateViewData(ViewData);
            return View(sponsorSocietyViewModel);
        }

        public IActionResult GiveCoins()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SponsorSociety/CoinCatalog" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SponsorCoins == 0)
            {
                return RedirectToAction("Index", "SponsorSociety");
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user);
            sponsorSocietyViewModel.UpdateViewData(ViewData);
            return View(sponsorSocietyViewModel);
        }

        [HttpPost]
        public IActionResult GetCoinsStripe(string token, string email, int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This amount is not allowed." });
            }

            SponsorCoinPackage sponsorCoinPackage = _donorRepository.GetSponsorCoinPackages().Find(x => x.MoneyAmount == (double)amount / 100);
            if (sponsorCoinPackage == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This is not a valid Sponsor Coin package." });
            }

            Dictionary<string, string> Metadata = new Dictionary<string, string>();
            Metadata.Add("Product", "Sponsor Coins");
            Metadata.Add("Quantity", $"{sponsorCoinPackage.CoinAmount}");
            ChargeCreateOptions options = new ChargeCreateOptions
            {
                Amount = amount,
                Currency = "USD",
                Description = "Sponsor Coins",
                Source = token,
                ReceiptEmail = email,
                Metadata = Metadata
            };

            ChargeService service = new ChargeService();
            Charge charge = service.Create(options);
            int tries = 0;
            while (charge.Status == "pending" && tries < 10)
            {
                Thread.Sleep(1000);
                charge = service.Get(charge.Id);
                tries++;
            }
            if (charge.Status == "pending")
            {
                return Json(new Status { Success = false, ErrorMessage = "Your payment timed out. Please try again." });
            }
            if (charge.Status == "succeeded")
            {
                AddCoins(user, amount, sponsorCoinPackage);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully purchased {Helper.FormatNumber(sponsorCoinPackage.CoinAmount)} Sponsor Coins!", ButtonText = "Head to the Coin Catalog!", ButtonUrl = "/SponsorSociety/CoinCatalog" });
            }

            return Json(new Status { Success = false, ErrorMessage = charge.FailureMessage });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetCoinsPaypal(double amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This amount is not allowed." });
            }

            SponsorCoinPackage sponsorCoinPackage = _donorRepository.GetSponsorCoinPackages().Find(x => x.MoneyAmount == amount);
            if (sponsorCoinPackage == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This is not a valid Sponsor Coin package." });
            }

            Dictionary<string, string> config = ConfigManager.Instance.GetProperties();
            string accessToken = new OAuthTokenCredential(config).GetAccessToken();
            APIContext apiContext = new APIContext(accessToken)
            {
                Config = ConfigManager.Instance.GetProperties()
            };

            Payment payment = new Payment
            {
                intent = "sale",
                payer = new Payer { payment_method = "paypal" },
                transactions = new List<Transaction>
                {
                    new Transaction
                    {
                        amount = new Amount
                        {
                            currency = "USD",
                            total = amount.ToString()
                        },
                        description = $"{sponsorCoinPackage.CoinAmount} Sponsor Coins"
                    }
                },
                redirect_urls = new RedirectUrls
                {
                    cancel_url = $"{Helper.GetDomain()}/SponsorSociety/Cancel",
                    return_url = $"{Helper.GetDomain()}/SponsorSociety/Success"
                }
            };

            Payment createdPayment = payment.Create(apiContext);
            return Json(new Status { Success = true, ReturnUrl = createdPayment.GetApprovalUrl() });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetSponsorPerk(SponsorPerkId sponsorPerkId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            SponsorPerk sponsorPerk = _donorRepository.GetSponsorPerks(user, settings).Find(x => x.Id == sponsorPerkId);
            if (sponsorPerk == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This perk does not exist." });
            }

            if (settings.SponsorCoins < sponsorPerk.Coins)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have enough Sponsor Coins to get this perk.", ButtonText = "Get Sponsor Coins!", ButtonUrl = "/SponsorSociety" });
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            switch (sponsorPerk.Id)
            {
                case SponsorPerkId.ElegantTheme:
                    return Json(new Status { Success = true, ReturnUrl = $"/SponsorSociety/ChooseCard?theme={ItemTheme.Elegant}" });
                case SponsorPerkId.CoinGift:
                    return Json(new Status { Success = true, ReturnUrl = $"/SponsorSociety/GiveCoins" });
                case SponsorPerkId.StoreMagician:
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        bool isActive = settings.StoreMagicianExpiryDate > DateTime.UtcNow;
                        settings.StoreMagicianExpiryDate = isActive ? settings.StoreMagicianExpiryDate.AddDays(30) : DateTime.UtcNow.AddDays(30);
                        _settingsRepository.SetSetting(user, nameof(settings.StoreMagicianExpiryDate), settings.StoreMagicianExpiryDate.ToString());
                        return Json(new Status { Success = true, SuccessMessage = $"You successfully activated the Store Magician for {(isActive ? "another 30 days" : "30 days")}!", ButtonText = "Head to the Store Magician!", ButtonUrl = "/StoreMagician", SoundUrl = soundUrl });
                    }
                    break;
                case SponsorPerkId.RemoveAds:
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        bool isActive = user.AdFreeExpiryDate > DateTime.UtcNow;
                        user.AdFreeExpiryDate = isActive ? user.AdFreeExpiryDate.AddDays(30) : DateTime.UtcNow.AddDays(30);
                        _userRepository.UpdateUser(user);
                        return Json(new Status { Success = true, SuccessMessage = $"You successfully removed all ads for {(isActive ? "another 30 days" : "30 days")}!", SoundUrl = soundUrl });
                    }
                    break;
                case SponsorPerkId.CompanionUpgrade:
                    if (settings.MaxCompanions == 10)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You've already upgraded your maximum Companion size." });
                    }
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        settings.MaxCompanions = 10;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxCompanions), settings.MaxCompanions.ToString());
                        return Json(new Status { Success = true, SuccessMessage = "You can now successfully have up to 10 active Companions on your profile!", SoundUrl = soundUrl });
                    }
                    break;
                case SponsorPerkId.MaximumStorageSize:
                    if (settings.MaxStorageSize == 200)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You can already store 200 items in your Storage." });
                    }
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        settings.MaxStorageSize = 200;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxStorageSize), settings.MaxStorageSize.ToString());
                        return Json(new Status { Success = true, SuccessMessage = "You can now successfully store up to 200 items in your Storage!", ButtonText = "Head to Storage!", ButtonUrl = "/Storage", SoundUrl = soundUrl });
                    }
                    break;
                case SponsorPerkId.UnicornCompanion:
                    if (_companionsRepository.GetCompanions(user).Count >= settings.MaxCompanions)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You can only have up to {settings.MaxCompanions} Companions activated at any time." });
                    }
                    return Json(new Status { Success = true, ReturnUrl = $"/SponsorSociety/ActivateCompanion?type={CompanionType.Unicorn}" });
                case SponsorPerkId.SpecialArtCard:
                    List<Item> items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
                    }
                    List<Item> exclusiveArtCards = _itemsRepository.GetAllItems(ItemCategory.Art, null, true);
                    exclusiveArtCards = exclusiveArtCards.FindAll(x => x.Description.Contains("Premium"));
                    Random rnd = new Random();
                    Item artCard = exclusiveArtCards[rnd.Next(exclusiveArtCards.Count)];
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        _itemsRepository.AddItem(user, artCard);
                        return Json(new Status { Success = true, SuccessMessage = $"The premium Art Card \"{artCard.Name}\" was successfully added to your bag!", ButtonText = "Head to bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
                    }
                    break;
                case SponsorPerkId.SponsorCardPack:
                    settings.SponsorCoins -= sponsorPerk.Coins;
                    if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
                    {
                        string uniqueString = string.Empty;
                        bool uniqueStringFound = false;
                        while (!uniqueStringFound)
                        {
                            uniqueString = Helper.GetRandomString(8);
                            uniqueStringFound = _promoCodesRepository.AddPromoCode(user, uniqueString, PromoEffect.RareCardPack, false, true, null, DateTime.UtcNow.AddDays(1));
                        }
                        return Json(new Status { Success = true, SuccessMessage = $"Your successfully created a promo code to redeem a free Card Pack - Rare for the next 24 hours!", SoundUrl = soundUrl });
                    }
                    break;
            }

            return Json(new Status { Success = false, ErrorMessage = "Your Sponsor Coins couldn't be used. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseTheme(ItemTheme theme, Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            SponsorPerk sponsorPerk = _donorRepository.GetSponsorPerks(user, settings).Find(x => x.Name.Contains(theme.ToString(), StringComparison.InvariantCultureIgnoreCase));
            if (sponsorPerk == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This perk could not be used." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot change a {Resources.DeckCard}'s theme while you're in an active Stadium match.", ButtonText = "Head to Stadium!", ButtonUrl = "/Stadium" });
            }

            if (settings.SponsorCoins < sponsorPerk.Coins)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have enough Sponsor Coins to get this perk." });
            }

            Item card = _itemsRepository.GetItems(user).Find(x => x.Category == ItemCategory.Deck && x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You no longer have this {Resources.DeckCard}." });
            }

            settings.SponsorCoins -= sponsorPerk.Coins;
            if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
            {
                card.Theme = ItemTheme.Elegant;
                _itemsRepository.UpdateItem(user, card);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully applied the theme and your {Resources.DeckCard} became \"{card.Name}\"!", ButtonText = "Head to Bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your Sponsor Coins couldn't be used. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ActivateCompanion(CompanionType type, string name, CompanionColor? selectedColor)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            SponsorPerk sponsorPerk = _donorRepository.GetSponsorPerks(user, settings).Find(x => x.Name.Contains(type.ToString()));
            if (sponsorPerk == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This is not a valid companion to activate." });
            }

            if (settings.SponsorCoins < sponsorPerk.Coins)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have enough Sponsor Coins to get this perk." });
            }

            if (selectedColor == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a color for your Companion." });
            }

            if (string.IsNullOrEmpty(name))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must give your Companion a name." });
            }

            if (name.Length > 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Companion's name must be 10 characters or less." });
            }

            settings.SponsorCoins -= sponsorPerk.Coins;
            if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
            {
                _companionsRepository.AddCompanion(user, type, name, selectedColor.Value, out Companion newCompanion);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionActivated))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionActivated);
                }

                Messages.AddSnack($"Your Companion \"{name}\" was successfully activated and is now visible on your profile!");

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{newCompanion.Id}", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your Sponsor Coins couldn't be used. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveCoins(string username, int amount, string message)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot gift someone less than 1 Sponsor Coin." });
            }

            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot gift yourself." });
            }

            User giftedUser = _userRepository.GetUser(username);
            if (giftedUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SponsorCoins < amount)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have enough Sponsor Coins to give this amount." });
            }

            Settings giftUserSettings = _settingsRepository.GetSettings(giftedUser, false);
            settings.SponsorCoins -= amount;
            giftUserSettings.SponsorCoins += amount;

            if (_settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString()))
            {
                _settingsRepository.SetSetting(giftedUser, nameof(giftUserSettings.SponsorCoins), giftUserSettings.SponsorCoins.ToString(), false);

                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    if (!string.IsNullOrEmpty(message))
                    {
                        message = $"They also included the following personal message: \"{message}\"";
                    }
                    _notificationsRepository.AddNotification(giftedUser, $"@{user.Username} gifted you {Helper.FormatNumber(amount)} Sponsor Coin{(amount == 1 ? "" : "s")}! {message}", NotificationLocation.SponsorSociety, "/SponsorSociety/CoinCatalog", domain: domain);
                    string subject = "You were gifted Sponsor Coins!";
                    string title = "Sponsor Coins";
                    string body = $"<b><a href='{domain}/User/{user.Username}'>{user.Username}</a></b> gifted you <b>{Helper.FormatNumber(amount)} Sponsor Coin{(amount == 1 ? "" : "s")}</b>! You can head to {Resources.SiteName} now and use them at the Coin Catalog to redeem exclusive perks! {message}";
                    _emailRepository.SendEmail(giftedUser, subject, title, body, "/SponsorSociety/CoinCatalog", false, domain);
                });

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully gave {giftedUser.Username} {Helper.FormatNumber(amount)} Sponsor Coin{(amount == 1 ? "" : "s")}!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not give any Sponsor Coins. Please try again." });
        }

        [HttpGet]
        public IActionResult GetSponsorSocietyPartialView(bool isPurchase = true)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            SponsorSocietyViewModel sponsorSocietyViewModel = GenerateModel(user);
            if (isPurchase)
            {
                return PartialView("_SponsorSocietyMainPartial", sponsorSocietyViewModel);
            }
            return PartialView("_SponsorSocietyCoinCatalogPartial", sponsorSocietyViewModel);
        }

        private void AddCoins(User user, int amount, SponsorCoinPackage sponsorCoinPackage)
        {
            _donorRepository.AddDonor(user, amount);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SponsorSocietyDonor))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SponsorSocietyDonor);
            }
            user.PremiumExpiryDate = DateTime.UtcNow.AddDays(30);
            _userRepository.UpdateUser(user);

            Settings settings = _settingsRepository.GetSettings(user);
            settings.SponsorCoins += sponsorCoinPackage.CoinAmount;
            _settingsRepository.SetSetting(user, nameof(settings.SponsorCoins), settings.SponsorCoins.ToString());

            string domain = Helper.GetDomain();
            Task.Run(() =>
            {
                _messagesRepository.SendPersonalMessage(user, domain, "Thanks for buying Sponsor Coins! I really appreciate it!");
            });
        }

        private SponsorSocietyViewModel GenerateModel(User user, ItemTheme theme = ItemTheme.Default, CompanionType companionType = CompanionType.None)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            AdminSettings adminSettings = _adminSettingsRepository.GetSettings();
            List<Donor> recentDonors = _donorRepository.GetDonorsWithDays(60, false);
            SponsorSocietyViewModel sponsorSocietyViewModel = new SponsorSocietyViewModel
            {
                Title = "Sponsor Society",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.SponsorSociety,
                RecentDonors = recentDonors,
                SponsorCoinPackages = _donorRepository.GetSponsorCoinPackages(),
                SponsorPerks = _donorRepository.GetSponsorPerks(user, settings).OrderBy(x => x.Coins).ThenBy(x => x.Name).ToList(),
                ActivePromoCodes = _promoCodesRepository.GetAllUnexpiredSponsorCodes(false),
                SponsorCoins = settings.SponsorCoins,
                WeeklyAmount = recentDonors.FindAll(x => (DateTime.UtcNow - x.CreatedDate).TotalDays <= 7).Sum(x => (double)x.Amount / 100),
                WeeklyTotal = adminSettings.TotalSponsorAmount,
                StripePublishableKey = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development" ? ConfigurationManager.AppSettings.Get("DevelopmentStripePublishableKey") : ConfigurationManager.AppSettings.Get("ProductionStripePublishableKey")
            };

            if (theme != ItemTheme.Default)
            {
                sponsorSocietyViewModel.Cards = _itemsRepository.GetItems(user).FindAll(x => x.Category == ItemCategory.Deck && x.Theme != theme);
                sponsorSocietyViewModel.Theme = theme;
            }
            if (companionType != CompanionType.None)
            {
                sponsorSocietyViewModel.Companion = companionType;
            }

            return sponsorSocietyViewModel;
        }
    }
}
