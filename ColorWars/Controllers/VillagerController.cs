﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class VillagerController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IMessagesRepository _messagesRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public VillagerController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IMessagesRepository messagesRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _messagesRepository = messagesRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Villager" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("ThemeSqueezer", "Villager");
            }

            VillagerViewModel villagerViewModel = GenerateModel(user, true);
            villagerViewModel.UpdateViewData(ViewData);
            return View(villagerViewModel);
        }

        public IActionResult ThemeSqueezer()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Villager/ThemeSqueezer" });
            }

            VillagerViewModel villagerViewModel = GenerateModel(user, false);
            villagerViewModel.UpdateViewData(ViewData);
            return View(villagerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveCard(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Card card = _itemsRepository.GetCards(user).FindAll(x => x.Color == CardColor.Red && x.DeckType == DeckType.None && x.Condition < ItemCondition.Unusable).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot give this item to the Weary Villager." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VillagerCardsGiven == 499)
            {
                int bagSize = _itemsRepository.GetItems(user).Count;
                if (bagSize >= settings.MaxBagSize)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot give your {Helper.FormatNumber(500)}th red {Resources.DeckCard} with a full bag." });
                }
            }

            string soundUrl = "";

            if (_itemsRepository.RemoveItem(user, card))
            {
                settings.VillagerCardsGiven++;
                _settingsRepository.SetSetting(user, nameof(settings.VillagerCardsGiven), settings.VillagerCardsGiven.ToString());

                if (settings.VillagerCardsGiven == 10)
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You can now use the Theme Squeezer machine!", ScrollToTop = true, ButtonText = "Head to the Theme Squeeze!", ButtonUrl = "/Villager/ThemeSqueezer", SoundUrl = soundUrl });
                }

                if (settings.VillagerCardsGiven % 15 == 0)
                {
                    settings.TrainingTokens++;
                    _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully earned 1 Training Token!", ScrollToTop = true, ButtonText = "Head to Training Center!", ButtonUrl = "/TrainingCenter" });
                }

                if (settings.VillagerCardsGiven == 500)
                {
                    Item item = _itemsRepository.GetAllItems(ItemCategory.All, null, true).Find(x => x.Category == ItemCategory.Art && x.Name == "Lost Time");
                    _itemsRepository.AddItem(user, item);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.EarnedVillagerItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.EarnedVillagerItem);
                    }

                    _messagesRepository.SendCourierMessage(user, $"The Weary Villager has a message for you: \"Thank you so much for the {Helper.FormatNumber(500)} red {Resources.DeckCards}! I used each piece of them to make an art collage. I hope you appreciate the Art Card. It shows how fragile time really is and how we should appreciate every moment. Thank you.\"");

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully gave the Weary Villager {Helper.FormatNumber(500)} red {Resources.DeckCards} and, as a reward, the item \"{item.Name}\" was added to your bag!", ScrollToTop = true, ButtonText = "Head to bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
                }

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You gave the Weary Village 1 red {Resources.DeckCard}!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The Weary Villager could not be given your red {Resources.DeckCard} at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SqueezeTheme(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Card card = _itemsRepository.GetCards(user).FindAll(x => x.DeckType == DeckType.None && x.Condition < ItemCondition.Unusable).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot add this {Resources.DeckCard} to the Theme Squeezer." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VillagerCardsGiven < 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't gained access to the Theme Squeezer machine yet." });
            }

            if (settings.ThemeCardSqueezeDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You already have a {Resources.DeckCard} in the Theme Squeezer machine." });
            }

            if (card.Theme == ItemTheme.Elegant)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot put a {Resources.DeckCard} with an Elegant Theme in the Theme Squeezer." });
            }

            if (_itemsRepository.RemoveItem(user, card))
            {
                settings.ThemeSqueezeType = card.Theme;
                settings.ThemeCardSqueezeDate = DateTime.UtcNow.AddDays(1);
                _settingsRepository.SetSetting(user, nameof(settings.ThemeSqueezeType), settings.ThemeSqueezeType.ToString());
                _settingsRepository.SetSetting(user, nameof(settings.ThemeCardSqueezeDate), settings.ThemeCardSqueezeDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"Your {Resources.DeckCard} was successfully added to the Theme Squeezer machine! It will be ready in 1 day.", ScrollToTop = true });
            }

            return Json(new Status { Success = false, ErrorMessage = $"This {Resources.DeckCard} could not be added to the Theme Squeezer machine." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeThemeCard()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VillagerCardsGiven < 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't gained access to the Theme Squeezer machine yet." });
            }

            if (settings.ThemeCardSqueezeDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have a {Resources.DeckCard} in the Theme Squeezer machine." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item themeCard = _itemsRepository.GetAllItems(ItemCategory.Theme, null, true).Find(x => x.Name.Contains(settings.ThemeSqueezeType.ToString()));
            if (_itemsRepository.AddItem(user, themeCard))
            {
                settings.ThemeCardSqueezeDate = DateTime.MinValue;
                _settingsRepository.SetSetting(user, nameof(settings.ThemeCardSqueezeDate), settings.ThemeCardSqueezeDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"The Theme Card \"{themeCard.Name}\" was successfully added to your bag!", ScrollToTop = true, ButtonText = "Head to bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The Theme Card couldn't be taken." });
        }

        [HttpGet]
        public IActionResult GetVillagerPartialView(bool isVillager)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            VillagerViewModel villagerViewModel = GenerateModel(user, isVillager);
            if (isVillager)
            {
                return PartialView("_VillagerMainPartial", villagerViewModel);
            }

            return PartialView("_VillagerThemeSqueezerPartial", villagerViewModel);
        }

        private VillagerViewModel GenerateModel(User user, bool isVillager)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            VillagerViewModel villagerViewModel = new VillagerViewModel
            {
                Title = "Weary Villager",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Villager,
                CardsGiven = settings.VillagerCardsGiven
            };

            if (isVillager)
            {
                villagerViewModel.Cards = _itemsRepository.GetCards(user).FindAll(x => x.Color == CardColor.Red && x.DeckType == DeckType.None && x.Condition < ItemCondition.Unusable);
            }
            else
            {
                villagerViewModel.ThemeFinishedDate = settings.ThemeCardSqueezeDate;
                if (settings.ThemeCardSqueezeDate == DateTime.MinValue)
                {
                    villagerViewModel.Cards = _itemsRepository.GetCards(user).FindAll(x => x.DeckType == DeckType.None && x.Condition < ItemCondition.Unusable);
                }
                else
                {
                    villagerViewModel.ThemeCard = _itemsRepository.GetAllItems(ItemCategory.Theme, null, true).Find(x => x.Name.Contains(settings.ThemeSqueezeType.ToString()));
                    villagerViewModel.CanTakeThemeCard = settings.ThemeCardSqueezeDate <= DateTime.UtcNow;
                }
            }

            return villagerViewModel;
        }
    }
}
