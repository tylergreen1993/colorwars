﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SurveyController : Controller
    {
        private ILoginRepository _loginRepository;
        private ISurveyRepository _surveyRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IMessagesRepository _messagesRepository;

        public SurveyController(ILoginRepository loginRepository, ISurveyRepository surveyRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, IMessagesRepository messagesRepository)
        {
            _loginRepository = loginRepository;
            _surveyRepository = surveyRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _messagesRepository = messagesRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Survey" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SurveyExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedSurvey)
            {
                settings.CheckedSurvey = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedSurvey), settings.CheckedSurvey.ToString());
            }

            SurveyViewModel surveyViewModel = GenerateModel(user);
            surveyViewModel.UpdateViewData(ViewData);
            return View(surveyViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitSurvey(string answer0, string answer1, string answer2, string answer3, string answer4, string answer5)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SurveyExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have any surveys." });
            }

            List<SurveyAnswer> surveyAnswers = _surveyRepository.GetSurveyAnswers(user).FindAll(x => x.CreatedDate >= settings.SurveyExpiryDate.AddDays(-3));
            if (surveyAnswers.Count > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already completed this survey." });
            }

            string[] answers = { answer0, answer1, answer2, answer3, answer4, answer5 };

            for (int i = 0; i < answers.Length; i++)
            {
                if (i == answers.Length - 1)
                {
                    continue;
                }

                if (string.IsNullOrEmpty(answers[i]))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You must fill out an answer for Question {i + 1}." });
                }
            }

            List<SurveyQuestion> surveyQuestions = _surveyRepository.GetSurveyQuestions(user);
            for (int i = 0; i < surveyQuestions.Count; i++)
            {
                if (!string.IsNullOrEmpty(answers[i]))
                {
                    if (_surveyRepository.AddSurveyAnswer(user, surveyQuestions[i].Id, answers[i]))
                    {
                        if (surveyQuestions[i].Id == SurveyQuestionId.RecommendedToOthers && int.Parse(answers[i]) < 8)
                        {
                            _messagesRepository.SendPersonalMessage(user, Helper.GetDomain(), $"Hey! So I saw you rated {Resources.SiteName} a {answers[i]}/10, anything I could do to make it better?");
                        }
                    }
                }
            }

            if (_pointsRepository.AddPoints(user, 5000))
            {
                settings.SurveyExpiryDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.SurveyExpiryDate), settings.SurveyExpiryDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompletedSurvey))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompletedSurvey);
                }

                Messages.SetSuccessMessage($"You successfully completed the survey and earned {Helper.GetFormattedPoints(5000)}!");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The survey could not be completed." });
        }

        private SurveyViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            if (string.IsNullOrEmpty(settings.SurveyQuestionIds))
            {
                _surveyRepository.SaveNewSurveyQuestions(user);
            }

            SurveyViewModel surveyViewModel = new SurveyViewModel
            {
                Title = "Survey",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Survey,
                SurveyQuestions = _surveyRepository.GetSurveyQuestions(user),
                Reward = 5000,
                ExpiryDate = settings.SurveyExpiryDate
            };

            return surveyViewModel;
        }
    }
}
