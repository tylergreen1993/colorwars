﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class MerchantsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public MerchantsController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index(bool fromTab, string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Merchants" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Jewel", "Merchants");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedMerchants)
            {
                settings.CheckedMerchants = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedMerchants), settings.CheckedMerchants.ToString());
            }

            if (!fromTab && settings.MerchantsContestEnteredDate.AddHours(3) > DateTime.UtcNow)
            {
                return RedirectToAction("Jewel", "Merchants");
            }

            MerchantsViewModel merchantsViewModel = GenerateModel(user, true);
            merchantsViewModel.UpdateViewData(ViewData);
            return View(merchantsViewModel);
        }

        public IActionResult Jewel()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Merchants/Jewel" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MerchantsViewModel merchantsViewModel = GenerateModel(user, false);
            merchantsViewModel.UpdateViewData(ViewData);
            return View(merchantsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EnterContest(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Traveling Merchants have already left.", ShouldRefresh = true });
            }

            if (user.Points < 500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You do not have enough {Resources.Currency} on hand to enter the contest." });
            }

            int hoursSinceLastContestEntry = (int)(DateTime.UtcNow - settings.MerchantsContestEnteredDate).TotalHours;
            if (hoursSinceLastContestEntry < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've entered the contest too recently. Try again in {3 - hoursSinceLastContestEntry} hour{(3 - hoursSinceLastContestEntry == 1 ? "" : "s")}." });
            }

            Item item = _itemsRepository.GetItems(user).FindAll(x => x.Condition != ItemCondition.Unusable).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You no longer have this item or it has an Unusable condition." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your cannot enter the contest with a full bag." });
            }

            _pointsRepository.SubtractPoints(user, 500);

            int score = item.Points / 100;
            score += item.GetAgeInHours() / 10;

            switch (item.Condition)
            {
                case ItemCondition.New:
                    score *= 2;
                    break;
                case ItemCondition.Good:
                    score /= 2;
                    break;
                case ItemCondition.Poor:
                    score /= 4;
                    break;
            }

            if (item.OriginalPoints < 2500)
            {
                score /= 2;
            }

            if (item.Uses < item.TotalUses)
            {
                score = (int)Math.Round(score * 0.75);
            }

            if (item.Theme != ItemTheme.Default)
            {
                score = (int)Math.Round(score * 1.5);
            }

            if (item.IsExclusive)
            {
                score = (int)Math.Round(score * 1.5);
            }

            int prizeMultiplier = 0;
            if (score >= 10000)
            {
                prizeMultiplier = 4;
            }
            else if (score >= 5000)
            {
                prizeMultiplier = 3;
            }
            else if (score >= 2500)
            {
                prizeMultiplier = 2;
            }
            else if (score >= 1000)
            {
                prizeMultiplier = 1;
            }

            settings.MerchantsContestEnteredDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.MerchantsContestEnteredDate), settings.MerchantsContestEnteredDate.ToString());

            if (prizeMultiplier == 0)
            {
                return Json(new Status { Success = true, InfoMessage = $"The Traveling Merchants gave your item a score of... {Helper.FormatNumber(score)}! This does not qualify for any prizes. Better luck next time!", Points = Helper.GetFormattedPoints(user.Points) });
            }

            List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points > 1000 * Math.Pow(prizeMultiplier, 2) && x.Points <= 25000 * prizeMultiplier);
            Random rnd = new Random();
            Item prizeItem = items[rnd.Next(items.Count)];

            _itemsRepository.AddItem(user, prizeItem);

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WonContestPrize))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WonContestPrize);
            }
            if (prizeMultiplier == 4)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WonTier1ContestPrize))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WonTier1ContestPrize);
                }
            }

            return Json(new Status { Success = true, SuccessMessage = $"The Traveling Merchants gave your item a score of... {Helper.FormatNumber(score)}! You won a Tier {5 - prizeMultiplier} prize! The item \"{prizeItem.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SellJewel(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Traveling Merchants have already left.", ShouldRefresh = true });
            }

            if (settings.MerchantsJewelsSold >= 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've sold the Traveling Merchants too many Jewel Cards. You can sell them more the next time they return." });
            }

            Jewel jewel = _itemsRepository.GetJewels(user).Find(x => x.SelectionId == selectionId);
            if (jewel == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Jewel Card or it has an Unusable condition." });
            }

            int offerPrice = (int)Math.Round(jewel.Points * settings.MerchantsJewelCardMultiplier);

            if (_pointsRepository.AddPoints(user, offerPrice))
            {
                _itemsRepository.RemoveItem(user, jewel);

                settings.MerchantsJewelsSold += 1;
                _settingsRepository.SetSetting(user, nameof(settings.MerchantsJewelsSold), settings.MerchantsJewelsSold.ToString());

                if (offerPrice > jewel.Points)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SoldJewelCardToMerchantsForProfit))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SoldJewelCardToMerchantsForProfit);
                    }
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully sold your Jewel Card \"{jewel.Name}\" for {Helper.GetFormattedPoints(offerPrice)}!", Points = Helper.GetFormattedPoints(user.Points) });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your Jewel Card could not be sold. Please try again." });
        }

        [HttpGet]
        public IActionResult GetMerchantsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Traveling Merchants have already left." });
            }

            MerchantsViewModel merchantsViewModel = GenerateModel(user, true);
            return PartialView("_MerchantsMainPartial", merchantsViewModel);
        }

        [HttpGet]
        public IActionResult GetMerchantsJewelPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MerchantsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Traveling Merchants have already left." });
            }

            MerchantsViewModel merchantsViewModel = GenerateModel(user, false);
            return PartialView("_MerchantsJewelPartial", merchantsViewModel);
        }

        private MerchantsViewModel GenerateModel(User user, bool isContest)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            MerchantsViewModel merchantsViewModel = new MerchantsViewModel
            {
                Title = "Traveling Merchants",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Merchants,
                CanEnterContest = (DateTime.UtcNow - settings.MerchantsContestEnteredDate).TotalHours >= 3,
                ExpiryDate = settings.MerchantsExpiryDate
            };

            if (isContest)
            {
                merchantsViewModel.Items = _itemsRepository.GetItems(user).FindAll(x => x.Condition != ItemCondition.Unusable);
            }
            else
            {
                List<Jewel> jewels = _itemsRepository.GetJewels(user);
                foreach (Jewel jewel in jewels)
                {
                    jewel.Points = (int)Math.Round(jewel.Points * settings.MerchantsJewelCardMultiplier);
                }
                merchantsViewModel.Jewels = jewels;
            }

            return merchantsViewModel;
        }
    }
}
