﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TourneyController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ITourneyRepository _tourneyRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private ISettingsRepository _settingsRepository;
        private IUserRepository _userRepository;
        private INotificationsRepository _notificationsRepository;
        private IEmailRepository _emailRepository;
        private IBlockedRepository _blockedRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public TourneyController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ITourneyRepository tourneyRepository, IMatchHistoryRepository matchHistoryRepository, ISettingsRepository settingsRepository,
        IUserRepository userRepository, INotificationsRepository notificationsRepository, IEmailRepository emailRepository,
        IBlockedRepository blockedRepository, IAdminHistoryRepository adminHistoryRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _tourneyRepository = tourneyRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _settingsRepository = settingsRepository;
            _userRepository = userRepository;
            _notificationsRepository = notificationsRepository;
            _emailRepository = emailRepository;
            _blockedRepository = blockedRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public IActionResult Index(int id, int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Tourney?id={id}" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 2)
            {
                Messages.AddSnack("You haven't unlocked Tourneys yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            TourneyViewModel tourneyViewModel = GenerateModel(user, false, false, id, page);
            if (id > 0 && tourneyViewModel.CurrentTourney == null)
            {
                Messages.AddSnack("This tourney no longer exists.", true);
            }
            tourneyViewModel.UpdateViewData(ViewData);
            return View(tourneyViewModel);
        }

        public IActionResult Create()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Tourney/Create" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 2)
            {
                Messages.AddSnack("You haven't unlocked Tourneys yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            TourneyViewModel tourneyViewModel = GenerateModel(user, false, true);
            tourneyViewModel.UpdateViewData(ViewData);
            return View(tourneyViewModel);
        }

        public IActionResult Participant()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Tourney/Participant" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 2)
            {
                Messages.AddSnack("You haven't unlocked Tourneys yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            TourneyViewModel tourneyViewModel = GenerateModel(user, true, false);
            tourneyViewModel.UpdateViewData(ViewData);
            return View(tourneyViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(string name, TourneyCaliber caliber, int matchesPerRound, int requiredParticipants, int entranceCost, string noHostParticipation, string sponsorTourney)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(name))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must give your tourney a name." });
            }

            if (name.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "You tourney name must be 30 characters or less." });
            }

            bool isNoHostTourney = noHostParticipation == "on";
            bool isSponsorTourney = sponsorTourney == "on";

            if (caliber != TourneyCaliber.All && !isNoHostTourney)
            {
                bool isElite = _matchHistoryRepository.IsElite(user);
                if ((isElite && caliber == TourneyCaliber.NonEliteOnly) || (!isElite && caliber == TourneyCaliber.EliteOnly))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You don't meet your own tourney requirements." });
                }
            }

            if (matchesPerRound != 1 && matchesPerRound != 3 && matchesPerRound != 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your tourney must be a best of either 1, 3, or 5 matches." });
            }

            if (entranceCost < 0 || entranceCost > 10000000)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot enter this amount as an entrance cost." });
            }

            if (isSponsorTourney && entranceCost == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot sponsor a tourney for {Helper.GetFormattedPoints(0)}." });
            }

            if (requiredParticipants != 4 && requiredParticipants != 8)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your tourney must either have 4 or 8 participants." });
            }

            List<Tourney> userTourneys = _tourneyRepository.GetUserTourneys(user).FindAll(x => x.Creator == user.Username && x.GetStatus() != TourneyStatus.Completed);
            if (userTourneys.Count >= 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot create a new tourney when you're already hosting 3 active tourneys." });
            }

            if (entranceCost > 0 && (isSponsorTourney || !isNoHostTourney))
            {
                if (user.Points < entranceCost)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to {(isSponsorTourney ? "sponsor" : "enter")} your own tourney." });
                }

                _pointsRepository.SubtractPoints(user, entranceCost);
            }

            if (_tourneyRepository.AddTourney(user, entranceCost, matchesPerRound, requiredParticipants, isSponsorTourney, name, caliber, out Tourney tourney))
            {
                if (!isNoHostTourney)
                {
                    _tourneyRepository.AddTourneyMatch(user, tourney, 0);
                }

                return Json(new Status { Success = true, ReturnUrl = $"/Tourney?id={tourney.Id}" });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your tourney could not be created. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Join(int tourneyId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 2)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot enter a tourney yet." });
            }

            Tourney tourney = _tourneyRepository.GetTourneyWithId(tourneyId);
            if (tourney == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This tourney doesn't exist." });
            }

            if (tourney.GetStatus() != TourneyStatus.NotStarted)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't join this tourney." });
            }

            if (tourney.IsParticipant(user))
            {
                return Json(new Status { Success = false, ErrorMessage = "You're already participating in this tourney." });
            }

            if (user.Points < tourney.EntranceCost && !tourney.IsSponsored)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to join this tourney." });
            }

            if (tourney.TourneyCaliber != TourneyCaliber.All)
            {
                bool isElite = _matchHistoryRepository.IsElite(user);
                if ((isElite && tourney.TourneyCaliber == TourneyCaliber.NonEliteOnly) || (!isElite && tourney.TourneyCaliber == TourneyCaliber.EliteOnly))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You don't meet the tourney requirements." });
                }
            }

            if (tourney.EntranceCost > 0 && !tourney.IsSponsored)
            {
                _pointsRepository.SubtractPoints(user, tourney.EntranceCost);
            }

            User tourneyCreator = _userRepository.GetUser(tourney.Creator);
            if (tourneyCreator != null)
            {
                if (_blockedRepository.HasBlockedUser(tourneyCreator, user, false))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You are not allowed to join this tourney. Please join another one instead." });
                }
            }

            if (_tourneyRepository.AddTourneyMatch(user, tourney, 0))
            {
                if (tourney.RequiredParticipants == tourney.GetEntrantsCount() + 1)
                {
                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        foreach (string username in tourney.GetEntrants(0))
                        {
                            User entrantUser = _userRepository.GetUser(username);
                            if (entrantUser != null)
                            {
                                _notificationsRepository.AddNotification(entrantUser, $"All {Helper.FormatNumber(tourney.RequiredParticipants)} users have joined the \"{tourney.Name}\" tourney! You should challenge your first opponent as soon as possible!", NotificationLocation.Tourney, $"/Tourney?id={tourney.Id}", domain: domain);

                                string subject = "Your tourney has started!";
                                string title = $"{tourney.Name} Tourney";
                                string body = $"All <b>{Helper.FormatNumber(tourney.RequiredParticipants)} users</b> have joined the <b>{tourney.Name}</b> tourney! Click the button below to see your first opponent and make sure to challenge them as soon as possible! ";
                                _emailRepository.SendEmail(entrantUser, subject, title, body, $"/Tourney?id={tourney.Id}", domain: domain);
                            }
                        }
                    });
                    return Json(new Status { Success = true, SuccessMessage = "You successfully joined the tourney! You should challenge your first opponent as soon as possible!", Points = user.GetFormattedPoints() });
                }

                return Json(new Status { Success = true, SuccessMessage = "You successfully joined the tourney! You'll be notified when it begins!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "You cannot join the tourney at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Forfeit(int tourneyId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Tourney tourney = _tourneyRepository.GetTourneyWithId(tourneyId);
            if (tourney == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This tourney does not exist." });
            }

            TourneyMatch tourneyMatch = tourney.TourneyMatches.FindAll(x => x.Username1 == user.Username || x.Username2 == user.Username).LastOrDefault();
            if (tourneyMatch == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You're not in this tourney anymore." });
            }

            string tourneyMatchWinner = tourneyMatch.Username1 == user.Username ? tourneyMatch.Username2 : tourneyMatch.Username1;
            User tourneyMatchWinnerUser = _userRepository.GetUser(tourneyMatchWinner);
            if (tourneyMatchWinnerUser != null)
            {
                tourneyMatch.Winner = tourneyMatchWinnerUser.Username;
                if (_tourneyRepository.UpdateTourneyMatch(tourneyMatch))
                {
                    _tourneyRepository.AddTourneyProgress(tourneyMatchWinnerUser, tourneyMatch.Id, MatchStatus.Won, false);
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully forfeited from the tourney!" });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You cannot forfeit from the tourney at this time." });
        }

        [HttpPost]
        public IActionResult Delete(int tourneyId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Tourney tourney = _tourneyRepository.GetTourneyWithId(tourneyId);
            if (tourney == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This tourney does not exist." });
            }

            if (tourney.Creator != user.Username && user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot delete this tourney." });
            }

            if (tourney.GetStatus() == TourneyStatus.InProgress && user.Role == UserRole.Default)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot delete a tourney that has already started." });
            }

            if (tourney.GetStatus() == TourneyStatus.Completed)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot delete a completed tourney." });
            }

            if (_tourneyRepository.DeleteTourney(tourney))
            {
                if (user.Username != tourney.Creator)
                {
                    User tourneyCreator = _userRepository.GetUser(tourney.Creator);
                    if (tourneyCreator != null)
                    {
                        _adminHistoryRepository.AddAdminHistory(tourneyCreator, user, $"Deleted tourney \"{tourney.Name}\".");
                    }
                }

                Messages.AddSnack("Your tourney was successfully deleted!");
                return Json(new Status { Success = true, ReturnUrl = "/Tourney" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This tourney could not be deleted." });
        }

        [HttpGet]
        public IActionResult GetTourneyPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TourneyViewModel tourneyViewModel = GenerateModel(user, false, false, id);
            if (id == 0)
            {
                return PartialView("_TourneyAllPartial", tourneyViewModel);
            }
            return PartialView("_TourneyIdPartial", tourneyViewModel);
        }

        private TourneyViewModel GenerateModel(User user, bool isParticipant, bool isCreate, int id = 0, int page = 0)
        {
            TourneyViewModel tourneyViewModel = new TourneyViewModel
            {
                Title = "Tourney",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Tourney
            };

            if (isCreate)
            {
                return tourneyViewModel;
            }

            if (id > 0)
            {
                tourneyViewModel.CurrentTourney = _tourneyRepository.GetTourneyWithId(id);
            }
            if (tourneyViewModel.CurrentTourney == null || (tourneyViewModel.CurrentTourney.GetStatus() != TourneyStatus.Completed && (DateTime.UtcNow - tourneyViewModel.CurrentTourney.CreatedDate).TotalHours >= 96))
            {
                if (tourneyViewModel.CurrentTourney != null)
                {
                    _tourneyRepository.DeleteTourney(tourneyViewModel.CurrentTourney, true);
                    tourneyViewModel.CurrentTourney = null;
                }

                List<Tourney> tourneys = isParticipant ? _tourneyRepository.GetUserTourneys(user) : _tourneyRepository.GetAllTourneys();
                tourneyViewModel.Tourneys = tourneys;
                List<Tourney> incompleteTourneys = tourneys.FindAll(x => x.GetStatus() != TourneyStatus.Completed);
                foreach (Tourney tourney in incompleteTourneys)
                {
                    if ((DateTime.UtcNow - tourney.CreatedDate).TotalHours >= 96)
                    {
                        if (_tourneyRepository.DeleteTourney(tourney, true))
                        {
                            tourneyViewModel.Tourneys.Remove(tourney);
                        }
                    }
                }

                int resultsPerPage = 15;
                tourneyViewModel.TotalPages = (int)Math.Ceiling((double)tourneyViewModel.Tourneys.Count / resultsPerPage);
                if (page < 0 || page >= tourneyViewModel.TotalPages)
                {
                    page = 0;
                }
                tourneyViewModel.Tourneys = tourneyViewModel.Tourneys.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                tourneyViewModel.CurrentPage = page;
            }

            if (tourneyViewModel.CurrentTourney != null)
            {
                TourneyMatch tourneyMatch = tourneyViewModel.CurrentTourney.TourneyMatches.Find(x => (x.Username1 == user.Username || x.Username2 == user.Username) && string.IsNullOrEmpty(x.GetWinner()));
                if (tourneyMatch != null)
                {
                    string opponent = tourneyMatch.Username1 == user.Username ? tourneyMatch.Username2 : tourneyMatch.Username1;
                    User tourneyOpponent = _userRepository.GetUser(opponent);
                    if (tourneyOpponent != null)
                    {
                        tourneyViewModel.TourneyOpponent = tourneyOpponent;
                    }
                }
            }

            return tourneyViewModel;
        }
    }
}
