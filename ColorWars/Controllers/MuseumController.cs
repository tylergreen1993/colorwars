﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class MuseumController : Controller
    {
        private ILoginRepository _loginRepository;
        private IMuseumRepository _museumRepository;
        private IItemsRepository _itemsRepository;
        private IAttentionHallRepository _attentionHallRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public MuseumController(ILoginRepository loginRepository, IMuseumRepository museumRepository, IItemsRepository itemsRepository,
        IAttentionHallRepository attentionHallRepository, IPointsRepository pointsRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _museumRepository = museumRepository;
            _itemsRepository = itemsRepository;
            _attentionHallRepository = attentionHallRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                if (tag == "Boutique")
                {
                    return RedirectToAction("Boutique", "Museum");
                }
                if (tag == "Records")
                {
                    return RedirectToAction("Records", "Museum");
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedMuseum)
            {
                settings.CheckedMuseum = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedMuseum), settings.CheckedMuseum.ToString());
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.North);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        public IActionResult EastWing()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum/EastWing" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.East);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        public IActionResult WestWing()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum/WestWing" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.West);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        public IActionResult SouthWing()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum/SouthWing" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.South);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        public IActionResult Boutique()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum/Boutique" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.Boutique);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        public IActionResult Records()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Museum/Records" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.Records);
            museumViewModel.UpdateViewData(ViewData);
            return View(museumViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DonateEastWing()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            if (user.Points < 5000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already unlocked the East Wing of the Museum." });
            }

            if (_pointsRepository.SubtractPoints(user, 5000))
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MuseumEastWingUnlocked);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully donated {Helper.GetFormattedPoints(5000)} and unlocked the East Wing of the Museum!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The East Wing of the Museum could not be unlocked." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DonateWestWing(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already unlocked the West Wing of the Museum." });
            }

            Item item = _itemsRepository.GetItems(user, true).FindAll(x => x.GetAgeInHours() >= 500).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This item doesn't match the criteria or no longer exists." });
            }

            _itemsRepository.RemoveItem(user, item);

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MuseumWestWingUnlocked);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully donated the item \"{item.Name}\" and unlocked the West Wing of the Museum!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UnlockSouthWing()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already unlocked the South Wing of the Museum." });
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.StreakCards5DayStreak))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You haven't earned a 5x play streak playing Streak Cards.", ButtonText = "Head to Streak Cards!", ButtonUrl = "/StreakCards" });
            }

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MuseumSouthWingUnlocked);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully unlocked the South Wing of the Museum!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyBoutiqueItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            int minutesSinceLastBoutiquePurchase = (int)(DateTime.UtcNow - settings.LastBoutiquePurchaseDate).TotalMinutes;
            if (minutesSinceLastBoutiquePurchase < 60)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought an item from the Boutique too recently. Try again in {60 - minutesSinceLastBoutiquePurchase} minute{(60 - minutesSinceLastBoutiquePurchase == 1 ? "" : "s")}." });
            }

            Occasion holiday = Helper.GetCurrentOccasion();
            List<Item> artCards = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).FindAll(x => !x.IsExclusive || x.Name.Equals(holiday.GetDisplayName(), StringComparison.InvariantCultureIgnoreCase)).ToList();
            Item item = artCards.Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is not available at the Boutique." });
            }

            if (user.Points < item.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_pointsRepository.SubtractPoints(user, item.Points))
            {
                _itemsRepository.AddItem(user, item);

                settings.LastBoutiquePurchaseDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastBoutiquePurchaseDate), settings.LastBoutiquePurchaseDate.ToString());

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully bought the item \"{item.Name}\" for {Helper.GetFormattedPoints(item.Points)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item couldn't be bought." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UnlockBriefcase(int pinCode)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            if (settings.GoldBriefcasePinCode == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not able to open this yet." });
            }

            if (settings.GoldBriefcasePinCode == -1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already opened the Gold Briefcase." });
            }

            if (pinCode != settings.GoldBriefcasePinCode)
            {
                return Json(new Status { Success = false, ErrorMessage = "The code you entered isn't right." });
            }

            settings.GoldBriefcasePinCode = -1;
            _settingsRepository.SetSetting(user, nameof(settings.GoldBriefcasePinCode), settings.GoldBriefcasePinCode.ToString());

            if (_pointsRepository.AddPoints(user, 10000))
            {
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully opened the Gold Briefcase and found {Helper.GetFormattedPoints(10000)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "The briefcase couldn't be opened." });
        }

        [HttpGet]
        public IActionResult GetMuseumEastWingPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.East);
            return PartialView("_MuseumEastWingPartial", museumViewModel);
        }

        [HttpGet]
        public IActionResult GetMuseumWestWingPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.West);
            return PartialView("_MuseumWestWingPartial", museumViewModel);
        }

        [HttpGet]
        public IActionResult GetMuseumSouthWingPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.South);
            return PartialView("_MuseumSouthWingPartial", museumViewModel);
        }

        [HttpGet]
        public IActionResult GetMuseumBoutiquePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MuseumExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Museum is no longer open." });
            }

            MuseumViewModel museumViewModel = GenerateModel(user, MuseumWing.Boutique);
            return PartialView("_MuseumBoutiquePartial", museumViewModel);
        }

        private MuseumViewModel GenerateModel(User user, MuseumWing museumWing)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            MuseumViewModel museumViewModel = new MuseumViewModel
            {
                Title = "Museum",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Museum,
                CurrentWing = museumWing,
                MuseumDisplays = _museumRepository.GetMuseumDisplays(user, settings, museumWing),
                EastWingUnlocked = accomplishments.Any(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked),
                WestWingUnlocked = accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked),
                SouthWingUnlocked = accomplishments.Any(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked),
                ExpiryDate = settings.MuseumExpiryDate
            };

            if (museumWing == MuseumWing.West && !museumViewModel.WestWingUnlocked)
            {
                museumViewModel.Items = _itemsRepository.GetItems(user, true).FindAll(x => x.GetAgeInHours() >= 500);
            }
            if (museumWing == MuseumWing.South && !museumViewModel.SouthWingUnlocked)
            {
                museumViewModel.HasStreakAccomplishment = accomplishments.Any(x => x.Id == AccomplishmentId.StreakCards5DayStreak);
            }
            if (museumWing == MuseumWing.Boutique)
            {
                Occasion holiday = Helper.GetCurrentOccasion();
                museumViewModel.Items = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).FindAll(x => !x.IsExclusive || x.Name.Equals(holiday.GetDisplayName(), StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(x => x.IsExclusive).ThenByDescending(x => x.Points).ToList();
                museumViewModel.CanBuyBoutiqueItem = (DateTime.UtcNow - settings.LastBoutiquePurchaseDate).TotalHours >= 1;
            }
            if (museumWing == MuseumWing.Records)
            {
                museumViewModel.RarestAccomplishment = _accomplishmentsRepository.GetAccomplishmentsRankings().FindAll(x => x.Category > AccomplishmentCategory.Orange).LastOrDefault();
                museumViewModel.OldestItem = _itemsRepository.GetTopOldestItems(false).FirstOrDefault();
                museumViewModel.LongestOwnedItem = _itemsRepository.GetTopLongestOwnedItems(false).OrderBy(x => x.CreatedDate).FirstOrDefault();
                museumViewModel.TopAttentionHallPoster = _attentionHallRepository.GetTopAttentionHallPosters().FirstOrDefault();
                museumViewModel.TopAttentionHallCommenter = _attentionHallRepository.GetTopAttentionHallCommenters().FirstOrDefault();
            }

            return museumViewModel;
        }
    }
}
