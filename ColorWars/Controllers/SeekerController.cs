﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SeekerController : Controller
    {
        private IPointsRepository _pointsRepository;
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISeekerRepository _seekerRepository;
        private IGroupsRepository _groupsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public SeekerController(IPointsRepository pointsRepository, ILoginRepository loginRepository, IItemsRepository itemsRepository, ISeekerRepository seekerRepository,
        IGroupsRepository groupsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _pointsRepository = pointsRepository;
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _seekerRepository = seekerRepository;
            _groupsRepository = groupsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Seeker" });
            }

            SeekerViewModel seekerViewModel = GenerateModel(user);
            seekerViewModel.UpdateViewData(ViewData);
            return View(seekerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Redeem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (DateTime.UtcNow >= settings.SeekerChallengeExpiryDate)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Seeker Challenge has already expired." });
            }

            List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
            List<SeekerChallenge> seekerChallenges = _seekerRepository.GetSeekerChallenges(user);
            List<Item> challengeItems = GetChallengeItems(allItems, seekerChallenges);
            Item rewardItem = allItems.Find(x => x.Id == seekerChallenges.First().RewardId);
            rewardItem.Theme = seekerChallenges.First().RewardTheme;

            if (challengeItems.Count == 0 || rewardItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Seeker didn't respond.", ShouldRefresh = true });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            List<Item> matchingItems = new List<Item>();
            foreach (Item item in challengeItems)
            {
                Item matchingItem = items.Find(x => x.Id == item.Id && x.Theme == item.Theme);
                if (matchingItem != null)
                {
                    matchingItems.Add(matchingItem);
                    items.RemoveAll(x => x.SelectionId == matchingItem.SelectionId);
                }
            }

            if (matchingItems.Count != challengeItems.Count)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have every item The Seeker requested." });
            }

            _seekerRepository.DeleteSeekerChallenges(user);

            foreach (Item matchingItem in matchingItems)
            {
                _itemsRepository.RemoveItem(user, matchingItem);
            }
            _itemsRepository.AddItem(user, rewardItem);

            settings.SeekerLevel += 1;
            _settingsRepository.SetSetting(user, nameof(settings.SeekerLevel), settings.SeekerLevel.ToString());

            settings.SeekerChallengeExpiryDate = DateTime.UtcNow.AddDays(3);
            _settingsRepository.SetSetting(user, nameof(settings.SeekerChallengeExpiryDate), settings.SeekerChallengeExpiryDate.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstSeeker))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstSeeker);
            }
            if (settings.SeekerLevel + 1 == 10)  //as levels start at 0
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RankTenSeeker))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RankTenSeeker);
                }
            }
            else if (settings.SeekerLevel + 1 == 50)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RankFiftySeeker))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RankFiftySeeker);
                }
            }
            else if (settings.SeekerLevel + 1 == 100)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RankOneHundredSeeker))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RankOneHundredSeeker);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The Seeker successfully took your item{(matchingItems.Count == 1 ? "" : "s")} and, in return, added the item \"{rewardItem.Name}\" to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Skip()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int skipCost = (settings.SeekerLevel + 1) * 1000;
            if (user.Points < skipCost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} to skip this task." });
            }

            if (_pointsRepository.SubtractPoints(user, skipCost))
            {
                settings.SeekerChallengeExpiryDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.SeekerChallengeExpiryDate), settings.SeekerChallengeExpiryDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully skipped this task for {Helper.GetFormattedPoints(skipCost)}!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "This task could not be skipped." });
        }

        [HttpGet]
        public IActionResult GetSeekerPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            SeekerViewModel seekerViewModel = GenerateModel(user);
            return PartialView("_SeekerMainPartial", seekerViewModel);
        }

        private SeekerViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            int seekerLevel = settings.SeekerLevel;

            GroupPower groupPower = _groupsRepository.GetGroupPower(user);
            bool hasBetterSeekerOffersGroupPower = groupPower == GroupPower.BetterSeekerOffers;

            List<Item> challengeItems;
            Item rewardItem;
            List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);

            bool challengeExpired = false;
            DateTime expiryDate = settings.SeekerChallengeExpiryDate;
            if (DateTime.UtcNow >= expiryDate)
            {
                _seekerRepository.DeleteSeekerChallenges(user);
                settings.SeekerChallengeExpiryDate = DateTime.UtcNow.AddDays(3);
                _settingsRepository.SetSetting(user, nameof(settings.SeekerChallengeExpiryDate), settings.SeekerChallengeExpiryDate.ToString());
                challengeExpired = true;
            }

            List<SeekerChallenge> seekerChallenges = challengeExpired ? null : _seekerRepository.GetSeekerChallenges(user);
            if (seekerChallenges?.Count > 3)
            {
                _seekerRepository.DeleteSeekerChallenges(user);
                seekerChallenges = null;
            }

            if (seekerChallenges == null || seekerChallenges.Count == 0)
            {
                challengeItems = GenerateChallengeItems(allItems, seekerLevel);
                rewardItem = GenerateReward(allItems.FindAll(x => !x.IsExclusive), challengeItems, seekerLevel, hasBetterSeekerOffersGroupPower);
                foreach (Item item in challengeItems)
                {
                    _seekerRepository.AddSeekerChallenge(user, item, rewardItem);
                }
            }
            else
            {
                challengeItems = GetChallengeItems(allItems, seekerChallenges);
                rewardItem = allItems.Find(x => x.Id == seekerChallenges.First().RewardId);
                rewardItem.Theme = seekerChallenges.First().RewardTheme;
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            foreach (Item item in challengeItems)
            {
                Item matchingItem = items.Find(x => x.Id == item.Id && x.Theme == item.Theme);
                if (matchingItem != null)
                {
                    item.SelectionId = matchingItem.SelectionId;
                    items.RemoveAll(x => x.SelectionId == matchingItem.SelectionId);
                }
            }

            SeekerViewModel seekerViewModel = new SeekerViewModel
            {
                Title = "Seeker's Hut",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.SeekersHut,
                Level = seekerLevel,
                ChallengeItems = challengeItems.OrderBy(x => x.Points).ToList(),
                RewardItem = rewardItem,
                HasSeekerGroupPower = hasBetterSeekerOffersGroupPower,
                HasChallengeExpired = challengeExpired && expiryDate != DateTime.MinValue,
                ExpiryDate = settings.SeekerChallengeExpiryDate
            };

            return seekerViewModel;
        }

        private List<Item> GetChallengeItems(List<Item> allItems, List<SeekerChallenge> seekerChallenges)
        {
            List<Item> challengeItems = new List<Item>();
            foreach (SeekerChallenge seekerChallenge in seekerChallenges)
            {
                Item challengeItem = allItems.Find(x => x.Id == seekerChallenge.ItemId).DeepCopy();
                if (challengeItem != null)
                {
                    challengeItem.Theme = seekerChallenge.ItemTheme;
                    challengeItems.Add(challengeItem);
                }
            }

            return challengeItems;
        }

        private List<Item> GenerateChallengeItems(List<Item> allItems, int seekerLevel)
        {
            Random rnd = new Random();
            int points = 750 * rnd.Next(2, 6) * (seekerLevel + 1);
            List<Item> items = allItems.FindAll(x => x.Points <= points * 1.45);
            items.RemoveAll(x => x.Category == ItemCategory.Custom);
            if (seekerLevel <= 75)
            {
                items = items.FindAll(x => !x.IsExclusive);
            }

            List<Item> challengeItems = new List<Item>();
            int count = 0;
            while (count < 3 && points > 0)
            {
                Item item = items[rnd.Next(items.Count)].DeepCopy();
                if (item.Category == ItemCategory.Deck && rnd.Next(Math.Max(100 - seekerLevel, 5)) == 1)
                {
                    ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    ItemTheme theme = themes[rnd.Next(themes.Length)];
                    item.Theme = theme;
                }
                if (item.IsExclusive)
                {
                    item.Points *= 2;
                }
                points -= item.Points;
                challengeItems.Add(item);
                count++;
            }

            return challengeItems;
        }

        private Item GenerateReward(List<Item> allItems, List<Item> challengeItems, int seekerLevel, bool hasBetterSeekerReward)
        {
            Random rnd = new Random();
            int challengePoints = challengeItems.Sum(x => x.Points);
            double rewardPointsVariation = hasBetterSeekerReward ? 2 : 1.75;
            if (challengeItems.Any(x => x.Theme > ItemTheme.Default))
            {
                rewardPointsVariation *= 1.25;
            }
            int totalThemedChallengeItems = challengeItems.FindAll(x => x.Theme > ItemTheme.Default).Count;

            Item rewardItem = allItems.Aggregate((x, y) => Math.Abs(x.Points - (challengePoints * rewardPointsVariation)) < Math.Abs(y.Points - (challengePoints * rewardPointsVariation)) ? x : y);
            if (rewardItem.Category == ItemCategory.Deck && rnd.Next(Math.Max(100 / (totalThemedChallengeItems + 1) - seekerLevel, 20)) == 1)
            {
                ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                ItemTheme theme = themes[rnd.Next(themes.Length)];
                rewardItem.Theme = theme;
            }

            return rewardItem;
        }
    }
}
