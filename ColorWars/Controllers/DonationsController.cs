﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class DonationsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IDonationsRepository _donationsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private ISoundsRepository _soundsRepository;

        public DonationsController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IDonationsRepository donationsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, IAdminSettingsRepository adminSettingsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _donationsRepository = donationsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Donations" });
            }

            DonationsViewModel donationsViewModel = GenerateModel(user, true);
            donationsViewModel.UpdateViewData(ViewData);
            return View(donationsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Donate(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems you no longer have this item." });
            }

            if (item.Category == ItemCategory.Custom)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot donate a Custom Card to the Donation Zone." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (_donationsRepository.AddDonationItem(user, item))
            {
                settings.DonatedItems++;
                _settingsRepository.SetSetting(user, nameof(settings.DonatedItems), settings.DonatedItems.ToString());

                _itemsRepository.RemoveItem(user, item);
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstDonation))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstDonation);
            }
            if (item.Points >= 50000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigDonation))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigDonation);
                }
            }
            if (settings.DonatedItems >= 10)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DonatedTenItems))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DonatedTenItems);
                }
            }
            if (settings.DonatedItems >= 50)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DonatedFiftyItems))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DonatedFiftyItems);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully donated your item \"{item.Name}\"!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Take(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int minutesSinceTakingLastDonation = (int)(DateTime.UtcNow - settings.DonationsTakeDate).TotalMinutes;
            if (minutesSinceTakingLastDonation < 60)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You already took an item recently. Try again in {60 - minutesSinceTakingLastDonation} minute{(60 - minutesSinceTakingLastDonation == 1 ? "" : "s")}." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to take another item." });
            }

            List<Item> items = _donationsRepository.GetDonationItems().FindAll(x => x.Condition < ItemCondition.Unusable);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer available." });
            }

            settings.DonationsTakeDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.DonationsTakeDate), settings.DonationsTakeDate.ToString());

            _itemsRepository.AddItem(user, item);
            _donationsRepository.DeleteDonationItem(item);

            if (item.Username == user.Username)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DonationReturn))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DonationReturn);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetDonationsPartialView(bool isTaking)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            DonationsViewModel donationsViewModel = GenerateModel(user, isTaking);
            if (isTaking)
            {
                return PartialView("_DonationsMainPartial", donationsViewModel);
            }
            return PartialView("_DonationsDonatePartial", donationsViewModel);
        }

        private DonationsViewModel GenerateModel(User user, bool isTaking)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            DonationsViewModel donationsViewModel = new DonationsViewModel
            {
                Title = "Donation Zone",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.DonationZone,
                CanTakeDonation = (DateTime.UtcNow - settings.DonationsTakeDate).TotalMinutes >= 60
            };
            donationsViewModel.Items = isTaking ? _donationsRepository.GetDonationItems().Take(40).ToList() : _itemsRepository.GetItems(user, true);

            if (isTaking)
            {
                Random rnd = new Random();
                if (donationsViewModel.Items.Count < 10)
                {
                    Task.Run(() =>
                    {
                        AdminSettings adminSettings = _adminSettingsRepository.GetSettings(false);
                        if ((DateTime.UtcNow - adminSettings.LastDonationZoneItemAdded).TotalMinutes >= 5)
                        {
                            adminSettings.LastDonationZoneItemAdded = DateTime.UtcNow;
                            _adminSettingsRepository.SetSetting(nameof(adminSettings.LastDonationZoneItemAdded), adminSettings.LastDonationZoneItemAdded.ToString());
                            List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points >= 500 && x.Points <= 50000);
                            Item item = items[rnd.Next(items.Count)];
                            _donationsRepository.AddSpecialDonationItem(item);
                        }
                    });
                }
            }

            return donationsViewModel;
        }
    }
}
