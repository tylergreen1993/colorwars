﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TopCounterController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public TopCounterController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IItemsRepository itemsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TopCounter" });
            }

            TopCounterViewModel topCounterViewModel = GenerateModel(user);
            topCounterViewModel.UpdateViewData(ViewData);
            return View(topCounterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Send(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "The amount must be greater than 0." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LastTopCounterPlayDate).TotalHours < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played too recently. Try again in {settings.LastTopCounterPlayDate.AddHours(1).GetTimeUntil()}." });
            }

            settings.LastTopCounterPlayDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastTopCounterPlayDate), settings.LastTopCounterPlayDate.ToString());

            List<Card> cards = GetCards(user, settings);
            int totalAmount = cards.Sum(x => x.Amount);

            settings.TopCounterCards = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.TopCounterCards), settings.TopCounterCards);

            string soundUrl = "";

            if (amount == totalAmount)
            {
                _pointsRepository.AddPoints(user, 500);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TopCounterWon))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TopCounterWon);
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully got the amount right and earned {Helper.GetFormattedPoints(500)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

            return Json(new Status { Success = true, DangerMessage = $"You got it wrong! The correct amount was {Helper.FormatNumber(totalAmount)}. Better luck next time!", SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetTopCounterPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TopCounterViewModel topCounterViewModel = GenerateModel(user);
            return PartialView("_TopCounterMainPartial", topCounterViewModel);
        }

        private TopCounterViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            TopCounterViewModel topCounterViewModel = new TopCounterViewModel
            {
                Title = "Top Counter",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                CanPlay = (DateTime.UtcNow - settings.LastTopCounterPlayDate).TotalHours >= 1,
                LastPlayDate = settings.LastTopCounterPlayDate,
            };

            if (topCounterViewModel.CanPlay)
            {
                topCounterViewModel.Cards = GetCards(user, settings);
            }

            return topCounterViewModel;
        }

        private List<Card> GetCards(User user, Settings settings)
        {
            List<Card> cards = _itemsRepository.GetAllCards();
            List<Card> displayedCards = new List<Card>();
            string cardString = settings.TopCounterCards;
            if (string.IsNullOrEmpty(cardString))
            {
                Random rnd = new Random();
                for (int i = 0; i < 15; i++)
                {
                    List<Card> choosableCards = rnd.Next(4) < 3 ? cards.FindAll(x => x.Amount <= 50) : cards;
                    displayedCards.Add(choosableCards[rnd.Next(choosableCards.Count)]);
                }
                settings.TopCounterCards = string.Join("|", displayedCards.Select(x => x.Amount));
                _settingsRepository.SetSetting(user, nameof(settings.TopCounterCards), settings.TopCounterCards);
            }
            else
            {
                List<int> cardsAmount = cardString.Split("|").Select(x => int.Parse(x)).ToList();
                foreach (int amount in cardsAmount)
                {
                    displayedCards.Add(cards.Find(x => x.Amount == amount));
                }
            }
            return displayedCards;
        }
    }
}
