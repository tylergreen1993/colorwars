﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StadiumController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IRepairRepository _repairRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IItemsRepository _itemsRepository;
        private IMatchStateRepository _matchStateRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private IUserRepository _userRepository;
        private IRightSideUpWorldRepository _rightSideUpWorldRepository;
        private ILibraryRepository _libraryRepository;
        private IPartnershipRepository _partnershipRepository;
        private IBlockedRepository _blockedRepository;
        private ISettingsRepository _settingsRepository;
        private ICPURepository _cpuRepository;
        private IRelicsRepository _relicsRepository;
        private ITourneyRepository _tourneyRepository;
        private ITowerRepository _towerRepository;
        private IMessagesRepository _messagesRepository;
        private INotificationsRepository _notificationsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public StadiumController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IRepairRepository repairRepository, IPromoCodesRepository promoCodesRepository,
        IItemsRepository itemsRepository, IMatchStateRepository matchStateRepository, IUserRepository userRepository, IBlockedRepository blockedRepository,
        ISettingsRepository settingsRepository, ICPURepository cpuRepository, IMatchHistoryRepository matchHistoryRepository, IPowerMovesRepository powerMovesRepository,
        IRelicsRepository relicsRepository, ITourneyRepository tourneyRepository, ITowerRepository towerRepository, IRightSideUpWorldRepository rightSideUpWorldRepository, ILibraryRepository libraryRepository,
        IPartnershipRepository partnershipRepository, IMessagesRepository messagesRepository, INotificationsRepository notificationsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _repairRepository = repairRepository;
            _promoCodesRepository = promoCodesRepository;
            _itemsRepository = itemsRepository;
            _matchStateRepository = matchStateRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _powerMovesRepository = powerMovesRepository;
            _userRepository = userRepository;
            _rightSideUpWorldRepository = rightSideUpWorldRepository;
            _libraryRepository = libraryRepository;
            _partnershipRepository = partnershipRepository;
            _blockedRepository = blockedRepository;
            _settingsRepository = settingsRepository;
            _cpuRepository = cpuRepository;
            _messagesRepository = messagesRepository;
            _notificationsRepository = notificationsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _relicsRepository = relicsRepository;
            _tourneyRepository = tourneyRepository;
            _towerRepository = towerRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(bool fromChallenge, StadiumTab stadiumTab)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Stadium" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage < IntroStage.Third)
            {
                return RedirectToAction("Index", "Intro");
            }
            if (settings.CurrentIntroStage == IntroStage.Third)
            {
                List<Card> deckCards = _itemsRepository.GetCards(user, true);
                if (deckCards.Any(x => x.Condition != ItemCondition.New))
                {
                    _itemsRepository.RepairAllItems(user);
                }
                if (!_matchStateRepository.IsInMatch(user))
                {
                    StartMatch(0, true, false, string.Empty, false, 0);
                }
            }

            StadiumViewModel stadiumViewModel = GenerateModel(user, fromChallenge, stadiumTab);
            stadiumViewModel.UpdateViewData(ViewData);
            return View(stadiumViewModel);
        }

        public IActionResult Statistics(string id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (string.IsNullOrEmpty(id) && user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Stadium/Statistics" });
            }

            User profile = string.IsNullOrEmpty(id) ? user : _userRepository.GetUser(id);
            if (profile == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (user != null && user.Username != profile.Username)
            {
                List<Blocked> blockedUsers = _blockedRepository.GetBlocked(profile, false);
                if (blockedUsers.Exists(x => x.BlockedUser == user.Username))
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(profile, false, user != null && user.Username == profile.Username);
            Settings settings = _settingsRepository.GetSettings(profile, user != null && user.Username == profile.Username);
            if (settings.OnlyShowUserMatchesStatistics)
            {
                matchHistories = matchHistories.FindAll(x => x.CPUType == CPUType.None);
            }
            int totalCPULevels = _cpuRepository.GetCPUCount();
            int cpuLevel = (settings.MatchLevel % totalCPULevels) + 1;
            int intensity = settings.MatchLevel / totalCPULevels;
            MatchRanking matchRanking = _matchHistoryRepository.GetMatchRanking(profile);

            StadiumStatisticsViewModel stadiumStatisticsViewModel = new StadiumStatisticsViewModel
            {
                Title = "Stadium",
                User = user,
                Profile = profile,
                CPU = _cpuRepository.GetCPU(settings.MatchLevel),
                MatchHistories = matchHistories,
                CPULevel = cpuLevel,
                TotalCPULevels = totalCPULevels,
                Intensity = intensity,
                TotalCPUs = totalCPULevels,
                CPULevelPercent = Math.Round((double)(cpuLevel - 1) / totalCPULevels * 100, 1),
                UserWinPercent = GetUserWinPercent(matchHistories),
                MatchRanking = matchRanking,
                IsElite = _matchHistoryRepository.IsElite(profile, matchRanking),
                OnlyShowUserMatches = settings.OnlyShowUserMatchesStatistics
            };

            if (user == null || user.Username != profile.Username)
            {
                stadiumStatisticsViewModel.Parent = LocationArea.Profile.ToString();
                stadiumStatisticsViewModel.ParentUrl = $"User/{stadiumStatisticsViewModel.Profile.Username}";
            }

            stadiumStatisticsViewModel.UpdateViewData(ViewData);
            return View(stadiumStatisticsViewModel);
        }

        public new IActionResult Challenge()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Stadium/Challenge" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.StadiumChallengeExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Stadium", new { fromChallenge = true });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                MatchState matchState = GetNextMatchState(user);
                if (matchState != null && !matchState.IsCPUChallenge)
                {
                    Messages.AddSnack("You're already in an active Stadium match.", true);
                }
                return RedirectToAction("Index", "Stadium");
            }

            if (!settings.CheckedStadiumChallenge)
            {
                settings.CheckedStadiumChallenge = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedStadiumChallenge), settings.CheckedStadiumChallenge.ToString());
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);

            StadiumChallengeViewModel stadiumChallengeViewModel = new StadiumChallengeViewModel
            {
                Title = "Stadium",
                User = user,
                ChallengeExpiryDate = settings.StadiumChallengeExpiryDate,
                Challenge = settings.StadiumChallenge,
                HasDoubleStadiumCPUReward = settings.HasDoubleStadiumCPUReward,
                Reward = Math.Min(15000, (settings.StadiumChallengeWins + 1) * 1000),
                Deck = deck,
                IsDeckAllowed = !IsBadChallengeDeck(deck, settings.StadiumChallenge),
                MatchLevel = settings.MatchLevel
            };

            stadiumChallengeViewModel.UpdateViewData(ViewData);
            return View(stadiumChallengeViewModel);
        }

        public IActionResult ChallengeUser(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Stadium/ChallengeUser?username={username}" });
            }

            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
            {
                return RedirectToAction("Index", "Stadium");
            }

            User challengeUser = _userRepository.GetUser(username);
            if (challengeUser == null)
            {
                return RedirectToAction("Index", "Stadium");
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                MatchState matchState = GetNextMatchState(user);
                if (matchState != null)
                {
                    if (matchState.Opponent != username)
                    {
                        Messages.AddSnack($"You're already in a Stadium match with {matchState.Opponent}!", true);
                    }
                }
                return RedirectToAction("Index", "Stadium");
            }

            MatchWaitlist matchWaitlist = _matchStateRepository.GetMatchWaitlist(challengeUser);
            bool hasPendingRequest = matchWaitlist != null && matchWaitlist.ChallengeUser == user.Username;

            Settings settings = _settingsRepository.GetSettings(challengeUser, false);
            if (!settings.AllowUserChallenges && !hasPendingRequest)
            {
                return RedirectToAction("Index", "Stadium");
            }

            if (_blockedRepository.HasBlockedUser(user, challengeUser))
            {
                return RedirectToAction("Index", "Stadium");
            }

            if (_blockedRepository.HasBlockedUser(challengeUser, user))
            {
                return RedirectToAction("Index", "Stadium");
            }

            if (_matchStateRepository.IsInMatch(challengeUser, true, false))
            {
                Messages.AddSnack($"{challengeUser.Username} is already in a match!", true);
                return RedirectToAction("Index", "Stadium");
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            bool isDeckAllowed = deck.Count == Helper.GetMaxDeckSize() && deck.All(x => x.Condition < ItemCondition.Unusable);

            StadiumChallengeUserViewModel stadiumChallengeUserViewModel = new StadiumChallengeUserViewModel
            {
                Title = "Stadium",
                User = user,
                ChallengeUser = challengeUser,
                Deck = deck,
                IsDeckAllowed = isDeckAllowed,
                HasPendingRequest = hasPendingRequest,
                MatchLevel = _settingsRepository.GetSettings(user).MatchLevel
            };

            stadiumChallengeUserViewModel.UpdateViewData(ViewData);
            return View(stadiumChallengeUserViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StartMatch(int betAmount, bool isCPUMatch, bool isCPUChallenge, string challengeUsername, bool isSkipped, int tourneyId, bool includeReturnUrl = false, bool isRematch = false)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            string opponentName = string.Empty;
            TourneyMatch mostRecentTourneyMatch = null;
            Settings settings = _settingsRepository.GetSettings(user);
            if (isCPUMatch)
            {
                if (isCPUChallenge)
                {
                    if (settings.StadiumChallengeExpiryDate <= DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You do not have an active Stadium Challenge." });
                    }
                    if (IsBadChallengeDeck(deck, settings.StadiumChallenge))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your Deck needs to be changed to meet this challenge's criteria." });
                    }
                }
                else
                {
                    CPU cpu = _cpuRepository.GetCPU(settings.MatchLevel);
                    int totalCPUs = _cpuRepository.GetCPUCount();
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!_cpuRepository.CanChallengeCheckpointCPU(user, accomplishments, cpu, out _, out string reason, out string reasonButtonText, out string reasonButtonUrl))
                    {
                        return Json(new Status { Success = false, ErrorMessage = reason, ButtonText = reasonButtonText, ButtonUrl = reasonButtonUrl, ShowSpecialMessage = true, ShouldRefresh = isSkipped });
                    }
                    if (settings.MatchLevel >= totalCPUs && !accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You can only rematch all the CPU Challengers once you've defeated the Right-Side Up Guard.", ButtonText = "Head to the Guard!", ButtonUrl = "/TrainingCenter/Plateau", ShouldRefresh = isSkipped });
                    }
                }
            }
            else
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Any(x => x.Id == AccomplishmentId.FirstCPUMatch))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"Try challenging a CPU before challenging a user." });
                }
                if (tourneyId != 0)
                {
                    Tourney tourney = _tourneyRepository.GetTourneyWithId(tourneyId);
                    if (tourney == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This tourney no longer exists." });
                    }
                    mostRecentTourneyMatch = tourney.TourneyMatches.FindAll(x => x.Username1 == user.Username || x.Username2 == user.Username).LastOrDefault();
                    if (mostRecentTourneyMatch == null || !string.IsNullOrEmpty(mostRecentTourneyMatch.GetWinner()))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You're not in this tourney anymore." });
                    }

                    challengeUsername = user.Username == mostRecentTourneyMatch.Username1 ? mostRecentTourneyMatch.Username2 : mostRecentTourneyMatch.Username1;
                    if (challengeUsername == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have anyone to challenge at this time." });
                    }
                    User challengeUser = _userRepository.GetUser(challengeUsername);
                    if (challengeUser == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This user no longer exists." });
                    }

                    MatchWaitlist matchWaitlist = _matchStateRepository.GetMatchWaitlist(challengeUser);

                    EndMatch();
                    opponentName = FindOpponent(user, 0, false, challengeUser.Username, mostRecentTourneyMatch.Id);

                    if (!isRematch && (matchWaitlist == null || matchWaitlist.ChallengeUser != user.Username))
                    {
                        _notificationsRepository.AddNotification(challengeUser, $"@{user.Username} has challenged you to your \"{tourney.Name}\" tourney match! You have 2 minutes before this invitation expires.", NotificationLocation.Tourney, $"/Tourney?id={tourney.Id}");
                    }
                }
                else if (string.IsNullOrEmpty(challengeUsername))
                {
                    if (user.Points < Helper.GetMinMatchPoints() * 11)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You need at least {Helper.GetFormattedPoints(Helper.GetMinMatchPoints() * 11)} on hand." });
                    }
                    if (user.Points < betAmount)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency}." });
                    }
                    if (betAmount < Helper.GetMinMatchPoints())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You can't put less than {Helper.GetFormattedPoints(50)} on the match." });
                    }

                    bool isElite = _matchHistoryRepository.IsElite(user);

                    int maxPoints = Math.Min(user.Points / (isElite ? 3 : 10), isElite ? 25000 : 10000);
                    if (betAmount > maxPoints)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You can only put up to {Helper.GetFormattedPoints(maxPoints)} on the match." });
                    }

                    opponentName = FindOpponent(user, betAmount, isElite, null, Guid.Empty);
                }
                else
                {
                    if (challengeUsername.Equals(user.Username))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge yourself to a Stadium match." });
                    }
                    User challengeUser = _userRepository.GetUser(challengeUsername);
                    if (challengeUser == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This user no longer exists." });
                    }

                    MatchWaitlist matchWaitlist = _matchStateRepository.GetMatchWaitlist(challengeUser);
                    bool hasPendingRequest = matchWaitlist != null && matchWaitlist.ChallengeUser == user.Username;

                    Settings challengeUserSettings = _settingsRepository.GetSettings(challengeUser, false);
                    if ((!challengeUserSettings.AllowUserChallenges && !hasPendingRequest) || _blockedRepository.HasBlockedUser(user, challengeUser) || _blockedRepository.HasBlockedUser(challengeUser, user))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge this user to a Stadium match." });
                    }

                    EndMatch();
                    opponentName = FindOpponent(user, 0, false, challengeUser.Username, Guid.Empty);

                    if (matchWaitlist == null || matchWaitlist.ChallengeUser != user.Username)
                    {
                        _notificationsRepository.AddNotification(challengeUser, $"@{user.Username} has challenged you to a Stadium match! You have 2 minutes to accept the invitation.", NotificationLocation.Stadium, $"/Stadium/ChallengeUser?username={user.Username}");
                    }
                }
            }

            if (isCPUMatch || !string.IsNullOrEmpty(opponentName))
            {
                InitializeMatch(user, deck, betAmount, isCPUMatch, isCPUChallenge, null, opponentName, mostRecentTourneyMatch?.Id ?? Guid.Empty);
            }

            return Json(new Status { Success = true, Points = Helper.GetFormattedPoints(user.Points), ReturnUrl = includeReturnUrl ? "/Stadium" : "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EnterStadium()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.StadiumTutorialLevel >= 0)
            {
                return Json(new Status { Success = true, ShouldRefresh = true });
            }

            settings.StadiumTutorialLevel++;
            _settingsRepository.SetSetting(user, nameof(settings.StadiumTutorialLevel), settings.StadiumTutorialLevel.ToString());

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Duel(Guid deckCardId, Guid enhancerId, PowerMoveType powerMoveId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
            MatchState matchState = GetNextMatchState(user, matchStates);
            if (matchState == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "The match has already finished.", ShouldRefresh = true });
            }

            if (!matchState.IsCPU)
            {
                int secondsLeft = matchStates.OrderByDescending(x => x.UpdatedDate).First().GetSecondsLeft();
                if (secondsLeft <= 0)
                {
                    return Json(new Status { Success = false, ShouldRefresh = true, ErrorMessage = "The match ran out of time." });
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (matchState.CardId == Guid.Empty && deckCardId != Guid.Empty)
            {
                List<Card> deck = _itemsRepository.GetCards(user, true);
                Card selectedCard = deck.Find(x => x.SelectionId == deckCardId);
                if (selectedCard != null)
                {
                    List<MatchState> playedMatchStates = matchStates.FindAll(x => x.Status != MatchStatus.NotPlayed);
                    if (matchStates.Count > Helper.GetMaxDeckSize() || !playedMatchStates.Any(x => x.CardId == selectedCard.SelectionId))
                    {
                        matchState.CardId = selectedCard.SelectionId;
                    }
                }
            }

            if (matchState.CardId == Guid.Empty)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must choose a {Resources.DeckCard} to play." });
            }

            if (enhancerId != Guid.Empty)
            {
                if (settings.StadiumIsConfused)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot choose an Enhancer as you're confused." });
                }
                List<Enhancer> enhancers = GetEnhancersForMatch(user, settings, matchState.IsCPUChallenge, settings.StadiumChallenge, false);
                Enhancer selectedEnhancer = enhancers.Find(x => x.SelectionId == enhancerId);
                if (selectedEnhancer == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"Your chosen Enhancer no longer exists." });
                }

                matchState.EnhancerId = selectedEnhancer.Id;
                if (selectedEnhancer.Uses > 0 && settings.LavenderStopExpiryDate <= DateTime.UtcNow)
                {
                    selectedEnhancer.Uses -= 1;
                    _itemsRepository.UpdateItem(user, selectedEnhancer);
                }
                if (selectedEnhancer.Uses != settings.StadiumEnhancerUses)
                {
                    settings.StadiumEnhancerUses = selectedEnhancer.Uses;
                    _settingsRepository.SetSetting(user, nameof(settings.StadiumEnhancerUses), settings.StadiumEnhancerUses.ToString());
                }
            }

            if (matchState.PowerMove == PowerMoveType.None && powerMoveId != PowerMoveType.None)
            {
                PowerMove powerMove = _powerMovesRepository.GetPowerMoves(user, matchStates, settings.MatchLevel).Find(x => x.Type == powerMoveId);
                if (powerMove == null || !powerMove.IsActive || powerMove.IsDisabled)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"That Power Move is not available at this time." });
                }

                Random rnd = new Random();
                if (powerMoveId == PowerMoveType.AllOutAttack && !settings.StadiumHasLuckUp && rnd.Next(2) == 1)
                {
                    powerMoveId = PowerMoveType.AllOutAttackNegative;
                }
                else if (powerMoveId == PowerMoveType.LadyLuck && !settings.StadiumHasLuckUp && settings.MatchLevel > 2 && rnd.Next(2) == 1)
                {
                    powerMoveId = PowerMoveType.LadyLuckNegative;
                }
                else if (powerMoveId == PowerMoveType.BlockersBane && !settings.StadiumHasLuckUp && rnd.Next(4) == 1)
                {
                    powerMoveId = PowerMoveType.BlockersBaneNegative;
                }
                else if (powerMoveId == PowerMoveType.BigShot && !settings.StadiumHasLuckUp && rnd.Next(2) == 1)
                {
                    powerMoveId = PowerMoveType.BigShotNegative;
                }

                matchState.PowerMove = powerMoveId;
            }

            if (matchState.IsCPU)
            {
                settings ??= _settingsRepository.GetSettings(user);
                PowerMoveType preChosenPowerMove = matchState.CPUPowerMove;
                if (preChosenPowerMove == PowerMoveType.None)
                {
                    matchState.CPUPowerMove = _cpuRepository.GetPowerMove(user, matchStates, settings);
                }
                Guid cpuEnhancerId = matchState.CPUEnhancerId;
                matchState = _cpuRepository.EnhanceCPULogic(user, matchStates, matchState, settings);
                if (matchState.CPUPowerMove == PowerMoveType.None || (cpuEnhancerId != Guid.Empty && matchState.CPUEnhancerId == Guid.Empty && matchState.CPUPowerMove != PowerMoveType.SelfDestruct && matchState.CPUPowerMove != PowerMoveType.EnhancerSwap)) //try once more for a new option
                {
                    matchState.CPUEnhancerId = cpuEnhancerId;
                    matchState.CPUPowerMove = _cpuRepository.GetPowerMove(user, matchStates, settings);
                    matchState = _cpuRepository.EnhanceCPULogic(user, matchStates, matchState, settings);
                }
                if (preChosenPowerMove != PowerMoveType.None && matchState.CPUPowerMove != preChosenPowerMove)
                {
                    matchState.CPUPowerMove = preChosenPowerMove;
                }
            }

            matchState.IsInSelection = false;
            _matchStateRepository.UpdateMatchState(user, matchState);

            return Json(new Status { Success = true, Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NextRound()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            StadiumViewModel stadiumViewModel = GenerateModel(user);
            if (stadiumViewModel.SecondsLeft <= 0 && !stadiumViewModel.IsCPU)
            {
                return Json(new Status { Success = false, ShouldRefresh = true, ErrorMessage = "The match ran out of time." });
            }

            MatchState matchState = stadiumViewModel.MatchState;
            if (matchState == null || matchState.IsInSelection)
            {
                return Json(new Status { Success = false, ErrorMessage = "The match has already finished.", ShouldRefresh = true });
            }

            matchState.Status = stadiumViewModel.RoundResult;
            _matchStateRepository.UpdateMatchState(user, matchState);

            stadiumViewModel.Results[matchState.Position] = matchState.Status;
            MatchStatus finalStatus = _matchStateRepository.GetFinalStatus(stadiumViewModel.Results);

            Settings settings = _settingsRepository.GetSettings(user);
            int cpusCount = _cpuRepository.GetCPUCount();
            if (!settings.HasSeenStadiumTutorial)
            {
                settings.HasSeenStadiumTutorial = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumTutorial), settings.HasSeenStadiumTutorial.ToString());
            }
            else if (!settings.HasSeenStadiumRelicsTutorial && stadiumViewModel.ShowRelicsTutorial)
            {
                settings.HasSeenStadiumRelicsTutorial = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumRelicsTutorial), settings.HasSeenStadiumRelicsTutorial.ToString());
            }
            else if (!settings.HasSeenStadiumCheckpointTutorial && stadiumViewModel.ShowCheckpointTutorial)
            {
                settings.HasSeenStadiumCheckpointTutorial = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumCheckpointTutorial), settings.HasSeenStadiumCheckpointTutorial.ToString());
            }
            else if (!settings.HasSeenStadiumSuddenDeathTutorial && stadiumViewModel.ShowSuddenDeathTutorial)
            {
                settings.HasSeenStadiumSuddenDeathTutorial = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumSuddenDeathTutorial), settings.HasSeenStadiumSuddenDeathTutorial.ToString());
            }
            else if (!settings.HasSeenStadiumTipTutorial && stadiumViewModel.ShowStadiumTipTutorial)
            {
                settings.HasSeenStadiumTipTutorial = true;
                stadiumViewModel.ShowStadiumTipTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumTipTutorial), settings.HasSeenStadiumTipTutorial.ToString());
            }

            ResetLastRoundSettings(user, settings, stadiumViewModel);

            if (finalStatus == MatchStatus.Won)
            {
                int reward = matchState.Points * (matchState.IsCPU ? 1 : 2);
                int tokens = 2;
                if (settings.StadiumRewardLuckBonus > 0)
                {
                    reward = (int)Math.Round(reward * settings.StadiumRewardLuckBonus);
                }
                if (matchState.IsCPU)
                {
                    if (stadiumViewModel.IsCPUChallenge)
                    {
                        settings.StadiumChallengeWins += 1;
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumChallengeWins), settings.StadiumChallengeWins.ToString());
                    }
                    else if (stadiumViewModel.IsSpecialCPU)
                    {
                        if (matchState.Opponent == SpecialOpponents.Guard.GetDisplayName())
                        {
                            _rightSideUpWorldRepository.AddRightSideUpGuardHallOfFame(user);
                            _powerMovesRepository.AddPowerMove(user, PowerMoveType.BigShot);
                            Item item = _itemsRepository.GetAllItems(ItemCategory.Library).Last();
                            string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, item);
                            _messagesRepository.SendCourierMessage(user, $"The Right-Side Up Guard has a message for you: \"Congratulations! You defeated me at the Stadium and proved yourself to be a worthy opponent. I look forward to continue watching your successes but, you should know, there is still someone stronger than me. This promo code might point you in the right direction: {promoCode}. Good luck!\"");
                            _messagesRepository.SendPersonalMessage(user, Helper.GetDomain(), "Congrats on beating the Guard! Next up is finishing the Phantom Door, seeing the Beyond World, and exploring what's beyond the Library :)");
                        }
                        else if (matchState.Opponent == SpecialOpponents.Unknown.GetDisplayName())
                        {
                            Item item = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).Find(x => x.Name == "Unknown");
                            string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, item);
                            string message = $"I admit defeat. You were a worthy adversary. If can understand my way of writing, you'll be able to redeem this promo code: {promoCode}. What will it be? That's for you to discover!";
                            Random rnd = new Random();
                            _messagesRepository.SendCourierMessage(user, $"The Unknown has a cryptic message for you: \"{Helper.ShiftText(message, rnd.Next(10) + 5)}\"");
                        }
                        else if (matchState.Opponent == SpecialOpponents.ShowroomOwner.GetDisplayName())
                        {
                            _messagesRepository.SendCourierMessage(user, "The Showroom Owner has a message for you: \"Congratulations! You defeated me at the Stadium. You now have your own Showroom on your profile and can access the Showroom Store at the Buyers Club!\"");
                        }
                        else if (matchState.Opponent == SpecialOpponents.PhantomSpirit.GetDisplayName())
                        {
                            settings.PhantomDoorLevel = 2;
                            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

                            _messagesRepository.SendCourierMessage(user, "The Phantom Spirit has a message for you: \"You defeated me and freed my spirit. I've opened up the next passage of the Phantom Door for you. Please hurry!\"");
                        }
                        else if (matchState.Opponent == SpecialOpponents.Evena.GetDisplayName())
                        {
                            settings.HasDefeatedEvena = true;
                            _settingsRepository.SetSetting(user, nameof(settings.HasDefeatedEvena), settings.HasDefeatedEvena.ToString());
                        }
                        else if (matchState.Opponent == SpecialOpponents.Oddna.GetDisplayName())
                        {
                            settings.HasDefeatedOddna = true;
                            _settingsRepository.SetSetting(user, nameof(settings.HasDefeatedOddna), settings.HasDefeatedOddna.ToString());
                        }
                        else if (matchState.Opponent == SpecialOpponents.PhantomBoss.GetDisplayName())
                        {
                            settings.PhantomDoorLevel = 10;
                            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

                            _messagesRepository.SendCourierMessage(user, "The Phantom Boss has a message for you: \"No!! You defeated me and freed the spirits! I will come back one day and you will pay!\" It seems that the Phantom Boss abruptly disappeared after sending that message.");
                        }
                        else if (matchState.Opponent == $"Mirror {user.Username.First().ToString().ToUpper() + user.Username.Substring(1)}")
                        {
                            settings.BeyondWorldLevel = 2;
                            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());
                        }
                        else if (matchState.Opponent == SpecialOpponents.Guardian.GetDisplayName())
                        {
                            if (settings.SorceressGuardiansDefeated < 4)
                            {
                                settings.SorceressGuardiansDefeated++;
                                _settingsRepository.SetSetting(user, nameof(settings.SorceressGuardiansDefeated), settings.SorceressGuardiansDefeated.ToString());
                            }
                            if (settings.SorceressGuardiansDefeated == 4)
                            {
                                settings.BeyondWorldLevel = 4;
                                _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());
                            }
                        }
                        else if (matchState.Opponent == SpecialOpponents.BeyondCreature.GetDisplayName())
                        {
                            settings.BeyondWorldLevel++;
                            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());
                        }
                        else if (matchState.Opponent.Contains("Floor"))
                        {
                            settings.TowerFloorLevel++;
                            _settingsRepository.SetSetting(user, nameof(settings.TowerFloorLevel), settings.TowerFloorLevel.ToString());
                            if (settings.TowerFloorLevel > settings.TowerFloorRecordLevel)
                            {
                                settings.TowerFloorRecordLevel = settings.TowerFloorLevel;
                                _settingsRepository.SetSetting(user, nameof(settings.TowerFloorRecordLevel), settings.TowerFloorRecordLevel.ToString());
                            }
                            int floorLevelRecord = settings.TowerFloorLevel + 1;
                            if (floorLevelRecord >= 5)
                            {
                                GiveTowerAccomplishments(user, floorLevelRecord);
                            }
                            if (settings.TowerFloorLevel <= 2)
                            {
                                tokens = 0;
                            }
                            tokens = settings.TowerFloorLevel <= 9 ? 1 : 2;
                        }
                        else if (matchState.Opponent == SpecialOpponents.ChallengerDome.GetDisplayName())
                        {
                            tokens = 0;
                        }
                    }
                    else
                    {
                        if (stadiumViewModel.CPU.Level == settings.MatchLevel % cpusCount)
                        {
                            settings.MatchLevel += 1;
                            _settingsRepository.SetSetting(user, nameof(settings.MatchLevel), settings.MatchLevel.ToString());
                        }
                        tokens = stadiumViewModel.CPU.Level / 5 + 1;
                    }

                    if (settings.HasDoubleStadiumCPUReward)
                    {
                        settings.HasDoubleStadiumCPUReward = false;
                        _settingsRepository.SetSetting(user, nameof(settings.HasDoubleStadiumCPUReward), settings.HasDoubleStadiumCPUReward.ToString());
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(settings.PartnershipUsername) && reward > 0)
                    {
                        User partnershipUser = _userRepository.GetUser(settings.PartnershipUsername);
                        if (partnershipUser != null)
                        {
                            int earnings = (int)Math.Round(reward / 2 * 0.1);
                            _pointsRepository.AddPoints(partnershipUser, earnings);
                            _partnershipRepository.AddPartnershipEarning(user, partnershipUser, earnings);
                        }
                    }
                    tokens = stadiumViewModel.IsElite ? 5 : 2;
                }

                if (stadiumViewModel.Results.FindAll(x => x == MatchStatus.Lost).Count == 0)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumShutOut))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumShutOut);
                    }
                }

                if (reward > 0)
                {
                    _pointsRepository.AddPoints(user, reward);
                    if (stadiumViewModel.IsCPU)
                    {
                        PowerMove powerMove = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel).Find(x => x.MinCPULevel == settings.MatchLevel);
                        if (powerMove != null && powerMove.CreatedDate == DateTime.MinValue)
                        {
                            _powerMovesRepository.AddPowerMove(user, powerMove.Type);
                        }
                    }

                    settings.TrainingTokens += tokens;
                    _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());
                }
            }
            if (finalStatus == MatchStatus.Lost)
            {
                if (matchState.IsSpecialCPU)
                {
                    if (matchState.Opponent.Contains("Floor"))
                    {
                        if (settings.TowerFloorLevel > 0)
                        {
                            _towerRepository.AddTowerRanking(user, settings.TowerFloorLevel);
                        }
                        settings.TowerFloorLevel = 0;
                        _settingsRepository.SetSetting(user, nameof(settings.TowerFloorLevel), settings.TowerFloorLevel.ToString());
                    }
                }
            }
            if (finalStatus == MatchStatus.Draw)
            {
                AddSuddenDeathRound(user);
            }
            else if (finalStatus != MatchStatus.NotPlayed)
            {
                if (settings.CurrentIntroStage == IntroStage.Third)
                {
                    settings.CurrentIntroStage = IntroStage.Fourth;
                    _settingsRepository.SetSetting(user, nameof(settings.CurrentIntroStage), settings.CurrentIntroStage.ToString());
                }
                if (stadiumViewModel.IsCPUChallenge)
                {
                    settings.StadiumChallengeExpiryDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.StadiumChallengeExpiryDate), settings.StadiumChallengeExpiryDate.ToString());
                }
                if (stadiumViewModel.IsSpecialCPU)
                {
                    if (stadiumViewModel.OpponentName == SpecialOpponents.ChallengerDome.GetDisplayName())
                    {
                        int daysSinceLastStreak = (int)(DateTime.UtcNow - settings.ChallengerDomeStreakDate).TotalDays;
                        if (daysSinceLastStreak >= 1)
                        {
                            if (daysSinceLastStreak < 2)
                            {
                                settings.ChallengerDomeStreak++;
                                AddChallengerDomeAccomplishments(user, settings.ChallengerDomeStreak);
                            }
                            else
                            {
                                settings.ChallengerDomeStreak = 1;
                            }
                            _settingsRepository.SetSetting(user, nameof(settings.ChallengerDomeStreak), settings.ChallengerDomeStreak.ToString());
                            settings.ChallengerDomeStreakDate = DateTime.UtcNow;
                            _settingsRepository.SetSetting(user, nameof(settings.ChallengerDomeStreakDate), settings.ChallengerDomeStreakDate.ToString());
                        }
                    }
                }

                Guid tourneyMatchId = _matchStateRepository.GetMatchStates(user).Find(x => x.TourneyMatchId != Guid.Empty)?.TourneyMatchId ?? Guid.Empty;
                _matchHistoryRepository.AddMatchHistory(user, new MatchHistory
                {
                    Username = user.Username,
                    Opponent = stadiumViewModel.OpponentName,
                    CPUType = GetCPUType(stadiumViewModel),
                    Points = stadiumViewModel.Points,
                    Level = stadiumViewModel.CPU.Level,
                    Intensity = stadiumViewModel.CPU.Intensity,
                    TourneyMatchId = tourneyMatchId,
                    Result = finalStatus == MatchStatus.Won ? MatchHistoryResult.Won : MatchHistoryResult.Lost
                });
                AddMatchEndedAccomplishments(user, finalStatus == MatchStatus.Won, false, settings.MatchLevel, stadiumViewModel.MatchState, settings.StadiumChallengeWins, settings);
                if (tourneyMatchId != Guid.Empty)
                {
                    _tourneyRepository.AddTourneyProgress(user, tourneyMatchId, finalStatus, true);
                }
            }

            if (((matchState.IsCPU && !matchState.IsSpecialCPU && !matchState.IsCPUChallenge) || !matchState.IsCPU) && finalStatus == MatchStatus.Won)
            {
                int winStreak = GetWinStreak(user, _matchHistoryRepository.GetMatchHistory(user), matchState.IsCPU);
                if (winStreak > 1)
                {
                    int additionalReward = Math.Min(50000, (int)Math.Round(matchState.Points * ((double)(winStreak - 1) / 10)));
                    _pointsRepository.AddPoints(user, additionalReward);
                }
                if (matchState.IsCPU && settings.MatchLevel % 5 == 0)
                {
                    _pointsRepository.AddPoints(user, Math.Min(50000, settings.MatchLevel * 1000));
                }
            }

            _itemsRepository.RemoveUsedItems(user);

            string soundUrl = string.Empty;
            if (finalStatus == MatchStatus.Won)
            {
                if ((stadiumViewModel.IsCPU && !stadiumViewModel.IsCPUChallenge && stadiumViewModel.CPU.IsCheckpoint) || (stadiumViewModel.IsSpecialCPU && !(stadiumViewModel.OpponentName == SpecialOpponents.ChallengerDome.GetDisplayName() || stadiumViewModel.OpponentName.Contains("Floor"))))
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.VictoryAlt, settings);
                }
                else
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Victory, settings);
                }
            }
            else if (finalStatus == MatchStatus.Lost)
            {
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Defeat, settings);
            }

            return Json(new Status { Success = true, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl, CanPlaySound = settings.EnableSoundEffects });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EndMatch(bool rematch = false, bool skipCPU = false, int tourneyId = 0)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }


            Response.Cookies.Delete("ShowIntro");

            List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
            if (matchStates.FirstOrDefault() != null && !matchStates.First().IsCPU)
            {
                if (_matchStateRepository.IsInMatch(user))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You're already in a user match." });
                }
            }

            bool isUser;
            if (matchStates == null || matchStates?.FirstOrDefault() == null)
            {
                if (rematch)
                {
                    isUser = false;
                }
                else
                {
                    return Json(new Status { Success = true });
                }
            }
            else
            {
                MatchState matchState = matchStates.First();
                if (!skipCPU)
                {
                    _matchStateRepository.DeleteMatchState(user, matchState.Opponent, matchState.IsCPU, false);
                }
                isUser = !matchState.IsCPU;
            }

            if (rematch)
            {
                int rematchPoints = 0;
                if (tourneyId > 0)
                {
                    // it's a tourney
                }
                else if (isUser)
                {
                    bool isElite = _matchHistoryRepository.IsElite(user);
                    rematchPoints = Math.Min(user.Points / (isElite ? 3 : 10), isElite ? 25000 : 10000);
                }
                else
                {
                    if (skipCPU)
                    {
                        Settings settings = _settingsRepository.GetSettings(user);
                        CPU cpu = _cpuRepository.GetCPU(settings.MatchLevel);
                        if (cpu.IsCheckpoint)
                        {
                            return Json(new Status { Success = false, ErrorMessage = $"You can't skip a Checkpoint CPU." });
                        }
                        if ((cpu.Level + 2) % 5 == 0)
                        {
                            return Json(new Status { Success = false, ErrorMessage = $"If you skip {cpu.Name}, your next challenger will be the Level {cpu.Level + 2} Checkpoint CPU. You can't skip to a Checkpoint CPU." });
                        }
                        if (cpu.Level == 1 || cpu.Level == 7)
                        {
                            return Json(new Status { Success = false, ErrorMessage = $"You cannot skip a CPU that will reward you a new Power Move." });
                        }
                        int skipCost = (cpu.Level + 1) * 500 * (cpu.Intensity + 1);
                        if (user.Points < skipCost)
                        {
                            return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to skip this CPU Challenger." });
                        }
                        List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(user);
                        int retryCount = GetCPUMatchCount(matchHistories, cpu);
                        if (retryCount < 2)
                        {
                            return Json(new Status { Success = false, ErrorMessage = $"You can only skip {cpu.Name} when you've rematched them at least once." });
                        }
                        if (_pointsRepository.SubtractPoints(user, skipCost))
                        {
                            settings.MatchLevel++;
                            _settingsRepository.SetSetting(user, nameof(settings.MatchLevel), settings.MatchLevel.ToString());

                            MatchState matchState = matchStates.First();
                            _matchStateRepository.DeleteMatchState(user, matchState.Opponent, matchState.IsCPU, false);

                            Messages.AddSnack($"You skipped {cpu.Name} for {Helper.GetFormattedPoints(skipCost)} and moved on to the Level {cpu.Level + 2} CPU!");
                        }
                    }
                }
                return StartMatch(rematchPoints, !isUser, false, string.Empty, skipCPU, tourneyId, isRematch: true);
            }

            return Json(new Status { Success = true, Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpPost]
        public IActionResult ConcedeMatch()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MatchState matchState = _matchStateRepository.GetMatchStates(user).FirstOrDefault();
            if (matchState == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currenly in a match." });
            }

            if (!matchState.IsCPU)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot concede in a user match." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot concede this match." });
            }

            if (matchState.IsSpecialCPU)
            {
                if (matchState.Opponent.Contains("Floor"))
                {
                    if (settings.TowerFloorLevel > 0)
                    {
                        _towerRepository.AddTowerRanking(user, settings.TowerFloorLevel);
                    }
                    settings.TowerFloorLevel = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.TowerFloorLevel), settings.TowerFloorLevel.ToString());
                }
            }

            StadiumViewModel stadiumViewModel = GenerateModel(user);
            _matchHistoryRepository.AddMatchHistory(user, new MatchHistory
            {
                Username = user.Username,
                Opponent = stadiumViewModel.OpponentName,
                CPUType = GetCPUType(stadiumViewModel),
                Points = stadiumViewModel.Points,
                Level = stadiumViewModel.CPU.Level,
                Intensity = stadiumViewModel.CPU.Intensity,
                Result = MatchHistoryResult.Lost
            });

            Messages.AddSnack($"You successfully conceded the match against {matchState.Opponent}!");

            return EndMatch();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EndWaitlist()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (_matchStateRepository.IsInMatch(user, false))
            {
                Messages.AddSnack("You're already in a match.", false);
                return Json(new Status { Success = true, ShouldRefresh = true });
            }

            MatchWaitlist matchWaitList = _matchStateRepository.GetMatchWaitlist(user);
            if (string.IsNullOrEmpty(matchWaitList?.Username2))
            {
                _matchStateRepository.DeleteMatchWaitlist(user);
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "You are already in match." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeGuard()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The portal to the Right-Side Up World has closed.", ShouldRefresh = true });
            }

            if (user.Points < 500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CPUCompleted))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can only challenge The Right-Side Up Guard when you've defeated all the CPU Challengers." });
            }
            if (accomplishments.Exists(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already defeated the Right-Side Up Guard." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            if (_pointsRepository.SubtractPoints(user, 500))
            {
                CPU cpu = _cpuRepository.GetRightSideUpGuard();

                InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

                return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The Stadium match could not be started." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeShowroomOwner()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (settings.MatchLevel < 9 && (accomplishments.Count < 15 || user.GetAgeInMinutes() < 30 || settings.MatchLevel < 2))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You haven't been invited to challenge the Showroom Owner yet." });
            }
            if (accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomAccess))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already defeated the Showroom Owner at the Stadium." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            CPU cpu = _cpuRepository.GetShowroomOwner();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengePhantomSpirit()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 1 || !settings.HasLearntTranslation)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge the Phantom Spirit at this time." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            if (deck.Any(x => x.Amount > 50))
            {
                return Json(new Status { Success = false, ErrorMessage = $"None of your {Resources.DeckCards} can be greater than 50 to challenge the Phantom Spirit." });
            }

            CPU cpu = _cpuRepository.GetPhantomSpirit();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeEvena()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 5)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge Evena at this time." });
            }

            if (settings.HasDefeatedEvena)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already defeated Evena at the Stadium." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            if (deck.Any(x => x.Amount % 2 != 0))
            {
                return Json(new Status { Success = false, ErrorMessage = $"None of your {Resources.DeckCards} can be odd to challenge Evena." });
            }

            CPU cpu = _cpuRepository.GetEvena();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeOddna()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 5)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge Oddna at this time." });
            }

            if (settings.HasDefeatedOddna)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already defeated Oddna at the Stadium." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            if (deck.Any(x => x.Amount % 2 == 0))
            {
                return Json(new Status { Success = false, ErrorMessage = $"None of your {Resources.DeckCards} can be even to challenge Oddna." });
            }

            CPU cpu = _cpuRepository.GetOddna();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengePhantomBoss()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 9)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge the Phantom Boss at this time." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Count(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated || x.Id == AccomplishmentId.TowerFloorFive) != 2)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You need to earn both accomplishments to challenge the Phantom Boss." });
            }

            if ((DateTime.UtcNow - settings.LastPhantomBossStadiumChallengeDate).TotalMinutes < 10)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've challenged the Phantom Boss too recently. Try again in {settings.LastPhantomBossStadiumChallengeDate.AddMinutes(10).GetTimeUntil()}." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            settings.LastPhantomBossStadiumChallengeDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastPhantomBossStadiumChallengeDate), settings.LastPhantomBossStadiumChallengeDate.ToString());

            CPU cpu = _cpuRepository.GetPhantomBoss();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeUnknown()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You have already defeated The Unknown." });
            }

            if (!_libraryRepository.CanPullLever(accomplishments))
            {
                return Json(new Status { Success = false, ErrorMessage = "You aren't able to challenge The Unknown yet." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int minutesSinceLastChallenge = (int)(DateTime.UtcNow - settings.LastUnknownStadiumChallengeDate).TotalMinutes;
            if (minutesSinceLastChallenge < 30)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've challenged The Unknown at the Stadium too recently. Try again in {30 - minutesSinceLastChallenge} minute{(30 - minutesSinceLastChallenge == 1 ? "" : "s")}." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            settings.LastUnknownStadiumChallengeDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastUnknownStadiumChallengeDate), settings.LastUnknownStadiumChallengeDate.ToString());

            CPU cpu = _cpuRepository.GetUnknown();

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeTower()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't enter The Tower yet." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            CPU cpu = _cpuRepository.GetTowerCPU(settings.TowerFloorLevel);

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeDome()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFive))
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't enter The Challenger Dome yet." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LastChallengerDomeMatchDate).TotalMinutes < 15)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've been to the Challenger Dome too recently. Try again in {settings.LastChallengerDomeMatchDate.AddMinutes(15).GetTimeUntil()}." });
            }

            settings.LastChallengerDomeMatchDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastChallengerDomeMatchDate), settings.LastChallengerDomeMatchDate.ToString());

            CPU cpu = _cpuRepository.GetChallengerDomeCPU(settings);

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeBeyondWorld()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot challenge anyone in the Beyond World at this time." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            if (!CheckIfCardsAllowed(user, deck, out string message, out string buttonText, out string buttonUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = message, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            CPU cpu = _cpuRepository.GetBeyondWorldCPU(user, settings);
            if (cpu == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot challenge this opponent in the Beyond World at this time." });
            }

            InitializeMatch(user, deck, 0, true, false, cpu, string.Empty, Guid.Empty);

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StartTourney(int tourneyId)
        {
            return StartMatch(0, false, false, string.Empty, false, tourneyId, true);
        }

        [HttpGet]
        public IActionResult PollForOpponentWaitlist(int tries)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MatchWaitlist matchWaitList = _matchStateRepository.GetMatchWaitlist(user);
            if (matchWaitList == null)
            {
                return Json(new Status { Success = false, StopExecuting = true });
            }
            if (string.IsNullOrEmpty(matchWaitList.Username2))
            {
                if (tries >= 60)
                {
                    _matchStateRepository.DeleteMatchWaitlist(user);
                }

                return Json(new Status { Success = false });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            InitializeMatch(user, deck, matchWaitList.Points, false, false, null, matchWaitList.Username2, matchWaitList.TourneyMatchId);
            _matchStateRepository.DeleteMatchWaitlist(user);
            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult PollForOpponentsMove()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MatchState userMatchState = GetNextMatchState(user);
            if (userMatchState == null || userMatchState.IsInSelection || userMatchState.IsCPU)
            {
                return Json(new Status { Success = false, StopExecuting = true });
            }

            User opponent = _userRepository.GetUser(userMatchState.Opponent);
            if (opponent == null)
            {
                return Json(new Status { Success = false, StopExecuting = true });
            }

            List<MatchState> opponentMatchStates = _matchStateRepository.GetMatchStates(opponent, true, false).FindAll(x => x.Opponent == user.Username);
            if (opponentMatchStates.Count <= userMatchState.Position)
            {
                return Json(new Status { Success = false });
            }

            MatchState opponentMatchState = opponentMatchStates[userMatchState.Position];
            if (opponentMatchState == null || opponentMatchState.IsInSelection)
            {
                return Json(new Status { Success = false });
            }

            if (opponentMatchState.CardId == Guid.Empty)
            {
                return Json(new Status { Success = false });
            }

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetStadiumPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            StadiumViewModel stadiumViewModel = GenerateModel(user);
            if (stadiumViewModel.IsInWaitlist)
            {
                return PartialView("_StadiumMatchWaitPartial", stadiumViewModel);
            }
            if (stadiumViewModel.FinalStatus == MatchStatus.Won || stadiumViewModel.FinalStatus == MatchStatus.Lost)
            {
                return PartialView("_StadiumMatchFinalPartial", stadiumViewModel);
            }
            if (stadiumViewModel.MatchState == null)
            {
                return PartialView("_StadiumMainPartial", stadiumViewModel);
            }
            if (stadiumViewModel.MatchState.IsInSelection)
            {
                return PartialView("_StadiumMatchSelectPartial", stadiumViewModel);
            }
            return PartialView("_StadiumMatchDuelPartial", stadiumViewModel);
        }

        [HttpGet]
        public IActionResult GetTip()
        {
            return Json(new Status { Success = true, SuccessMessage = GenerateTip() });
        }

        private StadiumViewModel GenerateModel(User user, bool fromChallenge = false, StadiumTab stadiumTab = StadiumTab.None)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
            List<Card> deck = _itemsRepository.GetCards(user, true).OrderBy(x => x.Amount).ToList();
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            CPU cpu = _cpuRepository.GetCPU(settings.MatchLevel);
            int cpuLevel = Math.Min(_cpuRepository.GetCPUCount(), settings.MatchLevel + 1);
            int totalCPUs = _cpuRepository.GetCPUCount();

            bool isCPUChallenge = matchStates.FirstOrDefault()?.IsCPUChallenge ?? false;

            StadiumViewModel stadiumViewModel = new StadiumViewModel
            {
                Title = "Stadium",
                User = user,
                OpponentName = matchStates.FirstOrDefault()?.Opponent ?? string.Empty,
                Points = matchStates.FirstOrDefault()?.Points ?? 0,
                IsCPU = matchStates.FirstOrDefault()?.IsCPU ?? false,
                IsCPUChallenge = isCPUChallenge,
                IsSpecialCPU = matchStates.FirstOrDefault()?.IsSpecialCPU ?? false,
                Deck = deck,
                Enhancers = GetEnhancersForMatch(user, settings, isCPUChallenge, settings.StadiumChallenge, true).OrderByDescending(x => x.OriginalPoints).ToList(),
                MatchState = GetNextMatchState(user, matchStates),
                Results = matchStates.Select(x => x.Status).ToList(),
                CPU = cpu,
                ThemeBoost = GetThemeBoost(deck, settings),
                TotalCPUs = totalCPUs,
                UserSettings = settings,
                FromChallenge = fromChallenge,
                StadiumTab = stadiumTab == StadiumTab.None ? settings.ShowUserMatchesFirst ? StadiumTab.User : StadiumTab.CPU : stadiumTab,
                ShowTutorial = !settings.HasSeenStadiumTutorial,
                ShowStadiumMatchIntro = CanShowStadiumIntro(matchStates),
                Accomplishments = accomplishments
            };

            stadiumViewModel.FinalStatus = _matchStateRepository.GetFinalStatus(stadiumViewModel.Results);
            if (stadiumViewModel.FinalStatus == MatchStatus.Draw)
            {
                stadiumViewModel.FinalStatus = MatchStatus.NotPlayed;
            }

            User opponent = null;
            if (matchStates.Count > 0)
            {
                stadiumViewModel.PowerMoves = _powerMovesRepository.GetPowerMoves(user, matchStates, settings.MatchLevel, false, stadiumViewModel.FinalStatus != MatchStatus.NotPlayed);
                stadiumViewModel.Relic = _relicsRepository.GetRelic(settings.MatchLevel);
                stadiumViewModel.NextRelic = _relicsRepository.GetAllRelics(settings.MatchLevel).Find(x => !x.Earned);
                if (!settings.HasSeenStadiumPowerMovesTutorial && stadiumViewModel.FinalStatus == MatchStatus.Won && stadiumViewModel.IsCPU && !stadiumViewModel.IsCPUChallenge && !stadiumViewModel.IsSpecialCPU && stadiumViewModel.PowerMoves.Any(x => x.MinCPULevel > 0 && !x.IsDisabled))
                {
                    settings.HasSeenStadiumPowerMovesTutorial = true;
                    stadiumViewModel.ShowPowerMovesTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumPowerMovesTutorial), settings.HasSeenStadiumPowerMovesTutorial.ToString());
                }
                else if (!settings.HasSeenEnhancerLimitedUsesTutorial && stadiumViewModel.MatchState != null && stadiumViewModel.MatchState.IsInSelection && stadiumViewModel.Enhancers.Any(x => x.Uses == 1))
                {
                    settings.HasSeenEnhancerLimitedUsesTutorial = true;
                    stadiumViewModel.ShowEnhancerLimitedUsesTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenEnhancerLimitedUsesTutorial), settings.HasSeenEnhancerLimitedUsesTutorial.ToString());
                }
                else if (!settings.HasSeenStadiumCheckpointDefeatedTutorial && stadiumViewModel.FinalStatus == MatchStatus.Won && stadiumViewModel.IsCPU && !stadiumViewModel.IsCPUChallenge && !stadiumViewModel.IsSpecialCPU && stadiumViewModel.UserSettings.MatchLevel == 5)
                {
                    settings.HasSeenStadiumCheckpointDefeatedTutorial = true;
                    stadiumViewModel.ShowFirstCheckpointDefeatedTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenStadiumCheckpointDefeatedTutorial), settings.HasSeenStadiumCheckpointDefeatedTutorial.ToString());
                }
                if (matchStates.First().IsCPU)
                {
                    stadiumViewModel.OpponentDisplayPicUrl = matchStates.First().ImageUrl;
                }
                else
                {
                    opponent = _userRepository.GetUser(matchStates.First().Opponent);
                    if (opponent != null)
                    {
                        stadiumViewModel.OpponentColor = opponent.Color;
                        stadiumViewModel.OpponentDisplayPicUrl = opponent.DisplayPicUrl;
                    }
                }
            }

            MatchWaitlist matchWaitlist = null;
            bool isInWaitlist = matchStates.Count == 0 && IsWaitingForMatch(user, out matchWaitlist);
            if (isInWaitlist)
            {
                stadiumViewModel.IsInWaitlist = true;
                stadiumViewModel.MatchWaitlist = matchWaitlist;
                stadiumViewModel.Tip = GenerateTip();
                stadiumViewModel.IsElite = _matchHistoryRepository.IsElite(user, stadiumViewModel.MatchRanking);
            }
            else if (stadiumViewModel.MatchState == null || stadiumViewModel.FinalStatus != MatchStatus.NotPlayed)
            {
                if (stadiumViewModel.MatchState == null || !stadiumViewModel.IsCPU)
                {
                    stadiumViewModel.MatchTags = _matchHistoryRepository.GetMatchTags(user);
                    stadiumViewModel.MatchRanking = _matchHistoryRepository.GetMatchRanking(user);
                    stadiumViewModel.IsElite = _matchHistoryRepository.IsElite(user, stadiumViewModel.MatchRanking);
                    stadiumViewModel.UserLobby = _matchStateRepository.GetMatchWaitlists(user, stadiumViewModel.IsElite);
                    if (stadiumViewModel.IsElite)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.EliteZone))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.EliteZone);
                        }
                        if (!settings.HasSeenEliteZoneTutorial)
                        {
                            settings.HasSeenEliteZoneTutorial = true;
                            stadiumViewModel.ShowEliteZoneTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenEliteZoneTutorial), settings.HasSeenEliteZoneTutorial.ToString());
                        }
                    }
                    if (settings.HasSeenTutorial && !settings.HasSeenSecondMatchTutorial && settings.MatchLevel < 2)
                    {
                        settings.HasSeenSecondMatchTutorial = true;
                        stadiumViewModel.ShowSecondMatchTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenSecondMatchTutorial), settings.HasSeenSecondMatchTutorial.ToString());
                    }
                    else if (settings.HasSeenTutorial && !settings.HasSeenRelicRequirementTutorial && (stadiumViewModel.Deck.Any(x => !_relicsRepository.HasEarned(x.Tier, settings.MatchLevel)) || stadiumViewModel.Enhancers.Any(x => !_relicsRepository.HasEarned(x.Tier, settings.MatchLevel))))
                    {
                        settings.HasSeenRelicRequirementTutorial = true;
                        stadiumViewModel.ShowRelicRequirementTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenRelicRequirementTutorial), settings.HasSeenRelicRequirementTutorial.ToString());
                    }
                }

                if (matchStates.FirstOrDefault() != null && matchStates.Any(x => x.TourneyMatchId != Guid.Empty))
                {
                    List<Tourney> tourneys = _tourneyRepository.GetUserTourneys(user);
                    Guid tourneyMatchId = matchStates.Find(x => x.TourneyMatchId != Guid.Empty).TourneyMatchId;
                    Tourney tourney = tourneys.Find(x => x.TourneyMatches.Any(x => x.Id == tourneyMatchId));
                    stadiumViewModel.Tourney = tourney;
                    if (tourney != null)
                    {
                        stadiumViewModel.TourneyMatch = tourney.TourneyMatches.Find(x => x.Id == tourneyMatchId);
                    }
                }

                List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(user);
                stadiumViewModel.UserWinPercent = GetUserWinPercent(matchHistories);
                stadiumViewModel.CPUMatchCount = GetCPUMatchCount(matchHistories, cpu);
                stadiumViewModel.CPUWinStreak = GetWinStreak(user, matchHistories, true);
                stadiumViewModel.UserWinStreak = GetWinStreak(user, matchHistories, false);
                stadiumViewModel.MatchHistories = matchHistories;
                stadiumViewModel.CanChallengeCPU = CanChallengeCPU(user, accomplishments, settings, cpu, totalCPUs, out string reason);
                stadiumViewModel.CheckpointCPURequirement = reason;
                stadiumViewModel.Tip = GenerateStrategyTip();

                if (stadiumViewModel.ShowTutorial && stadiumViewModel.Enhancers.Count == 0 || (stadiumViewModel.IsCPU && stadiumViewModel.FinalStatus == MatchStatus.Lost && stadiumViewModel.CPU.Intensity == 0 && stadiumViewModel.CPU.Level < 5 && stadiumViewModel.CPUMatchCount == 2 && !stadiumViewModel.Enhancers.Any(x => x.Name.Contains("+10"))))
                {
                    stadiumViewModel.LossEnhancer = _itemsRepository.GetAllItems().Find(x => x.Category == ItemCategory.Enhancer && x.TotalUses > 0 && x.Name.Equals("+10", StringComparison.InvariantCultureIgnoreCase));
                }
            }
            else
            {
                stadiumViewModel.WinsNeeded = _matchStateRepository.GetWinsNeeded(stadiumViewModel.Results);
                stadiumViewModel.ShowSuddenDeathTutorial = !settings.HasSeenStadiumSuddenDeathTutorial && stadiumViewModel.MatchState.Position >= Helper.GetMaxDeckSize();
                if (stadiumViewModel.IsCPU)
                {
                    stadiumViewModel.ShowRelicsTutorial = !settings.HasSeenStadiumRelicsTutorial && cpu.Level == 1 && cpu.Intensity == 0 && !stadiumViewModel.IsCPUChallenge && !stadiumViewModel.IsSpecialCPU;
                    stadiumViewModel.ShowCheckpointTutorial = !settings.HasSeenStadiumCheckpointTutorial && cpu.Level == 4 && cpu.Intensity == 0 && !stadiumViewModel.IsCPUChallenge && !stadiumViewModel.IsSpecialCPU;
                    stadiumViewModel.ShowStadiumTipTutorial = !settings.HasSeenStadiumTipTutorial && settings.MatchLevel >= 2 && stadiumViewModel.MatchState != null && stadiumViewModel.MatchState.Position == 1 && stadiumViewModel.Enhancers.Any(x => x.IsFiller);
                    if (stadiumViewModel.ShowTutorial && stadiumViewModel.Enhancers.Count == 0)
                    {
                        stadiumViewModel.LossEnhancer = _itemsRepository.GetAllItems().Find(x => x.Name.Equals("+10", StringComparison.InvariantCultureIgnoreCase) && x.Category == ItemCategory.Enhancer && x.TotalUses > 0);
                    }
                }
                else
                {
                    stadiumViewModel.SecondsLeft = matchStates.OrderByDescending(x => x.UpdatedDate).First().GetSecondsLeft();
                    if (stadiumViewModel.SecondsLeft <= 0)
                    {
                        stadiumViewModel.MatchState = null;
                        ExpireMatch(user, stadiumViewModel.OpponentName, stadiumViewModel.Points);
                        return stadiumViewModel;
                    }
                    if (stadiumViewModel.ShowStadiumMatchIntro)
                    {
                        stadiumViewModel.MatchRanking = _matchHistoryRepository.GetMatchRanking(user);
                        stadiumViewModel.IsElite = _matchHistoryRepository.IsElite(user, stadiumViewModel.MatchRanking);
                    }
                }

                stadiumViewModel.AvailableDeck = GetAvailableDeck(deck, matchStates);
                stadiumViewModel.NextCard = stadiumViewModel.Deck.Find(x => x.SelectionId == stadiumViewModel.MatchState.CardId);
                stadiumViewModel.NextEnhancer = stadiumViewModel.Enhancers.Find(x => x.Id == stadiumViewModel.MatchState.EnhancerId);
                stadiumViewModel.NextPowerMove = _powerMovesRepository.GetPowerMoves(user, matchStates, settings.MatchLevel, false, true).Find(x => x.Type == stadiumViewModel.MatchState.PowerMove);

                if (stadiumViewModel.MatchState.IsInSelection == false)
                {
                    GetOpponentCardsAndPowerMove(stadiumViewModel, matchStates, opponent);
                }
                stadiumViewModel.RoundPowers = GetRoundPower(stadiumViewModel, false);
                stadiumViewModel.RoundResult = GetStatus(stadiumViewModel);
            }

            return stadiumViewModel;
        }

        private void GetOpponentCardsAndPowerMove(StadiumViewModel stadiumViewModel, List<MatchState> matchStates, User opponent)
        {
            CPU cpu = null;
            MatchState currentMatchState = stadiumViewModel.MatchState;
            if (stadiumViewModel.IsCPU)
            {
                cpu = stadiumViewModel.CPU;

                Card opponentCard = stadiumViewModel.IsCPUChallenge || stadiumViewModel.IsSpecialCPU ? _itemsRepository.GetAllCards().Find(x => x.Id == currentMatchState.CPUCardId) : cpu.Deck.Find(x => x.Id == currentMatchState.CPUCardId);
                opponentCard.Theme = currentMatchState.CPUCardTheme;
                stadiumViewModel.OpponentCard = opponentCard;
                if (currentMatchState.CPUEnhancerId != Guid.Empty)
                {
                    stadiumViewModel.OpponentEnhancer = stadiumViewModel.IsCPUChallenge || stadiumViewModel.IsSpecialCPU ? _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == currentMatchState.CPUEnhancerId) : cpu.Enhancers.Find(x => x?.Id == currentMatchState.CPUEnhancerId);
                }

                List<Card> CPUDeck = new List<Card>();
                if (stadiumViewModel.IsCPUChallenge || stadiumViewModel.IsSpecialCPU)
                {
                    List<Card> allCards = _itemsRepository.GetAllCards();
                    foreach (MatchState matchState in matchStates)
                    {
                        Card deckCard = allCards.Find(x => x.Id == matchState.CPUCardId);
                        deckCard.Theme = matchState.CPUCardTheme;
                        CPUDeck.Add(deckCard);
                    }
                }
                else
                {
                    CPUDeck = cpu.Deck.OrderBy(x => matchStates.FindIndex(y => y.CPUCardId == x.Id)).ToList();
                }

                if (stadiumViewModel.IsSpecialCPU)
                {
                    cpu = null;
                }

                stadiumViewModel.OpponentDeck = CPUDeck;
                stadiumViewModel.OpponentCard.CreatedDate = stadiumViewModel.NextCard.CreatedDate.AddHours(-1);

                if (currentMatchState.CPUPowerMove != PowerMoveType.None)
                {
                    stadiumViewModel.OpponentPowerMove = _powerMovesRepository.GetPowerMoves(stadiumViewModel.User, matchStates, stadiumViewModel.UserSettings.MatchLevel, true, true).Find(x => x.Type == currentMatchState.CPUPowerMove);
                }
            }
            else
            {
                opponent ??= _userRepository.GetUser(stadiumViewModel.OpponentName);
                if (opponent == null)
                {
                    return;
                }

                List<MatchState> opponentMatchStates = _matchStateRepository.GetMatchStates(opponent, true, false).FindAll(x => x.Opponent == stadiumViewModel.User.Username);
                if (opponentMatchStates.Count <= currentMatchState.Position)
                {
                    return;
                }

                MatchState opponentMatchState = opponentMatchStates[currentMatchState.Position];
                if (opponentMatchState == null || opponentMatchState.IsInSelection)
                {
                    return;
                }

                stadiumViewModel.OpponentMatchState = opponentMatchState;

                stadiumViewModel.OpponentDeck = _itemsRepository.GetCards(opponent, true, false);
                Card opponentCard = stadiumViewModel.OpponentDeck.Find(x => x.SelectionId == opponentMatchState.CardId);
                stadiumViewModel.OpponentCard = opponentCard;

                if (opponentMatchState.EnhancerId != Guid.Empty)
                {
                    Enhancer opponentEnhancer = _itemsRepository.GetEnhancers(opponent, false).Find(x => x.Id == opponentMatchState.EnhancerId);
                    stadiumViewModel.OpponentEnhancer = opponentEnhancer;
                }

                stadiumViewModel.OpponentSettings = _settingsRepository.GetSettings(opponent, false);

                if (opponentMatchState.PowerMove != PowerMoveType.None)
                {
                    stadiumViewModel.OpponentPowerMove = _powerMovesRepository.GetPowerMoves(opponent, opponentMatchStates, stadiumViewModel.OpponentSettings.MatchLevel, false, true, false, currentMatchState.Position).Find(x => x.Type == opponentMatchState.PowerMove);
                }
            }

            CPUChallenge cpuChallenge = stadiumViewModel.IsCPUChallenge ? stadiumViewModel.UserSettings.StadiumChallenge : CPUChallenge.None;
            stadiumViewModel.OpponentRoundPowers = GetRoundPower(stadiumViewModel, true);
            stadiumViewModel.OpponentThemeBoost = GetThemeBoost(stadiumViewModel.OpponentDeck, stadiumViewModel.OpponentSettings);
        }

        private MatchStatus GetStatus(StadiumViewModel stadiumViewModel)
        {
            if (stadiumViewModel.NextCard == null || stadiumViewModel.OpponentCard == null)
            {
                return MatchStatus.NotPlayed;
            }
            if (stadiumViewModel.RoundPowers.Last().Power > stadiumViewModel.OpponentRoundPowers.Last().Power)
            {
                return MatchStatus.Won;
            }
            if (stadiumViewModel.RoundPowers.Last().Power < stadiumViewModel.OpponentRoundPowers.Last().Power)
            {
                return MatchStatus.Lost;
            }

            return MatchStatus.Draw;
        }

        private List<RoundPower> GetRoundPower(StadiumViewModel stadiumViewModel, bool isOpponent)
        {
            List<RoundPower> roundPowers = new List<RoundPower>();
            List<RoundPowerStage> roundPowerStages = Enum.GetValues(typeof(RoundPowerStage)).Cast<RoundPowerStage>().ToList();
            foreach (RoundPowerStage roundPowerStage in roundPowerStages)
            {
                int roundPower = (int)Math.Round(GetRoundPower(stadiumViewModel, isOpponent, roundPowerStage, out List<bool> isFaded, out List<bool> hasBackfired));
                roundPowers.Add(new RoundPower
                {
                    Stage = roundPowerStage,
                    Power = roundPower,
                    IsFaded = isFaded,
                    HasBackfired = hasBackfired
                });
            }

            return roundPowers;
        }

        private double GetRoundPower(StadiumViewModel stadiumViewModel, bool isOpponent, RoundPowerStage roundPowerStage, out List<bool> isFaded, out List<bool> hasBackfired)
        {
            User user = stadiumViewModel.User;
            List<Card> deck = isOpponent ? stadiumViewModel.OpponentDeck : stadiumViewModel.Deck;
            Settings settings = isOpponent ? stadiumViewModel.OpponentSettings : stadiumViewModel.UserSettings;
            Settings userSettings = stadiumViewModel.UserSettings;
            MatchState currentMatchState = isOpponent ? stadiumViewModel.OpponentMatchState : stadiumViewModel.MatchState;
            Card card = isOpponent ? stadiumViewModel.OpponentCard : stadiumViewModel.NextCard;
            Card opponentCard = isOpponent ? stadiumViewModel.NextCard : stadiumViewModel.OpponentCard;
            Enhancer enhancer = isOpponent ? stadiumViewModel.OpponentEnhancer : stadiumViewModel.NextEnhancer;
            Enhancer opponentEnhancer = isOpponent ? stadiumViewModel.NextEnhancer : stadiumViewModel.OpponentEnhancer;
            PowerMove powerMove = isOpponent ? stadiumViewModel.OpponentPowerMove : stadiumViewModel.NextPowerMove;
            PowerMove opponentPowerMove = isOpponent ? stadiumViewModel.NextPowerMove : stadiumViewModel.OpponentPowerMove;
            bool isCPU = stadiumViewModel.IsCPU;
            bool isCPUChallenge = stadiumViewModel.IsCPUChallenge;
            bool isSpecialCPU = stadiumViewModel.IsSpecialCPU;
            CPU cpu = stadiumViewModel.CPU;
            CPUChallenge stadiumChallenge = stadiumViewModel.IsCPUChallenge ? stadiumViewModel.UserSettings.StadiumChallenge : CPUChallenge.None;
            int currentRound = stadiumViewModel.MatchState.Position;
            isFaded = new List<bool> { false, false, false };
            hasBackfired = new List<bool> { false, false };

            if (deck == null || card == null || opponentCard == null) //if there is no deck or chosen card
            {
                return 0;
            }

            double cardAmount = card.Amount;
            if (stadiumChallenge == CPUChallenge.DeckCardsWorthDouble)
            {
                cardAmount *= 2;
            }

            if (roundPowerStage == RoundPowerStage.Deck) //if it's just the Deck round power, return
            {
                if ((!isOpponent || !isCPU) && !_relicsRepository.HasEarned(card.Tier, settings.MatchLevel))
                {
                    hasBackfired[0] = true;
                }
                return cardAmount;
            }
            if (roundPowerStage < RoundPowerStage.Final) //if we don't want Power Moves round power yet, don't set it
            {
                powerMove = null;
                opponentPowerMove = null;
            }
            else if (powerMove?.Type == PowerMoveType.None)
            {
                isFaded[2] = true;
            }

            // Counter Stop
            if (powerMove?.Type == PowerMoveType.CounterStop && opponentPowerMove?.Type == PowerMoveType.PowerStop)
            {
                opponentEnhancer = null;
            }
            if (opponentPowerMove?.Type == PowerMoveType.CounterStop && powerMove?.Type == PowerMoveType.PowerStop)
            {
                enhancer = null;
                isFaded[1] = true;
            }

            //Power Stop
            if (powerMove != null && powerMove.Type != PowerMoveType.CounterStop && opponentPowerMove?.Type == PowerMoveType.PowerStop)
            {
                isFaded[2] = true;
                powerMove.Type = PowerMoveType.None;
            }
            if (opponentPowerMove != null && opponentPowerMove.Type != PowerMoveType.CounterStop && powerMove?.Type == PowerMoveType.PowerStop)
            {
                opponentPowerMove.Type = PowerMoveType.None;
            }

            //Mirror Mirror
            if (powerMove?.Type == PowerMoveType.MirrorMirror && opponentPowerMove?.Type == PowerMoveType.MirrorMirror)
            {
                isFaded[2] = true;
            }
            else if (powerMove?.Type == PowerMoveType.MirrorMirror)
            {
                powerMove.Type = opponentPowerMove?.Type ?? PowerMoveType.None;
            }
            else if (opponentPowerMove?.Type == PowerMoveType.MirrorMirror)
            {
                opponentPowerMove.Type = powerMove?.Type ?? PowerMoveType.None;
            }

            //Deck Swap
            if (powerMove?.Type == PowerMoveType.DeckSwap ^ opponentPowerMove?.Type == PowerMoveType.DeckSwap)
            {
                Card currentCard = card;
                card = opponentCard;
                opponentCard = currentCard;
                cardAmount = card.Amount;
            }
            else if (powerMove?.Type == PowerMoveType.DeckSwap && opponentPowerMove?.Type == PowerMoveType.DeckSwap)
            {
                isFaded[2] = true;
                powerMove.Type = PowerMoveType.None;
                opponentPowerMove.Type = PowerMoveType.None;
            }

            if ((!isOpponent || !isCPU) && !_relicsRepository.HasEarned(card.Tier, settings.MatchLevel))
            {
                hasBackfired[0] = true;
            }

            //Enhancer Swap
            if (powerMove?.Type == PowerMoveType.EnhancerSwap ^ opponentPowerMove?.Type == PowerMoveType.EnhancerSwap)
            {
                Enhancer currentEnhancer = enhancer;
                enhancer = opponentEnhancer;
                opponentEnhancer = currentEnhancer;
            }
            else if (powerMove?.Type == PowerMoveType.EnhancerSwap && opponentPowerMove?.Type == PowerMoveType.EnhancerSwap)
            {
                isFaded[2] = true;
                powerMove.Type = PowerMoveType.None;
                opponentPowerMove.Type = PowerMoveType.None;
            }

            if ((!isOpponent || !isCPU) && enhancer != null && !_relicsRepository.HasEarned(enhancer.Tier, settings.MatchLevel))
            {
                hasBackfired[1] = true;
            }

            //Topsy-Turvy - Card
            if (powerMove?.Type == PowerMoveType.TopsyTurvy)
            {
                cardAmount *= 2;
            }

            //Blocker's Bane
            if (powerMove?.Type == PowerMoveType.BlockersBane || opponentPowerMove?.Type == PowerMoveType.BlockersBaneNegative)
            {
                opponentEnhancer = null;
            }
            if (powerMove?.Type == PowerMoveType.BlockersBaneNegative || opponentPowerMove?.Type == PowerMoveType.BlockersBane)
            {
                enhancer = null;
            }

            //Color Boost
            if (powerMove?.Type == PowerMoveType.ColorBoost)
            {
                cardAmount *= 2;
            }
            if (opponentPowerMove?.Type == PowerMoveType.ColorBoost && opponentCard?.Color == card.Color)
            {
                cardAmount *= 2;
            }

            //Color Reduce
            if (powerMove?.Type == PowerMoveType.ColorReduce && card.Color == opponentCard?.Color)
            {
                cardAmount /= 2;
            }
            if (opponentPowerMove?.Type == PowerMoveType.ColorReduce)
            {
                cardAmount /= 2;
            }

            //Winning Loser
            if (powerMove?.Type == PowerMoveType.WinningLoser || powerMove?.Type == PowerMoveType.UnderdogRiseNegative)
            {
                if (card.Amount < opponentCard?.Amount)
                {
                    cardAmount *= 2;
                }
                else if (card.Amount > opponentCard?.Amount)
                {
                    powerMove.Type = PowerMoveType.UnderdogRiseNegative;
                    powerMove.IsNegative = true;
                }
                else if (card.Amount == opponentCard?.Amount)
                {
                    powerMove.Type = PowerMoveType.None;
                }
            }
            if (opponentPowerMove?.Type == PowerMoveType.WinningLoser || opponentPowerMove?.Type == PowerMoveType.UnderdogRiseNegative)
            {
                if (card.Amount < opponentCard?.Amount)
                {
                    cardAmount *= 2;
                    opponentPowerMove.Type = PowerMoveType.UnderdogRiseNegative;
                    opponentPowerMove.IsNegative = true;
                }
                else if (card.Amount == opponentCard?.Amount)
                {
                    opponentPowerMove.Type = PowerMoveType.None;
                }
            }


            //Lose Control
            if (opponentPowerMove?.Type == PowerMoveType.LoseControl && !isOpponent)
            {
                List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
                MatchState nextMatchState = matchStates.Find(x => x.Position == stadiumViewModel.MatchState.Position + 1);
                if (nextMatchState?.CardId == Guid.Empty)
                {
                    Random rnd = new Random();
                    List<Card> availableDeck = GetAvailableDeck(deck, matchStates, false);
                    nextMatchState.CardId = availableDeck[rnd.Next(availableDeck.Count)].SelectionId;
                    _matchStateRepository.UpdateMatchState(user, nextMatchState);
                }
            }

            //Double Dip
            if (powerMove?.Type == PowerMoveType.DoubleDip && opponentPowerMove?.Type == PowerMoveType.DoubleDip)
            {
                isFaded[2] = true;
            }
            else if (opponentPowerMove?.Type == PowerMoveType.DoubleDip && (isCPU || !isOpponent))
            {
                List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
                MatchState nextMatchState = matchStates.Find(x => x.Position == stadiumViewModel.MatchState.Position + 1);
                if (isCPU && isOpponent)
                {
                    if (nextMatchState?.CPUPowerMove == PowerMoveType.None && powerMove != null && powerMove.Type != PowerMoveType.None)
                    {
                        nextMatchState.CPUPowerMove = (PowerMoveType)Math.Abs((int)powerMove.Type);
                        _matchStateRepository.UpdateMatchState(user, nextMatchState);
                    }
                }
                else
                {
                    if (nextMatchState?.PowerMove == PowerMoveType.None && powerMove != null && powerMove.Type != PowerMoveType.None)
                    {
                        nextMatchState.PowerMove = (PowerMoveType)Math.Abs((int)powerMove.Type);
                        _matchStateRepository.UpdateMatchState(user, nextMatchState);
                    }
                }
            }
            else if (powerMove?.Type == PowerMoveType.DoubleDip && (opponentPowerMove == null || opponentPowerMove.Type == PowerMoveType.None))
            {
                isFaded[2] = true;
            }

            //Confusion
            if (opponentPowerMove?.Type == PowerMoveType.Confusion)
            {
                if (isOpponent)
                {
                    if (!userSettings.OpponentStadiumIsConfused)
                    {
                        userSettings.OpponentStadiumIsConfused = true;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumIsConfused), userSettings.OpponentStadiumIsConfused.ToString());
                    }
                }
                else
                {
                    if (!userSettings.StadiumIsConfused)
                    {
                        userSettings.StadiumIsConfused = true;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumIsConfused), userSettings.StadiumIsConfused.ToString());
                    }
                }
            }

            //Revival
            if (powerMove?.Type == PowerMoveType.Revival)
            {
                if (isOpponent)
                {
                    if (currentRound > 0 && userSettings.OpponentStadiumRevivalRound != currentRound - 1)
                    {
                        userSettings.OpponentStadiumRevivalRound = currentRound - 1;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumRevivalRound), userSettings.OpponentStadiumRevivalRound.ToString());
                    }
                }
                else
                {
                    if (currentRound > 0 && userSettings.StadiumRevivalRound != currentRound - 1)
                    {
                        userSettings.StadiumRevivalRound = currentRound - 1;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumRevivalRound), userSettings.StadiumRevivalRound.ToString());
                    }
                }
            }

            //Planned Strike
            if (powerMove?.Type == PowerMoveType.PlannedStrike)
            {
                if (isOpponent)
                {
                    if (userSettings.OpponentStadiumPlannedStrikeMultiplier == 0)
                    {
                        userSettings.OpponentStadiumPlannedStrikeMultiplier = 2;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumPlannedStrikeMultiplier), userSettings.OpponentStadiumPlannedStrikeMultiplier.ToString());
                    }
                }
                else
                {
                    if (userSettings.StadiumPlannedStrikeMultiplier == 0)
                    {
                        userSettings.StadiumPlannedStrikeMultiplier = 2;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumPlannedStrikeMultiplier), userSettings.StadiumPlannedStrikeMultiplier.ToString());
                    }
                }
            }

            //Self-Destruct
            if (powerMove?.Type == PowerMoveType.SelfDestruct)
            {
                if (isOpponent)
                {
                    if (userSettings.OpponentStadiumSelfDestructMultiplier == 0)
                    {
                        userSettings.OpponentStadiumSelfDestructMultiplier = 2;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumSelfDestructMultiplier), userSettings.OpponentStadiumSelfDestructMultiplier.ToString());
                    }
                }
                else
                {
                    if (userSettings.StadiumSelfDestructMultiplier == 0)
                    {
                        userSettings.StadiumSelfDestructMultiplier = 2;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumSelfDestructMultiplier), userSettings.StadiumSelfDestructMultiplier.ToString());
                    }
                }
                isFaded[0] = true;
                isFaded[1] = true;
                return 0;
            }
            if (opponentPowerMove?.Type == PowerMoveType.SelfDestruct)
            {
                opponentEnhancer = null;
            }

            //Luck Up
            if (powerMove?.Type == PowerMoveType.LuckUp)
            {
                if (isOpponent)
                {
                    if (!userSettings.OpponentStadiumHasLuckUp)
                    {
                        userSettings.OpponentStadiumHasLuckUp = true;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumHasLuckUp), userSettings.OpponentStadiumHasLuckUp.ToString());
                    }
                }
                else
                {
                    if (!userSettings.StadiumHasLuckUp)
                    {
                        userSettings.StadiumHasLuckUp = true;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumHasLuckUp), userSettings.StadiumHasLuckUp.ToString());
                    }
                }
            }

            //Poison Touch
            if (powerMove?.Type == PowerMoveType.PoisonTouch)
            {
                if (isOpponent)
                {
                    if (userSettings.StadiumPoisonTouchCurse == 0)
                    {
                        userSettings.StadiumPoisonTouchCurse = 15;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumPoisonTouchCurse), userSettings.StadiumPoisonTouchCurse.ToString());
                    }
                }
                else
                {
                    if (userSettings.OpponentStadiumPoisonTouchCurse == 0)
                    {
                        userSettings.OpponentStadiumPoisonTouchCurse = 15;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentStadiumPoisonTouchCurse), userSettings.OpponentStadiumPoisonTouchCurse.ToString());
                    }
                }
            }

            double totalPower = cardAmount;
            if (roundPowerStage == RoundPowerStage.Final) //if we don't want any bonuses affected in the round power yet, don't set it
            {
                double bonusPercent = 1;
                if (deck.All(x => x.CreatedDate > DateTime.MinValue && x.GetAgeInHours() >= 720))
                {
                    bonusPercent += 0.1;
                }
                if (deck.All(x => x.Color == deck.First().Color))
                {
                    bonusPercent += 0.15;
                }
                if (deck.GroupBy(x => x.Color).ToList().Count == deck.Count)
                {
                    bonusPercent += 0.2;
                }
                double themeBoost = GetThemeBoost(deck, settings);
                if (themeBoost > 0)
                {
                    bonusPercent += themeBoost - 1;
                }
                if (stadiumViewModel.IsCPU)
                {
                    if (settings != null && (DateTime.UtcNow - settings.CandyExpiryDate).TotalMinutes < 0)
                    {
                        bonusPercent += (double)settings.CandyPercent / 100;
                    }
                    if (settings != null && (DateTime.UtcNow - settings.StadiumBonusExpiryDate).TotalMinutes < 0)
                    {
                        bonusPercent += (double)settings.StadiumBonusPercent / 100;
                    }
                    if (settings != null && (DateTime.UtcNow - settings.CurseExpiryDate).TotalMinutes < 0)
                    {
                        bonusPercent += (double)settings.CursePercent / 100;
                    }
                }

                if (isOpponent) //adds filler and intensity effects
                {
                    if (userSettings.OpponentFillerPercent > 0)
                    {
                        bonusPercent += (double)userSettings.OpponentFillerPercent / 100;
                    }
                    if (userSettings.OpponentStadiumSelfDestructMultiplier > 0)
                    {
                        bonusPercent += (double)userSettings.OpponentStadiumSelfDestructMultiplier - 1;
                    }
                    if (currentRound == Helper.GetMaxDeckSize() - 1 && userSettings.OpponentStadiumPlannedStrikeMultiplier > 0)
                    {
                        bonusPercent += (double)userSettings.OpponentStadiumPlannedStrikeMultiplier - 1;
                    }
                    if (opponentPowerMove?.Type != PowerMoveType.PoisonTouch && userSettings.OpponentStadiumPoisonTouchCurse > 0)
                    {
                        bonusPercent -= (double)userSettings.OpponentStadiumPoisonTouchCurse / 100;
                    }
                    if (powerMove?.Type != PowerMoveType.PsychUp && userSettings.StadiumOpponentPsychUpCurse > 0)
                    {
                        bonusPercent -= (double)userSettings.StadiumOpponentPsychUpCurse / 100;
                    }
                    if (userSettings.StadiumCPUBonus > 1)
                    {
                        bonusPercent += (double)userSettings.StadiumCPUBonus - 1;
                    }
                    if (isCPU && !isCPUChallenge && !isSpecialCPU && cpu != null && cpu.Intensity > 0)
                    {
                        bonusPercent += Math.Min(cpu.Intensity + 1, 9);
                    }
                }
                else
                {
                    if (userSettings.FillerPercent > 0)
                    {
                        bonusPercent += (double)userSettings.FillerPercent / 100;
                    }
                    if (userSettings.StadiumSelfDestructMultiplier > 0)
                    {
                        bonusPercent += (double)userSettings.StadiumSelfDestructMultiplier - 1;
                    }
                    if (currentRound == Helper.GetMaxDeckSize() - 1 && userSettings.StadiumPlannedStrikeMultiplier > 0)
                    {
                        bonusPercent += (double)userSettings.StadiumPlannedStrikeMultiplier - 1;
                    }
                    if (opponentPowerMove?.Type != PowerMoveType.PoisonTouch && userSettings.StadiumPoisonTouchCurse > 0)
                    {
                        bonusPercent -= (double)userSettings.StadiumPoisonTouchCurse / 100;
                    }
                    if (powerMove?.Type != PowerMoveType.PsychUp && userSettings.StadiumPsychUpCurse > 0)
                    {
                        bonusPercent -= (double)userSettings.StadiumPsychUpCurse / 100;
                    }
                }

                hasBackfired[0] = false;
                hasBackfired[1] = false;

                int roundPowerDeduction = 0;
                if (!isOpponent || !isCPU)
                {
                    if (!_relicsRepository.HasEarned(card.Tier, settings.MatchLevel))
                    {
                        roundPowerDeduction += 50;
                        hasBackfired[0] = true;
                    }
                    if (enhancer != null)
                    {
                        if (!_relicsRepository.HasEarned(enhancer.Tier, settings.MatchLevel))
                        {
                            roundPowerDeduction += 50;
                            hasBackfired[1] = true;
                        }
                    }

                    if (roundPowerDeduction > 0)
                    {
                        bonusPercent -= (double)roundPowerDeduction / 100;
                    }
                }

                totalPower *= Math.Max(0, bonusPercent);
            }

            //Flowing Power
            if (powerMove?.Type == PowerMoveType.FlowingPower)
            {
                totalPower *= 1.25;
            }

            //All Out Attack
            if (powerMove?.Type == PowerMoveType.AllOutAttack || powerMove?.Type == PowerMoveType.AllOutAttackNegative)
            {
                totalPower *= powerMove.Type == PowerMoveType.AllOutAttackNegative ? 0.5 : 2;
            }

            //Psych Up
            if (powerMove?.Type == PowerMoveType.PsychUp)
            {
                totalPower *= 2;
                if (isOpponent)
                {
                    if (userSettings.StadiumOpponentPsychUpCurse == 0)
                    {
                        userSettings.StadiumOpponentPsychUpCurse = 15;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumOpponentPsychUpCurse), userSettings.StadiumOpponentPsychUpCurse.ToString());
                    }
                }
                else
                {
                    if (userSettings.StadiumPsychUpCurse == 0)
                    {
                        userSettings.StadiumPsychUpCurse = 15;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumPsychUpCurse), userSettings.StadiumPsychUpCurse.ToString());
                    }
                }
            }

            //Lady Luck
            if (opponentPowerMove?.Type == PowerMoveType.LadyLuck || opponentPowerMove?.Type == PowerMoveType.LadyLuckNegative)
            {
                totalPower *= opponentPowerMove.Type == PowerMoveType.LadyLuckNegative ? 2 : 0.5;
            }

            //Big Shot
            if (powerMove?.Type == PowerMoveType.BigShot)
            {
                totalPower *= 2;
            }
            if (opponentPowerMove?.Type == PowerMoveType.BigShotNegative)
            {
                totalPower *= 2;
            }

            int enhancerAmount = enhancer?.Amount ?? 0;
            int opponentEnhancerAmount = opponentEnhancer?.Amount ?? 0;

            //Topsy-Turvy - Enhancer
            if (powerMove?.Type == PowerMoveType.TopsyTurvy)
            {
                opponentEnhancerAmount *= 2;
            }
            if (opponentPowerMove?.Type == PowerMoveType.TopsyTurvy)
            {
                enhancerAmount *= 2;
            }

            //Silenced Enhancers
            if (powerMove?.Type == PowerMoveType.SilencedEnhancers || opponentPowerMove?.Type == PowerMoveType.SilencedEnhancers)
            {
                isFaded[1] = true;
                return totalPower;
            }

            int enhancerMultiplier = 1;
            if (stadiumChallenge == CPUChallenge.EnhancersWorthDouble)
            {
                enhancerMultiplier *= 2;
            }

            //Doubled Enhancers
            if (powerMove?.Type == PowerMoveType.DoubledEnhancers)
            {
                enhancerMultiplier *= 2;
            }
            if (opponentPowerMove?.Type == PowerMoveType.DoubledEnhancers)
            {
                enhancerMultiplier *= 2;
            }

            if ((opponentEnhancer?.SpecialType == EnhancerSpecialType.Blocker && enhancer?.SpecialType != EnhancerSpecialType.Predictor) || (enhancer?.SpecialType == EnhancerSpecialType.Predictor && opponentEnhancer?.SpecialType != EnhancerSpecialType.Blocker))
            {
                enhancer = null;
            }

            enhancerAmount *= enhancerMultiplier;
            opponentEnhancerAmount *= enhancerMultiplier;
            if (opponentEnhancer?.SpecialType == EnhancerSpecialType.Dark && (enhancer == null || enhancer?.SpecialType != EnhancerSpecialType.Blocker))
            {
                totalPower *= Math.Max(0, 1 - (double)opponentEnhancerAmount / 100);
            }
            if (enhancer == null)
            {
                hasBackfired[1] = false;
                isFaded[1] = true;
                return totalPower;
            }
            if (enhancer.SpecialType == EnhancerSpecialType.Dark)
            {
                return totalPower;
            }
            if (enhancer.IsMultiplied)
            {
                return totalPower * enhancerAmount;
            }
            if (enhancer.IsPercent)
            {
                return totalPower * (1 + (double)enhancerAmount / 100);
            }
            if (enhancer.IsFiller)
            {
                if (roundPowerStage == RoundPowerStage.Final)
                {
                    if (isOpponent && userSettings.OpponentFillerPercent != enhancerAmount)
                    {
                        userSettings.OpponentFillerPercent = enhancerAmount;
                        _settingsRepository.SetSetting(user, nameof(userSettings.OpponentFillerPercent), userSettings.OpponentFillerPercent.ToString());
                    }
                    if (!isOpponent && userSettings.FillerPercent != enhancerAmount)
                    {
                        userSettings.FillerPercent = enhancerAmount;
                        _settingsRepository.SetSetting(user, nameof(userSettings.FillerPercent), userSettings.FillerPercent.ToString());
                    }
                }
                isFaded[0] = true;
                return 0;
            }
            if (enhancer.SpecialType == EnhancerSpecialType.Luck)
            {
                if (roundPowerStage == RoundPowerStage.Final)
                {
                    if (!isOpponent && userSettings.StadiumRewardLuckBonus <= 0)
                    {
                        userSettings.StadiumRewardLuckBonus = 1 + (double)enhancerAmount / 100;
                        _settingsRepository.SetSetting(user, nameof(userSettings.StadiumRewardLuckBonus), userSettings.StadiumRewardLuckBonus.ToString());
                    }
                }
                return totalPower;
            }
            if (opponentEnhancer?.SpecialType == EnhancerSpecialType.Blocker && enhancer.SpecialType == EnhancerSpecialType.Predictor)
            {
                return totalPower * enhancerAmount;
            }

            return totalPower + (enhancerAmount * totalPower / cardAmount);
        }

        private MatchState GetNextMatchState(User user, List<MatchState> matchStates = null)
        {
            if (matchStates == null)
            {
                matchStates = _matchStateRepository.GetMatchStates(user);
            }

            return matchStates.Find(x => x.Status == MatchStatus.NotPlayed);
        }

        private void ExpireMatch(User user, string opponentName, int points)
        {
            MatchState matchState = GetNextMatchState(user);
            MatchHistory matchHistory = new MatchHistory
            {
                Username = user.Username,
                Opponent = opponentName,
                CPUType = CPUType.None,
                Points = points,
                Result = MatchHistoryResult.Lost,
                TourneyMatchId = _matchStateRepository.GetMatchStates(user).Find(x => x.TourneyMatchId != Guid.Empty)?.TourneyMatchId ?? Guid.Empty
            };

            bool opponentExpired = false;
            User opponent = _userRepository.GetUser(opponentName);
            if (opponent != null)
            {
                List<MatchState> opponentMatchStates = _matchStateRepository.GetMatchStates(opponent, true, false);
                MatchState opponentMatchState = opponentMatchStates.FindAll(x => x.Status == MatchStatus.NotPlayed).FirstOrDefault();
                if (matchState != null && opponentMatchState != null)
                {
                    if (matchState.Position == opponentMatchState.Position)
                    {
                        if (opponentMatchState.IsInSelection && !matchState.IsInSelection)
                        {
                            opponentExpired = true;
                        }

                    }
                    else if (matchState.Position > opponentMatchState.Position)
                    {
                        opponentExpired = true;
                    }

                    if (opponentExpired && matchState.Points > 0)
                    {
                        _pointsRepository.AddPoints(user, matchState.Points * 2);
                        matchHistory.Result = MatchHistoryResult.Won;

                        Settings settings = _settingsRepository.GetSettings(user);
                        settings.TrainingTokens += _matchHistoryRepository.IsElite(user) ? 5 : 2;
                        _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());
                    }
                    else if (matchState.Points > 0)
                    {
                        _pointsRepository.AddPoints(opponent, opponentMatchState.Points * 2);

                        Settings opponentSettings = _settingsRepository.GetSettings(opponent, false);
                        opponentSettings.TrainingTokens += _matchHistoryRepository.IsElite(opponent) ? 5 : 2;
                        _settingsRepository.SetSetting(opponent, nameof(opponentSettings.TrainingTokens), opponentSettings.TrainingTokens.ToString(), false);
                    }
                    _matchHistoryRepository.AddMatchHistory(user, matchHistory);
                    _matchHistoryRepository.AddMatchHistory(opponent, new MatchHistory
                    {
                        Username = opponentName,
                        Opponent = user.Username,
                        CPUType = CPUType.None,
                        Points = opponentMatchState.Points,
                        Result = opponentExpired ? MatchHistoryResult.Lost : MatchHistoryResult.Won,
                        TourneyMatchId = opponentMatchStates.Find(x => x.TourneyMatchId != Guid.Empty)?.TourneyMatchId ?? Guid.Empty
                    }, false);
                    AddMatchEndedAccomplishments(opponent, !opponentExpired, true, 0, matchState, 0, _settingsRepository.GetSettings(user));
                    if (matchState.TourneyMatchId != Guid.Empty)
                    {
                        _tourneyRepository.AddTourneyProgress(user, matchState.TourneyMatchId, opponentExpired ? MatchStatus.Won : MatchStatus.Lost, true);
                        _tourneyRepository.AddTourneyProgress(opponent, matchState.TourneyMatchId, opponentExpired ? MatchStatus.Lost : MatchStatus.Won, false);
                    }
                }
            }
            _matchStateRepository.DeleteMatchState(user, opponentName, false, true);
            _itemsRepository.RemoveUsedItems(user);
        }

        private bool IsWaitingForMatch(User user, out MatchWaitlist matchWaitList)
        {
            matchWaitList = _matchStateRepository.GetMatchWaitlist(user);
            if (matchWaitList == null)
            {
                return false;
            }
            if (matchWaitList.IsExpired())
            {
                _matchStateRepository.DeleteMatchWaitlist(user);
                return false;
            }

            return true;
        }

        private string FindOpponent(User user, int betAmount, bool isElite, string challengeUser, Guid tourneyMatchId)
        {
            MatchWaitlist matchWaitlist = _matchStateRepository.AddOrUpdateMatchWaitlist(user, betAmount, isElite, challengeUser, tourneyMatchId);
            if (matchWaitlist == null || string.IsNullOrEmpty(matchWaitlist.Username2))
            {
                return string.Empty;
            }

            return matchWaitlist.Username;
        }

        private void InitializeMatch(User user, List<Card> deck, int betAmount, bool isCPUMatch, bool isCPUChallenge, CPU cpu, string opponentName, Guid tourneyMatchId)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            ResetLastMatchSettings(user, settings);
            EndMatch();

            if (isCPUMatch)
            {
                if (cpu == null)
                {
                    cpu = isCPUChallenge ? _cpuRepository.MakeChallenge(settings.StadiumChallenge, settings.StadiumChallengeWins) : _cpuRepository.GetCPU(settings.MatchLevel, true, true);
                }
                if (cpu.IsSpecial || isCPUChallenge)
                {
                    if (cpu.Intensity > 1)
                    {
                        settings.StadiumCPUBonus = cpu.Intensity;
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumCPUBonus), settings.StadiumCPUBonus.ToString());
                    }
                }
                else if (!settings.EnableCPUChallengingDifficulty)
                {
                    List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(user);
                    int cpuMatchCount = GetCPUMatchCount(matchHistories, cpu);
                    if ((cpuMatchCount >= 1 && cpu.Intensity == 0) || (cpuMatchCount >= 3 && cpu.Intensity > 0 && cpu.Intensity < 5))
                    {
                        Random rnd = new Random();
                        List<int> possibleIndexes = new List<int> { 0, 1, 2 };
                        int randomIndex = possibleIndexes[rnd.Next(possibleIndexes.Count)];
                        possibleIndexes.Remove(randomIndex);
                        cpu.Enhancers[randomIndex] = null;
                        if ((settings.MatchLevel < 5 && cpuMatchCount >= 2) || (cpuMatchCount >= 3 && cpu.Intensity == 0))
                        {
                            cpu.Enhancers[possibleIndexes[rnd.Next(possibleIndexes.Count)]] = null;
                        }
                    }
                }
            }

            for (int i = 0; i < deck.Count; i++)
            {
                MatchState newMatchState = new MatchState
                {
                    Username = user.Username,
                    Opponent = isCPUMatch ? cpu.Name : opponentName,
                    CardId = Guid.Empty,
                    Position = i,
                    Points = isCPUMatch ? cpu.Reward * (settings.HasDoubleStadiumCPUReward ? 2 : 1) : betAmount,
                    Status = MatchStatus.NotPlayed,
                    IsInSelection = true,
                    TourneyMatchId = tourneyMatchId,
                    IsCPU = isCPUMatch,
                    IsCPUChallenge = isCPUChallenge,
                    ImageUrl = isCPUMatch ? string.IsNullOrEmpty(cpu.ImageUrl) ? string.Empty : Helper.GetImageUrl(cpu.ImageUrl) : string.Empty,
                    IsSpecialCPU = cpu?.IsSpecial ?? false
                };

                if (isCPUMatch)
                {
                    newMatchState.CPUCardId = cpu.Deck[i].Id;
                    newMatchState.CPUCardTheme = cpu.Deck[i].Theme;
                    if (cpu.Enhancers[i] != null)
                    {
                        newMatchState.CPUEnhancerId = cpu.Enhancers[i].Id;
                    }
                }

                if (!_matchStateRepository.AddMatchState(user, newMatchState))
                {
                    _matchStateRepository.DeleteMatchState(user, newMatchState.Opponent, newMatchState.IsCPU, false);
                    i = -1;
                }
            }

            if (isCPUMatch == false && betAmount > 0)
            {
                _pointsRepository.SubtractPoints(user, betAmount);

                MatchRanking matchRanking = _matchHistoryRepository.GetMatchRanking(user);
                int rank = matchRanking?.Rank ?? 0;
                if (rank != settings.MatchStartGlobalRanking)
                {
                    settings.MatchStartGlobalRanking = rank;
                    _settingsRepository.SetSetting(user, nameof(settings.MatchStartGlobalRanking), settings.MatchStartGlobalRanking.ToString());
                }
            }
        }

        private double GetUserWinPercent(List<MatchHistory> matchHistories)
        {
            List<MatchHistory> userMatches = matchHistories.FindAll(x => x.CPUType == CPUType.None && x.Points > 0);
            if (userMatches.Count == 0)
            {
                return 0;
            }
            int userWins = userMatches.FindAll(x => x.Result == MatchHistoryResult.Won).Count;
            return Math.Round((double)userWins / userMatches.Count * 100, 1);
        }

        private int GetCPUMatchCount(List<MatchHistory> matchHistories, CPU cpu)
        {
            List<MatchHistory> CPUMatches = matchHistories.FindAll(x => x.CPUType == CPUType.Normal);
            if (CPUMatches.Count == 0)
            {
                return -1;
            }
            int rematches = CPUMatches.FindAll(x => x.Level == cpu.Level && x.Intensity == cpu.Intensity).Count;
            return rematches;
        }

        private int GetWinStreak(User user, List<MatchHistory> matchHistories, bool isCPU)
        {
            List<MatchHistory> matches = matchHistories.FindAll(x => x.CPUType == (isCPU ? CPUType.Normal : CPUType.None) && x.Points > 0);
            if (matches.Count == 0)
            {
                return 0;
            }

            int winStreak = 0;
            foreach (MatchHistory matchHistory in matches)
            {
                if (matchHistory.Result == MatchHistoryResult.Lost)
                {
                    break;
                }
                winStreak++;
            }

            if (winStreak > 1 && isCPU)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumCPUWinStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumCPUWinStreak);
                }
                if (winStreak >= 5)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumCPUFiveWinStreak))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumCPUFiveWinStreak);
                    }
                }
                if (winStreak >= 10)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumCPUTenWinStreak))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumCPUTenWinStreak);
                    }
                }
            }

            return winStreak;
        }

        private void AddMatchEndedAccomplishments(User user, bool matchWon, bool isOpponent, int CPULevel, MatchState matchState, int challengesWon, Settings settings)
        {
            bool isUser = !isOpponent;
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user, isUser);
            if (matchState.IsCPUChallenge)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstStadiumChallenge))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstStadiumChallenge);
                }
                if (!matchWon)
                {
                    return;
                }
                challengesWon++;
                if (challengesWon >= 5 && !accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumChallenge5Wins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumChallenge5Wins);
                }
                if (challengesWon >= 10 && !accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumChallenge10Wins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumChallenge10Wins);
                }
                if (challengesWon >= 25 && !accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumChallenge25Wins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumChallenge25Wins);
                }
                if (challengesWon >= 50 && !accomplishments.Exists(x => x.Id == AccomplishmentId.StadiumChallenge50Wins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StadiumChallenge50Wins);
                }
            }
            else if (matchState.IsCPU)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstCPUMatch))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstCPUMatch);
                }
                if (!matchWon)
                {
                    return;
                }
                if (matchState.IsSpecialCPU)
                {
                    if (matchState.Opponent == SpecialOpponents.Guard.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RightSideUpGuardDefeated);
                    }
                    else if (matchState.Opponent == SpecialOpponents.ShowroomOwner.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomAccess))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ShowroomAccess);
                    }
                    else if (matchState.Opponent == SpecialOpponents.Unknown.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.UnknownDefeated))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.UnknownDefeated);
                    }
                    else if (matchState.Opponent == SpecialOpponents.PhantomSpirit.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.PhantomSpiritDefeated))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.PhantomSpiritDefeated);
                    }
                    else if ((matchState.Opponent == SpecialOpponents.Evena.GetDisplayName() || matchState.Opponent == SpecialOpponents.Oddna.GetDisplayName()) && !accomplishments.Exists(x => x.Id == AccomplishmentId.SpiritSistersDefeated))
                    {
                        if (settings.HasDefeatedEvena && matchState.Opponent == SpecialOpponents.Oddna.GetDisplayName() || settings.HasDefeatedOddna && matchState.Opponent == SpecialOpponents.Evena.GetDisplayName())
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SpiritSistersDefeated);
                        }
                    }
                    else if (matchState.Opponent == SpecialOpponents.PhantomBoss.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.PhantomBossDefeated))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.PhantomBossDefeated);
                    }
                    else if (matchState.Opponent == SpecialOpponents.BeyondCreature.GetDisplayName() && !accomplishments.Exists(x => x.Id == AccomplishmentId.DefeatBeyondCreature))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DefeatBeyondCreature);
                    }
                }
                else
                {
                    int cpuChallengeCount = _cpuRepository.GetCPUCount();
                    if (CPULevel >= 2 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelTwo))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelTwo);
                    }
                    if (CPULevel >= 5 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelFive))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelFive);
                    }
                    if (CPULevel >= 10 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelTen))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelTen);
                    }
                    if (CPULevel >= 15 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelFifteen))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelFifteen);
                    }
                    if (CPULevel >= 20 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelTwenty))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelTwenty);
                    }
                    if (CPULevel >= 25 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelTwentyFive))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelTwentyFive);
                    }
                    if (CPULevel >= 30 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPULevelThirty))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPULevelThirty);
                    }
                    if (CPULevel >= cpuChallengeCount && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPUCompleted))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPUCompleted);
                    }
                    if (CPULevel >= cpuChallengeCount * 2 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPUCompletedSecondIntensity))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPUCompletedSecondIntensity);
                    }
                    if (CPULevel >= cpuChallengeCount * 3 && !accomplishments.Exists(x => x.Id == AccomplishmentId.CPUCompletedThirdIntensity))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CPUCompletedThirdIntensity);
                    }
                }
            }
            else
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstUserMatch))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstUserMatch, isUser);
                }
                List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(user, true, !isOpponent).FindAll(x => x.Result == MatchHistoryResult.Won);
                if (matchHistories.Count >= 200 && !accomplishments.Exists(x => x.Id == AccomplishmentId.TwoHundredUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TwoHundredUserWins, isUser);
                }
                if (matchHistories.Count >= 100 && !accomplishments.Exists(x => x.Id == AccomplishmentId.OneHundredUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OneHundredUserWins, isUser);
                }
                if (matchHistories.Count >= 50 && !accomplishments.Exists(x => x.Id == AccomplishmentId.FiftyUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FiftyUserWins, isUser);
                }
                if (matchHistories.Count >= 25 && !accomplishments.Exists(x => x.Id == AccomplishmentId.TwentyFiveUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TwentyFiveUserWins, isUser);
                }
                if (matchHistories.Count >= 15 && !accomplishments.Exists(x => x.Id == AccomplishmentId.FifteenUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FifteenUserWins, isUser);
                }
                if (matchHistories.Count >= 5 && !accomplishments.Exists(x => x.Id == AccomplishmentId.FiveUserWins))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FiveUserWins, isUser);
                }
            }
        }

        private void AddChallengerDomeAccomplishments(User user, int streak)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (streak >= 5)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ChallengerDomeFiveDayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ChallengerDomeFiveDayStreak);
                }
            }
            if (streak >= 10)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ChallengerDomeTenDayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ChallengerDomeTenDayStreak);
                }
            }
            if (streak >= 25)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ChallengerDomeTwentyFiveDayStreak))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ChallengerDomeTwentyFiveDayStreak);
                }
            }
        }

        private string GenerateTip()
        {
            List<string> tips = new List<string>
            {
                "If you can get your Global Ranking strong enough, you might qualify for the Elite Zone!",
                "As the difficulty of CPU Challengers progresses, your reward will be greater!",
                "If you wait until the timer runs out, the match will expire and you'll get a loss if it was your fault.",
                "You can sometimes find rare and powerful items in Card Packs.",
                "Only matches against real users will have an effect on your Global Ranking.",
                "Choose your Enhancer wisely. It can be the difference between winning or losing a round.",
                $"In the event of a tie after {Helper.GetMaxDeckSize()} rounds, you'll enter Sudden Death. The first to win a round will win the match!",
                $"{Resources.DeckCards} are ranked in strength by their color: Red, Orange, Yellow, Green, Blue, and Purple.",
                $"A strong Enhancer can be more valuable than a strong {Resources.DeckCard}.",
                $"If all your {Resources.DeckCards} are the same color, you'll get a 15% Color Bonus for each round.",
                "You can hide CPU Match Statistics in your Preferences along with other options!",
                $"You can remove any active Stadium effects for a cost of {Helper.GetFormattedPoints(100)}.",
                "Visit the Repair Shop often as an item that has an Unusable condition cannot be used at the Stadium.",
                $"You can give your {Resources.DeckCards}' themes. If your entire Deck has the same theme, you'll get a special bonus.",
                $"Every 5 CPU matches, you'll encounter a Checkpoint CPU who will be more powerful but offers a greater reward.",
                $"You can get {Resources.Currency} bonuses for winning multiple Stadium matches in a row.",
                $"Certain Enhancers like Blockers can stop your opponent from playing an Enhancer for that round.",
                $"It's usually worth buying an unlimited-use version of an Enhancer if you can afford it.",
                $"You can unlock new Power Moves by defeating CPU Challengers.",
                $"You can challenge someone for fun from their profile but this will not affect your Global Ranking or earn you any {Resources.Currency}.",
                $"Some Power Moves have a positive or negative effect. These are risky to use but can have a great outcome.",
                $"Bonuses from Candy Cards, {Resources.SiteName} Club, and outside of Stadium sources only work for CPU matches."
            };

            Random rnd = new Random();
            return tips[rnd.Next(tips.Count)];
        }

        private string GenerateStrategyTip()
        {
            List<string> tips = new List<string>
            {
                $"Try playing a Filler with an Enhancer Swap! Your opponent will get your Filler and get a 0 for the round.",
                $"Make sure not to waste a Self-Destruct with an Enhancer! It won't be used anyway.",
                $"If your opponent has a color bonus, use Color Boost when you've played a {Resources.DeckCard} color that isn't theirs.",
                $"If your opponent keeps playing stronger Enhancers, invest in a Blocker.",
                $"If your opponent keeps playing Blockers, invest in a Predictor.",
                $"You'll get a 15% Color Bonus if all your {Resources.DeckCards} are the same color.",
                $"You can earn new Power Moves by defeating CPU Challengers or by using Training Tokens at the Training Center.",
                $"Power Stop is a great Power Move to use in Round 5 to secure a win without unforeseen consequences!",
                $"While a Filler will make you lose the round, your Power Move still works. When you use Self-Destruct, it cancels out your Enhancer.",
                $"You can choose which Power Moves you want to take with you into the Stadium from your bag.",
                $"A strong Enhancer can be more valuable than a strong {Resources.DeckCard}.",
                $"A Lucky Clover Enhancer played with Doubled Enhancers can lead to triple the reward if you win.",
                $"CPUs will get a little easier if you've lost to them multiple times (unless you disable this in your Preferences).",
                $"Use your worst {Resources.DeckCard} with a Self-Destruct as it won't count anyway."
            };

            Random rnd = new Random();
            return tips[rnd.Next(tips.Count)];
        }

        private bool IsBadChallengeDeck(List<Card> deck, CPUChallenge challenge)
        {
            if (deck.Count < Helper.GetMaxDeckSize())
            {
                return true;
            }
            switch (challenge)
            {
                case CPUChallenge.OnlyEvenDeckCards:
                    return deck.Any(x => x.Amount % 2 != 0);
                case CPUChallenge.OnlyOddDeckCards:
                    return deck.Any(x => x.Amount % 2 == 0);
                case CPUChallenge.OnlyRedDeckCards:
                    return deck.Any(x => x.Color != CardColor.Red);
                case CPUChallenge.OnlySameColorDeckCards:
                    return !deck.All(x => x.Color == deck.First().Color);
                default:
                    return false;
            }
        }

        private List<Enhancer> GetEnhancersForMatch(User user, Settings settings, bool isCPUChallenge, CPUChallenge challenge, bool reloadPoints)
        {
            List<Enhancer> userEnhancers = _itemsRepository.GetEnhancers(user);
            if (reloadPoints)
            {
                foreach (Enhancer enhancer in userEnhancers)
                {
                    if (enhancer.Uses == 1)
                    {
                        enhancer.UpdatePoints(_repairRepository.GetRechargePoints(enhancer, settings.RepairCouponPercent));
                    }
                }
            }
            if (isCPUChallenge)
            {
                switch (challenge)
                {
                    case CPUChallenge.NoEnhancers:
                        return new List<Enhancer>();
                    case CPUChallenge.OnlyAdditionEnhancers:
                        return userEnhancers.FindAll(x => x.IsMultiplied == false && x.IsPercent == false && x.IsFiller == false && x.SpecialType == EnhancerSpecialType.None && x.Amount > 0);
                    case CPUChallenge.OnlyMultiplicationEnhancers:
                        return userEnhancers.FindAll(x => x.IsMultiplied);
                    default:
                        return userEnhancers;
                }
            }

            return userEnhancers;
        }

        private void ResetLastRoundSettings(User user, Settings settings, StadiumViewModel stadiumViewModel)
        {
            PowerMoveType powerMoveType = stadiumViewModel?.NextPowerMove?.Type ?? PowerMoveType.None;
            PowerMoveType opponentPowerMoveType = stadiumViewModel?.OpponentPowerMove?.Type ?? PowerMoveType.None;

            if (settings.StadiumSelfDestructMultiplier > 0 && powerMoveType != PowerMoveType.SelfDestruct)
            {
                settings.StadiumSelfDestructMultiplier = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumSelfDestructMultiplier), settings.StadiumSelfDestructMultiplier.ToString());
            }
            if (settings.OpponentStadiumSelfDestructMultiplier > 0 && opponentPowerMoveType != PowerMoveType.SelfDestruct)
            {
                settings.OpponentStadiumSelfDestructMultiplier = 0;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumSelfDestructMultiplier), settings.OpponentStadiumSelfDestructMultiplier.ToString());
            }
            if (settings.StadiumIsConfused && opponentPowerMoveType != PowerMoveType.Confusion)
            {
                settings.StadiumIsConfused = false;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumIsConfused), settings.StadiumIsConfused.ToString());
            }
            if (settings.OpponentStadiumIsConfused && powerMoveType != PowerMoveType.Confusion)
            {
                settings.OpponentStadiumIsConfused = false;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumIsConfused), settings.OpponentStadiumIsConfused.ToString());
            }
        }

        private void ResetLastMatchSettings(User user, Settings settings)
        {
            if (settings.FillerPercent > 0)
            {
                settings.FillerPercent = 0;
                _settingsRepository.SetSetting(user, nameof(settings.FillerPercent), settings.FillerPercent.ToString());
            }
            if (settings.OpponentFillerPercent > 0)
            {
                settings.OpponentFillerPercent = 0;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentFillerPercent), settings.OpponentFillerPercent.ToString());
            }
            if (settings.StadiumRewardLuckBonus > 0)
            {
                settings.StadiumRewardLuckBonus = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumRewardLuckBonus), settings.StadiumRewardLuckBonus.ToString());
            }
            if (settings.StadiumPlannedStrikeMultiplier > 0)
            {
                settings.StadiumPlannedStrikeMultiplier = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumPlannedStrikeMultiplier), settings.StadiumPlannedStrikeMultiplier.ToString());
            }
            if (settings.OpponentStadiumPlannedStrikeMultiplier > 0)
            {
                settings.OpponentStadiumPlannedStrikeMultiplier = 0;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumPlannedStrikeMultiplier), settings.OpponentStadiumPlannedStrikeMultiplier.ToString());
            }
            if (settings.StadiumPoisonTouchCurse > 0)
            {
                settings.StadiumPoisonTouchCurse = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumPoisonTouchCurse), settings.StadiumPoisonTouchCurse.ToString());
            }
            if (settings.OpponentStadiumPoisonTouchCurse > 0)
            {
                settings.OpponentStadiumPoisonTouchCurse = 0;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumPoisonTouchCurse), settings.OpponentStadiumPoisonTouchCurse.ToString());
            }
            if (settings.StadiumRevivalRound > -1)
            {
                settings.StadiumRevivalRound = -1;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumRevivalRound), settings.StadiumRevivalRound.ToString());
            }
            if (settings.OpponentStadiumRevivalRound > -1)
            {
                settings.OpponentStadiumRevivalRound = -1;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumRevivalRound), settings.OpponentStadiumRevivalRound.ToString());
            }
            if (settings.StadiumPsychUpCurse > 0)
            {
                settings.StadiumPsychUpCurse = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumPsychUpCurse), settings.StadiumPsychUpCurse.ToString());
            }
            if (settings.StadiumOpponentPsychUpCurse > 0)
            {
                settings.StadiumOpponentPsychUpCurse = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumOpponentPsychUpCurse), settings.StadiumOpponentPsychUpCurse.ToString());
            }
            if (settings.StadiumCPUBonus > 0)
            {
                settings.StadiumCPUBonus = 0;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumCPUBonus), settings.StadiumCPUBonus.ToString());
            }
            if (settings.StadiumHasLuckUp)
            {
                settings.StadiumHasLuckUp = false;
                _settingsRepository.SetSetting(user, nameof(settings.StadiumHasLuckUp), settings.StadiumHasLuckUp.ToString());
            }
            if (settings.OpponentStadiumHasLuckUp)
            {
                settings.OpponentStadiumHasLuckUp = false;
                _settingsRepository.SetSetting(user, nameof(settings.OpponentStadiumHasLuckUp), settings.OpponentStadiumHasLuckUp.ToString());
            }
            ResetLastRoundSettings(user, settings, null);
        }

        private double GetThemeBoost(List<Card> deck, Settings settings)
        {
            int themeBonusBoost = settings?.ThemeBonusBoost ?? 0;

            if (deck.All(x => x.Theme == ItemTheme.Default))
            {
                return 0;
            }

            double themeBonus = 0;
            if (deck.All(x => x.Theme == ItemTheme.Outline))
            {
                themeBonus = 1.15;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Striped))
            {
                themeBonus = 1.2;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Retro))
            {
                themeBonus = 1.25;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Squared))
            {
                themeBonus = 1.3;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Neon))
            {
                themeBonus = 1.35;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Astral))
            {
                themeBonus = 1.35;
            }
            else if (deck.All(x => x.Theme == ItemTheme.Club) || deck.All(x => x.Theme == ItemTheme.Elegant))
            {
                themeBonus = 1.25;
            }
            else
            {
                return 0;
            }

            return Math.Min(themeBonus + (double)themeBonusBoost / 100, 1.35);
        }

        private CPUType GetCPUType(StadiumViewModel stadiumViewModel)
        {
            if (stadiumViewModel.IsCPU)
            {
                if (stadiumViewModel.IsCPUChallenge)
                {
                    return CPUType.Challenge;
                }
                if (stadiumViewModel.IsSpecialCPU)
                {
                    return CPUType.Special;
                }
                return CPUType.Normal;
            }

            return CPUType.None;
        }

        private bool CanChallengeCPU(User user, List<Accomplishment> accomplishments, Settings settings, CPU cpu, int totalCPUs, out string reason)
        {
            reason = string.Empty;
            if (!_cpuRepository.CanChallengeCheckpointCPU(user, accomplishments, cpu, out _, out reason, out _, out _))
            {
                return false;
            }
            if (settings.MatchLevel >= totalCPUs && !accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return false;
            }

            return true;
        }

        private bool CanShowStadiumIntro(List<MatchState> matchStates)
        {
            if (matchStates == null || matchStates.Count == 0)
            {
                return false;
            }

            MatchState matchState = GetNextMatchState(null, matchStates);
            if (matchState == null)
            {
                return false;
            }

            if (!matchState.IsInSelection)
            {
                return false;
            }

            if (GlobalHttpContext.Current.Request.Cookies.ContainsKey("ShowIntro"))
            {
                return false;
            }

            if (matchState.Position == 0)
            {
                GlobalHttpContext.Current.Response.Cookies.Append("ShowIntro", "false");
                return true;
            }

            return false;
        }

        private List<Card> GetAvailableDeck(List<Card> deck, List<MatchState> matchStates, bool isPlayed = true)
        {
            if (matchStates.Count > Helper.GetMaxDeckSize())
            {
                return deck;
            }

            List<Card> availableDeck = new List<Card>();
            List<MatchState> previousMatchStates = matchStates.FindAll(x => isPlayed ? x.Status != MatchStatus.NotPlayed : x.CardId != Guid.Empty);

            foreach (Card card in deck)
            {
                if (!previousMatchStates.Any(x => x.CardId == card.SelectionId))
                {
                    availableDeck.Add(card);
                }
            }

            return availableDeck;
        }

        private bool CheckIfCardsAllowed(User user, List<Card> deck, out string message, out string buttonText, out string buttonUrl)
        {
            message = string.Empty;
            buttonText = string.Empty;
            buttonUrl = string.Empty;

            if (_matchStateRepository.IsInMatch(user))
            {
                message = $"You are already in an active Stadium match.";
                buttonText = $"Head to the Stadium!";
                buttonUrl = "/Stadium";

                return false;
            }

            if (deck.Count != Helper.GetMaxDeckSize())
            {
                message = $"You need to have {Helper.GetMaxDeckSize()} cards in your Deck to start a match.";
                buttonText = $"Head to your bag!";
                buttonUrl = "/Items";

                return false;
            }

            if (deck.Any(x => x.Condition == ItemCondition.Unusable))
            {
                message = $"You cannot have any {Resources.DeckCards} in your Deck that have an Unusable condition to start a match.";
                buttonText = $"Head to your bag!";
                buttonUrl = "/Items";

                return false;
            }

            return true;
        }

        private void GiveTowerAccomplishments(User user, int floorLevelRecord)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TowerFloorFive))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TowerFloorFive);
            }
            if (floorLevelRecord >= 10)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TowerFloorTen))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TowerFloorTen);
                }
            }
            if (floorLevelRecord >= 15)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TowerFloorFifteen))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TowerFloorFifteen);
                }
            }
            if (floorLevelRecord >= 25)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TowerFloorTwentyFive))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TowerFloorTwentyFive);
                }
            }
        }

        private void AddSuddenDeathRound(User user)
        {
            List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
            MatchState matchState = matchStates.Last();

            MatchState newMatchState = new MatchState
            {
                Username = user.Username,
                Opponent = matchState.Opponent,
                CardId = Guid.Empty,
                Position = matchState.Position + 1,
                Points = matchState.Points,
                Status = MatchStatus.NotPlayed,
                IsInSelection = true,
                IsCPU = matchState.IsCPU,
                IsCPUChallenge = matchState.IsCPUChallenge,
                IsSpecialCPU = matchState.IsSpecialCPU,
                TourneyMatchId = matchState.TourneyMatchId
            };

            List<MatchState> winningMatchStates = matchStates.FindAll(x => x.Status == MatchStatus.Won).ToList();
            if (winningMatchStates.Count == 0)
            {
                winningMatchStates = matchStates;
            }

            if (newMatchState.IsCPU)
            {
                Random rnd = new Random();
                List<Card> allCards = _itemsRepository.GetAllCards().FindAll(x => winningMatchStates.Any(y => y.CPUCardId == x.Id)).OrderBy(x => x.Amount).ToList();
                List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers().FindAll(x => winningMatchStates.Any(y => y.CPUEnhancerId == x.Id)).ToList();
                newMatchState.CPUCardId = allCards.Last().Id;
                newMatchState.CPUCardTheme = allCards.Last().Theme;
                allEnhancers.RemoveAll(x => x.IsFiller);
                if (allEnhancers.Count > 0)
                {
                    newMatchState.CPUEnhancerId = allEnhancers[rnd.Next(allEnhancers.Count)].Id;
                }
            }

            _matchStateRepository.AddMatchState(user, newMatchState);
        }
    }
}