﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class LottoController : Controller
    {
        private ILoginRepository _loginRepository;
        private ILottoRepository _lottoRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public LottoController(ILoginRepository loginRepository, ILottoRepository lottoRepository, IItemsRepository itemsRepository,
        IPointsRepository pointsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _lottoRepository = lottoRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Lotto" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Egg", "Lotto");
            }

            LottoViewModel lottoViewModel = GenerateModel(user, true);
            lottoViewModel.UpdateViewData(ViewData);
            return View(lottoViewModel);
        }

        public IActionResult Egg()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Lotto/Egg" });
            }

            LottoViewModel lottoViewModel = GenerateModel(user, false);
            lottoViewModel.UpdateViewData(ViewData);
            return View(lottoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Enter(int number)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.Points < 500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (number <= 0 || number > 250)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your number must be between 1 and {Helper.FormatNumber(250)}." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastLottoEntry = (int)(DateTime.UtcNow - settings.LastLottoEntry).TotalHours;
            if (hoursSinceLastLottoEntry < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can only enter the lotto again in {(3 - hoursSinceLastLottoEntry)} hour{(3 - hoursSinceLastLottoEntry == 1 ? "" : "s")}." });
            }

            List<int> pastLottoGuesses = GetPastLottoGuesses(settings);
            if (pastLottoGuesses.Any(x => x == number))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already guessed this number for this lotto." });
            }

            List<Lotto> lottos = _lottoRepository.GetLottos();

            Lotto lotto = lottos.FirstOrDefault();
            if (lotto == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "No lotto exists.", ShouldRefresh = true });
            }

            bool lottoWon = number == lotto.WinningNumber;

            settings.LastLottoEntry = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastLottoEntry), settings.LastLottoEntry.ToString());

            if (lottoWon)
            {
                double tax = GetTaxAmount(user);
                int lottoEarnings = (int)Math.Round(lotto.CurrentPot * (1 - tax));
                if (_pointsRepository.AddPoints(user, lottoEarnings))
                {
                    lotto.WinnerUsername = user.Username;
                    lotto.WonDate = DateTime.UtcNow.AddSeconds(-1);
                    _lottoRepository.UpdateLotto(lotto);

                    _lottoRepository.AddLotto(50000, GetNewNumber());

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WonLotto))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WonLotto);
                    }

                    if (pastLottoGuesses.Count > 0)
                    {
                        settings.PastLottoGuesses = string.Empty;
                        _settingsRepository.SetSetting(user, nameof(settings.PastLottoGuesses), settings.PastLottoGuesses);
                    }

                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"Congratulations! The number {Helper.FormatNumber(number)} was the winning number! You won the lotto and earned {Helper.GetFormattedPoints(lottoEarnings)} after taxes!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
                }
            }

            pastLottoGuesses.Add(number);
            settings.PastLottoGuesses = string.Join("-", pastLottoGuesses.Select(x => x));
            _settingsRepository.SetSetting(user, nameof(settings.PastLottoGuesses), settings.PastLottoGuesses);

            if (_pointsRepository.SubtractPoints(user, 500))
            {
                lotto.CurrentPot += 500;
                _lottoRepository.UpdateLotto(lotto);
            }

            return Json(new Status { Success = true, InfoMessage = $"You didn't win the lotto with the number {Helper.FormatNumber(number)} but the pot increased to {Helper.GetFormattedPoints(lotto.CurrentPot)}!", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> eggCards = _itemsRepository.GetAllItems(ItemCategory.Egg);
            Item eggCard = eggCards.Find(x => x.Id == itemId);
            if (eggCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item no longer exists." });
            }

            if (user.Points < eggCard.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to buy this item." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            int totalHoursSinceLastBuyCount = (int)(DateTime.UtcNow - settings.LastLottoBuyDate).TotalHours;
            if (totalHoursSinceLastBuyCount < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought an item from the Lotto too recently. Try again in {24 - totalHoursSinceLastBuyCount} hour{(24 - totalHoursSinceLastBuyCount == 1 ? "" : "s")}." });
            }

            if (_pointsRepository.SubtractPoints(user, eggCard.Points))
            {
                _itemsRepository.AddItem(user, eggCard);

                settings.LastLottoBuyDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastLottoBuyDate), settings.LastLottoBuyDate.ToString());
            }

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{eggCard.Name}\" was successfully added to your bag!", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpGet]
        public IActionResult GetLottoPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            LottoViewModel lottoViewModel = GenerateModel(user, true);
            return PartialView("_LottoMainPartial", lottoViewModel);
        }

        [HttpGet]
        public IActionResult GetEggPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            LottoViewModel lottoViewModel = GenerateModel(user, false);
            return PartialView("_LottoEggMainPartial", lottoViewModel);
        }

        private LottoViewModel GenerateModel(User user, bool isLotto)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            double hoursSinceLastLottoEntry = (DateTime.UtcNow - settings.LastLottoEntry).TotalHours;

            LottoViewModel lottoViewModel = new LottoViewModel
            {
                Title = "Lotto",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Lotto,
                CanEnterLotto = user.Points >= 500 && hoursSinceLastLottoEntry >= 3,
                TaxPercent = (int)Math.Round(GetTaxAmount(user) * 100)
            };

            if (isLotto)
            {
                List<Lotto> lottos = GetLottos();
                lottoViewModel.CurrentLotto = lottos.First();
                lottoViewModel.Lottos = lottos;
                lottoViewModel.PastGuesses = GetPastLottoGuesses(settings).OrderBy(x => x).ToList();

                if (lottos.Count > 1 && lottos[1].WonDate >= settings.LastLottoEntry && lottoViewModel.PastGuesses.Count > 0)
                {
                    settings.PastLottoGuesses = string.Empty;
                    _settingsRepository.SetSetting(user, nameof(settings.PastLottoGuesses), settings.PastLottoGuesses);
                    lottoViewModel.PastGuesses = new List<int>();
                }
            }
            else
            {
                lottoViewModel.EggCards = _itemsRepository.GetAllItems(ItemCategory.Egg);
                lottoViewModel.CanBuyLottoItem = (DateTime.UtcNow - settings.LastLottoBuyDate).TotalHours >= 24;
            }
            return lottoViewModel;
        }

        private List<Lotto> GetLottos()
        {
            List<Lotto> lottos = _lottoRepository.GetLottos();
            if (lottos == null || lottos.Count == 0)
            {
                _lottoRepository.AddLotto(50000, GetNewNumber());
            }
            else
            {
                return lottos;
            }

            return _lottoRepository.GetLottos();
        }

        private int GetNewNumber()
        {
            Random rnd = new Random();
            return rnd.Next(0, 250) + 1;
        }

        private double GetTaxAmount(User user)
        {
            int userAgeInDays = user.GetAgeInDays();
            if (userAgeInDays < 3)
            {
                return 0.8;
            }
            if (userAgeInDays < 5)
            {
                return 0.7;
            }
            if (userAgeInDays < 10)
            {
                return 0.6;
            }
            if (userAgeInDays < 20)
            {
                return 0.5;
            }
            if (userAgeInDays < 30)
            {
                return 0.35;
            }
            return 0.25;
        }

        private List<int> GetPastLottoGuesses(Settings settings)
        {
            if (string.IsNullOrEmpty(settings.PastLottoGuesses))
            {
                return new List<int>();
            }

            return settings.PastLottoGuesses.Split("-").Select(x => int.Parse(x)).ToList();
        }
    }
}
