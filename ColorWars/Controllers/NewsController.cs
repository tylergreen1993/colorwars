﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class NewsController : Controller
    {
        private ILoginRepository _loginRepository;
        private INewsRepository _newsRepository;
        private IUserRepository _userRepository;
        private IEmailRepository _emailRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private INotificationsRepository _notificationsRepository;
        private IBlockedRepository _blockedRepository;

        public NewsController(ILoginRepository loginRepository, INewsRepository newsRepository, IUserRepository userRepository,
        IEmailRepository emailRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        INotificationsRepository notificationsRepository, IBlockedRepository blockedRepository)
        {
            _loginRepository = loginRepository;
            _newsRepository = newsRepository;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _notificationsRepository = notificationsRepository;
            _blockedRepository = blockedRepository;
        }

        public IActionResult Index(int id, int page, string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"News?id={id}&page={page}" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Horoscope", "News");
            }

            NewsViewModel newsViewModel = GenerateModel(user, id, page, true);
            newsViewModel.UpdateViewData(ViewData);
            return View(newsViewModel);
        }

        public IActionResult Horoscope()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"News/Horoscope" });
            }

            NewsViewModel newsViewModel = GenerateModel(user, 0, 0, false);
            newsViewModel.UpdateViewData(ViewData);
            return View(newsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddArticle(string title, string author, string article)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to add a News article." });
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(author) || string.IsNullOrEmpty(article))
            {
                return Json(new Status { Success = false, ErrorMessage = "All fields are required." });
            }

            User userAuthor = _userRepository.GetUser(author);
            if (userAuthor == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist and cannot be the author of this News article." });
            }

            article = article.Replace(Environment.NewLine, "<br/>");
            if (_newsRepository.AddNews(user, userAuthor, title, article, out News news))
            {
                string domain = Helper.GetDomain();
                string url = $"/News?id={news.Id}";
                Task.Run(() =>
                {
                    if (userAuthor.Username != user.Username)
                    {
                        string subject = "Congratulations! Your News article was published!";
                        string emailTitle = "You made the News!";
                        string body = "Congratulations! Your article was selected and published on our News page! You check it out by clicking the button below.";

                        _emailRepository.SendEmail(userAuthor, subject, emailTitle, body, url, true, domain: domain);

                        _notificationsRepository.AddNotification(userAuthor, "Congratulations! Your News article was published!", NotificationLocation.News, url, domain: domain);
                    }

                    Helper.ExtractUserReply(article, out List<string> userMentions);
                    foreach (string username in userMentions)
                    {
                        if (userAuthor.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                        User userMention = _userRepository.GetUser(username);
                        if (userMention != null)
                        {
                            Settings userMentionSettings = _settingsRepository.GetSettings(userMention, false);
                            if (userMentionSettings.AllowUserMentionNotifications)
                            {
                                _notificationsRepository.AddNotification(userMention, $"@{userAuthor.Username} mentioned you in a News article!", NotificationLocation.News, url, domain: domain);
                            }
                        }
                    }

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(userAuthor, false);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.NewsGuru))
                    {
                        _accomplishmentsRepository.AddAccomplishment(userAuthor, AccomplishmentId.NewsGuru, false, domain: domain);
                    }
                });

                return Json(new Status { Success = true, SuccessMessage = "The News article was successfully added!", ReturnUrl = url });
            }

            return Json(new Status { Success = false, ErrorMessage = "The News article could not be published." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditArticle(int id, string title, string author, string article)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to edit a News article." });
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(author) || string.IsNullOrEmpty(article))
            {
                return Json(new Status { Success = false, ErrorMessage = "All fields are required." });
            }

            News newsArticle = _newsRepository.GetNews().Find(x => x.Id == id);
            if (newsArticle == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This News article doesn't appear to exist.." });
            }

            User userAuthor = _userRepository.GetUser(author);
            if (userAuthor == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist and cannot be the author of this News article." });
            }

            article = article.Replace(Environment.NewLine, "<br/>");

            newsArticle.Title = title;
            newsArticle.Username = userAuthor.Username;
            newsArticle.Content = article;

            _newsRepository.UpdateNews(user, newsArticle);

            return Json(new Status { Success = true, SuccessMessage = "The News article was successfully updated!", ButtonText = "Head to Article!", ButtonUrl = $"/News?id={newsArticle.Id}" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Comment(int id, string comment, Guid parentCommentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(comment))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your comment cannot be empty." });
            }

            if (comment.Length > 750)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your comment must be 750 characters or less." });
            }

            News article = _newsRepository.GetNews().Find(x => x.Id == id);
            if (article == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This News article doesn't exist." });
            }

            NewsComment parentNewsComment = null;
            if (parentCommentId != Guid.Empty)
            {
                List<NewsComment> newsComments = _newsRepository.GetNewsComments(user, article, true);
                parentNewsComment = newsComments.Find(x => x.Id == parentCommentId);
                if (parentNewsComment?.Level >= 6)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You are not allowed to reply to this comment." });
                }
            }

            User parentCommentUser = null;
            if (parentNewsComment != null && user.Username != parentNewsComment.Username)
            {
                parentCommentUser = _userRepository.GetUser(parentNewsComment.Username);
            }

            if (parentCommentUser != null && _blockedRepository.HasBlockedUser(parentCommentUser, user, false))
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to reply to this comment." });
            }

            comment = Regex.Replace(comment, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            if (_newsRepository.AddNewsComment(user, parentNewsComment, article, HttpUtility.HtmlEncode(comment), out NewsComment newsComment))
            {
                string url = $"/News?id={article.Id}#{newsComment.Id}";
                string domain = Helper.GetDomain();

                Task.Run(() =>
                {
                    if (parentCommentUser == null)
                    {
                        User author = _userRepository.GetUser(article.Username);
                        if (author != null && author.Username != user.Username)
                        {
                            _notificationsRepository.AddNotification(author, $"@{user.Username} commented on your News article!", NotificationLocation.News, url, domain: domain);
                        }
                    }
                    else
                    {
                        _notificationsRepository.AddNotification(parentCommentUser, $"@{user.Username} replied to your comment on a News article!", NotificationLocation.News, url, domain: domain);
                    }

                    Helper.ExtractUserReply(comment, out List<string> userMentions);
                    foreach (string username in userMentions)
                    {
                        if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                        if (parentCommentUser != null && parentCommentUser.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                        User userMention = _userRepository.GetUser(username);
                        if (userMention != null)
                        {
                            Settings userMentionSettings = _settingsRepository.GetSettings(userMention, false);
                            if (userMentionSettings.AllowUserMentionNotifications)
                            {
                                if (!_blockedRepository.HasBlockedUser(userMention, user, false))
                                {
                                    _notificationsRepository.AddNotification(userMention, $"@{user.Username} mentioned you in their comment on a News article!", NotificationLocation.News, url, domain: domain);
                                }
                            }
                        }
                    }
                });

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your comment could not be posted." });
        }

        [HttpPost]
        public IActionResult RemoveComment(int newsId, Guid commentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            News article = _newsRepository.GetNews().Find(x => x.Id == newsId);
            if (article == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This News article does not exist." });
            }

            List<NewsComment> newsComments = _newsRepository.GetNewsComments(user, article);
            if (newsComments == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "There were no comments found for this article." });
            }

            NewsComment newsComment = newsComments.Find(x => x.Id == commentId);
            if (newsComment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This comment no longer exists." });
            }

            if (newsComment.Username != user.Username && user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to remove this comment." });
            }

            _newsRepository.DeleteNewsComment(article, newsComment);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult LikeComment(int newsId, Guid commentId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            News article = _newsRepository.GetNews().Find(x => x.Id == newsId);
            if (article == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This News article does not exist." });
            }

            List<NewsComment> newsComments = _newsRepository.GetNewsComments(user, article);
            if (newsComments == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "There were no comments found for this article." });
            }

            NewsComment newsComment = newsComments.Find(x => x.Id == commentId);
            if (newsComment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This comment no longer exists." });
            }

            if (_newsRepository.SetNewsCommentLike(user, newsComment))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    if (newsComment.TotalLikes == 4 && user.Username != newsComment.Username)
                    {
                        User commentUser = _userRepository.GetUser(newsComment.Username);
                        if (commentUser != null)
                        {
                            _notificationsRepository.AddNotification(commentUser, "Your News comment received 5 likes!", NotificationLocation.News, $"/News?id={article.Id}#{newsComment.Id}", string.Empty, domain);
                        }
                    }
                });

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This comment could not be liked." });
        }

        [HttpPost]
        public IActionResult RemoveArticle(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to remove this News article." });
            }

            News article = _newsRepository.GetNews().Find(x => x.Id == id);
            if (article == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This News article no longer exists." });
            }

            _newsRepository.DeleteNews(user, article);

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetNewsArticlePartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            NewsViewModel newsViewModel = GenerateModel(user, id, 0, true);
            return PartialView("_NewsArticlePartial", newsViewModel);
        }

        private NewsViewModel GenerateModel(User user, int id, int page, bool isNews)
        {
            NewsViewModel newsViewModel = new NewsViewModel
            {
                Title = "News",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.News,
                CurrentPage = page
            };

            if (isNews)
            {
                List<News> news = _newsRepository.GetNews();
                if (id > 0)
                {
                    News article = news.Find(x => x.Id == id);
                    if (article != null)
                    {
                        article.Comments = _newsRepository.GetNewsComments(user, article, true);
                        newsViewModel.ShowTitle = false;
                    }
                    newsViewModel.Article = article;
                }
                if (newsViewModel.Article == null)
                {
                    int resultsPerPage = 10;
                    newsViewModel.TotalPages = (int)Math.Ceiling((double)news.Count / resultsPerPage);
                    if (page < 0 || page >= newsViewModel.TotalPages)
                    {
                        page = 0;
                    }
                    newsViewModel.News = news.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                }
            }
            else
            {
                Settings settings = _settingsRepository.GetSettings(user);
                if (DateTime.UtcNow >= settings.HoroscopeExpiryDate)
                {
                    AddNewHoroscope(user, settings);
                }
                newsViewModel.Horoscope = settings.ActiveHoroscope;
                newsViewModel.LuckLevel = settings.LuckLevel;
                newsViewModel.HoroscopeExpiryDate = settings.HoroscopeExpiryDate;
            }

            return newsViewModel;
        }

        private void AddNewHoroscope(User user, Settings settings)
        {
            Random rnd = new Random();

            if (rnd.Next(Math.Max(1000 - settings.LuckLevel * 2, 5)) == 1)
            {
                settings.ActiveHoroscope = HoroscopeType.Good;
            }
            else
            {
                switch (rnd.Next(3))
                {
                    case 1:
                        settings.ActiveHoroscope = HoroscopeType.Bad;
                        break;
                    case 2:
                        settings.ActiveHoroscope = HoroscopeType.Good;
                        break;
                    default:
                        settings.ActiveHoroscope = HoroscopeType.None;
                        break;
                }
            }

            if (settings.HoroscopeExpiryDate == DateTime.MinValue)
            {
                settings.ActiveHoroscope = HoroscopeType.Good;
            }

            settings.HoroscopeExpiryDate = DateTime.UtcNow.AddDays(1);
            _settingsRepository.SetSetting(user, nameof(settings.ActiveHoroscope), settings.ActiveHoroscope.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.HoroscopeExpiryDate), settings.HoroscopeExpiryDate.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HoroscopeRead))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HoroscopeRead);
            }
        }
    }
}
