﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class HighLowController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public HighLowController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "HighLow" });
            }

            HighLowViewModel highLowViewModel = GenerateModel(user);
            highLowViewModel.UpdateViewData(ViewData);
            return View(highLowViewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Start()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HighLowPot > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're already in the middle of a High Low game." });
            }

            if (user.Points < 500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, 500))
            {
                settings.HighLowPot = 500;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowPot), settings.HighLowPot.ToString());

                Random rnd = new Random();
                settings.HighLowNumber = rnd.Next(51) + 25;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowNumber), settings.HighLowNumber.ToString());
                if (settings.HighLowCardNumber > 0)
                {
                    settings.HighLowCardNumber = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.HighLowCardNumber), settings.HighLowCardNumber.ToString());
                }

                return Json(new Status { Success = true, Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not start a game of High Low." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Play(bool isHigher)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HighLowPot == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot start a new game of High Low until you finish the current one." });
            }

            if (settings.HighLowCardNumber > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to do this." });
            }

            string soundUrl = string.Empty;

            Random rnd = new Random();
            settings.HighLowCardNumber = 0;
            while (settings.HighLowCardNumber == 0 || settings.HighLowNumber == settings.HighLowCardNumber)
            {
                settings.HighLowCardNumber = rnd.Next(0, 100) + 1;
            }
            _settingsRepository.SetSetting(user, nameof(settings.HighLowCardNumber), settings.HighLowCardNumber.ToString());

            if ((settings.HighLowCardNumber > settings.HighLowNumber && isHigher) || (settings.HighLowCardNumber < settings.HighLowNumber && !isHigher))
            {
                settings.HighLowPot = (int)(settings.HighLowPot * 1.25) + 500;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowPot), settings.HighLowPot.ToString());

                settings.HighLowStreak++;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowStreak), settings.HighLowStreak.ToString());

                if (settings.HighLowStreak > settings.HighLowTopStreak)
                {
                    settings.HighLowTopStreak = settings.HighLowStreak;
                    _settingsRepository.SetSetting(user, nameof(settings.HighLowTopStreak), settings.HighLowTopStreak.ToString());
                }

                if (settings.HighLowPot >= 8000)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HighLowBigStreak))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HighLowBigStreak);
                    }
                }

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SoundUrl = soundUrl });
            }

            int earnings = settings.HighLowPot;

            settings.HighLowPot = 0;
            _settingsRepository.SetSetting(user, nameof(settings.HighLowPot), settings.HighLowPot.ToString());
            settings.HighLowStreak = 0;
            _settingsRepository.SetSetting(user, nameof(settings.HighLowStreak), settings.HighLowStreak.ToString());

            soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

            return Json(new Status { Success = true, DangerMessage = $"Too bad! The {Resources.DeckCard}'s value was {(isHigher ? "lower" : "higher")}! You forfeited your {Helper.GetFormattedPoints(earnings)} pot earnings!", SoundUrl = soundUrl });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Continue()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HighLowPot == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot start a new game of High Low until you finish the current one." });
            }

            if (settings.HighLowCardNumber == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to do this." });
            }

            Random rnd = new Random();
            settings.HighLowNumber = rnd.Next(51) + 25;
            _settingsRepository.SetSetting(user, nameof(settings.HighLowNumber), settings.HighLowNumber.ToString());
            settings.HighLowCardNumber = 0;
            _settingsRepository.SetSetting(user, nameof(settings.HighLowCardNumber), settings.HighLowCardNumber.ToString());

            return Json(new Status { Success = true });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult Take()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HighLowCardNumber == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to do this." });
            }

            if (settings.HighLowPot == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You have no {Resources.Currency} in your pot." });
            }

            if (_pointsRepository.AddPoints(user, settings.HighLowPot))
            {
                int earnings = settings.HighLowPot;

                settings.HighLowPot = 0;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowPot), settings.HighLowPot.ToString());
                settings.HighLowNumber = 0;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowNumber), settings.HighLowNumber.ToString());
                settings.HighLowCardNumber = 0;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowCardNumber), settings.HighLowCardNumber.ToString());
                settings.HighLowStreak = 0;
                _settingsRepository.SetSetting(user, nameof(settings.HighLowStreak), settings.HighLowStreak.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HighLowPotEarned))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HighLowPotEarned);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took all the {Resources.Currency} from your pot and earned {Helper.GetFormattedPoints(earnings)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You could not take your pot." });
        }

        [HttpGet]
        public IActionResult GetHighLowPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            HighLowViewModel highLowViewModel = GenerateModel(user);
            return PartialView("_HighLowMainPartial", highLowViewModel);
        }

        private HighLowViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            HighLowViewModel highLowViewModel = new HighLowViewModel
            {
                Title = "High Low",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Card = GetCard(settings.HighLowNumber > 0, settings.HighLowCardNumber),
                Pot = settings.HighLowPot,
                Number = settings.HighLowNumber,
                IsInGame = settings.HighLowNumber > 0,
                Streak = settings.HighLowStreak,
                TopStreak = settings.HighLowTopStreak
            };

            return highLowViewModel;
        }

        private Card GetCard(bool isInGame, int cardNumber)
        {
            if (isInGame && cardNumber > 0)
            {
                List<Card> cards = _itemsRepository.GetAllCards();
                Card card = cards.Find(x => x.Amount == cardNumber);
                if (card != null)
                {
                    return card;
                }
            }

            return new Card
            {
                Name = "Flipped Card",
                ImageUrl = "Cards/CardBack.png",
                Description = "Flip it over and see what's underneath!"
            };
        }
    }
}
