﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class WallController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public WallController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Wall" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Home");
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BrokeFourthWall))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BrokeFourthWall);
            }

            WallViewModel wallViewModel = GenerateModel(user);
            wallViewModel.UpdateViewData(ViewData);
            return View(wallViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddPoints()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer break the fourth wall.", ShouldRefresh = true });
            }

            int minutesSinceLastPointsAdd = (int)(DateTime.UtcNow - settings.LastCrystal4thPointsAddDate).TotalMinutes;
            if (minutesSinceLastPointsAdd < 10)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've added {Resources.Currency} to this user's account too recently. Try again in {10 - minutesSinceLastPointsAdd} minute{(10 - minutesSinceLastPointsAdd == 1 ? "" : "s")}." });
            }

            Random rnd = new Random();
            int points = rnd.Next(500000, 2000001);

            if (_pointsRepository.AddPoints(user, points))
            {
                settings.LastCrystal4thPointsAddDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastCrystal4thPointsAddDate), settings.LastCrystal4thPointsAddDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully added {Helper.GetFormattedPoints(points)} to this user's account!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You were unable to add {Resources.Currency} to this user's account." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddStadiumBonus()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer break the fourth wall.", ShouldRefresh = true });
            }

            if (settings.StadiumBonusExpiryDate > DateTime.UtcNow && settings.StadiumBonusPercent > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user already has an active Stadium Bonus." });
            }

            settings.StadiumBonusPercent = 40;
            settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(10);

            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());

            return Json(new Status { Success = true, SuccessMessage = "You successfully added a 40% Stadium Bonus for CPU matches for the next 10 minutes!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCurseProtect()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer break the fourth wall.", ShouldRefresh = true });
            }

            if (settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user already has an active Curse Protect." });
            }

            settings.SorceressStadiumCurseProtectExpiryDate = DateTime.UtcNow.AddDays(7);
            _settingsRepository.SetSetting(user, nameof(settings.SorceressStadiumCurseProtectExpiryDate), settings.SorceressStadiumCurseProtectExpiryDate.ToString());

            return Json(new Status { Success = true, SuccessMessage = "You successfully added a Curse Protect for the next 7 days!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddStealthProtect()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer break the fourth wall.", ShouldRefresh = true });
            }

            if (settings.StealthProtectExpiryDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user already has an active Stealth Protect." });
            }

            settings.StealthProtectExpiryDate = DateTime.UtcNow.AddDays(7);
            _settingsRepository.SetSetting(user, nameof(settings.StealthProtectExpiryDate), settings.StealthProtectExpiryDate.ToString());

            return Json(new Status { Success = true, SuccessMessage = "You successfully added a Stealth Protect for the next 7 days!" });
        }

        [HttpGet]
        public IActionResult GetWallPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.Crystal4thExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer break the fourth wall." });
            }

            WallViewModel wallViewModel = GenerateModel(user);
            return PartialView("_WallMainPartial", wallViewModel);
        }

        private WallViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            WallViewModel wallViewModel = new WallViewModel
            {
                Title = "Fourth Wall",
                User = user,
                CanAddPoints = (DateTime.UtcNow - settings.LastCrystal4thPointsAddDate).TotalMinutes >= 10,
                StadiumBonus = settings.StadiumBonusExpiryDate > DateTime.UtcNow ? settings.StadiumBonusPercent : 0,
                HasStealthProtect = settings.StealthProtectExpiryDate > DateTime.UtcNow,
                HasCurseProtect = settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow,
                ExpiryDate = settings.Crystal4thExpiryDate
            };

            return wallViewModel;
        }
    }
}
