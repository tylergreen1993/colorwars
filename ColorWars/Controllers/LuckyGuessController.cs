﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class LuckyGuessController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ILuckyGuessRepository _luckyGuessRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public LuckyGuessController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ILuckyGuessRepository luckyGuessRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _luckyGuessRepository = luckyGuessRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "LuckyGuess" });
            }

            LuckyGuessViewModel luckyGuessViewModel = GenerateModel(user);
            luckyGuessViewModel.UpdateViewData(ViewData);
            return View(luckyGuessViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LuckyGuessLastPlayDate).TotalHours < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played too recently. Try again in {settings.LuckyGuessLastPlayDate.AddHours(6).GetTimeUntil()}." });
            }

            if (position < 0 || position >= 50)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot choose this position." });
            }

            List<LuckyGuess> luckyGuesses = _luckyGuessRepository.GetLuckyGuesses();
            if (luckyGuesses.Any(x => x.Position == position))
            {
                return Json(new Status { Success = false, ErrorMessage = "This chest has already been opened." });
            }

            string soundUrl = string.Empty;

            LuckyGuess winningGuess = _luckyGuessRepository.GetLuckyGuessTreasures().First();
            bool isWinner = position == winningGuess.Position;
            if (_luckyGuessRepository.AddLuckyGuess(user, position, isWinner))
            {
                settings.LuckyGuessLastPlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LuckyGuessLastPlayDate), settings.LuckyGuessLastPlayDate.ToString());

                if (isWinner)
                {
                    int reward = Math.Max(5000, 25000 - (luckyGuesses.Count * 500));
                    _pointsRepository.AddPoints(user, reward);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LuckyGuessWinner))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LuckyGuessWinner);
                    }

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully guessed the correct chest and earned the prize of {Helper.GetFormattedPoints(reward)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = false, DangerMessage = $"You opened the chest and found nothing! Better luck next time!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This chest could not be opened. Please try again." });
        }

        [HttpGet]
        public IActionResult GetLuckyGuessPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            LuckyGuessViewModel luckyGuessViewModel = GenerateModel(user);
            return PartialView("_LuckyGuessMainPartial", luckyGuessViewModel);
        }

        private LuckyGuessViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            LuckyGuessViewModel luckyGuessViewModel = new LuckyGuessViewModel
            {
                Title = "Lucky Guess",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Chests = GetChests(),
                PastWinners = _luckyGuessRepository.GetLuckyGuessTreasures().FindAll(x => !string.IsNullOrEmpty(x.Username)),
                CanPlay = (DateTime.UtcNow - settings.LuckyGuessLastPlayDate).TotalHours >= 6
            };

            return luckyGuessViewModel;
        }

        private List<LuckyGuess> GetChests()
        {
            List<LuckyGuess> luckyGuesses = _luckyGuessRepository.GetLuckyGuesses();
            List<LuckyGuess> chests = new List<LuckyGuess>();

            for (int i = 0; i < 50; i++)
            {
                LuckyGuess luckyGuess = luckyGuesses.Find(x => x.Position == i);
                if (luckyGuess == null)
                {
                    chests.Add(new LuckyGuess
                    {
                        Position = i
                    });
                }
                else
                {
                    chests.Add(luckyGuess);
                }
            }

            return chests;
        }
    }
}
