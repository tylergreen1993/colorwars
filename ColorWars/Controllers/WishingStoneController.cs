﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using FuzzySharp;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class WishingStoneController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IWishingStoneRepository _wishingStoneRepository;
        private IGroupsRepository _groupsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public WishingStoneController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IWishingStoneRepository wishingStoneRepository,
        IGroupsRepository groupsRepository, IPointsRepository pointsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _wishingStoneRepository = wishingStoneRepository;
            _groupsRepository = groupsRepository;
            _pointsRepository = pointsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "WishingStone" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Quarry", "WishingStone");
            }

            WishingStoneViewModel wishingStoneViewModel = GenerateModel(user, true);
            wishingStoneViewModel.UpdateViewData(ViewData);
            return View(wishingStoneViewModel);
        }

        public IActionResult Quarry()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "WishingStone/Quarry" });
            }

            WishingStoneViewModel wishingStoneViewModel = GenerateModel(user, false);
            wishingStoneViewModel.UpdateViewData(ViewData);
            return View(wishingStoneViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult MakeWish(string wish)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (wish.Length >= 250)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your wish is too long." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastWish = (int)(DateTime.UtcNow - settings.LastWishDate).TotalHours;
            if (hoursSinceLastWish < 2)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've made a wish recently. Try again in {2 - hoursSinceLastWish} hour{(2 - hoursSinceLastWish == 1 ? "" : "s")}." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to make a wish." });
            }

            Item grantedItem = null;
            wish = wish.ToLower().Trim();
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
            int fuzzyMatchConfidence = 92;
            Item item = new Item
            {
                Name = wish
            };
            FuzzySharp.Extractor.ExtractedResult<Item> topItem = Process.ExtractOne(item, items, s => s.Name.ToLower());
            if (topItem.Score >= fuzzyMatchConfidence)
            {
                grantedItem = topItem.Value;
            }

            if (grantedItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Wishing Stone didn't recognize that item. Please make sure it's spelled correctly or try wishing for something else." });
            }
            if (grantedItem.Category == ItemCategory.Custom || grantedItem.Description.Contains("(Premium)"))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot wish for this item." });
            }

            GroupPower power = _groupsRepository.GetGroupPower(user);
            bool isWishingStoneLuck = power == GroupPower.BetterWishingStoneLuck;
            Random rnd = new Random();
            int odds = Math.Max(2, grantedItem.Points / (isWishingStoneLuck ? 1250 : 1000));
            if (grantedItem.IsExclusive)
            {
                odds *= 250;
            }
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds = (int)Math.Floor(odds * 0.9);
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    odds = (int)Math.Ceiling(odds * 1.1);
                }
            }
            bool isGranted = rnd.Next(0, odds) == 1;

            if (isGranted)
            {
                if (grantedItem.Category == ItemCategory.Deck && rnd.Next(2) == 1)
                {
                    List<ItemTheme> themes = new List<ItemTheme> { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    foreach (ItemTheme theme in themes)
                    {
                        if (wish.ToLower().Contains(theme.ToString().ToLower()))
                        {
                            grantedItem.Theme = theme;
                            break;
                        }
                    }
                }
                _itemsRepository.AddItem(user, grantedItem);
            }

            settings.LastWishDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastWishDate), settings.LastWishDate.ToString());

            _wishingStoneRepository.AddWish(user, grantedItem.Name, isGranted);

            AddAccomplishments(user, isGranted);

            if (isGranted)
            {
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                return Json(new Status { Success = true, SuccessMessage = $"Congratulations! Your wish was heard and the item \"{grantedItem.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = true, InfoMessage = $"Your wish was successfully sent out but nothing happened. Better luck next time!" });
        }


        [HttpPost]
        public IActionResult MineQuarryStone(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (position >= 30 || position < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot mine at this location." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int[] positions = GetQuarryStonePositions(settings);

            int minutesSinceLastMine = (int)(DateTime.UtcNow - settings.LastQuarryStoneMineDate).TotalMinutes;
            if (minutesSinceLastMine < 10 && positions.Count(x => x != 0) >= settings.MaxQuarryStonesMined)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've mined the Quarry too much recently. Try again in {10 - minutesSinceLastMine} minute{(10 - minutesSinceLastMine == 1 ? "" : "s")}." });
            }

            if (minutesSinceLastMine >= 10)
            {
                settings.LastQuarryStoneMineDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastQuarryStoneMineDate), settings.LastQuarryStoneMineDate.ToString());
                positions = new int[30];
            }

            if (positions.Length > position && positions[position] != 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already mined at this location." });
            }

            int odds = 3;
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds--;
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    odds++;
                }
            }

            Random rnd = new Random();
            bool successfulMine = rnd.Next(odds) == 1;
            int amount = successfulMine ? (rnd.Next(10) == 1 ? rnd.Next(5) == 1 ? 500 : rnd.Next(50, 100) + 1 : rnd.Next(20, 50) + 1) : -1;

            if (settings.HasDoubleQuarryHaul && successfulMine)
            {
                amount *= 2;
            }

            positions[position] = amount;
            settings.QuarryStonePositions = string.Join("_", positions);
            _settingsRepository.SetSetting(user, nameof(settings.QuarryStonePositions), settings.QuarryStonePositions);

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Mine, settings);

            string successMessage = string.Empty;
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SecretQuarryStonePatternFound))
            {
                int[] secretPattern = _wishingStoneRepository.GetSecretQuarryStonePattern(user, settings);
                if (SecretPatternFound(positions, secretPattern))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SecretQuarryStonePatternFound);

                    settings.MaxQuarryStonesMined = 15;
                    _settingsRepository.SetSetting(user, nameof(settings.MaxQuarryStonesMined), settings.MaxQuarryStonesMined.ToString());

                    successMessage = "You discovered the secret pattern and you can now mine 15 stones! ";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);
                }
            }

            if (settings.HasDoubleQuarryHaul && positions.Count(x => x != 0) >= settings.MaxQuarryStonesMined)
            {
                settings.HasDoubleQuarryHaul = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleQuarryHaul), settings.HasDoubleQuarryHaul.ToString());
            }

            if (successfulMine)
            {
                if (_pointsRepository.AddPoints(user, amount))
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.QuarryStoneMinedSuccess))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.QuarryStoneMinedSuccess);
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"{successMessage}You successfully mined {Helper.GetFormattedPoints(amount)}!", Points = Helper.GetFormattedPoints(user.Points), ScrollToTop = !string.IsNullOrEmpty(successMessage), SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = true, SuccessMessage = successMessage, ScrollToTop = !string.IsNullOrEmpty(successMessage), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetWishingStonePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            WishingStoneViewModel wishingStoneViewModel = GenerateModel(user, true);
            return PartialView("_WishingStoneMainPartial", wishingStoneViewModel);
        }

        [HttpGet]
        public IActionResult GetWishingStoneQuarryPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            WishingStoneViewModel wishingStoneViewModel = GenerateModel(user, false);
            return PartialView("_WishingStoneQuarryPartial", wishingStoneViewModel);
        }

        private WishingStoneViewModel GenerateModel(User user, bool isWishingStone)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            GroupPower power = _groupsRepository.GetGroupPower(user);
            WishingStoneViewModel wishingStoneViewModel = new WishingStoneViewModel
            {
                Title = "Wishing Stone",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.WishingStone,
                HasBetterLuck = power == GroupPower.BetterWishingStoneLuck
            };

            if (isWishingStone)
            {
                wishingStoneViewModel.Wishes = _wishingStoneRepository.GetWishes();
                wishingStoneViewModel.CanMakeWish = (DateTime.UtcNow - settings.LastWishDate).TotalHours >= 2;
            }
            else
            {
                int minutesSinceLastMine = (int)(DateTime.UtcNow - settings.LastQuarryStoneMineDate).TotalMinutes;
                if (minutesSinceLastMine >= 10)
                {
                    wishingStoneViewModel.QuarryStones = GenerateQuarryStones(new int[30]);
                    wishingStoneViewModel.CanMine = true;
                    wishingStoneViewModel.QuarryStonesLeft = settings.MaxQuarryStonesMined;
                }
                else
                {
                    int[] positions = GetQuarryStonePositions(settings);
                    int quarryStonesMined = positions.Count(x => x != 0);
                    wishingStoneViewModel.CanMine = positions.Count(x => x != 0) < settings.MaxQuarryStonesMined;
                    wishingStoneViewModel.QuarryStones = GenerateQuarryStones(string.IsNullOrEmpty(settings.QuarryStonePositions) ? new int[30] : positions);
                    wishingStoneViewModel.QuarryStonesLeft = settings.MaxQuarryStonesMined - quarryStonesMined;
                    wishingStoneViewModel.TotalEarned = wishingStoneViewModel.QuarryStones.FindAll(x => x.MineAmount > 0).Sum(x => x.MineAmount);
                }
                wishingStoneViewModel.MaxQuarryStonesCanMine = settings.MaxQuarryStonesMined;
                wishingStoneViewModel.HasBetterLuck = settings.HasDoubleQuarryHaul;
            }

            return wishingStoneViewModel;
        }

        private void AddAccomplishments(User user, bool isGranted)
        {
            if (isGranted)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WishGranted))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WishGranted);
                }
            }
        }

        private List<QuarryStone> GenerateQuarryStones(int[] positions)
        {
            List<QuarryStone> quarryStones = new List<QuarryStone>();

            List<QuarryStoneType> quarryStoneTypes = Enum.GetValues(typeof(QuarryStoneType)).Cast<QuarryStoneType>().ToList();

            Random rnd = new Random();
            for (int i = 0; i < 30; i++)
            {
                quarryStones.Add(new QuarryStone
                {
                    Position = i,
                    Type = quarryStoneTypes[rnd.Next(quarryStoneTypes.Count)],
                    IsRevealed = positions[i] != 0,
                    MineAmount = positions[i]
                });
            }

            return quarryStones;
        }

        private int[] GetQuarryStonePositions(Settings settings)
        {
            return string.IsNullOrEmpty(settings.QuarryStonePositions) ? new int[30] : settings.QuarryStonePositions.Split('_').Select(x => int.Parse(x)).ToArray();
        }

        private bool SecretPatternFound(int[] positions, int[] secretPattern)
        {
            foreach (int i in secretPattern)
            {
                if (positions[i - 1] == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
