﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class DisposerController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IDisposerRepository _disposerRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public DisposerController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IDisposerRepository disposerRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _disposerRepository = disposerRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Disposer" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Records", "Disposer");
            }

            DisposerViewModel disposerViewModel = GenerateModel(user, true);
            disposerViewModel.UpdateViewData(ViewData);
            return View(disposerViewModel);
        }

        public IActionResult Records()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Disposer/Records" });
            }

            DisposerViewModel disposerViewModel = GenerateModel(user, false);
            disposerViewModel.UpdateViewData(ViewData);
            return View(disposerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Dispose(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer in your bag." });
            }

            string infoMessage = string.Empty;

            _disposerRepository.DisposeItem(user, item, true, out bool recordQualifier);
            if (recordQualifier)
            {
                infoMessage = $"You qualified as one of the top disposers of the past month!";
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Shred, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully disposed of the item \"{item.Name}\" from your bag!", InfoMessage = infoMessage, SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RedeemTrainingToken()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int trainingTokens = settings.DisposerTrainingTokenProgress / Helper.GetMaxDisposerTrainingTokenProgess();
            if (trainingTokens == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not able to redeem any Training Tokens yet." });
            }

            settings.TrainingTokens += trainingTokens;
            settings.DisposerTrainingTokenProgress -= Helper.GetMaxDisposerTrainingTokenProgess() * trainingTokens;
            _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.DisposerTrainingTokenProgress), settings.DisposerTrainingTokenProgress.ToString());


            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RedeemDisposerTrainingToken))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RedeemDisposerTrainingToken);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully earned {Helper.FormatNumber(trainingTokens)} Training Token{(trainingTokens == 1 ? "" : "s")} to use at the Training Center!", SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetDisposerPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            DisposerViewModel disposerViewModel = GenerateModel(user, true);
            return PartialView("_DisposerMainPartial", disposerViewModel);
        }

        private DisposerViewModel GenerateModel(User user, bool isDisposer)
        {
            DisposerViewModel disposerViewModel = new DisposerViewModel
            {
                Title = "Disposer",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Disposer
            };

            if (isDisposer)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                disposerViewModel.Items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None).OrderByDescending(x => x.Condition).ToList();
                disposerViewModel.ShowDisposerConfirmation = settings.ShowDisposerConfirmation;
                disposerViewModel.DisposedPoints = settings.DisposerTrainingTokenProgress;
            }
            else
            {
                disposerViewModel.DisposerRecords = _disposerRepository.GetBiggestDisposerRecords(30).Take(50).ToList();
            }

            return disposerViewModel;
        }
    }
}
