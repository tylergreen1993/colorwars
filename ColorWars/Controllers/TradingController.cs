﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TradingController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ITradeRepository _tradeRepository;
        private IUserRepository _userRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public TradingController(ILoginRepository loginRepository, IItemsRepository itemsRepository, ITradeRepository tradeRepository,
        IUserRepository userRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IEmailRepository emailRepository, INotificationsRepository notificationsRepository, IAdminHistoryRepository adminHistoryRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _tradeRepository = tradeRepository;
            _userRepository = userRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _notificationRepository = notificationsRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public IActionResult Index(int id, string query, int page, bool matchExactSearch, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Trading{(id == 0 ? "" : $"?id={id}")}" });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            if (id == 0)
            {
                int resultsPerPage = 15;
                List<Trade> trades = _tradeRepository.GetAllTrades(query, matchExactSearch, category, condition);
                tradingViewModel.TotalPages = (int)Math.Ceiling((double)trades.Count / resultsPerPage);
                if (page < 0 || page >= tradingViewModel.TotalPages)
                {
                    page = 0;
                }
                tradingViewModel.Trades = trades.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                tradingViewModel.CurrentPage = page;
                tradingViewModel.SearchQuery = query;
                tradingViewModel.MatchExactSearch = matchExactSearch;
                tradingViewModel.SearchCategory = category;
                tradingViewModel.SearchCondition = condition;
            }
            else
            {
                Trade trade = _tradeRepository.GetTrade(id);
                if (trade == null)
                {
                    return RedirectToAction("Index", "Trading");
                }
                tradingViewModel.CurrentTrade = trade;
                tradingViewModel.Items = _itemsRepository.GetItems(user, true);
            }
            tradingViewModel.UpdateViewData(ViewData);
            return View(tradingViewModel);
        }

        public IActionResult Create()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Trading/Create" });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            tradingViewModel.Items = _itemsRepository.GetItems(user, true);
            tradingViewModel.UpdateViewData(ViewData);
            return View(tradingViewModel);
        }

        public IActionResult Postings(int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Trading/Postings" });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            int resultsPerPage = 15;
            List<Trade> trades = _tradeRepository.GetTrades(user);
            tradingViewModel.TotalPages = (int)Math.Ceiling((double)trades.Count / resultsPerPage);
            if (page < 0 || page >= tradingViewModel.TotalPages)
            {
                page = 0;
            }
            tradingViewModel.Trades = trades.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
            tradingViewModel.CurrentPage = page;
            tradingViewModel.UpdateViewData(ViewData);
            return View(tradingViewModel);
        }

        public IActionResult Offers(int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Trading/Postings" });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            int resultsPerPage = 15;
            List<Trade> trades = _tradeRepository.GetOffers(user);
            tradingViewModel.TotalPages = (int)Math.Ceiling((double)trades.Count / resultsPerPage);
            if (page < 0 || page >= tradingViewModel.TotalPages)
            {
                page = 0;
            }
            tradingViewModel.Trades = trades.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
            tradingViewModel.CurrentPage = page;
            tradingViewModel.UpdateViewData(ViewData);
            return View(tradingViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitTradePosting(Guid item1, Guid item2, Guid item3, string request)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (!string.IsNullOrEmpty(request) && request.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message is too long." });
            }

            List<Trade> userTrades = _tradeRepository.GetTrades(user, true);
            if (userTrades.Count > 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot create more than 10 active trade postings at the same time." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTen))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot create a trade posting unless you've won against a Level 10 CPU at the Stadium." });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item userItem1 = null;
            Item userItem2 = null;
            Item userItem3 = null;

            List<Item> validItems = new List<Item>();
            if (item1 != Guid.Empty)
            {
                userItem1 = items.Find(x => x.SelectionId == item1);
                if (userItem1 != null)
                {
                    validItems.Add(userItem1);
                }
            }
            if (item2 != Guid.Empty)
            {
                userItem2 = items.Find(x => x.SelectionId == item2);
                if (userItem2 != null)
                {
                    validItems.Add(userItem2);
                }
            }
            if (item3 != Guid.Empty)
            {
                userItem3 = items.Find(x => x.SelectionId == item3);
                if (userItem3 != null)
                {
                    validItems.Add(userItem3);
                }
            }

            if (validItems.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose at least one item to put up for trade." });
            }

            if (!string.IsNullOrEmpty(request))
            {
                request = Regex.Replace(request, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            }
            if (_tradeRepository.AddTrade(user, userItem1, userItem2, userItem3, request, out Trade trade))
            {
                foreach (Item item in validItems)
                {
                    _itemsRepository.RemoveItem(user, item);
                }
                return Json(new Status { Success = true, Id = trade.Id.ToString() });
            }

            return Json(new Status { Success = false, SuccessMessage = $"Your trade posting could not be created." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddOffer(int id, Guid item1, Guid item2, Guid item3)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Trade trade = _tradeRepository.GetTrade(id);
            if (trade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting no longer exists.", ShouldRefresh = true });
            }

            if (trade.Username == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot add an offer to your own posting." });
            }

            if (trade.IsAccepted)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting is already completed and accepted an offer." });
            }

            if (trade.Offers.Exists(x => x.Username == user.Username))
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already made an offer on this posting. Please remove it if you'd like to make another offer." });
            }

            int maxTradeOffers = 15;
            if (trade.Offers.Count >= maxTradeOffers)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This posting has reached a maximum of {maxTradeOffers} trade offers and cannot accept more." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTen))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot add an offer unless you've won against a Level 10 CPU at the Stadium." });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item userItem1 = null;
            Item userItem2 = null;
            Item userItem3 = null;

            List<Item> validItems = new List<Item>();
            if (item1 != Guid.Empty)
            {
                userItem1 = items.Find(x => x.SelectionId == item1);
                if (userItem1 != null)
                {
                    validItems.Add(userItem1);
                }
            }
            if (item2 != Guid.Empty)
            {
                userItem2 = items.Find(x => x.SelectionId == item2);
                if (userItem2 != null)
                {
                    validItems.Add(userItem2);
                }
            }
            if (item3 != Guid.Empty)
            {
                userItem3 = items.Find(x => x.SelectionId == item3);
                if (userItem3 != null)
                {
                    validItems.Add(userItem3);
                }
            }

            if (validItems.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose at least one item to put up as an offer." });
            }

            if (_tradeRepository.AddTradeOffer(user, trade, userItem1, userItem2, userItem3))
            {
                foreach (Item item in validItems)
                {
                    _itemsRepository.RemoveItem(user, item);
                }
            }

            User tradeUser = _userRepository.GetUser(trade.Username);
            if (tradeUser != null)
            {
                string url = $"/Trading?id={trade.Id}#{user.Username}";
                _notificationRepository.AddNotification(tradeUser, $"@{user.Username} added an offer to your trade posting!", NotificationLocation.Trade, url);
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully added an offer to the trade posting!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AcceptTradeOffer(int tradeId, Guid offerId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Trade trade = _tradeRepository.GetTrade(tradeId);
            if (trade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting no longer exists.", ShouldRefresh = true });
            }

            if (trade.Username != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to accept an offer for this posting." });
            }

            TradeOffer tradeOffer = trade.Offers.Find(x => x.Id == offerId);
            if (tradeOffer == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This offer no longer exists.", ShouldRefresh = true });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            Settings settings = _settingsRepository.GetSettings(user);
            if (tradeOffer.Items.Count + userItems.Count > settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot accept this trade offer as your bag would be full." });
            }

            if (_tradeRepository.CompleteTrade(trade, tradeOffer))
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TradeAccepted))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TradeAccepted);
                }

                User tradeUser = _userRepository.GetUser(tradeOffer.Username);
                if (tradeUser != null)
                {
                    accomplishments = _accomplishmentsRepository.GetAccomplishments(tradeUser, false);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TradeOfferAccepted))
                    {
                        _accomplishmentsRepository.AddAccomplishment(tradeUser, AccomplishmentId.TradeOfferAccepted, false);
                    }
                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        SendTradeOfferAccepted(tradeUser, trade, domain);
                    });
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully accepted the trade offer and completed your posting!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This offer could not be accepted." });
        }

        [HttpPost]
        public IActionResult EditRequest(int id, string request)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Trade trade = _tradeRepository.GetTrade(id);
            if (trade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting no longer exists.", ShouldRefresh = true });
            }

            if (trade.Username != user.Username && user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to edit this posting." });
            }

            if (trade.IsAccepted)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to edit a posting that has already accepted an offer." });
            }

            if (string.IsNullOrEmpty(request))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your new trade request cannot be empty." });
            }

            if (request.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message is too long." });
            }

            string oldRequest = trade.Request;
            request = Regex.Replace(request, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            trade.Request = request;
            if (_tradeRepository.UpdateTrade(trade))
            {
                if (trade.Username != user.Username)
                {
                    User tradeUser = _userRepository.GetUser(trade.Username);
                    if (tradeUser != null)
                    {
                        _adminHistoryRepository.AddAdminHistory(tradeUser, user, $"Updated Trade Id: {trade.Id} with new request: \"{trade.Request}\" from old request: \"{oldRequest}\".");
                    }
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully updated the trade request!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The trade request could not be updated." });
        }

        [HttpPost]
        public IActionResult RemoveTrade(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Trade trade = _tradeRepository.GetTrade(id);
            if (trade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting no longer exists.", ShouldRefresh = true });
            }

            if (trade.Username != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to remove this posting." });
            }

            if (trade.IsAccepted)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to remove a posting that has already accepted an offer." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            Settings settings = _settingsRepository.GetSettings(user);
            if (trade.Items.Count + userItems.Count > settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot remove this posting as your bag would be full." });
            }

            if (_tradeRepository.DeleteTrade(trade))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This posting could not be removed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveTradeOffer(int tradeId, Guid offerId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Trade trade = _tradeRepository.GetTrade(tradeId);
            if (trade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This posting no longer exists.", ShouldRefresh = true });
            }

            TradeOffer tradeOffer = trade.Offers.Find(x => x.Id == offerId);
            if (tradeOffer == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This trade offer no longer exists.", ShouldRefresh = true });
            }

            if (tradeOffer.Username != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to remove this trade offer." });
            }

            if (trade.IsAccepted)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not allowed to remove a trade offer if an offer has already been accepted." });
            }

            if (_tradeRepository.DeleteTradeOffer(tradeOffer))
            {
                return Json(new Status { Success = true, SuccessMessage = "Your trade offer was successfully removed!" });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            Settings settings = _settingsRepository.GetSettings(user);
            if (tradeOffer.Items.Count + userItems.Count > settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot remove this trade offer as your bag would be full." });
            }

            return Json(new Status { Success = false, ErrorMessage = "This trade offer could not be removed." });
        }

        [HttpGet]
        public IActionResult GetTradingOfferPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            tradingViewModel.CurrentTrade = _tradeRepository.GetTrade(id);
            tradingViewModel.Items = _itemsRepository.GetItems(user, true);
            return PartialView("_TradingIdPartial", tradingViewModel);
        }

        [HttpGet]
        public IActionResult GetTradingCreatePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TradingViewModel tradingViewModel = GenerateModel(user);
            tradingViewModel.Items = _itemsRepository.GetItems(user, true);
            return PartialView("_TradingCreatePartial", tradingViewModel);
        }

        private TradingViewModel GenerateModel(User user)
        {
            TradingViewModel tradingViewModel = new TradingViewModel
            {
                Title = "Trading Square",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.TradingSquare,
                CanTrade = _accomplishmentsRepository.GetAccomplishments(user).Any(x => x.Id == AccomplishmentId.CPULevelTen)
            };

            return tradingViewModel;
        }

        private void SendTradeOfferAccepted(User user, Trade trade, string domain)
        {
            string subject = "Your trade offer was accepted!";
            string title = "Offer Accepted";
            string body = $"Your trade offer was accepted and the item{(trade.Items.Count == 1 ? " has" : "s have")} been added to your bag! Click the button below to see the posting.";
            string url = $"/Trading?id={trade.Id}#{user.Username}";

            _emailRepository.SendEmail(user, subject, title, body, url, domain: domain);

            _notificationRepository.AddNotification(user, "Your trade offer was accepted!", NotificationLocation.Trade, url, domain: domain);
        }
    }
}
