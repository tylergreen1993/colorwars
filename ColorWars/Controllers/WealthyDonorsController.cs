﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class WealthyDonorsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IWealthyDonorsRepository _wealthyDonorsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private ISoundsRepository _soundsRepository;

        public WealthyDonorsController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IWealthyDonorsRepository wealthyDonorsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IAdminSettingsRepository adminSettingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _wealthyDonorsRepository = wealthyDonorsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "WealthyDonors" });
            }

            WealthyDonorsViewModel wealthyDonorsViewModel = GenerateModel(user);
            wealthyDonorsViewModel.UpdateViewData(ViewData);
            return View(wealthyDonorsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Take()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastWealthyDonorsTakeDate).TotalHours;
            if (hoursSinceLastGame < 8)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've taken from the pot too recently. Try again in {8 - hoursSinceLastGame} hour{(8 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            AdminSettings adminSettings = _adminSettingsRepository.GetSettings(false);
            List<WealthyDonor> wealthyDonors = _wealthyDonorsRepository.GetWealthyDonors();
            int currentPot = Math.Max(0, wealthyDonors.Sum(x => x.Amount) - adminSettings.WealthyDonorsPotTaken);

            int amountToTake = Math.Min(1500, currentPot / 20);
            if (amountToTake <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"There's no {Resources.Currency} in the pot to take." });
            }

            if (_pointsRepository.AddPoints(user, amountToTake))
            {
                adminSettings.WealthyDonorsPotTaken += amountToTake;
                _adminSettingsRepository.SetSetting(nameof(adminSettings.WealthyDonorsPotTaken), adminSettings.WealthyDonorsPotTaken.ToString());

                settings.LastWealthyDonorsTakeDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastWealthyDonorsTakeDate), settings.LastWealthyDonorsTakeDate.ToString());

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took from the pot and earned {Helper.GetFormattedPoints(amountToTake)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You could not take from the pot at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Donate(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must donate at least {Helper.GetFormattedPoints(1)}." });
            }

            if (amount > user.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to donate this amount." });
            }

            if (_pointsRepository.SubtractPoints(user, amount))
            {
                _wealthyDonorsRepository.AddWealthyDonor(user, amount);

                int totalAmount = amount;
                WealthyDonor wealthyDonor = _wealthyDonorsRepository.GetWealthyDonors().Find(x => x.Username == user.Username);
                if (wealthyDonor != null)
                {
                    totalAmount = wealthyDonor.Amount;
                }

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WealthyDonorDonation))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WealthyDonorDonation);
                }
                if (totalAmount >= 50000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WealthyDonorBigDonation))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WealthyDonorBigDonation);
                    }
                }
                if (totalAmount >= 250000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WealthyDonorExtraBigDonation))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WealthyDonorExtraBigDonation);
                    }
                }
                if (totalAmount >= 1000000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WealthyDonorMillionaireDonation))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WealthyDonorMillionaireDonation);
                    }
                }

                Settings settings = _settingsRepository.GetSettings(user);
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully donated {Helper.GetFormattedPoints(amount)} to the pot!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You could not donate any {Resources.Currency} at this time." });
        }

        [HttpGet]
        public IActionResult GetWealthyDonorsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            WealthyDonorsViewModel wealthyDonorsViewModel = GenerateModel(user);
            return PartialView("_WealthyDonorsMainPartial", wealthyDonorsViewModel);
        }

        private WealthyDonorsViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            AdminSettings adminSettings = _adminSettingsRepository.GetSettings(false);
            List<WealthyDonor> wealthyDonors = _wealthyDonorsRepository.GetWealthyDonors();
            WealthyDonorsViewModel wealthyDonorsViewModel = new WealthyDonorsViewModel
            {
                Title = "Wealthy Donors",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                TopDonors = _wealthyDonorsRepository.GetWealthyDonors().Take(50).ToList(),
                CurrentPot = Math.Max(0, wealthyDonors.Sum(x => x.Amount) - adminSettings.WealthyDonorsPotTaken),
                CanTake = (DateTime.UtcNow - settings.LastWealthyDonorsTakeDate).TotalHours >= 8
            };

            return wealthyDonorsViewModel;
        }
    }
}
