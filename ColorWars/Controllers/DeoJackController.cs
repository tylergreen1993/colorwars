﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class DeoJackController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public DeoJackController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IItemsRepository itemsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "DeoJack" });
            }

            DeoJackViewModel deoJackViewModel = GenerateModel(user);
            deoJackViewModel.UpdateViewData(ViewData);
            return View(deoJackViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StartGame(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeoJackPoints > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must finish your current game before you start a new one." });
            }

            if (amount <= 0 || amount > 25000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your amount to put down must be between 1 and {Helper.GetFormattedPoints(25000)}." });
            }

            if (user.Points < amount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, amount))
            {
                settings.DeoJackPoints = amount;
                _settingsRepository.SetSetting(user, nameof(settings.DeoJackPoints), settings.DeoJackPoints.ToString());

                Random rnd = new Random();
                List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Amount <= 11);
                List<Card> dealerCards = new List<Card>();
                List<Card> userCards = new List<Card>();

                dealerCards.Add(cards[rnd.Next(cards.Count)]);

                userCards.Add(cards[rnd.Next(cards.Count)]);
                if (userCards.First().Amount == 11)
                {
                    cards = cards.FindAll(x => x.Amount <= 10);
                }
                userCards.Add(cards[rnd.Next(cards.Count)]);

                settings.DeoJackCards = string.Join("|", userCards.Select(x => x.Amount));
                _settingsRepository.SetSetting(user, nameof(settings.DeoJackCards), settings.DeoJackCards);

                settings.DeoJackDealerCards = string.Join("|", dealerCards.Select(x => x.Amount));
                _settingsRepository.SetSetting(user, nameof(settings.DeoJackDealerCards), settings.DeoJackDealerCards);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You weren't able to start a new game. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Hit()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeoJackPoints == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currently in a game." });
            }

            List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Amount <= 11);
            List<Card> userCards = GetCards(cards, settings.DeoJackCards);
            if (userCards.Count == 0 || userCards.Sum(x => x.Amount) >= 21)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot hit at this point." });
            }

            Random rnd = new Random();
            userCards.Add(cards[rnd.Next(cards.Count)]);

            settings.DeoJackCards = string.Join("|", userCards.Select(x => x.Amount));
            _settingsRepository.SetSetting(user, nameof(settings.DeoJackCards), settings.DeoJackCards);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Stand()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeoJackPoints == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currently in a game." });
            }

            List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Amount <= 11);
            List<Card> userCards = GetCards(cards, settings.DeoJackCards);
            if (userCards.Count == 0 || userCards.Sum(x => x.Amount) >= 21)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot stand at this point." });
            }

            List<Card> dealerCards = GetCards(cards, settings.DeoJackDealerCards);
            if (dealerCards.Count > 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot stand at this point." });
            }

            Random rnd = new Random();
            dealerCards.Add(cards[rnd.Next(cards.Count)]);
            while (dealerCards.Sum(x => x.Amount) < userCards.Sum(x => x.Amount) || (dealerCards.Sum(x => x.Amount) == userCards.Sum(x => x.Amount) && dealerCards.Sum(x => x.Amount) < 16))
            {
                dealerCards.Add(cards[rnd.Next(cards.Count)]);
            }

            settings.DeoJackDealerCards = string.Join("|", dealerCards.Select(x => x.Amount));
            _settingsRepository.SetSetting(user, nameof(settings.DeoJackDealerCards), settings.DeoJackDealerCards);

            if (dealerCards.Sum(x => x.Amount) == userCards.Sum(x => x.Amount))
            {
                _pointsRepository.AddPoints(user, settings.DeoJackPoints);
            }

            return Json(new Status { Success = true, Points = user.GetFormattedPoints() });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EndGame()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeoJackPoints == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currently in a game." });
            }

            List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Amount <= 11);
            List<Card> userCards = GetCards(cards, settings.DeoJackCards);
            List<Card> dealerCards = GetCards(cards, settings.DeoJackDealerCards);

            int userTotal = userCards.Sum(x => x.Amount);
            int dealerTotal = dealerCards.Sum(x => x.Amount);

            if (userCards.Count == 0 || (dealerCards.Count <= 1 && userTotal < 21))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot end the game at this point." });
            }

            string soundUrl = string.Empty;

            if (userTotal == 21 || dealerTotal > 21 || (userTotal < 21 && userTotal > dealerTotal))
            {
                _pointsRepository.AddPoints(user, settings.DeoJackPoints * 2);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DeoJackWon))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DeoJackWon);
                }
            }

            settings.DeoJackPoints = 0;
            _settingsRepository.SetSetting(user, nameof(settings.DeoJackPoints), settings.DeoJackPoints.ToString());
            settings.DeoJackCards = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.DeoJackCards), settings.DeoJackCards);
            settings.DeoJackDealerCards = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.DeoJackDealerCards), settings.DeoJackDealerCards);

            return Json(new Status { Success = true, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetDeoJackPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            DeoJackViewModel deoJackViewModel = GenerateModel(user);
            return PartialView("_DeoJackMainPartial", deoJackViewModel);
        }

        private DeoJackViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            DeoJackViewModel deoJackViewModel = new DeoJackViewModel
            {
                Title = "DeoJack",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Points = settings.DeoJackPoints
            };

            if (settings.DeoJackPoints > 0)
            {
                List<Card> cards = _itemsRepository.GetAllCards();
                deoJackViewModel.Cards = GetCards(cards, settings.DeoJackCards).FindAll(x => x.Amount <= 11);
                deoJackViewModel.DealerCards = GetCards(cards, settings.DeoJackDealerCards);
                deoJackViewModel.Total = deoJackViewModel.Cards.Sum(x => x.Amount);
                deoJackViewModel.DealerTotal = deoJackViewModel.DealerCards.Sum(x => x.Amount);
                if (deoJackViewModel.DealerCards.Count > 1 || deoJackViewModel.Total >= 21)
                {
                    if (deoJackViewModel.Total == 21 || deoJackViewModel.DealerTotal > 21 || (deoJackViewModel.Total < 21 && deoJackViewModel.Total > deoJackViewModel.DealerTotal))
                    {
                        deoJackViewModel.GameState = DeoJackGameState.Won;
                    }
                    else if (deoJackViewModel.Total > 21 || deoJackViewModel.Total < deoJackViewModel.DealerTotal)
                    {
                        deoJackViewModel.GameState = DeoJackGameState.Lost;
                    }
                    else if (deoJackViewModel.Total == deoJackViewModel.DealerTotal)
                    {
                        deoJackViewModel.GameState = DeoJackGameState.Draw;
                    }
                }
            }

            return deoJackViewModel;
        }

        private List<Card> GetCards(List<Card> cards, string cardString)
        {
            List<int> cardsAmount = cardString.Split("|").Select(x => int.Parse(x)).ToList();
            List<Card> displayedCards = new List<Card>();
            foreach (int amount in cardsAmount)
            {
                displayedCards.Add(cards.Find(x => x.Amount == amount));
            }
            return displayedCards;
        }
    }
}
