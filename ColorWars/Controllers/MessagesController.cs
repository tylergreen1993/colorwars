﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using ColorWars.Classes;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ColorWars.Controllers
{
    public class MessagesController : Controller
    {
        private ILoginRepository _loginRepository;
        private IMessagesRepository _messagesRepository;
        private IUserRepository _userRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IBlockedRepository _blockedRepository;
        private IItemsRepository _itemsRepository;
        private IChecklistRepository _checklistRepository;
        private IClubRepository _clubRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public MessagesController(ILoginRepository loginRepository, IMessagesRepository messagesRepository, IUserRepository userRepository,
        IBlockedRepository blockedRepository, IItemsRepository itemsRepository, IChecklistRepository checklistRepository, IClubRepository clubRepository,
        IPointsRepository pointsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, IPromoCodesRepository promoCodesRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
            _messagesRepository = messagesRepository;
            _promoCodesRepository = promoCodesRepository;
            _blockedRepository = blockedRepository;
            _itemsRepository = itemsRepository;
            _checklistRepository = checklistRepository;
            _clubRepository = clubRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Messages{(string.IsNullOrEmpty(username) ? "" : $"?username={username}")}" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            MessagesViewModel messagesViewModel = GenerateModel(user, username, false);
            messagesViewModel.UpdateViewData(ViewData);
            return View(messagesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Message(string recipient, string message)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(message))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message can't be empty." });
            }

            if (message.Length > 1000)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message is too long." });
            }

            User correspondent = _userRepository.GetUser(recipient);
            if (correspondent == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer seems to exist." });
            }

            if (_blockedRepository.HasBlockedUser(correspondent, user, false))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot message this user." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            if (Helper.IsCourier(correspondent))
            {
                if (message.Contains($"Open Sesame", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HiddenTreasure))
                    {
                        if (user.GetAgeInHours() >= 1 && DateTime.UtcNow.Hour == user.CreatedDate.Hour)
                        {
                            if (_pointsRepository.AddPoints(user, 10000))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HiddenTreasure);
                                return Json(new Status { Success = true, SuccessMessage = $"You successfully wrote the secret message and earned {Helper.GetFormattedPoints(10000)}!", Points = user.GetFormattedPoints() });
                            }
                        }
                        return Json(new Status { Success = false, ErrorMessage = "You cannot enter that secret message at this time. If only you knew when your account was created.", Points = user.GetFormattedPoints() });
                    }
                }
                return Json(new Status { Success = false, ErrorMessage = $"{correspondent.Username} cannot receive any messages. If you have any questions, please post them at the Attention Hall.", ButtonText = "Head to the Attention Hall!", ButtonUrl = "/AttentionHall" });
            }

            if (user.Role < UserRole.Helper)
            {
                Settings settings = _settingsRepository.GetSettings(correspondent, false);
                if (settings.DisableMessages)
                {
                    List<Message> messages = _messagesRepository.GetMessagesBetweenUsernames(user, correspondent, false);
                    if (!messages.Exists(x => x.Sender == correspondent.Username))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"{correspondent.Username} has disabled messaging." });
                    }
                }
            }

            string strippedMessage = Regex.Replace(message, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            _messagesRepository.AddMessage(user, correspondent, strippedMessage);

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstMessage))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstMessage);
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GoToConversation(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(username))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a username to message." });
            }

            if (username.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot start a conversation with yourself." });
            }

            User correspondent = _userRepository.GetUser(username);
            if (correspondent == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user doesn't exist." });
            }

            if (_blockedRepository.HasBlockedUser(correspondent, user, false))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot message this user." });
            }

            if (user.Role < UserRole.Helper)
            {
                Settings settings = _settingsRepository.GetSettings(correspondent, false);
                if (settings.DisableMessages)
                {
                    List<Message> messages = _messagesRepository.GetMessagesBetweenUsernames(user, correspondent, false);
                    if (!messages.Exists(x => x.Sender == correspondent.Username))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"{correspondent.Username} has disabled messaging." });
                    }
                }
            }

            return Json(new Status { Success = true, ReturnUrl = $"Messages?username={correspondent.Username}" });
        }

        [HttpPost]
        public IActionResult Remove(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            User correspondent = _userRepository.GetUser(username);
            if (correspondent == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer seems to exist." });
            }

            _messagesRepository.DeleteMessagesBetweenUsernames(user, correspondent);
            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendCourierMessage()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false });
            }

            if (CheckForMessageSentCookie(user))
            {
                return Json(new Status { Success = false });
            }

            string domain = Helper.GetDomain();
            Task.Run(() =>
            {
                Settings settings = _settingsRepository.GetSettings(user);

                if (settings.CurrentIntroStage != IntroStage.Completed)
                {
                    return;
                }

                if ((DateTime.UtcNow - settings.SpecialCourierMessageSentDate).TotalMinutes < 5)
                {
                    return;
                }

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (SendSpecialCourierMessage(user, settings, accomplishments, domain))
                {
                    settings.SpecialCourierMessageSentDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.SpecialCourierMessageSentDate), settings.SpecialCourierMessageSentDate.ToString());
                }
                else if ((DateTime.UtcNow - settings.CourierMessageSentDate).TotalDays < 2.5)
                {
                    return;
                }
                else if (SendCourierMessage(user, settings.CourierMessageSentDate == DateTime.MinValue, domain))
                {
                    settings.CourierMessageSentDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.CourierMessageSentDate), settings.CourierMessageSentDate.ToString());
                }

                user.FlushSession = true;
                _userRepository.UpdateUser(user);
            });

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetUnreadMessages(string correspondent)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false });
            }

            int unreadCount = _messagesRepository.GetUnreadMessagesCount(user, correspondent);
            return Json(new Status { Success = true, Amount = unreadCount });
        }

        [HttpGet]
        public IActionResult GetToast()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                if (settings.CurrentIntroStage != IntroStage.Completed)
                {
                    return Json(new Status { Success = false });
                }
            }

            List<string[]> toastMessages = Messages.GetToast();
            if (toastMessages == null || toastMessages.Count == 0 || toastMessages.Any(x => x.Length < 3))
            {
                return Json(new Status { Success = false });
            }

            return Json(new { Success = true, Messages = toastMessages.ToArray() });
        }

        [HttpGet]
        public IActionResult GetMessagesPartialView(string username, bool isMessageBox, string searchQuery = "")
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MessagesViewModel messagesViewModel = GenerateModel(user, username, isMessageBox, searchQuery);
            if (messagesViewModel.Correspondent == null)
            {
                return PartialView("_MessagesAllMessagesPartial", messagesViewModel);
            }
            return PartialView("_MessagesConversationBodyPartial", messagesViewModel);
        }

        [HttpGet]
        public IActionResult GetMessagesBoxPartialView(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MessagesViewModel messagesViewModel = GenerateModel(user, username, true);
            if (messagesViewModel.Correspondent == null)
            {
                return PartialView("_MessagesBoxAllPartial", messagesViewModel);
            }
            return PartialView("_MessagesBoxConversationPartial", messagesViewModel);
        }

        private MessagesViewModel GenerateModel(User user, string username, bool isMessageBox, string searchQuery = "")
        {
            MessagesViewModel messagesViewModel = new MessagesViewModel
            {
                Title = "Messages",
                User = user,
                SearchQuery = searchQuery
            };

            if (string.IsNullOrEmpty(username) || username.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase))
            {
                messagesViewModel.AllMessages = _messagesRepository.GetAllMessages(user, string.IsNullOrEmpty(searchQuery) ? 50 : 500);
                if (!string.IsNullOrEmpty(searchQuery))
                {
                    messagesViewModel.AllMessages = messagesViewModel.AllMessages.FindAll(x => x.Sender != user.Username && x.Sender.Contains(searchQuery, StringComparison.InvariantCultureIgnoreCase) || x.Recipient != user.Username && x.Recipient.Contains(searchQuery, StringComparison.InvariantCultureIgnoreCase));
                }
            }
            else
            {
                User correspondent = _userRepository.GetUser(username);
                if (correspondent != null)
                {
                    List<Message> conversation = _messagesRepository.GetMessagesBetweenUsernames(user, correspondent);
                    Settings correspondentSettings = _settingsRepository.GetSettings(correspondent, false);
                    bool canSendMessages = true;
                    if (correspondent.Username == Helper.GetCourierUsername())
                    {
                        canSendMessages = false;
                    }
                    else
                    {
                        if (user.Role < UserRole.Helper)
                        {
                            if (correspondentSettings.DisableMessages)
                            {
                                if (!conversation.Exists(x => x.Sender == correspondent.Username))
                                {
                                    canSendMessages = false;
                                }
                            }
                        }
                    }

                    Settings settings = _settingsRepository.GetSettings(user);

                    messagesViewModel.Correspondent = correspondent;
                    messagesViewModel.Conversation = conversation;
                    messagesViewModel.IsBlocked = _blockedRepository.HasBlockedUser(user, correspondent);
                    messagesViewModel.CanSendMessage = canSendMessages;
                    messagesViewModel.ShowReadReceipts = settings.EnableReadReceipts && correspondentSettings.EnableReadReceipts;
                    messagesViewModel.IsMessageBox = isMessageBox;
                }
                messagesViewModel.AllMessages = _messagesRepository.GetAllMessages(user);
            }

            return messagesViewModel;
        }

        private bool SendCourierMessage(User user, bool isFirstCourierMessage, string domain)
        {
            Random rnd = new Random();
            int daysToExpire = rnd.Next(5) + 2;
            string promoCode;
            string message;

            if (isFirstCourierMessage)
            {
                Item cardPack = _itemsRepository.GetAllItems(ItemCategory.Upgrader).First();
                promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.RandomItem, cardPack);
                message = $"Welcome {user.GetFriendlyName()}! I'll sometimes send you promo codes to use at the Plaza. If you enter: {promoCode} within the next 7 days, you'll receive a free Upgrader!";
            }
            else
            {
                switch (rnd.Next(7))
                {
                    case 1:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.CardPack);
                        message = $"Express service! I have a promo code for you to enter on the Promo Codes page! Just enter: {promoCode} within the next {daysToExpire} days to get a free Card Pack!";
                        break;
                    case 2:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.GainPoints);
                        message = $"Hello {user.GetFriendlyName()}! I have a special promo code for you: {promoCode}. Redeem it on the Promo Codes page within the next {daysToExpire} days and you'll get free {Resources.Currency}!";
                        break;
                    case 3:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.ResetBankVisit);
                        message = $"I have a special promo code for you! Here it is: {promoCode}. Redeem it on the Promo Codes page within the next {daysToExpire} days to collect your daily interest at the Bank again!";
                        break;
                    case 4:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.RandomItem);
                        message = $"I've got something for you! Enter this promo code: {promoCode} on the Promo Codes page within the next {daysToExpire} days and you'll get a free item!";
                        break;
                    case 5:
                        List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 50000 && x.Points >= 1000);
                        Item item = items[rnd.Next(items.Count)];
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.RandomItem, item);
                        message = $"How's it going, {user.GetFriendlyName()}? If you enter the promo code: {promoCode} on the Promo Codes page within the next {daysToExpire} days, you'll get the item \"{item.Name}\" for free!";
                        break;
                    case 6:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.ResetSpinOfSuccessVisit);
                        message = $"Here's a new promo code, {user.GetFriendlyName()}! If you enter the promo code: {promoCode} on the Promo Codes page within the next {daysToExpire} days, you can spin the wheel again at the Spin of Success!";
                        break;
                    case 7:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.ResetTreasureDiveVisit);
                        message = $"Hey {user.GetFriendlyName()}! If you enter the promo code: {promoCode} on the Promo Codes page within the next {daysToExpire} days, you can play Treasure Dive again!";
                        break;
                    case 8:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire, PromoEffect.ResetWizardVist);
                        message = $"I've got something just for you, {user.GetFriendlyName()}! If you enter the promo code: {promoCode} on the Promo Codes page within the next {daysToExpire} days, you can visit the Young Wizard again!";
                        break;
                    default:
                        promoCode = _promoCodesRepository.GenerateRandomPromoCode(daysToExpire);
                        message = $"Special delivery! Here's a free promo code for you to redeem on the Promo Codes page: {promoCode}. Make sure to use it quick because it expires in {daysToExpire} days!";
                        break;
                }
            }

            return _messagesRepository.SendCourierMessage(user, message, domain);
        }

        private bool SendSpecialCourierMessage(User user, Settings settings, List<Accomplishment> accomplishments, string domain)
        {
            List<Accomplishment> filteredAccomplishments = accomplishments.FindAll(x => x.Category > AccomplishmentCategory.Orange);

            if (filteredAccomplishments.Count >= 10 && filteredAccomplishments.Count <= 15)
            {
                if (!settings.HasReceived10AccomplishmentsCourierMessage)
                {
                    _messagesRepository.SendCourierMessage(user, $"Congratulations on earning 10 accomplishments! Keep exploring {Resources.SiteName} to earn more of them! Who knows? You might even get something for earning a lot of them.", domain);

                    settings.HasReceived10AccomplishmentsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceived10AccomplishmentsCourierMessage), settings.HasReceived10AccomplishmentsCourierMessage.ToString());

                    return true;
                }
            }

            if ((accomplishments.Count >= 15 && user.GetAgeInMinutes() >= 30 && settings.MatchLevel >= 2) || settings.MatchLevel == 9)
            {
                if (!settings.HasReceivedShowroomCourierMessage && !accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess))
                {
                    _messagesRepository.SendCourierMessage(user, $"The Showroom Owner has a message for you: \"Hi {user.GetFriendlyName()}! I'd like to invite you to visit the Showroom Store. You can find it at the Buyers Club. Hope to see you soon.\"", domain);

                    settings.HasReceivedShowroomCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedShowroomCourierMessage), settings.HasReceivedShowroomCourierMessage.ToString());

                    return true;
                }
            }

            if (settings.MatchLevel >= 2 && settings.MatchLevel <= 5)
            {
                if (!settings.HasReceivedSecondMatchCourierMessage)
                {
                    _messagesRepository.SendCourierMessage(user, $"I have a message for you from an ethereal spirit. It reads: \"The door... the door will open soon. It's almost time... Defeat more challengers to prepare, {user.GetFriendlyName()}!\"", domain);

                    settings.HasReceivedSecondMatchCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedSecondMatchCourierMessage), settings.HasReceivedSecondMatchCourierMessage.ToString());

                    return true;
                }
            }
            if (settings.MatchLevel >= 8)
            {
                if (!settings.HasReceivedPhantomDoorCourierMessage)
                {
                    _messagesRepository.SendCourierMessage(user, $"A strange door has appeared at the Plaza, {user.GetFriendlyName()}! Nobody seems to know where it came from but it may be worth investigating!", domain);

                    settings.HasReceivedPhantomDoorCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedPhantomDoorCourierMessage), settings.HasReceivedPhantomDoorCourierMessage.ToString());

                    return true;
                }
            }

            if (!settings.HasReceivedBeyondWorldCourierMessage && accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                _messagesRepository.SendCourierMessage(user, $"The Young Wizard has a message for you: \"{user.GetFriendlyName()}! {user.GetFriendlyName()}! Please help! I messed up big time! I accidentally opened an entrance to the Beyond World and I don't know how to close it! Can you please investigate it at the Plaza and figure out how to close it?\"", domain);

                settings.HasReceivedBeyondWorldCourierMessage = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasReceivedBeyondWorldCourierMessage), settings.HasReceivedBeyondWorldCourierMessage.ToString());

                return true;
            }

            if (!settings.HasReceivedClubTrialCourierMessage && filteredAccomplishments.Count >= 15 && user.GetAgeInDays() >= 3)
            {
                settings.HasReceivedClubTrialCourierMessage = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasReceivedClubTrialCourierMessage), settings.HasReceivedClubTrialCourierMessage.ToString());

                if (_clubRepository.GetAllClubMemberships(user).Count == 0)
                {
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.ClubTrial);
                    _messagesRepository.SendCourierMessage(user, $"Interested in joining the {Resources.SiteName} Club? Redeem the promo code: {promoCode} within the next week and get a free month of {Resources.SiteName} Club membership!", domain);

                    return true;
                }
            }

            if (filteredAccomplishments.Count >= 20)
            {
                if (!settings.HasReceivedAccomplishmentsCourierMessage)
                {
                    List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 25000 && x.Points >= 15000);
                    Random rnd = new Random();
                    Item item = items[rnd.Next(items.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.RandomItem, item);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations on earning 20 accomplishments! Here's a special promo code as a reward: {promoCode}. Make sure to use it within the next week before it expires!", domain);

                    settings.HasReceivedAccomplishmentsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedAccomplishmentsCourierMessage), settings.HasReceivedAccomplishmentsCourierMessage.ToString());

                    return true;
                }
            }

            if (filteredAccomplishments.Count >= 35)
            {
                if (!settings.HasReceived35AccomplishmentsCourierMessage)
                {
                    List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 45000 && x.Points >= 35000);
                    Random rnd = new Random();
                    Item item = items[rnd.Next(items.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.RandomItem, item);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations on earning 35 accomplishments! Here's a special promo code as a reward: {promoCode}. Make sure to use it within the next week before it expires!", domain);

                    settings.HasReceived35AccomplishmentsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceived35AccomplishmentsCourierMessage), settings.HasReceived35AccomplishmentsCourierMessage.ToString());

                    return true;
                }
            }

            if (filteredAccomplishments.Count >= 50)
            {
                if (!settings.HasReceived50AccomplishmentsCourierMessage)
                {
                    List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 65000 && x.Points >= 45000);
                    Random rnd = new Random();
                    Item item = items[rnd.Next(items.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.RandomItem, item);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations on earning 50 accomplishments! Here's a special promo code as a reward: {promoCode}. Make sure to use it within the next week before it expires!", domain);

                    settings.HasReceived50AccomplishmentsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceived50AccomplishmentsCourierMessage), settings.HasReceived50AccomplishmentsCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Count >= 50)
            {
                if (!settings.HasReceivedStoreMagicianTrialMessage && settings.StoreMagicianExpiryDate <= DateTime.UtcNow)
                {
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(30, PromoEffect.StoreMagicianTrial);
                    _messagesRepository.SendCourierMessage(user, $"You can buy Sponsor Coins at the Sponsor Society to help support {Resources.SiteName} and unlock awesome perks! Here's a promo code for a free 3 day trial of the Store Magician so you can try it out for yourself: {promoCode}!", domain);

                    settings.HasReceivedStoreMagicianTrialMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedStoreMagicianTrialMessage), settings.HasReceivedStoreMagicianTrialMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFive || x.Id == AccomplishmentId.FiveUserWins))
            {
                if (!settings.HasReceivedFiveStadiumWinsCourierMessage)
                {
                    List<Enhancer> enhancers = _itemsRepository.GetAllEnhancers().FindAll(x => x.TotalUses > 0);
                    Enhancer enhancer = enhancers.Find(x => x.SpecialType == EnhancerSpecialType.Luck);
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, enhancer);
                    _messagesRepository.SendCourierMessage(user, $"The Right-Side Up Guard has a message for you: \"I've been watching you at the Stadium and I'm impressed. If you earn all the Relics, you'll get a chance to challenge me at the Stadium. In the meantime, here's a promo code that might help you out: {promoCode}.\"", domain);

                    _messagesRepository.SendPersonalMessage(user, domain, $"How're you liking {Resources.SiteName} so far? Your answer can really help improve the site (I read everything)! Also, just so you know, we have a Discord: {Helper.GetDiscordUrl()}");

                    settings.HasReceivedFiveStadiumWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedFiveStadiumWinsCourierMessage), settings.HasReceivedFiveStadiumWinsCourierMessage.ToString());

                    return true;
                }
            }

            if (user.GetAgeInDays() >= 30 && (DateTime.UtcNow - settings.LastRepairShopDiscountCourierDate).TotalDays >= 30)
            {
                _messagesRepository.SendCourierMessage(user, $"Hello {user.GetFriendlyName()}! I've given you a 50% discount on your next purchase at the Repair Shop! You'll want to take advantage of it soon!", domain);

                settings.RepairCouponPercent = 50;
                _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());

                settings.LastRepairShopDiscountCourierDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastRepairShopDiscountCourierDate), settings.LastRepairShopDiscountCourierDate.ToString());

                return true;
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTen))
            {
                if (!settings.HasReceivedTenStadiumWinsCourierMessage)
                {
                    List<Enhancer> enhancers = _itemsRepository.GetAllEnhancers().FindAll(x => x.TotalUses < 0);
                    Enhancer enhancer = enhancers.FindAll(x => x.IsFiller)[2];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, enhancer);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations! You defeated the Level 10 Checkpoint CPU at the Stadium! Here's a promo code for you to redeem as a reward: {promoCode}.", domain);

                    settings.HasReceivedTenStadiumWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedTenStadiumWinsCourierMessage), settings.HasReceivedTenStadiumWinsCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFifteen))
            {
                if (!settings.HasReceivedFifteenStadiumWinsCourierMessage)
                {
                    Item candyCard = _itemsRepository.GetAllItems(ItemCategory.Candy).Find(x => x.Name.Contains("Lemon Light", StringComparison.InvariantCultureIgnoreCase));
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, candyCard);
                    _messagesRepository.SendCourierMessage(user, $"The Right-Side Up Guard has a message for you: \"Hi {user.GetFriendlyName()}, once you've earned the 7 Relics by defeating all the Checkpoint CPUs, you should come visit me for a Stadium match. Until then, here's a promo code that might help you explore a little more: {promoCode}.\"", domain);

                    settings.HasReceivedFifteenStadiumWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedFifteenStadiumWinsCourierMessage), settings.HasReceivedFifteenStadiumWinsCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTwenty))
            {
                if (!settings.HasReceivedTwentyStadiumWinsCourierMessage)
                {
                    List<Item> companions = _itemsRepository.GetAllItems().FindAll(x => x.Category == ItemCategory.Companion);
                    Random rnd = new Random();
                    Item companion = companions[rnd.Next(companions.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, companion);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations! You've earned more than half of the Relics! As a reward, you may want to use this promo code to gain a new friend: {promoCode}.", domain);

                    settings.HasReceivedTwentyStadiumWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedTwentyStadiumWinsCourierMessage), settings.HasReceivedTwentyStadiumWinsCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.FiveUserWins))
            {
                if (!settings.HasReceivedFiveStadiumUserWinsCourierMessage)
                {
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.IncreaseBagSize);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations! You've won against five users at the Stadium! As a reward, here's a small promo code you can redeem: {promoCode}.", domain);

                    settings.HasReceivedFiveStadiumUserWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedFiveStadiumUserWinsCourierMessage), settings.HasReceivedFiveStadiumUserWinsCourierMessage.ToString());

                    return true;
                }
            }


            if (accomplishments.Any(x => x.Id == AccomplishmentId.FifteenUserWins))
            {
                if (!settings.HasReceivedFiftyStadiumUserWinsCourierMessage)
                {
                    Random rnd = new Random();
                    List<Item> themeItems = _itemsRepository.GetAllItems().FindAll(x => x.Category == ItemCategory.Theme && x.Points < 100000 && !x.Name.Contains("Default", StringComparison.InvariantCultureIgnoreCase));
                    Item themeItem = themeItems[rnd.Next(themeItems.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, themeItem);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations! You've won against 15 users at the Stadium! Since you've won so much, you may want to use this promo code: {promoCode}. It'll help make your Deck look more stylish.", domain);

                    settings.HasReceivedFiftyStadiumUserWinsCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedFiftyStadiumUserWinsCourierMessage), settings.HasReceivedFiftyStadiumUserWinsCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.FiveShowroomArtCards))
            {
                if (!settings.HasReceivedArtShowroomCourierMessage)
                {
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.GainBigPoints);
                    _messagesRepository.SendCourierMessage(user, $"The Museum Curator has a message for you: \"Thanks for keeping art alive and adding several official Art Cards to your Showroom! As a thanks, here's a promo code that may help you out a little: {promoCode}.\"", domain);

                    settings.HasReceivedArtShowroomCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedArtShowroomCourierMessage), settings.HasReceivedArtShowroomCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Count >= 100 && !accomplishments.Any(x => x.Id == AccomplishmentId.HiddenTreasure))
            {
                if (!settings.HasReceivedSecretCourierMessage)
                {
                    _messagesRepository.SendCourierMessage(user, $"There is a way to speak to me. But you have to know the two secret words to open me up.", domain);

                    settings.HasReceivedSecretCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedSecretCourierMessage), settings.HasReceivedSecretCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked) && accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked) && accomplishments.Any(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked))
            {
                if (!settings.HasReceivedAllMuseumOpenedCourierMessage)
                {
                    Random rnd = new Random();
                    List<Item> themeItems = _itemsRepository.GetAllItems().FindAll(x => x.Category == ItemCategory.Theme && x.Points < 100000 && !x.Name.Contains("Default", StringComparison.InvariantCultureIgnoreCase));
                    Item themeItem = themeItems[rnd.Next(themeItems.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(365, PromoEffect.RandomItem, themeItem);
                    _messagesRepository.SendCourierMessage(user, $"The Museum Curator has a message for you: \"Thanks for opening all the sections of the Museum! Here's a promo code as a token of my gratitude: {promoCode}.\"", domain);

                    settings.HasReceivedAllMuseumOpenedCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedAllMuseumOpenedCourierMessage), settings.HasReceivedAllMuseumOpenedCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.AllChecklist))
            {
                if (!settings.HasReceivedAllChecklistCourierMessage)
                {
                    List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 100000 && x.Points >= 50000);
                    Random rnd = new Random();
                    Item item = items[rnd.Next(items.Count)];
                    string promoCode = _promoCodesRepository.GenerateRandomPromoCode(7, PromoEffect.RandomItem, item);
                    _messagesRepository.SendCourierMessage(user, $"Congratulations on completing all the checklist items! Here's a special promo code as a reward: {promoCode}. Make sure to use it within the next week before it expires!", domain);

                    settings.HasReceivedAllChecklistCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedAllChecklistCourierMessage), settings.HasReceivedAllChecklistCourierMessage.ToString());

                    return true;
                }
            }
            else
            {
                DateTime lastAccomplishmentDate = user.CreatedDate;
                Checklist lastChecklist = _checklistRepository.GetLastEarnedChecklist(accomplishments);
                if (lastChecklist != null)
                {
                    Accomplishment accomplishment = accomplishments.Find(x => x.Id == lastChecklist.AccomplishmentId);
                    lastAccomplishmentDate = accomplishment.ReceivedDate;
                }

                List<Checklist> checklist = _checklistRepository.GetChecklist(accomplishments);
                if (checklist != null)
                {
                    Checklist nextChecklist = checklist.Find(x => !x.Completed);
                    if (settings.LastReceivedCourierChecklistAccomplishmentId != nextChecklist.AccomplishmentId)
                    {
                        if ((DateTime.UtcNow - lastAccomplishmentDate).TotalHours >= (nextChecklist.Rank > ChecklistRank.Intermediate ? 12 : 6))
                        {
                            _messagesRepository.SendCourierMessage(user, $"Hey {user.GetFriendlyName()}! When you get a chance, your checklist on your Feed suggests you should {nextChecklist.Description}!", domain);

                            settings.LastReceivedCourierChecklistAccomplishmentId = nextChecklist.AccomplishmentId;
                            _settingsRepository.SetSetting(user, nameof(settings.LastReceivedCourierChecklistAccomplishmentId), settings.LastReceivedCourierChecklistAccomplishmentId.ToString());

                            return true;
                        }
                    }
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.CPUCompleted))
            {
                if (!settings.HasReceivedStadiumChallengesCourierMessage)
                {
                    _messagesRepository.SendCourierMessage(user, "The Right-Side Up Guard has a message for you: \"You earned all the Relics. I'm impressed with your skills at the Stadium. I'd like to challenge you to a Stadium match in the Right-Side Up World. Please come by when you get the chance.\"", domain);

                    settings.HasReceivedStadiumChallengesCourierMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedStadiumChallengesCourierMessage), settings.HasReceivedStadiumChallengesCourierMessage.ToString());

                    return true;
                }
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked))
            {
                if (!settings.HasReceivedCourierPinCodeMessage)
                {
                    Random rnd = new Random();
                    int pinCode = rnd.Next(100, 1000);

                    _messagesRepository.SendCourierMessage(user, $"Hello {user.Username}! I have a special code to give you: {pinCode}. It's not a promo code but you might be able to use it to open something.", domain);

                    settings.GoldBriefcasePinCode = pinCode;
                    _settingsRepository.SetSetting(user, nameof(settings.GoldBriefcasePinCode), settings.GoldBriefcasePinCode.ToString());

                    settings.HasReceivedCourierPinCodeMessage = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasReceivedCourierPinCodeMessage), settings.HasReceivedCourierPinCodeMessage.ToString());

                    return true;
                }
            }

            return false;
        }

        private bool CheckForMessageSentCookie(User user)
        {
            bool courierMessageSent = !string.IsNullOrEmpty(Request.Cookies["CourierMessageSent"]) && Request.Cookies["CourierMessageSent"] == user.Username;
            if (courierMessageSent)
            {
                return true;
            }

            IResponseCookies cookies = Response.Cookies;
            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(2)
            };
            cookies.Append("CourierMessageSent", user.Username, options);

            return false;
        }
    }
}
