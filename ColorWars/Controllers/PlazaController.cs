﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using FuzzySharp;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PlazaController : Controller
    {
        private ILoginRepository _loginRepository;
        private ILocationsRepository _locationsRepository;
        private ISettingsRepository _settingsRepository;
        private ISecretGifterRepository _secretGifterRepository;
        private ISurveyRepository _surveyRepository;
        private INotificationsRepository _notificationsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private INoticeBoardTasksRepository _noticeBoardTasksRepository;
        private IAdminSettingsRepository _adminSettingsRepository;

        public PlazaController(ILoginRepository loginRepository, ILocationsRepository locationsRepository, ISettingsRepository settingsRepository,
        ISecretGifterRepository secretGifterRepository, ISurveyRepository surveyRepository, INotificationsRepository notificationsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, INoticeBoardTasksRepository noticeBoardTasksRepository, IAdminSettingsRepository adminSettingsRepository)
        {
            _loginRepository = loginRepository;
            _locationsRepository = locationsRepository;
            _settingsRepository = settingsRepository;
            _secretGifterRepository = secretGifterRepository;
            _surveyRepository = surveyRepository;
            _notificationsRepository = notificationsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _noticeBoardTasksRepository = noticeBoardTasksRepository;
            _adminSettingsRepository = adminSettingsRepository;
        }

        public IActionResult Index(bool locationNotAvailable, string searchQuery, LocationId locationId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Plaza?searchQuery={searchQuery}" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.VisitPlaza))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.VisitPlaza);
            }

            PlazaViewModel plazaViewModel = GenerateModel(user, settings, searchQuery, locationNotAvailable, locationId);
            plazaViewModel.UpdateViewData(ViewData);
            return View(plazaViewModel);
        }

        [HttpPost]
        public IActionResult UpdateLocation(LocationId locationId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (locationId == LocationId.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a valid location." });
            }

            List<Location> favoritedLocations = _locationsRepository.GetFavoritedLocations(user);
            if (favoritedLocations.Any(x => x.LocationId == locationId))
            {
                _locationsRepository.RemoveFavoritedLocation(user, locationId);
            }
            else
            {
                _locationsRepository.AddFavoritedLocation(user, locationId);
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult ToggleView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            settings.ExpandLocations = !settings.ExpandLocations;
            _settingsRepository.SetSetting(user, nameof(settings.ExpandLocations), settings.ExpandLocations.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult UpdatePosition(LocationId locationId, int newPosition)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (locationId == LocationId.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a valid location." });
            }

            List<Location> locations = _locationsRepository.GetFavoritedLocations(user);
            Location location = locations.Find(x => x.LocationId == locationId);
            if (location == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This location isn't favorited." });
            }

            if (newPosition < 0 || newPosition >= locations.Count)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't a valid position." });
            }

            if (newPosition == location.Position)
            {
                return Json(new Status { Success = false });
            }

            if (newPosition < location.Position)
            {
                foreach (Location updateLocation in locations.FindAll(x => x.Position >= newPosition && x.Position < location.Position))
                {
                    updateLocation.Position++;
                    _locationsRepository.UpdateFavoritedLocation(user, updateLocation);
                }
            }
            else
            {
                foreach (Location updateLocation in locations.FindAll(x => x.Position <= newPosition && x.Position > location.Position))
                {
                    updateLocation.Position--;
                    _locationsRepository.UpdateFavoritedLocation(user, updateLocation);
                }
            }

            location.Position = newPosition;
            _locationsRepository.UpdateFavoritedLocation(user, location);

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetPlazaPartialView(string searchQuery = "")
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            PlazaViewModel plazaViewModel = GenerateModel(user, settings, searchQuery);
            return PartialView("_PlazaMainPartial", plazaViewModel);
        }

        private PlazaViewModel GenerateModel(User user, Settings settings, string searchQuery = "", bool locationNotAvailable = false, LocationId? locationId = null)
        {
            bool canShowNewLocation = false;
            string successMessage = string.Empty;
            if (string.IsNullOrEmpty(searchQuery))
            {
                successMessage = Messages.GetSuccessMessage();
                canShowNewLocation = string.IsNullOrEmpty(successMessage) && (DateTime.UtcNow - settings.LastNewLocationDate).TotalHours >= 8;
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            FeedCategory showHiddenLocation = FeedCategory.None;
            if (ShouldShowRandomChallenge(user, settings, accomplishments))
            {
                showHiddenLocation = FeedCategory.Stadium;
            }
            else if (canShowNewLocation)
            {
                if (ShouldShowRewardsStore(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.RewardsStore;
                }
                else if (ShouldShowNoticeBoard(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.NoticeBoard;
                }
                else if (ShouldShowSecretGifter(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.SecretGifter;
                }
                else if (ShouldShowWarehouse(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.Warehouse;
                }
                else if (ShouldShowMuseum(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.Museum;
                }
                else if (ShouldShowMerchants(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.Merchants;
                }
                else if (ShouldShowSurvey(user, settings, accomplishments))
                {
                    showHiddenLocation = FeedCategory.Survey;
                }

                if (showHiddenLocation != FeedCategory.None)
                {
                    settings.LastNewLocationDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastNewLocationDate), settings.LastNewLocationDate.ToString());
                }
            }

            List<Location> allLocations = _locationsRepository.GetLocations(settings, accomplishments, LocationArea.None, false);
            List<Location> locations = allLocations.FindAll(x => x.LocationArea == LocationArea.Plaza);
            List<Location> favoritedLocations = _locationsRepository.GetFavoritedLocations(user);
            if (favoritedLocations.Count > 1 && favoritedLocations.GroupBy(x => x.Position).Any(x => x.Count() > 1))
            {
                for (int i = 0; i < favoritedLocations.Count; i++)
                {
                    favoritedLocations[i].Position = i;
                    _locationsRepository.UpdateFavoritedLocation(user, favoritedLocations[i]);
                }
            }
            foreach (Location favoritedLocation in favoritedLocations)
            {
                foreach (Location location in allLocations)
                {
                    if (favoritedLocation.LocationId == location.LocationId)
                    {
                        location.IsFavorited = true;
                        Location newLocation = CloneLocation(location);
                        newLocation.LocationType = LocationType.Favorites;
                        newLocation.Position = favoritedLocation.Position;
                        locations.Add(newLocation);
                        break;
                    }
                }
            }

            Location matchedUnavailableLocation = null;
            if (string.IsNullOrEmpty(searchQuery))
            {
                locations = locations.FindAll(x => x.LocationCategory != LocationCategory.Secret);
            }
            else
            {
                int fuzzyMatchConfidence = 75;
                List<Location> matchedLocations = new List<Location>();
                foreach (Location location in locations)
                {
                    if (Fuzz.WeightedRatio(searchQuery.ToLower(), location.Name.ToLower()) >= fuzzyMatchConfidence)
                    {
                        location.Tags = new List<string>();
                        matchedLocations.Add(location);
                        continue;
                    }
                    foreach (string tag in location.Tags)
                    {
                        if (Fuzz.WeightedRatio(searchQuery.ToLower(), tag.ToLower()) >= fuzzyMatchConfidence)
                        {
                            location.Tags = new List<string> { tag };
                            matchedLocations.Add(location);
                            break;
                        }
                    }
                }
                locations = matchedLocations;
                if (locations.Count == 0)
                {
                    List<Location> allPlazaLocations = _locationsRepository.GetLocations(settings, accomplishments, LocationArea.Plaza, true);
                    foreach (Location location in allPlazaLocations)
                    {
                        if (Fuzz.WeightedRatio(searchQuery.ToLower(), location.Name.ToLower()) >= fuzzyMatchConfidence)
                        {
                            location.Tags = new List<string> { };
                            matchedUnavailableLocation = location;
                            if (matchedUnavailableLocation.LocationCategory == LocationCategory.Limited)
                            {
                                location.LocationCategory = _locationsRepository.GetLocations(settings, accomplishments, LocationArea.Plaza, false).Any(x => x.LocationId == location.LocationId) ? location.LocationCategory : LocationCategory.Limited;
                            }
                            break;
                        }
                        foreach (string tag in location.Tags)
                        {
                            if (Fuzz.WeightedRatio(searchQuery.ToLower(), tag.ToLower()) >= fuzzyMatchConfidence)
                            {
                                location.Tags = new List<string> { tag };
                                matchedUnavailableLocation = location;
                                if (matchedUnavailableLocation.LocationCategory == LocationCategory.Limited)
                                {
                                    location.LocationCategory = _locationsRepository.GetLocations(settings, accomplishments, LocationArea.Plaza, false).Any(x => x.LocationId == location.LocationId) ? location.LocationCategory : LocationCategory.Limited;
                                }
                                break;
                            }
                        }
                    }
                }
            }

            List<IGrouping<LocationType, Location>> groupedLocations = locations.OrderBy(x => x.LocationType).ThenBy(x => x.Position).ThenByDescending(x => x.LocationCategory).ThenByDescending(x => x.IsFavorited).ThenBy(x => x.Name).GroupBy(x => x.LocationType).ToList();

            PlazaViewModel locationsViewModel = new PlazaViewModel
            {
                Title = "Plaza",
                User = user,
                LastLocation = locationId,
                Locations = groupedLocations,
                MatchedUnavailableLocation = matchedUnavailableLocation,
                SearchQuery = searchQuery,
                LocationNotAvailable = locationNotAvailable,
                SuccessMessage = successMessage,
                ShowHiddenLocation = showHiddenLocation,
                ExpandLocations = settings.ExpandLocations,
                CanShowAds = accomplishments.Count > 10 && user.AdFreeExpiryDate <= DateTime.UtcNow && Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != "Development" && _adminSettingsRepository.GetSettings().EnableAds
            };

            if (!settings.HasSeenLocationsTutorial)
            {
                settings.HasSeenLocationsTutorial = true;
                locationsViewModel.ShowLocationsTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenLocationsTutorial), settings.HasSeenLocationsTutorial.ToString());
            }
            else if (!settings.HasSeenLocationFavoritesTutorial && favoritedLocations.Count == 0 && accomplishments.Count >= 15)
            {
                settings.HasSeenLocationFavoritesTutorial = true;
                locationsViewModel.ShowFavoritesTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenLocationFavoritesTutorial), settings.HasSeenLocationFavoritesTutorial.ToString());
            }

            return locationsViewModel;
        }

        private Location CloneLocation(Location location)
        {
            return new Location
            {
                Name = location.Name,
                Description = location.Description,
                ImageUrl = location.ImageUrl,
                Url = location.Url,
                WidthToHeightRatio = location.WidthToHeightRatio,
                LocationId = location.LocationId,
                LocationCategory = location.LocationCategory,
                IsFavorited = location.IsFavorited,
                Tags = location.Tags
            };
        }

        private bool ShouldShowRandomChallenge(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (accomplishments.Count < 15)
            {
                return false;
            }

            if (!settings.ReceiveStadiumChallenges)
            {
                return false;
            }

            if (settings.StadiumChallengeExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.StadiumChallengeExpiryDate).TotalDays < 1)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            bool firstChallenge = settings.StadiumChallengeExpiryDate == DateTime.MinValue;

            settings.StadiumChallengeExpiryDate = DateTime.UtcNow.AddDays(1);
            _settingsRepository.SetSetting(user, nameof(settings.StadiumChallengeExpiryDate), settings.StadiumChallengeExpiryDate.ToString());

            if (settings.CheckedStadiumChallenge)
            {
                settings.CheckedStadiumChallenge = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedStadiumChallenge), settings.CheckedStadiumChallenge.ToString());
            }

            Array cpuChallenges = Enum.GetValues(typeof(CPUChallenge));
            settings.StadiumChallenge = firstChallenge ? CPUChallenge.OnlyAdditionEnhancers : (CPUChallenge)cpuChallenges.GetValue(rnd.Next(cpuChallenges.Length - 1) + 1);
            _settingsRepository.SetSetting(user, nameof(settings.StadiumChallenge), ((int)settings.StadiumChallenge).ToString());

            Task result = AddNewStadiumChallegeNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowMerchants(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInDays() < 7 && accomplishments.Count < 50)
            {
                return false;
            }

            if (settings.MerchantsExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.MerchantsExpiryDate).TotalDays < settings.DaysUntilNextMerchantsVisit)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.MerchantsExpiryDate = DateTime.UtcNow.AddHours(6);
            _settingsRepository.SetSetting(user, nameof(settings.MerchantsExpiryDate), settings.MerchantsExpiryDate.ToString());

            settings.DaysUntilNextMerchantsVisit = rnd.Next(6) + 5;
            _settingsRepository.SetSetting(user, nameof(settings.DaysUntilNextMerchantsVisit), settings.DaysUntilNextMerchantsVisit.ToString());

            settings.MerchantsJewelCardMultiplier = Math.Max(1.25, rnd.NextDouble() + 1);
            _settingsRepository.SetSetting(user, nameof(settings.MerchantsJewelCardMultiplier), settings.MerchantsJewelCardMultiplier.ToString());

            if (settings.MerchantsJewelsSold > 0)
            {
                settings.MerchantsJewelsSold = 0;
                _settingsRepository.SetSetting(user, nameof(settings.MerchantsJewelsSold), settings.MerchantsJewelsSold.ToString());
            }

            if (settings.CheckedMerchants)
            {
                settings.CheckedMerchants = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedMerchants), settings.CheckedMerchants.ToString());
            }

            Task result = AddNewMerchantsNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowRewardsStore(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInDays() < 4 && accomplishments.Count < 35)
            {
                return false;
            }

            if (settings.RewardsStoreExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.RewardsStoreExpiryDate).TotalDays < 3)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            int accomplishmentPoints = _accomplishmentsRepository.GetAccomplishmentPoints(user);
            if (accomplishmentPoints <= 0)
            {
                return false;
            }

            settings.RewardsStoreExpiryDate = DateTime.UtcNow.AddHours(6);
            _settingsRepository.SetSetting(user, nameof(settings.RewardsStoreExpiryDate), settings.RewardsStoreExpiryDate.ToString());

            if (settings.RewardsStoreTotalClaimed > 0)
            {
                settings.RewardsStoreTotalClaimed = 0;
                _settingsRepository.SetSetting(user, nameof(settings.RewardsStoreTotalClaimed), settings.RewardsStoreTotalClaimed.ToString());
            }

            if (settings.CheckedRewardsStore)
            {
                settings.CheckedRewardsStore = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedRewardsStore), settings.CheckedRewardsStore.ToString());
            }

            Task result = AddNewRewardsStoreNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowNoticeBoard(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInDays() < 2 && accomplishments.Count < 25)
            {
                return false;
            }

            if (settings.NoticeBoardExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.NoticeBoardExpiryDate).TotalDays < 3)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.NoticeBoardExpiryDate = DateTime.UtcNow.AddDays(3);
            _settingsRepository.SetSetting(user, nameof(settings.NoticeBoardExpiryDate), settings.NoticeBoardExpiryDate.ToString());

            _noticeBoardTasksRepository.DeleteNoticeBoardTask(user);

            if (settings.CheckedNoticeBoard)
            {
                settings.CheckedNoticeBoard = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedNoticeBoard), settings.CheckedNoticeBoard.ToString());
            }

            Task result = AddNoticeBoardNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowWarehouse(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInHours() < 12 && accomplishments.Count < 15)
            {
                return false;
            }

            if (settings.WarehouseExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.WarehouseExpiryDate).TotalDays < 4)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.WarehouseExpiryDate = DateTime.UtcNow.AddHours(6);
            _settingsRepository.SetSetting(user, nameof(settings.WarehouseExpiryDate), settings.WarehouseExpiryDate.ToString());

            if (settings.WarehouseRecentBuyCount > 0)
            {
                settings.WarehouseRecentBuyCount = 0;
                _settingsRepository.SetSetting(user, nameof(settings.WarehouseRecentBuyCount), settings.WarehouseRecentBuyCount.ToString());
            }

            if (settings.CheckedWarehouse)
            {
                settings.CheckedWarehouse = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedWarehouse), settings.CheckedWarehouse.ToString());
            }

            Task result = AddWarehouseNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowSecretGifter(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInDays() < 3 && accomplishments.Count < 30)
            {
                return false;
            }

            if (settings.SecretGifterExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.SecretGifterExpiryDate).TotalDays < 7)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.SecretGifterExpiryDate = DateTime.UtcNow.AddDays(1);
            _settingsRepository.SetSetting(user, nameof(settings.SecretGifterExpiryDate), settings.SecretGifterExpiryDate.ToString());

            SecretGiftUser secretGiftUser = _secretGifterRepository.GetSecretGiftUser(user);
            settings.SecretGiftUsername = secretGiftUser.Username;
            _settingsRepository.SetSetting(user, nameof(settings.SecretGiftUsername), settings.SecretGiftUsername);

            if (settings.CheckedSecretGifter)
            {
                settings.CheckedSecretGifter = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedSecretGifter), settings.CheckedSecretGifter.ToString());
            }

            Task result = AddSecretGifterNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowMuseum(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (user.GetAgeInDays() < 1 && accomplishments.Count < 20)
            {
                return false;
            }

            int daysUntilNextMuseumVisit = 3;
            if (accomplishments.Any(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked) && accomplishments.Any(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked) && accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked))
            {
                daysUntilNextMuseumVisit = 7;
            }
            if (settings.MuseumExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.MuseumExpiryDate).TotalDays < daysUntilNextMuseumVisit)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.MuseumExpiryDate = DateTime.UtcNow.AddDays(1);
            _settingsRepository.SetSetting(user, nameof(settings.MuseumExpiryDate), settings.MuseumExpiryDate.ToString());

            if (settings.CheckedMuseum)
            {
                settings.CheckedMuseum = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedMuseum), settings.CheckedMuseum.ToString());
            }

            Task result = AddMuseumNotification(user, Helper.GetDomain());

            return true;
        }

        private bool ShouldShowSurvey(User user, Settings settings, List<Accomplishment> accomplishments)
        {
            if (accomplishments.Count < 40)
            {
                return false;
            }

            if (settings.SurveyExpiryDate > DateTime.UtcNow || (DateTime.UtcNow - settings.SurveyExpiryDate).TotalDays < 14)
            {
                return false;
            }

            Random rnd = new Random();
            if (rnd.Next(10) != 1)
            {
                return false;
            }

            settings.SurveyExpiryDate = DateTime.UtcNow.AddDays(3);
            _settingsRepository.SetSetting(user, nameof(settings.SurveyExpiryDate), settings.SurveyExpiryDate.ToString());

            _surveyRepository.SaveNewSurveyQuestions(user);

            if (settings.CheckedSurvey)
            {
                settings.CheckedSurvey = false;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedSurvey), settings.CheckedSurvey.ToString());
            }

            Task result = AddSurveyNotification(user, Helper.GetDomain());

            return true;
        }

        private async Task AddNewStadiumChallegeNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedStadiumChallenge)
            {
                _notificationsRepository.AddNotification(user, "You have a new Stadium Challenge!", NotificationLocation.StadiumChallenge, "/Stadium/Challenge", domain: domain);
            }
        }

        private async Task AddNewMerchantsNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedMerchants)
            {
                _notificationsRepository.AddNotification(user, $"You can visit the Traveling Merchants at the Plaza before they leave in 6 hours!", NotificationLocation.Merchants, "/Merchants", domain: domain);
            }
        }

        private async Task AddNewRewardsStoreNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedRewardsStore)
            {
                _notificationsRepository.AddNotification(user, $"The Rewards Store is open at the Plaza for the next 6 hours!", NotificationLocation.RewardsStore, "/RewardsStore", domain: domain);
            }
        }

        private async Task AddNoticeBoardNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedNoticeBoard)
            {
                _notificationsRepository.AddNotification(user, $"There's a new task on the Notice Board at the Plaza!", NotificationLocation.NoticeBoard, "/NoticeBoard", domain: domain);
            }
        }

        private async Task AddWarehouseNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedWarehouse)
            {
                _notificationsRepository.AddNotification(user, $"The Warehouse is open at the Plaza for the next 6 hours!", NotificationLocation.Warehouse, "/Warehouse", domain: domain);
            }
        }

        private async Task AddSecretGifterNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedSecretGifter)
            {
                _notificationsRepository.AddNotification(user, $"You can be a Secret Gifter at the Plaza for the next 24 hours!", NotificationLocation.SecretGifter, "/SecretGifter", domain: domain);
            }
        }

        private async Task AddSurveyNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedSurvey)
            {
                _notificationsRepository.AddNotification(user, $"You have 3 days to fill out a survey at the Plaza!", NotificationLocation.Survey, "/Survey", domain: domain);
            }
        }

        private async Task AddMuseumNotification(User user, string domain)
        {
            await Task.Delay(10000);

            Settings settings = _settingsRepository.GetSettings(user, false);
            if (!settings.CheckedMuseum)
            {
                _notificationsRepository.AddNotification(user, $"The Museum at the Plaza is now open for the next 24 hours!", NotificationLocation.Museum, "/Museum", domain: domain);
            }
        }
    }
}
