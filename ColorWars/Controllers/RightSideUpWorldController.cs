﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class RightSideUpWorldController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IRightSideUpWorldRepository _rightSideUpWorldRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public RightSideUpWorldController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IRightSideUpWorldRepository rightSideUpWorldRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _rightSideUpWorldRepository = rightSideUpWorldRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"RightSideUpWorld" });
            }

            if (!string.IsNullOrEmpty(tag) && tag == "Right-Side Up Guard")
            {
                return RedirectToAction("Guard", "RightSideUpWorld");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate <= DateTime.UtcNow)
            {
                Messages.AddSnack("You haven't opened the Right-Side Up World.", false);

                return RedirectToAction("Index", "Plaza");
            }

            RightSideUpWorldViewModel rightSideUpWorldViewModel = GenerateModel(user, true);
            rightSideUpWorldViewModel.UpdateViewData(ViewData);
            return View(rightSideUpWorldViewModel);
        }

        public IActionResult Guard()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"RightSideUpWorld/Guard" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            RightSideUpWorldViewModel rightSideUpWorldViewModel = GenerateModel(user, false);
            rightSideUpWorldViewModel.UpdateViewData(ViewData);
            return View(rightSideUpWorldViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The portal to the Right-Side Up World has closed.", ShouldRefresh = true });
            }

            int minutesSinceLastPurchase = (int)(DateTime.UtcNow - settings.LastRightSideUpWorldItemPurchaseDate).TotalMinutes;
            if (minutesSinceLastPurchase < 30)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought an item from the Enchanted Store too recently. Try again in {30 - minutesSinceLastPurchase} minute{(30 - minutesSinceLastPurchase == 1 ? "" : "s")}." });
            }

            Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.SpecialType == EnhancerSpecialType.Dark).Find(x => x.Id == itemId);
            if (enhancer == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is not available." });
            }

            if (user.Points < enhancer.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_pointsRepository.SubtractPoints(user, enhancer.Points))
            {
                _itemsRepository.AddItem(user, enhancer);

                settings.LastRightSideUpWorldItemPurchaseDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastRightSideUpWorldItemPurchaseDate), settings.LastRightSideUpWorldItemPurchaseDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{enhancer.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points) });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item could not be bought." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeReward()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't defeated The Right-Side Up Guard yet." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HasTakenRightSideUpGuardItem)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already taken this item." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = _itemsRepository.GetAllItems(ItemCategory.Companion, null, true).Find(x => x.Name.Contains("Dragon", StringComparison.InvariantCultureIgnoreCase));
            if (_itemsRepository.AddItem(user, item))
            {
                settings.HasTakenRightSideUpGuardItem = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasTakenRightSideUpGuardItem), settings.HasTakenRightSideUpGuardItem.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took the item \"{item.Name}\" and it was added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item couldn't be taken." });
        }

        [HttpGet]
        public IActionResult GetRightSideUpWorldPartialView(bool isStore)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The portal to the Right-Side Up World has closed." });
            }

            RightSideUpWorldViewModel rightSideUpWorldViewModel = GenerateModel(user, isStore);
            if (isStore)
            {
                return PartialView("_RightSideUpWorldMainPartial", rightSideUpWorldViewModel);
            }

            return PartialView("_RightSideUpWorldGuardPartial", rightSideUpWorldViewModel);
        }

        private RightSideUpWorldViewModel GenerateModel(User user, bool isStore)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            RightSideUpWorldViewModel rightSideUpWorldViewModel = new RightSideUpWorldViewModel
            {
                Title = "Right-Side Up World",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.RightSideUpWorld,
                ExpiryDate = settings.RightSideUpWorldExpiryDate
            };

            if (isStore)
            {
                rightSideUpWorldViewModel.Items = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.SpecialType == EnhancerSpecialType.Dark);
                rightSideUpWorldViewModel.CanBuyItem = (DateTime.UtcNow - settings.LastRightSideUpWorldItemPurchaseDate).TotalMinutes >= 30;
            }
            else
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                rightSideUpWorldViewModel.Deck = _itemsRepository.GetCards(user, true);
                rightSideUpWorldViewModel.HallOfFame = _rightSideUpWorldRepository.GetRightSideUpGuardHallOfFame();
                rightSideUpWorldViewModel.HasDefeatedGuard = accomplishments.Exists(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated);
                rightSideUpWorldViewModel.HasTakenItem = settings.HasTakenRightSideUpGuardItem;

                if (rightSideUpWorldViewModel.HasDefeatedGuard && !rightSideUpWorldViewModel.HasTakenItem)
                {
                    rightSideUpWorldViewModel.Item = _itemsRepository.GetAllItems(ItemCategory.Companion, null, true).Find(x => x.Name.Contains("Dragon", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            return rightSideUpWorldViewModel;
        }
    }
}
