﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class RepairController : Controller
    {
        private ILoginRepository _loginRepository;
        private IRepairRepository _repairRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private IMatchStateRepository _matchStateRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public RepairController(ILoginRepository loginRepository, IRepairRepository repairRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        IMatchStateRepository matchStateRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _repairRepository = repairRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _matchStateRepository = matchStateRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(bool recharge, bool fromStadium, string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Repair?recharge={recharge}" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                recharge = true;
            }

            RepairViewModel repairViewModel = GenerateModel(user, recharge, fromStadium);
            repairViewModel.UpdateViewData(ViewData);
            return View(repairViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Repair(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item item = _itemsRepository.GetItems(user).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item.", ShouldRefresh = true });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int cost = _repairRepository.GetRepairPoints(item, settings.RepairCouponPercent);
            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                item.RepairedDate = DateTime.UtcNow;
                _itemsRepository.UpdateItem(user, item);
                if (settings.RepairCouponPercent > 0)
                {
                    settings.RepairCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());
                }
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RepairedItem))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RepairedItem);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

            return Json(new Status { Success = true, SuccessMessage = $"Your item \"{item.Name}\" was successfully repaired for {Helper.GetFormattedPoints(cost)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RepairAll()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int cost = GetRepairItems(user, settings.RepairCouponPercent).Sum(x => x.Points);
            if (cost == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have no items to repair." });
            }

            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                if (_itemsRepository.RepairAllItems(user))
                {
                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully repaired all the items in your bag for {Helper.GetFormattedPoints(cost)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }
            }


            return Json(new Status { Success = false, ErrorMessage = $"Your items could not be repaired." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Recharge(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item item = _itemsRepository.GetItems(user).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item.", ShouldRefresh = true });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int cost = _repairRepository.GetRechargePoints(item, settings.RepairCouponPercent);
            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                item.Uses = item.TotalUses;
                _itemsRepository.UpdateItem(user, item);
                if (settings.RepairCouponPercent > 0)
                {
                    settings.RepairCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

            return Json(new Status { Success = true, SuccessMessage = $"Your item \"{item.Name}\" was successfully recharged for {Helper.GetFormattedPoints(cost)} and now has {item.Uses} uses!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetRepairPartialView(bool isRecharge, bool fromStadium)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            RepairViewModel repairViewModel = GenerateModel(user, isRecharge, fromStadium);
            if (isRecharge)
            {
                return PartialView("_RechargeMainPartial", repairViewModel);
            }
            return PartialView("_RepairMainPartial", repairViewModel);
        }

        private RepairViewModel GenerateModel(User user, bool isRecharge, bool fromStadium)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            RepairViewModel repairViewModel = new RepairViewModel
            {
                Title = "Repair Shop",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.RepairShop,
                Items = (isRecharge ? GetRechargeItems(user, settings.RepairCouponPercent).OrderBy(x => x.Uses).ThenBy(x => x.Points) : GetRepairItems(user, settings.RepairCouponPercent).OrderByDescending(x => x.Condition).ThenBy(x => x.Points)).ToList(),
                DiscountPercent = settings.RepairCouponPercent,
                IsRecharge = isRecharge,
                DeckType = settings.DefaultDeckType,
                FromStadium = fromStadium && _matchStateRepository.IsInMatch(user)
            };

            if (!isRecharge)
            {
                repairViewModel.RepairAllCost = repairViewModel.Items.Sum(x => x.Points);
            }

            return repairViewModel;
        }

        private List<Item> GetRepairItems(User user, int discountPercent)
        {
            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.Condition != ItemCondition.New);
            foreach (Item item in items)
            {
                item.UpdatePoints(_repairRepository.GetRepairPoints(item, discountPercent));
            }
            return items;
        }

        private List<Item> GetRechargeItems(User user, int discountPercent)
        {
            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.Uses < x.TotalUses);
            foreach (Item item in items)
            {
                item.UpdatePoints(_repairRepository.GetRechargePoints(item, discountPercent));
            }
            return items;
        }
    }
}
