﻿using System;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PasswordResetController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPasswordResetRepository _passwordResetRepository;
        private IUserRepository _userRepository;
        private IEmailRepository _emailRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public PasswordResetController(ILoginRepository loginRepository, IPasswordResetRepository passwordResetRepository,
        IUserRepository userRepository, IEmailRepository emailRepository, IAdminHistoryRepository adminHistoryRepository)
        {
            _loginRepository = loginRepository;
            _passwordResetRepository = passwordResetRepository;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public IActionResult Index(string username, Guid token)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null || string.IsNullOrEmpty(username))
            {
                return RedirectToAction("Index", "Home");
            }

            PasswordReset passwordReset = _passwordResetRepository.GetPasswordReset(username, token);

            PasswordResetViewModel passwordResetViewModel = GenerateModel(username, passwordReset);
            passwordResetViewModel.UpdateViewData(ViewData);
            return View(passwordResetViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdatePassword(string username, Guid token, string password, string passwordVerify)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(passwordVerify))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to write a new password." });
            }

            if (password != passwordVerify)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your passwords do not match." });
            }

            if (password.Length < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your new password is too short." });
            }

            User user = _userRepository.GetUser(username, false);
            if (user == null || user.InactiveType == InactiveAccountType.Deleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "Something went wrong with updating your password. Please try again." });
            }

            PasswordReset passwordReset = _passwordResetRepository.GetPasswordReset(user.Username, token);
            if (passwordReset == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This password reset link has expired. Please request a new one." });
            }

            if (_userRepository.UpdateUser(user, password))
            {
                _loginRepository.LoginUser(user.Username, password);

                Messages.AddSnack("Your password was successfully reset!");

                string ipAddress = GlobalHttpContext.Current.Connection.RemoteIpAddress.ToString();
                string subject = "Your password was reset!";
                string title = "Password Reset";
                string body = $"Someone with the IP Address: <b>{ipAddress}</b> reset your password. If this is you, kindly disregard this message. Otherwise, please let us know by clicking the button below and contacting us.";

                _emailRepository.SendEmail(user, subject, title, body, "/Contact", true);

                User adminUser = Helper.GetAdminUser();
                _adminHistoryRepository.AddAdminHistory(user, adminUser, $"Password was reset by user with IP Address: {ipAddress}.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your password could not be reset. Please try again." });
        }

        private PasswordResetViewModel GenerateModel(string username, PasswordReset passwordReset)
        {
            PasswordResetViewModel passwordResetViewModel = new PasswordResetViewModel
            {
                Title = "Reset Your Password",
                Username = username,
                PasswordReset = passwordReset
            };

            if (passwordReset != null)
            {
                passwordResetViewModel.CanResetPassword = true;
                passwordResetViewModel.TimeLeftInMinutes = 30 - (int)(DateTime.UtcNow - passwordReset.CreatedDate).TotalMinutes;
            }

            return passwordResetViewModel;
        }
    }
}
