﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class JobsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IJobsRepository _jobsRepository;
        private IUserRepository _userRepository;
        private IEmailRepository _emailRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private INotificationsRepository _notificationsRepository;

        public JobsController(ILoginRepository loginRepository, IJobsRepository jobsRepository, IPointsRepository pointsRepository,
        IUserRepository userRepository, IEmailRepository emailRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISettingsRepository settingsRepository, INotificationsRepository notificationsRepository)
        {
            _loginRepository = loginRepository;
            _jobsRepository = jobsRepository;
            _pointsRepository = pointsRepository;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _notificationsRepository = notificationsRepository;
        }

        public IActionResult Index(bool expand, string tag, JobPosition jobPosition = JobPosition.Attention)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Jobs" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Helpers", "Jobs");
            }

            JobsViewModel jobsViewModel = GenerateModel(user, false, jobPosition, expand);
            jobsViewModel.UpdateViewData(ViewData);
            return View(jobsViewModel);
        }

        public IActionResult Helpers()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Jobs/Helpers" });
            }

            JobsViewModel jobsViewModel = GenerateModel(user, true);
            jobsViewModel.UpdateViewData(ViewData);
            return View(jobsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitJob(JobPosition position, string title, string content, string addToAttentionHall)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot leave any fields empty in your submission." });
            }

            if (title.Length > 50)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your submission title is too long." });
            }

            int maxLength = 2500;
            if (content.Length > maxLength)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your submission is too long. It should be {Helper.FormatNumber(maxLength)} characters or less." });
            }

            if (position == JobPosition.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a job position for your submission." });
            }

            List<JobSubmission> openJobSubmissions = _jobsRepository.GetJobSubmissionsForUser(user).FindAll(x => x.State == JobState.Open && x.Position != JobPosition.Helper);
            if (openJobSubmissions.Count >= 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't have more than 5 open job submissions at the same time. Please submit again once one has been reviewed." });
            }

            JobSubmission lastJobSubmission = openJobSubmissions.FirstOrDefault();
            if (lastJobSubmission != null)
            {
                int minutesSinceLastJobSubmission = (int)(DateTime.UtcNow - lastJobSubmission.CreatedDate).TotalMinutes;
                if (minutesSinceLastJobSubmission < 10)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You've made a job submission too recently. Try again in {10 - minutesSinceLastJobSubmission} minute{(10 - minutesSinceLastJobSubmission == 1 ? "" : "s")}." });
                }
            }

            bool isAddToAttentionHall = addToAttentionHall == "on";
            content = Regex.Replace(content, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            _jobsRepository.AddJobSubmission(user, position, title, content, isAddToAttentionHall);

            return Json(new Status { Success = true, SuccessMessage = "Your job submission was successfully sent! You'll be contacted if it's accepted!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConfirmJob(string status, Guid jobId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }
            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<JobSubmission> jobSubmissions = _jobsRepository.GetJobSubmissions(user, true);
            JobSubmission jobSubmission = jobSubmissions.Find(x => x.Id == jobId);
            if (jobSubmission == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This job submission is no longer open or no longer exists.", ShouldRefresh = true });
            }

            if (jobSubmission.Username == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot complete this job submission as it was submitted by you." });
            }

            User submitUser = _userRepository.GetUser(jobSubmission.Username);
            if (status != null && status == "accept")
            {
                jobSubmission.State = JobState.Accepted;
                jobSubmission.UpdatedDate = DateTime.UtcNow;
                if (submitUser != null)
                {
                    Settings settings = _settingsRepository.GetSettings(submitUser, false);

                    int pointsEarned = GetPointsFromJobTitle(settings.JobTitle);
                    _pointsRepository.AddPoints(submitUser, pointsEarned);

                    if (settings.JobTitle != JobTitle.President)
                    {
                        settings.JobTitle += 1;
                        _settingsRepository.SetSetting(submitUser, nameof(settings.JobTitle), ((int)settings.JobTitle).ToString(), false);
                    }

                    string subject = "Congratulations! Your job submission was accepted!";
                    string emailTitle = "Your Job Submission";
                    string body = $"Congratulations! Your job submission \"{jobSubmission.Title}\" was accepted! We've credited you with <b>{Helper.GetFormattedPoints(pointsEarned)}</b>.";
                    string url = "/Jobs?expand=true#jobSubmissions";

                    _emailRepository.SendEmail(submitUser, subject, emailTitle, body, url, true);

                    _notificationsRepository.AddNotification(submitUser, $"Congratulations! Your job submission \"{jobSubmission.Title}\" was accepted! You've been credited with {Helper.GetFormattedPoints(pointsEarned)}.", NotificationLocation.Jobs, url);

                    AddJobAccomplishments(submitUser, settings);
                }
            }
            else if (status != null && status == "reject")
            {
                jobSubmission.State = JobState.Declined;
                jobSubmission.UpdatedDate = DateTime.UtcNow;

                _notificationsRepository.AddNotification(submitUser, $"Your job submission \"{jobSubmission.Title}\" was set as Not for Now.", NotificationLocation.Jobs, "/Jobs?expand=true#jobSubmissions");
            }

            _jobsRepository.UpdateJobSubmission(user, jobSubmission);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConfirmHelper(string status, Guid jobId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }
            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<JobSubmission> jobSubmissions = _jobsRepository.GetJobSubmissions(user, true);
            JobSubmission jobSubmission = jobSubmissions.Find(x => x.Id == jobId);
            if (jobSubmission == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Helper application is no longer open or no longer exists.", ShouldRefresh = true });
            }

            User submitUser = _userRepository.GetUser(jobSubmission.Username);
            if (status != null && status == "accept")
            {
                jobSubmission.State = JobState.Accepted;
                jobSubmission.UpdatedDate = DateTime.UtcNow;
                if (submitUser != null && submitUser.Role < UserRole.Helper)
                {
                    submitUser.Role = UserRole.Helper;
                    _userRepository.UpdateUser(submitUser);

                    string subject = "Congratulations! Your Helper application was accepted!";
                    string emailTitle = "Your Helper Application";
                    string body = "Congratulations! You've been accepted into our Helper program! Please make sure to read the Helper Guidelines by clicking the button below.";
                    string url = "/HelperGuidelines";

                    _emailRepository.SendEmail(submitUser, subject, emailTitle, body, url, true);

                    _notificationsRepository.AddNotification(submitUser, "Congratulations! You've been accepted into the Helper program! Please make sure to read the Helper Guidelines to learn about your responsibilities.", NotificationLocation.Jobs, url);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(submitUser, false);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.Helper))
                    {
                        _accomplishmentsRepository.AddAccomplishment(submitUser, AccomplishmentId.Helper, false);
                    }
                }
            }
            else if (status != null && status == "reject")
            {
                jobSubmission.State = JobState.Declined;
                jobSubmission.UpdatedDate = DateTime.UtcNow;
                if (submitUser != null && submitUser.Role < UserRole.Helper)
                {
                    string subject = "Sorry! Your Helper application was declined!";
                    string emailTitle = "Your Helper Application";
                    string body = "Unfortunately, we had to decline your application to become a Helper. Don't worry! You can reapply two weeks after the date of your last application.";
                    string url = "/Jobs/Helpers";

                    _emailRepository.SendEmail(submitUser, subject, emailTitle, body, url, true);

                    _notificationsRepository.AddNotification(submitUser, "Unfortunately, we had to decline your application to become a Helper. Don't worry! You can reapply two weeks after the date of your last application.", NotificationLocation.Jobs, url);
                }
            }

            _jobsRepository.UpdateJobSubmission(user, jobSubmission);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitHelper(string reason)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }
            if (user.Role > UserRole.Default)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You are not allowed to apply to become a Helper." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<JobSubmission> openHelperSubmissions = _jobsRepository.GetJobSubmissionsForUser(user).FindAll(x => x.Position == JobPosition.Helper);
            if (!CanSubmitHelperApplication(user, openHelperSubmissions.FirstOrDefault()))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You do not meet all the necessary criteria to submit a Helper application. Please try again later!" });
            }

            if (string.IsNullOrEmpty(reason))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must answer why you want to become a Helper to submit your application." });
            }

            int maxLength = 2500;
            if (reason.Length > maxLength)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your submission is too long. It should be {Helper.FormatNumber(maxLength)} characters or less." });
            }

            reason = Regex.Replace(reason, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            _jobsRepository.AddJobSubmission(user, JobPosition.Helper, string.Empty, reason, false);

            return Json(new Status { Success = true, SuccessMessage = "Your Helper application was successfully submitted! You'll be contacted once it is reviewed!" });
        }

        [HttpGet]
        public IActionResult GetJobsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            JobsViewModel jobsViewModel = GenerateModel(user, false, JobPosition.Attention);
            return PartialView("_JobsMainPartial", jobsViewModel);
        }

        [HttpGet]
        public IActionResult GetHelpersPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            JobsViewModel jobsViewModel = GenerateModel(user, true);
            return PartialView("_HelpersMainPartial", jobsViewModel);
        }

        private JobsViewModel GenerateModel(User user, bool isHelper, JobPosition selectedPosition = JobPosition.None, bool expand = false)
        {
            List<JobSubmission> jobSubmissions = _jobsRepository.GetJobSubmissionsForUser(user);
            Settings settings = _settingsRepository.GetSettings(user);
            JobTitle jobTitle = settings.JobTitle;
            JobsViewModel jobsViewModel = new JobsViewModel
            {
                Title = "Job Center",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.JobCenter,
                JobSubmissions = isHelper ? jobSubmissions.FindAll(x => x.Position == JobPosition.Helper) : jobSubmissions.FindAll(x => x.Position != JobPosition.Helper),
                JobTitle = jobTitle,
                JobTitlePoints = GetPointsFromJobTitle(jobTitle),
                SelectedPosition = selectedPosition,
                Expand = expand
            };
            if (isHelper)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                jobsViewModel.CanSubmitHelperApplication = CanSubmitHelperApplication(user, jobsViewModel.JobSubmissions.FirstOrDefault());
                jobsViewModel.Accomplishments = accomplishments.FindAll(x => x.Category > AccomplishmentCategory.Orange);
                jobsViewModel.HasClaimedTermsReward = accomplishments.Any(x => x.Id == AccomplishmentId.ClaimedTermsReward);
            }
            return jobsViewModel;
        }

        private bool CanSubmitHelperApplication(User user, JobSubmission jobSubmission)
        {
            if (user.Role > UserRole.Default)
            {
                return false;
            }

            if (jobSubmission != null)
            {
                double daysSinceHelperSubmission = (DateTime.UtcNow - jobSubmission.CreatedDate).TotalDays;
                if (jobSubmission.State == JobState.Open || jobSubmission.State == JobState.Accepted || (jobSubmission.State == JobState.Declined && daysSinceHelperSubmission < 14))
                {
                    return false;
                }
            }

            double accountAge = (DateTime.UtcNow - user.CreatedDate).TotalDays;
            if (accountAge < 14)
            {
                return false;
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Count(x => x.Category > AccomplishmentCategory.Orange) < 50)
            {
                return false;
            }

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ClaimedTermsReward))
            {
                return false;
            }

            return true;
        }

        private int GetPointsFromJobTitle(JobTitle title)
        {
            return (int)title * 500 + 2500;
        }

        private void AddJobAccomplishments(User user, Settings settings)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user, false);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.JobAccepted))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.JobAccepted, false);
            }
            if (settings.JobTitle >= JobTitle.Professional)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.JobProfessional))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.JobProfessional, false);
                }
            }
            if (settings.JobTitle >= JobTitle.JrManager)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.JobManager))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.JobManager, false);
                }
            }
            if (settings.JobTitle >= JobTitle.President)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.JobPresident))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.JobPresident, false);
                }
            }
        }
    }
}
