﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StoreMagicianController : Controller
    {
        private ILoginRepository _loginRepository;
        private IAuctionsRepository _auctionsRepository;
        private IMarketStallRepository _marketStallRepository;
        private IStoreRepository _storeRepository;
        private IBuyersClubRepository _buyersClubRepository;
        private IShowcaseRepository _showcaseRepository;
        private IJunkyardRepository _junkyardRepository;
        private IDonationsRepository _donationsRepository;
        private ISettingsRepository _settingsRepository;

        public StoreMagicianController(ILoginRepository loginRepository, IAuctionsRepository auctionsRepository, IMarketStallRepository marketStallRepository,
        IStoreRepository storeRepository, IBuyersClubRepository buyersClubRepository, IShowcaseRepository showcaseRepository, IJunkyardRepository junkyardRepository,
        IDonationsRepository donationsRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _auctionsRepository = auctionsRepository;
            _marketStallRepository = marketStallRepository;
            _storeRepository = storeRepository;
            _buyersClubRepository = buyersClubRepository;
            _showcaseRepository = showcaseRepository;
            _junkyardRepository = junkyardRepository;
            _donationsRepository = donationsRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index(string searchQuery = "", bool matchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "StoreMagician" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.StoreMagicianExpiryDate <= DateTime.UtcNow)
            {
                Messages.AddSnack("This location must be activated from the Sponsor Society.", true);
                return RedirectToAction("Index", "Plaza");
            }

            StoreMagicianViewModel storeMagicianViewModel = GenerateModel(user, searchQuery, matchExactSearch, category, condition);
            storeMagicianViewModel.UpdateViewData(ViewData);
            return View(storeMagicianViewModel);
        }

        private StoreMagicianViewModel GenerateModel(User user, string searchQuery = "", bool matchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            StoreMagicianViewModel storeMagicianViewModel = new StoreMagicianViewModel
            {
                Title = "Store Magician",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.StoreMagician,
                ExpiryDate = settings.StoreMagicianExpiryDate,
                SearchQuery = searchQuery,
                MatchExactSearch = matchExactSearch,
                SearchCategory = category,
                SearchCondition = condition
            };

            if (!string.IsNullOrEmpty(searchQuery))
            {
                List<StoreMagicianItem> items = new List<StoreMagicianItem>();
                List<Auction> auctions = _auctionsRepository.GetAllAuctions(searchQuery, matchExactSearch, category, condition);
                foreach (Auction auction in auctions)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(auction.AuctionItem);
                    item.Cost = auction.HighestBid;
                    item.Location = "Auction House";
                    item.Url = $"/Auction?id={auction.Id}";
                    items.Add(item);
                }

                List<MarketStallItem> marketStallItems = _marketStallRepository.GetMarketStallItems(searchQuery, matchExactSearch, category, condition);
                foreach (MarketStallItem marketStallItem in marketStallItems)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(marketStallItem);
                    item.Cost = marketStallItem.Cost;
                    item.Location = "Market Stalls";
                    item.Url = $"/MarketStalls?id={marketStallItem.MarketStallId}#{marketStallItem.SelectionId}";
                    items.Add(item);
                }

                List<StoreItem> storeItems = _storeRepository.SearchStoreItems(searchQuery, matchExactSearch, condition, category);
                foreach (StoreItem storeItem in storeItems)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(storeItem);
                    item.Cost = storeItem.Cost;
                    item.Location = "General Store";
                    item.Url = $"/Store#{storeItem.Id}";
                    items.Add(item);
                }

                List<StoreItem> buyersClubItems = _buyersClubRepository.SearchStoreItems(searchQuery, matchExactSearch, condition, category);
                foreach (StoreItem buyersClubItem in buyersClubItems)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(buyersClubItem);
                    item.Cost = buyersClubItem.Cost;
                    item.Location = "Buyers Club";
                    item.Url = $"/BuyersClub#{buyersClubItem.Id}";
                    items.Add(item);
                }

                Item showcaseItem = _showcaseRepository.SearchShowcaseItem(searchQuery, matchExactSearch, condition, category);
                if (showcaseItem != null)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(showcaseItem);
                    item.Cost = showcaseItem.Points;
                    item.Location = "Timed Showcase";
                    item.Url = $"/Showcase";
                    items.Add(item);
                }

                List<Item> junkyardItems = _junkyardRepository.SearchItems(searchQuery, matchExactSearch, condition, category);
                foreach (Item junkyardItem in junkyardItems)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(junkyardItem);
                    item.Cost = junkyardItem.Points;
                    item.Location = "Junkyard";
                    item.Url = $"/Junkyard#{junkyardItem.SelectionId}";
                    items.Add(item);
                }

                List<Item> donationItems = _donationsRepository.SearchItems(searchQuery, matchExactSearch, condition, category);
                foreach (Item donationItem in donationItems)
                {
                    StoreMagicianItem item = new StoreMagicianItem();
                    item.Copy(donationItem);
                    item.Cost = 0;
                    item.Location = "Donation Zone";
                    item.Url = $"/Donations#{donationItem.SelectionId}";
                    items.Add(item);
                }

                storeMagicianViewModel.Items = items.OrderBy(x => x.Cost).Take(50).ToList();
            }

            return storeMagicianViewModel;
        }
    }
}
