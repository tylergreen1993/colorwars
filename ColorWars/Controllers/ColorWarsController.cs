﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ColorWarsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IColorWarsRepository _colorWarsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ColorWarsController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IColorWarsRepository colorWarsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _colorWarsRepository = colorWarsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "ColorWars" });
            }

            ColorWarsViewModel colorWarsViewModel = GenerateModel(user);
            colorWarsViewModel.UpdateViewData(ViewData);
            return View(colorWarsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Contribute()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            int contributionAmount = 500;

            if (user.Points < contributionAmount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int totalHoursSinceLastContribution = (int)(DateTime.UtcNow - settings.LastColorWarsContributeDate).TotalHours;
            if (totalHoursSinceLastContribution < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've contributed too recently. Try again {3 - totalHoursSinceLastContribution} hour{(3 - totalHoursSinceLastContribution == 1 ? "" : "s")}." });
            }

            if (_pointsRepository.SubtractPoints(user, contributionAmount))
            {
                ColorWarsColor teamColor = _colorWarsRepository.GetTeamColor(user);
                if (_colorWarsRepository.AddColorWarsSubmission(user, contributionAmount, teamColor))
                {
                    settings.LastColorWarsContributeDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastColorWarsContributeDate), settings.LastColorWarsContributeDate.ToString());

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ColorWarsContribute))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ColorWarsContribute);
                    }

                    string soundUrl = string.Empty;

                    List<ColorWarsSubmission> colorWarSubmissions = _colorWarsRepository.GetColorWarsSubmissions(teamColor);
                    if (colorWarSubmissions.Sum(x => x.Amount) >= Helper.WinningColorWarsPotSize())
                    {
                        if (_colorWarsRepository.CompleteColorWars(user, out int amountEarned))
                        {
                            user.Points += amountEarned;
                            _colorWarsRepository.AddColorWarsHistory(teamColor, amountEarned);

                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ColorWarsWinner))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ColorWarsWinner);
                            }

                            soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                            return Json(new Status { Success = true, SuccessMessage = $"Congratulations! You hit the {Helper.GetFormattedPoints(Helper.WinningColorWarsPotSize())} goal first and won Color Wars for your team! You successfully earned {Helper.GetFormattedPoints(amountEarned)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                        }
                    }

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully contributed {Helper.GetFormattedPoints(contributionAmount)} to your team's pot!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"You couldn't contribute to your team's pot." });
        }

        [HttpGet]
        public IActionResult GetColorWarsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ColorWarsViewModel colorWarsViewModel = GenerateModel(user);
            return PartialView("_ColorWarsMainPartial", colorWarsViewModel);
        }

        private ColorWarsViewModel GenerateModel(User user)
        {
            List<ColorWarsSubmission> colorWarsSubmissionsBlue = _colorWarsRepository.GetColorWarsSubmissions(ColorWarsColor.Blue);
            List<ColorWarsSubmission> colorWarsSubmissionsOrange = _colorWarsRepository.GetColorWarsSubmissions(ColorWarsColor.Orange);
            Settings settings = _settingsRepository.GetSettings(user);

            ColorWarsViewModel colorWarsViewModel = new ColorWarsViewModel
            {
                Title = "Color Wars",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                TeamColor = _colorWarsRepository.GetTeamColor(user),
                Team1Points = colorWarsSubmissionsOrange.Sum(x => x.Amount),
                Team2Points = colorWarsSubmissionsBlue.Sum(x => x.Amount),
                Team1Submissions = colorWarsSubmissionsOrange,
                Team2Submissions = colorWarsSubmissionsBlue,
                PointsGoal = Helper.WinningColorWarsPotSize(),
                ColorWarsHistories = _colorWarsRepository.GetColorWarsHistory(),
                CanContribute = user.Points >= 500 && (DateTime.UtcNow - settings.LastColorWarsContributeDate).TotalHours >= 3
            };

            return colorWarsViewModel;
        }
    }
}
