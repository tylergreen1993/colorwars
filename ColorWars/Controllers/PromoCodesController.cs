﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PromoCodesController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private ISecretGifterRepository _secretGifterRepository;
        private IClubRepository _clubRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public PromoCodesController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IPromoCodesRepository promoCodesRepository, ISecretGifterRepository secretGifterRepository, IClubRepository clubRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _promoCodesRepository = promoCodesRepository;
            _secretGifterRepository = secretGifterRepository;
            _clubRepository = clubRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string code)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"PromoCodes{(string.IsNullOrEmpty(code) ? "" : $"?code={code}")}" });
            }

            PromoCodesViewModel promoCodesViewModel = GenerateModel(user, code);
            promoCodesViewModel.UpdateViewData(ViewData);
            return View(promoCodesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Redeem(string promoCode)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            string soundUrl = string.Empty;

            if (string.IsNullOrEmpty(promoCode) || promoCode.Length != 8)
            {
                if (promoCode.Equals("wealthy", StringComparison.InvariantCultureIgnoreCase))
                {
                    return Json(new Status { Success = true, ReturnUrl = "/EliteShop" });
                }
                if (promoCode.Equals("strange", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (accomplishments.Exists(x => x.Id == AccomplishmentId.MazeUnlocked))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You've already gained access to the Maze." });
                    }

                    List<Card> cards = _itemsRepository.GetCards(user, true);
                    if (cards.Count < Helper.GetMaxDeckSize())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You cannot gain access to the Maze without {Helper.GetMaxDeckSize()} odd cards in your Deck." });
                    }
                    if (cards.Any(x => x.Amount % 2 == 0))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "Your Deck isn't odd enough to gain access to the Maze." });
                    }

                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MazeUnlocked);

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully gained access to the Maze!", ButtonText = "Head to the Maze!", ButtonUrl = "/SwapDeck/Maze", SoundUrl = soundUrl });
                }

                return Json(new Status { Success = false, ErrorMessage = "You entered an invalid promo code." });
            }

            PromoCode promo = _promoCodesRepository.GetPromoCode(promoCode);
            if (promo == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This promo code is either invalid, expired, or already used." });
            }
            if (promo.ExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "This promo code has already expired." });
            }

            List<Item> items = null;
            switch (promo.Effect)
            {
                case PromoEffect.AddStadiumBonus:
                    if (settings.StadiumBonusExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you already have an active Stadium Bonus." });
                    }
                    break;
                case PromoEffect.CardPack:
                    items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you don't have enough space in your bag to add a Card Pack." });
                    };
                    break;
                case PromoEffect.IncreaseBagSize:
                    if (settings.MaxBagSize >= Helper.GetMaxBagSize())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as your bag can't get any bigger." });
                    }
                    break;
                case PromoEffect.RandomItem:
                    items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you don't have enough space in your bag to add another item." });
                    };
                    break;
                case PromoEffect.JunkyardDiscount:
                    if (settings.JunkyardCouponPercent > 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you already have an active Junkyard discount." });
                    }
                    break;
                case PromoEffect.SecretGift:
                    items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you don't have enough space in your bag to add a Secret Gift." });
                    };
                    items = _secretGifterRepository.GetSecretGiftItems(user);
                    if (items.Count == 0 || items.Find(x => x.SelectionId == promo.ItemId) == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you don't have any Secret Gifts waiting for you." });
                    }
                    break;
                case PromoEffect.RepairShopDiscount:
                    if (settings.RepairCouponPercent > 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you already have an active Repair Shop discount." });
                    }
                    break;
                case PromoEffect.ClubTrial:
                    if (_clubRepository.GetCurrentMembership(user) != null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you already have an active {Resources.SiteName} Club membership." });
                    }
                    break;
                case PromoEffect.RareCardPack:
                    items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as you don't have enough space in your bag to add a Card Pack." });
                    };
                    break;
                case PromoEffect.StoreMagicianTrial:
                    if (settings.StoreMagicianExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as the Store Magician is already active for you." });
                    }
                    break;
                case PromoEffect.IncreaseMarketStallSize:
                    if (settings.MaxMarketStallSize >= Helper.GetMaxStallSize())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your promo code couldn't be redeemed as your Market Stall can't get any bigger." });
                    }
                    break;
            }

            promo = _promoCodesRepository.RedeemPromoCode(user, promoCode);
            if (promo == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already redeemed this promo code." });
            }

            Random rnd = new Random();
            string message = "";
            string buttonText = "";
            string buttonUrl = "";
            string infoMessage = "";
            switch (promo.Effect)
            {
                case PromoEffect.AddStadiumBonus:
                    int stadiumBonus = rnd.Next(4, 15) + 1;
                    AddStadiumBonus(user, stadiumBonus, settings);
                    message = $"Your promo code was successfully redeemed and you gained a {stadiumBonus}% Stadium Bonus for CPU matches for the next 10 minutes!";
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                    break;
                case PromoEffect.CardPack:
                    AddCardPack(user, false);
                    message = $"Your promo code was successfully redeemed and a Card Pack was added to your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.GainBigPoints:
                    int bigPoints = rnd.Next(2500, 10000);
                    _pointsRepository.AddPoints(user, bigPoints);
                    message = $"Your promo code was successfully redeemed and {Helper.GetFormattedPoints(bigPoints)} was added to your {Resources.Currency} on hand!";
                    break;
                case PromoEffect.GainPoints:
                    int smallPoints = rnd.Next(250, 1000);
                    _pointsRepository.AddPoints(user, smallPoints);
                    message = $"Your promo code was successfully redeemed and {Helper.GetFormattedPoints(smallPoints)} was added to your {Resources.Currency} on hand!";
                    break;
                case PromoEffect.IncreaseBagSize:
                    settings.MaxBagSize += 1;
                    _settingsRepository.SetSetting(user, nameof(settings.MaxBagSize), settings.MaxBagSize.ToString());
                    message = $"Your promo code was successfully redeemed and your bag size was increased to {settings.MaxBagSize} items!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.RandomItem:
                    Item item = null;
                    if (promo.ItemId != Guid.Empty)
                    {
                        item = _itemsRepository.GetAllItems(ItemCategory.All, null, true).Find(x => x.Id == promo.ItemId);
                    }
                    item = AddRandomItem(user, item, false);
                    message = $"Your promo code was successfully redeemed and the item \"{item.Name}\" was added to your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.ResetBankVisit:
                    settings.BankInterestDate = DateTime.UtcNow.AddDays(-1);
                    _settingsRepository.SetSetting(user, nameof(settings.BankInterestDate), settings.BankInterestDate.ToString());
                    message = $"Your promo code was successfully redeemed! You can collect your daily interest at the Bank again!";
                    buttonText = "Head to the Bank!";
                    buttonUrl = "/Bank";
                    break;
                case PromoEffect.ResetWizardVist:
                    settings.LastWizardDate = DateTime.UtcNow.AddHours(-3);
                    _settingsRepository.SetSetting(user, nameof(settings.LastWizardDate), settings.LastWizardDate.ToString());
                    message = $"Your promo code was successfully redeemed! You can visit the Young Wizard again!";
                    buttonText = "Head to the Young Wizard!";
                    buttonUrl = "/Wizard";
                    break;
                case PromoEffect.JunkyardDiscount:
                    int junkyardDiscount = rnd.Next(4, 15) + 1;
                    settings.JunkyardCouponPercent = junkyardDiscount;
                    _settingsRepository.SetSetting(user, nameof(settings.JunkyardCouponPercent), settings.JunkyardCouponPercent.ToString());
                    message = $"Your promo code was successfully redeemed and a {junkyardDiscount}% discount was applied to your next purchase at the Junkyard!";
                    buttonText = "Head to the Junkyard!";
                    buttonUrl = "/Junkyard";
                    break;
                case PromoEffect.SecretGift:
                    item = items.Find(x => x.SelectionId == promo.ItemId);
                    _itemsRepository.AddItem(user, item);
                    _secretGifterRepository.DeleteSecretGiftItem(item);
                    message = $"Your promo code was successfully redeemed and you got the item \"{item.Name}\" from your Secret Gifter!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.RepairShopDiscount:
                    int repairShopDiscount = rnd.Next(4, 15) + 1;
                    settings.RepairCouponPercent = repairShopDiscount;
                    _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());
                    message = $"Your promo code was successfully redeemed and a {repairShopDiscount}% discount was applied to your next purchase at the Repair Shop!";
                    buttonText = "Head to the Repair Shop!";
                    buttonUrl = "/Repair";
                    break;
                case PromoEffect.ResetSpinOfSuccessVisit:
                    settings.LastSpinSuccessDate = DateTime.UtcNow.AddHours(-3);
                    _settingsRepository.SetSetting(user, nameof(settings.LastSpinSuccessDate), settings.LastSpinSuccessDate.ToString());
                    message = $"Your promo code was successfully redeemed! You can spin the wheel again at the Spin of Success!";
                    buttonText = "Head to the Spin of Success!";
                    buttonUrl = "/SpinSuccess";
                    break;
                case PromoEffect.ClubTrial:
                    _clubRepository.AddClubMembership(user, 1, DateTime.UtcNow);
                    message = $"Your promo code was successfully redeemed! You now have a free month of {Resources.SiteName} Club membership!";
                    buttonText = $"Head to the {Resources.SiteName} Club!";
                    buttonUrl = "/Club";
                    break;
                case PromoEffect.ResetTreasureDiveVisit:
                    settings.LastTreasureDivePlayDate = DateTime.UtcNow.AddHours(-6);
                    _settingsRepository.SetSetting(user, nameof(settings.LastTreasureDivePlayDate), settings.LastTreasureDivePlayDate.ToString());
                    message = $"Your promo code was successfully redeemed! You can play Treasure Dive again!";
                    buttonText = "Head to Treasure Dive!";
                    buttonUrl = "/TreasureDive";
                    break;
                case PromoEffect.BigItem:
                    item = AddRandomItem(user, null, true);
                    message = $"Your promo code was successfully redeemed and the item \"{item.Name}\" was added to your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.RareCardPack:
                    AddCardPack(user, true);
                    message = $"Your promo code was successfully redeemed and a Card Pack - Rare was added to your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case PromoEffect.StoreMagicianTrial:
                    settings.StoreMagicianExpiryDate = DateTime.UtcNow.AddDays(3);
                    _settingsRepository.SetSetting(user, nameof(settings.StoreMagicianExpiryDate), settings.StoreMagicianExpiryDate.ToString());
                    message = $"Your promo code was successfully redeemed and you now have a free trial of the Store Magician for 3 days!";
                    buttonText = "Head to the Store Magician!";
                    buttonUrl = "/StoreMagician";
                    break;
                case PromoEffect.IncreaseMarketStallSize:
                    settings.MaxMarketStallSize += 1;
                    _settingsRepository.SetSetting(user, nameof(settings.MaxMarketStallSize), settings.MaxMarketStallSize.ToString());
                    message = $"Your promo code was successfully redeemed and your Market Stall size was increased to {settings.MaxMarketStallSize} items!";
                    buttonText = "Head to your Market Stall!";
                    buttonUrl = "/MarketStalls/Create";
                    break;
                default:
                    infoMessage = "Your promo code was redeemed but nothing happened!";
                    break;
            }

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.PromoRedeemed))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.PromoRedeemed);
            }

            _promoCodesRepository.AddPromoCodeHistory(user, promoCode, string.IsNullOrEmpty(message) ? infoMessage : message);

            soundUrl = string.Empty;
            if (!string.IsNullOrWhiteSpace(message))
            {
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);
            }

            return Json(new Status { Success = true, SuccessMessage = message, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetPromoCodesPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            PromoCodesViewModel promoCodesViewModel = GenerateModel(user, string.Empty);
            return PartialView("_PromoCodesMainPartial", promoCodesViewModel);
        }

        private PromoCodesViewModel GenerateModel(User user, string code)
        {
            PromoCodesViewModel promoCodesViewModel = new PromoCodesViewModel
            {
                Title = "Promo Codes",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.PromoCodes,
                PromoCodeHistories = _promoCodesRepository.GetPromoCodeHistory(user).Take(20).ToList(),
                Code = code
            };
            return promoCodesViewModel;
        }

        private void AddStadiumBonus(User user, int stadiumBonus, Settings settings)
        {
            settings.StadiumBonusPercent = stadiumBonus;
            settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(10);
            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());
        }

        private void AddCardPack(User user, bool isRare)
        {
            Random rnd = new Random();
            List<Item> allCardPacks = _itemsRepository.GetAllItems(ItemCategory.Pack);
            Item cardPack = isRare || rnd.Next(0, 5) == 1 ? isRare || rnd.Next(20) == 1 ? allCardPacks.Last() : allCardPacks[1] : allCardPacks.First();
            _itemsRepository.AddItem(user, cardPack);
        }

        private Item AddRandomItem(User user, Item item, bool isBig)
        {
            int maxPoints = 50000;
            int minPoints = 500;

            if (isBig)
            {
                maxPoints = 150000;
                minPoints = 2000;
            }

            if (item == null)
            {
                Random rnd = new Random();
                List<Item> allItems = _itemsRepository.GetAllItems().FindAll(x => x.Points <= maxPoints && x.Points >= minPoints);
                item = allItems[rnd.Next(allItems.Count)];
            }
            _itemsRepository.AddItem(user, item);

            return item;
        }
    }
}
