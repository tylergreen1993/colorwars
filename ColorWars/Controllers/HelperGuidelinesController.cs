﻿using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class HelperGuidelinesController : Controller
    {
        private ILoginRepository _loginRepository;

        public HelperGuidelinesController(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "HelperGuidelines" });
            }

            if (user.Role < UserRole.Helper)
            {
                return RedirectToAction("Index", "Home");
            }

            HelperGuidelinesViewModel helperGuidelinesViewModel = GenerateModel(user);
            helperGuidelinesViewModel.UpdateViewData(ViewData);
            return View(helperGuidelinesViewModel);
        }

        private HelperGuidelinesViewModel GenerateModel(User user)
        {
            HelperGuidelinesViewModel helperGuidelinesViewModel = new HelperGuidelinesViewModel
            {
                Title = "Helper Guidelines",
                User = user,
            };

            return helperGuidelinesViewModel;
        }
    }
}
