﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class AboutController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IStatisticsRepository _statisticsRepository;

        public AboutController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IStatisticsRepository statisticsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _statisticsRepository = statisticsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();

            AboutViewModel aboutViewModel = GenerateModel(user);
            aboutViewModel.UpdateViewData(ViewData);
            return View(aboutViewModel);
        }

        private AboutViewModel GenerateModel(User user)
        {
            List<Item> allItems = _itemsRepository.GetAllItems();
            allItems = allItems.FindAll(x => x.Category == ItemCategory.Deck || x.Category == ItemCategory.Enhancer || x.Category == ItemCategory.Crafting || x.Category == ItemCategory.Art);
            allItems.RemoveAll(x => x.Category == ItemCategory.Deck && x.Name.Length == 1);
            allItems.RemoveAll(x => x.Category == ItemCategory.Enhancer && (x.TotalUses <= 0 || !(x.Name.Contains("x", StringComparison.InvariantCultureIgnoreCase) || x.Name.Contains("+", StringComparison.InvariantCultureIgnoreCase))));
            allItems.RemoveAll(x => x.Category == ItemCategory.Crafting && (x.Name.Contains("-", StringComparison.InvariantCultureIgnoreCase) || x.Name.Contains("Pink", StringComparison.InvariantCultureIgnoreCase)));
            allItems.RemoveAll(x => x.Category == ItemCategory.Art && !(x.Name.Contains("Tropics", StringComparison.InvariantCultureIgnoreCase) || x.Name.Contains("Starry Night", StringComparison.InvariantCultureIgnoreCase) || x.Name.Contains("Grass Hills", StringComparison.InvariantCultureIgnoreCase) || x.Name.Contains("Sunrise", StringComparison.InvariantCultureIgnoreCase)));

            Random rnd = new Random();
            List<Item> displayItems = new List<Item>();
            while (displayItems.Count < 4)
            {
                Item newItem = allItems[rnd.Next(allItems.Count)];
                displayItems.Add(newItem);
                allItems.RemoveAll(x => x.Category == newItem.Category);
            }

            AboutViewModel aboutViewModel = new AboutViewModel
            {
                Title = "About",
                User = user,
                ShowTitle = false,
                DisplayItems = displayItems.OrderBy(x => x.Category).ToList(),
                Statistics = _statisticsRepository.GetStatistics()
            };

            return aboutViewModel;
        }
    }
}
