﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ColorWars.Controllers
{
    public class NotificationsController : Controller
    {
        private ILoginRepository _loginRepository;
        private INotificationsRepository _notificationsRepository;
        private ISettingsRepository _settingsRepository;

        public NotificationsController(ILoginRepository loginRepository, INotificationsRepository notificationsRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _notificationsRepository = notificationsRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Notifications" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            NotificationsViewModel notificationsViewModel = GenerateModel(user);
            notificationsViewModel.UpdateViewData(ViewData);
            return View(notificationsViewModel);
        }

        [HttpGet]
        public IActionResult GetUnreadNotifications()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false });
            }

            int unreadCount = _notificationsRepository.GetUnreadNotificationsCount(user);
            return Json(new Status { Success = true, Amount = unreadCount });
        }

        [HttpGet]
        public IActionResult GetUnreadCount()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false });
            }

            UnreadCount unreadCount = _notificationsRepository.GetUnreadCount(user);
            return Json(new Status { Success = true, Object = unreadCount });
        }

        private NotificationsViewModel GenerateModel(User user)
        {
            List<Notification> notifications = _notificationsRepository.GetNotifications(user);
            int unreadNotificationsCount = notifications.FindAll(x => x.SeenDate == DateTime.MinValue).Count;

            NotificationsViewModel notificationsViewModel = new NotificationsViewModel
            {
                Title = "Notifications",
                User = user,
                Notifications = notifications.Take(150).ToList(),
                UnreadNotificationsCount = unreadNotificationsCount
            };

            return notificationsViewModel;
        }
    }
}
