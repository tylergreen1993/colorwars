﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class WarehouseController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IWarehouseRepository _warehouseRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public WarehouseController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IWarehouseRepository warehouseRepository,
        IPointsRepository pointsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _warehouseRepository = warehouseRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Warehouse" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.WarehouseExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedWarehouse)
            {
                settings.CheckedWarehouse = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedWarehouse), settings.CheckedWarehouse.ToString());
            }

            WarehouseViewModel warehouseViewModel = GenerateModel(user);
            warehouseViewModel.UpdateViewData(ViewData);
            return View(warehouseViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyCompanion(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.WarehouseExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Warehouse has already closed.", ShouldRefresh = true });
            }

            List<Item> companions = _itemsRepository.GetAllItems(ItemCategory.Companion);
            Item companion = companions.Find(x => x.Id == itemId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Companion doesn't exist." });
            }

            if (user.Points < companion.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            int totalMinutesSinceLastMaxPurchase = (int)(DateTime.UtcNow - settings.WarehouseMaxPurchaseDate).TotalMinutes;
            if (totalMinutesSinceLastMaxPurchase < 60)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought too many items from the Warehouse recently. Try again in {60 - totalMinutesSinceLastMaxPurchase} minute{(60 - totalMinutesSinceLastMaxPurchase == 1 ? "" : "s")}." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_pointsRepository.SubtractPoints(user, companion.Points))
            {
                _itemsRepository.AddItem(user, companion);

                settings.WarehouseRecentBuyCount++;
                if (settings.WarehouseRecentBuyCount == 3)
                {
                    settings.WarehouseRecentBuyCount = 0;
                    settings.WarehouseMaxPurchaseDate = DateTime.UtcNow;

                    _settingsRepository.SetSetting(user, nameof(settings.WarehouseMaxPurchaseDate), settings.WarehouseMaxPurchaseDate.ToString());

                }
                _settingsRepository.SetSetting(user, nameof(settings.WarehouseRecentBuyCount), settings.WarehouseRecentBuyCount.ToString());

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully bought the Companion Card \"{companion.Name}\" for {Helper.GetFormattedPoints(companion.Points)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You weren't able to buy this Companion Card." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyBulk(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.WarehouseExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Warehouse has already closed.", ShouldRefresh = true });
            }

            List<StoreItem> items = _warehouseRepository.GetBulkItems(user);
            StoreItem bulkItem = items.Find(x => x.Id == itemId);
            if (items == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item isn't currently available." });
            }

            if (user.Points < bulkItem.Cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            int totalMinutesSinceLastMaxPurchase = (int)(DateTime.UtcNow - settings.WarehouseMaxPurchaseDate).TotalMinutes;
            if (totalMinutesSinceLastMaxPurchase < 60)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought too many items from the Warehouse recently. Try again in {60 - totalMinutesSinceLastMaxPurchase} minute{(60 - totalMinutesSinceLastMaxPurchase == 1 ? "" : "s")}." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count + 4 >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add 5 more items." });
            }

            Item item = _itemsRepository.GetAllItems().Find(x => x.Id == bulkItem.Id);
            if (_pointsRepository.SubtractPoints(user, bulkItem.Cost))
            {
                for (int i = 0; i < 5; i++)
                {
                    _itemsRepository.AddItem(user, item);
                }

                settings.WarehouseRecentBuyCount++;
                if (settings.WarehouseRecentBuyCount == 3)
                {
                    settings.WarehouseRecentBuyCount = 0;
                    settings.WarehouseMaxPurchaseDate = DateTime.UtcNow;

                    _settingsRepository.SetSetting(user, nameof(settings.WarehouseMaxPurchaseDate), settings.WarehouseMaxPurchaseDate.ToString());

                }
                _settingsRepository.SetSetting(user, nameof(settings.WarehouseRecentBuyCount), settings.WarehouseRecentBuyCount.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BulkPurchase))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BulkPurchase);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully bought 5 of the item \"{bulkItem.Name}\" for {Helper.GetFormattedPoints(bulkItem.Cost)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You weren't able to buy these items." });
        }

        [HttpGet]
        public IActionResult GetWarehousePartialView(bool isBulk)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.WarehouseExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Warehouse has already closed." });
            }

            WarehouseViewModel warehouseViewModel = GenerateModel(user);
            if (isBulk)
            {
                return PartialView("_WarehouseMainPartial", warehouseViewModel);
            }
            return PartialView("_WarehouseCompanionsPartial", warehouseViewModel);
        }

        private WarehouseViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            List<StoreItem> bulkItems = _warehouseRepository.GetBulkItems(user);
            if (bulkItems.Count == 0 || bulkItems.First().CreatedDate < settings.WarehouseExpiryDate.AddDays(-1))
            {
                _warehouseRepository.GenerateNewItems(user);
                bulkItems = _warehouseRepository.GetBulkItems(user);
            }

            WarehouseViewModel warehouseViewModel = new WarehouseViewModel
            {
                Title = "Warehouse",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Warehouse,
                BulkItems = bulkItems,
                Companions = _itemsRepository.GetAllItems(ItemCategory.Companion).OrderBy(x => x.Points).ThenBy(x => x.Name).ToList(),
                CanBuy = (DateTime.UtcNow - settings.WarehouseMaxPurchaseDate).TotalHours >= 1,
                ExpiryDate = settings.WarehouseExpiryDate
            };

            return warehouseViewModel;
        }
    }
}
