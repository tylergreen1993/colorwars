﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class BeyondWorldController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IColorWarsRepository _colorWarsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public BeyondWorldController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        IColorWarsRepository colorWarsRepository, IPointsRepository pointsRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _colorWarsRepository = colorWarsRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"BeyondWorld" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                Messages.AddSnack("You haven't unlocked the entrance to the Beyond World yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel == 8)
            {
                Messages.AddSnack("You've already closed the entrance to the Beyond World.", true);
                return RedirectToAction("Index", "Plaza");
            }

            BeyondWorldViewModel beyondWorldViewModel = GenerateModel(user);
            beyondWorldViewModel.UpdateViewData(ViewData);
            return View(beyondWorldViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SayName(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this at this time." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already given your name." });
            }

            if (!username.Equals(user.Username, System.StringComparison.InvariantCultureIgnoreCase) && !username.Equals(user.GetFriendlyName(), System.StringComparison.InvariantCultureIgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "That is not your name." });
            }

            settings.BeyondWorldLevel = 1;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyBeyondCard()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 2)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot buy a Beyond Card at this time." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            ColorWarsColor color = _colorWarsRepository.GetTeamColor(user);

            Item beyondCard = _itemsRepository.GetAllItems(ItemCategory.Beyond, null, true).Find(x => x.Name.Contains(color.ToString()));
            if (beyondCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "A Beyond Card could not be bought." });
            }

            if (user.Points < beyondCard.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_itemsRepository.AddItem(user, beyondCard))
            {
                _pointsRepository.SubtractPoints(user, beyondCard.Points);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{beyondCard.Name}\" was successfully added to your bag!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Beyond Card could not bought." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveBeyondCards()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 2)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot give Beyond Cards at this time." });
            }

            List<Beyond> beyondCards = _itemsRepository.GetBeyondCards(user);
            Beyond blueCard = beyondCards.Find(x => x.Color == CardColor.Blue);
            Beyond orangeCard = beyondCards.Find(x => x.Color == CardColor.Orange);

            if (blueCard == null || orangeCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You need both an Orange and Blue Beyond Card to continue." });
            }

            _itemsRepository.RemoveItem(user, blueCard);
            _itemsRepository.RemoveItem(user, orangeCard);

            settings.BeyondWorldLevel = 3;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult ChoosePath(BeyondWorldPath path)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 4)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot choose a path at this time." });
            }

            if (path == BeyondWorldPath.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a path." });
            }

            settings.BeyondWorldPath = path;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldPath), settings.BeyondWorldPath.ToString());

            settings.BeyondWorldLevel++;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult HeadToFinalRoom()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 6)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this at this time." });
            }

            settings.BeyondWorldLevel = 7;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 7)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot take an item at this time." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = GetItems(settings).Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot take this item." });
            }

            if (_itemsRepository.AddItem(user, item))
            {
                List<Guid> itemIds = string.IsNullOrEmpty(settings.TakenBeyondWorldItems) ? new List<Guid>() : settings.TakenBeyondWorldItems.Split("|").Select(Guid.Parse).ToList();
                itemIds.Add(item.Id);
                settings.TakenBeyondWorldItems = string.Join("|", itemIds);
                _settingsRepository.SetSetting(user, nameof(settings.TakenBeyondWorldItems), settings.TakenBeyondWorldItems);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = "The item was successfully added to your bag!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item could not be added to your bag at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CloseEntrance()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BeyondWorldLevel != 7)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot close the Beyond World at this time." });
            }

            settings.BeyondWorldLevel++;
            _settingsRepository.SetSetting(user, nameof(settings.BeyondWorldLevel), settings.BeyondWorldLevel.ToString());

            Messages.AddSnack("The Young Wizard fired a spell into the air and successfully closed the entrance to the Beyond World!");

            return Json(new Status { Success = true, ReturnUrl = "/Plaza" });
        }

        [HttpGet]
        public IActionResult GetBeyondWorldPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            BeyondWorldViewModel beyondWorldViewModel = GenerateModel(user, true);

            if (beyondWorldViewModel.Level == 0 || beyondWorldViewModel.Level == 1)
            {
                return PartialView("_BeyondWorldLevel0Partial", beyondWorldViewModel);
            }
            if (beyondWorldViewModel.Level == 2)
            {
                return PartialView("_BeyondWorldLevel2Partial", beyondWorldViewModel);
            }
            if (beyondWorldViewModel.Level == 3)
            {
                return PartialView("_BeyondWorldLevel3Partial", beyondWorldViewModel);
            }
            if (beyondWorldViewModel.Level == 4)
            {
                return PartialView("_BeyondWorldLevel4Partial", beyondWorldViewModel);
            }
            if (beyondWorldViewModel.Level == 5)
            {
                return PartialView("_BeyondWorldLevel5Partial", beyondWorldViewModel);
            }
            if (beyondWorldViewModel.Level == 6)
            {
                return PartialView("_BeyondWorldLevel6Partial", beyondWorldViewModel);
            }

            return PartialView("_BeyondWorldLevel7Partial", beyondWorldViewModel);
        }

        private BeyondWorldViewModel GenerateModel(User user, bool isReload = false)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            BeyondWorldViewModel beyondWorldViewModel = new BeyondWorldViewModel
            {
                Title = "Beyond World",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.BeyondWorld,
                Level = settings.BeyondWorldLevel,
                ShowAnimation = !isReload
            };

            if (beyondWorldViewModel.Level == 2)
            {
                beyondWorldViewModel.Color = _colorWarsRepository.GetTeamColor(user);
                beyondWorldViewModel.BeyondCard = _itemsRepository.GetAllItems(ItemCategory.Beyond, null, true).Find(x => x.Name.Contains(beyondWorldViewModel.Color.ToString()));
                beyondWorldViewModel.BeyondCards = _itemsRepository.GetBeyondCards(user);
            }
            if (beyondWorldViewModel.Level == 5)
            {
                beyondWorldViewModel.Deck = _itemsRepository.GetCards(user, true);
                beyondWorldViewModel.Path = settings.BeyondWorldPath;
            }
            if (beyondWorldViewModel.Level == 7)
            {
                List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
                beyondWorldViewModel.Path = settings.BeyondWorldPath;
                beyondWorldViewModel.HonorPathUsers = _settingsRepository.GetUsersByNameAndValue(nameof(settings.BeyondWorldPath), BeyondWorldPath.Honor.ToString());
                beyondWorldViewModel.GreedPathUsers = _settingsRepository.GetUsersByNameAndValue(nameof(settings.BeyondWorldPath), BeyondWorldPath.Greed.ToString());
                beyondWorldViewModel.Items = GetItems(settings);
            }

            return beyondWorldViewModel;
        }

        private List<Item> GetItems(Settings settings)
        {
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, null, true).FindAll(x => (x.Category == ItemCategory.Theme && x.Name == "Neon Theme") || (x.Category == ItemCategory.Fairy && x.Name == "Purple Fairy") || (x.Category == ItemCategory.Art && x.Name == "Beyond World"));
            List<Guid> takenItems = string.IsNullOrEmpty(settings.TakenBeyondWorldItems) ? new List<Guid>() : settings.TakenBeyondWorldItems.Split("|").Select(Guid.Parse).ToList();
            items.RemoveAll(x => takenItems.Any(y => y == x.Id));
            return items;
        }
    }
}
