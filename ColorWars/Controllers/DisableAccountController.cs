﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;

namespace ColorWars.Controllers
{
    public class DisableAccountController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;

        public DisableAccountController(ILoginRepository loginRepository, IUserRepository userRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "DisableAccount" });
            }

            DisableAccountViewModel disableAccountViewModel = GenerateModel(user);
            disableAccountViewModel.UpdateViewData(ViewData);
            return View(disableAccountViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Disable(InactiveAccountReason reason, string password)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.InactiveType == InactiveAccountType.Disabled)
            {
                return Json(new Status { Success = false, ErrorMessage = "This account is already disabled." });
            }

            if (reason <= InactiveAccountReason.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a reason for disabling your account." });
            }

            if (string.IsNullOrEmpty(password))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide your password to disable your account." });
            }

            if (!_userRepository.IsPasswordValid(user, password))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your password isn't right." });
            }

            _userRepository.DisableUser(user, reason);

            return Json(new Status { Success = true }); 
        }

        private DisableAccountViewModel GenerateModel(User user)
        {
            DisableAccountViewModel disableAccountViewModel = new DisableAccountViewModel
            {
                Title = "Disable Your Account",
                User = user
            };

            return disableAccountViewModel;
        }
    }
}
