﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Web;
using ColorWars.Classes;
using System.Linq;
using System.Drawing;
using Microsoft.AspNetCore.Http;
using ImageMagick;
using System.IO;

namespace ColorWars.Controllers
{
    public class PreferencesController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;
        private IRegisterRepository _registerRepository;
        private IBlockedRepository _blockedRepository;
        private IFeedRepository _feedRepository;
        private IClubRepository _clubRepository;
        private IBadgeRepository _badgeRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IGoogleStorageRepository _googleStorageRepository;
        private ISoundsRepository _soundsRepository;
        private IEmailRepository _emailRepository;

        public PreferencesController(ILoginRepository loginRepository, IUserRepository userRepository, IRegisterRepository registerRepository,
        IBlockedRepository blockedrepository, IFeedRepository feedRepository, IClubRepository clubRepository,
        IBadgeRepository badgeRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IGoogleStorageRepository googleStorageRepository, ISoundsRepository soundsRepository, IEmailRepository emailRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
            _registerRepository = registerRepository;
            _blockedRepository = blockedrepository;
            _feedRepository = feedRepository;
            _clubRepository = clubRepository;
            _badgeRepository = badgeRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _googleStorageRepository = googleStorageRepository;
            _soundsRepository = soundsRepository;
            _emailRepository = emailRepository;
        }

        public IActionResult Index(string blockUser)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Preferences" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            PreferencesViewModel preferencesViewModel = GenerateModel(user, blockUser);
            preferencesViewModel.UpdateViewData(ViewData);
            return View(preferencesViewModel);
        }

        public IActionResult Notepad()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Preferences/Notepad" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            PreferencesViewModel preferencesViewModel = GenerateModel(user);
            preferencesViewModel.UpdateViewData(ViewData);
            return View(preferencesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateProfile(Gender gender, string firstName, string location, string bio, string userColor, IFormFile file, BadgeType activeBadge)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            firstName ??= string.Empty;
            location ??= string.Empty;
            bio ??= string.Empty;
            userColor ??= string.Empty;

            Settings settings = _settingsRepository.GetSettings(user);

            if (user.Gender == gender && (user.FirstName?.Trim() ?? string.Empty) == firstName.Trim() && (user.Location?.Trim() ?? string.Empty) == location.Trim() && (user.Bio?.Trim() ?? string.Empty) == bio.Trim() && userColor == user.Color && settings.ActiveBadge == activeBadge && file == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "No changes were made." });
            }
            if (firstName.Length > 15)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your first name cannot be more than 15 characters." });
            }
            if (location.Length > 35)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your location cannot be more than 35 characters." });
            }
            if (bio.Length > 350)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bio cannot be more than 350 characters." });
            }

            if (!string.IsNullOrEmpty(userColor))
            {
                try
                {
                    _ = ColorTranslator.FromHtml(userColor);
                }
                catch (Exception)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must enter a valid color." });
                }
            }

            if (file != null)
            {
                if (!string.IsNullOrEmpty(user.DisplayPicUrl))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You already have an active display picture. Please remove that one before adding a new one." });
                }

                if (file.Length > 1000000)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)file.Length / 1000, 1)} KB. Your display picture cannot be greater than 250 KB." });
                }

                if (file.ContentType != "image/png" && file.ContentType != "image/jpg" && file.ContentType != "image/jpeg" && file.ContentType != "image/gif")
                {
                    return Json(new Status { Success = false, ErrorMessage = "This is not a valid image type." });
                }

                using (Stream fileStream = file.OpenReadStream())
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    memoryStream.Position = 0;

                    ImageOptimizer optimizer = new ImageOptimizer
                    {
                        OptimalCompression = true,
                        IgnoreUnsupportedFormats = true
                    };
                    optimizer.Compress(memoryStream);

                    if (memoryStream.Length > 250000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)memoryStream.Length / 1000, 1)} KB after being compressed. Your display picture cannot be greater than 250 KB." });
                    }

                    string extension = file.ContentType == "image/png" ? "png" : file.ContentType == "image/jpg" ? "jpg" : file.ContentType == "image/jpeg" ? "jpeg" : "gif";
                    string fileName = $"{user.Username}-display-{Guid.NewGuid()}.{extension}";

                    string url = _googleStorageRepository.UploadFileAsync(memoryStream, fileName, file.ContentType).Result;
                    if (string.IsNullOrEmpty(url))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "The image could not be uploaded. Please try again." });
                    }

                    user.DisplayPicUrl = url;
                }
            }

            if (settings.ActiveBadge != activeBadge)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!_badgeRepository.GetAvailableBadges(user, settings, accomplishments, _clubRepository.GetCurrentMembership(user) != null).Any(x => x.Type == activeBadge))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked this badge yet." });
                }

                settings.ActiveBadge = activeBadge;
                _settingsRepository.SetSetting(user, nameof(settings.ActiveBadge), settings.ActiveBadge.ToString());
            }

            if (!string.IsNullOrEmpty(bio) || !string.IsNullOrEmpty(firstName) || !string.IsNullOrEmpty(location) || gender != Gender.None || !string.IsNullOrEmpty(userColor) || !string.IsNullOrEmpty(user.DisplayPicUrl))
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SetUpProfile))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SetUpProfile);
                }
            }

            bio = Regex.Replace(bio, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);

            user.Gender = gender;
            user.FirstName = firstName;
            user.Location = location;
            user.Bio = HttpUtility.HtmlEncode(bio);
            user.Color = userColor;
            _userRepository.UpdateUser(user);

            return Json(new Status { Success = true, SuccessMessage = $"Your profile changes were made successfully!", ButtonText = "Head to your profile!", ButtonUrl = "/Profile" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateSettings(string emailAddress, string allowEmailNotifications, string showUserMatchesFirst, string disableMessages, string disableCPUStatistics,
        string showGroupOnProfile, string showNewGroupMemberNotifications, string showGroupMessagesFirst, string showMarketStallOnProfile, PointsLocation pointsLocation,
        string cpuChallenges, string sendMarketStallPurchaseEmail, string showNewsOnFeed, string receiveCommentNotificationsOnAttentionHallPost,
        string showPopularAttentionHallPostOnFeed, string showNotificationsForGroupJoinRequests, string showCompanionsOnProfile, string showStadiumFireworksAnimation,
        string showUnreadGroupMessagesOnFeed, string receiveAuctionHouseBidNotifications, string allowPartnershipRequests, string allowUserMentionNotifications, string showDisposerConfirmation,
        string userChallenges, string allowReadReceipts, string allowSoundEffects, string enableChallengingCPUDifficulty, int batchNotificationEmailMinutes, string enableMessagesTab)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(emailAddress) || _userRepository.IsEmailAddressInvalid(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to fill in a valid email address field." });
            }

            bool allowEmailNotificationsEnabled = allowEmailNotifications == "on";
            bool allowUserMatchesFirstEnabled = showUserMatchesFirst == "on";
            bool disableMessagesEnabled = disableMessages == "on";
            bool disableCPUStatisticsEnabled = disableCPUStatistics == "on";
            bool allowGroupOnProfile = showGroupOnProfile == "on";
            bool allowNewGroupMemberNotifications = showNewGroupMemberNotifications == "on";
            bool allowGroupMessagesFirst = showGroupMessagesFirst == "on";
            bool allowMarketStallOnProfile = showMarketStallOnProfile == "on";
            bool allowCPUChallenges = cpuChallenges == "on";
            bool allowMarketStallPurchaseEmail = sendMarketStallPurchaseEmail == "on";
            bool allowShowNewsOnFeed = showNewsOnFeed == "on";
            bool allowReceiveCommentNotificationsOnAttentionHallPost = receiveCommentNotificationsOnAttentionHallPost == "on";
            bool allowShowPopularAttentionHallPostOnFeed = showPopularAttentionHallPostOnFeed == "on";
            bool allowShowNotificationsForGroupJoinRequests = showNotificationsForGroupJoinRequests == "on";
            bool allowShowCompanionsOnProfile = showCompanionsOnProfile == "on";
            bool allowShowStadiumFireworksAnimation = showStadiumFireworksAnimation == "on";
            bool allowShowUnreadGroupMessagesOnFeed = showUnreadGroupMessagesOnFeed == "on";
            bool allowReceiveAuctionHouseBidNotifications = receiveAuctionHouseBidNotifications == "on";
            bool allowPartnershipRequestsEnabled = allowPartnershipRequests == "on";
            bool allowUserMentionNotificationsEnabled = allowUserMentionNotifications == "on";
            bool allowShowDisposerConfirmation = showDisposerConfirmation == "on";
            bool allowUserChallenges = userChallenges == "on";
            bool allowReadReceiptsEnabled = allowReadReceipts == "on";
            bool allowSoundEffectsEnabled = allowSoundEffects == "on";
            bool allowChallengingCPUDifficultyEnabled = enableChallengingCPUDifficulty == "on";
            bool allowEnableMessagesTab = enableMessagesTab == "on";

            Settings settings = _settingsRepository.GetSettings(user);

            if (user.EmailAddress.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase) && user.AllowNotifications == allowEmailNotificationsEnabled && settings.ShowUserMatchesFirst == allowUserMatchesFirstEnabled &&
            settings.DisableMessages == disableMessagesEnabled && settings.OnlyShowUserMatchesStatistics == disableCPUStatisticsEnabled &&
            settings.ShowGroupOnProfile == allowGroupOnProfile && settings.NotificationsForNewGroupMembers == allowNewGroupMemberNotifications && settings.ShowGroupMessagesFirst == allowGroupMessagesFirst &&
            settings.PointsLocation == pointsLocation && settings.ReceiveStadiumChallenges == allowCPUChallenges && settings.ShowMarketStallOnProfile == allowMarketStallOnProfile &&
            settings.EmailsForMarketStallPurchases == allowMarketStallPurchaseEmail && settings.ShowNewsOnFeed == allowShowNewsOnFeed && settings.ReceiveAttentionHallPostsCommentsNotifications == allowReceiveCommentNotificationsOnAttentionHallPost &&
            settings.ShowTrendingAttentionHallPostsOnFeed == allowShowPopularAttentionHallPostOnFeed && settings.NotificationsForGroupJoinRequests == allowShowNotificationsForGroupJoinRequests && settings.EnableReadReceipts == allowReadReceiptsEnabled &&
            settings.ShowCompanionsOnProfile == allowShowCompanionsOnProfile && settings.ShowStadiumFireworksAnimation == allowShowStadiumFireworksAnimation && settings.ShowUnreadGroupMessagesOnFeed == allowShowUnreadGroupMessagesOnFeed &&
            settings.ReceiveAuctionBidNotifications == allowReceiveAuctionHouseBidNotifications && settings.AllowPartnershipRequests == allowPartnershipRequestsEnabled && settings.AllowUserMentionNotifications == allowUserMentionNotificationsEnabled &&
            settings.ShowDisposerConfirmation == allowShowDisposerConfirmation && settings.AllowUserChallenges == allowUserChallenges &&settings.EnableSoundEffects == allowSoundEffectsEnabled &&
            settings.EnableCPUChallengingDifficulty == allowChallengingCPUDifficultyEnabled && settings.BatchNotificationEmailMinutes == batchNotificationEmailMinutes && settings.CanShowMessagesTab == allowEnableMessagesTab)
            {
                return Json(new Status { Success = false, ErrorMessage = "No changes were made." });
            }
            if (!user.EmailAddress.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase) && !_registerRepository.IsEmailAvailable(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "That email address is already in use." });
            }

            if (!user.EmailAddress.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase) || user.AllowNotifications != allowEmailNotificationsEnabled)
            {
                if (user.EmailAddress != emailAddress)
                {
                    if (settings.VerifiedEmail)
                    {
                        settings.VerifiedEmail = false;
                        _settingsRepository.SetSetting(user, nameof(settings.VerifiedEmail), settings.VerifiedEmail.ToString());
                    }
                    _emailRepository.SendEmailVerification(user);
                }
                user.EmailAddress = emailAddress;
                user.AllowNotifications = allowEmailNotificationsEnabled;
                _userRepository.UpdateUser(user);
            }

            if (settings.ShowUserMatchesFirst != allowUserMatchesFirstEnabled)
            {
                settings.ShowUserMatchesFirst = allowUserMatchesFirstEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.ShowUserMatchesFirst), settings.ShowUserMatchesFirst.ToString());
            }
            if (settings.DisableMessages != disableMessagesEnabled)
            {
                settings.DisableMessages = disableMessagesEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.DisableMessages), settings.DisableMessages.ToString());
            }
            if (settings.OnlyShowUserMatchesStatistics != disableCPUStatisticsEnabled)
            {
                settings.OnlyShowUserMatchesStatistics = disableCPUStatisticsEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.OnlyShowUserMatchesStatistics), settings.OnlyShowUserMatchesStatistics.ToString());
            }
            if (settings.ShowGroupOnProfile != allowGroupOnProfile)
            {
                settings.ShowGroupOnProfile = allowGroupOnProfile;
                _settingsRepository.SetSetting(user, nameof(settings.ShowGroupOnProfile), settings.ShowGroupOnProfile.ToString());
            }
            if (settings.NotificationsForNewGroupMembers != allowNewGroupMemberNotifications)
            {
                settings.NotificationsForNewGroupMembers = allowNewGroupMemberNotifications;
                _settingsRepository.SetSetting(user, nameof(settings.NotificationsForNewGroupMembers), settings.NotificationsForNewGroupMembers.ToString());
            }
            if (settings.ShowGroupMessagesFirst != allowGroupMessagesFirst)
            {
                settings.ShowGroupMessagesFirst = allowGroupMessagesFirst;
                _settingsRepository.SetSetting(user, nameof(settings.ShowGroupMessagesFirst), settings.ShowGroupMessagesFirst.ToString());
            }
            if (settings.PointsLocation != pointsLocation)
            {
                settings.PointsLocation = pointsLocation;
                _settingsRepository.SetSetting(user, nameof(settings.PointsLocation), ((int)settings.PointsLocation).ToString());
            }
            if (settings.ReceiveStadiumChallenges != allowCPUChallenges)
            {
                settings.ReceiveStadiumChallenges = allowCPUChallenges;
                _settingsRepository.SetSetting(user, nameof(settings.ReceiveStadiumChallenges), settings.ReceiveStadiumChallenges.ToString());
            }
            if (settings.ShowMarketStallOnProfile != allowMarketStallOnProfile)
            {
                settings.ShowMarketStallOnProfile = allowMarketStallOnProfile;
                _settingsRepository.SetSetting(user, nameof(settings.ShowMarketStallOnProfile), settings.ShowMarketStallOnProfile.ToString());
            }
            if (settings.EmailsForMarketStallPurchases != allowMarketStallPurchaseEmail)
            {
                settings.EmailsForMarketStallPurchases = allowMarketStallPurchaseEmail;
                _settingsRepository.SetSetting(user, nameof(settings.EmailsForMarketStallPurchases), settings.EmailsForMarketStallPurchases.ToString());
            }
            if (settings.ShowNewsOnFeed != allowShowNewsOnFeed)
            {
                settings.ShowNewsOnFeed = allowShowNewsOnFeed;
                _settingsRepository.SetSetting(user, nameof(settings.ShowNewsOnFeed), settings.ShowNewsOnFeed.ToString());
            }
            if (settings.ReceiveAttentionHallPostsCommentsNotifications != allowReceiveCommentNotificationsOnAttentionHallPost)
            {
                settings.ReceiveAttentionHallPostsCommentsNotifications = allowReceiveCommentNotificationsOnAttentionHallPost;
                _settingsRepository.SetSetting(user, nameof(settings.ReceiveAttentionHallPostsCommentsNotifications), settings.ReceiveAttentionHallPostsCommentsNotifications.ToString());
            }
            if (settings.ShowTrendingAttentionHallPostsOnFeed != allowShowPopularAttentionHallPostOnFeed)
            {
                settings.ShowTrendingAttentionHallPostsOnFeed = allowShowPopularAttentionHallPostOnFeed;
                _settingsRepository.SetSetting(user, nameof(settings.ShowTrendingAttentionHallPostsOnFeed), settings.ShowTrendingAttentionHallPostsOnFeed.ToString());
            }
            if (settings.NotificationsForGroupJoinRequests != allowShowNotificationsForGroupJoinRequests)
            {
                settings.NotificationsForGroupJoinRequests = allowShowNotificationsForGroupJoinRequests;
                _settingsRepository.SetSetting(user, nameof(settings.NotificationsForGroupJoinRequests), settings.NotificationsForGroupJoinRequests.ToString());
            }
            if (settings.ShowCompanionsOnProfile != allowShowCompanionsOnProfile)
            {
                settings.ShowCompanionsOnProfile = allowShowCompanionsOnProfile;
                _settingsRepository.SetSetting(user, nameof(settings.ShowCompanionsOnProfile), settings.ShowCompanionsOnProfile.ToString());
            }
            if (settings.ShowStadiumFireworksAnimation != allowShowStadiumFireworksAnimation)
            {
                settings.ShowStadiumFireworksAnimation = allowShowStadiumFireworksAnimation;
                _settingsRepository.SetSetting(user, nameof(settings.ShowStadiumFireworksAnimation), settings.ShowStadiumFireworksAnimation.ToString());
            }
            if (settings.ShowUnreadGroupMessagesOnFeed != allowShowUnreadGroupMessagesOnFeed)
            {
                settings.ShowUnreadGroupMessagesOnFeed = allowShowUnreadGroupMessagesOnFeed;
                _settingsRepository.SetSetting(user, nameof(settings.ShowUnreadGroupMessagesOnFeed), settings.ShowUnreadGroupMessagesOnFeed.ToString());
            }
            if (settings.ReceiveAuctionBidNotifications != allowReceiveAuctionHouseBidNotifications)
            {
                settings.ReceiveAuctionBidNotifications = allowReceiveAuctionHouseBidNotifications;
                _settingsRepository.SetSetting(user, nameof(settings.ReceiveAuctionBidNotifications), settings.ReceiveAuctionBidNotifications.ToString());
            }
            if (settings.AllowPartnershipRequests != allowPartnershipRequestsEnabled)
            {
                settings.AllowPartnershipRequests = allowPartnershipRequestsEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.AllowPartnershipRequests), settings.AllowPartnershipRequests.ToString());
            }
            if (settings.AllowUserMentionNotifications != allowUserMentionNotificationsEnabled)
            {
                settings.AllowUserMentionNotifications = allowUserMentionNotificationsEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.AllowUserMentionNotifications), settings.AllowUserMentionNotifications.ToString());
            }
            if (settings.ShowDisposerConfirmation != allowShowDisposerConfirmation)
            {
                settings.ShowDisposerConfirmation = allowShowDisposerConfirmation;
                _settingsRepository.SetSetting(user, nameof(settings.ShowDisposerConfirmation), settings.ShowDisposerConfirmation.ToString());
            }
            if (settings.AllowUserChallenges != allowUserChallenges)
            {
                settings.AllowUserChallenges = allowUserChallenges;
                _settingsRepository.SetSetting(user, nameof(settings.AllowUserChallenges), settings.AllowUserChallenges.ToString());
            }
            if (settings.EnableReadReceipts != allowReadReceiptsEnabled)
            {
                settings.EnableReadReceipts = allowReadReceiptsEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.EnableReadReceipts), settings.EnableReadReceipts.ToString());
            }
            if (settings.EnableSoundEffects != allowSoundEffectsEnabled)
            {
                settings.EnableSoundEffects = allowSoundEffectsEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.EnableSoundEffects), settings.EnableSoundEffects.ToString());
            }
            if (settings.EnableCPUChallengingDifficulty != allowChallengingCPUDifficultyEnabled)
            {
                settings.EnableCPUChallengingDifficulty = allowChallengingCPUDifficultyEnabled;
                _settingsRepository.SetSetting(user, nameof(settings.EnableCPUChallengingDifficulty), settings.EnableCPUChallengingDifficulty.ToString());
            }
            if (settings.CanShowMessagesTab != allowEnableMessagesTab)
            {
                settings.CanShowMessagesTab = allowEnableMessagesTab;
                _settingsRepository.SetSetting(user, nameof(settings.CanShowMessagesTab), settings.CanShowMessagesTab.ToString());
            }
            if (settings.BatchNotificationEmailMinutes != batchNotificationEmailMinutes && batchNotificationEmailMinutes > 0 && batchNotificationEmailMinutes <= 360)
            {
                settings.BatchNotificationEmailMinutes = batchNotificationEmailMinutes;
                _settingsRepository.SetSetting(user, nameof(settings.BatchNotificationEmailMinutes), settings.BatchNotificationEmailMinutes.ToString());
            }

            return Json(new Status { Success = true, SuccessMessage = $"Your settings changes were made successfully!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateAdmin(string currentPassword, string password, string passwordVerify, string blockedUsername)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(blockedUsername) && string.IsNullOrEmpty(password) && string.IsNullOrEmpty(passwordVerify))
            {
                return Json(new Status { Success = false, ErrorMessage = "No changes were made." });
            }

            if (!string.IsNullOrEmpty(password) || !string.IsNullOrEmpty(passwordVerify))
            {
                if (!_userRepository.IsPasswordValid(user, currentPassword))
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your current password isn't right." });
                }
                if (password != passwordVerify)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your new passwords do not match." });
                }
                if (password.Length < 5)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your new password is too short." });
                }
                if (currentPassword == password)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot change your password to the same password." });
                }

                _userRepository.UpdateUser(user, password);
            }

            if (!string.IsNullOrEmpty(blockedUsername))
            {
                User blockedUser = _userRepository.GetUser(blockedUsername);
                if (blockedUser == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This user cannot be blocked as they don't exist." });
                }
                if (blockedUser.Username == user.Username)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot block yourself." });
                }
                if (blockedUser.Role >= UserRole.Admin)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot block a {Resources.SiteName} team member." });
                }
                if (_blockedRepository.HasBlockedUser(user, blockedUser))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You've already blocked this user." });
                }
                Settings settings = _settingsRepository.GetSettings(user);
                if (settings.PartnershipUsername.Equals(blockedUser.Username, StringComparison.InvariantCultureIgnoreCase))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You must end your partnership with {blockedUser.Username} before you can block them." });
                }

                _blockedRepository.AddBlocked(user, blockedUser);
            }

            return Json(new Status { Success = true, SuccessMessage = $"Your admin changes were made successfully!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveNotes(string notes)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (notes == null)
            {
                notes = string.Empty;
            }

            if (notes == settings.Notes)
            {
                return Json(new Status { Success = false, ErrorMessage = "There are no changes." });
            }

            if (notes.Length >= 2500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot have more than {Helper.FormatNumber(2500)} characters." });
            }

            settings.Notes = notes;
            _settingsRepository.SetSetting(user, nameof(settings.Notes), settings.Notes);

            return Json(new Status { Success = true, SuccessMessage = "Your notepad was successfully updated!" });
        }

        [HttpPost]
        public IActionResult RemoveDisplayPic()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(user.DisplayPicUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = "You currently don't have a set display picture." });
            }

            string fileName = user.DisplayPicUrl.Split("/").Last();

            user.DisplayPicUrl = string.Empty;
            if (_userRepository.UpdateUser(user))
            {
                _googleStorageRepository.DeleteFileAsync(fileName);

                return Json(new Status { Success = true, SuccessMessage = "Your display picture was successfully removed!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your display picture could not be removed. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClearActiveSessions()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<ActiveSession> activeSessions = _userRepository.GetActiveSessions(user);
            if (activeSessions.Count <= 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are the only active session." });
            }

            _userRepository.DeleteActiveSessions(user);

            return Json(new Status { Success = true, SuccessMessage = "You successfully logged out all other active sessions!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EnableEmailNotifications()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.AllowNotifications)
            {
                return Json(new Status { Success = true });
            }

            user.AllowNotifications = true;
            _userRepository.UpdateUser(user);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EnableSFX()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.EnableSoundEffects)
            {
                return Json(new Status { Success = true });
            }

            settings.EnableSoundEffects = true;
            _settingsRepository.SetSetting(user, nameof(settings.EnableSoundEffects), settings.EnableSoundEffects.ToString());

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SoundUrl = soundUrl });
        }

        [HttpPost]
        public IActionResult RemoveBlockedUser(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            User blockedUser = _userRepository.GetUser(username);
            if (blockedUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            _blockedRepository.DeleteFromBlocked(user, blockedUser);

            return Json(new Status { Success = true, SuccessMessage = $"{blockedUser.Username} was successfully unblocked!" });
        }

        [HttpPost]
        public IActionResult ResendEmailVerification()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VerifiedEmail)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your email address is already verified." });
            }

            _emailRepository.SendEmailVerification(user);

            return Json(new Status { Success = true, SuccessMessage = $"A verification email was sent to {user.EmailAddress}. Please check your spam folder." });
        }

        [HttpPost]
        public IActionResult RemoveHiddenFeedCategory(FeedCategory category)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            List<HiddenFeedCategory> hiddenFeedCategories = _feedRepository.GetHiddenFeedCategories(user);
            if (!hiddenFeedCategories.Any(x => x.FeedId == category))
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currently hiding this feed category." });
            }

            if (_feedRepository.DeleteHiddenFeedCategory(user, category))
            {
                return Json(new Status { Success = true, SuccessMessage = $"You can now successfully see {category.GetDisplayName()} posts on your feed!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This feed category cannot be shown. Please try again." });
        }

        [HttpGet]
        public IActionResult GetPointsLocation()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            return Json(new Status { Success = true, ReturnUrl = $"/{settings.PointsLocation.GetDisplayName()}" });
        }

        [HttpGet]
        public IActionResult GetPreferencesPartialView(bool loadNotes = false)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            PreferencesViewModel preferencesViewModel = GenerateModel(user);
            if (loadNotes)
            {
                return PartialView("_PreferencesNotepadPartial", preferencesViewModel);
            }

            return PartialView("_PreferencesMainPartial", preferencesViewModel);
        }

        private PreferencesViewModel GenerateModel(User user, string blockUser = "")
        {
            if (!string.IsNullOrEmpty(blockUser))
            {
                User blockedUser = _userRepository.GetUser(blockUser);
                if (blockUser != null)
                {
                    if (_blockedRepository.HasBlockedUser(user, blockedUser))
                    {
                        Messages.AddSnack($"You've already blocked {blockedUser.Username}!", true);
                    }
                    else
                    {
                        if (_blockedRepository.AddBlocked(user, blockedUser))
                        {
                            Messages.AddSnack($"You successfully blocked {blockedUser.Username}!");
                        }
                    }
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            List<Badge> availableBadges = _badgeRepository.GetAvailableBadges(user, settings, accomplishments, _clubRepository.GetCurrentMembership(user) != null);

            PreferencesViewModel preferencesViewModel = new PreferencesViewModel
            {
                Title = "Preferences",
                User = user,
                Settings = settings,
                BlockedUsers = _blockedRepository.GetBlocked(user),
                ActiveSessions = _userRepository.GetActiveSessions(user),
                HiddenFeedCategories = _feedRepository.GetHiddenFeedCategories(user),
                AvailableBadges = availableBadges
            };

            return preferencesViewModel;
        }
    }
}
