﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TakeOfferController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ITakeOfferRepository _takeOfferRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public TakeOfferController(ILoginRepository loginRepository, IPointsRepository pointsRepository, ITakeOfferRepository takeOfferRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _takeOfferRepository = takeOfferRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TakeOffer" });
            }

            TakeOfferViewModel takeOfferViewModel = GenerateModel(user);
            takeOfferViewModel.UpdateViewData(ViewData);
            return View(takeOfferViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastTakeOfferPlayDate).TotalHours;
            if (hoursSinceLastGame < 12)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Take the Offer recently. Try again in {12 - hoursSinceLastGame} hour{(12 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            if (position > 15 || position < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This card can't be revealed." });
            }

            List<OfferCard> offerCards = _takeOfferRepository.GetOfferCards(user);
            if (offerCards.Count >= 16 || offerCards.All(x => (DateTime.UtcNow - x.FlipDate).TotalHours >= 12))
            {
                offerCards = new List<OfferCard>();
            }
            if (offerCards.Any(x => x.Position == position))
            {
                return Json(new Status { Success = false, ErrorMessage = "This card has already been revealed." });
            }

            List<int> possibleValues = GetPossibleValues(offerCards);
            Random rnd = new Random();
            int points = possibleValues[rnd.Next(possibleValues.Count)];

            if (_takeOfferRepository.AddOfferCard(user, position, points))
            {
                if (offerCards.Count == 0)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (points == GetPossibleValues().Last())
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TakeOfferBadLuck))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TakeOfferBadLuck);
                        }
                    }
                }
                if (offerCards.Count == 15)
                {
                    _pointsRepository.AddPoints(user, points);

                    settings.LastTakeOfferPlayDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastTakeOfferPlayDate), settings.LastTakeOfferPlayDate.ToString());

                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (points == GetPossibleValues().Last())
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.GotTopOfferPrize))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.GotTopOfferPrize);
                        }

                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);
                    }
                    else if (points == GetPossibleValues().First())
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.UnluckyOffer))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.UnluckyOffer);
                        }
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully revealed the last card and earned {Helper.GetFormattedPoints(points)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The card could not be revealed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeOffer()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastTakeOfferPlayDate).TotalHours;
            if (hoursSinceLastGame < 12)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Take the Offer recently. Try again in {12 - hoursSinceLastGame} hour{(12 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            List<OfferCard> offerCards = _takeOfferRepository.GetOfferCards(user);
            if (offerCards.Count >= 16 || offerCards.All(x => (DateTime.UtcNow - x.FlipDate).TotalHours >= 12))
            {
                offerCards = new List<OfferCard>();
            }
            int offer = GetOffer(offerCards);
            if (offer <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have an offer to take." });
            }

            if (_pointsRepository.AddPoints(user, offer))
            {
                settings.LastTakeOfferPlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastTakeOfferPlayDate), settings.LastTakeOfferPlayDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TookOffer))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TookOffer);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShortAlt, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took the offer and earned {Helper.GetFormattedPoints(offer)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not take the offer." });
        }

        [HttpGet]
        public IActionResult GetTakeOfferPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TakeOfferViewModel takeOfferViewModel = GenerateModel(user);
            return PartialView("_TakeOfferMainPartial", takeOfferViewModel);
        }

        private TakeOfferViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            bool canPlay = (DateTime.UtcNow - settings.LastTakeOfferPlayDate).TotalHours >= 12;

            List<OfferCard> offerCards = GetOfferCards(user, canPlay);
            int offer = GetOffer(offerCards);

            TakeOfferViewModel takeOfferViewModel = new TakeOfferViewModel
            {
                Title = "Take the Offer",
                User = user,
                Offer = offer,
                OfferCards = offerCards,
                HighestValueRemaining = GetPossibleValues(offerCards).LastOrDefault(),
                OfferTaken = canPlay ? 0 : offerCards.Any(x => x.IsHidden) ? offer : offerCards.OrderBy(x => x.FlipDate).Last().Points,
                CanPlay = canPlay,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair
            };

            return takeOfferViewModel;
        }

        private List<OfferCard> GetOfferCards(User user, bool isNewGame)
        {
            List<OfferCard> offerCards = _takeOfferRepository.GetOfferCards(user);
            if (offerCards.Count > 16)
            {
                _takeOfferRepository.DeleteOfferCards(user);
            }
            if (offerCards.All(x => (DateTime.UtcNow - x.FlipDate).TotalHours >= 12) && isNewGame)
            {
                offerCards = new List<OfferCard>();
            }
            for (int i = 0; i < 16; i++)
            {
                OfferCard offerCard = offerCards.Find(x => x.Position == i);
                if (offerCard == null)
                {
                    offerCards.Add(new OfferCard
                    {
                        Position = i
                    });
                }
            }

            return offerCards.OrderBy(x => x.Position).ToList();
        }

        private int GetOffer(List<OfferCard> offerCards)
        {
            if (offerCards.All(x => x.IsHidden) || (offerCards.All(x => !x.IsHidden) && offerCards.Count == 15))
            {
                return 0;
            }

            List<int> possibleValues = GetPossibleValues(offerCards);
            if (possibleValues.Count == 0)
            {
                return 0;
            }

            return (int)Math.Round(possibleValues.Average());
        }

        private List<int> GetPossibleValues(List<OfferCard> offerCards = null)
        {
            List<int> possibleValues = new List<int> { 1, 5, 50, 100, 150, 250, 500, 750, 1000, 1500, 1750, 2000, 2500, 5000, 10000, 25000 };
            if (offerCards == null)
            {
                return possibleValues;
            }
            List<OfferCard> flippedOfferCards = offerCards.FindAll(y => !y.IsHidden);
            possibleValues.RemoveAll(x => flippedOfferCards.Any(y => y.Points == x));

            return possibleValues;
        }
    }
}
