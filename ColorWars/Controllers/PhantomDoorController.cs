﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PhantomDoorController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IRelicsRepository _relicsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public PhantomDoorController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        IPointsRepository pointsRepository, IRelicsRepository relicsRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _relicsRepository = relicsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"PhantomDoor" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 8)
            {
                Messages.AddSnack("You haven't unlocked The Phantom Door yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            if (settings.PhantomDoorLevel > 10)
            {
                Messages.AddSnack("You've already destroyed the Phantom Door.", true);
                return RedirectToAction("Index", "Plaza");
            }

            PhantomDoorViewModel phantomDoorViewModel = GenerateModel(user);
            phantomDoorViewModel.UpdateViewData(ViewData);
            return View(phantomDoorViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already opened the Phantom Door." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (!relics.Find(x => x.Level == 10).Earned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have the right Relics yet to open the Phantom Door." });
            }

            settings.PhantomDoorLevel = 1;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetNoteHint()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't need that hint at this time." });
            }

            if (user.Points < 1500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to get a hint." });
            }

            if (_pointsRepository.SubtractPoints(user, 1500))
            {
                settings.ReceivedPhantomDoorLanguageHint = true;
                _settingsRepository.SetSetting(user, nameof(settings.ReceivedPhantomDoorLanguageHint), settings.ReceivedPhantomDoorLanguageHint.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You asked for a hint and one was successfully inscribed on the note for {Helper.GetFormattedPoints(1500)}!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "You couldn't get a hint at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenSecondDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 2)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open this door at this time." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (!relics.Find(x => x.Level == 15).Earned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have the right Relic yet to open the door." });
            }

            settings.PhantomDoorLevel = 3;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveCard(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this at this time." });
            }

            Card card = _itemsRepository.GetCards(user).FindAll(x => x.Condition < ItemCondition.Unusable).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You do not have this {Resources.DeckCard} anymore." });
            }

            char character = card.Color.ToString().ToUpper().First();

            settings.PhantomDoorSpeltWord += character;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorSpeltWord), settings.PhantomDoorSpeltWord);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetSpeltWord()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this at this time." });
            }

            settings.PhantomDoorSpeltWord = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorSpeltWord), settings.PhantomDoorSpeltWord);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitSpeltWord()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this at this time." });
            }

            if (settings.PhantomDoorSpeltWord != "ROYBORG")
            {
                return Json(new Status { Success = false, ErrorMessage = "That name is not correct." });
            }

            settings.PhantomDoorLevel = 4;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenThirdDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 4)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open this door at this time." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (!relics.Find(x => x.Level == 20).Earned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have the right Relic yet to open the door." });
            }

            settings.PhantomDoorLevel = 5;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DefeatSpiritSisters()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open do this at this time." });
            }

            if (!settings.HasDefeatedEvena || !settings.HasDefeatedOddna)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't defeated both of the Spirit Sisters yet." });
            }

            settings.PhantomDoorLevel = 6;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenFourthDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 6)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open this door at this time." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (!relics.Find(x => x.Level == 30).Earned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have the right Relic yet to open the door." });
            }

            settings.PhantomDoorLevel = 7;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult PressButton()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 7 || DateTime.UtcNow.Minute != 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't press the button at this time." });
            }

            Item item = _itemsRepository.GetItems(user).Find(x => x.Category == ItemCategory.Jewel && x.Name == "Eternal");
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You need a special card you can make by crafting with you when you press the button." });
            }

            settings.PhantomDoorLevel = 8;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenFifthDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 8)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open this door at this time." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (!relics.Find(x => x.Level == 35).Earned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have the right Relic yet to open the door." });
            }

            settings.PhantomDoorLevel = 9;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't open take an item at this time." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = GetItems(settings).Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot take this item." });
            }

            if (_itemsRepository.AddItem(user, item))
            {
                List<Guid> itemIds = string.IsNullOrEmpty(settings.TakenPhantomItems) ? new List<Guid>() : settings.TakenPhantomItems.Split("|").Select(Guid.Parse).ToList();
                itemIds.Add(item.Id);
                settings.TakenPhantomItems = string.Join("|", itemIds);
                _settingsRepository.SetSetting(user, nameof(settings.TakenPhantomItems), settings.TakenPhantomItems);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = "The item was successfully added to your bag!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item could not be added to your bag at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DestroyDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.PhantomDoorLevel != 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't destroy the Phantom Door at this time." });
            }

            settings.PhantomDoorLevel = 11;
            _settingsRepository.SetSetting(user, nameof(settings.PhantomDoorLevel), settings.PhantomDoorLevel.ToString());

            Messages.AddSnack("Congratulations! You successfully destroyed the Phantom Door!");

            return Json(new Status { Success = true, ReturnUrl = "/Plaza" });
        }

        [HttpGet]
        public IActionResult GetPhantomDoorPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            PhantomDoorViewModel phantomDoorViewModel = GenerateModel(user);
            if (phantomDoorViewModel.Level == 0)
            {
                return PartialView("_PhantomDoorLevel0Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 1)
            {
                return PartialView("_PhantomDoorLevel1Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 2)
            {
                return PartialView("_PhantomDoorLevel2Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 3)
            {
                return PartialView("_PhantomDoorLevel3Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 4)
            {
                return PartialView("_PhantomDoorLevel4Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 5)
            {
                return PartialView("_PhantomDoorLevel5Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 6)
            {
                return PartialView("_PhantomDoorLevel6Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 7)
            {
                return PartialView("_PhantomDoorLevel7Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 8)
            {
                return PartialView("_PhantomDoorLevel8Partial", phantomDoorViewModel);
            }
            if (phantomDoorViewModel.Level == 9)
            {
                return PartialView("_PhantomDoorLevel9Partial", phantomDoorViewModel);
            }

            return PartialView("_PhantomDoorLevel10Partial", phantomDoorViewModel);
        }

        private PhantomDoorViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            PhantomDoorViewModel phantomDoorViewModel = new PhantomDoorViewModel
            {
                Title = "Phantom Door",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.PhantomDoor,
                Relics = relics,
                Level = settings.PhantomDoorLevel
            };

            if (settings.PhantomDoorLevel == 0)
            {
                phantomDoorViewModel.CanOpenDoor = relics.Any(x => x.Level == 10 && x.Earned);
            }
            else if (settings.PhantomDoorLevel == 1)
            {
                phantomDoorViewModel.CanReadNote = settings.HasLearntTranslation;
                phantomDoorViewModel.HasReceivedLanguageHint = settings.ReceivedPhantomDoorLanguageHint;
                phantomDoorViewModel.Cards = _itemsRepository.GetCards(user, true);
            }
            else if (settings.PhantomDoorLevel == 2)
            {
                phantomDoorViewModel.CanOpenDoor = relics.Any(x => x.Level == 15 && x.Earned);
            }
            else if (settings.PhantomDoorLevel == 3)
            {
                phantomDoorViewModel.Cards = _itemsRepository.GetCards(user).FindAll(x => x.Condition < ItemCondition.Unusable).ToList();
                phantomDoorViewModel.SpeltWord = settings.PhantomDoorSpeltWord;
            }
            else if (settings.PhantomDoorLevel == 4)
            {
                phantomDoorViewModel.CanOpenDoor = relics.Any(x => x.Level == 20 && x.Earned);
            }
            else if (settings.PhantomDoorLevel == 5)
            {
                phantomDoorViewModel.Cards = _itemsRepository.GetCards(user, true);
                phantomDoorViewModel.SpiritSisters = GenerateSpiritSisters(phantomDoorViewModel.Cards, settings);
            }
            else if (settings.PhantomDoorLevel == 6)
            {
                phantomDoorViewModel.CanOpenDoor = relics.Any(x => x.Level == 30 && x.Earned);
            }
            else if (settings.PhantomDoorLevel == 7)
            {
                phantomDoorViewModel.Item = _itemsRepository.GetItems(user).Find(x => x.Category == ItemCategory.Jewel && x.Name == "Eternal");
                phantomDoorViewModel.CanOpenDoor = DateTime.UtcNow.Minute == 30 && phantomDoorViewModel.Item != null;
            }
            else if (settings.PhantomDoorLevel == 8)
            {
                phantomDoorViewModel.CanOpenDoor = relics.Any(x => x.Level == 35 && x.Earned);
            }
            else if (settings.PhantomDoorLevel == 9)
            {
                phantomDoorViewModel.Accomplishments = _accomplishmentsRepository.GetAccomplishments(user).FindAll(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated || x.Id == AccomplishmentId.TowerFloorFive);
                phantomDoorViewModel.CanOpenDoor = phantomDoorViewModel.Accomplishments.Count == 2 && (DateTime.UtcNow - settings.LastPhantomBossStadiumChallengeDate).TotalMinutes >= 10;
            }
            else if (settings.PhantomDoorLevel == 10)
            {
                phantomDoorViewModel.Items = GetItems(settings);
            }

            return phantomDoorViewModel;
        }

        private List<SpiritSister> GenerateSpiritSisters(List<Card> cards, Settings settings)
        {
            return new List<SpiritSister>
            {
                new SpiritSister
                {
                    Name = "Evena",
                    Description = "The most even Spirit Sister.",
                    ImageUrl = "PhantomDoor/Evena.png",
                    ChallengeUrl = "ChallengeEvena",
                    CanChallenge = cards.All(x => x.Amount % 2 == 0),
                    HasDefeated = settings.HasDefeatedEvena
                },
                new SpiritSister
                {
                    Name = "Oddna",
                    Description = "The most odd Spirit Sister.",
                    ImageUrl = "PhantomDoor/Oddna.png",
                    ChallengeUrl = "ChallengeOddna",
                    CanChallenge = cards.All(x => x.Amount % 2 != 0),
                    HasDefeated = settings.HasDefeatedOddna
                }
            };
        }

        private List<Item> GetItems(Settings settings)
        {
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, null, true).FindAll(x => (x.Name == "Purple Fairy" && x.Category == ItemCategory.Fairy) || (x.Name == "Purple" && x.Category == ItemCategory.Seed) || (x.Name == "Astral Theme" && x.Category == ItemCategory.Theme) || (x.Name == "Card Pack - Rare" && x.Category == ItemCategory.Pack));
            List<Guid> takenItems = string.IsNullOrEmpty(settings.TakenPhantomItems) ? new List<Guid>() : settings.TakenPhantomItems.Split("|").Select(Guid.Parse).ToList();
            items.RemoveAll(x => takenItems.Any(y => y == x.Id));
            return items;
        }
    }
}
