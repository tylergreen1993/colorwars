﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class RewardsStoreController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IRewardsStoreRepository _rewardsStoreRepository;
        private IPointsRepository _pointsRepository;
        private IStocksRepository _stocksRepository;
        private IClubRepository _clubRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public RewardsStoreController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IRewardsStoreRepository rewardsStoreRepository, IStocksRepository stocksRepository, IClubRepository clubRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _rewardsStoreRepository = rewardsStoreRepository;
            _stocksRepository = stocksRepository;
            _clubRepository = clubRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"RewardsStore" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RewardsStoreExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedRewardsStore)
            {
                settings.CheckedRewardsStore = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedRewardsStore), settings.CheckedRewardsStore.ToString());
            }

            RewardsStoreViewModel rewardsStoreViewModel = GenerateModel(user);
            rewardsStoreViewModel.UpdateViewData(ViewData);
            return View(rewardsStoreViewModel);
        }

        public IActionResult Partnerships()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"RewardsStore" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RewardsStoreExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }
            if (settings.PartnershipEnabled)
            {
                return RedirectToAction("Index", "RewardsStore");
            }

            RewardsStoreViewModel rewardsStoreViewModel = GenerateModel(user);
            rewardsStoreViewModel.UpdateViewData(ViewData);
            return View(rewardsStoreViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Redeem(RewardId rewardId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RewardsStoreExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Rewards Store has already closed.", ShouldRefresh = true });
            }

            int points = _accomplishmentsRepository.GetAccomplishmentPoints(user);

            Reward reward = _rewardsStoreRepository.GetAllRewards().Find(x => x.Id == rewardId);
            if (reward == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This reward no longer exists." });
            }

            if (points < reward.Cost)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have enough points to redeem this reward." });
            }

            int totalMinutesSinceLastMaxClaimed = (int)(DateTime.UtcNow - settings.RewardsStoreMaxClaimedDate).TotalMinutes;
            if (totalMinutesSinceLastMaxClaimed < 60)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've claimed too many rewards recently. Try again in {60 - totalMinutesSinceLastMaxClaimed} minute{(60 - totalMinutesSinceLastMaxClaimed == 1 ? "" : "s")}." });
            }

            Random rnd = new Random();
            string successMessage = "";
            string buttonText = "";
            string buttonUrl = "";
            switch (reward.Id)
            {
                case RewardId.DoubleBankInterest:
                    if (settings.HasDoubleBankInterest)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You already have this reward." });
                    }
                    settings.HasDoubleBankInterest = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasDoubleBankInterest), settings.HasDoubleBankInterest.ToString());
                    buttonText = "Head to the Bank!";
                    buttonUrl = "/Bank";
                    break;
                case RewardId.DoubleCPUReward:
                    if (settings.HasDoubleStadiumCPUReward)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You already have this reward." });
                    }
                    settings.HasDoubleStadiumCPUReward = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasDoubleStadiumCPUReward), settings.HasDoubleStadiumCPUReward.ToString());
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                    break;
                case RewardId.ExtraLibraryVisit:
                    settings.LastLibraryKnowledgeUseDate = DateTime.UtcNow.AddHours(-12);
                    _settingsRepository.SetSetting(user, nameof(settings.LastLibraryKnowledgeUseDate), settings.LastLibraryKnowledgeUseDate.ToString());
                    buttonText = "Head to the Library!";
                    buttonUrl = "/Library";
                    break;
                case RewardId.ExtraSwapDeckVisit:
                    settings.LastSwapDeckDate = DateTime.UtcNow.AddDays(-1);
                    _settingsRepository.SetSetting(user, nameof(settings.LastSwapDeckDate), settings.LastSwapDeckDate.ToString());
                    buttonText = "Head to Swap Deck!";
                    buttonUrl = "/SwapDeck";
                    break;
                case RewardId.ExtraWishingStoneVisit:
                    settings.LastWishDate = DateTime.UtcNow.AddHours(-2);
                    _settingsRepository.SetSetting(user, nameof(settings.LastWishDate), settings.LastWishDate.ToString());
                    buttonText = "Head to the Wishing Stone!";
                    buttonUrl = "/WishingStone";
                    break;
                case RewardId.ExtraWizardVisit:
                    settings.LastWizardDate = DateTime.UtcNow.AddHours(-3);
                    _settingsRepository.SetSetting(user, nameof(settings.LastWizardDate), settings.LastWizardDate.ToString());
                    buttonText = "Head to the Young Wizard!";
                    buttonUrl = "/Wizard";
                    break;
                case RewardId.ImproveDeckCard:
                    List<Card> items = _itemsRepository.GetCards(user, false).FindAll(x => x.Amount < 100);
                    if (items.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"None of your {Resources.DeckCards} can be improved." });
                    }

                    Card card = items[rnd.Next(items.Count)];
                    Card newCard = _itemsRepository.GetAllCards().Find(x => x.Amount == card.Amount + 1);

                    newCard.CreatedDate = card.CreatedDate;
                    newCard.RepairedDate = card.RepairedDate;
                    newCard.Theme = card.Theme;
                    newCard.DeckType = card.DeckType;

                    _itemsRepository.RemoveItem(user, card);
                    _itemsRepository.AddItem(user, newCard);

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and your {Resources.DeckCard} \"{card.Name}\" became \"{newCard.Name}\"!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case RewardId.MaxBagSize:
                    if (settings.MaxBagSize >= Helper.GetMaxBagSize())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your bag can't get any bigger." });
                    }

                    settings.MaxBagSize = Math.Min(Helper.GetMaxBagSize(), settings.MaxBagSize + 15);
                    _settingsRepository.SetSetting(user, nameof(settings.MaxBagSize), settings.MaxBagSize.ToString());

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and your bag can now hold {Helper.FormatNumber(settings.MaxBagSize)} items!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case RewardId.MysteryPointsBag:
                    int amountPoints = rnd.Next(7500, 20000);
                    _pointsRepository.AddPoints(user, amountPoints);

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and the Mystery {Resources.Currency} Bag earned you... {Helper.GetFormattedPoints(amountPoints)}!";
                    break;
                case RewardId.MysteryStockBag:
                    List<Stock> stocks = _stocksRepository.GetAllStocks();
                    Stock stock = stocks[rnd.Next(stocks.Count)];
                    int amount = 100;
                    _stocksRepository.AddStock(user, stock, amount, false, true);

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and 100 shares of the stock \"{stock.Name}\" were added to your Stock Market portfolio!";
                    buttonText = "Head to your Portfolio!";
                    buttonUrl = "/StockMarket/Portfolio";
                    break;
                case RewardId.OneMonthClub:
                    ClubMembership clubMembership = _clubRepository.GetAllClubMemberships(user).LastOrDefault();
                    if (clubMembership == null)
                    {
                        _clubRepository.AddClubMembership(user, 1, DateTime.UtcNow);
                    }
                    else
                    {
                        _clubRepository.AddClubMembership(user, 1, clubMembership.EndDate);
                    }
                    buttonText = $"Head to the {Resources.SiteName} Club!";
                    buttonUrl = "/Club";
                    break;
                case RewardId.RepairAllDeck:
                    List<Card> deck = _itemsRepository.GetCards(user, true);
                    foreach (Card deckCard in deck)
                    {
                        deckCard.RepairedDate = DateTime.UtcNow;
                        _itemsRepository.UpdateItem(user, deckCard);
                    }
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case RewardId.StadiumBonus:
                    if (settings.StadiumBonusPercent > 0 && settings.StadiumBonusExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have an active Stadium Bonus." });
                    }
                    int stadiumBonus = rnd.Next(10) + 11;
                    settings.StadiumBonusPercent = stadiumBonus;
                    settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(15);

                    _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
                    _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and got a {stadiumBonus}% Stadium Bonus for CPU matches for the next 15 minutes!";
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                    break;
                case RewardId.ThemeChange:
                    List<Card> cards = _itemsRepository.GetCards(user).FindAll(x => x.Theme == ItemTheme.Default);
                    if (cards.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"All of your {Resources.DeckCards} already have a theme." });
                    }
                    Card rndCard = cards[rnd.Next(cards.Count)];

                    ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    ItemTheme theme = themes[rnd.Next(themes.Length)];
                    rndCard.Theme = theme;
                    _itemsRepository.UpdateItem(user, rndCard);

                    successMessage = $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and your {Resources.DeckCard} became \"{rndCard.Name}\"!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case RewardId.UnlockPartnership:
                    if (settings.PartnershipEnabled)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have unlocked the ability to form partnerships." });
                    }
                    settings.PartnershipEnabled = true;
                    _settingsRepository.SetSetting(user, nameof(settings.PartnershipEnabled), settings.PartnershipEnabled.ToString());

                    successMessage = "You successfully unlocked the ability to form partnerships!";
                    break;
            }

            settings.AccomplishmentPointsSpent += reward.Cost;
            _settingsRepository.SetSetting(user, nameof(settings.AccomplishmentPointsSpent), settings.AccomplishmentPointsSpent.ToString());

            settings.RewardsStoreTotalClaimed++;
            if (settings.RewardsStoreTotalClaimed == 3)
            {
                settings.RewardsStoreTotalClaimed = 0;
                settings.RewardsStoreMaxClaimedDate = DateTime.UtcNow;

                _settingsRepository.SetSetting(user, nameof(settings.RewardsStoreMaxClaimedDate), settings.RewardsStoreMaxClaimedDate.ToString());

            }
            _settingsRepository.SetSetting(user, nameof(settings.RewardsStoreTotalClaimed), settings.RewardsStoreTotalClaimed.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RewardsStorePurchase))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RewardsStorePurchase);
            }
            if (reward.Category > AccomplishmentCategory.Orange)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigRewardsStorePurchase))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigRewardsStorePurchase);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            successMessage = string.IsNullOrEmpty(successMessage) ? $"You successfully redeemed {Helper.FormatNumber(reward.Cost)} points and got the reward \"{reward.Name}\"!" : successMessage;

            return Json(new Status { Success = true, SuccessMessage = successMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetRewardsStorePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RewardsStoreExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Rewards Store has already closed." });
            }

            RewardsStoreViewModel rewardsStoreViewModel = GenerateModel(user);
            return PartialView("_RewardsStoreMainPartial", rewardsStoreViewModel);
        }

        private RewardsStoreViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            RewardsStoreViewModel rewardsStoreViewModel = new RewardsStoreViewModel
            {
                Title = "Rewards Store",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.RewardsStore,
                RewardPoints = _accomplishmentsRepository.GetAccomplishmentPoints(user),
                Rewards = _rewardsStoreRepository.GetAllRewards().FindAll(x => !x.IsHidden),
                CanRedeem = (DateTime.UtcNow - settings.RewardsStoreMaxClaimedDate).TotalHours >= 1,
                IsPartnershipEnabled = settings.PartnershipEnabled,
                ExpiryDate = settings.RewardsStoreExpiryDate
            };
            return rewardsStoreViewModel;
        }
    }
}
