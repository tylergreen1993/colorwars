﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SwapDeckController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISwapDeckRepository _swapDeckRepository;
        private ISettingsRepository _settingsRepository;
        private IMatchStateRepository _matchStateRepository;
        private IPointsRepository _pointsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public SwapDeckController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        ISwapDeckRepository swapDeckRepository, ISettingsRepository settingsRepository, IMatchStateRepository matchStateRepository,
        IPointsRepository pointsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _swapDeckRepository = swapDeckRepository;
            _settingsRepository = settingsRepository;
            _matchStateRepository = matchStateRepository;
            _pointsRepository = pointsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SwapDeck" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Maze", "SwapDeck");
            }

            SwapDeckViewModel swapDeckViewModel = GenerateModel(user);
            swapDeckViewModel.UpdateViewData(ViewData);
            return View(swapDeckViewModel);
        }

        public IActionResult Maze()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SwapDeck/Maze" });
            }

            SwapDeckViewModel swapDeckViewModel = GenerateModel(user);
            swapDeckViewModel.UpdateViewData(ViewData);
            return View(swapDeckViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Swap(Guid selectedDeckCard)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastSwapDeckPlay = (int)(DateTime.UtcNow - settings.LastSwapDeckDate).TotalHours;
            if (hoursSinceLastSwapDeckPlay < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've swapped a {Resources.DeckCard} too recently. Try again in {24 - hoursSinceLastSwapDeckPlay} hour{(24 - hoursSinceLastSwapDeckPlay == 1 ? "" : "s")}." });
            }

            if (selectedDeckCard == Guid.Empty)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You need to choose a {Resources.DeckCard} that you want to swap." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);
            Card selectedCard = deck.Find(x => x.SelectionId == selectedDeckCard);
            if (selectedCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This {Resources.DeckCard} doesn't appear to exist in your Deck." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't swap a {Resources.DeckCard} while you're in a Stadium match." });
            }

            string message;
            string soundUrl;

            Random rnd = new Random();
            int randomNumber = rnd.Next(100);
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (rnd.Next(3) == 1)
                {
                    if (settings.ActiveHoroscope == HoroscopeType.Good)
                    {
                        randomNumber = 80;
                    }
                    else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                    {
                        randomNumber = 60;
                    }
                }
            }

            if (settings.HasBetterSwapDeckLuck)
            {
                randomNumber = rnd.Next(3) == 1 ? 40 : 80;

                settings.HasBetterSwapDeckLuck = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSwapDeckLuck), settings.HasBetterSwapDeckLuck.ToString());
            }

            if (settings.LastSwapDeckDate == DateTime.MinValue)
            {
                randomNumber = 80;
            }

            settings.LastSwapDeckDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastSwapDeckDate), settings.LastSwapDeckDate.ToString());

            if (rnd.Next(4) == 1)
            {
                settings.LuckLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LuckLevel), settings.LuckLevel.ToString());
            }

            if (randomNumber < 5)
            {
                List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers();
                Enhancer enhancer = allEnhancers.Aggregate((x, y) => Math.Abs(x.Points - selectedCard.OriginalPoints) < Math.Abs(y.Points - selectedCard.OriginalPoints) ? x : y);
                enhancer.CreatedDate = selectedCard.CreatedDate;
                enhancer.RepairedDate = selectedCard.RepairedDate;

                _itemsRepository.RemoveItem(user, selectedCard);
                _itemsRepository.AddItem(user, enhancer);

                message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was successfully swapped for the Enhancer \"{enhancer.Name}\"!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.None, message);

                return Json(new Status { Success = true, SuccessMessage = message });
            }
            else if (randomNumber < 20)
            {
                int conditionDays = rnd.Next(2, 21);

                selectedCard.RepairedDate = selectedCard.RepairedDate.AddDays(-conditionDays);
                _itemsRepository.UpdateItem(user, selectedCard);

                message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped and it came back having its condition worsened by {conditionDays} days!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Negative, message);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = true, DangerMessage = message, Id = selectedCard.SelectionId.ToString(), SoundUrl = soundUrl });
            }
            else if (randomNumber < 25)
            {
                List<ItemTheme> themes = new List<ItemTheme> { ItemTheme.Default, ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared };
                themes.RemoveAll(x => x == selectedCard.Theme);
                ItemTheme theme = themes[rnd.Next(themes.Count)];

                if (selectedCard.Theme == ItemTheme.Default)
                {
                    if (rnd.Next(5) == 1)
                    {
                        message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped but it came back exactly the same. Try again next time.";
                        _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.None, message);

                        return Json(new Status { Success = true, InfoMessage = message, Id = selectedCard.SelectionId.ToString() });
                    }
                }
                else
                {
                    if (theme == ItemTheme.Default || rnd.Next(5) == 1)
                    {
                        selectedCard.Theme = ItemTheme.Default;
                        _itemsRepository.UpdateItem(user, selectedCard);

                        message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped and it came back without its theme!";
                        _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Negative, message);

                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                        return Json(new Status { Success = true, DangerMessage = message, Id = selectedCard.SelectionId.ToString(), SoundUrl = soundUrl });
                    }
                }

                selectedCard.Theme = theme;
                _itemsRepository.UpdateItem(user, selectedCard);

                message = $"Your {Resources.DeckCard} was successfully swapped and it came back having its theme changed to \"{selectedCard.Name}\"!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Positive, message);

                return Json(new Status { Success = true, SuccessMessage = message, Id = selectedCard.SelectionId.ToString() });
            }
            else if (randomNumber < 45)
            {
                int conditionDays = rnd.Next(2, 21);
                DateTime newDate = selectedCard.RepairedDate.AddDays(conditionDays);
                if (newDate > DateTime.UtcNow)
                {
                    newDate = DateTime.UtcNow;
                }

                selectedCard.RepairedDate = newDate;
                _itemsRepository.UpdateItem(user, selectedCard);

                message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was successfully swapped and it came back having its condition improved by {conditionDays} days!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Positive, message);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShortAlt, settings);

                return Json(new Status { Success = true, SuccessMessage = message, Id = selectedCard.SelectionId.ToString(), SoundUrl = soundUrl });
            }
            else if (randomNumber < 65)
            {
                int changeAmount = rnd.Next(1, 3);
                if (selectedCard.Amount - changeAmount <= 0)
                {
                    message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped but it came back exactly the same. Try again next time.";
                    _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.None, message);

                    return Json(new Status { Success = true, InfoMessage = message, Id = selectedCard.SelectionId.ToString() });
                }

                Card newCard = _itemsRepository.SwapCard(user, selectedCard, -changeAmount);

                message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped and it came back worse as \"{newCard.Name}\"!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Negative, message);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = true, DangerMessage = message, Id = newCard.SelectionId.ToString(), SoundUrl = soundUrl });
            }
            else
            {
                int changeAmount = rnd.Next(1, 3);
                if (rnd.Next(100) == 1)
                {
                    changeAmount = rnd.Next(5, 10);
                }
                if (selectedCard.Amount + changeAmount > 100)
                {
                    message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was swapped but it came back exactly the same. Try again next time.";
                    _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.None, message);

                    return Json(new Status { Success = true, InfoMessage = message, Id = selectedCard.SelectionId.ToString() });
                }

                Card newCard = _itemsRepository.SwapCard(user, selectedCard, changeAmount);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ImprovedDeckCardSwap))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ImprovedDeckCardSwap);
                }

                message = $"Your {Resources.DeckCard} \"{selectedCard.Name}\" was successfully swapped and it came back better as \"{newCard.Name}\"!";
                _swapDeckRepository.AddSwapDeckHistory(user, SwapDeckType.Positive, message);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = message, Id = newCard.SelectionId.ToString(), SoundUrl = soundUrl });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EnterMaze(MazeDirection direction)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastMazeEntry = (int)(DateTime.UtcNow - settings.LastMazeEntryDate).TotalHours;
            if (hoursSinceLastMazeEntry < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've entered the Maze too recently. Try again in {24 - hoursSinceLastMazeEntry} hour{(24 - hoursSinceLastMazeEntry == 1 ? "" : "s")}." });
            }

            string soundUrl;
            Random rnd = new Random();

            if (settings.MazeLevel == 0)
            {
                if (user.Points < 500)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
                }

                if (_pointsRepository.SubtractPoints(user, 500))
                {
                    settings.MazeLevel = 1;
                    _settingsRepository.SetSetting(user, nameof(settings.MazeLevel), settings.MazeLevel.ToString());

                    return Json(new Status { Success = true, Points = user.GetFormattedPoints() });
                }
            }
            else if (settings.MazeLevel < 4)
            {
                int odds = 3;
                if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
                {
                    if (settings.ActiveHoroscope == HoroscopeType.Good || settings.MazeLevel % 2 == 0 ? direction == MazeDirection.Left : direction == MazeDirection.Right)
                    {
                        odds = 2;
                    }
                    else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                    {
                        odds = 4;
                    }
                }

                if (rnd.Next(odds) == 1)
                {
                    settings.MazeLevel++;
                    _settingsRepository.SetSetting(user, nameof(settings.MazeLevel), settings.MazeLevel.ToString());

                    return Json(new Status { Success = true });
                }

                ResetMaze(user, settings);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = false, ErrorMessage = $"You made a wrong turn in the Maze and had to escape! Better luck next time!", SoundUrl = soundUrl });
            }
            else
            {
                ResetMaze(user, settings);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MazeEndReached))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MazeEndReached);
                }

                List<Item> items = _itemsRepository.GetItems(user);
                if (settings.MaxBagSize > items.Count)
                {
                    List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
                    Item item;
                    int maxPoints = rnd.Next(2) == 1 ? rnd.Next(50000, 100000) : rnd.Next(25000, 45000);
                    if (rnd.Next(4) == 1)
                    {
                        item = allItems.Find(x => x.IsExclusive && x.Category == ItemCategory.Art && x.Name.Equals("The Maze", StringComparison.InvariantCultureIgnoreCase));

                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MazeRareItem))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MazeRareItem);
                        }
                    }
                    else
                    {
                        allItems = allItems.FindAll(x => !x.IsExclusive && x.Points >= 20000 && x.Points <= maxPoints);
                        item = allItems[rnd.Next(allItems.Count)];
                    }

                    _itemsRepository.AddItem(user, item);

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully made it to the end of the Maze and the item \"{item.Name}\" was added to your bag!", SoundUrl = soundUrl });
                }

                int points = rnd.Next(2) == 1 ? rnd.Next(35000, 50000) : rnd.Next(25000, 35000);
                if (_pointsRepository.AddPoints(user, points))
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully made it to the end of the Maze and earned {Helper.GetFormattedPoints(user.Points)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"You couldn't progress further in the Maze." });
        }

        [HttpGet]
        public IActionResult GetSwapDeckPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            SwapDeckViewModel swapDeckViewModel = GenerateModel(user);
            return PartialView("_SwapDeckMainPartial", swapDeckViewModel);
        }

        [HttpGet]
        public IActionResult GetSwapDeckMazePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            SwapDeckViewModel swapDeckViewModel = GenerateModel(user);
            return PartialView("_SwapDeckMazePartial", swapDeckViewModel);
        }

        private SwapDeckViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            SwapDeckViewModel swapDeckViewModel = new SwapDeckViewModel
            {
                Title = "Swap Deck",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Deck = _itemsRepository.GetCards(user, true).OrderBy(x => x.Amount).ToList(),
                CanSwapDeck = (DateTime.UtcNow - settings.LastSwapDeckDate).TotalHours >= 24,
                CanAccessMaze = accomplishments.Any(x => x.Id == AccomplishmentId.MazeUnlocked),
                CanEnterMaze = (DateTime.UtcNow - settings.LastMazeEntryDate).TotalHours >= 24,
                HasBetterSwapDeckLuck = settings.HasBetterSwapDeckLuck,
                MazeLevel = settings.MazeLevel,
                SwapDeckHistories = _swapDeckRepository.GetSwapDeckHistory(user).Take(15).ToList(),
                MazeEntryDate = settings.LastMazeEntryDate
            };

            return swapDeckViewModel;
        }

        private void ResetMaze(User user, Settings settings)
        {
            settings.MazeLevel = 0;
            settings.LastMazeEntryDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.MazeLevel), settings.MazeLevel.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.LastMazeEntryDate), settings.LastMazeEntryDate.ToString());
        }
    }
}
