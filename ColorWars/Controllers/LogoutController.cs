﻿using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class LogoutController : Controller
    {
        private ILoginRepository _loginRepository;
        private ILogoutRepository _logoutRepository;

        public LogoutController(ILoginRepository loginRepository, ILogoutRepository logoutRepository)
        {
            _loginRepository = loginRepository;
            _logoutRepository = logoutRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }

            _logoutRepository.Logout(user);

            if (Request.Headers.ContainsKey("Referer")) //intentionally spelt this way
            {
                return Redirect(Request.Headers["Referer"].ToString());
            }
            return RedirectToAction("Index", "Login");
        }
    }
}
