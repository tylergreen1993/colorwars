﻿using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

namespace ColorWars.Controllers
{
    public class ErrorController : Controller
    {
        private ILoginRepository _loginRepository;

        public ErrorController(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        [Route("/Error/{code}")]
        public IActionResult Index(System.Net.HttpStatusCode code)
        {
            User user = _loginRepository.AuthenticateUser();

            int errorCode = (int)code;
            string errorReason = ReasonPhrases.GetReasonPhrase(errorCode);
            ErrorViewModel errorViewModel = new ErrorViewModel
            {
                Title = string.IsNullOrEmpty(errorReason) ? errorCode.ToString() : errorReason,
                StatusCode = code,
                User = user
            };

            errorViewModel.UpdateViewData(ViewData);
            return View(errorViewModel);
        }
    }
}
