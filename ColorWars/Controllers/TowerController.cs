﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TowerController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ITowerRepository _towerRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public TowerController(ILoginRepository loginRepository, IItemsRepository itemsRepository, ITowerRepository towerRepository, 
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _towerRepository = towerRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Tower" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                Messages.AddSnack("You haven't unlocked The Tower yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            TowerViewModel towerViewModel = GenerateModel(user);
            towerViewModel.UpdateViewData(ViewData);
            return View(towerViewModel);
        }

        private TowerViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            TowerViewModel towerViewModel = new TowerViewModel
            {
                Title = "The Tower",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Tower,
                Deck = _itemsRepository.GetCards(user, true),
                TowerRankings = _towerRepository.GetTopTowerRankings().Take(50).ToList(),
                RecentTowerRankings = _towerRepository.GetTowerRankings(30).Take(50).ToList(),
                CurrentStreak = settings.TowerFloorLevel,
                RecordStreak = settings.TowerFloorRecordLevel
            };
            return towerViewModel;
        }
    }
}
