﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SecretGifterController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISecretGifterRepository _secretGifterRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IMessagesRepository _messagesRepository;
        private IUserRepository _userRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;

        public SecretGifterController(ILoginRepository loginRepository, IItemsRepository itemsRepository, ISecretGifterRepository secretGifterRepository,
        IPromoCodesRepository promoCodesRepository, IMessagesRepository messagesRepository, IUserRepository userRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, IEmailRepository emailRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _secretGifterRepository = secretGifterRepository;
            _promoCodesRepository = promoCodesRepository;
            _messagesRepository = messagesRepository;
            _userRepository = userRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SecretGifter" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SecretGifterExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedSecretGifter)
            {
                settings.CheckedSecretGifter = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedSecretGifter), settings.CheckedSecretGifter.ToString());
            }

            SecretGifterViewModel secretGifterViewModel = GenerateModel(user);
            secretGifterViewModel.UpdateViewData(ViewData);
            return View(secretGifterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendSecretGift(Guid selectionId, string message)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.SecretGifterExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can no longer send {settings.SecretGiftUsername} a Secret Gift.", ShouldRefresh = true });
            }

            if (selectionId == Guid.Empty)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose one of your items to send as a Secret Gift." });
            }

            Item item = _itemsRepository.GetItems(user, true).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            if (message?.Length > 100)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message must be 100 characters or less." });
            }

            User giftReceiver = _userRepository.GetUser(settings.SecretGiftUsername);
            if (giftReceiver == null)
            {
                settings.SecretGifterExpiryDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.SecretGifterExpiryDate), settings.SecretGifterExpiryDate.ToString());

                return Json(new Status { Success = false, ErrorMessage = "You can no longer send this user a Secret Gift." });
            }

            if (_secretGifterRepository.AddSecretGiftItem(giftReceiver, item))
            {
                bool uniqueStringFound = false;
                string uniqueString = string.Empty;
                while (!uniqueStringFound)
                {
                    uniqueString = Helper.GetRandomString(8);
                    uniqueStringFound = _promoCodesRepository.AddPromoCode(user, uniqueString, PromoEffect.SecretGift, true, false, item, DateTime.UtcNow.AddDays(365));
                }

                string courierMessage = $"You received a gift from a Secret Gifter! You can claim this gift by entering the promo code: {uniqueString} on the Promo Codes page.";
                if (!string.IsNullOrEmpty(message))
                {
                    courierMessage += $" They also included the following personal message: \"{message}\"";
                }
                _messagesRepository.SendCourierMessage(giftReceiver, courierMessage);

                _itemsRepository.RemoveItem(user, item);

                settings.SecretGifterRank++;
                _settingsRepository.SetSetting(user, nameof(settings.SecretGifterRank), settings.SecretGifterRank.ToString());

                settings.SecretGiftUsername = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.SecretGiftUsername), settings.SecretGiftUsername);

                settings.SecretGifterExpiryDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.SecretGifterExpiryDate), settings.SecretGifterExpiryDate.ToString());

                SendSecretGiftReceivedEmail(giftReceiver, message, uniqueString);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SentSecretGift))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SentSecretGift);
                }
                if (item.Points >= 25000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SentBigSecretGift))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SentBigSecretGift);
                    }
                }
                if (settings.SecretGifterRank >= 9)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigSecretGifter))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigSecretGifter);
                    }
                }

                Messages.AddSnack($"You successfully sent a Secret Gift and your Secret Gifter Rank increased to {Helper.FormatNumber(settings.SecretGifterRank + 1)}!");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "You weren't able to send your Secret Gift." });
        }

        private SecretGifterViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            if (string.IsNullOrEmpty(settings.SecretGiftUsername))
            {
                SecretGiftUser secretGiftUser = _secretGifterRepository.GetSecretGiftUser(user);
                settings.SecretGiftUsername = secretGiftUser.Username;
                _settingsRepository.SetSetting(user, nameof(settings.SecretGiftUsername), settings.SecretGiftUsername);
            }

            SecretGifterViewModel secretGifterViewModel = new SecretGifterViewModel
            {
                Title = "Secret Gifter",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.SecretGifter,
                Items = _itemsRepository.GetItems(user, true),
                SecretGiftUsername = settings.SecretGiftUsername,
                ExpiryDate = settings.SecretGifterExpiryDate
            };

            return secretGifterViewModel;
        }

        private void SendSecretGiftReceivedEmail(User user, string personalMessage, string promoCode)
        {
            string subject = "You have a Secret Gift!";
            string title = "Secret Gift";
            string url = $"/PromoCodes?code={promoCode}";
            string message = $"You received a gift from a Secret Gifter! You can claim this gift by entering the promo code: <b><a href='{Helper.GetDomain() + url}'>{promoCode}</a></b> on the Promo Codes page.";
            if (!string.IsNullOrEmpty(personalMessage))
            {
                message += $"<p>They also included the following personal message: \"{personalMessage}\"";
            }

            _emailRepository.SendEmail(user, subject, title, message, url);
        }
    }
}
