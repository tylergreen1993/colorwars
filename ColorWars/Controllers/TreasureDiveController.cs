﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TreasureDiveController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ITreasureDiveRepository _treasureDiveRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public TreasureDiveController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ITreasureDiveRepository treasureDiveRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _treasureDiveRepository = treasureDiveRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TreasureDive" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("OnTheHour", "TreasureDive");
            }

            TreasureDiveViewModel treasureDiveViewModel = GenerateModel(user);
            treasureDiveViewModel.UpdateViewData(ViewData);
            return View(treasureDiveViewModel);
        }

        public IActionResult OnTheHour()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "OnTheHour" });
            }

            TreasureDiveViewModel treasureDiveViewModel = GenerateModel(user);
            treasureDiveViewModel.UpdateViewData(ViewData);
            return View(treasureDiveViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int x, int y)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (x < 0 || y < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't press this point." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastTreasureDivePlayDate).TotalHours;
            if (hoursSinceLastGame < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Treasure Dive too recently. Try again in {6 - hoursSinceLastGame} hour{(6 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            TreasureDive treasureDive = GetTreasureDive(user, settings);
            if (treasureDive.IsMatch())
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already found the hidden treasure." });
            }
            if (treasureDive.Attempts >= 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already used all of your 10 attempts to find the hidden treasure." });
            }

            if (settings.HasBetterTreasureDiveLuck)
            {
                int treasureCoordinate = treasureDive.GetTreasureCoordinates().X;
                if (x < 135 && treasureCoordinate > 135 || x > 135 && treasureCoordinate < 135)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This part of the map has been blocked off as the treasure isn't there." });
                }
            }

            treasureDive.Attempts++;
            Point point = new Point
            {
                X = x,
                Y = y
            };
            treasureDive.UpdateLastGuessCoordinates(point);
            _treasureDiveRepository.UpdateTreasureDive(treasureDive);

            if (treasureDive.IsMatch())
            {
                settings.LastTreasureDivePlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastTreasureDivePlayDate), settings.LastTreasureDivePlayDate.ToString());

                int points = 10000 - (1000 * (treasureDive.Attempts - 1));
                _pointsRepository.AddPoints(user, points);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FoundTreasureDive))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FoundTreasureDive);
                }
                if (treasureDive.Attempts == 1)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FoundTreasureDiveFirstTry))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FoundTreasureDiveFirstTry);
                    }
                }

                if (settings.HasBetterTreasureDiveLuck)
                {
                    settings.HasBetterTreasureDiveLuck = false;
                    _settingsRepository.SetSetting(user, nameof(settings.HasBetterTreasureDiveLuck), settings.HasBetterTreasureDiveLuck.ToString());
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShortAlt, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully found the hidden treasure in {(treasureDive.Attempts == 1 ? "1 try" : $"{treasureDive.Attempts} tries")} and earned {Helper.GetFormattedPoints(points)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }
            if (treasureDive.Attempts == 10)
            {
                settings.LastTreasureDivePlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastTreasureDivePlayDate), settings.LastTreasureDivePlayDate.ToString());

                if (settings.HasBetterTreasureDiveLuck)
                {
                    settings.HasBetterTreasureDiveLuck = false;
                    _settingsRepository.SetSetting(user, nameof(settings.HasBetterTreasureDiveLuck), settings.HasBetterTreasureDiveLuck.ToString());
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = true, DangerMessage = "You used all your attempts but didn't find the hidden treasure! Better luck next time!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult IsOnHour()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (DateTime.UtcNow.Minute != 0)
            {
                int minutesAway = 60 - DateTime.UtcNow.Minute;
                return Json(new Status { Success = false, ErrorMessage = $"You cannot push the button as it's {minutesAway} minute{(minutesAway == 1 ? "" : "s")} away from the hour. Come back when it's on the hour!" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LastOnTheHourDate).TotalMinutes < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already pushed the button this hour." });
            }

            if (_pointsRepository.AddPoints(user, 1000))
            {
                settings.LastOnTheHourDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastOnTheHourDate), settings.LastOnTheHourDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OnTheHour))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OnTheHour);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully pressed the button on the hour and earned {Helper.GetFormattedPoints(1000)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not collect a reward at this time." });
        }

        [HttpGet]
        public IActionResult GetTreasureDivePartialView(bool isTreasureDive)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TreasureDiveViewModel treasureDiveViewModel = GenerateModel(user);
            if (isTreasureDive)
            {
                return PartialView("_TreasureDiveMainPartial", treasureDiveViewModel);
            }

            return PartialView("_TreasureDiveOnTheHourPartial", treasureDiveViewModel);
        }

        private TreasureDiveViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            TreasureDive treasureDive = GetTreasureDive(user, settings);
            TreasureDiveViewModel treasureDiveViewModel = new TreasureDiveViewModel
            {
                Title = "Treasure Dive",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                TreasureDive = treasureDive,
                CanPlay = treasureDive.Attempts < 10 && !treasureDive.IsMatch(),
                IsOnTheHour = DateTime.UtcNow.Minute == 0 && (DateTime.UtcNow - settings.LastOnTheHourDate).TotalMinutes >= 1,
                HasTreasureHint = settings.HasBetterTreasureDiveLuck
            };

            return treasureDiveViewModel;
        }

        private TreasureDive GetTreasureDive(User user, Settings settings)
        {
            TreasureDive treasureDive = _treasureDiveRepository.GetTreasureDive(user);
            if (treasureDive == null)
            {
                _treasureDiveRepository.AddTreasureDive(user);
                return _treasureDiveRepository.GetTreasureDive(user);
            }
            if ((DateTime.UtcNow - settings.LastTreasureDivePlayDate).TotalHours >= 6 && (treasureDive.Attempts == 10 || treasureDive.IsMatch()))
            {
                _treasureDiveRepository.DeleteTreasureDive(treasureDive);
                _treasureDiveRepository.AddTreasureDive(user);
                return _treasureDiveRepository.GetTreasureDive(user);
            }
            return treasureDive;
        }
    }
}
