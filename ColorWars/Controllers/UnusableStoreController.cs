﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class UnusableStoreController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private IJunkyardRepository _junkyardRepository;
        private IDonationsRepository _donationsRepository;
        private ISettingsRepository _settingsRepository;

        public UnusableStoreController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        IJunkyardRepository junkyardRepository, IDonationsRepository donationsRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _junkyardRepository = junkyardRepository;
            _donationsRepository = donationsRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "UnusableStoreC" });
            }

            UnusableStoreViewModel unusableStoreViewModel = GenerateModel(user);
            unusableStoreViewModel.UpdateViewData(ViewData);
            return View(unusableStoreViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Buy(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = GetItems().Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer available." });
            }

            if (user.Points < item.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_itemsRepository.AddItem(user, item))
            {
                if (_pointsRepository.SubtractPoints(user, item.Points))
                {
                    if (_junkyardRepository.GetJunkyardItems(true).Any(x => x.SelectionId == item.SelectionId))
                    {
                        _junkyardRepository.DeleteJunkyardItem(item);
                    }
                    else
                    {
                        _donationsRepository.DeleteDonationItem(item);
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully bought the item \"{item.Name}\" for {Helper.GetFormattedPoints(item.Points)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = user.GetFormattedPoints() });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"This item could not be bought." });
        }

        [HttpGet]
        public IActionResult GetUnusableStorePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            UnusableStoreViewModel unusableStoreViewModel = GenerateModel(user);
            return PartialView("_UnusableStoreMainPartial", unusableStoreViewModel);
        }

        private UnusableStoreViewModel GenerateModel(User user)
        {
            UnusableStoreViewModel unusableStoreViewModel = new UnusableStoreViewModel
            {
                Title = "Unusable Store",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.UnusableStore,
                Items = GetItems().OrderBy(x => x.Points).ToList()
            };

            return unusableStoreViewModel;
        }

        private List<Item> GetItems()
        {
            List<Item> junkyardItems = _junkyardRepository.GetJunkyardItems(true).FindAll(x => x.Condition == ItemCondition.Unusable);
            List<Item> donationItems = _donationsRepository.GetDonationItems(true).FindAll(x => x.Condition == ItemCondition.Unusable);

            List<Item> allItems = new List<Item>();
            allItems.AddRange(junkyardItems);
            allItems.AddRange(donationItems);

            List<Item> items = new List<Item>();
            foreach (Item item in allItems.OrderBy(x => x.AddedDate).Take(40))
            {
                item.UpdatePoints((int)Math.Round(item.Points * 0.65));
                items.Add(item);
            }

            return items;
        }
    }
}
