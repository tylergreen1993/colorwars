﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TrainingCenterController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ITrainingCenterRepository _trainingCenterRepository;
        private IMatchStateRepository _matchStateRepository;
        private IRelicsRepository _relicsRepository;
        private IPointsRepository _pointsRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public TrainingCenterController(ILoginRepository loginRepository, IItemsRepository itemsRepository, ITrainingCenterRepository trainingCenterRepository,
        IMatchStateRepository matchStateRepository, IRelicsRepository relicsRepository, IPointsRepository pointsRepository, IPowerMovesRepository powerMovesRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _trainingCenterRepository = trainingCenterRepository;
            _matchStateRepository = matchStateRepository;
            _relicsRepository = relicsRepository;
            _pointsRepository = pointsRepository;
            _powerMovesRepository = powerMovesRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TrainingCenter" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Plateau", "TrainingCenter");
            }

            TrainingCenterViewModel trainingCenterViewModel = GenerateModel(user);
            trainingCenterViewModel.UpdateViewData(ViewData);
            return View(trainingCenterViewModel);
        }

        public IActionResult Plateau(bool expand)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TrainingCenter/Plateau" });
            }

            TrainingCenterViewModel trainingCenterViewModel = GenerateModel(user, expand);
            trainingCenterViewModel.UpdateViewData(ViewData);
            return View(trainingCenterViewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult UpgradeCard(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Card card = _itemsRepository.GetCards(user).FindAll(x => x.Amount < 100).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot upgrade this {Resources.DeckCard}." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (card.DeckType == settings.DefaultDeckType && _matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot upgrade this {Resources.DeckCard} while you're in an active Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            int cost = (int)card.Color + 1;

            if (settings.TrainingTokens < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough Training Tokens to upgrade this {Resources.DeckCard}." });
            }

            settings.TrainingTokens -= cost;
            if (_settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString()))
            {
                Card newCard = _itemsRepository.SwapCard(user, card, 1);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.UpgradedCard))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.UpgradedCard);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully upgraded your {Resources.DeckCard} and it became \"{newCard.Name}\"!", Id = newCard.SelectionId.ToString(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your {Resources.DeckCard} could not be upgraded at this time." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult GetPowerMove(PowerMoveType powerMoveType)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this yet." });
            }

            List<PowerMove> unlockedPowerMoves = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel);
            PowerMove powerMove = GetPowerMoves(user, unlockedPowerMoves).Find(x => x.Type == powerMoveType);
            if (powerMove == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Power Move cannot be learned." });
            }

            if (settings.TrainingTokens < powerMove.Tokens)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough Training Tokens to learn this Power Move." });
            }

            settings.TrainingTokens -= powerMove.Tokens;
            if (_settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString()))
            {
                _powerMovesRepository.AddPowerMove(user, powerMove.Type);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully learned the Power Move \"{powerMove.Name}\"!", SoundUrl = soundUrl, ButtonText = "Head to bag!", ButtonUrl = "/Items?showPowerMoves?=true" });
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your couldn't learn that Power Move at this time." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult GetUpgrade(TrainingCenterUpgradeId upgradeId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this yet." });
            }

            TrainingCenterUpgrade trainingCenterUpgrade = _trainingCenterRepository.GetUpgrades(settings).Find(x => x.Id == upgradeId);
            if (trainingCenterUpgrade == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot get this upgrade." });
            }

            if (settings.TrainingTokens < trainingCenterUpgrade.Tokens)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough Training Tokens to get this upgrade." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot get an upgrade while you're in a Stadium match.", ButtonText = "Head to Stadium!", ButtonUrl = "/Stadium" });
            }

            string buttonText = string.Empty;
            string buttonUrl = string.Empty;

            switch (trainingCenterUpgrade.Id)
            {
                case TrainingCenterUpgradeId.ExtraPowerMoves:
                    if (settings.MaxPowerMoves == 10)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already can take up to 10 Power Moves into a Stadium match." });
                    }
                    break;
                case TrainingCenterUpgradeId.ExtraDeckSlot:
                    if (settings.UnlockedExtraDeck)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have an extra Deck slot." });
                    }
                    break;
                case TrainingCenterUpgradeId.ThemeBonusIncrease:
                    if (settings.ThemeBonusBoost == 20)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have the maximum Theme Bonus for all themes." });
                    }
                    break;
                case TrainingCenterUpgradeId.StadiumBonus:
                    if (settings.StadiumBonusPercent > 0 && settings.StadiumBonusExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have an active Stadium bonus against CPUs." });
                    }
                    break;
            }

            settings.TrainingTokens -= trainingCenterUpgrade.Tokens;
            if (_settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString()))
            {
                switch (trainingCenterUpgrade.Id)
                {
                    case TrainingCenterUpgradeId.ExtraPowerMoves:
                        settings.MaxPowerMoves = 10;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxPowerMoves), settings.MaxPowerMoves.ToString());
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items?showPowerMoves=true";
                        break;
                    case TrainingCenterUpgradeId.ExtraDeckSlot:
                        settings.UnlockedExtraDeck = true;
                        _settingsRepository.SetSetting(user, nameof(settings.UnlockedExtraDeck), settings.UnlockedExtraDeck.ToString());
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items";
                        break;
                    case TrainingCenterUpgradeId.ThemeBonusIncrease:
                        settings.ThemeBonusBoost += 5;
                        _settingsRepository.SetSetting(user, nameof(settings.ThemeBonusBoost), settings.ThemeBonusBoost.ToString());
                        break;
                    case TrainingCenterUpgradeId.StadiumBonus:
                        settings.StadiumBonusPercent = 15;
                        settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(10);
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());
                        break;
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully got the upgrade \"{trainingCenterUpgrade.Name}\"!", SoundUrl = soundUrl, ButtonText = buttonText, ButtonUrl = buttonUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You couldn't get the upgrade at this time." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult AccessPlateau()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (relics.All(x => x.Earned))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You haven't earned all {relics.Count} Relics yet. You still need to earn {relics.Count(x => !x.Earned)} more." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult OpenPortal()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.RightSideUpWorldExpiryDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already opened a portal to the Right-Side Up World." });
            }

            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (relics.All(x => !x.Earned))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot enter the Ancient Plateau yet." });
            }

            if (user.Points < 5000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (_pointsRepository.SubtractPoints(user, 5000))
            {
                settings.RightSideUpWorldExpiryDate = DateTime.UtcNow.AddDays(1);
                _settingsRepository.SetSetting(user, nameof(settings.RightSideUpWorldExpiryDate), settings.RightSideUpWorldExpiryDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OpenedRightSideUpWorldPortal))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OpenedRightSideUpWorldPortal);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                return Json(new Status { Success = true, SuccessMessage = "You successfully opened a portal to the Right-Side Up World and can now access it for the next 24 hours at the Plaza!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "The portal couldn't be opened." });
        }

        [HttpGet]
        public IActionResult GetTrainingCenterPartialView(bool isTraining)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TrainingCenterViewModel trainingCenterViewModel = GenerateModel(user);
            if (isTraining)
            {
                return PartialView("_TrainingCenterMainPartial", trainingCenterViewModel);
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);
            if (relics.Any(x => !x.Earned))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot enter the Ancient Plateau yet." });
            }

            return PartialView("_TrainingCenterPlateauEntrancePartial", trainingCenterViewModel);
        }

        private TrainingCenterViewModel GenerateModel(User user, bool isExpand = false)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<PowerMove> unlockedPowerMoves = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel);
            List<Relic> relics = _relicsRepository.GetAllRelics(settings.MatchLevel);

            TrainingCenterViewModel trainingCenterViewModel = new TrainingCenterViewModel
            {
                Title = "Training Center",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.TrainingCenter,
                Cards = _itemsRepository.GetCards(user).FindAll(x => x.Amount < 100).OrderBy(x => x.Amount).ToList(),
                PowerMoves = GetPowerMoves(user, unlockedPowerMoves),
                AllPowerMoves = _powerMovesRepository.GetAllPowerMoves(),
                Upgrades = _trainingCenterRepository.GetUpgrades(settings),
                Relics = relics,
                NextRelic = relics.Find(x => !x.Earned),
                Tokens = settings.TrainingTokens,
                IsInMatch = _matchStateRepository.IsInMatch(user),
                HasUnlocked = settings.MatchLevel >= 5,
                ExpandAll = isExpand,
                PortalExpiryDate = settings.RightSideUpWorldExpiryDate,
                DeckType = settings.DefaultDeckType
            };

            return trainingCenterViewModel;
        }

        private List<PowerMove> GetPowerMoves(User user, List<PowerMove> unlockedPowerMoves)
        {
            List<PowerMove> earnablePowerMoves = _powerMovesRepository.GetEarnablePowerMoves();
            List<PowerMove> powerMoves = unlockedPowerMoves.FindAll(x => x.Tokens > 0);
            earnablePowerMoves.RemoveAll(x => powerMoves.Any(y => y.Type == x.Type));
            return earnablePowerMoves;
        }
    }
}
