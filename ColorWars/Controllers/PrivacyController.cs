﻿using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PrivacyController : Controller
    {
        private ILoginRepository _loginRepository;

        public PrivacyController(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();

            PrivacyViewModel privacyViewModel = GenerateModel(user);
            privacyViewModel.UpdateViewData(ViewData);
            return View(privacyViewModel);
        }

        private PrivacyViewModel GenerateModel(User user)
        {
            PrivacyViewModel privacyViewModel = new PrivacyViewModel
            {
                Title = "Privacy Policy",
                User = user,
            };

            return privacyViewModel;
        }
    }
}
