﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using FuzzySharp;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ItemsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IMatchStateRepository _matchStateRepository;
        private IUserRepository _userRepository;
        private ICompanionsRepository _companionsRepository;
        private IProfileRepository _profileRepository;
        private ISafetyDepositRepository _safetyDepositBoxRepository;
        private IGuideRepository _guideRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private IRelicsRepository _relicsRepository;
        private ISettingsRepository _settingsRepository;
        private INotificationsRepository _notificationsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ItemsController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IMatchStateRepository matchStateRepository, IUserRepository userRepository, ICompanionsRepository companionsRepository, IProfileRepository profileRepository,
        ISafetyDepositRepository safetyDepositRepository, IGuideRepository guideRepository, IPowerMovesRepository powerMovesRepository, IRelicsRepository relicsRepository,
        ISettingsRepository settingsRepository, INotificationsRepository notificationsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _matchStateRepository = matchStateRepository;
            _userRepository = userRepository;
            _companionsRepository = companionsRepository;
            _profileRepository = profileRepository;
            _safetyDepositBoxRepository = safetyDepositRepository;
            _powerMovesRepository = powerMovesRepository;
            _relicsRepository = relicsRepository;
            _settingsRepository = settingsRepository;
            _guideRepository = guideRepository;
            _notificationsRepository = notificationsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(ItemSort? sort, Guid? selectionId, bool showPowerMoves)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Items" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }


            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.VisitBag))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.VisitBag);
            }

            if (sort.HasValue)
            {
                if (settings.ItemSort != sort)
                {
                    settings.ItemSort = sort.Value;
                    _settingsRepository.SetSetting(user, nameof(settings.ItemSort), settings.ItemSort.ToString());
                }
            }
            else
            {
                sort = settings.ItemSort;
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, selectionId: selectionId, sort: sort.Value, showPowerMoves: showPowerMoves);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult Guide()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Items/Guide" });
            }

            ItemsGuideViewModel itemsGuideViewModel = GenerateGuideModel(user);
            itemsGuideViewModel.UpdateViewData(ViewData);
            return View(itemsGuideViewModel);
        }

        public IActionResult Theme(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Theme?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<Theme> themes = _itemsRepository.GetThemeCards(user);
            Theme theme = themes.Find(x => x.SelectionId == id);
            if (theme == null)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, theme: theme);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult Sleeve(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Sleeve?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<Sleeve> sleeves = _itemsRepository.GetSleeves(user);
            Sleeve sleeve = sleeves.Find(x => x.SelectionId == id);
            if (sleeve == null)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, sleeve: sleeve);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult Stealth(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Stealth?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<Stealth> stealthCards = _itemsRepository.GetStealthCards(user);
            Stealth stealth = stealthCards.Find(x => x.SelectionId == id);
            if (stealth == null)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, stealth: stealth);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult Fairy(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Fairy?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<Fairy> fairyCards = _itemsRepository.GetFairyCards(user);
            Fairy fairy = fairyCards.Find(x => x.SelectionId == id);
            if (fairy == null)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, fairy: fairy);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult Companion(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Companion?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<CompanionItem> companions = _itemsRepository.GetCompanions(user);
            CompanionItem companion = companions.Find(x => x.SelectionId == id);
            if (companion == null)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, companion: companion);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult UpgradeDeck(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/Companion?id={id}" });
            }

            if (id == Guid.Empty)
            {
                return RedirectToAction("Index", "Items");
            }

            List<Upgrader> upgraders = _itemsRepository.GetUpgraders(user);
            Upgrader upgrader = upgraders.Find(x => x.SelectionId == id);
            if (upgrader == null)
            {
                return RedirectToAction("Index", "Items");
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                Messages.AddSnack("You can't use this Upgrader while you're in a match.", true);
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, upgrader: upgrader);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult UpgradeCompanion(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/UpgradeCompanion" });
            }

            List<Companion> companions = _companionsRepository.GetCompanions(user);
            if (companions.Count == 0)
            {
                Messages.AddSnack("You don't have any active Companions.", true);
                return RedirectToAction("Index", "Items");
            }

            Candy candy = _itemsRepository.GetCandy(user).Find(x => x.SelectionId == selectionId);
            if (candy == null)
            {
                return RedirectToAction("Index", "Items");
            }
            if (candy.Type != CandyType.Companion)
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, selectedCandy: candy);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        public IActionResult ColorCompanion(Guid selectionId, Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Items/ColorCompanion" });
            }

            List<Companion> companions = _companionsRepository.GetCompanions(user);
            if (companions.Count == 0)
            {
                Messages.AddSnack("You don't have any active Companions.", true);
                return RedirectToAction("Index", "Items");
            }
            Companion companion = null;
            if (companionId != Guid.Empty)
            {
                companion = companions.Find(x => x.Id == companionId);
                if (companion == null)
                {
                    Messages.AddSnack("You no longer have this Companion.", true);
                    return RedirectToAction("Index", "Items");
                }
            }

            Candy candy = _itemsRepository.GetCandy(user).Find(x => x.SelectionId == selectionId);
            if (candy == null)
            {
                return RedirectToAction("Index", "Items");
            }
            if (candy.Type != CandyType.CompanionDye || (companion != null && candy.Amount == 0))
            {
                return RedirectToAction("Index", "Items");
            }

            ItemsViewModel itemsViewModel = GenerateModel(user, selectedCandy: candy, selectedCompanion: companion);
            itemsViewModel.UpdateViewData(ViewData);
            return View(itemsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveFromDeck(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Card card = _itemsRepository.GetCards(user, true).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That card couldn't be found." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't remove a card from your Deck while you're in a Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            card.DeckType = DeckType.None;
            _itemsRepository.UpdateItem(user, card);

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddToDeck(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Card> cards = _itemsRepository.GetCards(user);

            Settings settings = _settingsRepository.GetSettings(user);
            List<Card> deckCards = cards.FindAll(x => x.DeckType == settings.DefaultDeckType);
            if (deckCards.Count >= Helper.GetMaxDeckSize())
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can only have {Helper.GetMaxDeckSize()} {Resources.DeckCards} in your Deck. Pull one from your Deck to add this one." });
            }

            Card card = cards.Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That card couldn't be found." });
            }

            card.DeckType = settings.DefaultDeckType;
            _itemsRepository.UpdateItem(user, card);

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            return Json(new Status { Success = true, SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenPack(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (settings.MaxBagSize - items.Count < 4)
            {
                int itemsOver = (items.Count + 4) - settings.MaxBagSize;
                return Json(new Status { Success = false, ErrorMessage = $"You have too many items in your bag to open the Card Pack. Remove {itemsOver} item{(itemsOver == 1 ? "" : "s")} and then you can open it!" });
            }

            CardPack cardPack = _itemsRepository.GetCardPacks(user).Find(x => x.SelectionId == selectionId);
            if (cardPack == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That Card Pack couldn't be found." });
            }

            _itemsRepository.RemoveItem(user, cardPack);
            List<Item> itemsReceived = OpenPack(user, cardPack, settings);

            string itemsReceivedMessage = "You successfully opened the Card Pack and it contained:";
            int index = 0;
            foreach (Item item in itemsReceived)
            {
                itemsReceivedMessage += $" \"{item.Name}\" ({item.Category})";
                itemsReceivedMessage += index == (itemsReceived.Count - 1) ? "!" : index == (itemsReceived.Count - 2) ? ", and" : ",";
                index++;
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShortAlt, settings);

            return Json(new Status { Success = true, SuccessMessage = itemsReceivedMessage, SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EatCandy(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Candy> candies = _itemsRepository.GetCandy(user);
            Candy candy = candies.Find(x => x.SelectionId == selectionId);
            if (candy == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That Candy Card couldn't be found." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (candy.Type == CandyType.Wizard)
            {
                if (settings.HasWizardBetterLuck)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.HasWizardBetterLuck = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasWizardBetterLuck), settings.HasWizardBetterLuck.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained more luck for your next visit to the Young Wizard!", ButtonText = "Head to the Young Wizard!", ButtonUrl = "/Wizard" });
            }

            if (candy.Type == CandyType.Crafting)
            {
                if (settings.CraftingLuckExpiryDate > DateTime.UtcNow)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.CraftingLuckExpiryDate = DateTime.UtcNow.AddMinutes(10);
                _settingsRepository.SetSetting(user, nameof(settings.CraftingLuckExpiryDate), settings.CraftingLuckExpiryDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained more luck while crafting for the next 10 minutes!", ButtonText = "Head to Crafting!", ButtonUrl = "/Crafting" });
            }

            if (candy.Type == CandyType.Spin)
            {
                if (settings.HasBetterSpinLuck)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.HasBetterSpinLuck = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSpinLuck), settings.HasBetterSpinLuck.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained more luck for your next spin at the Spin of Success!", ButtonText = "Head to the Spin of Success!", ButtonUrl = "/SpinSuccess" });
            }

            if (candy.Type == CandyType.CinnamonTreasure)
            {
                if (settings.HasBetterTreasureDiveLuck)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.HasBetterTreasureDiveLuck = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterTreasureDiveLuck), settings.HasBetterTreasureDiveLuck.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and now have a better sense where the treasure might be the next time you play Treasure Dive!", ButtonText = "Head to Treasure Dive!", ButtonUrl = "/TreasureDive" });
            }

            if (candy.Type == CandyType.Companion)
            {
                if (_companionsRepository.GetCompanions(user).Count == 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have any Companions to upgrade." });
                }

                return Json(new Status { Success = true, ReturnUrl = $"/Items/UpgradeCompanion?selectionId={candy.SelectionId}" });
            }

            if (candy.Type == CandyType.CompanionDye)
            {
                if (_companionsRepository.GetCompanions(user).Count == 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have any Companions to dye." });
                }

                return Json(new Status { Success = true, ReturnUrl = $"/Items/ColorCompanion?selectionId={candy.SelectionId}" });
            }

            if (candy.Type == CandyType.Quarry)
            {
                if (settings.HasDoubleQuarryHaul)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.HasDoubleQuarryHaul = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleQuarryHaul), settings.HasDoubleQuarryHaul.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and will get double your regular earnings the next time you mine the Wishing Stone Quarry!", ButtonText = "Head to the Quarry!", ButtonUrl = "/WishingStone/Quarry" });
            }

            if (candy.Type == CandyType.Light)
            {
                if (settings.DeepCavernsExpiryDate > DateTime.UtcNow)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.DeepCavernsExpiryDate = DateTime.UtcNow.AddDays(3);
                _settingsRepository.SetSetting(user, nameof(settings.DeepCavernsExpiryDate), settings.DeepCavernsExpiryDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and can now explore the Deep Caverns for the next 3 days!", ButtonText = "Head to the Deep Caverns!", ButtonUrl = "/Caverns" });
            }

            if (candy.Type == CandyType.Lavender)
            {
                if (settings.LavenderStopExpiryDate > DateTime.UtcNow)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.LavenderStopExpiryDate = DateTime.UtcNow.AddMinutes(candy.TotalTime);
                _settingsRepository.SetSetting(user, nameof(settings.LavenderStopExpiryDate), settings.LavenderStopExpiryDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained its effects at the Stadium for CPU matches for {candy.TotalTime} minutes!", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            if (candy.Type == CandyType.Wall)
            {
                if (settings.Crystal4thExpiryDate > DateTime.UtcNow)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.Crystal4thExpiryDate = DateTime.UtcNow.AddMinutes(candy.TotalTime);
                _settingsRepository.SetSetting(user, nameof(settings.Crystal4thExpiryDate), settings.Crystal4thExpiryDate.ToString());

                return Json(new Status { Success = true, ReturnUrl = "/Wall" });
            }

            if (candy.Type == CandyType.SwapDeck)
            {
                if (settings.HasBetterSwapDeckLuck)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.HasBetterSwapDeckLuck = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSwapDeckLuck), settings.HasBetterSwapDeckLuck.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained more luck the next time you try Swap Deck!", ButtonText = "Head to Swap Deck!", ButtonUrl = "/SwapDeck" });
            }

            if (candy.Type == CandyType.Bubble)
            {
                if (settings.BagProtectExpiryDate > DateTime.UtcNow)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You already have an active {candy.Name} Candy Card effect." });
                }

                _itemsRepository.RemoveItem(user, candy);
                settings.BagProtectExpiryDate = DateTime.UtcNow.AddDays(7);
                _settingsRepository.SetSetting(user, nameof(settings.BagProtectExpiryDate), settings.BagProtectExpiryDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and no items can be stolen from your bag for 7 days!" });
            }

            if ((settings.CandyExpiryDate - DateTime.UtcNow).TotalSeconds > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an active Candy Card effect. You cannot eat a new Candy Card until the existing one expires." });
            }
            settings.CandyExpiryDate = DateTime.UtcNow.AddMinutes(candy.TotalTime);

            bool isSour = false;
            if (candy.Type == CandyType.Sour)
            {
                Random rnd = new Random();
                if (rnd.Next(0, 2) == 0)
                {
                    isSour = true;
                }
            }
            settings.CandyPercent = candy.Amount * (isSour ? -1 : 1);

            _itemsRepository.RemoveItem(user, candy);
            _settingsRepository.SetSetting(user, nameof(settings.CandyExpiryDate), settings.CandyExpiryDate.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.CandyPercent), settings.CandyPercent.ToString());

            if (isSour)
            {
                return Json(new Status { Success = true, DangerMessage = $"Oh no! You ate the Candy Card and it was sour! You gained its negative effects for {candy.TotalTime} minutes!", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }
            return Json(new Status { Success = true, SuccessMessage = $"You successfully ate the Candy Card and gained its effects for {candy.TotalTime} minutes!", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseCoupon(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Coupon> coupons = _itemsRepository.GetCoupons(user);
            Coupon coupon = coupons.Find(x => x.SelectionId == selectionId);
            if (coupon == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That Coupon Card couldn't be found." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string buttonText = "";
            string buttonUrl = "";
            if (coupon.Type == CouponType.Junkyard)
            {
                if (settings.JunkyardCouponPercent > 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must redeem your active Junkyard Coupon before using a new one." });
                }

                settings.JunkyardCouponPercent = coupon.Amount;
                _settingsRepository.SetSetting(user, nameof(settings.JunkyardCouponPercent), settings.JunkyardCouponPercent.ToString());
                buttonText = "Head to the Junkyard!";
                buttonUrl = "/Junkyard";
            }
            else if (coupon.Type == CouponType.Repair)
            {
                if (settings.RepairCouponPercent > 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must redeem your active Repair Shop Coupon before using a new one." });
                }

                settings.RepairCouponPercent = coupon.Amount;
                _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());
                buttonText = "Head to the Repair Shop!";
                buttonUrl = "/Repair";
            }
            else if (coupon.Type == CouponType.Store)
            {
                if (settings.GeneralStoreCouponPercent > 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must redeem your active General Store Coupon before using a new one." });
                }

                settings.GeneralStoreCouponPercent = coupon.Amount;
                _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreCouponPercent), settings.GeneralStoreCouponPercent.ToString());
                buttonText = "Head to the General Store!";
                buttonUrl = "/Store";
            }

            if (coupon.Uses > 1)
            {
                coupon.Uses -= 1;
                _itemsRepository.UpdateItem(user, coupon);
            }
            else
            {
                _itemsRepository.RemoveItem(user, coupon);
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully used the Coupon Card and applied it to your next purchase!", ButtonText = buttonText, ButtonUrl = buttonUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenEgg(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Egg> eggCards = _itemsRepository.GetEggCards(user);
            Egg eggCard = eggCards.Find(x => x.SelectionId == selectionId);
            if (eggCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "That Egg Card couldn't be found." });
            }

            int totalHours = eggCard.GetAgeInHours();

            var multiplier = 10;
            if (eggCard.Type == EggType.Enhanced)
            {
                multiplier = 20;
            }
            else if (eggCard.Type == EggType.Club)
            {
                multiplier = 25;
            }

            int totalPoints = Math.Min(Math.Max(1, totalHours * multiplier), 250000);

            if (_pointsRepository.AddPoints(user, totalPoints))
            {
                _itemsRepository.RemoveItem(user, eggCard);

                if (totalHours >= 1000)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OldEggCard))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OldEggCard);
                    }
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully opened the Egg Card! It was {Helper.FormatNumber(totalHours)} hour{(totalHours == 1 ? "" : "s")} old and earned you {Helper.GetFormattedPoints(totalPoints)}!", Points = Helper.GetFormattedPoints(user.Points) });
            }

            return Json(new Status { Success = false, ErrorMessage = "That Egg Card couldn't be opened." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseTheme(Guid themeSelectionId, Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Theme theme = _itemsRepository.GetThemeCards(user).Find(x => x.SelectionId == themeSelectionId);
            if (theme == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Theme Card." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot change a {Resources.DeckCard}'s theme while you're in an active Stadium match.", ButtonText = "Head to Stadium", ButtonUrl = "/Stadium" });
            }

            string successMessage = null;
            if (selectionId == Guid.Empty)
            {
                if (theme.Type == ThemeType.Card)
                {
                    return Json(new Status { Success = true, ReturnUrl = $"/Items/Theme?id={themeSelectionId}" });
                }

                List<Card> deck = _itemsRepository.GetCards(user, true);
                if (deck.Count == 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You don't have any {Resources.DeckCards} in your Deck." });
                }
                if (deck.All(x => x.Theme == theme.Theme))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"All the {Resources.DeckCards} in your Deck already have this theme." });
                }

                foreach (Card card in deck)
                {
                    if (card.Theme != theme.Theme)
                    {
                        card.Theme = theme.Theme;
                        _itemsRepository.UpdateItem(user, card);
                    }
                }

                successMessage = $"You successfully applied the {theme.Theme} Theme to all of your {Resources.DeckCards}!";
            }
            else
            {
                Item item = _itemsRepository.GetItems(user).Find(x => x.SelectionId == selectionId);
                if (item == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
                }

                if (item.Category != ItemCategory.Deck)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You can only apply a theme to a {Resources.DeckCard}." });
                }

                if (item.Theme == theme.Theme)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"This {Resources.DeckCard} already has the {theme.Theme} Theme." });
                }

                if (item.Points == 1)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ThemedLowItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ThemedLowItem);
                    }
                }

                item.Theme = theme.Theme;
                _itemsRepository.UpdateItem(user, item);

                Messages.AddSnack($"You successfully applied the theme and your {Resources.DeckCard} became \"{item.Name}\"!");
            }

            _itemsRepository.RemoveItem(user, theme);

            if (theme.Theme > ItemTheme.Default)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.UsedTheme))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.UsedTheme);
                }
                if (theme.Theme == ItemTheme.Astral)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.UsedRareTheme))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.UsedRareTheme);
                    }
                }
            }

            if (string.IsNullOrEmpty(successMessage))
            {
                return Json(new Status { Success = true, ReturnUrl = $"/Items#{selectionId}" });
            }

            return Json(new Status { Success = true, SuccessMessage = successMessage });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseSleeve(Guid sleeveSelectionId, Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Sleeve sleeve = _itemsRepository.GetSleeves(user).Find(x => x.SelectionId == sleeveSelectionId);
            if (sleeve == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Sleeve." });
            }

            Item item = _itemsRepository.GetItems(user).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            if (item.Category == ItemCategory.Sleeve)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot use a Sleeve on a Sleeve." });
            }

            if (sleeve.Type == SleeveType.Normal || sleeve.Type == SleeveType.Enhanced)
            {
                if (item.Condition != ItemCondition.New)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You can only use a Sleeve on an item that has a New condition." });
                }

                if ((item.RepairedDate - item.CreatedDate).TotalDays >= 1)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot use a Sleeve on an item that has already had its condition changed." });
                }

                item.RepairedDate = item.CreatedDate.AddDays(sleeve.Type == SleeveType.Enhanced ? 14 : 7);
            }
            else if (sleeve.Type == SleeveType.Club)
            {
                item.RepairedDate = item.RepairedDate.AddDays(14);
            }

            _itemsRepository.UpdateItem(user, item);
            _itemsRepository.RemoveItem(user, sleeve);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully used the Sleeve and your item \"{item.Name}\" had its condition improved!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseUpgrader(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Upgrader upgrader = _itemsRepository.GetUpgraders(user).Find(x => x.SelectionId == selectionId);
            if (upgrader == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Upgrader." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (upgrader.Type == UpgraderType.Bag)
            {
                if (settings.MaxBagSize >= Helper.GetMaxBagSize())
                {
                    return Json(new Status { Success = false, ErrorMessage = "That Upgrader could not be used as your bag can't get any bigger." });
                }

                settings.MaxBagSize = Math.Min(Helper.GetMaxBagSize(), settings.MaxBagSize + upgrader.Amount);
                _settingsRepository.SetSetting(user, nameof(settings.MaxBagSize), settings.MaxBagSize.ToString());

                _itemsRepository.RemoveItem(user, upgrader);

                return Json(new Status { Success = true, SuccessMessage = $"Your maximum bag size was successfully increased to {Helper.FormatNumber(settings.MaxBagSize)} items!" });
            }
            else if (upgrader.Type == UpgraderType.Market)
            {
                if (settings.MaxMarketStallSize >= Helper.GetMaxStallSize())
                {
                    return Json(new Status { Success = false, ErrorMessage = "That Upgrader could not be used as your Market Stall can't get any bigger." });
                }

                settings.MaxMarketStallSize = Math.Min(Helper.GetMaxStallSize(), settings.MaxMarketStallSize + upgrader.Amount);
                _settingsRepository.SetSetting(user, nameof(settings.MaxMarketStallSize), settings.MaxMarketStallSize.ToString());

                _itemsRepository.RemoveItem(user, upgrader);

                return Json(new Status { Success = true, SuccessMessage = $"Your maximum Market Stall size was successfully increased to {Helper.FormatNumber(settings.MaxMarketStallSize)} items!", ButtonText = "Head to your Market Stall!", ButtonUrl = "/MarketStalls/Create" });
            }
            else if (upgrader.Type == UpgraderType.Storage)
            {
                if (settings.MaxStorageSize >= Helper.GetMaxStorageSize())
                {
                    return Json(new Status { Success = false, ErrorMessage = "That Upgrader could not be used as your Storage can't get any bigger." });
                }

                settings.MaxStorageSize = Math.Min(Helper.GetMaxStorageSize(), settings.MaxStorageSize + upgrader.Amount);
                _settingsRepository.SetSetting(user, nameof(settings.MaxStorageSize), settings.MaxStorageSize.ToString());

                _itemsRepository.RemoveItem(user, upgrader);

                return Json(new Status { Success = true, SuccessMessage = $"Your maximum Storage size was successfully increased to {Helper.FormatNumber(settings.MaxStorageSize)} items!", ButtonText = "Head to your Storage!", ButtonUrl = "/Storage" });
            }
            else if (upgrader.Type >= UpgraderType.Red || upgrader.Type <= UpgraderType.Purple || upgrader.Type == UpgraderType.Negative)
            {
                if (_matchStateRepository.IsInMatch(user))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot upgrade a {Resources.DeckCard} while you're in a Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
                }

                return Json(new Status { Success = true, ReturnUrl = $"/Items/UpgradeDeck?id={upgrader.SelectionId}" });
            }

            return Json(new Status { Success = false, ErrorMessage = "That Upgrader couldn't be used." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseStealth(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Stealth stealth = _itemsRepository.GetStealthCards(user).Find(x => x.SelectionId == selectionId);
            if (stealth == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Stealth Card." });
            }

            if (stealth.Type == StealthType.Grab)
            {
                return Json(new Status { Success = true, ReturnUrl = $"/Items/Stealth?id={stealth.SelectionId}" });
            }
            if (stealth.Type == StealthType.Protect)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                if ((settings.StealthProtectExpiryDate - DateTime.UtcNow).TotalDays > 3)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You already have an active Stealth Protect." });
                }
                settings.StealthProtectExpiryDate = DateTime.UtcNow.AddDays(stealth.Quality == StealthQuality.Diamond ? 30 : 7);
                _settingsRepository.SetSetting(user, nameof(settings.StealthProtectExpiryDate), settings.StealthProtectExpiryDate.ToString());

                _itemsRepository.RemoveItem(user, stealth);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully protected yourself from a Stealth Grab for {(stealth.Quality == StealthQuality.Diamond ? "1 month" : "1 week")}!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Stealth Card couldn't be used." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseFairy(Guid selectionId, string itemName)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Fairy fairy = _itemsRepository.GetFairyCards(user).Find(x => x.SelectionId == selectionId);
            if (fairy == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Fairy Card." });
            }

            if (string.IsNullOrEmpty(itemName))
            {
                return Json(new Status { Success = true, ReturnUrl = $"/Items/Fairy?id={fairy.SelectionId}" });
            }

            Item grantedItem = null;
            itemName = itemName.ToLower().Trim();
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
            int fuzzyMatchConfidence = 95;
            Item item = new Item
            {
                Name = itemName
            };
            FuzzySharp.Extractor.ExtractedResult<Item> topItem = Process.ExtractOne(item, items, s => s.Name.ToLower());
            if (topItem.Score >= fuzzyMatchConfidence)
            {
                grantedItem = topItem.Value;
            }

            if (grantedItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The {fairy.Name} didn't recognize that item. Please make sure it's spelled correctly or try asking for something else." });
            }
            if (grantedItem.IsExclusive)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The {grantedItem.Category.GetDisplayName()} \"{grantedItem.Name}\" is a special item and cannot be granted by the {fairy.Name}." });
            }
            if (grantedItem.Points > fairy.Points / 1.25)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The {grantedItem.Category.GetDisplayName()} \"{grantedItem.Name}\" costs too much {Resources.Currency}." });
            }

            if (_itemsRepository.AddItem(user, grantedItem))
            {
                _itemsRepository.RemoveItem(user, fairy);

                return Json(new Status { Success = true, SuccessMessage = $"Your wish was granted and the item \"{grantedItem.Name}\" was successfully added to your bag!" });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The {fairy.Name} could not grant you this item." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CustomizeCompanion(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            CompanionItem companion = _itemsRepository.GetCompanions(user).Find(x => x.SelectionId == selectionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Companion." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (_companionsRepository.GetCompanions(user).Count >= settings.MaxCompanions)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot have more than {settings.MaxCompanions} active Companions." });
            }

            return Json(new Status { Success = true, ReturnUrl = $"/Items/Companion?id={companion.SelectionId}" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ActivateCompanion(Guid selectionId, string name, CompanionColor? selectedColor)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            CompanionItem companion = _itemsRepository.GetCompanions(user).Find(x => x.SelectionId == selectionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Companion." });
            }

            if (string.IsNullOrEmpty(name))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must give your Companion a name." });
            }

            if (name.Length > 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Companion's name must be 10 characters or less." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (_companionsRepository.GetCompanions(user).Count >= settings.MaxCompanions)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot activate this Companion as you've already reached the limit of {settings.MaxCompanions} active Companions." });
            }

            if (selectedColor == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a color for your Companion." });
            }

            if (_companionsRepository.AddCompanion(user, companion.Type, name, selectedColor.Value, out Companion newCompanion))
            {
                _itemsRepository.RemoveItem(user, companion);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionActivated))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionActivated);
                }

                Messages.AddSnack($"Your Companion \"{name}\" was successfully activated and is now visible on your profile!");

                return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{newCompanion.Id}" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Companion couldn't be created." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StealthGrab(Guid selectionId, string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Stealth stealth = _itemsRepository.GetStealthCards(user).Find(x => x.SelectionId == selectionId);
            if (stealth == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Stealth Card." });
            }

            if (stealth.Type != StealthType.Grab)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This Stealth Card cannot be used for stealing {Resources.Currency}." });
            }

            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot steal from yourself." });
            }

            User stealUser = _userRepository.GetUser(username);
            if (stealUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This user does not exist. Make sure to check your spelling." });
            }

            if (stealUser.GetAgeInDays() < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot steal from this user as they joined less than 3 days ago." });
            }

            if (stealUser.Points == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot steal from this user as they have {Helper.GetFormattedPoints(0)} on hand." });
            }

            Settings stealUserSettings = _settingsRepository.GetSettings(stealUser, false);
            if (stealUserSettings.StealthProtectExpiryDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot steal from this user as they have an active Stealth Protect." });
            }

            if ((DateTime.UtcNow - stealUserSettings.StealthGrabStolenLastDate).TotalDays < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot steal from this user as they have had their {Resources.Currency} on hand stolen too recently." });
            }

            Random rnd = new Random();
            double percentToSteal = rnd.Next(stealth.Quality == StealthQuality.Diamond ? 25 : 15, stealth.Quality == StealthQuality.Diamond ? 50 : 25);
            int amountToSteal = (int)Math.Round(stealUser.Points * percentToSteal / 100);
            if (_pointsRepository.SubtractPoints(stealUser, amountToSteal))
            {
                stealUserSettings.StealthGrabStolenLastDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(stealUser, nameof(stealUserSettings.StealthGrabStolenLastDate), stealUserSettings.StealthGrabStolenLastDate.ToString(), false);

                if (_pointsRepository.AddPoints(user, amountToSteal))
                {
                    _itemsRepository.RemoveItem(user, stealth);

                    _notificationsRepository.AddNotification(stealUser, $"@{user.Username} used a Stealth Grab and stole {Helper.GetFormattedPoints(amountToSteal)} from your {Resources.Currency} on hand!", NotificationLocation.General, "#");

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.StolePoints))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.StolePoints);
                    }

                    List<Accomplishment> stealUserAccomplishments = _accomplishmentsRepository.GetAccomplishments(stealUser, false);
                    if (!stealUserAccomplishments.Exists(x => x.Id == AccomplishmentId.RobbedPoints))
                    {
                        _accomplishmentsRepository.AddAccomplishment(stealUser, AccomplishmentId.RobbedPoints, false);
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"You used your Stealth Grab and successfully stole {Helper.GetFormattedPoints(amountToSteal)} from {stealUser.Username}!", Points = Helper.GetFormattedPoints(user.Points) });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your Stealth Grab couldn't be used with this user." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CompleteSection(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            bool hasShowroom = accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess);

            List<Guid> itemIds = GetItemIds(user, hasShowroom);

            Settings settings = _settingsRepository.GetSettings(user);
            List<int> redeemedIds = settings.RedeemedGuideIds.Split('-').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x)).ToList();

            ItemSection itemSection = _guideRepository.GetItemSections(itemIds, redeemedIds).Find(x => x.Id == id);
            if (itemSection == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This section does not exist." });
            }
            if (!itemSection.IsCompleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have not completed this section." });
            }
            if (itemSection.IsRedeemed)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have already redeemed your reward for this section." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to redeem the reward." });
            }

            redeemedIds.Add(itemSection.Id);
            string redeemedIdsString = string.Join("-", redeemedIds);
            settings.RedeemedGuideIds = redeemedIdsString;
            if (_settingsRepository.SetSetting(user, nameof(settings.RedeemedGuideIds), settings.RedeemedGuideIds))
            {
                Item item = itemSection.Items.FindAll(x => !x.IsExclusive).OrderBy(x => x.Points).Last();
                _itemsRepository.AddItem(user, item);
                _pointsRepository.AddPoints(user, 10000);

                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompletedGuideSection))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompletedGuideSection);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully completed the section and the item \"{item.Name}\" was added to your bag and you earned {Helper.GetFormattedPoints(10000)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "The reward for this section could not be redeemed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseDeckUpgrader(Guid upgraderSelectionId, Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Upgrader upgrader = _itemsRepository.GetUpgraders(user).Find(x => x.SelectionId == upgraderSelectionId);
            if (upgrader == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Upgrader." });
            }

            if (upgrader.Type < UpgraderType.Red || (upgrader.Type > UpgraderType.Purple && upgrader.Type != UpgraderType.Negative))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot use this Upgrader to upgrade a {Resources.DeckCard}." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot upgrade a {Resources.DeckCard} while you're in a Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            Card card = _itemsRepository.GetCards(user).Find(x => x.SelectionId == selectionId);
            if (card == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You no longer have this {Resources.DeckCard}." });
            }

            if (upgrader.Type == UpgraderType.Negative)
            {
                if (card.Amount <= 1)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot downgrade this {Resources.DeckCard} with this Upgrader." });
                }
                upgrader.Amount = -upgrader.Amount;
            }
            else
            {
                if (card.Amount >= 100 || card.Color.ToString() != upgrader.Type.ToString())
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot upgrade this {Resources.DeckCard} with this Upgrader." });
                }
            }

            Card newCard = _itemsRepository.SwapCard(user, card, upgrader.Amount);
            if (newCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your {Resources.DeckCard} could not be upgraded." });
            }

            _itemsRepository.RemoveItem(user, upgrader);

            Messages.AddSnack($"You successfully {(upgrader.Type == UpgraderType.Negative ? "downgraded" : "upgraded")} your {Resources.DeckCard} and it became \"{newCard.Name}\"!");

            return Json(new Status { Success = true, ReturnUrl = $"/Items#{newCard.SelectionId}" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseCompanionCandy(Guid candySelectionId, Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Candy candy = _itemsRepository.GetCandy(user).Find(x => x.SelectionId == candySelectionId);
            if (candy == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Candy Card." });
            }
            if (candy.Type != CandyType.Companion)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must use a Companion Candy." });
            }

            Companion companion = _companionsRepository.GetCompanions(user).Find(x => x.Id == companionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Companion." });
            }

            companion.Level++;
            if (_companionsRepository.UpdateCompanion(companion))
            {
                _itemsRepository.RemoveItem(user, candy);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (companion.Level + 1 >= 10)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelTen))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelTen);
                    }

                }
                if (companion.Level + 1 >= 25)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelTwentyFive))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelTwentyFive);
                    }
                }
                if (companion.Level + 1 >= 50)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelFifty))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelFifty);
                    }
                }

                Messages.AddSnack($"{companion.Name} successfully increased to Level {Helper.FormatNumber(companion.Level + 1)}!");

                return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{companion.Id}" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Companion Candy could not be used at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseCompanionDye(Guid candySelectionId, Guid companionId, CompanionColor? selectedColor)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Candy candy = _itemsRepository.GetCandy(user).Find(x => x.SelectionId == candySelectionId);
            if (candy == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Candy Card." });
            }
            if (candy.Type != CandyType.CompanionDye)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must use a Companion Dye Candy." });
            }

            Companion companion = _companionsRepository.GetCompanions(user).Find(x => x.Id == companionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Companion." });
            }

            if (candy.Amount == 0)
            {
                List<CompanionColor> companionColors = Enum.GetValues(typeof(CompanionColor)).Cast<CompanionColor>().ToList();
                companionColors.RemoveAll(x => x == companion.Color);

                Random rnd = new Random();
                companion.Color = companionColors[rnd.Next(companionColors.Count)];
            }
            else
            {
                if (selectedColor == null || selectedColor == companion.Color)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must choose a new color for your Companion." });
                }
                companion.Color = selectedColor.Value;
            }

            if (_companionsRepository.UpdateCompanion(companion))
            {
                _itemsRepository.RemoveItem(user, candy);

                Messages.AddSnack($"{companion.Name} successfully had a color change!");

                return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{companion.Id}" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Companion Dye Candy could not be used at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ActivatePowerMove(PowerMoveType type)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot change your active Power Moves while you're in a Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            List<PowerMove> powerMoves = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel);
            PowerMove powerMove = powerMoves.Find(x => x.Type == type);
            if (powerMove == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot use this Power Move as you have not unlocked it yet." });
            }

            if (!powerMove.IsActive && powerMoves.Count(x => x.IsActive) == settings.MaxPowerMoves)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can only have {settings.MaxPowerMoves} active Power Moves. You must remove one before you can activate this one." });
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

            powerMove.IsActive = !powerMove.IsActive;
            if (_powerMovesRepository.UpdatePowerMove(user, powerMove))
            {
                return Json(new Status { Success = true, SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your Power Move could not be {(powerMove.IsActive ? "activated" : "deactivated")}. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChooseDeck(DeckType selectedDeck)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (!settings.UnlockedExtraDeck)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked an extra Deck slot yet." });
            }

            if (selectedDeck == DeckType.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't a valid selection." });
            }

            if (settings.DefaultDeckType == selectedDeck)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have this Deck set as your default Deck." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot change your Deck while you're in a Stadium match.", ButtonText = "Head to the Stadium!", ButtonUrl = "/Stadium" });
            }

            settings.DefaultDeckType = selectedDeck;
            _settingsRepository.SetSetting(user, nameof(settings.DefaultDeckType), settings.DefaultDeckType.ToString());

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetItemsPartialView(bool loadPowerMoves)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            ItemsViewModel itemsViewModel = GenerateModel(user, sort: settings.ItemSort, showPowerMoves: loadPowerMoves);
            return PartialView("_ItemsPartial", itemsViewModel);
        }

        [HttpGet]
        public IActionResult GetItemsGuidePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ItemsGuideViewModel itemsGuideViewModel = GenerateGuideModel(user);
            return PartialView("_ItemsGuidePartial", itemsGuideViewModel);
        }

        private ItemsViewModel GenerateModel(User user, Theme theme = null, Sleeve sleeve = null, Stealth stealth = null, CompanionItem companion = null, Candy selectedCandy = null, Companion selectedCompanion = null, Upgrader upgrader = null, Fairy fairy = null, ItemSort sort = ItemSort.Category, Guid? selectionId = null, bool showPowerMoves = false)
        {
            List<Item> allUserItems = _itemsRepository.GetItems(user);
            ItemsViewModel itemsViewModel = new ItemsViewModel
            {
                Title = "Bag",
                User = user,
                Sort = sort,
                SelectionId = selectionId ?? Guid.Empty
            };

            if (theme != null)
            {
                itemsViewModel.Items = allUserItems.FindAll(x => x.Category == ItemCategory.Deck && x.Theme != theme.Theme);
                itemsViewModel.SelectedTheme = theme;
            }
            else if (sleeve != null)
            {
                allUserItems = allUserItems.FindAll(x => x.Category != ItemCategory.Sleeve);
                itemsViewModel.Items = sleeve.Type == SleeveType.Normal || sleeve.Type == SleeveType.Enhanced ? allUserItems.FindAll(x => x.Condition == ItemCondition.New && (x.RepairedDate - x.CreatedDate).TotalDays < 1) : allUserItems;
                itemsViewModel.SelectedSleeve = sleeve;
            }
            else if (stealth != null)
            {
                itemsViewModel.SelectedStealth = stealth;
            }
            else if (fairy != null)
            {
                itemsViewModel.SelectedFairy = fairy;
            }
            else if (selectedCompanion != null)
            {
                itemsViewModel.SelectedCandy = selectedCandy;
                itemsViewModel.SelectedActivatedCompanion = selectedCompanion;
                itemsViewModel.CompanionColors = Enum.GetValues(typeof(CompanionColor)).Cast<CompanionColor>().ToList();
                itemsViewModel.CompanionColors.RemoveAll(x => x == selectedCompanion.Color);
            }
            else if (companion != null)
            {
                itemsViewModel.SelectedCompanion = companion;
            }
            else if (upgrader != null)
            {
                itemsViewModel.SelectedUpgrader = upgrader;
                itemsViewModel.Items = upgrader.Type == UpgraderType.Negative ? _itemsRepository.GetCards(user).FindAll(x => x.Amount > 1).Select(x => (Item)x).ToList() : _itemsRepository.GetCards(user).FindAll(x => x.Amount < 100 && x.Color.ToString() == upgrader.Type.ToString()).Select(x => (Item)x).ToList();
            }
            else if (selectedCandy != null)
            {
                itemsViewModel.SelectedCandy = selectedCandy;
                if (selectedCandy.Type == CandyType.Companion || selectedCandy.Type == CandyType.CompanionDye)
                {
                    itemsViewModel.Companions = _companionsRepository.GetCompanions(user);
                }
            }
            else
            {
                Settings settings = _settingsRepository.GetSettings(user);
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                itemsViewModel.Items = allUserItems.FindAll(x => x.DeckType == DeckType.None);
                itemsViewModel.Deck = _itemsRepository.GetCards(user, true);
                itemsViewModel.PowerMoves = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel).OrderByDescending(x => x.IsActive).ThenBy(x => x.Name).ToList();
                itemsViewModel.TotalItems = allUserItems.Count;
                itemsViewModel.MaxItemsCount = settings.MaxBagSize - (allUserItems.Count - itemsViewModel.Items.Count);
                itemsViewModel.MaxPowerMoves = settings.MaxPowerMoves;
                itemsViewModel.IsInMatch = _matchStateRepository.IsInMatch(user);
                itemsViewModel.ShowPowerMovesFirst = showPowerMoves;
                itemsViewModel.MaxBagSize = settings.MaxBagSize;
                itemsViewModel.BagAddedLastDate = settings.BagAddedLastDate;
                itemsViewModel.UnlockedExtraDeck = settings.UnlockedExtraDeck;
                itemsViewModel.DeckType = settings.DefaultDeckType;

                itemsViewModel.HasShowroom = accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomAccess);
                itemsViewModel.IsFirstCardPack = (DateTime.UtcNow - (accomplishments.Find(x => x.Id == AccomplishmentId.OpenedCardPack)?.ReceivedDate ?? DateTime.MinValue)).TotalSeconds < 5;
                itemsViewModel.UnearnedCards = allUserItems.FindAll(x => !_relicsRepository.HasEarned(x.Tier, settings.MatchLevel)).Select(x => x.SelectionId).ToList();

                if (!settings.HasSeenBagTutorial)
                {
                    settings.HasSeenBagTutorial = true;
                    itemsViewModel.ShowTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenBagTutorial), settings.HasSeenBagTutorial.ToString());
                }
                else if (!settings.HasSeenBagPowerMovesTutorial && itemsViewModel.PowerMoves.Any(x => x.MinCPULevel != 0))
                {
                    settings.HasSeenBagPowerMovesTutorial = true;
                    itemsViewModel.ShowPowerMovesTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenBagPowerMovesTutorial), settings.HasSeenBagPowerMovesTutorial.ToString());
                    itemsViewModel.ShowPowerMovesFirst = true;
                }
                else if (!settings.HasSeenRelicRequirementTutorial && allUserItems.Any(x => !_relicsRepository.HasEarned(x.Tier, settings.MatchLevel)))
                {
                    settings.HasSeenRelicRequirementTutorial = true;
                    itemsViewModel.ShowRelicRequirementTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenRelicRequirementTutorial), settings.HasSeenRelicRequirementTutorial.ToString());
                }

                if (itemsViewModel.Items.Any(x => x.AddedDate > itemsViewModel.BagAddedLastDate))
                {
                    settings.BagAddedLastDate = itemsViewModel.Items.Select(x => x.AddedDate).Max().AddSeconds(1);
                    _settingsRepository.SetSetting(user, nameof(settings.BagAddedLastDate), settings.BagAddedLastDate.ToString());
                }
            }

            return itemsViewModel;
        }

        private ItemsGuideViewModel GenerateGuideModel(User user)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            bool hasShowroom = accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomAccess);

            List<Guid> itemIds = GetItemIds(user, hasShowroom);

            Settings settings = _settingsRepository.GetSettings(user);
            List<int> redeemedIds = settings.RedeemedGuideIds.Split('-').Where(x => !string.IsNullOrEmpty(x)).Select(x => int.Parse(x)).ToList();

            ItemsGuideViewModel itemsGuideViewModel = new ItemsGuideViewModel
            {
                Title = "Collector's Guide",
                User = user,
                ItemIds = itemIds.Distinct().ToList(),
                ItemSections = _guideRepository.GetItemSections(itemIds, redeemedIds),
                HasShowroom = hasShowroom,
                Parent = "Bag",
                ParentUrl = "Items"
            };

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompletedGuide))
            {
                if (itemsGuideViewModel.ItemSections.All(x => x.IsRedeemed))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompletedGuide);
                }
            }

            return itemsGuideViewModel;
        }

        private List<Item> OpenPack(User user, CardPack cardPack, Settings settings)
        {
            ItemCategory[] excludedItems = { ItemCategory.Pack, ItemCategory.Seed };
            List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, excludedItems);
            allItems.Sort((x, y) => x.Points.CompareTo(y.Points));

            Random rnd = new Random();
            List<Item> itemsReceived = new List<Item>();
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Exists(x => x.Id == AccomplishmentId.OpenedCardPack))
            {
                int packAmount = 5;
                int value = cardPack.Points / packAmount * (packAmount - 1);
                int itemIndex;
                Item newItem;

                for (int i = 0; i < packAmount - 1; i++)
                {
                    if (value > 0)
                    {
                        List<Item> availableItems = allItems.FindAll(x => x.Points <= value * 1.15);
                        itemIndex = rnd.Next(availableItems.Count);
                        newItem = availableItems[itemIndex];
                        value -= newItem.Points;
                    }
                    else
                    {
                        newItem = allItems[rnd.Next(10 * ((int)cardPack.Type + 1))];
                    }

                    itemsReceived.Add(newItem);
                }

                int odds = 50;
                if (cardPack.Type == CardPackType.Enhanced)
                {
                    odds = 20;
                }
                else if (cardPack.Type == CardPackType.Rare || cardPack.Type == CardPackType.Club)
                {
                    odds = 5;
                }

                if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
                {
                    if (settings.ActiveHoroscope == HoroscopeType.Good)
                    {
                        odds = (int)Math.Floor(odds * 0.9);
                    }
                    else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                    {
                        odds = (int)Math.Ceiling(odds * 1.1);
                    }
                }

                if (rnd.Next(odds) == 1)
                {
                    allItems = allItems.FindAll(x => x.Points > cardPack.Points);
                }
                else
                {
                    allItems = allItems.FindAll(x => x.Points < cardPack.Points * 1.25);
                }

                newItem = allItems[rnd.Next(allItems.Count)];

                itemsReceived.Add(newItem);

                if (newItem.Points >= Math.Max(100000, cardPack.Points))
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigCardPackItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigCardPackItem);
                    }
                }
            }
            else
            {
                List<Item> artCards = allItems.FindAll(x => x.Category == ItemCategory.Art && !x.Description.Contains("Community Contribution")).ToList();
                itemsReceived.Add(allItems.Find(x => x.Category == ItemCategory.Deck && x.Name == "18"));
                itemsReceived.Add(allItems.Find(x => x.Category == ItemCategory.Enhancer && x.Name == "Blocker"));
                itemsReceived.Add(allItems.Find(x => x.Category == ItemCategory.Frozen));
                itemsReceived.Add(artCards[rnd.Next(artCards.Count)]);
                itemsReceived.Add(allItems.Find(x => x.Category == ItemCategory.Enhancer && x.Name == "10% Filler"));

                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OpenedCardPack);
            }

            foreach (Item item in itemsReceived)
            {
                if (item.Category == ItemCategory.Deck && rnd.Next(10) == 1)
                {
                    ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    ItemTheme theme = themes[rnd.Next(themes.Length)];
                    item.Theme = theme;
                }
                item.CreatedDate = DateTime.UtcNow;
                item.RepairedDate = DateTime.UtcNow;
                _itemsRepository.AddItem(user, item);
            }

            return itemsReceived.OrderBy(x => x.Points).ToList();
        }

        private List<Guid> GetItemIds(User user, bool hasShowroom)
        {
            List<Guid> itemIds = new List<Guid>();
            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.Condition < ItemCondition.Unusable);
            itemIds.AddRange(items.Select(x => x.Id));
            List<Item> storageItems = _safetyDepositBoxRepository.GetSafetyDepositItems(user).FindAll(x => x.Condition < ItemCondition.Unusable);
            itemIds.AddRange(storageItems.Select(x => x.Id));
            if (hasShowroom)
            {
                List<Item> showroomItems = _profileRepository.GetShowroomItems(user);
                showroomItems.ForEach(x => x.RepairedDate = x.RepairedDate.AddSeconds((DateTime.UtcNow - x.AddedDate).TotalSeconds));
                showroomItems = showroomItems.FindAll(x => x.Condition < ItemCondition.Unusable);
                itemIds.AddRange(showroomItems.Select(x => x.Id));
            }

            return itemIds;
        }
    }
}
