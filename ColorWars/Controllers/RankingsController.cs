﻿using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class RankingsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private IPointsRepository _pointsRepository;

        public RankingsController(ILoginRepository loginRepository, IMatchHistoryRepository matchHistoryRepository, IPointsRepository pointsRepository)
        {
            _loginRepository = loginRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _pointsRepository = pointsRepository;
        }

        public IActionResult Index(bool wealthActive)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Rankings" });
            }

            RankingsViewModel rankingsViewModel = GenerateModel(user, wealthActive);
            rankingsViewModel.UpdateViewData(ViewData);
            return View(rankingsViewModel);
        }

        private RankingsViewModel GenerateModel(User user, bool wealthRankingsFirst)
        {
            RankingsViewModel rankingsViewModel = new RankingsViewModel
            {
                Title = "Rankings",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Rankings,
                MatchRankings = _matchHistoryRepository.GetTopMatchRankings(),
                WealthRankings = _pointsRepository.GetTopWealthRankings(),
                WealthRankingsFirst = wealthRankingsFirst
            };
            return rankingsViewModel;
        }
    }
}
