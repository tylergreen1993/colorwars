﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ReferralsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IReferralsRepository _referralsRepository;
        private IItemsRepository _itemsRepository;
        private IGroupsRepository _groupsRepository;
        private IEmailRepository _emailRepository;
        private IUserRepository _userRepository;
        private ISettingsRepository _settingsRepository;
        private IAdminSettingsRepository _adminSettingsRepository;

        public ReferralsController(ILoginRepository loginRepository, IReferralsRepository referralsRepository, IItemsRepository itemsRepository,
        IGroupsRepository groupsRepository, IEmailRepository emailRepository, IUserRepository userRepository, ISettingsRepository settingsRepository,
        IAdminSettingsRepository adminSettingsRepository)
        {
            _loginRepository = loginRepository;
            _referralsRepository = referralsRepository;
            _itemsRepository = itemsRepository;
            _groupsRepository = groupsRepository;
            _emailRepository = emailRepository;
            _userRepository = userRepository;
            _settingsRepository = settingsRepository;
            _adminSettingsRepository = adminSettingsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Referrals" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Leaders", "Referrals");
            }

            ReferralsViewModel referralsViewModel = GenerateModel(user, true);
            referralsViewModel.UpdateViewData(ViewData);
            return View(referralsViewModel);
        }

        public IActionResult Leaders()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Referrals/Leaders" });
            }

            ReferralsViewModel referralsViewModel = GenerateModel(user, false);
            referralsViewModel.UpdateViewData(ViewData);
            return View(referralsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitReferral(string emailAddress, bool includeSnack = true)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a valid email address." });
            }

            User userEmail = _userRepository.GetUserWithEmail(emailAddress);
            if (userEmail != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "A user already exists with this email address." });
            }

            string subject = $"You were referred by {user.GetFriendlyName()}!";
            string title = $"{Resources.SiteName} Invitation";
            string body = $"You were invited to join {Resources.SiteName} by your friend, {user.GetFriendlyName()}. To sign up, just click the button below.";
            string url = $"?referral={user.Username}&emailAddress={emailAddress}";

            AdminSettings adminSettings = _adminSettingsRepository.GetSettings();
            if (adminSettings.IsBetaEnabled)
            {
                url = $"{url}&betaCode={adminSettings.BetaCode}";
            }

            _emailRepository.SendEmail(emailAddress, subject, title, body, url);

            if (includeSnack)
            {
                Messages.AddSnack("Your email was successfully sent!");
            }

            return Json(new Status { Success = true });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeReward(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = GetClaimableArtCards(settings, _referralsRepository.GetReferrals(user)).Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot take this item." });
            }

            if (_itemsRepository.AddItem(user, item))
            {
                settings.ReferralItemsClaimed += $"|{item.Id}";
                _settingsRepository.SetSetting(user, nameof(settings.ReferralItemsClaimed), settings.ReferralItemsClaimed);

                return Json(new Status { Success = true, SuccessMessage = "The item was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item could not be added to your bag at this time." });
        }

        [HttpGet]
        public IActionResult GetReferralsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ReferralsViewModel referralsViewModel = GenerateModel(user, true);
            return PartialView("_ReferralsMainPartial", referralsViewModel);
        }

        private ReferralsViewModel GenerateModel(User user, bool isReferral)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            ReferralsViewModel referralsViewModel = new ReferralsViewModel
            {
                Title = "Referrals",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Referrals
            };

            if (isReferral)
            {
                List<Referral> referrals = _referralsRepository.GetReferrals(user);

                int totalEarned = referrals.Sum(x => x.InterestTimes) * Helper.PointsPerReferralCollect() + settings.ExtraReferralPointsEarned;

                string uniqueLink = $"{Helper.GetDomain(HttpContext)}?referral={user.Username}";
                AdminSettings adminSettings = _adminSettingsRepository.GetSettings();
                if (adminSettings.IsBetaEnabled)
                {
                    uniqueLink = $"{uniqueLink}&betaCode={adminSettings.BetaCode}";
                }

                GroupPower groupPower = _groupsRepository.GetGroupPower(user);

                referralsViewModel.Referrals = referrals.OrderByDescending(x => x.TransactionDate).ThenByDescending(x => x.CreatedDate).ToList();
                referralsViewModel.TotalEarned = totalEarned;
                referralsViewModel.UniqueLink = uniqueLink;
                referralsViewModel.HasAdditionalReferralsPower = groupPower == GroupPower.AdditionalReferralsBonus;

                if (referralsViewModel.Referrals.Count > 0)
                {
                    referralsViewModel.Rewards = GetClaimableArtCards(settings, referrals);
                }
            }
            else
            {
                referralsViewModel.TopReferrers = _referralsRepository.GetTopReferrers(30);
            }

            return referralsViewModel;
        }

        private List<Item> GetClaimableArtCards(Settings settings, List<Referral> referrals)
        {
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).FindAll(x => x.IsExclusive && (x.Name == "Endless" || (x.Name == "Maw of the Void" && referrals.Count >= 5)));

            List<Item> claimableArtCards = new List<Item>();
            foreach (Item item in items)
            {
                if (!settings.ReferralItemsClaimed.Contains(item.Id.ToString()))
                {
                    claimableArtCards.Add(item);
                }
            }

            return claimableArtCards;
        }
    }
}
