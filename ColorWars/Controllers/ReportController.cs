﻿using System;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ReportController : Controller
    {
        private ILoginRepository _loginRepository;
        private IBlockedRepository _blockedRepository;
        private IReportsRepository _reportsRepository;
        private IUserRepository _userRepository;
        private INotificationsRepository _notificationsRepository;
        private ICaptchaRepository _captchaRepository;
        private ISettingsRepository _settingsRepository;

        public ReportController(ILoginRepository loginRepository, IBlockedRepository blockedRepository, IReportsRepository reportsRepository,
        IUserRepository userRepository, INotificationsRepository notificationsRepository, ICaptchaRepository captchaRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _blockedRepository = blockedRepository;
            _reportsRepository = reportsRepository;
            _userRepository = userRepository;
            _notificationsRepository = notificationsRepository;
            _captchaRepository = captchaRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index(string username, int attentionHallPostId)
        {
            User reportedUser = _userRepository.GetUser(username);
            if (reportedUser == null)
            {
                return RedirectToAction("Index", "Home");
            }

            User user = _loginRepository.AuthenticateUser();
            if (user != null && user.Username == reportedUser.Username)
            {
                return RedirectToAction("Index", "Home");
            }

            ReportViewModel reportViewModel = GenerateModel(user, reportedUser, attentionHallPostId);
            reportViewModel.UpdateViewData(ViewData);
            return View(reportViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Report(string username, int attentionHallPostId, string reportText, string blockUser, int mathAnswer)
        {
            User user = _loginRepository.AuthenticateUser();

            User reportedUser = _userRepository.GetUser(username);
            if (reportedUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(reportText))
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your reason for reporting {(attentionHallPostId > 0 ? "this post" : reportedUser.Username)} cannot be empty." });
            }
            if (reportText.Length > 1000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your report message is too long." });
            }

            if (user == null && !_captchaRepository.VerifyCaptcha("report", mathAnswer))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your answer to the math problem isn't quite right." });
            }

            if (user != null && blockUser == "on" && reportedUser.Role < UserRole.Admin)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                if (!settings.PartnershipUsername.Equals(reportedUser.Username, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!_blockedRepository.HasBlockedUser(user, reportedUser))
                    {
                        _blockedRepository.AddBlocked(user, reportedUser);
                    }
                }
            }

            if (_reportsRepository.AddReport(user, reportedUser, reportText, attentionHallPostId))
            {
                _notificationsRepository.AddNotification(user, $"Thanks for reporting a {(attentionHallPostId > 0 ? "post that" : "user who")} may be violating our Terms of Service. We take this very seriously and we will be looking into it as quickly as possible.", NotificationLocation.General, "#");
            }

            return Json(new Status { Success = true });
        }

        private ReportViewModel GenerateModel(User user, User reportedUser, int attentionHallPostId)
        {
            string title = $"Report {reportedUser.Username}";
            if (attentionHallPostId > 0)
            {
                title = "Report Post";
            }

            Captcha captcha = _captchaRepository.GenerateCaptcha("report");

            ReportViewModel reportViewModel = new ReportViewModel
            {
                Title = title,
                User = user,
                ReportedUser = reportedUser,
                AttentionHallPostId = attentionHallPostId,
                IsUserBlocked = _blockedRepository.HasBlockedUser(user, reportedUser),
                Number1 = captcha.Number1,
                Number2 = captcha.Number2
            };

            return reportViewModel;
        }
    }
}
