﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ChallengerDomeController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public ChallengerDomeController(ILoginRepository loginRepository, IItemsRepository itemsRepository, 
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"ChallengerDome" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFive))
            {
                Messages.AddSnack("You haven't unlocked the Challenger Dome yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            ChallengerDomeViewModel challengerDomeViewModel = GenerateModel(user);
            challengerDomeViewModel.UpdateViewData(ViewData);
            return View(challengerDomeViewModel);
        }

        private ChallengerDomeViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            int daysSinceLastStreak = (int)(DateTime.UtcNow - settings.ChallengerDomeStreakDate).TotalDays;
            if (daysSinceLastStreak >= 2)
            {
                if (settings.ChallengerDomeStreak > 0)
                {
                    settings.ChallengerDomeStreak = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.ChallengerDomeStreak), settings.ChallengerDomeStreak.ToString());

                    Messages.AddSnack("You lost your streak!", true);
                }
            }
            ChallengerDomeViewModel challengerDomeViewModel = new ChallengerDomeViewModel
            {
                Title = "Challenger Dome",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Deck = _itemsRepository.GetCards(user, true),
                ChallengerDomeStreak = settings.ChallengerDomeStreak,
                CanPlay = (DateTime.UtcNow - settings.LastChallengerDomeMatchDate).TotalMinutes >= 15,
                MatchLevel = settings.MatchLevel
            };
            return challengerDomeViewModel;
        }
    }
}
