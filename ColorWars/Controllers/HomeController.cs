﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;
using ColorWars.Classes;

namespace ColorWars.Controllers
{
    public class HomeController : Controller
    {
        private ILoginRepository _loginRepository;
        private IFeedRepository _feedRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private INotificationsRepository _notificationsRepository;
        private INewsRepository _newsRepository;
        private IReferralsRepository _referralsRepository;
        private IAuctionsRepository _auctionsRepository;
        private ITradeRepository _tradeRepository;
        private IJobsRepository _jobsRepository;
        private IGroupsRepository _groupsRepository;
        private IMarketStallRepository _marketStallRepository;
        private IAttentionHallRepository _attentionHallRepository;
        private IClubRepository _clubRepository;
        private IStoreDiscountRepository _storeDiscountRepository;
        private ICompanionsRepository _companionsRepository;
        private IPartnershipRepository _partnershipRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private IMatchStateRepository _matchStateRepository;
        private IStocksRepository _stocksRepository;
        private ITourneyRepository _tourneyRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IBlockedRepository _blockedRepository;
        private IChecklistRepository _checklistRepository;
        private IDonorRepository _donorRepository;
        private ILocationsRepository _locationsRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IBadgeRepository _badgeRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private ICPURepository _cpuRepository;
        private ISoundsRepository _soundsRepository;

        public HomeController(ILoginRepository loginRepository, IFeedRepository feedRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        INotificationsRepository notificationsRepository, INewsRepository newsRepository, IReferralsRepository referralsRepository, IAuctionsRepository auctionsRepository,
        ITradeRepository tradeRepository, IJobsRepository jobsRepository, IGroupsRepository groupsRepository, IMarketStallRepository marketStallRepository,
        IAttentionHallRepository attentionHallRepository, IClubRepository clubRepository, IStoreDiscountRepository storeDiscountRepository, ICompanionsRepository companionsRepository,
        IPartnershipRepository partnershipRepository, IMatchHistoryRepository matchHistoryRepository, IPowerMovesRepository powerMovesRepository, IMatchStateRepository matchStateRepository,
        IStocksRepository stocksRepository, ITourneyRepository tourneyRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IChecklistRepository checklistRepository, IBlockedRepository blockedRepository, IDonorRepository donorRepository, ILocationsRepository locationsRepository, IPromoCodesRepository promoCodesRepository,
        IBadgeRepository badgeRepository, IAdminSettingsRepository adminSettingsRepository, ICPURepository cPURepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _feedRepository = feedRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _notificationsRepository = notificationsRepository;
            _newsRepository = newsRepository;
            _referralsRepository = referralsRepository;
            _auctionsRepository = auctionsRepository;
            _tradeRepository = tradeRepository;
            _jobsRepository = jobsRepository;
            _groupsRepository = groupsRepository;
            _marketStallRepository = marketStallRepository;
            _attentionHallRepository = attentionHallRepository;
            _clubRepository = clubRepository;
            _storeDiscountRepository = storeDiscountRepository;
            _companionsRepository = companionsRepository;
            _partnershipRepository = partnershipRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _powerMovesRepository = powerMovesRepository;
            _matchStateRepository = matchStateRepository;
            _stocksRepository = stocksRepository;
            _tourneyRepository = tourneyRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _blockedRepository = blockedRepository;
            _checklistRepository = checklistRepository;
            _donorRepository = donorRepository;
            _locationsRepository = locationsRepository;
            _promoCodesRepository = promoCodesRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _badgeRepository = badgeRepository;
            _cpuRepository = cPURepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string referral, string emailAddress, string betaCode, bool highlightChecklist)
        {
            User user = _loginRepository.AuthenticateUser();
            Settings settings = null;
            if (user == null)
            {
                if (!string.IsNullOrEmpty(referral))
                {
                    IResponseCookies cookies = Response.Cookies;
                    CookieOptions options = new CookieOptions
                    {
                        Expires = DateTime.Now.AddMinutes(30)
                    };
                    cookies.Append("Referral", string.Join("|", new List<string> { referral, emailAddress, betaCode }), options);
                }
            }
            else
            {
                settings = _settingsRepository.GetSettings(user);
                if (settings.CurrentIntroStage != IntroStage.Completed)
                {
                    return RedirectToAction("Index", "Intro");
                }
            }

            HomeViewModel homeViewModel = GenerateModel(user, settings, highlightChecklist);
            homeViewModel.UpdateViewData(ViewData);
            return View(homeViewModel);
        }

        [HttpPost]
        public IActionResult HideFeedCategory(FeedCategory category)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (category <= FeedCategory.All)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot hide this feed category." });
            }

            if (_feedRepository.GetHiddenFeedCategories(user).Any(x => x.FeedId == category))
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already hidden this feed category." });
            }

            if (_feedRepository.AddHiddenFeedCategory(user, category))
            {
                Messages.AddSnack($"You successfully hid all {category.GetDisplayName()} posts from your feed!");
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This feed category could not be hidden." });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult RedeemDailyPrize()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            int daysSinceLastDailyPrize = (int)(DateTime.UtcNow - settings.LastLoginBonusDate).TotalDays;
            if (daysSinceLastDailyPrize < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've redeemed your last daily bonus too recently." });
            }

            List<BonusPrize> bonusPrizes = GetBonusPrizes(user, settings);
            if (bonusPrizes.All(x => x.IsRedeemed))
            {
                settings.LoginBonusStreak = 0;
                _settingsRepository.SetSetting(user, nameof(settings.LoginBonusStreak), settings.LoginBonusStreak.ToString());
                bonusPrizes = GetBonusPrizes(user, settings);
            }

            BonusPrize nextBonus = bonusPrizes.Find(x => !x.IsRedeemed);
            if (_pointsRepository.AddPoints(user, nextBonus.Reward))
            {
                string successMessage = $"You successfully redeemed your Day {nextBonus.Day} daily bonus and earned {Helper.GetFormattedPoints(nextBonus.Reward)}!";
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DailyBonusEarned))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DailyBonusEarned);
                }
                if (nextBonus.Day == 7)
                {
                    settings.LoginBonusStreak = 0;
                    successMessage = $"Congratulations! You successfully redeemed your daily bonus for a week straight and earned the top prize of {Helper.GetFormattedPoints(nextBonus.Reward)}!";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);

                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DailyBonusFullWeek))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DailyBonusFullWeek);
                    }
                }
                else
                {
                    settings.LoginBonusStreak += 1;
                }

                settings.LastLoginBonusDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LoginBonusStreak), settings.LoginBonusStreak.ToString());
                _settingsRepository.SetSetting(user, nameof(settings.LastLoginBonusDate), settings.LastLoginBonusDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = successMessage, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your daily bonus could not be redeemed." });
        }

        [HttpGet]
        public IActionResult GetHomePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            HomeViewModel homeViewModel = GenerateModel(user, settings);

            return PartialView("_HomeAuthenticatedPartial", homeViewModel);
        }

        private HomeViewModel GenerateModel(User user, Settings settings, bool highlightChecklist = false)
        {
            HomeViewModel homeViewModel = new HomeViewModel
            {
                Title = user == null ? "Sign Up" : "Feed",
                User = user,
                ShowTitle = false,
                ShowFooter = user == null
            };

            if (user == null)
            {
                return homeViewModel;
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            List<Checklist> checklist = _checklistRepository.GetChecklist(accomplishments);
            AdminSettings adminSettings = _adminSettingsRepository.GetSettings();

            homeViewModel.AdminSettings = adminSettings;
            homeViewModel.BlockedUsers = _blockedRepository.GetBlocked(user);
            homeViewModel.LastFeedVisitDate = settings.LastFeedVisitDate;
            homeViewModel.Checklist = checklist;
            homeViewModel.NextChallenger = _cpuRepository.GetCPU(settings.MatchLevel);
            homeViewModel.Feed = GetFeed(user, accomplishments, settings, adminSettings, checklist?.FirstOrDefault());

            if (settings.HasSeenTutorial && homeViewModel.Feed.Any(x => x.CreatedDate > settings.LastFeedVisitDate))
            {
                if ((DateTime.UtcNow - settings.LastFeedVisitDate).TotalDays >= 2)
                {
                    Messages.AddSnack($"Welcome back, {user.GetFriendlyName()}!");
                }

                homeViewModel.CanHighlightFeeds = true;

                if (homeViewModel.Checklist != null)
                {
                    homeViewModel.HighlightChecklist = true;
                }

                settings.LastFeedVisitDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastFeedVisitDate), settings.LastFeedVisitDate.ToString());
            }

            if ((DateTime.UtcNow - settings.LastLoginBonusDate).TotalDays >= 1)
            {
                homeViewModel.BonusPrizes = GetBonusPrizes(user, settings);
            }

            UnreadCount unreadCount = _notificationsRepository.GetUnreadCount(user);
            homeViewModel.UnreadNotifications = unreadCount.Notifications;
            homeViewModel.UnreadMessages = unreadCount.Messages;
            homeViewModel.FavoritedLocations = _locationsRepository.GetFavoritedPlazaList(user, settings);
            homeViewModel.Accomplishments = accomplishments;
            homeViewModel.HighlightChecklist = highlightChecklist;
            homeViewModel.ActiveBadge = _badgeRepository.GetActiveBadge(settings.ActiveBadge);
            homeViewModel.IsJoinDay = user.CreatedDate.Year != DateTime.UtcNow.Year && user.CreatedDate.DayOfYear == DateTime.UtcNow.DayOfYear;

            if (checklist != null && settings.LastCheckedChecklistRank != checklist.First().Rank)
            {
                homeViewModel.ShowChecklistAnimation = true;
                Messages.AddSnack($"Congratulations! You completed your checklist tasks and graduated to the {checklist.First().Rank.GetDisplayName()} rank!");
                settings.LastCheckedChecklistRank = checklist.First().Rank;
                _settingsRepository.SetSetting(user, nameof(settings.LastCheckedChecklistRank), settings.LastCheckedChecklistRank.ToString());
            }

            if (!settings.HasSeenTutorial)
            {
                settings.HasSeenTutorial = true;
                homeViewModel.ShowTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenTutorial), settings.HasSeenTutorial.ToString());
                settings.LastFeedVisitDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastFeedVisitDate), settings.LastFeedVisitDate.ToString());
            }
            else if (!user.AllowNotifications && !settings.HasSeenEnableNotifications && user.GetAgeInMinutes() >= 30)
            {
                settings.HasSeenEnableNotifications = true;
                homeViewModel.ShowEnableNotifications = _settingsRepository.SetSetting(user, nameof(settings.HasSeenEnableNotifications), settings.HasSeenEnableNotifications.ToString());
            }
            else if (!settings.HasSeenReferUserTutorial && accomplishments.Count >= 20 && !accomplishments.Any(x => x.Id == AccomplishmentId.ReferredUser))
            {
                settings.HasSeenReferUserTutorial = true;
                homeViewModel.ShowReferUserTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenReferUserTutorial), settings.HasSeenReferUserTutorial.ToString());
            }
            else if (!settings.HasSeenSponsorTutorial && accomplishments.Count >= 35 && !accomplishments.Any(x => x.Id == AccomplishmentId.SponsorSocietyDonor))
            {
                settings.HasSeenSponsorTutorial = true;
                homeViewModel.ShowSponsorTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenSponsorTutorial), settings.HasSeenSponsorTutorial.ToString());
            }
            else if (!settings.EnableSoundEffects && !settings.HasSeenEnableSoundEffects && accomplishments.Count >= 5)
            {
                settings.HasSeenEnableSoundEffects = true;
                homeViewModel.ShowEnableSFX = _settingsRepository.SetSetting(user, nameof(settings.HasSeenEnableSoundEffects), settings.HasSeenEnableSoundEffects.ToString());
            }
            else if (!settings.HasSeenMessagesPopup && !accomplishments.Any(x => x.Id == AccomplishmentId.PromoRedeemed) && accomplishments.Count >= 15)
            {
                settings.HasSeenMessagesPopup = true;
                homeViewModel.ShowMessagesPopup = _settingsRepository.SetSetting(user, nameof(settings.HasSeenMessagesPopup), settings.HasSeenMessagesPopup.ToString());
            }

            return homeViewModel;
        }

        private List<Feed> GetFeed(User user, List<Accomplishment> accomplishments, Settings settings, AdminSettings adminSettings, Checklist nextChecklist)
        {
            List<Feed> feed = new List<Feed>();

            feed.AddRange(_feedRepository.ConvertFrom(user));
            feed.AddRange(_feedRepository.ConvertFrom(user, settings));

            List<HiddenFeedCategory> hiddenFeedCategories = _feedRepository.GetHiddenFeedCategories(user);

            if (settings.ShowNewsOnFeed && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.News))
            {
                List<News> allNews = _newsRepository.GetNews().Take(5).ToList();
                foreach (News news in allNews)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(news));
                }
            }

            feed.AddRange(_feedRepository.ConvertFrom(user, accomplishments, settings));
            AddAccomplishments(user, accomplishments);

            if (accomplishments.Exists(x => x.Id == AccomplishmentId.ReferredUser) && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Referrals))
            {
                List<Referral> referrals = _referralsRepository.GetReferrals(user).Take(10).ToList();
                foreach (Referral referral in referrals)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(referral));
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Auctions))
            {
                List<Auction> auctions = _auctionsRepository.GetAuctions(user, false, true).Take(10).ToList();
                foreach (Auction auction in auctions)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(user, auction, settings));
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Trades))
            {
                List<Trade> trades = _tradeRepository.GetTrades(user, true, false).Take(10).ToList();
                foreach (Trade trade in trades)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(user, trade));
                }
            }

            if (accomplishments.Count >= 15)
            {
                List<Donor> donors = _donorRepository.GetDonorsWithDays(7, false);
                feed.AddRange(_feedRepository.ConvertFrom(donors, adminSettings));
            }

            List<PromoCode> promoCodes = _promoCodesRepository.GetAllUnexpiredSponsorCodes();
            feed.AddRange(_feedRepository.ConvertFrom(promoCodes));

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Jobs))
            {
                if (accomplishments.Exists(x => x.Id == AccomplishmentId.Helper || x.Id == AccomplishmentId.JobAccepted))
                {
                    List<JobSubmission> jobSubmissions = _jobsRepository.GetJobSubmissionsForUser(user).Take(10).ToList();
                    foreach (JobSubmission jobSubmission in jobSubmissions)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(jobSubmission));
                    }
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.RepairShop))
            {
                List<Item> items = _itemsRepository.GetItems(user).OrderByDescending(x => x.Points).ToList();
                int index = 0;
                foreach (Item item in items)
                {
                    if (item.DeckType == settings.DefaultDeckType || index < 10)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(item));
                    }
                    index++;
                }
            }

            if (accomplishments.Exists(x => x.Id == AccomplishmentId.GroupJoined) && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Groups))
            {
                List<Group> groups = _groupsRepository.GetGroups(user, false, false);
                foreach (Group group in groups)
                {
                    if (group != null)
                    {
                        List<GroupRequest> groupRequests = null;
                        if (group.IsPrivate && settings.NotificationsForGroupJoinRequests && group.Role > GroupRole.Standard)
                        {
                            groupRequests = _groupsRepository.GetGroupRequests(group).FindAll(x => !x.Accepted).ToList();
                        }
                        feed.AddRange(_feedRepository.ConvertFrom(group, groupRequests, settings.ShowUnreadGroupMessagesOnFeed));
                    }
                }
            }

            if (accomplishments.Exists(x => x.Id == AccomplishmentId.OpenedMarketStall) && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.MarketStalls))
            {
                List<MarketStallHistory> marketStallHistories = _marketStallRepository.GetMarketStallHistory(user);
                if (marketStallHistories != null)
                {
                    marketStallHistories = marketStallHistories.Take(15).ToList();
                    List<MarketStallItem> marketStallItems = _marketStallRepository.GetMarketStall(user)?.Items ?? null;
                    if (marketStallItems != null)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(marketStallHistories, marketStallItems));
                    }
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.AttentionHall))
            {
                if (settings.ReceiveAttentionHallPostsCommentsNotifications && accomplishments.Exists(x => x.Id == AccomplishmentId.FirstAttentionHallPost))
                {
                    List<AttentionHallComment> attentionHallComments = _attentionHallRepository.GetAttentionHallCommentsFromPosts(user);
                    attentionHallComments.RemoveAll(x => x.Username == user.Username);
                    IEnumerable<IGrouping<int, AttentionHallComment>> attentionHallCommentsGrouping = attentionHallComments.GroupBy(x => x.PostId);
                    foreach (IGrouping<int, AttentionHallComment> attentionHallComment in attentionHallCommentsGrouping.Take(25))
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(attentionHallComment));
                    }
                }

                if (settings.ShowTrendingAttentionHallPostsOnFeed)
                {
                    AttentionHallPost attentionHallPost = _attentionHallRepository.GetMostPopularAttentionHallPost(user);
                    if (attentionHallPost != null)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(attentionHallPost));
                    }
                }
            }

            if (accomplishments.Exists(x => x.Id == AccomplishmentId.JoinedClub) && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Club))
            {
                ClubMembership clubMembership = _clubRepository.GetCurrentMembership(user);
                if (clubMembership != null)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(clubMembership));
                }
                DateTime membershipEndDate = _clubRepository.GetMembershipEndDate(user);
                if (membershipEndDate > DateTime.MinValue)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(FeedCategory.Club, membershipEndDate));
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Store))
            {
                ItemCategory discountItemCategory = _storeDiscountRepository.GetDiscountCategory(user);
                if (discountItemCategory != ItemCategory.None)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(discountItemCategory));
                }
            }

            if (settings.ShowCompanionsOnProfile && accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionActivated) && !hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Companions))
            {
                List<Companion> companions = _companionsRepository.GetCompanions(user);
                foreach (Companion companion in companions)
                {
                    feed.AddRange(_feedRepository.ConvertFrom(user, companion));
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Partnership))
            {
                if (settings.PartnershipEnabled && accomplishments.Exists(x => x.Id == AccomplishmentId.PartnershipFormed))
                {
                    if (string.IsNullOrEmpty(settings.PartnershipUsername))
                    {
                        if (settings.AllowPartnershipRequests)
                        {
                            List<PartnershipRequest> partnershipRequests = _partnershipRepository.GetPartnershipRequests(user).Take(10).ToList();
                            foreach (PartnershipRequest partnershipRequest in partnershipRequests)
                            {
                                feed.AddRange(_feedRepository.ConvertFrom(partnershipRequest));
                            }
                        }
                    }
                    else
                    {
                        List<PartnershipEarning> partnershipEarnings = _partnershipRepository.GetEarningsFromPartnership(user);
                        foreach (PartnershipEarning partnershipEarning in partnershipEarnings)
                        {
                            feed.AddRange(_feedRepository.ConvertFrom(partnershipEarning));
                        }
                    }
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Stocks))
            {
                if (accomplishments.Exists(x => x.Id == AccomplishmentId.StockBuy))
                {
                    List<Stock> stocks = _stocksRepository.GetStocks(user).FindAll(x => (DateTime.UtcNow - x.PurchaseDate).TotalDays >= 30);
                    feed.AddRange(_feedRepository.ConvertFrom(stocks));
                }
            }

            bool isElite = _matchHistoryRepository.IsElite(user);
            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Tourney))
            {
                if (settings.MatchLevel >= 2)
                {
                    List<Tourney> tourneys = _tourneyRepository.GetUserTourneys(user, true, false);
                    feed.AddRange(_feedRepository.ConvertFrom(user, tourneys));

                    Tourney trendingTourney = _tourneyRepository.GetTrendingTourney(user, isElite);
                    if (trendingTourney != null)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(trendingTourney));
                    }
                }
            }

            if (!hiddenFeedCategories.Any(x => x.FeedId == FeedCategory.Stadium))
            {
                List<PowerMove> powerMoves = _powerMovesRepository.GetPowerMoves(user, null, settings.MatchLevel);
                feed.AddRange(_feedRepository.ConvertFrom(powerMoves));
                if (accomplishments.Exists(x => x.Id == AccomplishmentId.FirstCPUMatch))
                {
                    List<MatchHistory> matchHistories = _matchHistoryRepository.GetMatchHistory(user);
                    MatchHistory matchHistory = matchHistories.Find(x => x.CPUType == CPUType.Normal);
                    if (matchHistory != null)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(user, matchHistory, settings, accomplishments));
                    }

                    List<MatchWaitlist> matchWaitlists = _matchStateRepository.GetMatchWaitlists(user, isElite);
                    if (matchWaitlists.Any(x => x.Username != user.Username))
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(user, matchWaitlists));
                    }
                }

                if (accomplishments.Exists(x => x.Id == AccomplishmentId.FirstUserMatch))
                {
                    List<MatchTag> matchTags = _matchHistoryRepository.GetMatchTags(user).Take(10).ToList();
                    foreach (MatchTag matchTag in matchTags)
                    {
                        feed.AddRange(_feedRepository.ConvertFrom(matchTag));
                    }
                }
            }

            if (nextChecklist != null)
            {
                feed.AddRange(_feedRepository.ConvertFrom(user, nextChecklist));
            }

            foreach (HiddenFeedCategory hiddenFeedCategory in hiddenFeedCategories)
            {
                if (hiddenFeedCategory.FeedId != FeedCategory.All && hiddenFeedCategory.FeedId != FeedCategory.SponsorSociety)
                {
                    feed.RemoveAll(x => x.Category == hiddenFeedCategory.FeedId);
                }
            }

            return feed.FindAll(x => x.CreatedDate <= DateTime.UtcNow && x.CreatedDate >= user.CreatedDate).OrderByDescending(x => x.CreatedDate).ThenByDescending(x => x.Category).Take(150).ToList();
        }

        private List<BonusPrize> GetBonusPrizes(User user, Settings settings)
        {
            bool isExpired = false;
            if (settings.LoginBonusStreak > 0 && (DateTime.UtcNow - settings.LastLoginBonusDate).TotalDays >= 2)
            {
                isExpired = true;
                settings.LoginBonusStreak = 0;
                _settingsRepository.SetSetting(user, nameof(settings.LoginBonusStreak), settings.LoginBonusStreak.ToString());
            }

            List<BonusPrize> bonusPrizes = new List<BonusPrize>
            {
                new BonusPrize
                {
                    Day = 1,
                    Reward = 500,
                    ImageUrl = "DailyBonus/Bag.png"
                },
                new BonusPrize
                {
                    Day = 2,
                    Reward = 1000,
                    ImageUrl = "DailyBonus/Bag.png"
                },
                new BonusPrize
                {
                    Day = 3,
                    Reward = 2000,
                    ImageUrl = "DailyBonus/Bag.png"
                },
                new BonusPrize
                {
                    Day = 4,
                    Reward = 3500,
                    ImageUrl = "DailyBonus/DoubleBag.png"
                },
                new BonusPrize
                {
                    Day = 5,
                    Reward = 5000,
                    ImageUrl = "DailyBonus/DoubleBag.png"
                },
                new BonusPrize
                {
                    Day = 6,
                    Reward = 7500,
                    ImageUrl = "DailyBonus/DoubleBag.png"
                },
                new BonusPrize
                {
                    Day = 7,
                    Reward = 25000,
                    ImageUrl = "DailyBonus/TopPrize.png"
                }
            };

            int streak = settings.LoginBonusStreak;
            bonusPrizes.ForEach(x => x.IsActive = streak >= x.Day - 1);
            bonusPrizes.ForEach(x => x.IsRedeemed = streak >= x.Day);
            bonusPrizes.ForEach(x => x.IsExpired = isExpired);

            return bonusPrizes;
        }

        private void AddAccomplishments(User user, List<Accomplishment> accomplishments)
        {
            if (user.CreatedDate.AddYears(1) <= DateTime.UtcNow)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OneYear))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OneYear);
                }
            }
            if (user.CreatedDate.AddMonths(6) <= DateTime.UtcNow)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SixMonths))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SixMonths);
                }
            }
            if (user.CreatedDate.AddMonths(1) <= DateTime.UtcNow)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OneMonth))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OneMonth);
                }
            }
            if (user.GetAgeInDays() >= 7)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OneWeek))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OneWeek);
                }

                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WhiteWhale))
                {
                    Random rnd = new Random();
                    if (rnd.Next(25000) == 1)
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WhiteWhale);
                    }
                }
            }

            List<Checklist> checklist = _checklistRepository.GetChecklist(accomplishments);
            if (checklist == null)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.AllChecklist))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.AllChecklist);
                }
            }
        }
    }
}
