﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class CraftingController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private ICraftingRepository _craftingRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public CraftingController(ILoginRepository loginRepository, IItemsRepository itemsRepository, ICraftingRepository craftingRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _craftingRepository = craftingRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Crafting" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Furnace", "Crafting");
            }

            CraftingViewModel craftingViewModel = GenerateModel(user, true);
            craftingViewModel.UpdateViewData(ViewData);
            return View(craftingViewModel);
        }

        public IActionResult Furnace()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Crafting/Furnace" });
            }

            CraftingViewModel craftingViewModel = GenerateModel(user, false);
            craftingViewModel.UpdateViewData(ViewData);
            return View(craftingViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Craft(Guid item1, Guid item2, Guid item3)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            List<Guid> recipeCardsIds = new List<Guid> { item1, item2, item3 };
            List<Item> recipeCards = new List<Item>();

            if (recipeCardsIds.Distinct().Count() != 3 || recipeCardsIds.Any(x => x == Guid.Empty))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must use exactly 3 cards to craft a new item." });
            }

            foreach (Guid recipeCardId in recipeCardsIds)
            {
                Item recipeCard = userItems.Find(x => x.SelectionId == recipeCardId);
                if (recipeCard == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "It looks like one of your cards no longer exists." });
                }
                recipeCards.Add(recipeCard);
            }

            if (!recipeCards.All(x => x.Category == recipeCards.First().Category))
            {
                return Json(new Status { Success = false, ErrorMessage = "All the cards in your Recipe need to be the same category." });
            }

            ItemCategory category = recipeCards.First().Category;

            Settings settings = _settingsRepository.GetSettings(user);
            bool hasBetterCraftingLuck = settings.CraftingLuckExpiryDate > DateTime.UtcNow;

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            Item craftedItem = null;
            if (category == ItemCategory.Theme)
            {
                List<Theme> themeCards = _itemsRepository.GetThemeCards(user).FindAll(x => recipeCards.Any(y => y.SelectionId == x.SelectionId));
                if (themeCards.Any(x => x.Theme != themeCards.First().Theme))
                {
                    return Json(new Status { Success = false, ErrorMessage = "All your Theme Cards must be the same type." });
                }

                ItemTheme chosenTheme = themeCards.First().Theme;
                if (chosenTheme == ItemTheme.Club)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You cannot craft using Club Themes." });
                }

                List<Item> allThemeCards = _itemsRepository.GetAllItems(ItemCategory.Theme, null, true);
                craftedItem = allThemeCards.Find(x => x.Name == $"{chosenTheme} Theme - Deck");

                if (craftedItem == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "The crafting didn't work as expected! Please try again." });
                }

                foreach (Theme theme in themeCards)
                {
                    _itemsRepository.RemoveItem(user, theme);
                }
            }
            else if (category == ItemCategory.Crafting)
            {
                List<Crafting> craftingCards = _itemsRepository.GetCrafting(user).FindAll(x => recipeCards.Any(y => y.SelectionId == x.SelectionId));
                if (craftingCards.FindAll(x => x.Color == CardColor.Club).Count > 1)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This isn't a valid recipe. You can only use 1 Club Crafting Card in a recipe." });
                }

                Random rnd = new Random();
                if (craftingCards.Any(x => x.Type == CraftingType.Partial))
                {
                    if (!craftingCards.All(x => x.Type == CraftingType.Partial))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "This isn't a valid recipe. You can can only use a Partial Crafting Card with other Partial Crafting Cards." });
                    }

                    if (craftingCards.Any(x => x.Color != craftingCards.First().Color))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "This isn't a valid recipe. All your Partial Crafting Cards must be the same color." });
                    }

                    List<Item> items = _itemsRepository.GetAllItems(ItemCategory.Crafting);
                    craftedItem = items.Find(x => x.Name == craftingCards.First().Color.ToString());
                }
                else
                {
                    bool isEnhancer = false;
                    Crafting clubCard = craftingCards.Find(x => x.Color == CardColor.Club);
                    bool containsClubCard = clubCard != null && craftingCards.RemoveAll(x => x.Color == CardColor.Club) > 0;

                    if (craftingCards.Any(x => x.Color == CardColor.Pink) || !craftingCards.All(x => x.Color == craftingCards.First().Color))
                    {
                        int pinkCardCount = craftingCards.FindAll(x => x.Color == CardColor.Pink).Count;
                        if ((containsClubCard && pinkCardCount < 1) || (!containsClubCard && pinkCardCount < 2))
                        {
                            return Json(new Status { Success = false, ErrorMessage = "You didn't use a valid recipe to craft a new item." });
                        }
                        isEnhancer = true;
                    }

                    if (containsClubCard)
                    {
                        craftingCards.Add(clubCard);
                    }

                    if (isEnhancer)
                    {
                        if (DateTime.UtcNow.Minute == 0)
                        {
                            craftedItem = _itemsRepository.GetAllItems(ItemCategory.Jewel, null, true).Find(x => x.Name == "Eternal");

                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CraftedEternal))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CraftedEternal);
                            }
                        }
                        else
                        {
                            List<Enhancer> enhancers = _itemsRepository.GetAllEnhancers();

                            int totalPoints = craftingCards.Sum(x => x.OriginalPoints);
                            double multiplier = rnd.NextDouble() + (hasBetterCraftingLuck ? 1.25 : 1);
                            craftedItem = enhancers.Aggregate((x, y) => Math.Abs(x.Points - (totalPoints * multiplier)) < Math.Abs(y.Points - (totalPoints * multiplier)) ? x : y);
                        }
                    }
                    else
                    {
                        List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Color == craftingCards.First().Color);
                        foreach (Crafting craftingCard in craftingCards)
                        {
                            if (craftingCard.IsEnhanced)
                            {
                                cards.RemoveRange(0, cards.Count / 4);
                            }
                            if (hasBetterCraftingLuck)
                            {
                                cards.RemoveRange(0, cards.Count / 6);
                            }
                        }
                        craftedItem = cards[rnd.Next(cards.Count)];
                    }
                }

                if (craftedItem == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "The crafting didn't work as expected! Please try again." });
                }

                foreach (Item item in craftingCards)
                {
                    _itemsRepository.RemoveItem(user, item);
                }
            }
            else
            {
                List<Item> otherCards = _itemsRepository.GetItems(user).FindAll(x => GetAllowedOtherCards().Any(y => y.Id == x.Id));
                List<Item> chosenOtherCards = otherCards.FindAll(x => recipeCards.Any(y => y.SelectionId == x.SelectionId));
                if (chosenOtherCards.Any(x => x.Id != chosenOtherCards.First().Id))
                {
                    return Json(new Status { Success = false, ErrorMessage = "All your cards must be the same card." });
                }

                Item chosenItem = chosenOtherCards.First();
                switch (chosenItem.Name)
                {
                    case "Companion Dye":
                        craftedItem = _itemsRepository.GetAllItems(ItemCategory.Candy, null, true).Find(x => x.Name == "Companion Dye - Enhanced");
                        break;
                    default:
                        return Json(new Status { Success = false, ErrorMessage = "These cards cannot craft a new card." });
                }

                if (craftedItem == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "The crafting didn't work as expected! Please try again." });
                }

                foreach (Item item in chosenOtherCards)
                {
                    _itemsRepository.RemoveItem(user, item);
                }
            }

            _itemsRepository.AddItem(user, craftedItem);

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstCrafting))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstCrafting);
            }
            if (craftedItem.Points >= 50000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigCrafting))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigCrafting);
                }
            }
            if (craftedItem.Points >= 200000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HugeCrafting))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HugeCrafting);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You crafted the item \"{craftedItem.Name}\" and it was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddToFurnace(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item item = _craftingRepository.GetFurnaceItem(user);
            if (item != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an item in the Furnace." });
            }

            Frozen frozenItem = _itemsRepository.GetFrozenItems(user).Find(x => x.SelectionId == selectionId);
            if (frozenItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            if (_craftingRepository.AddFurnaceItem(user, frozenItem))
            {
                _itemsRepository.RemoveItem(user, frozenItem);

                Settings settings = _settingsRepository.GetSettings(user);
                settings.FurnaceDefrostDate = DateTime.UtcNow.AddDays((int)frozenItem.Type);

                _settingsRepository.SetSetting(user, nameof(settings.FurnaceDefrostDate), settings.FurnaceDefrostDate.ToString());

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = "Your item was successfully added to the Furnace!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be added to the Furnace." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetFurnaceItem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item item = _craftingRepository.GetFurnaceItem(user);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You currently don't have an item in the Furnace." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.FurnaceDefrostDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your item hasn't been defrosted yet." });
            }

            if (item.Category == ItemCategory.Frozen)
            {
                item = _craftingRepository.DefrostItem(user, item);
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_craftingRepository.DeleteFurnaceItem(item))
            {
                _itemsRepository.AddItem(user, item);

                settings.FurnaceDefrostDate = DateTime.MinValue;
                _settingsRepository.SetSetting(user, nameof(settings.FurnaceDefrostDate), settings.FurnaceDefrostDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DefrostedItem))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DefrostedItem);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully added the item \"{item.Name}\" to your bag!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your item couldn't be removed from the Furnace." });
        }

        [HttpGet]
        public IActionResult GetCraftingPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            CraftingViewModel craftingViewModel = GenerateModel(user, true);
            return PartialView("_CraftingMainPartial", craftingViewModel);
        }

        [HttpGet]
        public IActionResult GetFurnacePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            CraftingViewModel craftingViewModel = GenerateModel(user, false);
            return PartialView("_FurnaceMainPartial", craftingViewModel);
        }

        private CraftingViewModel GenerateModel(User user, bool isCrafting)
        {
            CraftingViewModel craftingViewModel = new CraftingViewModel
            {
                Title = "Crafting",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Crafting
            };

            Settings settings = _settingsRepository.GetSettings(user);

            if (isCrafting)
            {
                List<Crafting> craftingCards = _itemsRepository.GetCrafting(user);
                List<IGrouping<CardColor, Crafting>> groupedCraftingCards = craftingCards.FindAll(x => x.Type == CraftingType.Normal).GroupBy(x => x.Color).ToList();
                List<IGrouping<CardColor, Crafting>> groupedPartialCards = craftingCards.FindAll(x => x.Type == CraftingType.Partial).GroupBy(x => x.Color).ToList();
                List<IGrouping<ItemTheme, Theme>> groupedThemedCards = _itemsRepository.GetThemeCards(user).FindAll(x => x.Type == ThemeType.Card && x.Theme != ItemTheme.Club).GroupBy(x => x.Theme).ToList();
                List<IGrouping<string, Item>> groupedOtherCards = _itemsRepository.GetItems(user, true).FindAll(x => GetAllowedOtherCards().Any(y => y.Id == x.Id)).GroupBy(x => x.Name).ToList();
                craftingViewModel.GroupedCraftingCards = groupedCraftingCards;
                craftingViewModel.GroupedPartialCraftingCards = groupedPartialCards;
                craftingViewModel.GroupedThemeCards = groupedThemedCards;
                craftingViewModel.GroupedOtherCards = groupedOtherCards;
                craftingViewModel.BetterCraftingLuckExpiryDate = settings.CraftingLuckExpiryDate;
            }
            else
            {
                craftingViewModel.FrozenItems = _itemsRepository.GetFrozenItems(user);
                craftingViewModel.FurnaceItem = _craftingRepository.GetFurnaceItem(user);
                craftingViewModel.DefrostDate = settings.FurnaceDefrostDate;

                if (craftingViewModel.FurnaceItem != null && craftingViewModel.FurnaceItem.Category == ItemCategory.Frozen && craftingViewModel.DefrostDate < DateTime.UtcNow)
                {
                    craftingViewModel.FurnaceItem = _craftingRepository.DefrostItem(user, craftingViewModel.FurnaceItem);
                }
            }

            return craftingViewModel;
        }

        private List<Item> GetAllowedOtherCards()
        {
            List<Item> allowedOtherCards = new List<Item>();

            List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);

            allowedOtherCards.Add(allItems.Find(x => x.Category == ItemCategory.Candy && x.Name == "Companion Dye"));

            return allowedOtherCards;
        }
    }
}
