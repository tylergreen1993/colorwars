﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class AdminController : Controller
    {
        private ILoginRepository _loginRepository;
        private INewsRepository _newsRepository;
        private IJobsRepository _jobsRepository;
        private IUserRepository _userRepository;
        private IBankRepository _bankRepository;
        private IReportsRepository _reportsRepository;
        private IContactsRepository _contactsRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IMessagesRepository _messagesRepository;
        private ISuspensionRepository _suspensionRepository;
        private IGroupsRepository _groupsRepository;
        private IMarketStallRepository _marketStallRepository;
        private IPasswordResetRepository _passwordResetRepository;
        private IItemsRepository _itemsRepository;
        private ISurveyRepository _surveyRepository;
        private INotificationsRepository _notificationsRepository;
        private IAdminHistoryRepository _adminHistoryRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private IStatisticsRepository _statisticsRepository;
        private IWishingStoneRepository _wishingStoneRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private IGoogleStorageRepository _googleStorageRepository;

        public AdminController(ILoginRepository loginRepository, INewsRepository newsRepository, IJobsRepository jobsRepository,
        IUserRepository userRepository, IBankRepository bankRepository, IReportsRepository reportsRepository, IContactsRepository contactsRepository,
        IPromoCodesRepository promoCodesRepository, IMessagesRepository messagesRepository, ISuspensionRepository suspensionRepository,
        IGroupsRepository groupsRepository, IMarketStallRepository marketStallRepository, IPasswordResetRepository passwordResetRepository,
        IItemsRepository itemsRepository, ISurveyRepository surveyRepository, INotificationsRepository notificationsRepository,
        IAdminHistoryRepository adminHistoryRepository, IAdminSettingsRepository adminSettingsRepository, IStatisticsRepository statisticsRepository,
        IWishingStoneRepository wishingStoneRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IEmailRepository emailRepository, IGoogleStorageRepository googleStorageRepository)
        {
            _loginRepository = loginRepository;
            _newsRepository = newsRepository;
            _jobsRepository = jobsRepository;
            _userRepository = userRepository;
            _bankRepository = bankRepository;
            _reportsRepository = reportsRepository;
            _contactsRepository = contactsRepository;
            _promoCodesRepository = promoCodesRepository;
            _messagesRepository = messagesRepository;
            _suspensionRepository = suspensionRepository;
            _groupsRepository = groupsRepository;
            _marketStallRepository = marketStallRepository;
            _passwordResetRepository = passwordResetRepository;
            _itemsRepository = itemsRepository;
            _surveyRepository = surveyRepository;
            _notificationsRepository = notificationsRepository;
            _adminHistoryRepository = adminHistoryRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _statisticsRepository = statisticsRepository;
            _wishingStoneRepository = wishingStoneRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _googleStorageRepository = googleStorageRepository;
        }

        public IActionResult Index(ActiveAdminPage page, ActiveAdminPage type, int newsId, bool isHelper, JobPosition jobPosition, string lookup, InactiveAccountType inactiveType, string query, Gender gender)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Admin" });
            }
            if (user.Role < UserRole.Helper)
            {
                return RedirectToAction("Index", "Home");
            }

            AdminViewModel adminViewModel = GenerateModel(user, page, type, newsId, isHelper, jobPosition, inactiveType, lookup, query, gender);
            adminViewModel.UpdateViewData(ViewData);
            return View(adminViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClaimReport(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            List<Report> unclaimedReports = _reportsRepository.GetAllUnclaimedReports(user);
            Report report = unclaimedReports.Find(x => x.Id == id);
            if (report == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This report is no longer unclaimed or no longer exists.", ShouldRefresh = true });
            }

            if (report.Username == user.Username || report.ReportedUser == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot claim this report as it involves you." });
            }

            List<Report> claimedReports = _reportsRepository.GetClaimedReports(user);
            if (claimedReports.Count >= 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot have more than 5 claimed reports at any time." });
            }

            report.ClaimedUser = user.Username;
            _reportsRepository.UpdateReport(user, report);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CompleteReport(Guid id, string outcomeNotes, string status)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (status == "complete" && string.IsNullOrEmpty(outcomeNotes))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must fill out your outcome write-up." });
            }

            List<Report> claimedReports = _reportsRepository.GetClaimedReports(user);
            Report report = claimedReports.Find(x => x.Id == id);
            if (report == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This report is either already completed or no longer exists.", ShouldRefresh = true });
            }

            if (status != null)
            {
                if (status == "complete")
                {
                    report.IsCompleted = true;
                    report.Outcome = outcomeNotes;
                }
                else if (status == "unclaim")
                {
                    report.ClaimedUser = null;
                }
                _reportsRepository.UpdateReport(user, report);
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClaimContact(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            List<Contact> unclaimedContacts = _contactsRepository.GetAllUnclaimedContacts(user);
            Contact contact = unclaimedContacts.Find(x => x.Id == id);
            if (contact == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This contact submission is no longer unclaimed or no longer exists.", ShouldRefresh = true });
            }

            if (contact.Username == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot claim this contact submission as it was submitted by you." });
            }

            List<Contact> claimedContacts = _contactsRepository.GetClaimedContacts(user);
            if (claimedContacts.Count >= 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot have more than 5 claimed contact submissions at any time." });
            }

            contact.ClaimedUser = user.Username;
            _contactsRepository.UpdateContact(user, contact);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveDisplayPicture(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookUpUser = _userRepository.GetUser(username);
            if (lookUpUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (string.IsNullOrEmpty(lookUpUser.DisplayPicUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not have a display picture set." });
            }

            if (lookUpUser.Role >= user.Role || user.Username == lookUpUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot remove this user's display picture." });
            }

            string fileName = lookUpUser.DisplayPicUrl.Split("/").Last();
            lookUpUser.DisplayPicUrl = string.Empty;
            if (_userRepository.UpdateUser(lookUpUser))
            {
                _googleStorageRepository.DeleteFileAsync(fileName);
                _adminHistoryRepository.AddAdminHistory(lookUpUser, user, "Removed display picture.");
                _notificationsRepository.AddNotification(lookUpUser, "Your display picture was removed for abusing the Terms of Service.", NotificationLocation.General, "/Preferences");
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CompleteContact(Guid id, string outcomeNotes, string status)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (status == "complete" && string.IsNullOrEmpty(outcomeNotes))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must fill out your outcome write-up." });
            }

            List<Contact> claimedContacts = _contactsRepository.GetClaimedContacts(user);
            Contact contact = claimedContacts.Find(x => x.Id == id);
            if (contact == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This contact submission is either already completed or no longer exists.", ShouldRefresh = true });
            }

            if (status != null)
            {
                if (status == "complete")
                {
                    contact.IsCompleted = true;
                    contact.Outcome = outcomeNotes;
                }
                else if (status == "unclaim")
                {
                    contact.ClaimedUser = null;
                }
                _contactsRepository.UpdateContact(user, contact);
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GeneratePromoCode(PromoEffect type, int expiresDays, string oneTimeUse, Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (expiresDays < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your promo code must expire in at least 1 day." });
            }

            if (expiresDays > 1000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your promo code cannot expire in more than {Helper.FormatNumber(1000)} days." });
            }

            bool isOneTimeUse = oneTimeUse == "on";

            Item item = null;
            if (itemId != Guid.Empty)
            {
                item = _itemsRepository.GetAllItems(ItemCategory.All, null, true).Find(x => x.Id == itemId);
            }

            bool uniqueStringFound = false;
            string uniqueString = string.Empty;
            while (!uniqueStringFound)
            {
                uniqueString = Helper.GetRandomString(8);
                uniqueStringFound = _promoCodesRepository.AddPromoCode(user, uniqueString, type, isOneTimeUse, false, item, DateTime.UtcNow.AddDays(expiresDays));
            }

            _adminHistoryRepository.AddAdminHistory(user, user, $"Generated {(isOneTimeUse ? "one time use" : "global")} promo code with type \"{type.GetDisplayName()}\"{(item == null ? "." : $" and item \"{item.Name}\".")}");

            return Json(new Status { Success = true, SuccessMessage = $"You successfully generated the promo code: \"{uniqueString}\"!", ButtonText = "Head to Promo Codes!", ButtonUrl = $"/PromoCodes?code={uniqueString}" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddNotes(string username, string notes)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (string.IsNullOrEmpty(notes))
            {
                return Json(new Status { Success = false, ErrorMessage = "You did not write any notes about this user." });
            }

            if (notes.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your notes cannot exceed 500 characters." });
            }

            User lookUpUser = _userRepository.GetUser(username);
            if (lookUpUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (lookUpUser.Username == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot add notes about yourself." });
            }

            _adminHistoryRepository.AddAdminHistory(lookUpUser, user, $"Added Notes: \"{notes}\"");

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SuspendUser(string username, string reason, int suspendDays)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User suspendUser = _userRepository.GetUser(username);
            if (suspendUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == suspendUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot suspend yourself." });
            }

            if (suspendUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot suspend this user." });
            }

            if (suspendUser.IsSuspended)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user is already suspended." });
            }

            if (suspendDays == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot suspend a user for 0 days." });
            }

            if (user.Role < UserRole.Admin && (suspendDays < 0 || suspendDays > 30))
            {
                return Json(new Status { Success = false, ErrorMessage = "You are only allowed to suspend a user for up to 30 days." });
            }

            if (string.IsNullOrEmpty(reason))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a reason for suspension." });
            }

            DateTime suspensionDate;
            if (suspendDays > 0)
            {
                suspensionDate = DateTime.UtcNow.AddDays(suspendDays);
            }
            else
            {
                suspensionDate = DateTime.UtcNow.AddYears(100);
            }

            if (_suspensionRepository.SuspendUser(suspendUser, user, reason, suspensionDate))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user could not be suspended." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UnsuspendUser(string username, string reason)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User suspendedUser = _userRepository.GetUser(username);
            if (suspendedUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == suspendedUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot unsuspend yourself." });
            }

            if (suspendedUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot unsuspend this user." });
            }

            if (!suspendedUser.IsSuspended)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user is not suspended." });
            }

            if (string.IsNullOrEmpty(reason))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a reason for unsuspension." });
            }

            if (_suspensionRepository.UnsuspendUser(suspendedUser, user, reason))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user could not be unsuspended." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BanIPAddress(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookUpUser = _userRepository.GetUser(username);
            if (lookUpUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (lookUpUser.Role > user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot ban this IP Address." });
            }

            if (_userRepository.GetBannedIPAddresses().Any(x => x.IPAddress == lookUpUser.IPAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "This user's IP Address has already been banned." });
            }

            if (_userRepository.AddBannedIPAddress(lookUpUser.IPAddress))
            {
                _adminHistoryRepository.AddAdminHistory(lookUpUser, user, $"Banned IP Address: {lookUpUser.IPAddress}.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user's IP Address could not be banned." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UnbanIPAddress(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookUpUser = _userRepository.GetUser(username);
            if (lookUpUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (lookUpUser.Role > user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot unban this IP Address." });
            }

            if (!_userRepository.GetBannedIPAddresses().Any(x => x.IPAddress == lookUpUser.IPAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "This user's IP Address is not banned." });
            }

            if (_userRepository.DeleteBannedIPAddress(lookUpUser.IPAddress))
            {
                _adminHistoryRepository.AddAdminHistory(lookUpUser, user, $"Unbanned IP Address: {lookUpUser.IPAddress}.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user's IP Address could not have its ban removed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetProfile(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot reset your own profile." });
            }

            if (lookupUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot reset this user's profile." });
            }

            lookupUser.Bio = string.Empty;
            lookupUser.Gender = Gender.None;
            lookupUser.Location = string.Empty;

            if (_userRepository.UpdateUser(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Reset profile information.");
                _notificationsRepository.AddNotification(lookupUser, "Your profile information was reset for violating the Terms of Service.", NotificationLocation.General, "/Terms");
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The user's profile could not be reset." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult FlushSession(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot flush your own session." });
            }

            if (lookupUser.Role > user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot flush this user's session." });
            }

            if (lookupUser.FlushSession)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user's session has already been flushed." });
            }

            lookupUser.FlushSession = true;

            if (_userRepository.UpdateUser(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Flushed session.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The user's session could not be flushed." });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GiveCommunityHelperAccomplishment(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot give yourself this accomplishment." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(lookupUser, false);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.CommunityHelper))
            {
                return Json(new Status { Success = false, ErrorMessage = "This user already has that accomplishment." });
            }

            _accomplishmentsRepository.AddAccomplishment(lookupUser, AccomplishmentId.CommunityHelper, false);

            _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Gave \"Community Helper\" accomplishment.");

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveActiveSessions(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can log out your active sessions in your Preferences." });
            }

            if (lookupUser.Role > user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot log out this user's active sessions." });
            }

            List<ActiveSession> activeSessions = _userRepository.GetActiveSessions(lookupUser);
            if (activeSessions.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user has no active sessions." });
            }

            if (_userRepository.DeleteActiveSessions(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Logged out active sessions.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The user could not not be logged out of their active sessions." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteWishingStoneWishes(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (lookupUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot delete this user's Wishing Stone wishes." });
            }

            if (_wishingStoneRepository.DeleteWishes(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Deleted all Wishing Stone wishes.");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user could not have their Wishing Stone wishes deleted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddSponsorCoins(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role <= UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            Settings lookUpSettings = _settingsRepository.GetSettings(lookupUser, user.Username == lookupUser.Username);
            lookUpSettings.SponsorCoins += 50;
            _settingsRepository.SetSetting(lookupUser, nameof(lookUpSettings.SponsorCoins), lookUpSettings.SponsorCoins.ToString(), user.Username == lookupUser.Username);

            if (user.Username != lookupUser.Username)
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    _notificationsRepository.AddNotification(lookupUser, $"@{user.Username} successfully added 50 Sponsor Coins to your account!", NotificationLocation.SponsorSociety, domain);
                });
            }

            _adminHistoryRepository.AddAdminHistory(lookupUser, user, "Added 50 Sponsor Coins.");

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetPassword(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot generate a reset password link for yourself." });
            }

            if (lookupUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot generate a reset password link for this user." });
            }

            PasswordReset passwordReset = _passwordResetRepository.CreateOrUpdatePasswordReset(lookupUser);
            if (passwordReset == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "A reset password link couldn't be created for this user. Try again later." });
            }

            string domain = Helper.GetDomain(GlobalHttpContext.Current);
            string resetPasswordUrl = $"{domain}/PasswordReset?username={lookupUser.Username}&token={passwordReset.Id}";

            _adminHistoryRepository.AddAdminHistory(lookupUser, user, $"Generated user's reset password link.");

            return Json(new Status { Success = true, SuccessMessage = $"The user's reset password link is: \"{resetPasswordUrl}\"." });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PromoteUser(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot promote yourself." });
            }

            if (lookupUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot promote this user." });
            }

            lookupUser.Role += 1;
            if (_userRepository.UpdateUser(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, $"Promoted to the role of {lookupUser.Role}.");
                _adminHistoryRepository.AddAdminHistory(user, user, $"Promoted {lookupUser.Username} to the role of {lookupUser.Role}.");
                _notificationsRepository.AddNotification(lookupUser, $"You were successfully promoted to the role of {lookupUser.Role}!", NotificationLocation.General, "/Profile");
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user could not be promoted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DemoteUser(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.Admin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User lookupUser = _userRepository.GetUser(username);
            if (lookupUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == lookupUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot demote yourself." });
            }

            if (lookupUser.Role >= user.Role || lookupUser.Role <= UserRole.Default)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot demote this user." });
            }

            lookupUser.Role -= 1;
            if (_userRepository.UpdateUser(lookupUser))
            {
                _adminHistoryRepository.AddAdminHistory(lookupUser, user, $"Demoted to the role of {lookupUser.Role}.");
                _adminHistoryRepository.AddAdminHistory(user, user, $"Demoted {lookupUser.Username} to the role of {lookupUser.Role}.");
                if (user.Role == UserRole.Default)
                {
                    _jobsRepository.DeleteHelperSubmission(user, lookupUser);
                    _notificationsRepository.AddNotification(lookupUser, $"Your Helper role was revoked for violating the Helper guidelines.", NotificationLocation.General, "/");
                }
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This user could not be demoted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateAdminSettings(string enableBetaMode, string betaCode, double weeklySponsorAmount, string enableAds)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.SuperAdmin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            bool allowEnableBetaMode = enableBetaMode == "on";
            bool allowEnableAds = enableAds == "on";

            AdminSettings adminSettings = _adminSettingsRepository.GetSettings();

            if (adminSettings.IsBetaEnabled == allowEnableBetaMode && adminSettings.BetaCode == betaCode && adminSettings.TotalSponsorAmount == weeklySponsorAmount)
            {
                return Json(new Status { Success = false, ErrorMessage = "No changes were made." });
            }

            if (adminSettings.IsBetaEnabled != allowEnableBetaMode)
            {
                adminSettings.IsBetaEnabled = allowEnableBetaMode;
                _adminSettingsRepository.SetSetting(nameof(adminSettings.IsBetaEnabled), adminSettings.IsBetaEnabled.ToString());
            }
            if (!string.IsNullOrEmpty(betaCode) && adminSettings.BetaCode != betaCode)
            {
                adminSettings.BetaCode = betaCode;
                _adminSettingsRepository.SetSetting(nameof(adminSettings.BetaCode), adminSettings.BetaCode);
            }
            if (adminSettings.TotalSponsorAmount != weeklySponsorAmount)
            {
                adminSettings.TotalSponsorAmount = weeklySponsorAmount;
                _adminSettingsRepository.SetSetting(nameof(adminSettings.TotalSponsorAmount), adminSettings.TotalSponsorAmount.ToString());
            }
            if (adminSettings.EnableAds != allowEnableAds)
            {
                adminSettings.EnableAds = allowEnableAds;
                _adminSettingsRepository.SetSetting(nameof(adminSettings.EnableAds), adminSettings.EnableAds.ToString());
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendReturnEmail()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.Role < UserRole.SuperAdmin)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            List<User> users = _userRepository.GetNonReturningUsers(user);
            if (users.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "There are no users to email." });
            }

            string domain = Helper.GetDomain();
            Task.Run(() =>
            {
                foreach (User user in users)
                {
                    Settings settings = _settingsRepository.GetSettings(user, false);
                    settings.HasReceivedReturnMessage = true;
                    if (_settingsRepository.SetSetting(user, nameof(settings.HasReceivedReturnMessage), settings.HasReceivedReturnMessage.ToString(), false))
                    {
                        string code = _promoCodesRepository.GenerateRandomPromoCode(30, PromoEffect.CardPack);
                        string subject = $"How's it going, {user.GetFriendlyName()}?";
                        string title = $"{Resources.SiteName} Follow-Up";
                        string body = $"{Resources.SiteName} is constantly updating with new features and I'd love to get your opinions on them. If you're feeling a bit confused about how everything works, don't worry! You can <a href='{domain}/Contact'>contact</a> me and I'll be happy to answer any questions you have. I hope to see you around, and because a gift's always nice, here's a promo code to get a <b>free Card Pack</b>: <b><a href='{domain}/PromoCodes?code={code}'>{code}</a></b>. You have <b>30 days</b> to redeem it!";

                        Task _ = _emailRepository.SendEmail(user, subject, title, body, $"/PromoCodes?code={code}", false, domain, "Tyler");
                    }
                }
            });

            return Json(new Status { Success = true });
        }

        private AdminViewModel GenerateModel(User user, ActiveAdminPage page, ActiveAdminPage type, int newsId, bool isHelper, JobPosition jobPosition, InactiveAccountType inactiveType, string lookup, string query, Gender gender)
        {
            AdminViewModel adminViewModel = new AdminViewModel
            {
                Title = "Admin",
                User = user,
                ActivePage = page,
                Type = type,
                JobPosition = jobPosition,
                InactiveAccountType = inactiveType == InactiveAccountType.None ? InactiveAccountType.Deleted : inactiveType,
                Reports = _reportsRepository.GetAllUnclaimedReports(user).FindAll(x => !x.ReportedUser.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase)),
                SearchQuery = query,
                Gender = gender
            };

            if (page == ActiveAdminPage.News && newsId > 0)
            {
                News article = _newsRepository.GetNews().Find(x => x.Id == newsId);
                if (article != null)
                {
                    article.Content = article.Content.Replace("<br/>", Environment.NewLine);
                }
                adminViewModel.Article = article;
            }
            else if (page == ActiveAdminPage.Contact)
            {
                adminViewModel.ContactSubmissions = _contactsRepository.GetAllUnclaimedContacts(user);
                adminViewModel.ClaimedContactSubmissions = _contactsRepository.GetClaimedContacts(user);
            }
            else if (page == ActiveAdminPage.Jobs)
            {
                adminViewModel.IsHelper = isHelper;
                adminViewModel.JobSubmissions = _jobsRepository.GetJobSubmissions(user, true).FindAll(x => x.Position != JobPosition.Helper);
                adminViewModel.HelperJobSubmissions = _jobsRepository.GetJobSubmissions(user, true, false, JobPosition.Helper);
            }
            else if (page == ActiveAdminPage.Reports)
            {
                adminViewModel.ClaimedReports = _reportsRepository.GetClaimedReports(user);
            }
            else if (page == ActiveAdminPage.Claims)
            {
                if (type == ActiveAdminPage.Reports)
                {
                    adminViewModel.ClaimedReports = _reportsRepository.GetClaimedReports(user);
                }
                else if (type == ActiveAdminPage.Contact)
                {
                    adminViewModel.ContactSubmissions = _contactsRepository.GetAllUnclaimedContacts(user);
                    adminViewModel.ClaimedContactSubmissions = _contactsRepository.GetClaimedContacts(user);
                }
            }
            else if (page == ActiveAdminPage.Promos)
            {
                adminViewModel.PreviousPromoCodes = _promoCodesRepository.GetAllUnexpiredGlobalPromoCodes(user);
                adminViewModel.Items = _itemsRepository.GetAllItems(ItemCategory.All, null, true).OrderBy(x => x.Category).ThenBy(x => x.Points).ThenBy(x => x.Name).ToList();
            }
            else if (page == ActiveAdminPage.CompletedReports)
            {
                List<Report> reports = _reportsRepository.GetAllCompletedReports(user);
                if (!string.IsNullOrEmpty(query))
                {
                    reports = reports.FindAll(x => x.ReportedUser.Equals(query, StringComparison.InvariantCultureIgnoreCase));
                }
                reports = reports.FindAll(x => !x.ReportedUser.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase));
                adminViewModel.CompletedReports = reports.Take(100).ToList();
            }
            else if (page == ActiveAdminPage.CompletedJobs)
            {
                adminViewModel.JobSubmissions = _jobsRepository.GetJobSubmissions(user, false, true, jobPosition).Take(100).ToList();
            }
            else if (page == ActiveAdminPage.CompletedContact)
            {
                List<Contact> completedContactSubmissions = _contactsRepository.GetAllCompletedContacts(user);
                if (!string.IsNullOrEmpty(query))
                {
                    completedContactSubmissions = completedContactSubmissions.FindAll(x => x.Username.Equals(query, StringComparison.InvariantCultureIgnoreCase));
                }
                adminViewModel.ContactSubmissions = completedContactSubmissions.Take(100).ToList();
            }
            else if (page == ActiveAdminPage.Surveys)
            {
                User surveyUser = null;
                if (!string.IsNullOrEmpty(query))
                {
                    surveyUser = _userRepository.GetUser(query);
                }
                List<SurveyAnswer> recentSurveyAnswers = _surveyRepository.GetSurveyAnswers(surveyUser, user).FindAll(x => x.CreatedDate >= DateTime.UtcNow.AddDays(-90));
                List<IGrouping<string, SurveyAnswer>> surveyAnswers = recentSurveyAnswers.GroupBy(x => x.Question).ToList();
                adminViewModel.SurveyAnswers = surveyAnswers;
                List<int> surveyScoreAnswers = recentSurveyAnswers.FindAll(x => x.SurveyQuestionId == SurveyQuestionId.RecommendedToOthers).Select(x => int.Parse(x.Answer)).ToList();
                adminViewModel.SurveyScore = surveyScoreAnswers.Count > 0 ? (int)Math.Round((100 * (double)surveyScoreAnswers.FindAll(x => x >= 9).Count / surveyScoreAnswers.Count) - (100 * (double)surveyScoreAnswers.FindAll(x => x <= 6).Count / surveyScoreAnswers.Count)) : 0;
            }
            else if (page == ActiveAdminPage.InactiveAccounts)
            {
                if (inactiveType == InactiveAccountType.Disabled)
                {
                    adminViewModel.InactiveAccounts = _userRepository.GetInactiveUsers(user, InactiveAccountType.Disabled);
                }
                else
                {
                    adminViewModel.InactiveAccounts = _userRepository.GetInactiveUsers(user, InactiveAccountType.Deleted);
                }
            }
            else if (page == ActiveAdminPage.Settings)
            {
                adminViewModel.AdminSettings = _adminSettingsRepository.GetSettings();
                adminViewModel.NonReturningUsers = _userRepository.GetNonReturningUsers(user);
            }
            else if (page == ActiveAdminPage.TransferHistory)
            {
                User transferUser = null;
                if (!string.IsNullOrEmpty(query))
                {
                    transferUser = _userRepository.GetUser(query);
                }
                adminViewModel.TransferHistories = _adminHistoryRepository.GetTransferHistory(user, transferUser);
            }
            else if (page == ActiveAdminPage.Members)
            {
                adminViewModel.Users = _userRepository.GetUsersWithAdminRoles().FindAll(x => x.InactiveType == InactiveAccountType.None);
            }
            else if (page == ActiveAdminPage.UserStatistics)
            {
                adminViewModel.RecentUsers = _userRepository.GetRecentUsers(user, gender).FindAll(x => x.InactiveType == InactiveAccountType.None);
                adminViewModel.ReturningUsers = _userRepository.GetReturningUsers(user, gender).FindAll(x => x.InactiveType == InactiveAccountType.None);
                adminViewModel.AllUsers = _userRepository.GetAllUsers(user, gender, 14).FindAll(x => x.InactiveType == InactiveAccountType.None);
                adminViewModel.Statistics = _statisticsRepository.GetStatistics(false);
                adminViewModel.DAUMAUPercent = _userRepository.GetDAUMAUStatistic(user);
                adminViewModel.ActiveUsers = _userRepository.GetActiveUsers(user, 60).OrderByDescending(x => x.ModifiedDate).ToList();
                adminViewModel.RecentlyReturningUsers = _userRepository.GetRecentlyReturningUsers(user).OrderByDescending(x => x.ModifiedDate).ToList();
                adminViewModel.Accomplishments = _accomplishmentsRepository.GetAccomplishmentsRankings();
            }
            else if (page == ActiveAdminPage.Users)
            {
                User lookupUser = _userRepository.GetUser(lookup, false);
                if (lookupUser == null)
                {
                    lookupUser = _userRepository.GetUserWithEmail(lookup);
                    if (lookupUser == null)
                    {
                        adminViewModel.Users = _userRepository.GetUsersByIPAddress(lookup, true);
                        adminViewModel.IPAddress = lookup;
                    }
                }
                if (lookupUser?.InactiveType == InactiveAccountType.Deleted)
                {
                    lookupUser = null;
                }
                if (lookupUser != null)
                {
                    adminViewModel.LookUpUser = lookupUser;
                    adminViewModel.LookUpBank = _bankRepository.GetBank(lookupUser, false);
                    adminViewModel.SuspensionHistories = _suspensionRepository.GetSuspensionHistory(lookupUser, user);
                    adminViewModel.LookUpGroup = _groupsRepository.GetGroups(lookupUser, true, false).Find(x => x.IsPrimary);
                    adminViewModel.LookUpMarketStall = _marketStallRepository.GetMarketStall(lookupUser);
                    adminViewModel.AdminHistories = _adminHistoryRepository.GetAdminHistory(lookupUser, user);
                    adminViewModel.Reports = _reportsRepository.GetReports(user, lookupUser, true).FindAll(x => !x.ReportedUser.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase));
                    adminViewModel.ActiveSessions = _userRepository.GetActiveSessions(lookupUser);
                    adminViewModel.Accomplishments = _accomplishmentsRepository.GetAccomplishments(lookupUser, false).OrderBy(x => x.ReceivedDate).ToList();
                    adminViewModel.LookUpSettings = _settingsRepository.GetSettings(lookupUser, false);
                    adminViewModel.IsIPAddressBanned = _userRepository.GetBannedIPAddresses().Any(x => x.IPAddress == lookupUser.IPAddress);
                    adminViewModel.SameIPUsers = _userRepository.GetUsersByIPAddress(adminViewModel.LookUpUser.IPAddress);

                    if (user.Role > UserRole.Helper)
                    {
                        adminViewModel.ContactSubmissions = _contactsRepository.GetAllContactSubmissions(user, lookupUser).FindAll(x => !x.Username.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase));
                        string logsPath = $"Logs/{lookupUser.Username}_Logs.txt";
                        if (System.IO.File.Exists(logsPath))
                        {
                            List<string> userLogs = System.IO.File.ReadAllText(logsPath).Split(": END").ToList();
                            userLogs = userLogs.FindAll(x => !string.IsNullOrEmpty(x) && x != Environment.NewLine);
                            userLogs.Reverse();
                            adminViewModel.UserLogs = userLogs;
                        }
                    }

                    Dictionary<User, List<Message>> messagesDictionary = new Dictionary<User, List<Message>>();
                    if (user.Username != lookupUser.Username)
                    {
                        List<Report> claimedReports = _reportsRepository.GetClaimedReports(user);
                        foreach (Report claimedReport in claimedReports)
                        {
                            if (lookupUser.Username == claimedReport.Username)
                            {
                                continue;
                            }

                            User reportUser = _userRepository.GetUser(claimedReport.Username);
                            if (reportUser != null)
                            {
                                List<Message> messages = _messagesRepository.GetMessagesBetweenUsernames(reportUser, lookupUser, false);
                                if (messages?.Count > 0)
                                {
                                    messagesDictionary[reportUser] = messages;
                                }
                            }
                        }
                    }
                    adminViewModel.LookUpMessages = messagesDictionary;
                }
            }

            return adminViewModel;
        }
    }
}
