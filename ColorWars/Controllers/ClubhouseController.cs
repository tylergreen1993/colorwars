﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ClubhouseController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ClubhouseController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Clubhouse" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTwentyFive))
            {
                Messages.AddSnack("You haven't unlocked the Clubhouse yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            ClubhouseViewModel clubhouseViewModel = GenerateModel(user);
            clubhouseViewModel.UpdateViewData(ViewData);
            return View(clubhouseViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Join()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.Points < 1000000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTwentyFive))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot access the Clubhouse yet." });
            }

            if (accomplishments.Any(x => x.Id == AccomplishmentId.ClubhouseMember))
            {
                return Json(new Status { Success = false, ErrorMessage = "You're already a member of the Clubhouse." });
            }

            if (_accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ClubhouseMember))
            {
                _pointsRepository.SubtractPoints(user, 1000000);

                Settings settings = _settingsRepository.GetSettings(user);
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully joined the Clubhouse!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You couldn't join the Clubhouse at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyItem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LastClubhouseBuyDate).TotalDays < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought an Art Card too recently from the Clubhouse. Try again in {settings.LastClubhouseBuyDate.AddDays(1).GetTimeUntil()}." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTwentyFive))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot access the Clubhouse yet." });
            }

            Item item = GetDailyItem();
            if (user.Points < item.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item.", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            if (_pointsRepository.SubtractPoints(user, item.Points))
            {
                _itemsRepository.AddItem(user, item);

                settings.LastClubhouseBuyDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastClubhouseBuyDate), settings.LastClubhouseBuyDate.ToString());

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"This item could not be bought." });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConvertTokens(int tokens)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelTwentyFive))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot access the Clubhouse yet." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            int trainingTokens = settings.TrainingTokens;
            int rewardPoints = _accomplishmentsRepository.GetAccomplishmentPoints(user);

            if (tokens < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must use at least 3 Training Tokens." });
            }

            if (tokens % 3 != 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "Every 3 Training Tokens becomes 1 Reward Point. Your amount must be divisible by 3." });
            }

            if (tokens > trainingTokens)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have that many Training Tokens" });
            }

            int additionalRewardPoints = tokens / 3;
            settings.AccomplishmentPointsSpent -= additionalRewardPoints;
            _settingsRepository.SetSetting(user, nameof(settings.AccomplishmentPointsSpent), settings.AccomplishmentPointsSpent.ToString());
            settings.TrainingTokens -= tokens;
            _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You used {Helper.FormatNumber(tokens)} Training Tokens and earned {tokens/3} Reward Point{(tokens/3 == 1 ? "" : "s")}!", ButtonText = "Visit the Rewards Store!", ButtonUrl = "/RewardsStore", SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetClubhousePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ClubhouseViewModel clubhouseViewModel = GenerateModel(user);
            if (clubhouseViewModel.IsMember)
            {
                return PartialView("_ClubhouseMemberPartial", clubhouseViewModel);
            }

            return PartialView("_ClubhouseMainPartial", clubhouseViewModel);
        }

        private ClubhouseViewModel GenerateModel(User user)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            ClubhouseViewModel clubhouseViewModel = new ClubhouseViewModel
            {
                Title = "Clubhouse",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Clubhouse,
                IsMember = accomplishments.Any(x => x.Id == AccomplishmentId.ClubhouseMember),
                SampleItems = GetArtCards().Take(5).ToList()
            };

            if (clubhouseViewModel.IsMember)
            {
                Settings settings = _settingsRepository.GetSettings(user);

                clubhouseViewModel.DailyItem = GetDailyItem();
                clubhouseViewModel.CanBuy = (DateTime.UtcNow - settings.LastClubhouseBuyDate).TotalDays >= 1;
                clubhouseViewModel.TrainingTokens = settings.TrainingTokens;
                clubhouseViewModel.RewardPoints = _accomplishmentsRepository.GetAccomplishmentPoints(user);
            }

            return clubhouseViewModel;
        }

        private List<Item> GetArtCards()
        {
            return _itemsRepository.GetAllItems(ItemCategory.Art, null, true).FindAll(x => x.Points == 500000);
        }

        private Item GetDailyItem()
        {
            List<Item> artCards = GetArtCards();
            return artCards[DateTime.UtcNow.Date.Day % artCards.Count];
        }
    }
}
