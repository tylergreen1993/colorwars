﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class CavernsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ICavernsRepository _cavernsRepository;
        private ICompanionsRepository _companionsRepository;
        private ICraftingRepository _craftingRepository;
        private IUserRepository _userRepository;
        private INotificationsRepository _notificationsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public CavernsController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        ICavernsRepository cavernsRepository, ICompanionsRepository companionsRepository, ICraftingRepository craftingRepository, IUserRepository userRepository,
        INotificationsRepository notificationsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _cavernsRepository = cavernsRepository;
            _companionsRepository = companionsRepository;
            _craftingRepository = craftingRepository;
            _userRepository = userRepository;
            _notificationsRepository = notificationsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Caverns" });
            }

            if (!string.IsNullOrEmpty(tag) && tag == "Seed Cards")
            {
                return RedirectToAction("SeedCards", "Caverns");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            CavernsViewModel cavernsViewModel = GenerateModel(user, true);
            cavernsViewModel.UpdateViewData(ViewData);
            return View(cavernsViewModel);
        }

        public IActionResult SeedCards()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Caverns/SeedCards" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }

            CavernsViewModel cavernsViewModel = GenerateModel(user, false);
            cavernsViewModel.UpdateViewData(ViewData);
            return View(cavernsViewModel);
        }

        public IActionResult RenameCompanion()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Caverns/RenameCompanion" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }
            if (settings.LastSorceressEffectDate.AddHours(2) > DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Caverns");
            }

            SorceressEffect selectedEffect = _cavernsRepository.GetAllSorceressEffects(settings).Find(x => x.Id == SorceressEffectId.RenameCompanion);

            CavernsViewModel cavernsViewModel = GenerateModel(user, true, selectedEffect);
            cavernsViewModel.Companions = _companionsRepository.GetCompanions(user);
            cavernsViewModel.UpdateViewData(ViewData);
            return View(cavernsViewModel);
        }

        public IActionResult CurseUser()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Caverns/CurseUser" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza");
            }
            if (settings.LastSorceressEffectDate.AddHours(2) > DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Caverns");
            }

            SorceressEffect selectedEffect = _cavernsRepository.GetAllSorceressEffects(settings).Find(x => x.Id == SorceressEffectId.CurseUser);

            CavernsViewModel cavernsViewModel = GenerateModel(user, true, selectedEffect);
            cavernsViewModel.UpdateViewData(ViewData);
            return View(cavernsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CastEffect(SorceressEffectId effectId, Guid selectedId, string name)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns.", ShouldRefresh = true });
            }

            int hoursSinceLastEffect = (int)(DateTime.UtcNow - settings.LastSorceressEffectDate).TotalHours;
            if (hoursSinceLastEffect < 2)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've had the Sorceress cast an effect too recently. Try again in {2 - hoursSinceLastEffect} hour{(2 - hoursSinceLastEffect == 1 ? "" : "s")}." });
            }

            SorceressEffect effect = _cavernsRepository.GetAllSorceressEffects(settings).Find(x => x.Id == effectId);
            if (effect == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't an available effect." });
            }

            if (user.Points < effect.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            if (string.IsNullOrEmpty(name))
            {
                if (effect.Id == SorceressEffectId.CurseUser)
                {
                    return Json(new Status { Success = true, ReturnUrl = "/Caverns/CurseUser" });
                }
                if (effect.Id == SorceressEffectId.RenameCompanion)
                {
                    List<Companion> companions = _companionsRepository.GetCompanions(user);
                    if (companions.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have any Companions." });
                    }

                    return Json(new Status { Success = true, ReturnUrl = "/Caverns/RenameCompanion" });
                }
            }

            string buttonText = string.Empty;
            string buttonUrl = string.Empty;
            switch (effect.Id)
            {
                case SorceressEffectId.CompanionHappiness:
                    List<Companion> companions = _companionsRepository.GetCompanions(user);
                    if (companions.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have any Companions." });
                    }

                    foreach (Companion companion in companions)
                    {
                        companion.HappinessDate = DateTime.UtcNow;
                        _companionsRepository.UpdateCompanion(companion);
                    }

                    buttonText = "Visit Companions!";
                    buttonUrl = "/Profile#companions";

                    break;
                case SorceressEffectId.CurseUser:
                    if (name.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You cannot cast a curse upon yourself." });
                    }

                    User cursedUser = _userRepository.GetUser(name);
                    if (cursedUser == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This user does not exist." });
                    }

                    if (cursedUser.GetAgeInDays() < 3)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You cannot cast a curse on this user as they joined less than 3 days ago." });
                    }

                    Settings cursedUserSettings = _settingsRepository.GetSettings(cursedUser, false);
                    if (cursedUserSettings.CurseExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This user already has an active Stadium Curse." });
                    }
                    if (cursedUserSettings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"This user has an active Curse Protection." });
                    }

                    cursedUserSettings.CursePercent = -15;
                    cursedUserSettings.CurseExpiryDate = DateTime.UtcNow.AddMinutes(10);
                    _settingsRepository.SetSetting(cursedUser, nameof(cursedUserSettings.CursePercent), cursedUserSettings.CursePercent.ToString(), false);
                    _settingsRepository.SetSetting(cursedUser, nameof(cursedUserSettings.CurseExpiryDate), cursedUserSettings.CurseExpiryDate.ToString(), false);

                    _notificationsRepository.AddNotification(cursedUser, $"@{user.Username} cast a 15% Stadium Curse on you for CPU matches for the next 10 minutes!", NotificationLocation.Stadium, "/Stadium");

                    break;
                case SorceressEffectId.CrystalBall:
                    settings.LastCrystalBallPlayDate = DateTime.UtcNow.AddHours(-6);
                    _settingsRepository.SetSetting(user, nameof(settings.LastCrystalBallPlayDate), settings.LastCrystalBallPlayDate.ToString());

                    buttonText = "Visit Crystal Ball!";
                    buttonUrl = "/CrystalBall";

                    break;
                case SorceressEffectId.PositiveHoroscope:
                    if (settings.HoroscopeExpiryDate <= DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have an active horoscope." });
                    }
                    if (settings.ActiveHoroscope == HoroscopeType.Good)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have a positive horoscope." });
                    }

                    settings.ActiveHoroscope = HoroscopeType.Good;
                    _settingsRepository.SetSetting(user, nameof(settings.ActiveHoroscope), settings.ActiveHoroscope.ToString());

                    buttonText = "Visit Horoscope!";
                    buttonUrl = "/News/Horoscope";

                    break;
                case SorceressEffectId.ProtectUser:
                    if (settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You already have an active Curse Protection." });
                    }

                    settings.SorceressStadiumCurseProtectExpiryDate = DateTime.UtcNow.AddDays(7);
                    _settingsRepository.SetSetting(user, nameof(settings.SorceressStadiumCurseProtectExpiryDate), settings.SorceressStadiumCurseProtectExpiryDate.ToString());

                    break;
                case SorceressEffectId.RenameCompanion:
                    if (name.Length > 10)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your Companion's name cannot be greater than 10 characters." });
                    }

                    Companion chosenCompanion = _companionsRepository.GetCompanions(user).Find(x => x.Id == selectedId);
                    if (chosenCompanion == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You need to choose an active Companion." });
                    }

                    if (chosenCompanion.Name == name)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your Companion already has this name." });
                    }

                    chosenCompanion.Name = name;
                    _companionsRepository.UpdateCompanion(chosenCompanion);

                    buttonText = "Visit Companions!";
                    buttonUrl = "/Profile#companions";

                    break;
                case SorceressEffectId.ReduceFurnace:
                    Item furnaceItem = _craftingRepository.GetFurnaceItem(user);
                    if (furnaceItem == null)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your don't currently have an item in the Furnace." });
                    }

                    if (settings.FurnaceDefrostDate < DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your Furnace item has already defrosted." });
                    }

                    settings.FurnaceDefrostDate = settings.FurnaceDefrostDate.AddDays(-1);
                    _settingsRepository.SetSetting(user, nameof(settings.FurnaceDefrostDate), settings.FurnaceDefrostDate.ToString());

                    buttonText = "Visit Furnace!";
                    buttonUrl = "/Crafting/Furnace";

                    break;
                case SorceressEffectId.LearnTranslation:
                    if (settings.HasLearntTranslation)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You've already learnt this ability." });
                    }

                    settings.HasLearntTranslation = true;
                    _settingsRepository.SetSetting(user, nameof(settings.HasLearntTranslation), settings.HasLearntTranslation.ToString());

                    buttonText = "Visit the Phantom Door!";
                    buttonUrl = "/PhantomDoor";

                    break;
            }

            _pointsRepository.SubtractPoints(user, effect.Points);

            settings.LastSorceressEffectDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastSorceressEffectDate), settings.LastSorceressEffectDate.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SorceressEffect))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SorceressEffect);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The Sorceress successfully cast the effect \"{effect.Name}\" for {Helper.GetFormattedPoints(effect.Points)}!", ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuySeedCard(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns.", ShouldRefresh = true });
            }

            Item seedCard = _itemsRepository.GetAllItems(ItemCategory.Seed).Find(x => x.Id == itemId);
            if (seedCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Seed Card no longer exists." });
            }

            if (user.Points < seedCard.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to buy this Seed Card." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_pointsRepository.SubtractPoints(user, seedCard.Points))
            {
                _itemsRepository.AddItem(user, seedCard);

                return Json(new Status { Success = true, SuccessMessage = $"The Seed Card \"{seedCard.Name}\" was successfully added to your bag!", Points = Helper.GetFormattedPoints(user.Points) });
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = false, ErrorMessage = "You were unable to buy this Seed Card.", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddSeedCard(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns.", ShouldRefresh = true });
            }

            Item seedCard = _cavernsRepository.GetPlantedItem(user);
            if (seedCard != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have a planted Seed Card." });
            }

            Seed seed = _itemsRepository.GetSeedCards(user).Find(x => x.SelectionId == selectionId);
            if (seed == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this Seed Card." });
            }

            if (_cavernsRepository.AddPlantedItem(user, seed))
            {
                _itemsRepository.RemoveItem(user, seed);

                settings.SeedCardTakeDate = DateTime.UtcNow.AddDays(2);
                _settingsRepository.SetSetting(user, nameof(settings.SeedCardTakeDate), settings.SeedCardTakeDate.ToString());

                settings.PlantedItemSeedColor = seed.Color;
                _settingsRepository.SetSetting(user, nameof(settings.PlantedItemSeedColor), settings.PlantedItemSeedColor.ToString());


                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = "You successfully planted the Seed Card!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Seed Card could not be planted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetPlantedItem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns.", ShouldRefresh = true });
            }

            if (settings.SeedCardTakeDate > DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Seed Card is not ready to be taken." });
            }

            Item plantedItem = _cavernsRepository.GetPlantedItem(user);
            if (plantedItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a planted Seed Card." });
            }

            if (plantedItem.Category == ItemCategory.Seed)
            {
                plantedItem = _cavernsRepository.GetItem(user, plantedItem, settings.PlantedItemSeedColor);
            }

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_cavernsRepository.DeletePlantedItem(plantedItem))
            {
                _itemsRepository.AddItem(user, plantedItem);

                settings.SeedCardTakeDate = DateTime.MinValue;
                _settingsRepository.SetSetting(user, nameof(settings.SeedCardTakeDate), settings.SeedCardTakeDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (plantedItem.Category == ItemCategory.Art)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TookMultiColorPlantedItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TookMultiColorPlantedItem);
                    }
                }
                else
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TookPlantedItem))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TookPlantedItem);
                    }
                    if (settings.PlantedItemSeedColor == CardColor.Purple)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TookRarePlantedItem))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TookRarePlantedItem);
                        }
                    }
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully added the item \"{plantedItem.Name}\" to your bag!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your planted item couldn't be taken." });
        }

        [HttpGet]
        public IActionResult GetCavernsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns." });
            }

            CavernsViewModel cavernsViewModel = GenerateModel(user, true);
            return PartialView("_CavernsMainPartial", cavernsViewModel);
        }

        [HttpGet]
        public IActionResult GetCavernsSeedCardsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DeepCavernsExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can no longer explore the Deep Caverns." });
            }

            CavernsViewModel cavernsViewModel = GenerateModel(user, false);
            return PartialView("_CavernsSeedCardsPartial", cavernsViewModel);
        }

        private CavernsViewModel GenerateModel(User user, bool isSorceress, SorceressEffect selectedEffect = null)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            CavernsViewModel cavernsViewModel = new CavernsViewModel
            {
                Title = "Deep Caverns",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Caverns,
                ExpiryDate = settings.DeepCavernsExpiryDate
            };

            if (isSorceress)
            {
                cavernsViewModel.Effects = _cavernsRepository.GetAllSorceressEffects(settings);
                cavernsViewModel.CanCastEffect = (DateTime.UtcNow - settings.LastSorceressEffectDate).TotalHours >= 2;
                cavernsViewModel.SelectedEffect = selectedEffect;
            }
            else
            {
                cavernsViewModel.SeedCards = _itemsRepository.GetAllItems(ItemCategory.Seed);
                cavernsViewModel.UserSeedCards = _itemsRepository.GetSeedCards(user);
                cavernsViewModel.PlantedItem = _cavernsRepository.GetPlantedItem(user);
                cavernsViewModel.SeedCardTakeDate = settings.SeedCardTakeDate;

                if (cavernsViewModel.PlantedItem != null && cavernsViewModel.PlantedItem.Category == ItemCategory.Seed && cavernsViewModel.SeedCardTakeDate < DateTime.UtcNow)
                {
                    cavernsViewModel.PlantedItem = _cavernsRepository.GetItem(user, cavernsViewModel.PlantedItem, settings.PlantedItemSeedColor);
                }
            }

            return cavernsViewModel;
        }
    }
}
