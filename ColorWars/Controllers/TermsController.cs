﻿using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class TermsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public TermsController(ILoginRepository loginRepository, IPointsRepository pointsRepository, ISettingsRepository settingsRepository, 
        IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();

            TermsViewModel termsViewModel = GenerateModel(user);
            termsViewModel.UpdateViewData(ViewData);
            return View(termsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClaimReward()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HasClaimedTermsReward)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have already claimed this reward." });
            }

            _pointsRepository.AddPoints(user, 500);

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ClaimedTermsReward))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ClaimedTermsReward);
            }

            settings.HasClaimedTermsReward = true;
            _settingsRepository.SetSetting(user, nameof(settings.HasClaimedTermsReward), settings.HasClaimedTermsReward.ToString());

            return Json(new Status { Success = true, SuccessMessage = $"You successfully claimed the reward and earned {Helper.GetFormattedPoints(500)}!", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpGet]
        public IActionResult GetTermsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TermsViewModel termsViewModel = GenerateModel(user);
            return PartialView("_TermsMainPartial", termsViewModel);
        }

        private TermsViewModel GenerateModel(User user)
        {
            TermsViewModel termsViewModel = new TermsViewModel
            {
                Title = "Terms of Service",
                User = user
            };

            if (user != null)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                termsViewModel.HasClaimedReward = settings.HasClaimedTermsReward;
            }

            return termsViewModel;
        }
    }
}
