﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class BuyersClubController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IBuyersClubRepository _buyersClubRepository;
        private IPointsRepository _pointsRepository;
        private IProfileRepository _profileRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISettingsRepository _settingsRepository;
        private ISoundsRepository _soundsRepository;

        public BuyersClubController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IBuyersClubRepository buyersClubRepository,
        IPointsRepository pointsRepository, IProfileRepository profileRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _buyersClubRepository = buyersClubRepository;
            _pointsRepository = pointsRepository;
            _profileRepository = profileRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag, bool showDiscount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "BuyersClub" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Showroom", "BuyersClub");
            }

            BuyersClubViewModel buyersClubViewModel = GenerateModel(user, true, showDiscount);
            buyersClubViewModel.UpdateViewData(ViewData);
            return View(buyersClubViewModel);
        }

        public IActionResult Showroom(bool showTitleChange)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "BuyersClub/Showroom" });
            }

            BuyersClubViewModel buyersClubViewModel = GenerateModel(user, false, false, showTitleChange);
            buyersClubViewModel.UpdateViewData(ViewData);
            return View(buyersClubViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Buy(Guid itemId, bool showDiscount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            List<Item> userItems = _itemsRepository.GetItems(user);
            if (userItems.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            int totalMinutesSinceLastBuyCount = (int)(DateTime.UtcNow - settings.LastBuyersClubBuyDate).TotalMinutes;
            if (totalMinutesSinceLastBuyCount < 60 && settings.BuyersClubRecentBuyCount >= 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought too many items from the Buyers Club recently. Try again in {60 - totalMinutesSinceLastBuyCount} minute{(60 - totalMinutesSinceLastBuyCount == 1 ? "" : "s")}." });
            }

            List<StoreItem> items = _buyersClubRepository.GetItems();
            StoreItem item = items.Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer available.", ShouldRefresh = true });
            }

            bool usePoints = false;
            if (showDiscount && settings.BuyersClubPoints >= Helper.GetBuyersClubPointsRequired())
            {
                item.Cost = (int)Math.Round(item.Cost * 0.85);
                usePoints = true;
            }

            if (user.Points < item.Cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            int buyersClubPointsEarned = (int)Math.Round(item.Cost * 0.1);
            if (_pointsRepository.SubtractPoints(user, item.Cost))
            {
                if (usePoints)
                {
                    settings.BuyersClubPoints -= Helper.GetBuyersClubPointsRequired();
                }
                settings.BuyersClubPoints += buyersClubPointsEarned;
                _settingsRepository.SetSetting(user, nameof(settings.BuyersClubPoints), settings.BuyersClubPoints.ToString());

                _itemsRepository.AddItem(user, item);

                if (totalMinutesSinceLastBuyCount >= 60)
                {
                    settings.LastBuyersClubBuyDate = DateTime.UtcNow;
                    settings.BuyersClubRecentBuyCount = 1;
                    _settingsRepository.SetSetting(user, nameof(settings.LastBuyersClubBuyDate), settings.LastBuyersClubBuyDate.ToString());
                }
                else
                {
                    settings.BuyersClubRecentBuyCount++;
                }
                _settingsRepository.SetSetting(user, nameof(settings.BuyersClubRecentBuyCount), settings.BuyersClubRecentBuyCount.ToString());
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BuyersClubBuy))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BuyersClubBuy);
            }
            if (usePoints && !accomplishments.Exists(x => x.Id == AccomplishmentId.BuyersClubRedeem))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BuyersClubRedeem);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your bag! You earned {Helper.FormatNumber(buyersClubPointsEarned)} Buyers Club Point{(buyersClubPointsEarned == 1 ? "" : "s")}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangeShowroomInfo(string title, string description, string color)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess))
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have access to a Showroom." });
            }

            if (string.IsNullOrEmpty(title))
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your Showroom title cannot be empty." });
            }
            if (title.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your Showroom title is too long." });
            }

            if (string.IsNullOrEmpty(description))
            {
                description = string.Empty;
            }
            else
            {
                if (description.Length > 350)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"Your Showroom description must be 350 characters or less." });
                }
                description = System.Text.RegularExpressions.Regex.Replace(description, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            }

            if (!string.IsNullOrEmpty(color))
            {
                try
                {
                    _ = ColorTranslator.FromHtml(color);
                }
                catch (Exception)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must enter a valid color." });
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);

            if (settings.ShowroomTitle != title)
            {
                settings.ShowroomTitle = title;
                _settingsRepository.SetSetting(user, nameof(settings.ShowroomTitle), settings.ShowroomTitle);
            }

            if (settings.ShowroomDescription != description)
            {
                settings.ShowroomDescription = description;
                _settingsRepository.SetSetting(user, nameof(settings.ShowroomDescription), settings.ShowroomDescription);
            }

            if (settings.ShowroomColor != color)
            {
                settings.ShowroomColor = color;
                _settingsRepository.SetSetting(user, nameof(settings.ShowroomColor), settings.ShowroomColor);
            }

            return Json(new Status { Success = true, ReturnUrl = "/Profile/Showroom" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyShowroomEnhancement(ShowroomEnhancementId showroomEnhancementId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess))
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have access to the Showroom Store." });
            }

            ShowroomEnhancement showroomEnhancement = _buyersClubRepository.GetShowroomEnhancements().Find(x => x.Id == showroomEnhancementId);
            if (showroomEnhancement == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Showroom Enhancement does not exist." });
            }

            if (user.Points < showroomEnhancement.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            string additionalMessage = string.Empty;
            switch (showroomEnhancement.Id)
            {
                case ShowroomEnhancementId.ShowroomAssessment:
                    if ((DateTime.UtcNow - settings.LastShowroomAssessmentDate).TotalDays < 2)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You've had your Showroom assessed too recently. Try again in {settings.LastShowroomAssessmentDate.AddDays(2).GetTimeUntil()}." });
                    }
                    List<Item> showroomItems = _profileRepository.GetShowroomItems(user);
                    List<Item> showroomArtItems = showroomItems.FindAll(x => x.Category == ItemCategory.Art).ToList();
                    List<Item> allArtItems = _itemsRepository.GetAllItems(ItemCategory.Art, null, true);
                    int totalUniqueShowroomArtItems = showroomArtItems.Select(x => x.Id).Distinct().Count();
                    int rareShowroomArtItems = showroomArtItems.FindAll(x => x.IsExclusive).Select(x => x.Id).Distinct().Count();
                    int score = (int)Math.Min(10, Math.Round(((float)showroomItems.Count / Helper.GetMaxShowroomSize() * 4) + ((float)totalUniqueShowroomArtItems / allArtItems.Count * 5) + ((float)rareShowroomArtItems / allArtItems.Count(x => x.IsExclusive) * 8)) + 2);
                    ShowroomCertification showroomCertification = score <= 5 ? ShowroomCertification.None : score <= 7 ? ShowroomCertification.Bronze : score <= 8 ? ShowroomCertification.Silver : score <= 9 ? ShowroomCertification.Gold : ShowroomCertification.Platinum;
                    string certification = score <= 5 ? "This doesn't qualify for any certification. Maybe next time!" : $"Your Showroom is certified: {showroomCertification}! Congratulations!";
                    additionalMessage = $"The Museum Curator says: \"Hmm... {showroomArtItems.Count} official Art Card{(showroomArtItems.Count == 1 ? "" : "s")}, {totalUniqueShowroomArtItems} unique one{(totalUniqueShowroomArtItems == 1 ? "" : "s")}, and {rareShowroomArtItems} rare one{(rareShowroomArtItems == 1 ? "" : "s")}! I'll give your Showroom a {score}/10! {certification}\"";
                    settings.ShowroomCertification = showroomCertification;
                    _settingsRepository.SetSetting(user, nameof(settings.ShowroomCertification), settings.ShowroomCertification.ToString());
                    settings.LastShowroomAssessmentDate = DateTime.UtcNow;
                    _settingsRepository.SetSetting(user, nameof(settings.LastShowroomAssessmentDate), settings.LastShowroomAssessmentDate.ToString());
                    if (showroomCertification == ShowroomCertification.Silver)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomSilver))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ShowroomSilver);
                        }
                    }
                    else if (showroomCertification == ShowroomCertification.Gold)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomGold))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ShowroomGold);
                        }
                    }
                    else if (showroomCertification == ShowroomCertification.Platinum)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ShowroomPlatinum))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ShowroomPlatinum);
                        }
                    }
                    break;
                case ShowroomEnhancementId.IncreaseSize:
                    if (settings.MaxShowroomSize >= Helper.GetMaxShowroomSize())
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"Your Showroom cannot have more than {Helper.GetMaxShowroomSize()} items." });
                    }
                    settings.MaxShowroomSize++;
                    _settingsRepository.SetSetting(user, nameof(settings.MaxShowroomSize), settings.MaxShowroomSize.ToString());
                    additionalMessage = $"Your maximum Showroom size increased to {settings.MaxShowroomSize} items!";
                    break;
                case ShowroomEnhancementId.RandomArtCard:
                    showroomArtItems = _profileRepository.GetShowroomItems(user);
                    if (showroomArtItems.Count >= settings.MaxShowroomSize)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "Your Showroom is full and cannot have another item added to it." });
                    }
                    Random rnd = new Random();
                    Occasion holiday = Helper.GetCurrentOccasion();
                    List<Item> artCards = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).FindAll(x => !x.IsExclusive || x.Name.Equals(holiday.GetDisplayName(), StringComparison.InvariantCultureIgnoreCase));
                    Item artCard = artCards[rnd.Next(artCards.Count)];
                    artCard.Username = user.Username;
                    _profileRepository.AddShowroomItem(user, artCard);
                    additionalMessage = $"The Art Card \"{artCard.Name}\" was added to your Showroom!";
                    break;
                case ShowroomEnhancementId.RechargeItems:
                    showroomArtItems = _profileRepository.GetShowroomItems(user).FindAll(x => x.Uses < x.TotalUses && (DateTime.UtcNow - x.AddedDate).TotalDays >= 3).ToList();
                    if (showroomArtItems.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have any items that have been in your Showroom for 3 or more days and that need to be recharged." });
                    }
                    foreach (Item showroomItem in showroomArtItems)
                    {
                        showroomItem.Uses = showroomItem.TotalUses;
                        _profileRepository.UpdateShowroomItem(showroomItem);
                    }
                    break;
                case ShowroomEnhancementId.RepairItems:
                    showroomArtItems = _profileRepository.GetShowroomItems(user).FindAll(x => x.Condition > ItemCondition.New && (DateTime.UtcNow - x.AddedDate).TotalDays >= 30).ToList();
                    if (showroomArtItems.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"You don't have any items that have been in your Showroom for 30 or more days and that need to be repaired." });
                    }
                    foreach (Item showroomItem in showroomArtItems)
                    {
                        showroomItem.RepairedDate = DateTime.UtcNow;
                        _profileRepository.UpdateShowroomItem(showroomItem);
                    }
                    break;
            }

            if (_pointsRepository.SubtractPoints(user, showroomEnhancement.Points))
            {
                return Json(new Status { Success = true, SuccessMessage = $"The Showroom Enhancement \"{showroomEnhancement.Name}\" was successfully applied for {Helper.GetFormattedPoints(showroomEnhancement.Points)}! {additionalMessage}", Points = Helper.GetFormattedPoints(user.Points) });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Showroom Enhancement couldn't be bought." });
        }

        [HttpGet]
        public IActionResult GetBuyersClubPartialView(bool showDiscount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            BuyersClubViewModel buyersClubViewModel = GenerateModel(user, true, showDiscount);
            return PartialView("_BuyersClubMainPartial", buyersClubViewModel);
        }

        [HttpGet]
        public IActionResult GetBuyersClubShowroomPartialView(bool showTitleChange)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            BuyersClubViewModel buyersClubViewModel = GenerateModel(user, false, false, true);
            if (!buyersClubViewModel.CanAccessShowroom)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have access to the Showroom Store." });
            }

            if (showTitleChange)
            {
                return PartialView("_BuyersClubShowroomTitleChangePartial", buyersClubViewModel);
            }
            return PartialView("_BuyersClubShowroomPartial", buyersClubViewModel);
        }

        private BuyersClubViewModel GenerateModel(User user, bool isBuyersClub, bool showDiscount, bool showShowroomTitleChange = false)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            BuyersClubViewModel buyersClubViewModel = new BuyersClubViewModel
            {
                Title = "Buyers Club",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.BuyersClub,
                BuyersClubPoints = settings.BuyersClubPoints,
                ShowshowShowroomTitleChange = showShowroomTitleChange
            };

            if (isBuyersClub)
            {
                List<StoreItem> buyersClubItems = _buyersClubRepository.GenerateBuyersClubItems();
                if (settings.BuyersClubPoints >= Helper.GetBuyersClubPointsRequired() && showDiscount)
                {
                    foreach (StoreItem buyersClubItem in buyersClubItems)
                    {
                        buyersClubItem.Cost = (int)Math.Round(buyersClubItem.Cost * 0.85);
                    }
                }

                buyersClubViewModel.StoreItems = buyersClubItems;
                buyersClubViewModel.CanBuy = settings.BuyersClubRecentBuyCount < 3 || (DateTime.UtcNow - settings.LastBuyersClubBuyDate).TotalHours >= 1;
                buyersClubViewModel.ShowDiscount = showDiscount;
                buyersClubViewModel.RemainingItems = 3 - settings.BuyersClubRecentBuyCount;
                buyersClubViewModel.LastBuyDate = settings.LastBuyersClubBuyDate;
            }
            else
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                buyersClubViewModel.CanChallengeShowroomOwner = (accomplishments.Count >= 20 && user.GetAgeInMinutes() >= 30 && settings.MatchLevel >= 3) || settings.MatchLevel == 9;
                buyersClubViewModel.CanAccessShowroom = accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess);
                buyersClubViewModel.ShowroomEnhancements = _buyersClubRepository.GetShowroomEnhancements();
                buyersClubViewModel.ShowroomTitle = settings.ShowroomTitle;
                buyersClubViewModel.ShowroomDescription = settings.ShowroomDescription;
                buyersClubViewModel.ShowroomColor = string.IsNullOrEmpty(settings.ShowroomColor) ? string.IsNullOrEmpty(user.Color) ? "#337ab7" : user.Color : settings.ShowroomColor;
            }

            return buyersClubViewModel;
        }
    }
}
