﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;

namespace ColorWars.Controllers
{
    public class DeleteAccountController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;

        public DeleteAccountController(ILoginRepository loginRepository, IUserRepository userRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "DeleteAccount" });
            }

            DeleteAccountViewModel deleteAccountViewModel = GenerateModel(user);
            deleteAccountViewModel.UpdateViewData(ViewData);
            return View(deleteAccountViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(InactiveAccountReason reason, string info, string password)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (user.InactiveType == InactiveAccountType.Deleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "This account is already deleted." });
            }

            if (reason <= InactiveAccountReason.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a reason for deleting your account." });
            }

            if (info?.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your additional info must be 500 characters or less." });
            }

            if (string.IsNullOrEmpty(password))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide your password to delete your account." });
            }

            if (!_userRepository.IsPasswordValid(user, password))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your password isn't right." });
            }

            _userRepository.DeleteUser(user, reason, info);

            return Json(new Status { Success = true }); 
        }

        private DeleteAccountViewModel GenerateModel(User user)
        {
            DeleteAccountViewModel deleteAccountViewModel = new DeleteAccountViewModel
            {
                Title = "Delete Your Account",
                User = user
            };

            return deleteAccountViewModel;
        }
    }
}
