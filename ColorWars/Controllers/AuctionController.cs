﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class AuctionController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private IAuctionsRepository _auctionsRepository;
        private IUserRepository _userRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationsRepository;
        private ISoundsRepository _soundsRepository;

        public AuctionController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        IAuctionsRepository auctionsRepository, IUserRepository userRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, IEmailRepository emailRepository, INotificationsRepository notificationsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _auctionsRepository = auctionsRepository;
            _userRepository = userRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _notificationsRepository = notificationsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(int id, string query, int page, bool matchExactSearch, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"Auction{(id == 0 ? "" : $"?id={id}")}" });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            if (id == 0)
            {
                int resultsPerPage = 15;
                List<Auction> auctions = _auctionsRepository.GetAllAuctions(query, matchExactSearch, category, condition);
                auctionViewModel.TotalPages = (int)Math.Ceiling((double)auctions.Count / resultsPerPage);
                if (page < 0 || page >= auctionViewModel.TotalPages)
                {
                    page = 0;
                }
                auctionViewModel.Auctions = auctions.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                auctionViewModel.CurrentPage = page;
                auctionViewModel.SearchQuery = query;
                auctionViewModel.MatchExactSearch = matchExactSearch;
                auctionViewModel.SearchCategory = category;
                auctionViewModel.SearchCondition = condition;
            }
            else
            {
                Auction auction = _auctionsRepository.GetAuction(id);
                if (auction == null)
                {
                    return RedirectToAction("Index", "Auction");
                }
                auctionViewModel.CurrentAuction = auction;
            }
            auctionViewModel.UpdateViewData(ViewData);
            return View(auctionViewModel);
        }

        public IActionResult Create()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Auction/Create" });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.Items = _itemsRepository.GetItems(user, true);
            auctionViewModel.UpdateViewData(ViewData);
            return View(auctionViewModel);
        }

        public IActionResult Bids(int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Auction/Bids" });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            int resultsPerPage = 15;
            List<Auction> auctions = _auctionsRepository.GetAuctions(user, false, false, false);
            auctionViewModel.TotalPages = (int)Math.Ceiling((double)auctions.Count / resultsPerPage);
            if (page < 0 || page >= auctionViewModel.TotalPages)
            {
                page = 0;
            }
            auctionViewModel.Auctions = auctions.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
            auctionViewModel.CurrentPage = page;
            auctionViewModel.UpdateViewData(ViewData);
            return View(auctionViewModel);
        }

        public IActionResult Past(int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Auction/Past" });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            int resultsPerPage = 15;
            List<Auction> auctions = _auctionsRepository.GetPastAuctions();
            auctionViewModel.TotalPages = (int)Math.Ceiling((double)auctions.Count / resultsPerPage);
            if (page < 0 || page >= auctionViewModel.TotalPages)
            {
                page = 0;
            }
            auctionViewModel.Auctions = auctions.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
            auctionViewModel.CurrentPage = page;
            auctionViewModel.UpdateViewData(ViewData);
            return View(auctionViewModel);
        }

        public IActionResult Collection()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Auction/Collection" });
            }

            List<Item> collectItems = new List<Item>();
            int collectPoints = 0;
            List<Auction> updatedAuctions = new List<Auction>();
            GetCollectionResults(user, ref collectItems, ref collectPoints, ref updatedAuctions);

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.CollectPoints = collectPoints;
            auctionViewModel.CollectItems = collectItems;
            auctionViewModel.UpdateViewData(ViewData);
            return View(auctionViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AuctionItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item item = items?.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item.", ShouldRefresh = true });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.SelectedItem = item;

            return PartialView("_AuctionCreateItemPartial", auctionViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateAuction(Guid selectionId, int points, int minBidIncrement, int length)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item item = items?.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item.", ShouldRefresh = true });
            }

            if (points <= 0 || points > 9500000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The starting price must be between 1 and {Helper.GetFormattedPoints(9500000)}." });
            }
            if (minBidIncrement <= 0 || minBidIncrement > 1000000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The minimum bid increment must be between 1 and {Helper.GetFormattedPoints(1000000)}." });
            }
            if (length < 1 || length > 24 * 7)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The auction end date isn't a valid length of time." });
            }

            List<Auction> activeAuctions = _auctionsRepository.GetAuctions(user, true, false, false).FindAll(x => x.Username == user.Username);
            if (activeAuctions.Count >= 10)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot create more than 10 active auctions at the same time." });
            }

            DateTime endDate = DateTime.UtcNow.AddHours(length);
            if (_auctionsRepository.AddAuction(user, points, minBidIncrement, item, endDate, out Auction auction))
            {
                _itemsRepository.RemoveItem(user, item);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.AuctionParticipant))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.AuctionParticipant);
                }

                return Json(new Status { Success = true, Id = auction.Id.ToString() });
            }
            return Json(new Status { Success = false, ErrorMessage = $"Your auction couldn't be creatd." });
        }

        [HttpPost]
        public IActionResult EndAuction(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Auction auction = _auctionsRepository.GetAuction(id);
            if (auction.Username != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot end this auction as you didn't create it." });
            }

            if (auction.Bids.Count > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot end this auction as there's already a bid placed on it." });
            }

            if (auction.IsFinished())
            {
                return Json(new Status { Success = false, ErrorMessage = "This auction has already finished." });
            }

            auction.EndDate = DateTime.UtcNow;
            if (_auctionsRepository.UpdateAuction(auction))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The auction couldn't be ended." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubmitBid(int auctionId, int bidAmount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Auction auction = _auctionsRepository.GetAuction(auctionId);
            if (auction == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This auction doesn't seem to exist.", ShouldRefresh = true });
            }
            if (auction.Username == user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't bid on your own auction." });
            }
            if (auction.IsFinished())
            {
                return Json(new Status { Success = false, ErrorMessage = "This auction has already finished." });
            }
            int minBidAmount = user.Username == auction.HighestBidder ? auction.MinBidIncrement : auction.HighestBid + (auction.Bids.Count == 0 ? 0 : auction.MinBidIncrement);
            if (bidAmount < minBidAmount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must bid at least {Helper.GetFormattedPoints(minBidAmount)}." });
            }
            if (user.Points < bidAmount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (_pointsRepository.SubtractPoints(user, bidAmount))
            {
                if (_auctionsRepository.AddBid(user, auction, bidAmount))
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.AuctionParticipant))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.AuctionParticipant);
                    }

                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        if (user.Username != auction.HighestBidder)
                        {
                            Bid previousBid = auction.Bids?.FirstOrDefault();
                            if (previousBid != null)
                            {
                                User previousBidder = _userRepository.GetUser(previousBid.Username);
                                if (previousBidder != null)
                                {
                                    _pointsRepository.AddPoints(previousBidder, previousBid.Points);
                                    SendOutbidNotification(user, previousBidder, auction, domain);
                                }
                            }
                        }
                        if (auction.Username != user.Username && settings.ReceiveAuctionBidNotifications)
                        {
                            User auctionUser = _userRepository.GetUser(auction.Username);
                            if (auctionUser != null)
                            {
                                if (auction.HighestBidder == user.Username)
                                {
                                    bidAmount += auction.HighestBid;
                                }
                                _notificationsRepository.AddNotification(auctionUser, $"@{user.Username} placed a bid of {Helper.GetFormattedPoints(bidAmount)} on your auction!", NotificationLocation.Auction, $"/Auction?id={auction.Id}#{user.Username}", domain: domain);
                            }
                        }
                    });
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"Your bid of {Helper.GetFormattedPoints(bidAmount)} was successfully made!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CollectEarnings()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> collectItems = new List<Item>();
            int collectPoints = 0;
            List<Auction> updatedAuctions = new List<Auction>();
            GetCollectionResults(user, ref collectItems, ref collectPoints, ref updatedAuctions);

            string buttonText = "";
            string buttonUrl = "";
            if (collectItems.Count > 0)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                List<Item> items = _itemsRepository.GetItems(user);
                if (items.Count + collectItems.Count > settings.MaxBagSize)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You have too many items in your bag to collect any more items." });
                }
                buttonText = "Head to your bag!";
                buttonUrl = "/Items";
            }

            if (collectItems.Count == 0 && collectPoints == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have nothing to collect." });
            }

            foreach (Auction auction in updatedAuctions.Distinct())
            {
                _auctionsRepository.UpdateAuction(auction);
            }
            foreach (Item item in collectItems)
            {
                _itemsRepository.AddItem(user, item);
            }
            if (collectPoints > 0)
            {
                _pointsRepository.AddPoints(user, collectPoints);
            }

            if (updatedAuctions.Exists(x => x.WinnerCollected && x.Username != user.Username))
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FirstAuctionWin))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FirstAuctionWin);
                }
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully collected your earnings!", ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpGet]
        public IActionResult GetAuctionCreatePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.Items = _itemsRepository.GetItems(user, true);
            return PartialView("_AuctionCreateSelectPartial", auctionViewModel);
        }

        [HttpGet]
        public IActionResult GetAuctionBidPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.CurrentAuction = _auctionsRepository.GetAuction(id);
            return PartialView("_AuctionIdPartial", auctionViewModel);
        }

        [HttpGet]
        public IActionResult GetAuctionCollectionPartialView(Guid id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            List<Item> collectItems = new List<Item>();
            int collectPoints = 0;
            List<Auction> updatedAuctions = new List<Auction>();
            GetCollectionResults(user, ref collectItems, ref collectPoints, ref updatedAuctions);

            AuctionViewModel auctionViewModel = GenerateModel(user);
            auctionViewModel.CollectPoints = collectPoints;
            auctionViewModel.CollectItems = collectItems;
            auctionViewModel.UpdateViewData(ViewData);
            return PartialView("_AuctionCollectionPartial", auctionViewModel);
        }

        private AuctionViewModel GenerateModel(User user)
        {
            AuctionViewModel auctionViewModel = new AuctionViewModel
            {
                Title = "Auction House",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.AuctionHouse,
                UncollectedEarnings = CheckIfUncollectedEarnings(user)
            };

            return auctionViewModel;
        }

        private void GetCollectionResults(User user, ref List<Item> collectItems, ref int collectPoints, ref List<Auction> updatedAuctions)
        {
            List<Auction> auctions = _auctionsRepository.GetAuctions(user, false, true, false).FindAll(x => x.IsFinished());
            foreach (Auction auction in auctions)
            {
                if (auction.Username == user.Username)
                {
                    if (!auction.CreatorCollected)
                    {
                        if (auction.BidCount > 0)
                        {
                            collectPoints += (int)Math.Round(auction.HighestBid * 0.9);
                            auction.CreatorCollected = true;
                            updatedAuctions.Add(auction);
                        }
                        else
                        {
                            Item item = _auctionsRepository.GetItemWithAuctionId(auction.Id);
                            if (item != null)
                            {
                                collectItems.Add(item);
                                auction.CreatorCollected = true;
                                updatedAuctions.Add(auction);
                            }
                        }
                    }
                }
                else if (auction.BidCount > 0 && auction.TopBidder == user.Username)
                {
                    if (!auction.WinnerCollected)
                    {
                        Item item = _auctionsRepository.GetItemWithAuctionId(auction.Id);
                        if (item != null)
                        {
                            collectItems.Add(item);
                            auction.WinnerCollected = true;
                            updatedAuctions.Add(auction);
                        }
                    }
                }
            }
        }

        private bool CheckIfUncollectedEarnings(User user)
        {
            List<Auction> auctions = _auctionsRepository.GetAuctions(user, false, true, false).FindAll(x => x.IsFinished());
            foreach (Auction auction in auctions)
            {
                if (!auction.CreatorCollected && auction.Username == user.Username)
                {
                    return true;
                }
                if (!auction.WinnerCollected && auction.BidCount > 0 && auction.TopBidder == user.Username)
                {
                    return true;
                }
            }
            return false;
        }

        private void SendOutbidNotification(User user, User bidder, Auction auction, string domain)
        {
            if (user.Username == bidder.Username)
            {
                return;
            }

            string subject = "You were outbid in an auction!";
            string title = "Outbid in Auction";
            string body = $"You were outbid in an auction! But don't fret, you can place a new bid by clicking the button below. You only have {auction.EndDate.GetTimeUntil()} left!";
            string url = $"/Auction?id={auction.Id}#{user.Username}";

            _emailRepository.SendEmail(bidder, subject, title, body, url, domain: domain);

            _notificationsRepository.AddNotification(bidder, $"@{user.Username} outbid you in an auction!", NotificationLocation.Auction, url, domain: domain);
        }
    }
}
