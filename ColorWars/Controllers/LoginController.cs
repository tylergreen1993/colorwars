﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ColorWars.Classes;

namespace ColorWars.Controllers
{
    public class LoginController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;
        private IEmailRepository _emailRepository;
        private IPasswordResetRepository _passwordResetRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public LoginController(ILoginRepository loginRepository, IUserRepository userRepository, IEmailRepository emailRepository,
        IPasswordResetRepository passwordResetRepository, IAdminHistoryRepository adminHistoryRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
            _passwordResetRepository = passwordResetRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public IActionResult Index(string ru)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                return RedirectToAction("Index", "Home");
            }

            LoginViewModel loginViewModel = new LoginViewModel
            {
                Title = "Log in",
                ReturnUrl = ru
            };

            loginViewModel.UpdateViewData(ViewData);
            return View(loginViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(string username, string password, string returnUrl)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                return Json(new Status { Success = true, ReturnUrl = string.Empty });
            }

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to enter both your username and your password." });
            }

            User verifyUser = username.Contains("@") ? _userRepository.GetUserWithEmail(username, false) : _userRepository.GetUser(username, false);
            if (verifyUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This {(username.Contains("@") ? "email" : "username")} hasn't been signed up yet." });
            }
            if (verifyUser.InactiveType == InactiveAccountType.Deleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "This account has been permanently deleted." });
            }

            int timeSinceLastLoginAttempt = (int)(DateTime.UtcNow - verifyUser.LoginAttemptDate).TotalMinutes;
            if (verifyUser.LoginAttempts >= 3 && timeSinceLastLoginAttempt <= 30)
            {
                return Json(new Status { Success = false, ErrorMessage = $"Your account has been locked for too many failed login attempts. Reset your password or try again in {30 - timeSinceLastLoginAttempt} minute{(30 - timeSinceLastLoginAttempt == 1 ? "" : "s")}." });
            }

            List<ActiveSession> activeSessions = _userRepository.GetActiveSessions(verifyUser);
            if (activeSessions.Count >= 20)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are logged in to your account with too many devices. Log out of one of your active sessions." });
            }

            user = _loginRepository.LoginUser(verifyUser.Username, password);
            if (user == null)
            {
                verifyUser.LoginAttempts = verifyUser.LoginAttempts >= 3 ? 1 : verifyUser.LoginAttempts + 1;
                verifyUser.LoginAttemptDate = DateTime.UtcNow;
                _userRepository.UpdateUser(verifyUser);

                if (verifyUser.LoginAttempts == 3)
                {
                    string domain = Helper.GetDomain();
                    string ipAddress = GlobalHttpContext.Current.Connection.RemoteIpAddress.ToString();
                    Task.Run(() =>
                    {
                        string subject = "Multiple Login Requests";
                        string title = "Bad Logins";
                        string body = $"Someone with the IP Address: <b>{ipAddress}</b> has tried to access your account multiple times with an invalid password. If this is you, kindly disregard this message. Otherwise, please let us know by clicking the button below and contacting us.";

                        _emailRepository.SendEmail(verifyUser, subject, title, body, "/Contact", true, domain);
                        User adminUser = Helper.GetAdminUser();
                        _adminHistoryRepository.AddAdminHistory(verifyUser, adminUser, $"Multiple failed attempts were made to log in with IP Address: {ipAddress}.");
                    });
                }

                return Json(new Status { Success = false, ErrorMessage = "You wrote an incorrect username and password combination. Please make sure you wrote the right password!" });
            }

            return Json(new Status { Success = true, ReturnUrl = returnUrl ?? string.Empty });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ForgotPassword(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to enter a username to reset your password." });
            }

            User user = username.Contains("@") ? _userRepository.GetUserWithEmail(username, false) : _userRepository.GetUser(username, false);
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This {(username.Contains("@") ? "email" : "username")} hasn't been signed up yet." });
            }
            if (user.InactiveType == InactiveAccountType.Deleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "This account has been deleted." });
            }

            int timeSinceLastRequest = (int)(DateTime.UtcNow - user.ForgotEmailDate).TotalMinutes;
            if (timeSinceLastRequest < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You recently requested info for this user. Try again in {5 - timeSinceLastRequest} minute{((5 - timeSinceLastRequest) == 1 ? "" : "s")}." });
            }

            string subject = $"Reset your password";
            string title = "Forgot your password?";
            string body = $"The password for your {Resources.SiteName} account was requested! In case you forgot it, you can set up a new password by clicking the button below.";

            PasswordReset passwordReset = _passwordResetRepository.CreateOrUpdatePasswordReset(user);
            if (passwordReset == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "We couldn't create your password reset email. Please try again." });
            }

            user.ForgotEmailDate = DateTime.UtcNow;
            _userRepository.UpdateUser(user);

            _emailRepository.SendEmail(user, subject, title, body, $"/PasswordReset?username={user.Username}&token={passwordReset.Id}", true);

            return Json(new Status { Success = true, SuccessMessage = "A password reset email has been successfully sent to you!" });
        }
    }
}
