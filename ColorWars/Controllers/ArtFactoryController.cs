﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using ImageMagick;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ArtFactoryController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IArtFactoryRepository _artFactoryRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IGoogleStorageRepository _googleStorageRepository;
        private IWebHostEnvironment _webHostEnvironment;
        private ISoundsRepository _soundsRepository;

        public ArtFactoryController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IArtFactoryRepository artFactoryRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IGoogleStorageRepository googleStorageRepository, IWebHostEnvironment webHostEnvironment, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _artFactoryRepository = artFactoryRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _googleStorageRepository = googleStorageRepository;
            _webHostEnvironment = webHostEnvironment;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"ArtFactory" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFifteen))
            {
                Messages.AddSnack("You haven't unlocked the Art Factory yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            ArtFactoryViewModel artFactoryViewModel = GenerateModel(user, true);
            artFactoryViewModel.UpdateViewData(ViewData);
            return View(artFactoryViewModel);
        }

        public IActionResult RecentCards()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"ArtFactory/RecentCards" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CPULevelFifteen))
            {
                Messages.AddSnack("You haven't unlocked the Art Factory yet.", true);
                return RedirectToAction("Index", "Plaza");
            }

            ArtFactoryViewModel artFactoryViewModel = GenerateModel(user, false);
            artFactoryViewModel.UpdateViewData(ViewData);
            return View(artFactoryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upload(IFormFile file, string artCardName)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 15)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked the Art Factory yet." });
            }

            if (!string.IsNullOrEmpty(settings.ArtFactoryPreviewImage))
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have a pending Custom Card preview." });
            }

            if (string.IsNullOrEmpty(artCardName))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must give your Custom Card a name." });
            }

            if (artCardName.Length > 15)
            {
                return Json(new Status { Success = false, ErrorMessage = "You Custom Card's name must be 15 characters or less." });
            }

            if (!Regex.IsMatch(artCardName, "^[a-zA-Z0-9_. ]*$", RegexOptions.IgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Custom Card's name isn't valid." });
            }

            if (file == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provided an image." });
            }

            int maxSizeInKb = 200;

            if (file.Length > maxSizeInKb * 2000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)file.Length / 1000, 1)} KB. Your uploaded image cannot be greater than {maxSizeInKb} KB." });
            }

            if (file.ContentType != "image/png" && file.ContentType != "image/jpg" && file.ContentType != "image/jpeg" && file.ContentType != "image/gif")
            {
                return Json(new Status { Success = false, ErrorMessage = "Your image's file type isn't supported." });
            }

            using (Stream fileStream = file.OpenReadStream())
            using (MemoryStream memoryStream = new MemoryStream())
            {
                fileStream.CopyTo(memoryStream);
                memoryStream.Position = 0;

                Image backImage = Image.FromStream(memoryStream);
                System.Drawing.Imaging.ImageFormat imageFormat = System.Drawing.Imaging.ImageFormat.Png;

                Image topImage = Image.FromFile(_webHostEnvironment.WebRootFileProvider.GetFileInfo("images/ArtFactory/Template.png")?.PhysicalPath);
                Bitmap tempBitmap = new Bitmap(topImage.Width, topImage.Height);
                using (MemoryStream memoryStreamNewImage = new MemoryStream())
                {
                    using (Graphics g = Graphics.FromImage(tempBitmap))
                    {
                        g.DrawImage(backImage, 12, 12, topImage.Width - 24, topImage.Height - 24);
                        g.DrawImage(topImage, 0, 0, topImage.Width, topImage.Height);
                        tempBitmap.Save(memoryStreamNewImage, imageFormat);
                        memoryStreamNewImage.Position = 0;
                    }

                    if (memoryStream.Length > maxSizeInKb * 1000)
                    {
                        ImageOptimizer optimizer = new ImageOptimizer
                        {
                            OptimalCompression = true,
                            IgnoreUnsupportedFormats = true
                        };
                        optimizer.Compress(memoryStreamNewImage);
                    }

                    if (memoryStreamNewImage.Length > maxSizeInKb * 1000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)memoryStreamNewImage.Length / 1000, 1)} KB after being compressed. Your uploaded image cannot be greater than {maxSizeInKb} KB." });
                    }

                    string extension = "png";
                    string fileName = $"{user.Username}-{artCardName}-{Guid.NewGuid()}.{extension}";

                    string url = _googleStorageRepository.UploadFileAsync(memoryStreamNewImage, fileName, file.ContentType).Result;
                    if (string.IsNullOrEmpty(url))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "Your image could not be uploaded. Please try again." });
                    }

                    settings.ArtFactoryPreviewImage = url;
                    _settingsRepository.SetSetting(user, nameof(settings.ArtFactoryPreviewImage), settings.ArtFactoryPreviewImage);

                    return Json(new Status { Success = true });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 15)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked the Art Factory yet." });
            }

            if (string.IsNullOrEmpty(settings.ArtFactoryPreviewImage))
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a pending Custom Card preview." });
            }

            if (user.Points < 5000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item.", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            Item custom = _itemsRepository.GetAllItems(ItemCategory.Custom, null, true).First();
            custom.Name = settings.ArtFactoryPreviewImage.Split("-")[2];
            custom.ImageUrl = settings.ArtFactoryPreviewImage;

            if (_itemsRepository.AddItem(user, custom, true))
            {
                _pointsRepository.SubtractPoints(user, 5000);

                _artFactoryRepository.AddUserCustomCard(user, custom.Name, custom.ImageUrl);

                settings.ArtFactoryPreviewImage = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.ArtFactoryPreviewImage), settings.ArtFactoryPreviewImage);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Scissors, settings);

                return Json(new Status { Success = true, SuccessMessage = $"Your Custom Card \"{custom.Name}\" was successfully created and added to your bag!", Points = user.GetFormattedPoints(), ButtonText = "Head to your bag!", ButtonUrl = "/Items", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"Your Custom Card couldn't be created." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Discard()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.MatchLevel < 15)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked the Art Factory yet." });
            }

            if (string.IsNullOrEmpty(settings.ArtFactoryPreviewImage))
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a pending Custom Card preview." });
            }

            string fileName = settings.ArtFactoryPreviewImage.Split("/").Last();
            _googleStorageRepository.DeleteFileAsync(fileName);

            settings.ArtFactoryPreviewImage = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.ArtFactoryPreviewImage), settings.ArtFactoryPreviewImage);

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetArtFactoryPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ArtFactoryViewModel artFactoryViewModel = GenerateModel(user, true);
            return PartialView("_ArtFactoryMainPartial", artFactoryViewModel);
        }

        private ArtFactoryViewModel GenerateModel(User user, bool isCreate)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            ArtFactoryViewModel artFactoryViewModel = new ArtFactoryViewModel
            {
                Title = "Art Factory",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.ArtFactory
            };

            if (isCreate)
            {
                artFactoryViewModel.CustomCard = GetCustomCard(settings.ArtFactoryPreviewImage);
            }
            else
            {
                artFactoryViewModel.RecentCards = _artFactoryRepository.GetRecentUserCustomCards();
            }

            return artFactoryViewModel;
        }

        private Item GetCustomCard(string imageUrl)
        {
            if (string.IsNullOrEmpty(imageUrl))
            {
                return null;
            }

            Item custom = _itemsRepository.GetAllItems(ItemCategory.Custom, null, true).First();
            custom.Name = imageUrl.Split("-")[2];
            custom.ImageUrl = imageUrl;

            return custom;
        }
    }
}
