﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using ImageMagick;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class GroupsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IGroupsRepository _groupsRepository;
        private IPointsRepository _pointsRepository;
        private IUserRepository _userRepository;
        private IBlockedRepository _blockedRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationsRepository;
        private ISettingsRepository _settingsRepository;
        private IAdminHistoryRepository _adminHistoryRepository;
        private IGoogleStorageRepository _googleStorageRepository;
        private ISoundsRepository _soundsRepository;

        public GroupsController(ILoginRepository loginRepository, IGroupsRepository groupsRepository, IPointsRepository pointsRepository,
        IUserRepository userRepository, IBlockedRepository blockedRepository, IAccomplishmentsRepository accomplishmentsRepository, IEmailRepository emailRepository,
        INotificationsRepository notificationsRepository, ISettingsRepository settingsRepository, IAdminHistoryRepository adminHistoryRepository,
        IGoogleStorageRepository googleStorageRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _groupsRepository = groupsRepository;
            _pointsRepository = pointsRepository;
            _userRepository = userRepository;
            _blockedRepository = blockedRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _notificationsRepository = notificationsRepository;
            _settingsRepository = settingsRepository;
            _adminHistoryRepository = adminHistoryRepository;
            _googleStorageRepository = googleStorageRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Groups" });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            Group currentGroup;
            if (id == 0)
            {
                currentGroup = groupsViewModel.UserGroups.Find(x => x.IsPrimary);
                if (currentGroup != null)
                {
                    Settings settings = _settingsRepository.GetSettings(user);
                    if (settings.ShowGroupMessagesFirst)
                    {
                        return RedirectToAction("Messages", "Groups", new { id = currentGroup.Id });
                    }
                }
            }
            else
            {
                currentGroup = _groupsRepository.GetGroup(user, id);
            }
            groupsViewModel.CurrentGroup = currentGroup;

            if (currentGroup == null)
            {
                groupsViewModel.AllGroups = _groupsRepository.GetTopGroups();
            }
            else
            {
                groupsViewModel.UniqueLink = $"{Helper.GetDomain(HttpContext)}/Groups?id={currentGroup.Id}";
                if (currentGroup.IsUserMember)
                {
                    groupsViewModel.UnreadGroupMessages = _groupsRepository.GetUnreadGroupMessages(user, groupsViewModel.CurrentGroup)?.Count ?? 0;
                    GroupMember groupMember = currentGroup.Members.Find(x => x.Username == user.Username);
                    groupsViewModel.UserMember = groupMember;
                    if (groupMember.Role > GroupRole.Standard && currentGroup.IsPrivate)
                    {
                        groupsViewModel.Requests = _groupsRepository.GetGroupRequests(groupsViewModel.CurrentGroup).FindAll(x => !x.Accepted);
                    }
                }
                else
                {
                    if (currentGroup.IsPrivate)
                    {
                        GroupRequest groupRequest = _groupsRepository.GetGroupRequests(currentGroup).Find(x => x.Username == user.Username);
                        if (groupRequest != null)
                        {
                            groupsViewModel.RequestStatus = groupRequest.Accepted ? GroupRequestStatus.Accepted : GroupRequestStatus.Pending;
                        }
                    }
                }
                UpdateGroupPrize(groupsViewModel);
            }

            groupsViewModel.UpdateViewData(ViewData);
            return View(groupsViewModel);
        }

        public IActionResult Create(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Groups/Create" });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            groupsViewModel.CurrentGroup = _groupsRepository.GetGroup(user, id);
            if (id > 0 && groupsViewModel.CurrentGroup == null)
            {
                return RedirectToAction("Index", "Groups");
            }

            if (groupsViewModel.CurrentGroup != null && user.Role <= UserRole.Helper)
            {
                groupsViewModel.UserMember = groupsViewModel.CurrentGroup.Members.Find(x => x.Username == user.Username);
                if (groupsViewModel.UserMember == null || groupsViewModel.UserMember.Role != GroupRole.Creator)
                {
                    return RedirectToAction("Index", "Groups");
                }
            }

            groupsViewModel.UpdateViewData(ViewData);
            return View(groupsViewModel);
        }

        public IActionResult All(string query, int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Groups/All" });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            int resultsPerPage = 15;
            List<Group> groups = string.IsNullOrEmpty(query) ? _groupsRepository.GetTopGroups(false) : _groupsRepository.GetGroupsWithSearchQuery(query);
            groupsViewModel.TotalPages = (int)Math.Ceiling((double)groups.Count / resultsPerPage);
            if (page < 0 || page >= groupsViewModel.TotalPages)
            {
                page = 0;
            }
            groupsViewModel.AllGroups = groups.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
            groupsViewModel.CurrentPage = page;
            groupsViewModel.SearchQuery = query;
            groupsViewModel.UpdateViewData(ViewData);
            return View(groupsViewModel);
        }

        public IActionResult Messages(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Groups/Messages" });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            Group group = _groupsRepository.GetGroup(user, id);
            if (group == null || (user.Role < UserRole.Helper && !group.IsUserMember))
            {
                return RedirectToAction("Index", "Groups");
            }

            groupsViewModel.CurrentGroup = group;
            groupsViewModel.Messages = _groupsRepository.GetGroupMessages(group);

            Settings settings = _settingsRepository.GetSettings(user);
            if (groupsViewModel.Messages.Count > 0 && group.IsUserMember)
            {
                if (group.LastReadMessageDate < groupsViewModel.Messages.Last().CreatedDate)
                {
                    GroupMember userMember = group.Members.Find(x => x.Username == user.Username);
                    userMember.LastReadMessageDate = groupsViewModel.Messages.Last().CreatedDate.AddSeconds(1);
                    _groupsRepository.UpdateGroupMember(userMember);
                }
            }

            groupsViewModel.UpdateViewData(ViewData);
            return View(groupsViewModel);
        }

        public IActionResult AllUser()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Groups/AllUser" });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            groupsViewModel.UpdateViewData(ViewData);
            return View(groupsViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(string name, IFormFile file, GroupPower power, string description, string isPrivateGroup)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must fill in all the fields to create a new group." });
            }

            if (name.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group name is too long." });
            }

            if (description.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group description is too long." });
            }

            if (power <= GroupPower.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to choose a valid Group Power." });
            }

            if (user.Points < 500)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to create a group." });
            }

            List<Group> userGroups = _groupsRepository.GetGroups(user);
            if (userGroups.Count(x => x.Members.Find(y => y.Username == user.Username).Role == GroupRole.Creator) >= 10)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already created 10 groups. Please remove one before creating a new one." });
            }

            string displayPicUrl = string.Empty;
            if (file != null)
            {
                if (file.Length > 1000000)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)file.Length / 1000, 1)} KB. Your group's display picture cannot be greater than 250 KB." });
                }

                if (file.ContentType != "image/png" && file.ContentType != "image/jpg" && file.ContentType != "image/jpeg" && file.ContentType != "image/gif")
                {
                    return Json(new Status { Success = false, ErrorMessage = "This is not a valid image type." });
                }

                using (Stream fileStream = file.OpenReadStream())
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    memoryStream.Position = 0;

                    ImageOptimizer optimizer = new ImageOptimizer
                    {
                        OptimalCompression = true,
                        IgnoreUnsupportedFormats = true
                    };
                    optimizer.Compress(memoryStream);

                    if (memoryStream.Length > 250000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)memoryStream.Length / 1000, 1)} KB after being compressed. Your group's display picture cannot be greater than 250 KB." });
                    }

                    string extension = file.ContentType == "image/png" ? "png" : file.ContentType == "image/jpg" ? "jpg" : file.ContentType == "image/jpeg" ? "jpeg" : "gif";
                    string fileName = $"group-display-{Guid.NewGuid()}.{extension}";

                    string url = _googleStorageRepository.UploadFileAsync(memoryStream, fileName, file.ContentType).Result;
                    if (string.IsNullOrEmpty(url))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "The image could not be uploaded. Please try again." });
                    }

                    displayPicUrl = url;
                }
            }

            bool isPrivateGroupEnabled = isPrivateGroup == "on";

            description = System.Text.RegularExpressions.Regex.Replace(description, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);

            if (_pointsRepository.SubtractPoints(user, 500))
            {
                if (_groupsRepository.AddGroup(user, name, displayPicUrl, power, description, isPrivateGroupEnabled, out Group group))
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.GroupJoined))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.GroupJoined);
                    }
                    return Json(new Status { Success = true, Id = group.Id.ToString() });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = "The group could not be created." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int groupId, string name, IFormFile file, GroupPower power, string description, string isPrivateGroup)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (string.IsNullOrEmpty(name))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group must have a name." });
            }

            if (power <= GroupPower.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a Group Power." });
            }

            if (name.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group name is too long." });
            }

            if (string.IsNullOrEmpty(description))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group must have a description." });
            }

            if (description.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your group description is too long." });
            }

            bool isPrivateGroupEnabled = isPrivateGroup == "on";

            description = System.Text.RegularExpressions.Regex.Replace(description, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot edit this group as it does not exist." });
            }

            if (file != null)
            {
                if (!string.IsNullOrEmpty(group.DisplayPicUrl))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You already have an active group display picture. Please remove that one before adding a new one." });
                }

                if (file.Length > 1000000)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)file.Length / 1000, 1)} KB. Your group's display picture cannot be greater than 250 KB." });
                }

                if (file.ContentType != "image/png" && file.ContentType != "image/jpg" && file.ContentType != "image/jpeg" && file.ContentType != "image/gif")
                {
                    return Json(new Status { Success = false, ErrorMessage = "This is not a valid image type." });
                }

                using (Stream fileStream = file.OpenReadStream())
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    memoryStream.Position = 0;

                    ImageOptimizer optimizer = new ImageOptimizer
                    {
                        OptimalCompression = true,
                        IgnoreUnsupportedFormats = true
                    };
                    optimizer.Compress(memoryStream);

                    if (memoryStream.Length > 250000)
                    {
                        return Json(new Status { Success = false, ErrorMessage = $"The image you uploaded is {Math.Round((double)memoryStream.Length / 1000, 1)} KB after being compressed. Your group's display picture cannot be greater than 250 KB." });
                    }

                    string extension = file.ContentType == "image/png" ? "png" : file.ContentType == "image/jpg" ? "jpg" : file.ContentType == "image/jpeg" ? "jpeg" : "gif";
                    string fileName = $"group-display-{Guid.NewGuid()}.{extension}";

                    string url = _googleStorageRepository.UploadFileAsync(memoryStream, fileName, file.ContentType).Result;
                    if (string.IsNullOrEmpty(url))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "The image could not be uploaded. Please try again." });
                    }

                    group.DisplayPicUrl = url;
                }
            }

            GroupMember groupMember = group.Members.Find(x => x.Username == user.Username);
            if (groupMember == null || groupMember.Role != GroupRole.Creator)
            {
                if (user.Role <= UserRole.Helper)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You do not have permission to edit this group." });
                }

                User groupCreator = _userRepository.GetUser(group.Members?.Find(x => x.Role == GroupRole.Creator).Username);
                if (groupCreator != null)
                {
                    _adminHistoryRepository.AddAdminHistory(groupCreator, user, $"Updated group information with id {group.Id}.");
                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        _notificationsRepository.AddNotification(groupCreator, "Your group information was changed for violating the Terms of Service.", NotificationLocation.Groups, $"/Groups?id={group.Id}", string.Empty, domain);
                    });
                }
            }

            if (group.Power != power)
            {
                if ((DateTime.UtcNow - group.LastPowerChangeDate).TotalDays < 1)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You've changed your Group Power too recently." });
                }
                group.Power = power;
                group.LastPowerChangeDate = DateTime.UtcNow;
            }

            group.Name = name;
            group.Description = description;
            group.IsPrivate = isPrivateGroupEnabled;

            if (_groupsRepository.UpdateGroup(group))
            {
                return Json(new Status { Success = true, Id = group.Id.ToString() });
            }

            return Json(new Status { Success = false, ErrorMessage = "This group could not be edited." });
        }

        [HttpPost]
        public IActionResult RemoveDisplayPic(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot edit this group as it does not exist." });
            }

            GroupMember groupMember = group.Members.Find(x => x.Username == user.Username);
            if (groupMember == null || groupMember.Role != GroupRole.Creator)
            {
                if (user.Role <= UserRole.Helper)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You do not have permission to edit this group." });
                }

                User groupCreator = _userRepository.GetUser(group.Members?.Find(x => x.Role == GroupRole.Creator).Username);
                if (groupCreator != null)
                {
                    _adminHistoryRepository.AddAdminHistory(groupCreator, user, $"Remove group display picture with id {group.Id}.");
                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        _notificationsRepository.AddNotification(groupCreator, "Your group display picture was changed for violating the Terms of Service.", NotificationLocation.Groups, $"/Groups?id={group.Id}", string.Empty, domain);
                    });
                }
            }

            if (string.IsNullOrEmpty(group.DisplayPicUrl))
            {
                return Json(new Status { Success = false, ErrorMessage = "There isn't a set display picture for this group." });
            }

            string fileName = group.DisplayPicUrl.Split("/").Last();

            group.DisplayPicUrl = string.Empty;
            if (_groupsRepository.UpdateGroup(group))
            {
                _googleStorageRepository.DeleteFileAsync(fileName);

                return Json(new Status { Success = true, SuccessMessage = "The group's display picture was successfully removed!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The group's display picture could not be removed. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult JoinGroup(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group no longer exists or cannot be found." });
            }

            if (group.IsUserBanned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have been banned from this group." });
            }

            if (group.IsUserMember)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are already a member of this group." });
            }

            if (user.Points < 50)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to join this group." });
            }

            User founderUser = _userRepository.GetUser(group.Members.Find(x => x.Role == GroupRole.Creator).Username);
            if (founderUser != null)
            {
                if (_blockedRepository.HasBlockedUser(founderUser, user, false))
                {
                    return Json(new Status { Success = false, ErrorMessage = "You are not allowed to join this group." });
                }
            }

            if (group.IsPrivate)
            {
                GroupRequest groupRequest = _groupsRepository.GetGroupRequests(group).Find(x => x.Username == user.Username);
                if (groupRequest == null)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must request to join this group before you can become a member." });
                }
                if (!groupRequest.Accepted)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your request to join this group hasn't been accepted yet." });
                }
            }

            if (_pointsRepository.SubtractPoints(user, 50))
            {
                bool isFirstGroup = _groupsRepository.GetGroups(user).Count == 0;
                if (_groupsRepository.AddGroupMember(user, group, user.Username == group.CreatedBy ? GroupRole.Creator : GroupRole.Standard, isFirstGroup))
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.GroupJoined))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.GroupJoined);
                    }
                    _groupsRepository.AddGroupMessage(Helper.GetAdminUser(), group, $"@{user.Username} joined the group! Say hello!");
                    if (group.Members.Any(x => x.Username == group.CreatedBy))
                    {
                        User founder = _userRepository.GetUser(group.CreatedBy);
                        if (founder != null)
                        {
                            _pointsRepository.AddPoints(founder, 25);
                            string domain = Helper.GetDomain();
                            Task.Run(() =>
                            {
                                Settings settings = _settingsRepository.GetSettings(founder, false);
                                if (settings.NotificationsForNewGroupMembers)
                                {
                                    _notificationsRepository.AddNotification(founder, $"@{user.Username} joined your group \"{group.Name}\" and earned you {Helper.GetFormattedPoints(25)}!", NotificationLocation.Groups, $"/Groups?id={group.Id}", domain: domain);
                                }
                                List<Accomplishment> founderAccomplishments = _accomplishmentsRepository.GetAccomplishments(founder, false);
                                if (!founderAccomplishments.Exists(x => x.Id == AccomplishmentId.NewMemberGroup))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(founder, AccomplishmentId.NewMemberGroup, false, domain: domain);
                                }
                                if (group.Members.Count >= 49)
                                {
                                    if (!founderAccomplishments.Exists(x => x.Id == AccomplishmentId.PopularGroup))
                                    {
                                        _accomplishmentsRepository.AddAccomplishment(founder, AccomplishmentId.PopularGroup, false, domain: domain);
                                    }
                                }
                            });
                        }
                    }
                }
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully joined the group!", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LeaveGroup(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group no longer exists or cannot be found." });
            }

            if (group.IsUserBanned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have been banned from this group." });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null || !currentGroup.IsUserMember)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot leave this group as you are not a member of it." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have already left this group." });
            }

            _groupsRepository.DeleteGroupMember(groupMember);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully left the group!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Ban(string memberUser, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (userMember == null || userMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == memberUser);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer belongs to this group." });
            }

            if (groupMember.Role == GroupRole.Banned)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user is already banned from this group." });
            }

            if (userMember.Role == GroupRole.Leader && groupMember.Role > GroupRole.Standard)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to ban this user." });
            }

            if (userMember.Username == groupMember.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot ban yourself." });
            }

            if (_groupsRepository.BanGroupMember(groupMember))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    User groupMemberUser = _userRepository.GetUser(groupMember.Username);
                    if (groupMemberUser != null)
                    {
                        _notificationsRepository.AddNotification(groupMemberUser, $"You were removed from the group \"{currentGroup.Name}\".", NotificationLocation.Groups, $"/Groups?id={currentGroup.Id}", domain: domain);
                    }
                });
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully banned {groupMember.Username} from the group!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Unban(string memberUser, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (userMember == null || userMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == memberUser);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer belongs to this group." });
            }

            if (groupMember.Role != GroupRole.Banned)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user isn't banned from this group." });
            }

            if (userMember.Role == GroupRole.Leader && groupMember.Role > GroupRole.Standard)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to unban this user." });
            }

            if (userMember.Username == groupMember.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot unban yourself." });
            }

            _groupsRepository.UnbanGroupMember(groupMember);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully unbanned {groupMember.Username} from the group!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Promote(string memberUser, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (userMember == null || userMember.Role < GroupRole.Creator)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == memberUser);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer belongs to this group." });
            }

            if (groupMember.Role > GroupRole.Standard)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user isn't able to be promoted." });
            }

            if (currentGroup.Members.FindAll(x => x.Role > GroupRole.Standard).Count >= 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"A group cannot have more than 5 members with a {GroupRole.Leader.GetDisplayName()} role." });
            }

            groupMember.Role = GroupRole.Leader;
            if (_groupsRepository.UpdateGroupMember(groupMember))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    User groupMemberUser = _userRepository.GetUser(groupMember.Username);
                    if (groupMemberUser != null)
                    {
                        string subject = "Group Promotion";
                        string title = "Group Promotion";
                        string body = $"Congratulations! You were promoted to the role of {groupMember.Role.GetDisplayName()} in the group \"{currentGroup.Name}\"! Click the button below to visit your group.";
                        string url = $"/Groups?id={currentGroup.Id}";
                        _emailRepository.SendEmail(groupMemberUser, subject, title, body, url, domain: domain);

                        _notificationsRepository.AddNotification(groupMemberUser, $"You were promoted to the role of {groupMember.Role.GetDisplayName()} in the group \"{currentGroup.Name}\"!", NotificationLocation.Groups, url, domain: domain);

                        List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(groupMemberUser, false);
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.GroupPromotion))
                        {
                            _accomplishmentsRepository.AddAccomplishment(groupMemberUser, AccomplishmentId.GroupPromotion, false, domain: domain);
                        }
                    }
                });
            }

            return Json(new Status { Success = true, SuccessMessage = $"You successfully promoted {groupMember.Username} to the role of {groupMember.Role.GetDisplayName()}!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Demote(string memberUser, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (userMember == null || userMember.Role < GroupRole.Creator)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == memberUser);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer belongs to this group." });
            }

            if (groupMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user isn't able to be demoted." });
            }

            groupMember.Role = GroupRole.Standard;
            _groupsRepository.UpdateGroupMember(groupMember);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully demoted {groupMember.Username} to the role of {groupMember.Role.GetDisplayName()}!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClaimPrize(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (groupMember == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer belong to any group." });
            }

            if (!groupMember.IsPrizeMember)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not chosen to claim the daily prize." });
            }

            if (!groupMember.IsPrimary)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can only claim a prize from a group if you're the primary member." });
            }

            int hoursSinceLastPrizeDate = (int)(DateTime.UtcNow - currentGroup.LastPrizeDate).TotalHours;
            if (hoursSinceLastPrizeDate < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must wait another {24 - hoursSinceLastPrizeDate} hour{(24 - hoursSinceLastPrizeDate == 1 ? "" : "s")} before you can claim the daily prize." });
            }
            if (hoursSinceLastPrizeDate >= 48)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your time has expired to claim the daily prize." });
            }

            int amount = _groupsRepository.ClaimPrize(groupMember, currentGroup);

            currentGroup.LastPrizeDate = DateTime.UtcNow;
            _groupsRepository.UpdateGroup(currentGroup);

            groupMember.IsPrizeMember = false;

            Random rnd = new Random();
            List<GroupMember> activeGroupMembers = currentGroup.Members.FindAll(x => x.IsActive);
            GroupMember newDailyPrizeMember = activeGroupMembers.Count > 1 ? activeGroupMembers[rnd.Next(activeGroupMembers.Count)] : currentGroup.Members[rnd.Next(currentGroup.Members.Count)];
            newDailyPrizeMember.IsPrizeMember = true;

            if (newDailyPrizeMember.Username != groupMember.Username)
            {
                _groupsRepository.UpdateGroupMember(groupMember);
                _groupsRepository.UpdateGroupMember(newDailyPrizeMember);
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.GroupClaimedPrize))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.GroupClaimedPrize);
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully claimed the daily prize of {Helper.GetFormattedPoints(amount)}!", Points = Helper.GetFormattedPoints(user.Points + amount), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditWhiteboard(string whiteboard, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (userMember == null || userMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            if (currentGroup.Whiteboard == whiteboard)
            {
                return Json(new Status { Success = false, ErrorMessage = "No changes were made to the whiteboard." });
            }

            if (whiteboard?.Length > 500)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your whiteboard text cannot exceed 500 characters." });
            }

            whiteboard = System.Text.RegularExpressions.Regex.Replace(whiteboard, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            currentGroup.Whiteboard = whiteboard;

            _groupsRepository.UpdateGroup(currentGroup);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully updated the whiteboard text!" });
        }

        [HttpPost]
        public IActionResult RemoveGroup(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, id);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group does not exist." });
            }

            if (user.Role < UserRole.Helper || (user.Role == UserRole.Helper && group.Members.Count >= 10))
            {
                if (!group.IsUserMember && user.Role < UserRole.Helper)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You are not a member of this group." });
                }
                GroupMember userMember = group.Members.Find(x => x.Username == user.Username);
                if (userMember == null || userMember.Role < GroupRole.Creator)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
                }
            }

            if (_groupsRepository.DeleteGroup(group))
            {
                if (user.Role > UserRole.Default && group.CreatedBy != user.Username)
                {
                    User groupCreator = _userRepository.GetUser(group.CreatedBy);
                    if (groupCreator != null)
                    {
                        _adminHistoryRepository.AddAdminHistory(groupCreator, user, $"Deleted group \"{group.Name}\" with id {group.Id}.");
                    }
                }

                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    foreach (GroupMember groupMember in group.Members)
                    {
                        if (groupMember.Username != user.Username)
                        {
                            User groupUser = _userRepository.GetUser(groupMember.Username);
                            if (groupUser != null)
                            {
                                _notificationsRepository.AddNotification(groupUser, "Your group was removed for violating the Terms of Service. You can look for a new group to join!", NotificationLocation.Groups, "/Groups", string.Empty, domain);
                            }
                        }
                    }
                });
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult RemoveMessage(Guid id, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group does not exist." });
            }

            GroupMessage groupMessage = _groupsRepository.GetGroupMessages(group).Find(x => x.Id == id);
            if (groupMessage == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This message does not exist." });
            }

            if (groupMessage.Sender != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot remove this message as it's not yours." });
            }

            if (groupMessage.IsDeleted)
            {
                return Json(new Status { Success = false, ErrorMessage = "This message has already been deleted." });
            }

            groupMessage.IsDeleted = true;
            if (_groupsRepository.UpdateGroupMessage(groupMessage))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This message couldn't be deleted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RequestGroup(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group no longer exists or cannot be found." });
            }

            if (group.IsUserBanned)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have been banned from this group." });
            }

            if (!group.IsPrivate)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group is not private and you do not need to request to join it." });
            }

            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null || currentGroup.IsUserMember)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are already a member of this group." });
            }

            GroupRequest groupRequest = _groupsRepository.GetGroupRequests(group).Find(x => x.Username == user.Username);
            if (groupRequest != null)
            {
                if (groupRequest.Accepted)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your group request has already been accepted." });
                }

                return Json(new Status { Success = false, ErrorMessage = "Your group request is currently pending." });
            }

            if (_groupsRepository.AddGroupRequest(user, group))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    foreach (GroupMember groupMember in group.Members.FindAll(x => x.Role > GroupRole.Standard))
                    {
                        User groupMemberUser = _userRepository.GetUser(groupMember.Username);
                        if (groupMemberUser != null)
                        {
                            Settings settings = _settingsRepository.GetSettings(groupMemberUser, false);
                            if (settings.NotificationsForGroupJoinRequests)
                            {
                                _notificationsRepository.AddNotification(groupMemberUser, $"@{user.Username} requested to join your group.", NotificationLocation.Groups, $"/Groups?id={groupMember.GroupId}#requests", domain: domain);
                            }
                        }
                    }

                });

                return Json(new Status { Success = true, SuccessMessage = "You successfully requested to join the group! You'll be notified if your request is accepted." });
            }

            return Json(new Status { Success = false, ErrorMessage = "You cannot request to join this group at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AcceptRequest(string username, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            if (!group.IsPrivate)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group is not private and cannot accept join requests." });
            }

            GroupMember groupMember = group.Members.Find(x => x.Username == user.Username);
            if (groupMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to accept a join request for this group." });
            }

            GroupRequest groupRequest = _groupsRepository.GetGroupRequests(group).Find(x => x.Username == username);
            if (groupRequest == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This join request no longer exists." });
            }

            groupRequest.Accepted = true;
            if (_groupsRepository.UpdateGroupRequest(groupRequest))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    User requestUser = _userRepository.GetUser(username);
                    if (requestUser != null)
                    {
                        string subject = "Your group request was accepted!";
                        string title = "Group Request Accepted";
                        string body = $"Your request to join the group, \"{group.Name}\" was accepted! You're now able to join the group. Click the button below to visit the group and join it.";
                        string url = $"/Groups?id={group.Id}";
                        _emailRepository.SendEmail(requestUser, subject, title, body, url, domain: domain);

                        _notificationsRepository.AddNotification(requestUser, $"Your request to join the group \"{group.Name}\" was accepted! You're now able to join the group.", NotificationLocation.Groups, url, domain: domain);
                    }
                });

                return Json(new Status { Success = true, SuccessMessage = "You successfully accepted the request!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The join request couldn't be accepted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RejectRequest(string username, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of any group." });
            }

            if (!group.IsPrivate)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group is not private and cannot reject join requests." });
            }

            GroupMember groupMember = group.Members.Find(x => x.Username == user.Username);
            if (groupMember.Role < GroupRole.Leader)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to reject a join request for this group." });
            }

            GroupRequest groupRequest = _groupsRepository.GetGroupRequests(group).Find(x => x.Username == username);
            if (groupRequest == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This join request no longer exists." });
            }

            User requestUser = _userRepository.GetUser(username);
            if (requestUser != null)
            {
                if (_groupsRepository.DeleteGroupRequest(requestUser, group))
                {
                    return Json(new Status { Success = true, SuccessMessage = "You successfully rejected the request!" });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = "The join request couldn't be rejected." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendMessage(string message, int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Group currentGroup =  _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group does not exist." });
            }

            if (string.IsNullOrEmpty(message))
            {
                return Json(new Status { Success = false, ErrorMessage = "You must enter a message." });
            }

            if (message.Length > 1000)
            {
                return Json(new Status { Success = false, ErrorMessage = "You message is too long." });
            }

            string strippedMessage = System.Text.RegularExpressions.Regex.Replace(message, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            if (_groupsRepository.AddGroupMessage(user, currentGroup, strippedMessage))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    Helper.ExtractUserReply(message, out List<string> userMentions);
                    if (message.Contains("@here"))
                    {
                        foreach (GroupMember member in currentGroup.Members)
                        {
                            if (user.Username.Equals(member.Username, StringComparison.InvariantCultureIgnoreCase))
                            {
                                continue;
                            }
                            User userMention = _userRepository.GetUser(member.Username);
                            if (userMention != null)
                            {
                                _notificationsRepository.AddNotification(userMention, $"@{user.Username} mentioned everyone in your group chat.", NotificationLocation.Groups, $"/Groups/Messages?id={currentGroup.Id}", domain: domain);
                            }
                        }
                    }
                    else
                    {
                        foreach (string username in userMentions)
                        {
                            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                            {
                                continue;
                            }
                            User userMention = _userRepository.GetUser(username);
                            if (userMention != null)
                            {
                                bool isGroupMember = currentGroup.Members.Exists(x => x.Username == username);
                                if (isGroupMember || userMention.Role > UserRole.Default)
                                {
                                    _notificationsRepository.AddNotification(userMention, $"@{user.Username} mentioned you in {(isGroupMember ? "your" : "their")} group chat.", NotificationLocation.Groups, $"/Groups/Messages?id={currentGroup.Id}", domain: domain);
                                }
                            }
                        }
                    }
                });
            }

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult SetPrimaryGroup(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Group> groups = _groupsRepository.GetGroups(user);
            Group group = groups.Find(x => x.Id == groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not a member of this group." });
            }

            if (group.IsPrimary)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group is already your Primary Group." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.LastPrimaryGroupChangeDate).TotalDays < 1)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've changed your Primary Group too recently. Try again in {settings.LastPrimaryGroupChangeDate.AddDays(1).GetTimeUntil()}." });
            }

            Group primaryGroup = groups.Find(x => x.IsPrimary);
            if (primaryGroup != null)
            {
                GroupMember primaryGroupMember = primaryGroup.Members.Find(x => x.Username == user.Username);
                primaryGroupMember.IsPrimary = false;
                _groupsRepository.UpdateGroupMember(primaryGroupMember);
            }

            GroupMember groupMember = group.Members.Find(x => x.Username == user.Username);
            groupMember.IsPrimary = true;
            _groupsRepository.UpdateGroupMember(groupMember);

            settings.LastPrimaryGroupChangeDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastPrimaryGroupChangeDate), settings.LastPrimaryGroupChangeDate.ToString());

            return Json(new Status { Success = true, SuccessMessage = $"The group \"{group.Name}\" was successfully set as your Primary Group!" });
        }

        [HttpGet]
        public IActionResult GetGroupsPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            groupsViewModel.CurrentGroup = _groupsRepository.GetGroup(user, id);
            if (groupsViewModel.CurrentGroup != null)
            {
                groupsViewModel.UniqueLink = $"{Helper.GetDomain(HttpContext)}/Groups?id={groupsViewModel.CurrentGroup.Id}";
                if (groupsViewModel.CurrentGroup.IsUserMember)
                {
                    groupsViewModel.UnreadGroupMessages = _groupsRepository.GetUnreadGroupMessages(user, groupsViewModel.CurrentGroup)?.Count ?? 0;
                    GroupMember groupMember = groupsViewModel.CurrentGroup.Members.Find(x => x.Username == user.Username);
                    groupsViewModel.UserMember = groupMember;
                    if (groupMember.Role > GroupRole.Standard && groupsViewModel.CurrentGroup.IsPrivate)
                    {
                        groupsViewModel.Requests = _groupsRepository.GetGroupRequests(groupsViewModel.CurrentGroup).FindAll(x => !x.Accepted);
                    }
                }
                else
                {
                    if (groupsViewModel.CurrentGroup.IsPrivate)
                    {
                        GroupRequest groupRequest = _groupsRepository.GetGroupRequests(groupsViewModel.CurrentGroup).Find(x => x.Username == user.Username);
                        if (groupRequest != null)
                        {
                            groupsViewModel.RequestStatus = groupRequest.Accepted ? GroupRequestStatus.Accepted : GroupRequestStatus.Pending;
                        }
                    }
                }
                UpdateGroupPrize(groupsViewModel);
            }

            return PartialView("_GroupsIdPartial", groupsViewModel);
        }

        [HttpGet]
        public IActionResult GetUserGroupsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            return PartialView("_GroupsAllUserPartial", groupsViewModel);
        }

        [HttpGet]
        public IActionResult GetGroupMessagesPartialView(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            GroupsViewModel groupsViewModel = GenerateModel(user);
            Group currentGroup = _groupsRepository.GetGroup(user, groupId);
            if (currentGroup == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You aren't part of any group." });
            }
            groupsViewModel.CurrentGroup = currentGroup;
            groupsViewModel.Messages = _groupsRepository.GetGroupMessages(currentGroup);

            Settings settings = _settingsRepository.GetSettings(user);
            if (groupsViewModel.Messages.Count > 0 && currentGroup.IsUserMember)
            {
                GroupMember userMember = currentGroup.Members.Find(x => x.Username == user.Username);
                if (userMember.LastReadMessageDate < groupsViewModel.Messages.Last().CreatedDate)
                {
                    userMember.LastReadMessageDate = groupsViewModel.Messages.Last().CreatedDate.AddSeconds(1);
                    _groupsRepository.UpdateGroupMember(userMember);
                }
            }

            return PartialView("_GroupsMessagesBodyPartial", groupsViewModel);
        }

        [HttpGet]
        public IActionResult GetUnreadGroupMessages(int groupId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            Group group = _groupsRepository.GetGroup(user, groupId);
            if (group == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This group does not exist." });
            }

            return Json(new Status { Success = true, Amount = _groupsRepository.GetUnreadGroupMessages(user, group)?.Count ?? 0 });
        }

        private GroupsViewModel GenerateModel(User user)
        {
            GroupsViewModel groupsViewModel = new GroupsViewModel
            {
                Title = "Groups",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Groups,
                UserGroups = _groupsRepository.GetGroups(user)
            };
            return groupsViewModel;
        }

        private void UpdateGroupPrize(GroupsViewModel groupsViewModel)
        {
            if (groupsViewModel == null || groupsViewModel.CurrentGroup == null || groupsViewModel.CurrentGroup.Members == null)
            {
                return;
            }

            bool updateGroupMember = false;
            Group currentGroup = groupsViewModel.CurrentGroup;
            List<GroupMember> groupMembers = currentGroup.Members.FindAll(x => x.Role != GroupRole.Banned);
            GroupMember groupMember = groupMembers.Find(x => x.IsPrizeMember);
            if (groupMember == null)
            {
                updateGroupMember = true;
            }

            DateTime lastPrizeDate = currentGroup.LastPrizeDate;
            double daysSinceLastPrizeDate = (DateTime.UtcNow - lastPrizeDate).TotalDays;
            if (daysSinceLastPrizeDate >= 2)
            {
                groupsViewModel.CurrentGroup.LastPrizeDate = DateTime.UtcNow;
                _groupsRepository.UpdateGroup(groupsViewModel.CurrentGroup);
                if (groupMember != null)
                {
                    groupMember.IsPrizeMember = false;
                    _groupsRepository.UpdateGroupMember(groupMember);
                }
                updateGroupMember = true;
            }
            if (updateGroupMember)
            {
                groupMembers = groupMembers.FindAll(x => x.IsPrimary);
                if (groupMembers.Count == 0)
                {
                    return;
                }
                Random rnd = new Random();
                List<GroupMember> activeGroupMembers = groupMembers.FindAll(x => x.IsActive);
                groupMember = activeGroupMembers.Count > 1 ? activeGroupMembers[rnd.Next(activeGroupMembers.Count)] : groupMembers[rnd.Next(groupMembers.Count)];
                groupMember.IsPrizeMember = true;
                _groupsRepository.UpdateGroupMember(groupMember);
            }

            groupsViewModel.PrizeMember = groupMember;
        }
    }
}
