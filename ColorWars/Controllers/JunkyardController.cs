﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class JunkyardController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IJunkyardRepository _junkyardRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private ISoundsRepository _soundsRepository;

        public JunkyardController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IItemsRepository itemsRepository, ISettingsRepository settingsRepository, IJunkyardRepository junkyardRepository,
        IAdminSettingsRepository adminSettingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _settingsRepository = settingsRepository;
            _junkyardRepository = junkyardRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Junkyard" });
            }

            JunkyardViewModel junkyardViewModel = GenerateModel(user, true);
            junkyardViewModel.UpdateViewData(ViewData);
            return View(junkyardViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Buy(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            List<Item> junkyardItems = _junkyardRepository.GetJunkyardItems();
            Item junkyardItem = junkyardItems?.Find(x => x.SelectionId == selectionId);
            if (junkyardItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems this item is no longer available.", ShouldRefresh = true });
            }

            int cost = GetBuyPrice(junkyardItem.Points, settings.JunkyardCouponPercent);
            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to afford this item." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                _itemsRepository.AddItem(user, junkyardItem);
                _junkyardRepository.DeleteJunkyardItem(junkyardItem);
                if (settings.JunkyardCouponPercent > 0)
                {
                    settings.JunkyardCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.JunkyardCouponPercent), settings.JunkyardCouponPercent.ToString());
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{junkyardItem.Name}\" was successfully bought for {Helper.GetFormattedPoints(cost)} and added to your bag!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Sell(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Item> items = _itemsRepository.GetItems(user, true);
            Item item = items.Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems you no longer have this item.", ShouldRefresh = true });
            }

            if (item.Category == ItemCategory.Custom)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot sell a Custom Card to the Junkyard." });
            }

            if (item.Uses == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item could not be sold. Please try again.", ShouldRefresh = true });
            }

            int cost = GetSellPrice(item.Points);
            if (_pointsRepository.AddPoints(user, cost))
            {
                if (_junkyardRepository.AddJunkyardItem(item))
                {
                    _itemsRepository.RemoveItem(user, item);
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully sold to the Junkyard for {Helper.GetFormattedPoints(cost)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetJunkyardPartialView(bool isBuy)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            JunkyardViewModel junkyardViewModel = GenerateModel(user, isBuy);
            if (isBuy)
            {
                return PartialView("_JunkyardBuyPartial", junkyardViewModel);
            }
            return PartialView("_JunkyardSellPartial", junkyardViewModel);
        }

        private JunkyardViewModel GenerateModel(User user, bool isBuy)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            JunkyardViewModel junkyardViewModel = new JunkyardViewModel
            {
                Title = "Junkyard",
                Parent = LocationArea.Plaza.ToString(),
                User = user,
                LocationId = LocationId.Junkyard,
                DiscountPercent = settings.JunkyardCouponPercent
            };

            if (isBuy)
            {
                List<Item> items = _junkyardRepository.GetJunkyardItems().Take(100).ToList();
                foreach (Item item in items)
                {
                    item.UpdatePoints(GetBuyPrice(item.Points, settings.JunkyardCouponPercent));
                }
                junkyardViewModel.Items = items;

                Random rnd = new Random();
                if (rnd.Next(25) == 1)
                {
                    Task.Run(() =>
                    {
                        AdminSettings adminSettings = _adminSettingsRepository.GetSettings(false);
                        if ((DateTime.UtcNow - adminSettings.LastDonationZoneItemAdded).TotalHours >= 1)
                        {
                            adminSettings.LastJunkyardItemAdded = DateTime.UtcNow;
                            _adminSettingsRepository.SetSetting(nameof(adminSettings.LastJunkyardItemAdded), adminSettings.LastJunkyardItemAdded.ToString());
                            List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points >= 5000 && x.Points <= 50000);
                            Item item = items[rnd.Next(items.Count)];
                            item.Uses = item.TotalUses;
                            _junkyardRepository.AddSpecialJunkyardItem(item);
                        }
                    });
                }
            }
            else
            {
                List<Item> items = _itemsRepository.GetItems(user, true);
                foreach (Item item in items)
                {
                    item.UpdatePoints(GetSellPrice(item.Points));
                }
                junkyardViewModel.SellItems = items;
            }

            return junkyardViewModel;
        }

        private int GetBuyPrice(int points, int discountPercent)
        {
            points = (int)Math.Round(points * 0.75);
            if (discountPercent > 0)
            {
                points = (int)Math.Round(points * ((double)(100 - discountPercent) / 100));
            }
            return Math.Max(1, points);
        }

        private int GetSellPrice(int points)
        {
            return (int)Math.Max(1, Math.Round(points * 0.45));
        }
    }
}
