﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class FairController : Controller
    {
        private ILoginRepository _loginRepository;
        private ILocationsRepository _locationsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IChecklistRepository _checklistRepository;

        public FairController(ILoginRepository loginRepository, ILocationsRepository locationsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, IChecklistRepository checklistRepository)
        {
            _loginRepository = loginRepository;
            _locationsRepository = locationsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _checklistRepository = checklistRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Fair" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.VisitFair))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.VisitFair);
            }

            FairViewModel fairViewModel = GenerateModel(user, tag);
            fairViewModel.UpdateViewData(ViewData);
            return View(fairViewModel);
        }

        [HttpPost]
        public IActionResult UpdateLocation(LocationId locationId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (locationId == LocationId.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must provide a valid location." });
            }

            List<Location> favoritedLocations = _locationsRepository.GetFavoritedLocations(user);
            if (favoritedLocations.Any(x => x.LocationId == locationId))
            {
                _locationsRepository.RemoveFavoritedLocation(user, locationId);
            }
            else
            {
                _locationsRepository.AddFavoritedLocation(user, locationId);
            }

            return Json(new Status { Success = true });
        }

        [HttpGet]
        public IActionResult GetFairPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            FairViewModel fairViewModel = GenerateModel(user, "");
            return PartialView("_FairMainPartial", fairViewModel);
        }

        private FairViewModel GenerateModel(User user, string tag)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            List<Location> locations = _locationsRepository.GetLocations(settings, accomplishments, LocationArea.Fair, false);
            List < Location > favoritedLocations = _locationsRepository.GetFavoritedLocations(user);
            foreach (Location favoritedLocation in favoritedLocations)
            {
                foreach (Location location in locations)
                {
                    if (favoritedLocation.LocationId == location.LocationId)
                    {
                        location.IsFavorited = true;
                        break;
                    }
                }
            }

            FairViewModel fairViewModel = new FairViewModel
            {
                Title = "Fair",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Locations = locations.OrderByDescending(x => x.IsFavorited).ThenBy(x => x.Name).ToList().GroupBy(x => x.LocationCategory).OrderByDescending(x => x.Key),
                Tag = tag
            };

            if (!settings.HasSeenFairTutorial)
            {
                settings.HasSeenFairTutorial = true;
                fairViewModel.ShowFairTutorial = _settingsRepository.SetSetting(user, nameof(settings.HasSeenFairTutorial), settings.HasSeenFairTutorial.ToString());
            }

            return fairViewModel;
        }
    }
}
