﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class NoticeBoardController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private INoticeBoardTasksRepository _noticeBoardTasksRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public NoticeBoardController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        INoticeBoardTasksRepository noticeBoardTasksRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _noticeBoardTasksRepository = noticeBoardTasksRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"NoticeBoard" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.NoticeBoardExpiryDate <= DateTime.UtcNow)
            {
                return RedirectToAction("Index", "Plaza", new { locationNotAvailable = true });
            }

            if (!settings.CheckedNoticeBoard)
            {
                settings.CheckedNoticeBoard = true;
                _settingsRepository.SetSetting(user, nameof(settings.CheckedNoticeBoard), settings.CheckedNoticeBoard.ToString());
            }

            NoticeBoardViewModel noticeBoardViewModel = GenerateModel(user);
            noticeBoardViewModel.UpdateViewData(ViewData);
            return View(noticeBoardViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CompleteTask(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.NoticeBoardExpiryDate <= DateTime.UtcNow)
            {
                return Json(new Status { Success = false, ErrorMessage = "The Notice Board doesn't have any open tasks.", ShouldRefresh = true });
            }

            Item item = _itemsRepository.GetItems(user).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            NoticeBoardTask noticeBoardTask = _noticeBoardTasksRepository.GetNoticeBoardTask(user);
            if (noticeBoardTask == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a Notice Board task." });
            }

            List<Item> matchingItems = GetMatchingItems(user, noticeBoardTask);
            if (matchingItems.Count == 0 || !matchingItems.Any(x => x.SelectionId == item.SelectionId))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your item doesn't match the Notice Board task requirements." });
            }

            if (_pointsRepository.AddPoints(user, noticeBoardTask.Reward))
            {
                _itemsRepository.RemoveItem(user, item);

                settings.NoticeBoardTasksCompleted++;
                settings.NoticeBoardExpiryDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.NoticeBoardTasksCompleted), settings.NoticeBoardTasksCompleted.ToString());
                _settingsRepository.SetSetting(user, nameof(settings.NoticeBoardExpiryDate), settings.NoticeBoardExpiryDate.ToString());

                _noticeBoardTasksRepository.DeleteNoticeBoardTask(user);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.NoticeBoardTaskCompleted))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.NoticeBoardTaskCompleted);
                }
                if (noticeBoardTask.Reward >= 50000)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HardNoticeBoardTaskCompleted))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HardNoticeBoardTaskCompleted);
                    }
                }

                Messages.SetSuccessMessage($"You successfully completed the Notice Board task and earned {Helper.GetFormattedPoints(noticeBoardTask.Reward)}!");

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "The Notice Board task could not be completed." });
        }

        private NoticeBoardViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            NoticeBoardTask noticeBoardTask = _noticeBoardTasksRepository.GetNoticeBoardTask(user);
            if (noticeBoardTask == null)
            {
                noticeBoardTask = GenerateNewNoticeBoardTask(user, settings);
            }

            Item item = _itemsRepository.GetAllItems().Find(x => x.Id == noticeBoardTask.ItemId);
            List<Item> matchingItems = GetMatchingItems(user, noticeBoardTask);

            NoticeBoardViewModel noticeBoardViewModel = new NoticeBoardViewModel
            {
                Title = "Notice Board",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.NoticeBoard,
                Task = noticeBoardTask,
                Item = item,
                Items = matchingItems,
                ExpiryDate = settings.NoticeBoardExpiryDate
            };
            return noticeBoardViewModel;
        }

        private NoticeBoardTask GenerateNewNoticeBoardTask(User user, Settings settings)
        {
            Random rnd = new Random();
            List<Item> items = _itemsRepository.GetAllItems().FindAll(x => x.Points >= 1000 && x.Points <= Math.Min(100000, (settings.NoticeBoardTasksCompleted + 1) * 5000));
            Item item = items[rnd.Next(items.Count)];

            List<ItemCondition> itemConditions = Enum.GetValues(typeof(ItemCondition)).Cast<ItemCondition>().ToList();
            itemConditions.Remove(ItemCondition.All);
            ItemCondition itemCondition = rnd.Next(2) == 1 ? ItemCondition.New : itemConditions[rnd.Next(itemConditions.Count)];

            bool isExactCondition = itemCondition == ItemCondition.New;
            bool hasTotalUses = item.TotalUses > 1 && rnd.Next(0, 2) == 1;
            int itemAgeInDays = rnd.Next(1, 31);

            double reward = item.Points * 1.35;
            switch (itemCondition)
            {
                case ItemCondition.New:
                    reward *= 1.2;
                    break;
                case ItemCondition.Good:
                    reward *= 1.25;
                    break;
                case ItemCondition.Poor:
                    reward *= 1.3;
                    break;
            }
            if (isExactCondition)
            {
                reward *= 1.15;
            }
            if (hasTotalUses)
            {
                reward *= 1.15;
            }
            if (item.IsExclusive)
            {
                reward *= 1.5;
            }
            reward *= 1 + 0.015 * itemAgeInDays;
            reward = Math.Min(item.Points * 5, reward * 1.5);

            int roundedReward = (int)(Math.Round(reward / 10) * 10);

            if (_noticeBoardTasksRepository.AddNoticeBoardTask(user, item, itemCondition, isExactCondition, hasTotalUses, roundedReward, itemAgeInDays))
            {
                return _noticeBoardTasksRepository.GetNoticeBoardTask(user);
            }

            Log.Debug($"Can't generate new Notice Board task for user {user.Username}.");
            throw new Exception("Can't generate new Notice Board task.");
        }

        private List<Item> GetMatchingItems(User user, NoticeBoardTask noticeBoardTask)
        {
            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None && x.Id == noticeBoardTask.ItemId);
            List<Item> matchingItems = new List<Item>();
            foreach (Item item in items)
            {
                if (noticeBoardTask.HasTotalUses && item.Uses < item.TotalUses)
                {
                    continue;
                }
                if (noticeBoardTask.IsExactCondition)
                {
                    if (item.Condition != noticeBoardTask.ItemCondition)
                    {
                        continue;
                    }
                }
                else
                {
                    if (item.Condition > noticeBoardTask.ItemCondition)
                    {
                        continue;
                    }
                }
                if (item.GetAgeInHours() < (DateTime.UtcNow - noticeBoardTask.ItemCreatedDate).TotalHours - (DateTime.UtcNow - noticeBoardTask.CreatedDate).TotalHours)
                {
                    continue;
                }
                matchingItems.Add(item);
            }

            return matchingItems;
        }
    }
}
