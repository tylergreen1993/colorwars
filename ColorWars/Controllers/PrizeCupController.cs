﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class PrizeCupController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public PrizeCupController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "PrizeCup" });
            }

            PrizeCupViewModel prizeCupViewModel = GenerateModel(user);
            prizeCupViewModel.UpdateViewData(ViewData);
            return View(prizeCupViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (position >= 20 || position < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot flip this cup." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastFlip = (int)(DateTime.UtcNow - settings.LastPrizeCupPlayDate).TotalHours;
            if (hoursSinceLastFlip < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Prize Cup too recently. Try again in {5 - hoursSinceLastFlip} hour{(5 - hoursSinceLastFlip == 1 ? "" : "s")}." });
            }

            List<PrizeCup> prizeCups = GetPrizeCups(user, settings);
            if (prizeCups[position].IsRevealed)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already flipped over this cup." });
            }

            CardColor marbleColor = CardColor.Green;
            Random rnd = new Random();
            if (rnd.Next(5) == 1)
            {
                marbleColor = CardColor.Gold;
            }
            else if (prizeCups.Count(x => x.IsRevealed) > 1 && rnd.Next(4) == 1)
            {
                marbleColor = CardColor.Red;
            }

            prizeCups[position].Color = marbleColor;
            settings.PrizeCupPositions = string.Join("-", prizeCups.Select(x => x.Color));
            _settingsRepository.SetSetting(user, nameof(settings.PrizeCupPositions), settings.PrizeCupPositions);

            if (marbleColor == CardColor.Red)
            {
                settings.FinishedPrizeCupGame = true;
                _settingsRepository.SetSetting(user, nameof(settings.FinishedPrizeCupGame), settings.FinishedPrizeCupGame.ToString());

                settings.LastPrizeCupPlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastPrizeCupPlayDate), settings.LastPrizeCupPlayDate.ToString());

                int points = GetPointsEarned(prizeCups);

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

                return Json(new Status { Success = false, DangerMessage = $"You found a Red Marble and lost out on the {Helper.GetFormattedPoints(points)} Prize Pot! Better luck next time!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = true, SuccessMessage = $"You found a {marbleColor} Marble and {Helper.GetFormattedPoints(marbleColor == CardColor.Green ? 250 : 1000)} was added to your Prize Pot!" });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult TakePot()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastFlip = (int)(DateTime.UtcNow - settings.LastPrizeCupPlayDate).TotalHours;
            if (hoursSinceLastFlip < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Prize Cup too recently. Try again in {5 - hoursSinceLastFlip} hour{(5 - hoursSinceLastFlip == 1 ? "" : "s")}." });
            }

            List<PrizeCup> prizeCups = GetPrizeCups(user, settings);
            int pot = GetPointsEarned(prizeCups);
            if (prizeCups.Any(x => x.Color == CardColor.Red) || pot <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't take {Helper.GetFormattedPoints(0)}." });
            }

            if (_pointsRepository.AddPoints(user, pot))
            {
                settings.FinishedPrizeCupGame = true;
                _settingsRepository.SetSetting(user, nameof(settings.FinishedPrizeCupGame), settings.FinishedPrizeCupGame.ToString());

                settings.LastPrizeCupPlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastPrizeCupPlayDate), settings.LastPrizeCupPlayDate.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.PrizeCupTookPot))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.PrizeCupTookPot);
                }

                if (prizeCups.All(x => x.IsRevealed && x.Color != CardColor.Red))
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RiskyPrizeCupWin))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RiskyPrizeCupWin);
                    }
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took the Prize Pot and earned {Helper.GetFormattedPoints(pot)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You couldn't take the Prize Pot." });
        }

        [HttpGet]
        public IActionResult GetPrizeCupPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            PrizeCupViewModel prizeCupViewModel = GenerateModel(user);
            return PartialView("_PrizeCupMainPartial", prizeCupViewModel);
        }

        private PrizeCupViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<PrizeCup> prizeCups = GetPrizeCups(user, settings);
            bool canPlay = (DateTime.UtcNow - settings.LastPrizeCupPlayDate).TotalHours >= 5;

            PrizeCupViewModel prizeCupViewModel = new PrizeCupViewModel
            {
                Title = "Prize Cup",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                PrizeCups = prizeCups,
                Pot = GetPointsEarned(prizeCups),
                CanPlay = canPlay
            };

            return prizeCupViewModel;
        }

        private List<PrizeCup> GetPrizeCups(User user, Settings settings)
        {
            if (settings.FinishedPrizeCupGame && (DateTime.UtcNow - settings.LastPrizeCupPlayDate).TotalHours >= 5)
            {
                settings.FinishedPrizeCupGame = false;
                _settingsRepository.SetSetting(user, nameof(settings.FinishedPrizeCupGame), settings.FinishedPrizeCupGame.ToString());

                settings.PrizeCupPositions = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.PrizeCupPositions), settings.PrizeCupPositions);
            }

            int totalPrizeCups = 20;
            List<PrizeCup> prizeCups = new List<PrizeCup>();
            if (string.IsNullOrEmpty(settings.PrizeCupPositions))
            {
                for (int i = 0; i < totalPrizeCups; i++)
                {
                    prizeCups.Add(new PrizeCup
                    {
                        Position = i,
                        Color = CardColor.None
                    });
                }
            }
            else
            {
                List<CardColor> positions = settings.PrizeCupPositions.Split("-").Select(x => Enum.Parse<CardColor>(x)).ToList();
                for (int i = 0; i < totalPrizeCups; i++)
                {
                    prizeCups.Add(new PrizeCup
                    {
                        Position = i,
                        Color = positions[i]
                    });
                }
            }

            return prizeCups;
        }

        private int GetPointsEarned(List<PrizeCup> prizeCups)
        {
            return prizeCups.Sum(x => x.Color == CardColor.Green ? 250 : x.Color == CardColor.Gold ? 1000 : 0);
        }
    }
}
