﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class StoreController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private IStoreRepository _storeRepository;
        private IStoreDiscountRepository _storeDiscountRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public StoreController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        IStoreRepository storeRepository, IStoreDiscountRepository storeDiscountRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _storeRepository = storeRepository;
            _storeDiscountRepository = storeDiscountRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Store" });
            }

            StoreViewModel storeViewModel = GenerateModel(user);
            storeViewModel.UpdateViewData(ViewData);
            return View(storeViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Bargain(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            ItemCategory discountItemCategory = _storeDiscountRepository.GetDiscountCategory(user);

            List<StoreItem> storeItems = ApplyCoupon(_storeRepository.GetStoreItems(), settings.GeneralStoreCouponPercent, discountItemCategory);
            StoreItem storeItem = storeItems?.Find(x => x.Id == itemId);

            if (storeItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems this item is no longer in stock.", ShouldRefresh = true });
            }

            StoreViewModel storeViewModel = GenerateModel(user);
            storeViewModel.SelectedItem = storeItem;

            return PartialView("_StoreBargainPartial", storeViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult MakeBargain(Guid itemId, int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item.", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            int totalMinutesSinceLastBuyCount = (int)(DateTime.UtcNow - settings.LastGeneralStoreBuyDate).TotalMinutes;
            if (totalMinutesSinceLastBuyCount < 60 && settings.GeneralStoreRecentBuyCount >= 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought too many items from the General Store recently. Try again in {60 - totalMinutesSinceLastBuyCount} minute{(60 - totalMinutesSinceLastBuyCount == 1 ? "" : "s")}." });
            }

            ItemCategory discountItemCategory = _storeDiscountRepository.GetDiscountCategory(user);

            List<StoreItem> storeItems = ApplyCoupon(_storeRepository.GetStoreItems(), settings.GeneralStoreCouponPercent, discountItemCategory);
            StoreItem storeItem = storeItems?.Find(x => x.Id == itemId);
            if (storeItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems this item is no longer in stock.", ShouldRefresh = true });
            }

            if (amount > user.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }
            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You must offer more than {Helper.GetFormattedPoints(0)}." });
            }
            if (amount > storeItem.Cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"It wouldn't be a very good bargain to spend more than the item's cost." });
            }
            if (amount < storeItem.MinCost)
            {
                if (amount >= storeItem.MinCost * 0.8)
                {
                    double percentOffStoreMinItemCost = (double)amount / storeItem.MinCost;
                    int differenceBetweenMinCostandCost = storeItem.Cost - storeItem.MinCost;
                    int counterCost = storeItem.MinCost + (int)Math.Round(differenceBetweenMinCostandCost * (1 - percentOffStoreMinItemCost));

                    return Json(new Status { Success = true, InfoMessage = $"The General Store declined your offer of {Helper.GetFormattedPoints(amount)}. How about {Helper.GetFormattedPoints(counterCost)}?{(counterCost == storeItem.MinCost ? " This is as low as you'll get!" : "")}" });
                }
                return Json(new Status { Success = true, InfoMessage = $"The General Store declined your offer of {Helper.GetFormattedPoints(amount)}. It was too low to even give a counter-offer." });
            }

            Item item = _itemsRepository.GetAllItems().Find(x => x.Id == storeItem.Id);
            if (_pointsRepository.SubtractPoints(user, amount))
            {
                _itemsRepository.AddItem(user, item);

                if (settings.GeneralStoreCouponPercent > 0)
                {
                    settings.GeneralStoreCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreCouponPercent), settings.GeneralStoreCouponPercent.ToString());
                }

                if (totalMinutesSinceLastBuyCount >= 60)
                {
                    settings.LastGeneralStoreBuyDate = DateTime.UtcNow;
                    settings.GeneralStoreRecentBuyCount = 1;
                    _settingsRepository.SetSetting(user, nameof(settings.LastGeneralStoreBuyDate), settings.LastGeneralStoreBuyDate.ToString());
                }
                else
                {
                    settings.GeneralStoreRecentBuyCount++;
                }
                _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreRecentBuyCount), settings.GeneralStoreRecentBuyCount.ToString());
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (amount < storeItem.Cost)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SuccessfulBargain))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SuccessfulBargain);
                }
            }
            if (amount >= 100000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigSpender))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigSpender);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You offer of {Helper.GetFormattedPoints(amount)} was accepted! The item \"{item.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult QuickBuy(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            int totalMinutesSinceLastBuyCount = (int)(DateTime.UtcNow - settings.LastGeneralStoreBuyDate).TotalMinutes;
            if (totalMinutesSinceLastBuyCount < 60 && settings.GeneralStoreRecentBuyCount >= 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought too many items from the General Store recently. Try again in {60 - totalMinutesSinceLastBuyCount} minute{(60 - totalMinutesSinceLastBuyCount == 1 ? "" : "s")}." });
            }

            ItemCategory discountItemCategory = _storeDiscountRepository.GetDiscountCategory(user);

            List<StoreItem> storeItems = ApplyCoupon(_storeRepository.GetStoreItems(), settings.GeneralStoreCouponPercent, discountItemCategory);
            StoreItem storeItem = storeItems?.Find(x => x.Id == itemId);
            if (storeItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "It seems this item is no longer in stock.", ShouldRefresh = true });
            }

            if (storeItem.Cost > user.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Item item = _itemsRepository.GetAllItems().Find(x => x.Id == storeItem.Id);
            if (_pointsRepository.SubtractPoints(user, storeItem.Cost))
            {
                _itemsRepository.AddItem(user, item);

                if (settings.GeneralStoreCouponPercent > 0)
                {
                    settings.GeneralStoreCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreCouponPercent), settings.GeneralStoreCouponPercent.ToString());
                }

                if (totalMinutesSinceLastBuyCount >= 60)
                {
                    settings.LastGeneralStoreBuyDate = DateTime.UtcNow;
                    settings.GeneralStoreRecentBuyCount = 1;
                    _settingsRepository.SetSetting(user, nameof(settings.LastGeneralStoreBuyDate), settings.LastGeneralStoreBuyDate.ToString());
                }
                else
                {
                    settings.GeneralStoreRecentBuyCount++;
                }
                _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreRecentBuyCount), settings.GeneralStoreRecentBuyCount.ToString());
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetStorePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            StoreViewModel storeViewModel = GenerateModel(user);
            return PartialView("_StoreMainPartial", storeViewModel);
        }

        private StoreViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            ItemCategory discountItemCategory = _storeDiscountRepository.GetDiscountCategory(user);

            StoreViewModel storeViewModel = new StoreViewModel
            {
                Title = "General Store",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.GeneralStore,
                StoreItems = ApplyCoupon(_storeRepository.GenerateStoreItems(), settings.GeneralStoreCouponPercent, discountItemCategory),
                CanBuy = settings.GeneralStoreRecentBuyCount < 3 || (DateTime.UtcNow - settings.LastGeneralStoreBuyDate).TotalHours >= 1,
                RemainingItems = 3 - settings.GeneralStoreRecentBuyCount,
                DiscountPercent = settings.GeneralStoreCouponPercent,
                DiscountItemCategory = discountItemCategory,
                LastBuyDate = settings.LastGeneralStoreBuyDate
            };

            return storeViewModel;
        }

        private List<StoreItem> ApplyCoupon(List<StoreItem> storeItems, int couponPercent, ItemCategory discountItemCategory)
        {
            if (couponPercent <= 0 && discountItemCategory == ItemCategory.None)
            {
                return storeItems;
            }

            foreach (StoreItem storeItem in storeItems)
            {
                if (couponPercent > 0)
                {
                    storeItem.Cost = Math.Max(1, (int)Math.Round(storeItem.Cost * ((double)(100 - couponPercent) / 100)));
                    storeItem.MinCost = Math.Max(1, (int)Math.Round(storeItem.MinCost * ((double)(100 - couponPercent) / 100)));
                }

                if (storeItem.Category == discountItemCategory)
                {
                    storeItem.Cost = Math.Max(1, (int)Math.Round(storeItem.Cost * 0.85));
                    storeItem.MinCost = Math.Max(1, (int)Math.Round(storeItem.MinCost * 0.85));
                }
            }

            return storeItems;
        }
    }
}
