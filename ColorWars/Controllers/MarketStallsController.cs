﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ColorWars.Controllers
{
    public class MarketStallsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IMarketStallRepository _marketStallRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IRepairRepository _repairRepository;
        private ISettingsRepository _settingsRepository;
        private IUserRepository _userRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationRepository;
        private IAdminHistoryRepository _adminHistoryRepository;
        private ISoundsRepository _soundsRepository;

        public MarketStallsController(ILoginRepository loginRepository, IMarketStallRepository marketStallRepository, IItemsRepository itemsRepository,
        IPointsRepository pointsRepository, IRepairRepository repairRepository, ISettingsRepository settingsRepository, IUserRepository userRepository,
        IAccomplishmentsRepository accomplishmentsRepository, IEmailRepository emailRepository, INotificationsRepository notificationsRepository,
        IAdminHistoryRepository adminHistoryRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _marketStallRepository = marketStallRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _repairRepository = repairRepository;
            _settingsRepository = settingsRepository;
            _userRepository = userRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _emailRepository = emailRepository;
            _notificationRepository = notificationsRepository;
            _adminHistoryRepository = adminHistoryRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(int id, string query, int page, bool openLedger, bool matchExactSearch, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"MarketStalls{(id == 0 ? "" : $"?id={id}")}" });
            }

            MarketStallsViewModel marketStallsViewModel = GenerateModel(user, id, query, page, false, openLedger, matchExactSearch, category, condition);
            marketStallsViewModel.UpdateViewData(ViewData);
            return View(marketStallsViewModel);
        }

        public IActionResult Create()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = $"MarketStalls/Create" });
            }

            MarketStallsViewModel marketStallsViewModel = GenerateModel(user, 0, string.Empty, 0, true);
            marketStallsViewModel.UpdateViewData(ViewData);

            if (marketStallsViewModel.UserMarketStallId > 0)
            {
                return RedirectToAction("Index", "MarketStalls", new { id = marketStallsViewModel.UserMarketStallId });
            }

            return View(marketStallsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(string marketStallName, string description, string color, string marketStallItems)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<MarketStallItem> items = JsonConvert.DeserializeObject<List<MarketStallItem>>(marketStallItems);
            if (items == null || items.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to choose at least one item to create a Market Stall." });
            }

            if (string.IsNullOrEmpty(marketStallName))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to choose a name for your Market Stall." });
            }

            if (!string.IsNullOrEmpty(color))
            {
                try
                {
                    _ = ColorTranslator.FromHtml(color);
                }
                catch (Exception)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must enter a valid color." });
                }
            }

            if (marketStallName.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Market Stall name is too long." });
            }

            if (!string.IsNullOrEmpty(description))
            {
                if (description.Length > 500)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your Market Stall message is too long." });
                }

                description = System.Text.RegularExpressions.Regex.Replace(description, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            }

            if (user.Points < 250)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You need at least {Helper.GetFormattedPoints(250)} on hand to create a Market Stall." });
            }

            if (items.Any(x => x.Cost <= 0))
            {
                return Json(new Status { Success = false, ErrorMessage = $"None of your items can have a negative cost." });
            }

            if (items.Any(x => x.Cost > 1000000))
            {
                return Json(new Status { Success = false, ErrorMessage = $"None of your items can cost more than {Helper.GetFormattedPoints(1000000)}." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (items.Count > settings.MaxMarketStallSize)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot have more than {Helper.FormatNumber(settings.MaxMarketStallSize)} items in your Market Stall." });
            }

            int userMarketStallId = _marketStallRepository.GetUserMarketStallId(user);
            if (userMarketStallId != 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have a Market Stall." });
            }

            List<Item> userItems = _itemsRepository.GetItems(user, true);
            List<Item> itemsToAdd = userItems.FindAll(x => items.Any(y => y.SelectionId == x.SelectionId));
            if (itemsToAdd.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't chosen any valid items." });
            }

            if (_pointsRepository.SubtractPoints(user, 250))
            {
                if (_marketStallRepository.AddMarketStall(user, marketStallName, color, description, out MarketStall marketStall))
                {
                    foreach (Item item in itemsToAdd)
                    {
                        int cost = items.Find(x => x.SelectionId == item.SelectionId).Cost;
                        if (_marketStallRepository.AddMarketStallItem(user, marketStall, item, cost))
                        {
                            _itemsRepository.RemoveItem(user, item);
                        }
                    }

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OpenedMarketStall))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OpenedMarketStall);
                    }

                    return Json(new Status { Success = true, Id = marketStall.Id.ToString() });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = "Your Market Stall could not be created." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyItem(int marketStallId, Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MarketStall marketStall = _marketStallRepository.GetMarketStall(marketStallId);
            if (marketStall == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item and this Market Stall no longer exist." });
            }

            int userMarketStallId = _marketStallRepository.GetUserMarketStallId(user);
            if (marketStall.Id == userMarketStallId)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot buy an item from your own Market Stall." });
            }

            MarketStallItem marketStallItem = marketStall.Items.Find(x => x.SelectionId == selectionId);
            if (marketStallItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer available for sale." });
            }

            if (user.Points < marketStallItem.Cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to buy this item." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            if (_pointsRepository.SubtractPoints(user, marketStallItem.Cost))
            {
                if (_marketStallRepository.DeleteMarketStallItem(marketStallItem))
                {
                    _itemsRepository.AddItem(user, marketStallItem);
                    _marketStallRepository.AddMarketStallHistory(user, marketStall, marketStallItem);

                    string domain = Helper.GetDomain();
                    Task.Run(() =>
                    {
                        User marketStallOwner = _userRepository.GetUser(marketStall.Username);
                        if (marketStallOwner != null)
                        {
                            _pointsRepository.AddPoints(marketStallOwner, marketStallItem.Cost);

                            string url = $"/MarketStalls?id={marketStall.Id}&openLedger=true#marketStallLedgers";
                            _notificationRepository.AddNotification(marketStallOwner, $"@{user.Username} bought your item \"{marketStallItem.Name}\" for {Helper.GetFormattedPoints(marketStallItem.Cost)}!", NotificationLocation.MarketStalls, url, domain: domain);

                            if (marketStall.Items.Count == 1)
                            {
                                _notificationRepository.AddNotification(marketStallOwner, $"Your Market Stall is out of items! You should consider adding some more.", NotificationLocation.MarketStalls, $"/MarketStalls?id={marketStall.Id}", domain: domain);
                            }

                            Settings marketStallOwnerSettings = _settingsRepository.GetSettings(marketStallOwner, false);
                            if (marketStallOwnerSettings.EmailsForMarketStallPurchases)
                            {
                                string subject = "An item was bought from your Market Stall";
                                string title = "Market Stall Purchase";
                                string body = $"Your item <b>\"{marketStallItem.Name}\"</b> was bought from your Market Stall for <b>{Helper.GetFormattedPoints(marketStallItem.Cost)}</b>! You can click the button below to visit your Market Stall.";

                                _emailRepository.SendEmail(marketStallOwner, subject, title, body, url, domain: domain);
                            }

                            if (marketStallItem.Cost >= 50000)
                            {
                                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(marketStallOwner, false);
                                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MarketStallBigItemBought))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(marketStallOwner, AccomplishmentId.MarketStallBigItemBought, false, domain);
                                }
                            }

                            List<MarketStallHistory> marketStallHistory = _marketStallRepository.GetMarketStallHistory(marketStall);
                            if (marketStallHistory.Sum(x => x.Cost) + marketStallItem.Cost >= 1000000)
                            {
                                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(marketStallOwner, false);
                                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MillionaireMarketStall))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(marketStallOwner, AccomplishmentId.MillionaireMarketStall, false, domain);
                                }
                            }

                            if (marketStallItem.Cost >= marketStallItem.Points * 1.5 || marketStallItem.Cost <= marketStallItem.Points * 0.5)
                            {
                                int difference = Math.Abs(marketStallItem.Points - marketStallItem.Cost);
                                if (difference >= 5000)
                                {
                                    _adminHistoryRepository.AddTransferHistory(marketStall.Id, TransferType.MarketStalls, marketStallOwner, user, difference);
                                }
                            }
                        }
                    });

                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully purchased the item \"{marketStallItem.Name}\" for {Helper.GetFormattedPoints(marketStallItem.Cost)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be bought." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MarketStall marketStall = _marketStallRepository.GetMarketStall(user);
            if (marketStall == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not own a Market Stall." });
            }

            MarketStallItem marketStallItem = marketStall.Items.Find(x => x.SelectionId == selectionId);
            if (marketStallItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item does not exist in your Market Stall." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add this item back." });
            }

            if (_marketStallRepository.DeleteMarketStallItem(marketStallItem))
            {
                _itemsRepository.AddItem(user, marketStallItem);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{marketStallItem.Name}\" was successfully added back to your bag!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be removed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Manage(string marketStallName, string description, string color, int discount, string marketStallItems)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MarketStall marketStall = _marketStallRepository.GetMarketStall(user);
            if (marketStall == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not own a Market Stall." });
            }

            if (string.IsNullOrEmpty(marketStallName))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to choose a name for your Market Stall." });
            }

            if (!string.IsNullOrEmpty(description))
            {
                if (description.Length > 500)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your Market Stall message is too long." });
                }

                description = System.Text.RegularExpressions.Regex.Replace(description, @"(\r?\n\s*){2,}", Environment.NewLine + Environment.NewLine);
            }

            if (!string.IsNullOrEmpty(color))
            {
                try
                {
                    _ = ColorTranslator.FromHtml(color);
                }
                catch (Exception)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You must enter a valid color." });
                }
            }

            if (marketStallName.Length > 30)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your Market Stall name is too long." });
            }

            if (discount < 0 || discount > 75)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your discount percentage is not allowed." });
            }

            bool hasChanges = false;

            List<MarketStallItem> items = JsonConvert.DeserializeObject<List<MarketStallItem>>(marketStallItems);
            if (items != null && items.Count > 0)
            {
                if (items.Any(x => x.Cost <= 0))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"None of your items can have a negative cost." });
                }

                if (items.Any(x => x.Cost > 1000000))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"None of your items can cost more than {Helper.GetFormattedPoints(1000000)}." });
                }

                Settings settings = _settingsRepository.GetSettings(user);
                if (items.Count + marketStall.Items.Count > settings.MaxMarketStallSize)
                {
                    return Json(new Status { Success = false, ErrorMessage = $"You cannot have more than {Helper.FormatNumber(settings.MaxMarketStallSize)} items in your Market Stall." });
                }

                List<Item> userItems = _itemsRepository.GetItems(user, true);
                List<Item> itemsToAdd = userItems.FindAll(x => items.Any(y => y.SelectionId == x.SelectionId));
                if (itemsToAdd.Count == 0)
                {
                    return Json(new Status { Success = false, ErrorMessage = "You haven't chosen any valid items." });
                }

                hasChanges = true;

                foreach (Item item in itemsToAdd)
                {
                    int cost = items.Find(x => x.SelectionId == item.SelectionId).Cost;
                    if (_marketStallRepository.AddMarketStallItem(user, marketStall, item, cost))
                    {
                        _itemsRepository.RemoveItem(user, item);
                    }
                }
            }

            bool hasMarketStallChanges = false;
            if (marketStallName != marketStall.Name)
            {
                hasChanges = true;
                hasMarketStallChanges = true;
                marketStall.Name = marketStallName;
            }
            if (description != marketStall.Description)
            {
                hasChanges = true;
                hasMarketStallChanges = true;
                marketStall.Description = description;
            }
            if (color != marketStall.Color)
            {
                hasChanges = true;
                hasMarketStallChanges = true;
                marketStall.Color = color;
            }
            if (discount != marketStall.Discount)
            {
                hasChanges = true;
                hasMarketStallChanges = true;
                marketStall.Discount = discount;
            }

            if (hasMarketStallChanges)
            {
                _marketStallRepository.UpdateMarketStall(marketStall);
            }

            if (hasChanges)
            {
                return Json(new Status { Success = true, SuccessMessage = $"Your Market Stall was successfully updated!" });
            }

            return Json(new Status { Success = true, InfoMessage = $"No changes were made to your Market Stall." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Repair()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MarketStall marketStall = _marketStallRepository.GetMarketStall(user);
            if (marketStall == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not own a Market Stall." });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            int repairCost = marketStall.Items.Sum(x => _repairRepository.GetRepairPoints(x, settings.RepairCouponPercent));
            if (repairCost <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have any items in your Market Stall that need to be repaired." });
            }

            if (user.Points < repairCost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to repair all your Market Stall items." });
            }

            if (_pointsRepository.SubtractPoints(user, repairCost))
            {
                if (_marketStallRepository.RepairAllMarketStallItems(marketStall))
                {
                    settings.RepairCouponPercent = 0;
                    _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), settings.RepairCouponPercent.ToString());

                    return Json(new Status { Success = true, SuccessMessage = marketStall.Items.Count == 1 ? "The item in your Market Stall was successfully repaired!" : "All the items in your Market Stall were successfully repaired!", Points = user.GetFormattedPoints() });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = "The items in your Market Stall could not be repaired." });
        }

        [HttpPost]
        public IActionResult RemoveMarketStall(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            MarketStall marketStall = user.Role > UserRole.Helper ? _marketStallRepository.GetMarketStall(id) : _marketStallRepository.GetMarketStall(user);
            if (marketStall == null)
            {
                if (user.Role > UserRole.Helper)
                {
                    return Json(new Status { Success = false, ErrorMessage = "This Market Stall does not exist." });
                }
                return Json(new Status { Success = false, ErrorMessage = "You do not own a Market Stall." });
            }

            if (marketStall.Id != id)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not own this Market Stall." });
            }

            if (marketStall.Username == user.Username)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                List<Item> items = _itemsRepository.GetItems(user);
                if (items.Count + marketStall.Items.Count > settings.MaxBagSize)
                {
                    return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add all the items back from your Market Stall." });
                }
            }

            if (_marketStallRepository.DeleteMarketStall(marketStall))
            {
                if (marketStall.Username != user.Username)
                {
                    User marketStallOwner = _userRepository.GetUser(marketStall.Username);
                    if (marketStallOwner != null)
                    {
                        _adminHistoryRepository.AddAdminHistory(marketStallOwner, user, $"Deleted Market Stall \"{marketStall.Name}\".");
                        _notificationRepository.AddNotification(marketStallOwner, "Your Market Stall was removed for violating the Terms of Service.", NotificationLocation.MarketStalls, "/MarketStalls");
                    }
                }

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Market Stall could not be removed." });
        }

        [HttpGet]
        public IActionResult GetMarketStallsMainPartialView(string query, int page)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MarketStallsViewModel marketStallsViewModel = GenerateModel(user, 0, query, page, false);
            return PartialView("_MarketStallsSearchPartial", marketStallsViewModel);
        }

        [HttpGet]
        public IActionResult GetMarketStallsIdPartialView(int id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MarketStallsViewModel marketStallsViewModel = GenerateModel(user, id, string.Empty, 0, false);
            return PartialView("_MarketStallsIdPartial", marketStallsViewModel);
        }

        private MarketStallsViewModel GenerateModel(User user, int id, string query, int page, bool isCreate, bool openLedger = false, bool matchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            int userMarketStallId = _marketStallRepository.GetUserMarketStallId(user);
            MarketStallsViewModel marketStallsViewModel = new MarketStallsViewModel
            {
                Title = "Market Stalls",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.MarketStalls,
                UserMarketStallId = userMarketStallId,
                IsUserMarketStall = id > 0 && id == userMarketStallId,
                SearchQuery = query,
                OpenLedger = openLedger,
                MatchExactSearch = matchExactSearch,
                SearchCategory = category,
                SearchCondition = condition
            };

            if (isCreate)
            {
                marketStallsViewModel.MaxMarketStallItems = settings.MaxMarketStallSize;
                marketStallsViewModel.UserItems = _itemsRepository.GetItems(user, true);
                return marketStallsViewModel;
            }

            if (id > 0)
            {
                marketStallsViewModel.CurrentMarketStall = _marketStallRepository.GetMarketStall(id);
                if (marketStallsViewModel.IsUserMarketStall)
                {
                    marketStallsViewModel.MaxMarketStallItems = settings.MaxMarketStallSize;
                    marketStallsViewModel.UserItems = _itemsRepository.GetItems(user, true);
                    marketStallsViewModel.RepairCost = marketStallsViewModel.CurrentMarketStall.Items.Sum(x => _repairRepository.GetRepairPoints(x, settings.RepairCouponPercent));
                }
                if (marketStallsViewModel.IsUserMarketStall || user.Role >= UserRole.Helper)
                {
                    marketStallsViewModel.MarketStallHistories = _marketStallRepository.GetMarketStallHistory(marketStallsViewModel.CurrentMarketStall);
                }
            }
            if (marketStallsViewModel.CurrentMarketStall == null)
            {
                if (string.IsNullOrEmpty(query))
                {
                    marketStallsViewModel.NewMarketStalls = _marketStallRepository.GetNewMarketStalls();
                }
                else
                {
                    int resultsPerPage = 15;
                    List<MarketStallItem> marketStallItems = _marketStallRepository.GetMarketStallItems(query, matchExactSearch, category, condition);
                    marketStallsViewModel.TotalPages = (int)Math.Ceiling((double)marketStallItems.Count / resultsPerPage);
                    if (page < 0 || page >= marketStallsViewModel.TotalPages)
                    {
                        page = 0;
                    }
                    marketStallsViewModel.MarketStallItems = marketStallItems.Skip(resultsPerPage * page).Take(resultsPerPage).ToList();
                    marketStallsViewModel.CurrentPage = page;
                }
            }

            return marketStallsViewModel;
        }
    }
}
