﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ProfileController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private IGroupsRepository _groupsRepository;
        private IMarketStallRepository _marketStallRepository;
        private IBlockedRepository _blockedRepository;
        private ICompanionsRepository _companionsRepository;
        private IProfileRepository _profileRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IPartnershipRepository _partnershipRepository;
        private IBadgeRepository _badgeRepository;
        private IAdminHistoryRepository _adminHistoryRepository;
        private IMatchStateRepository _matchStateRepository;
        private IRelicsRepository _relicsRepository;
        private IClubRepository _clubRepository;
        private IAttentionHallRepository _attentionHallRepository;
        private ISettingsRepository _settingsRepository;
        private INotificationsRepository _notificationsRepository;
        private IEmailRepository _emailRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ProfileController(ILoginRepository loginRepository, IUserRepository userRepository, IMatchHistoryRepository matchHistoryRepository,
        IGroupsRepository groupsRepository, IMarketStallRepository marketStallRepository, IBlockedRepository blockedRepository, ICompanionsRepository companionsRepository,
        IProfileRepository profileRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository, IPartnershipRepository partnershipRepository, IBadgeRepository badgeRepository, 
        IAdminHistoryRepository adminHistoryRepository, IMatchStateRepository matchStateRepository, IRelicsRepository relicsRepository, IClubRepository clubRepository, IAttentionHallRepository attentionHallRepository, 
        ISettingsRepository settingsRepository, INotificationsRepository notificationsRepository, IEmailRepository emailRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _groupsRepository = groupsRepository;
            _marketStallRepository = marketStallRepository;
            _blockedRepository = blockedRepository;
            _companionsRepository = companionsRepository;
            _profileRepository = profileRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _partnershipRepository = partnershipRepository;
            _badgeRepository = badgeRepository;
            _adminHistoryRepository = adminHistoryRepository;
            _relicsRepository = relicsRepository;
            _matchStateRepository = matchStateRepository;
            _clubRepository = clubRepository;
            _attentionHallRepository = attentionHallRepository;
            _settingsRepository = settingsRepository;
            _notificationsRepository = notificationsRepository;
            _emailRepository = emailRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string id, AccomplishmentSort? sort)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null && string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index", "Login", new { ru = "Profile" });
            }

            User profile = string.IsNullOrEmpty(id) ? user : _userRepository.GetUser(id, false);
            if (profile == null)
            {
                Messages.AddSnack("That user does not exist.", true);

                return RedirectToAction("Index", "Home");
            }
            if (profile.InactiveType != InactiveAccountType.None)
            {
                return RedirectToAction("Inactive", "Profile");
            }
            if (profile.IsSuspended)
            {
                return RedirectToAction("Suspended", "Profile");
            }

            if (user != null && user.Username != profile.Username)
            {
                List<Blocked> blockedUsers = _blockedRepository.GetBlocked(profile, false);
                if (blockedUsers.Exists(x => x.BlockedUser == user.Username))
                {
                    Messages.AddSnack("You're not able to access that user's profile.", true);

                    return RedirectToAction("Index", "Home");
                }
            }
            else if (user != null && user.Username == profile.Username)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                if (settings.CurrentIntroStage != IntroStage.Completed)
                {
                    return RedirectToAction("Index", "Intro");
                }
                if (sort.HasValue)
                {
                    if (settings.AccomplishmentSort != sort)
                    {
                        settings.AccomplishmentSort = sort.Value;
                        _settingsRepository.SetSetting(user, nameof(settings.AccomplishmentSort), settings.AccomplishmentSort.ToString());
                    }
                }
            }

            ProfileViewModel profileViewModel = GenerateModel(user, profile, true);
            profileViewModel.UpdateViewData(ViewData);
            return View(profileViewModel);
        }

        public IActionResult Showroom(string id)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null && string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index", "Login", new { ru = "Profile" });
            }

            User profile = string.IsNullOrEmpty(id) ? user : _userRepository.GetUser(id, false);
            if (profile == null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (profile?.InactiveType != InactiveAccountType.None)
            {
                return RedirectToAction("Inactive", "Profile");
            }

            if (user != null && user.Username != profile.Username)
            {
                List<Blocked> blockedUsers = _blockedRepository.GetBlocked(profile, false);
                if (blockedUsers.Exists(x => x.BlockedUser == user.Username))
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            ProfileViewModel profileViewModel = GenerateModel(user, profile, false);
            if (!profileViewModel.HasShowroom)
            {
                return RedirectToAction("Index", "Profile", new { id });
            }
            profileViewModel.UpdateViewData(ViewData);
            return View(profileViewModel);
        }

        public IActionResult Partnership()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Profile/Partnership" });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (string.IsNullOrEmpty(settings.PartnershipUsername))
            {
                return RedirectToAction("Index", "Profile");
            }

            User partnershipUser = _userRepository.GetUser(settings.PartnershipUsername);
            if (partnershipUser == null)
            {
                settings.PartnershipUsername = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.PartnershipUsername), settings.PartnershipUsername);

                return RedirectToAction("Index", "Profile");
            }

            ProfilePartnershipViewModel profilePartnershipViewModel = new ProfilePartnershipViewModel
            {
                Title = "Partnership",
                User = user,
                Parent = LocationArea.Profile.ToString(),
                ParentUrl = $"User/{user.Username}",
                PartnershipUser = partnershipUser,
                PartnershipEarnings = _partnershipRepository.GetEarningsForPartnership(partnershipUser, user)
            };
            profilePartnershipViewModel.UpdateViewData(ViewData);
            return View(profilePartnershipViewModel);
        }

        public IActionResult Inactive()
        {
            User user = _loginRepository.AuthenticateUser();
            ProfileViewModel profileViewModel = new ProfileViewModel
            {
                User = user,
                Title = "Inactive Profile"
            };
            profileViewModel.UpdateViewData(ViewData);
            return View(profileViewModel);
        }

        public IActionResult Suspended()
        {
            User user = _loginRepository.AuthenticateUser();
            ProfileViewModel profileViewModel = new ProfileViewModel
            {
                User = user,
                Title = "Suspended Profile"
            };
            profileViewModel.UpdateViewData(ViewData);
            return View(profileViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Message(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must log in to message someone." });
            }
            if (user.Username == username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot message yourself." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            User profile = _userRepository.GetUser(username);
            if (profile == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            Settings settings = _settingsRepository.GetSettings(profile, user.Username == profile.Username);
            if (settings.DisableMessages)
            {
                return Json(new Status { Success = false, ErrorMessage = $"{profile.Username} has disabled messaging." });
            }

            return Json(new Status { Success = true, ReturnUrl = $"/Messages?username={username}" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChallengeUser(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must log in to challenge someone at the Stadium." });
            }
            if (user.Username == username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot challenge yourself to a Stadium match." });
            }

            User profile = _userRepository.GetUser(username);
            if (profile == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            Settings settings = _settingsRepository.GetSettings(profile, false);
            if (!settings.AllowUserChallenges)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not allow Stadium challenge requests." });
            }

            if (_matchStateRepository.IsInMatch(user))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge {profile.Username} at this time as you're currently in a Stadium match." });
            }

            if (_matchStateRepository.IsInMatch(profile, true, false))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge {profile.Username} at this time as they're currently in a Stadium match." });
            }

            if (_blockedRepository.HasBlockedUser(user, profile))
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot challenge a user who you've blocked." });
            }

            return Json(new Status { Success = true, ReturnUrl = $"/Stadium/ChallengeUser?username={username}" });
        }

        [HttpPost]
        public IActionResult CompanionInteract(string username, Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            User profileUser = user != null && user.Username == username ? user : _userRepository.GetUser(username);
            if (profileUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Companion companion = _companionsRepository.GetCompanions(profileUser, user?.Username == profileUser.Username).Find(x => x.Id == companionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Companion no longer exists." });
            }

            Settings profileSettings = _settingsRepository.GetSettings(profileUser, user?.Username == profileUser.Username);
            if (!profileSettings.ShowCompanionsOnProfile)
            {
                return Json(new Status { Success = false, ErrorMessage = "Companions have been disabled for this profile." });
            }

            string infoMessage = companion.Interact(user, out bool shouldUpdate);

            if (user != null)
            {
                Random rnd = new Random();
                if (companion.CanGiveGift(user))
                {
                    int randomPoints = rnd.Next(50, companion.GetHappiness() * 10 + 1) * Math.Min(50, (companion.Level + 1) / 5 + 1);
                    if (companion.Type == CompanionType.Dragon || companion.Type == CompanionType.Unicorn)
                    {
                        randomPoints *= 2;
                    }
                    else if (companion.Type == CompanionType.Panda)
                    {
                        randomPoints *= 5;
                    }
                    if (_pointsRepository.AddPoints(user, randomPoints))
                    {
                        companion.LastGiftGivenDate = DateTime.UtcNow;
                        if (rnd.Next(2) == 1)
                        {
                            companion.Level++;
                            Messages.AddSnack($"{companion.Name} successfully increased to Level {Helper.FormatNumber(companion.Level + 1)}!");

                            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                            if (companion.Level + 1 >= 10)
                            {
                                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelTen))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelTen);
                                }

                            }
                            if (companion.Level + 1 >= 25)
                            {
                                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelTwentyFive))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelTwentyFive);
                                }
                            }
                            if (companion.Level + 1 >= 50)
                            {
                                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionLevelFifty))
                                {
                                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionLevelFifty);
                                }
                            }
                        }
                        _companionsRepository.UpdateCompanion(companion);

                        if ((DateTime.UtcNow - companion.CreatedDate).TotalDays >= 7)
                        {
                            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HappyCompanion))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HappyCompanion);
                            }
                        }

                        return Json(new Status { Success = true, SuccessMessage = $"{companion.Name} found {Helper.GetFormattedPoints(randomPoints)} and successfully brought it to you!", Points = Helper.GetFormattedPoints(user.Points), ShouldRefresh = true });
                    }
                }
            }

            if (shouldUpdate)
            {
                _companionsRepository.UpdateCompanion(companion);
            }

            return Json(new Status { Success = true, SuccessMessage = shouldUpdate ? $"{companion.Name}'{(companion.Name[companion.Name.Length - 1] == 's' ? "" : "s")} happiness successfully increased to {companion.GetHappiness()}%!" : "", InfoMessage = infoMessage, ShouldRefresh = shouldUpdate });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveCompanion(string username, Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            User profileUser = user.Username == username ? user : _userRepository.GetUser(username);
            if (profileUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            Companion companion = _companionsRepository.GetCompanions(profileUser, user?.Username == profileUser.Username).Find(x => x.Id == companionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Companion no longer exists." });
            }

            if (user.Role < UserRole.Admin && companion.Username != user.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to remove this Companion." });
            }

            Settings profileSettings = _settingsRepository.GetSettings(profileUser, user.Username == profileUser.Username);
            if (!profileSettings.ShowCompanionsOnProfile)
            {
                return Json(new Status { Success = false, ErrorMessage = "Companions have been disabled for this profile." });
            }

            User companionUser = user.Username == companion.Username ? user : _userRepository.GetUser(companion.Username);

            if (_companionsRepository.DeleteCompanion(companionUser, companion))
            {
                if (user.Username != companionUser.Username)
                {
                    _adminHistoryRepository.AddAdminHistory(profileUser, user, $"Removed Companion {companion.Name}.");

                    return Json(new Status { Success = true, SuccessMessage = $"{companion.Name} was successfully removed!" });
                }

                _companionsRepository.AddAdoptionCompanion(companion);

                return Json(new Status { Success = true, SuccessMessage = $"{companion.Name} was successfully sent to Top Adoptions!", ButtonText = "Head to Top Adoptions!", ButtonUrl = "/TopAdoptions" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Companion could not be removed." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RequestPartnership(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot form a partnership with yourself." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (!string.IsNullOrEmpty(settings.PartnershipUsername))
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an active partnership." });
            }
            if (!settings.PartnershipEnabled)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked the ability to form partnerships yet." });
            }

            User profileUser = _userRepository.GetUser(username);
            if (profileUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            Settings partnerSettings = _settingsRepository.GetSettings(profileUser, false);
            if (!partnerSettings.AllowPartnershipRequests)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user has disabled partnership requests." });
            }
            if (!string.IsNullOrEmpty(partnerSettings.PartnershipUsername))
            {
                return Json(new Status { Success = false, ErrorMessage = "This user already has an active partnership." });
            }
            if (!partnerSettings.PartnershipEnabled)
            {
                return Json(new Status { Success = false, ErrorMessage = $"{profileUser.Username} hasn't unlocked the ability to form partnerships yet." });
            }

            List<PartnershipRequest> partnershipRequests = _partnershipRepository.GetPartnershipRequests(profileUser);
            if (partnershipRequests.Any(x => x.RequestUsername == user.Username))
            {
                return CancelPartnershipRequest(username);
            }

            if (_blockedRepository.HasBlockedUser(profileUser, user, false) || _blockedRepository.HasBlockedUser(user, profileUser))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot request to form a partnership with this user." });
            }

            if (_partnershipRepository.AddPartnershipRequest(user, profileUser))
            {
                _notificationsRepository.AddNotification(profileUser, $"@{user.Username} requested to form a partnership with you.", NotificationLocation.Partnership, "/Profile");
                return Json(new Status { Success = true, SuccessMessage = $"You successfully requested to form a partnership with {profileUser.Username}!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "You cannot request to form a partnership with this user." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CancelPartnershipRequest(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            User profileUser = _userRepository.GetUser(username);
            if (profileUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            PartnershipRequest partnershipRequest = _partnershipRepository.GetPartnershipRequests(profileUser).Find(x => x.RequestUsername == user.Username);
            if (partnershipRequest == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have any pending partnership requests with this user." });
            }

            if (_partnershipRepository.DeletePartnershipRequest(profileUser, user))
            {
                return Json(new Status { Success = true, SuccessMessage = $"The partnership request was successfully cancelled!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not cancel this partnership request." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AlterPartnership(string username, bool accepted)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (!string.IsNullOrEmpty(settings.PartnershipUsername))
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an active partnership." });
            }
            if (!settings.PartnershipEnabled)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't unlocked the ability to form partnerships yet." });
            }

            PartnershipRequest partnershipRequest = _partnershipRepository.GetPartnershipRequests(user).Find(x => x.RequestUsername == username);
            if (partnershipRequest == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"This partnership request no longer exists." });
            }

            User partnershipUser = _userRepository.GetUser(partnershipRequest.RequestUsername);
            if (partnershipUser == null)
            {
                _partnershipRepository.DeletePartnershipRequest(user, new User { Username = username });
                return Json(new Status { Success = false, ErrorMessage = $"This user no longer exists.", ShouldRefresh = true });
            }

            if (_blockedRepository.HasBlockedUser(partnershipUser, user, false) || _blockedRepository.HasBlockedUser(user, partnershipUser))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot form a partnership with this user." });
            }

            if (_partnershipRepository.DeletePartnershipRequest(user, partnershipUser))
            {
                if (accepted)
                {
                    Settings partnerSettings = _settingsRepository.GetSettings(partnershipUser, false);
                    if (!string.IsNullOrEmpty(partnerSettings.PartnershipUsername))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "This user already has an active partnership.", ShouldRefresh = true });
                    }

                    settings.PartnershipUsername = partnershipUser.Username;
                    _settingsRepository.SetSetting(user, nameof(settings.PartnershipUsername), settings.PartnershipUsername);
                    partnerSettings.PartnershipUsername = user.Username;
                    _settingsRepository.SetSetting(partnershipUser, nameof(partnerSettings.PartnershipUsername), partnerSettings.PartnershipUsername, false);

                    string subject = "Your partnership request was accepted!";
                    string title = "New Partnership";
                    string body = $"Congratulations! {user.Username} accepted your request and formed a partnership with you! Click the button below to check out your new partnership.";
                    string url = "/Profile";

                    _emailRepository.SendEmail(partnershipUser, subject, title, body, url);
                    _notificationsRepository.AddNotification(partnershipUser, $"@{user.Username} accepted your request and formed a partnership with you!", NotificationLocation.Partnership, url);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.PartnershipFormed))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.PartnershipFormed);
                    }

                    List<Accomplishment> partnershipAccomplishments = _accomplishmentsRepository.GetAccomplishments(partnershipUser, false);
                    if (!partnershipAccomplishments.Exists(x => x.Id == AccomplishmentId.PartnershipFormed))
                    {
                        _accomplishmentsRepository.AddAccomplishment(partnershipUser, AccomplishmentId.PartnershipFormed, false);
                    }

                    return Json(new Status { Success = true, SuccessMessage = $"You successfully formed a partnership with {partnershipUser.Username}!" });
                }

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "You cannot form a partnership with this user." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EndPartnership()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (string.IsNullOrEmpty(settings.PartnershipUsername))
            {
                return Json(new Status { Success = false, ErrorMessage = "You're not currently in a partnership." });
            }

            User partnershipUser = _userRepository.GetUser(settings.PartnershipUsername);
            if (partnershipUser == null)
            {
                settings.PartnershipUsername = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.PartnershipUsername), settings.PartnershipUsername);

                return Json(new Status { Success = true, SuccessMessage = "You successfully ended your partnership for free!" });
            }

            if (user.Points < 5000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You do not have enough {Resources.Currency} on hand to end this partnership." });
            }

            if (_pointsRepository.SubtractPoints(user, 5000))
            {
                settings.PartnershipUsername = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.PartnershipUsername), settings.PartnershipUsername);

                Settings partnershipSettings = _settingsRepository.GetSettings(partnershipUser, false);
                partnershipSettings.PartnershipUsername = string.Empty;
                _settingsRepository.SetSetting(partnershipUser, nameof(partnershipSettings.PartnershipUsername), partnershipSettings.PartnershipUsername, false);

                _notificationsRepository.AddNotification(partnershipUser, $"@{user.Username} ended your partnership.", NotificationLocation.Partnership, "/Profile");

                return Json(new Status { Success = true, SuccessMessage = $"You successfully ended your partnership!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not end this partnership." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddShowroomItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            Accomplishment showroomAccomplishment = accomplishments.Find(x => x.Id == AccomplishmentId.ShowroomAccess);
            if (showroomAccomplishment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a Showroom." });
            }

            Item item = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You no longer have this item." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> showroomItems = _profileRepository.GetShowroomItems(user);
            if (showroomItems.Count >= settings.MaxShowroomSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot add any more items to your Showroom." });
            }

            if (_profileRepository.AddShowroomItem(user, item))
            {
                _itemsRepository.RemoveItem(user, item);

                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FiveShowroomArtCards))
                {
                    if (showroomItems.FindAll(x => x.Category == ItemCategory.Art).Count >= 4 && item.Category == ItemCategory.Art)
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FiveShowroomArtCards);
                    }
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to your Showroom!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item couldn't be added to your Showroom." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveShowroomItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Accomplishment showroomAccomplishment = _accomplishmentsRepository.GetAccomplishments(user).Find(x => x.Id == AccomplishmentId.ShowroomAccess);
            if (showroomAccomplishment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a Showroom." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            List<Item> showroomItems = _profileRepository.GetShowroomItems(user);
            Item showroomItem = showroomItems.Find(x => x.SelectionId == selectionId);
            if (showroomItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer in your Showroom." });
            }

            if (_profileRepository.DeleteShowroomItem(showroomItem))
            {
                DateTime showroomDate = showroomItem.RepairedDate > showroomItem.AddedDate ? showroomItem.RepairedDate : showroomItem.AddedDate;
                int secondsSinceAdded = (int)(DateTime.UtcNow - showroomDate).TotalSeconds;
                showroomItem.RepairedDate = showroomItem.RepairedDate.AddSeconds(secondsSinceAdded);
                _itemsRepository.AddItem(user, showroomItem);

                foreach (Item updateItem in showroomItems.FindAll(x => x.Position > showroomItem.Position))
                {
                    updateItem.Position--;
                    _profileRepository.UpdateShowroomItem(updateItem);
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Plop, settings);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{showroomItem.Name}\" was successfully added back to your bag!", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item couldn't be added to your bag." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveShowroomInfo(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.Role < UserRole.Helper)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have permission to do this." });
            }

            User profileUser = _userRepository.GetUser(username);
            if (profileUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user does not exist." });
            }

            if (user.Username == profileUser.Username)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot do this to your own Showroom." });
            }

            if (profileUser.Role >= user.Role)
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot reset this user's Showroom info." });
            }

            Settings profileSettings = _settingsRepository.GetSettings(profileUser, false);
            profileSettings.ShowroomTitle = string.Empty;
            profileSettings.ShowroomDescription = string.Empty;
            _settingsRepository.SetSetting(profileUser, nameof(profileSettings.ShowroomTitle), profileSettings.ShowroomTitle, false);
            _settingsRepository.SetSetting(profileUser, nameof(profileSettings.ShowroomDescription), profileSettings.ShowroomDescription, false);

            _adminHistoryRepository.AddAdminHistory(profileUser, user, "Reset Showroom information.");
            _notificationsRepository.AddNotification(profileUser, "Your Showroom information was reset for violating the Terms of Service.", NotificationLocation.General, "/Profile/Showroom");

            return Json(new Status { Success = true, SuccessMessage = "The Showroom info was successfully reset!" });
        }

        [HttpPost]
        public IActionResult UpdateCompanionPosition(int newPosition, Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Companion> companions = _companionsRepository.GetCompanions(user);
            Companion chosenCompanion = companions.Find(x => x.Id == companionId);
            if (chosenCompanion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have this Companion." });
            }

            if (newPosition < 0 || newPosition >= companions.Count)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't a valid position." });
            }

            if (newPosition == chosenCompanion.Position)
            {
                return Json(new Status { Success = false });
            }

            if (newPosition < chosenCompanion.Position)
            {
                foreach (Companion companion in companions.FindAll(x => x.Position >= newPosition && x.Position < chosenCompanion.Position))
                {
                    companion.Position++;
                    _companionsRepository.UpdateCompanion(companion);
                }
            }
            else
            {
                foreach (Companion companion in companions.FindAll(x => x.Position <= newPosition && x.Position > chosenCompanion.Position))
                {
                    companion.Position--;
                    _companionsRepository.UpdateCompanion(companion);
                }
            }

            chosenCompanion.Position = newPosition;
            _companionsRepository.UpdateCompanion(chosenCompanion);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult UpdatePosition(Guid selectionId, int newPosition)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Accomplishment showroomAccomplishment = _accomplishmentsRepository.GetAccomplishments(user).Find(x => x.Id == AccomplishmentId.ShowroomAccess);
            if (showroomAccomplishment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have a Showroom." });
            }

            List<Item> showroomItems = _profileRepository.GetShowroomItems(user);
            Item showroomItem = showroomItems.Find(x => x.SelectionId == selectionId);
            if (showroomItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer in your Showroom." });
            }

            if (newPosition < 0 || newPosition >= showroomItems.Count)
            {
                return Json(new Status { Success = false, ErrorMessage = "This isn't a valid position." });
            }

            if (newPosition == showroomItem.Position)
            {
                return Json(new Status { Success = false });
            }

            if (newPosition < showroomItem.Position)
            {
                foreach (Item updateItem in showroomItems.FindAll(x => x.Position >= newPosition && x.Position < showroomItem.Position))
                {
                    updateItem.Position++;
                    _profileRepository.UpdateShowroomItem(updateItem);
                }
            }
            else
            {
                foreach (Item updateItem in showroomItems.FindAll(x => x.Position <= newPosition && x.Position > showroomItem.Position))
                {
                    updateItem.Position--;
                    _profileRepository.UpdateShowroomItem(updateItem);
                }
            }

            showroomItem.Position = newPosition;
            _profileRepository.UpdateShowroomItem(showroomItem);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        public IActionResult LikeShowroom(string showroomUsername)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            User showroomUser = user;
            if (user.Username != showroomUsername)
            {
                showroomUser = _userRepository.GetUser(showroomUsername);
            }
            if (showroomUser == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            Accomplishment showroomAccomplishment = _accomplishmentsRepository.GetAccomplishments(showroomUser, false).Find(x => x.Id == AccomplishmentId.ShowroomAccess);
            if (showroomAccomplishment == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Showroom doesn't exist." });
            }

            if (_profileRepository.SetShowroomLike(user, showroomUser))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    List<ShowroomLike> likes = _profileRepository.GetShowroomLikes(showroomUser);
                    if (likes.Count == 10)
                    {
                        List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(showroomUser, user.Username == showroomUser.Username);
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.PopularShowroom))
                        {
                            _accomplishmentsRepository.AddAccomplishment(showroomUser, AccomplishmentId.PopularShowroom, user.Username == showroomUser.Username, domain);
                        }
                    }
                });
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "This Showroom couldn't be liked." });
        }

        [HttpGet]
        public IActionResult GetProfilePartialView(string username)
        {
            User user = _loginRepository.AuthenticateUser();
            User profile = string.IsNullOrEmpty(username) ? user : _userRepository.GetUser(username);
            if (profile == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
            }

            if (user != null && user.Username != profile.Username)
            {
                List<Blocked> blockedUsers = _blockedRepository.GetBlocked(profile, false);
                if (blockedUsers.Exists(x => x.BlockedUser == user.Username))
                {
                    return Json(new Status { Success = false, ErrorMessage = "This user no longer exists." });
                }
            }

            ProfileViewModel profileViewModel = GenerateModel(user, profile, true);
            return PartialView("_ProfileMainPartial", profileViewModel);
        }

        [HttpGet]
        public IActionResult GetProfileShowroomPartialView(bool isCollapsed, string showroomUsername)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            User profile = user;
            if (!string.IsNullOrEmpty(showroomUsername) && showroomUsername != user.Username)
            {
                profile = _userRepository.GetUser(showroomUsername);
            }

            ProfileViewModel profileViewModel = GenerateModel(user, profile, false, isCollapsed);
            if (!profileViewModel.HasShowroom)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Showroom doesn't exist." });
            }
            return PartialView("_ProfileShowroomPartial", profileViewModel);
        }

        private ProfileViewModel GenerateModel(User user, User profile, bool isProfile, bool isCollapsed = true)
        {
            bool isUserProfile = user != null && user.Username == profile.Username;
            List<Accomplishment> allAccomplishments = _accomplishmentsRepository.GetAccomplishments(profile, isUserProfile);
            List<Accomplishment> accomplishments = allAccomplishments.FindAll(x => x.Category > AccomplishmentCategory.Orange);
            Settings profileSettings = _settingsRepository.GetSettings(profile, isUserProfile);
            ProfileViewModel profileViewModel = new ProfileViewModel
            {
                Title = isProfile ? profile.GetFriendlyName() == profile.Username ? profile.Username : $"{profile.GetFriendlyName()} ({profile.Username})" : "Showroom",
                User = user,
                ShowTitle = !isProfile && isUserProfile,
                Profile = profile,
                ProfileSettings = profileSettings,
                Accomplishments = SortAccomplishments(accomplishments, profileSettings.AccomplishmentSort),
                AllAccomplishments = _accomplishmentsRepository.GetAllAccomplishments().FindAll(x => x.Category > AccomplishmentCategory.Orange).ToList(),
                HasShowroom = accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess),
                IsUserProfile = isUserProfile,
                IsCollapsed = isCollapsed,
                Sort = profileSettings.AccomplishmentSort
            };

            if (isUserProfile && isProfile && accomplishments.Count > 0 && accomplishments.Count < 10 && !profileSettings.HasSeenProfileAccomplishmentTutorial)
            {
                profileSettings.HasSeenProfileAccomplishmentTutorial = true;
                profileViewModel.ShowAccomplishmentsTutorial = _settingsRepository.SetSetting(user, nameof(profileSettings.HasSeenProfileAccomplishmentTutorial), profileSettings.HasSeenProfileAccomplishmentTutorial.ToString());
            }

            if (isProfile)
            {
                Settings userSettings = isUserProfile ? profileSettings : user != null ? _settingsRepository.GetSettings(user) : null;
                profileViewModel.MatchHistories = _matchHistoryRepository.GetMatchHistory(profile, true, isUserProfile);
                profileViewModel.MatchRanking = _matchHistoryRepository.GetMatchRanking(profile);
                profileViewModel.IsElite = _matchHistoryRepository.IsElite(profile, profileViewModel.MatchRanking);
                profileViewModel.IsBlocked = user != null && _blockedRepository.HasBlockedUser(user, profile);
                profileViewModel.IsJoinDay = profile.CreatedDate.Year != DateTime.UtcNow.Year && profile.CreatedDate.DayOfYear == DateTime.UtcNow.DayOfYear;
                profileViewModel.MatchTags = _matchHistoryRepository.GetMatchTags(profile, isUserProfile);
                profileViewModel.Relics = _relicsRepository.GetAllRelics(profileSettings.MatchLevel).FindAll(x => x.Earned).ToList();
                profileViewModel.ActiveBadge = _badgeRepository.GetActiveBadge(profileSettings.ActiveBadge);
                profileViewModel.AttentionHallPoints = _attentionHallRepository.GetAttentionHallPoints(profile);

                List<Badge> allBadges = _badgeRepository.GetAvailableBadges(profile, profileViewModel.ProfileSettings, allAccomplishments, _clubRepository.GetCurrentMembership(profile, isUserProfile) != null);
                allBadges.RemoveAll(x => x.Type == BadgeType.None);
                if (allBadges.Any(x => x == profileViewModel.ActiveBadge))
                {
                    profileViewModel.AllBadges = allBadges;
                }

                if (profileSettings.ShowGroupOnProfile)
                {
                    profileViewModel.Group = _groupsRepository.GetGroups(profile, false, false).Find(x => x.IsPrimary);
                }
                if (profileSettings.ShowMarketStallOnProfile)
                {
                    profileViewModel.MarketStall = _marketStallRepository.GetMarketStall(profile);
                }
                if (profileSettings.ShowCompanionsOnProfile)
                {
                    profileViewModel.Companions = _companionsRepository.GetCompanions(profile, isUserProfile);
                    if (profileViewModel.Companions.Count > 1 && profileViewModel.Companions.GroupBy(x => x.Position).Any(x => x.Count() > 1))
                    {
                        for (int i = 0; i < profileViewModel.Companions.Count; i++)
                        {
                            profileViewModel.Companions[i].Position = i;
                            _companionsRepository.UpdateCompanion(profileViewModel.Companions[i]);
                        }
                    }
                }
                if (user != null && string.IsNullOrEmpty(profileSettings.PartnershipUsername) && string.IsNullOrEmpty(userSettings.PartnershipUsername) && profileSettings.AllowPartnershipRequests && profileSettings.PartnershipEnabled && userSettings.PartnershipEnabled)
                {
                    profileViewModel.PartnershipRequests = _partnershipRepository.GetPartnershipRequests(profile);
                    profileViewModel.CanRequestPartnership = !isUserProfile && !profileViewModel.IsBlocked;
                    profileViewModel.PartnershipRequestPending = profileViewModel.PartnershipRequests.Any(x => x.RequestUsername == user.Username);
                }
            }
            else
            {
                profileViewModel.Parent = LocationArea.Profile.ToString();
                profileViewModel.ParentUrl = $"User/{profileViewModel.Profile.Username}";
                profileViewModel.ShowroomItems = _profileRepository.GetShowroomItems(profile, isUserProfile);
                if (profileViewModel.ShowroomItems.Count > 1 && profileViewModel.ShowroomItems.GroupBy(x => x.Position).Any(x => x.Count() > 1))
                {
                    for (int i = 0; i < profileViewModel.ShowroomItems.Count; i++)
                    {
                        profileViewModel.ShowroomItems[i].Position = i;
                        _profileRepository.UpdateShowroomItem(profileViewModel.ShowroomItems[i]);
                    }
                }
                profileViewModel.MaxShowroomSize = profileSettings.MaxShowroomSize;
                profileViewModel.ShowroomLikes = _profileRepository.GetShowroomLikes(profile);
                profileViewModel.HasLikedShowroom = user != null && profileViewModel.ShowroomLikes.Any(x => x.Username == user.Username);
                profileViewModel.ShowroomColor = string.IsNullOrEmpty(profileSettings.ShowroomColor) ? string.IsNullOrEmpty(profile.Color) ? "#337ab7" : profile.Color : profileSettings.ShowroomColor;
                if (isUserProfile)
                {
                    profileViewModel.Items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
                }
            }

            return profileViewModel;
        }

        private List<Accomplishment> SortAccomplishments(List<Accomplishment> accomplishments, AccomplishmentSort sort)
        {
            if (sort == AccomplishmentSort.Name)
            {
                return accomplishments.OrderBy(x => x.Name).ThenBy(x => x.Category).ToList();
            }
            if (sort == AccomplishmentSort.Newest)
            {
                return accomplishments.OrderByDescending(x => x.ReceivedDate).ThenBy(x => x.Category).ToList();
            }
            if (sort == AccomplishmentSort.Hardest)
            {
                return accomplishments.OrderByDescending(x => x.Category).ThenBy(x => x.Name).ToList();
            }
            if (sort == AccomplishmentSort.Rarest)
            {
                return accomplishments.OrderBy(x => x.PercentOwned).ThenBy(x => x.Name).ToList();
            }

            return accomplishments;
        }
    }
}