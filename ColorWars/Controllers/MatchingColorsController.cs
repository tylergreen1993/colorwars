﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class MatchingColorsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public MatchingColorsController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "MatchingColors" });
            }

            MatchingColorsViewModel matchingColorsViewModel = GenerateModel(user);
            matchingColorsViewModel.UpdateViewData(ViewData);
            return View(matchingColorsViewModel);
        }

        [HttpPost]
        public IActionResult Reveal(int position)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastMatchingColorsPlayDate).TotalHours;
            if (hoursSinceLastGame < 8)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played Matching Colors recently. Try again in {8 - hoursSinceLastGame} hour{(8 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            List<MatchingCard> matchingCards = GetMatchingCards(user, settings);

            if (position > matchingCards.Count || position < 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Color Card can't be revealed." });
            }

            MatchingCard matchingCard = matchingCards.Find(x => x.Position == position);
            if (matchingCard == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Color Card doesn't exist." });
            }

            if (!matchingCard.IsHidden)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already revealed this Color Card." });
            }

            Random rnd = new Random();
            int randomNumber = rnd.Next(100);
            CardColor cardColor;
            if (randomNumber < 30)
            {
                cardColor = CardColor.Red;
            }
            else if (randomNumber < 55)
            {
                cardColor = CardColor.Orange;
            }
            else if (randomNumber < 75)
            {
                cardColor = CardColor.Yellow;
            }
            else if (randomNumber < 85)
            {
                cardColor = CardColor.Green;
            }
            else if (randomNumber < 95)
            {
                cardColor = CardColor.Blue;
            }
            else
            {
                cardColor = CardColor.Purple;
            }

            matchingCards[position].Color = cardColor;
            settings.MatchingColorsPositions = string.Join("|", matchingCards.Select(x => (int)x.Color));
            _settingsRepository.SetSetting(user, nameof(settings.MatchingColorsPositions), settings.MatchingColorsPositions);

            CardColor matchingColor = matchingCards.FindAll(x => !x.IsHidden).GroupBy(x => x.Color).FirstOrDefault(x => x.Count() >= 3)?.FirstOrDefault()?.Color ?? CardColor.None;
            bool playedAllCards = matchingCards.FindAll(x => !x.IsHidden).Count == matchingCards.Count;

            if (playedAllCards || matchingColor > CardColor.None)
            {
                settings.LastMatchingColorsPlayDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastMatchingColorsPlayDate), settings.LastMatchingColorsPlayDate.ToString());
            }

            if (matchingColor == CardColor.None)
            {
                if (playedAllCards)
                {
                    return Json(new Status { Success = true, InfoMessage = $"You flipped all the Color Cards but found no 3 matching colors! Better luck next time." });
                }

                return Json(new Status { Success = true });
            }

            int points = 0;
            switch (matchingColor)
            {
                case CardColor.Red:
                    points = 500;
                    break;
                case CardColor.Orange:
                    points = 1000;
                    break;
                case CardColor.Yellow:
                    points = 2500;
                    break;
                case CardColor.Green:
                    points = 5000;
                    break;
                case CardColor.Blue:
                    points = 7500;
                    break;
                case CardColor.Purple:
                    points = 10000;
                    break;
            }

            if (_pointsRepository.AddPoints(user, points))
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MatchingColorsMatch))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MatchingColorsMatch);
                }
                if (matchingColor >= CardColor.Blue)
                {
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MatchingColorsBigMatch))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MatchingColorsBigMatch);
                    }
                }

                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = $"You successfully matched 3 {matchingColor} Cards and earned {Helper.GetFormattedPoints(points)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "You couldn't be rewarded for matching a color." });
        }

        [HttpGet]
        public IActionResult GetMatchingColorsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            MatchingColorsViewModel matchingColorsViewModel = GenerateModel(user);
            return PartialView("_MatchingColorsMainPartial", matchingColorsViewModel);
        }

        private MatchingColorsViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            List<MatchingCard> matchingCards = GetMatchingCards(user, settings);

            MatchingColorsViewModel matchingColorsViewModel = new MatchingColorsViewModel
            {
                Title = "Matching Colors",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                MatchingCards = matchingCards,
                CanPlayMatchingCards = (DateTime.UtcNow - settings.LastMatchingColorsPlayDate).TotalHours >= 8,
                MatchingColor = matchingCards.FindAll(x => !x.IsHidden).GroupBy(x => x.Color).FirstOrDefault(x => x.Count() >= 3)?.FirstOrDefault()?.Color ?? CardColor.None
            };

            return matchingColorsViewModel;
        }

        private List<MatchingCard> GetMatchingCards(User user, Settings settings)
        {
            List<MatchingCard> matchingCards = new List<MatchingCard>();
            int totalCards = 8;
            if ((DateTime.UtcNow - settings.LastMatchingColorsPlayDate).TotalHours >= 8 && IsPlayCompleted(settings.MatchingColorsPositions))
            {
                for (int i = 0; i < totalCards; i++)
                {
                    matchingCards.Add(new MatchingCard
                    {
                        Position = i,
                        Color = CardColor.None
                    });
                }

                if (!string.IsNullOrEmpty(settings.MatchingColorsPositions))
                {
                    settings.MatchingColorsPositions = string.Empty;
                    _settingsRepository.SetSetting(user, nameof(settings.MatchingColorsPositions), settings.MatchingColorsPositions);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(settings.MatchingColorsPositions))
                {
                    for (int i = 0; i < totalCards; i++)
                    {
                        matchingCards.Add(new MatchingCard
                        {
                            Position = i,
                            Color = CardColor.None
                        });
                    }
                }
                else
                {
                    List<CardColor> matchingColors = settings.MatchingColorsPositions.Split("|").Select(x => (CardColor)int.Parse(x)).ToList();
                    for (int i = 0; i < matchingColors.Count; i++)
                    {
                        matchingCards.Add(new MatchingCard
                        {
                            Position = i,
                            Color = matchingColors[i]
                        });
                    }
                }
            }

            return matchingCards;
        }

        private bool IsPlayCompleted(string matchingColorsPositions)
        {
            if (string.IsNullOrEmpty(matchingColorsPositions))
            {
                return true;
            }

            List<CardColor> matchingColors = matchingColorsPositions.Split("|").Select(x => (CardColor)int.Parse(x)).ToList();
            if (matchingColors.All(x => x != CardColor.None))
            {
                return true;
            }

            if (matchingColors.FindAll(x => x != CardColor.None).GroupBy(x => x).Any(x => x.Count() >= 3))
            {
                return true;
            }

            return false;
        }
    }
}
