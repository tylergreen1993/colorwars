﻿using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class EffectsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IEffectsRepository _effectsRepository;
        private IGroupsRepository _groupsRepository;

        public EffectsController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IEffectsRepository effectsRepository, 
        IGroupsRepository groupsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _effectsRepository = effectsRepository;
            _groupsRepository = groupsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Effects" });
            }

            EffectsViewModel effectsViewModel = GenerateModel(user);
            effectsViewModel.UpdateViewData(ViewData);
            return View(effectsViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveAll()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            int cost = 100;
            if (user.Points < cost)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to remove all your effects." });
            }

            if (_pointsRepository.SubtractPoints(user, cost))
            {
                _effectsRepository.RemoveAllActiveEffects(user);
            }

            return Json(new Status { Success = true, SuccessMessage = "You successfully removed all your active effects!", Points = Helper.GetFormattedPoints(user.Points) });
        }

        [HttpGet]
        public IActionResult GetEffectsPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            EffectsViewModel effectsViewModel = GenerateModel(user);
            return PartialView("_EffectsMainPartial", effectsViewModel);
        }

        private EffectsViewModel GenerateModel(User user)
        {
            EffectsViewModel effectsViewModel = new EffectsViewModel
            {
                Title = "Active Effects",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.ActiveEffects,
                ActiveEffects = _effectsRepository.GetActiveEffects(user),
                ActiveGroupPower = _groupsRepository.GetGroupPower(user)
            };
            return effectsViewModel;
        }
    }
}
