﻿using Microsoft.AspNetCore.Mvc;
using ColorWars.Models;
using ColorWars.Repositories;
using System.Linq;
using System;
using ColorWars.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ColorWars.Controllers
{
    public class IntroController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IUserRepository _userRepository;
        private IRegisterRepository _registerRepository;
        private IAdminSettingsRepository _adminSettingsRepository;
        private IGroupsRepository _groupsRepository;
        private INotificationsRepository _notificationsRepository;
        private IMatchStateRepository _matchStateRepository;
        private IPromoCodesRepository _promoCodesRepository;
        private IEmailRepository _emailRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IMessagesRepository _messagesRepository;
        private IReferralsRepository _referralsRepository;
        private ISoundsRepository _soundsRepository;

        public IntroController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IUserRepository userRepository, IRegisterRepository registerRepository, IAdminSettingsRepository adminSettingsRepository,
        IGroupsRepository groupsRepository, INotificationsRepository notificationsRepository, IMatchStateRepository matchStateRepository,
        IPromoCodesRepository promoCodesRepository, IEmailRepository emailRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, IMessagesRepository messagesRepository, IReferralsRepository referralsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _userRepository = userRepository;
            _registerRepository = registerRepository;
            _adminSettingsRepository = adminSettingsRepository;
            _groupsRepository = groupsRepository;
            _notificationsRepository = notificationsRepository;
            _matchStateRepository = matchStateRepository;
            _promoCodesRepository = promoCodesRepository;
            _emailRepository = emailRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _messagesRepository = messagesRepository;
            _referralsRepository = referralsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();

            Settings settings = null;
            if (user != null)
            {
                settings = _settingsRepository.GetSettings(user);
                if (settings.CurrentIntroStage == IntroStage.Third)
                {
                    return RedirectToAction("Index", "Stadium");
                }
                if (settings.CurrentIntroStage == IntroStage.Completed)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            IntroViewModel introViewModel = GenerateModel(user, settings);
            introViewModel.UpdateViewData(ViewData);
            return View(introViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Start()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already signed up." });
            }

            GlobalHttpContext.Current.Session.Remove("StarterCards");

            return Json(new Status { Success = true, ReturnUrl = "/Intro" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChooseStarterCards(int packId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                Settings settings = _settingsRepository.GetSettings(user);
                settings.CurrentIntroStage = IntroStage.Third;
                _settingsRepository.SetSetting(user, nameof(settings.CurrentIntroStage), settings.CurrentIntroStage.ToString());

                return Json(new Status { Success = true, ReturnUrl = "/" });
            }

            StarterItems starterItems = GetStarterItems().Find(x => x.Index == packId);
            if (starterItems == null)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You need to choose a valid Starter Pack." });
            }

            foreach (Item item in starterItems.Items)
            {
                if (item.Category == ItemCategory.Deck)
                {
                    item.DeckType = DeckType.Primary;
                }
            }

            GlobalHttpContext.Current.Session.SetObject("StarterCards", starterItems.Items);

            return Json(new Status { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(string username, string password, string emailAddress, string allowSoundEffects, string betaCode)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                return Json(new Status { Success = true });
            }

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "You need to fill in all the fields to sign up." });
            }

            if (_userRepository.IsUsernameInvalid(username))
            {
                return Json(new Status { Success = false, ErrorMessage = "That username is not allowed." });
            }

            if (_userRepository.IsEmailAddressInvalid(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your email address is not allowed." });
            }

            if (password.Length < 5)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your password is too short." });
            }

            AdminSettings adminSettings = _adminSettingsRepository.GetSettings();
            if (adminSettings.IsBetaEnabled)
            {
                if (string.IsNullOrEmpty(betaCode) || !betaCode.Equals(adminSettings.BetaCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    return Json(new Status { Success = false, ErrorMessage = $"{Resources.SiteName} is currently in beta and requires a special code to sign up. You can request one on the Contact page." });
                }
            }

            if (!_registerRepository.IsEmailAvailable(emailAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "A user already exists with that email address." });
            }

            string ipAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            List<User> usersWithIPAddress = _userRepository.GetUsersByIPAddress(ipAddress).FindAll(x => (DateTime.UtcNow - x.CreatedDate).TotalHours < 3);
            if (usersWithIPAddress.Count >= 3)
            {
                return Json(new Status { Success = false, ErrorMessage = "Too many users on your device have signed up for an account recently. Try signing up later." });
            }

            if (_userRepository.GetBannedIPAddresses().Any(x => x.IPAddress == ipAddress))
            {
                return Json(new Status { Success = false, ErrorMessage = "You cannot sign up for an account. If you believe this is a mistake, you can let us know on the Contact page." });
            }

            if (!_registerRepository.IsUsernameAvailable(username))
            {
                return Json(new Status { Success = false, ErrorMessage = "This username already exists." });
            }

            List<Item> starterCards = GlobalHttpContext.Current.Session.GetObject<List<Item>>("StarterCards");
            if (starterCards == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your starter cards are no longer available." });
            }

            string referral = Request.Cookies["Referral"]?.Split("|").FirstOrDefault() ?? string.Empty;
            User referralUser = CheckReferral(referral);

            user = _registerRepository.CreateNewUser(username, password, emailAddress, true, referralUser?.Username ?? string.Empty);
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your sign up didn't work properly. Please try again." });
            }

            foreach (Item item in starterCards)
            {
                _itemsRepository.AddItem(user, item);
            }

            bool enableSoundEffects = allowSoundEffects == "on";

            Settings settings = _settingsRepository.GetSettings(user);

            if (enableSoundEffects)
            {
                settings.EnableSoundEffects = true;
                _settingsRepository.SetSetting(user, nameof(settings.EnableSoundEffects), settings.EnableSoundEffects.ToString());
            }

            settings.CurrentIntroStage = IntroStage.Third;
            _settingsRepository.SetSetting(user, nameof(settings.CurrentIntroStage), settings.CurrentIntroStage.ToString());

            if (referralUser != null)
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(referralUser, false);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ReferredUser))
                    {
                        _accomplishmentsRepository.AddAccomplishment(referralUser, AccomplishmentId.ReferredUser, false, domain);
                    }
                    List<Referral> referredUsers = _referralsRepository.GetReferrals(referralUser);
                    if (referredUsers.Count >= 5)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.Referred5Users))
                        {
                            _accomplishmentsRepository.AddAccomplishment(referralUser, AccomplishmentId.Referred5Users, false, domain);
                        }
                    }
                    if (referredUsers.Count >= 10)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.Referred10Users))
                        {
                            _accomplishmentsRepository.AddAccomplishment(referralUser, AccomplishmentId.Referred10Users, false, domain);
                        }
                    }
                    if (referredUsers.Count >= 20)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.Referred20Users))
                        {
                            _accomplishmentsRepository.AddAccomplishment(referralUser, AccomplishmentId.Referred20Users, false, domain);
                        }
                    }

                    GroupPower groupPower = _groupsRepository.GetGroupPower(referralUser, false);
                    if (groupPower == GroupPower.AdditionalReferralsBonus)
                    {
                        if (_pointsRepository.AddPoints(referralUser, 500))
                        {
                            Settings settings = _settingsRepository.GetSettings(referralUser, false);
                            settings.ExtraReferralPointsEarned += 500;
                            _settingsRepository.SetSetting(referralUser, nameof(settings.ExtraReferralPointsEarned), settings.ExtraReferralPointsEarned.ToString(), false);

                            _notificationsRepository.AddNotification(referralUser, $"@{user.Username} signed up from your referral and earned you {Helper.GetFormattedPoints(500)}!", NotificationLocation.Referral, $"/Referrals#{user.Username}", domain: domain);
                        }
                    }
                    else
                    {
                        _notificationsRepository.AddNotification(referralUser, $"@{user.Username} signed up from your referral!", NotificationLocation.Referral, "/Referrals", domain: domain);
                    }
                });
            }

            Task _ = SendWelcomeEmail(user, Helper.GetDomain());

            return Json(new Status { Success = true, ReturnUrl = "/Stadium" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCurrency()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (user.Points >= 5000)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've already taken the {Resources.Currency}." });
            }

            if (_pointsRepository.AddPoints(user, 5000))
            {
                Item cardPack = _itemsRepository.GetAllItems(ItemCategory.Pack).First();
                _itemsRepository.AddItem(user, cardPack);

                Settings settings = _settingsRepository.GetSettings(user);
                string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                return Json(new Status { Success = true, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = $"The {Resources.Currency} could not be added. Please try again." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Complete()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Fourth)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already completed this stage." });
            }

            settings.CurrentIntroStage = IntroStage.Completed;
            _settingsRepository.SetSetting(user, nameof(settings.CurrentIntroStage), settings.CurrentIntroStage.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompletedTutorial))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompletedTutorial);
            }

            List<MatchState> matchStates = _matchStateRepository.GetMatchStates(user);
            if (matchStates.Count > 0)
            {
                _matchStateRepository.DeleteMatchState(user, matchStates.First().Opponent, true, false);
            }

            string domain = Helper.GetDomain();
            Task.Run(() =>
            {
                _messagesRepository.SendPersonalMessage(user, domain);
            });
            _emailRepository.SendEmailVerification(user);

            return Json(new Status { Success = true, ReturnUrl = "/" });
        }

        [HttpGet]
        public IActionResult IsUsernameAvailable(string username)
        {
            if (_userRepository.IsUsernameInvalid(username))
            {
                return Json(new Status { Success = false });
            }

            bool isUsernameAvailable = _registerRepository.IsUsernameAvailable(username);
            return Json(new Status { Success = isUsernameAvailable });
        }

        [HttpGet]
        public IActionResult IsEmailAvailable(string emailAddress)
        {
            if (_userRepository.IsEmailAddressInvalid(emailAddress))
            {
                return Json(new Status { Success = false });
            }

            bool isEmailAddressAvailable = _registerRepository.IsEmailAvailable(emailAddress);
            return Json(new Status { Success = isEmailAddressAvailable });
        }

        [HttpGet]
        public IActionResult GetIntroPartialView()
        {
            User user = _loginRepository.AuthenticateUser();

            Settings settings = null;
            if (user != null)
            {
                settings = _settingsRepository.GetSettings(user);
                if (settings.CurrentIntroStage == IntroStage.Completed)
                {
                    return Json(new Status { Success = true, ReturnUrl = "/" });
                }
            }

            IntroViewModel introViewModel = GenerateModel(user, settings);
            if (introViewModel.Stage == IntroStage.First)
            {
                return PartialView("_IntroMainPartial", introViewModel);
            }
            if (introViewModel.Stage == IntroStage.Second)
            {
                return PartialView("_IntroSecondPartial", introViewModel);
            }
            return PartialView("_IntroThirdPartial", introViewModel);
        }

        private IntroViewModel GenerateModel(User user, Settings settings)
        {
            IntroViewModel introViewModel = new IntroViewModel
            {
                Title = settings == null ? "Choose Your Cards" : $"Welcome to {Resources.SiteName}!",
                User = user,
                ShowTitle = false,
                StarterItems = GetStarterItems(),
                Stage = settings == null ? (GlobalHttpContext.Current.Session.GetObject<List<Item>>("StarterCards") == null ? IntroStage.First : IntroStage.Second) : settings.CurrentIntroStage,
                AllottedBudget = 15000
            };

            if (introViewModel.Stage == IntroStage.Second)
            {
                string[] referral = GlobalHttpContext.Current.Request.Cookies["Referral"]?.Split("|") ?? null;
                User referralUser = CheckReferral(referral?.FirstOrDefault());
                if (referralUser != null && referral.Length == 3)
                {
                    introViewModel.Referral = referralUser.GetFriendlyName();
                    introViewModel.EmailAddress = referral[1];
                    introViewModel.BetaCode = referral[2];
                }
                AdminSettings adminSettings = _adminSettingsRepository.GetSettings();
                introViewModel.IsBetaEnabled = adminSettings.IsBetaEnabled;
            }

            return introViewModel;
        }

        private List<StarterItems> GetStarterItems()
        {
            List<Card> cards = _itemsRepository.GetAllCards();
            List<Enhancer> enhancers = _itemsRepository.GetAllEnhancers().FindAll(x => x.TotalUses == -1);

            return new List<StarterItems>
            {
                new StarterItems
                {
                    Index = 0,
                    Title = "Balanced",
                    Balance = 85,
                    Power = 60,
                    Enhancer = 70,
                    Items = new List<Item>
                    {
                        cards[17],
                        cards[17],
                        cards[15],
                        cards[15],
                        cards[14],
                        enhancers.Find(x => x.Name == "+5 - Unlimited")
                    }
                },
                 new StarterItems
                {
                    Index = 1,
                    Title = "Brute",
                    Balance = 40,
                    Power = 80,
                    Enhancer = 50,
                    Items = new List<Item>
                    {
                        cards[24],
                        cards[23],
                        cards[10],
                        cards[9],
                        cards[9],
                        enhancers.Find(x => x.Name == "20% - Unlimited")
                    }
                },
                new StarterItems
                {
                    Index = 2,
                    Title = "Strategic",
                    Balance = 70,
                    Power = 50,
                    Enhancer = 85,
                    Items = new List<Item>
                    {
                        cards[17],
                        cards[15],
                        cards[12],
                        cards[11],
                        cards[11],
                        enhancers.Find(x => x.Name == "+10 - Unlimited")
                    }
                }
            };
        }

        private User CheckReferral(string referral)
        {
            if (string.IsNullOrEmpty(referral))
            {
                return null;
            }

            User referralUser = _userRepository.GetUser(referral);
            if (referralUser == null)
            {
                GlobalHttpContext.Current.Response.Cookies.Delete("Referral");
            }
            return referralUser;
        }

        private async Task SendWelcomeEmail(User recipient, string domain)
        {
            await Task.Delay(TimeSpan.FromHours(2));

            recipient = _userRepository.GetUser(recipient.Username);
            if (recipient != null)
            {
                string code = _promoCodesRepository.GenerateRandomPromoCode(30, PromoEffect.CardPack);
                string subject = $"Welcome!";
                string title = $"Welcome to {Resources.SiteName}";
                string body = $"Thanks for joining {Resources.SiteName}! If you have any questions at all, feel free to let me know and I'd love to answer them! You can join the Discord at: {Helper.GetDiscordUrl()} for tips, advice, and discussion. Also, as a welcoming present, here's a promo code to get a free Card Pack: <b><a href='{domain}/PromoCodes?code={code}'>{code}</a></b>.";

                Task _ = _emailRepository.SendEmail(recipient, subject, title, body, $"/PromoCodes?code={code}", false, domain, "Tyler");
            }
        }
    }
}
