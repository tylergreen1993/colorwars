﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class WizardController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private IWizardRepository _wizardRepository;
        private IStocksRepository _stocksRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public WizardController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IWizardRepository wizardRepository, IStocksRepository stocksRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISettingsRepository settingsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _wizardRepository = wizardRepository;
            _stocksRepository = stocksRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Wizard" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Records", "Wizard");
            }

            WizardViewModel wizardViewModel = GenerateModel(user, false);
            wizardViewModel.UpdateViewData(ViewData);
            return View(wizardViewModel);
        }

        public IActionResult Records()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Wizard/Records" });
            }

            WizardViewModel wizardViewModel = GenerateModel(user, true);
            wizardViewModel.UpdateViewData(ViewData);
            return View(wizardViewModel);
        }

        public IActionResult SecretCavern()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Wizard/SecretCavern" });
            }

            WizardViewModel wizardViewModel = GenerateModel(user, true);
            if (!wizardViewModel.CanSeeSecretCavern)
            {
                return RedirectToAction("Index", "Wizard");
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SecretCavernsDiscovered))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SecretCavernsDiscovered);
            }

            wizardViewModel.UpdateViewData(ViewData);
            return View(wizardViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CastEffect(WizardWandColor selectedWandColor)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int minutesSinceLastEffect = (int)(DateTime.UtcNow - settings.LastWizardDate).TotalMinutes;
            if (minutesSinceLastEffect < 180)
            {
                string time;
                int minsLeft = 180 - minutesSinceLastEffect;
                if (minsLeft >= 60)
                {
                    int hours = (int)Math.Round((double)minsLeft / 60);
                    time = $"{hours} hour{(hours == 1 ? "" : "s")}";
                }
                else
                {
                    time = $"{minsLeft} minute{(minsLeft == 1 ? "" : "s")}";
                }

                return Json(new Status { Success = false, ErrorMessage = $"The Young Wizard's powers are recharging. Try again in {time}." });
            }

            if (selectedWandColor == WizardWandColor.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a wand for the Young Wizard to use." });
            }

            List<Card> deck = _itemsRepository.GetCards(user, true);

            int noEffectOdds = 20;
            if (deck.All(x => x.Color.ToString() == selectedWandColor.ToString()))
            {
                noEffectOdds = 10;
            }

            Random rnd = new Random();
            int positiveOdds = 45;
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    positiveOdds = (int)Math.Floor(positiveOdds * 0.9);
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    positiveOdds = (int)Math.Ceiling(positiveOdds * 1.1);
                }
            }

            if (rnd.Next(10) == 1)
            {
                settings.LuckLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LuckLevel), settings.LuckLevel.ToString());
            }

            bool positiveEffect = settings.HasWizardBetterLuck || rnd.Next(100) >= positiveOdds;
            bool noEffect = rnd.Next(100) <= noEffectOdds;
            int randomNumber = rnd.Next(100);

            if (settings.LastWizardDate == DateTime.MinValue)
            {
                positiveEffect = true;
                noEffect = false;
            }

            settings.LastWizardDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastWizardDate), settings.LastWizardDate.ToString());

            if (settings.HasWizardBetterLuck)
            {
                settings.HasWizardBetterLuck = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasWizardBetterLuck), settings.HasWizardBetterLuck.ToString());
            }

            bool hasWizardGoldWand = settings.HasWizardGoldWand && selectedWandColor == WizardWandColor.Gold;
            if (settings.HasWizardGoldWand)
            {
                settings.HasWizardGoldWand = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasWizardGoldWand), settings.HasWizardGoldWand.ToString());
            }
            else if (!settings.HasWizardGoldWand && rnd.Next(0, 10) == 1 && user.GetAgeInHours() > 6)
            {
                settings.HasWizardGoldWand = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasWizardGoldWand), settings.HasWizardGoldWand.ToString());
            }

            if (hasWizardGoldWand)
            {
                positiveEffect = true;
                noEffect = false;
            }

            string soundUrl = "";
            string infoMessage = "";
            if (noEffect)
            {
                infoMessage = "The Young Wizard tried to cast an effect on you but nothing happened! Try again next time.";

                _wizardRepository.AddWizardHistory(user, WizardType.None, infoMessage);

                return Json(new Status { Success = true, InfoMessage = infoMessage, Points = Helper.GetFormattedPoints(user.Points) });
            }
            if (positiveEffect)
            {
                string successMessage = "";
                string buttonText = "";
                string buttonUrl = "";
                if (hasWizardGoldWand && rnd.Next(250) == 1)
                {
                    _pointsRepository.AddPoints(user, 25000);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WizardExtraLuck))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WizardExtraLuck);
                    }

                    successMessage = $"The Young Wizard successfully added {Helper.GetFormattedPoints(25000)} to your {Resources.Currency} on hand!";
                }
                else if (randomNumber < 5)
                {
                    List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.Condition != ItemCondition.New && x.OriginalPoints <= 65000);
                    if (items.Count > 0)
                    {
                        Item item = items[rnd.Next(items.Count)];
                        item.RepairedDate = DateTime.UtcNow;
                        _itemsRepository.UpdateItem(user, item);

                        successMessage = $"The Young Wizard successfully repaired your item \"{item.Name}\" and it now has a New condition!";
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        if (settings.LastMazeEntryDate > DateTime.MinValue)
                        {
                            settings.LastMazeEntryDate = DateTime.UtcNow.AddDays(-1);
                            _settingsRepository.SetSetting(user, nameof(settings.LastMazeEntryDate), settings.LastMazeEntryDate.ToString());
                            successMessage = $"The Young Wizard successfully let you visit the Maze again today if you already have!";
                            buttonText = "Head to the Maze!";
                            buttonUrl = "/SwapDeck/Maze";
                        }
                        else
                        {
                            infoMessage = "The Young Wizard tried to repair one of your items but all the applicable ones already have a New condition!";
                        }
                    }
                }
                else if (randomNumber < 15)
                {
                    settings.BuyersClubPoints += 1500;
                    _settingsRepository.SetSetting(user, nameof(settings.BuyersClubPoints), settings.BuyersClubPoints.ToString());

                    successMessage = $"The Young Wizard successfully granted you {Helper.FormatNumber(1500)} Buyers Club Points to be spent at the Buyers Club!";
                    buttonText = "Head to the Buyers Club!";
                    buttonUrl = "/BuyersClub";
                }
                else if (randomNumber < 25)
                {
                    List<Item> userItems = _itemsRepository.GetItems(user);
                    if (userItems.Count < settings.MaxBagSize)
                    {
                        List<Item> items = _itemsRepository.GetAllItems(ItemCategory.Crafting).FindAll(x => x.Points < (hasWizardGoldWand ? 25000 : 10000));
                        Item item = items[rnd.Next(items.Count)];
                        _itemsRepository.AddItem(user, item);

                        successMessage = $"The Young Wizard successfully added a \"{item.Name}\" Crafting Card to your bag!";
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = $"The Young Wizard tried to add a Crafting Card to your bag but it was full!";
                    }
                }
                else if (randomNumber < 30)
                {
                    settings.TrainingTokens++;
                    _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());

                    successMessage = $"The Young Wizard successfully gave you 1 Training Token!";
                    buttonText = "Head to Training Center!";
                    buttonUrl = "/TrainingCenter";
                }
                else if (randomNumber < 45)
                {
                    int amount = rnd.Next(1250) + 250;
                    _pointsRepository.AddPoints(user, amount);

                    successMessage = $"The Young Wizard successfully added {Helper.GetFormattedPoints(amount)} to your {Resources.Currency} on hand!";
                }
                else if (randomNumber < 50)
                {
                    if (settings.MaxBagSize < Helper.GetMaxBagSize())
                    {
                        settings.MaxBagSize += 1;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxBagSize), settings.MaxBagSize.ToString());

                        successMessage = $"The Young Wizard successfully increased your maximum bag size to {settings.MaxBagSize} items!";
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = $"The Young Wizard tried to increase your maximum bag size but it can't get any bigger!";
                    }
                }
                else if (randomNumber < 55)
                {
                    List<Item> userItems = _itemsRepository.GetItems(user);
                    if (userItems.Count < settings.MaxBagSize)
                    {
                        List<Item> frozenItems = _itemsRepository.GetAllItems(ItemCategory.Frozen);
                        Item frozenItem = frozenItems[rnd.Next(2)];

                        _itemsRepository.AddItem(user, frozenItem);

                        successMessage = $"The Young Wizard successfully added the item \"{frozenItem.Name}\" to your bag!";
                        buttonText = "Head to bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = $"The Young Wizard tried to add an item to your bag but it was full!";
                    }
                }
                else if (randomNumber < 60)
                {
                    Array libraryKnowledgetypes = Enum.GetValues(typeof(LibraryKnowledgeType));
                    LibraryKnowledgeType libraryKnowledgeType = (LibraryKnowledgeType)libraryKnowledgetypes.GetValue(rnd.Next(libraryKnowledgetypes.Length - 1) + 1);
                    if (libraryKnowledgeType == LibraryKnowledgeType.Opportunity)
                    {
                        settings.LibraryOpportunityLevel++;
                        _settingsRepository.SetSetting(user, nameof(settings.LibraryOpportunityLevel), settings.LibraryOpportunityLevel.ToString());

                        successMessage = $"The Young Wizard successfully increased your Opportunity Knowledge to Level {settings.LibraryOpportunityLevel} at the Library!";
                    }
                    else if (libraryKnowledgeType == LibraryKnowledgeType.Wealth)
                    {
                        settings.LibraryWealthLevel++;
                        _settingsRepository.SetSetting(user, nameof(settings.LibraryWealthLevel), settings.LibraryWealthLevel.ToString());

                        successMessage = $"The Young Wizard successfully increased your Wealth Knowledge to Level {settings.LibraryWealthLevel} at the Library!";
                    }
                    else
                    {
                        settings.LibraryPowerLevel++;
                        _settingsRepository.SetSetting(user, nameof(settings.LibraryPowerLevel), settings.LibraryPowerLevel.ToString());

                        successMessage = $"The Young Wizard successfully increased your Power Knowledge to Level {settings.LibraryPowerLevel} at the Library!";
                    }
                    buttonText = "Head to the Library!";
                    buttonUrl = "/Library";
                }
                else if (randomNumber < 65)
                {
                    List<Item> userItems = _itemsRepository.GetItems(user).FindAll(x => x.Category == ItemCategory.Deck && x.Theme == ItemTheme.Default);
                    foreach (Item item in userItems)
                    {
                        if (rnd.Next(hasWizardGoldWand ? 50 : 100) == 1)
                        {
                            ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                            ItemTheme theme = themes[rnd.Next(themes.Length)];
                            item.Theme = theme;
                            _itemsRepository.UpdateItem(user, item);

                            successMessage = $"The Young Wizard successfully changed your item's theme and it became \"{item.Name}\"!";
                            buttonText = "Head to your bag!";
                            buttonUrl = "/Items";
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(successMessage))
                    {
                        if (hasWizardGoldWand && settings.MaxStorageSize < Helper.GetMaxStorageSize())
                        {
                            settings.MaxStorageSize += 1;
                            _settingsRepository.SetSetting(user, nameof(settings.MaxStorageSize), settings.MaxStorageSize.ToString());

                            successMessage = $"The Young Wizard successfully increased your maximum Storage size to {settings.MaxBagSize} items!";
                            buttonText = "Head to your Storage!";
                            buttonUrl = "/Storage";
                        }
                        else
                        {
                            infoMessage = "The Young Wizard tried to cast an effect on you but nothing happened! Try again next time.";
                        }
                    }
                }
                else if (randomNumber < 75)
                {
                    List<Item> userItems = _itemsRepository.GetItems(user).FindAll(x => x.Uses < x.TotalUses);
                    if (userItems.Count > 0)
                    {
                        Item item = userItems[rnd.Next(userItems.Count)];
                        item.Uses = item.TotalUses;
                        _itemsRepository.UpdateItem(user, item);

                        successMessage = $"The Young Wizard successfully recharged your item \"{item.Name}\" and it now has {item.TotalUses} uses!";
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = $"The Young Wizard tried to recharge one of your items but they all have their original total uses!";
                    }
                }
                else if (randomNumber < 90)
                {
                    List<Item> userItems = _itemsRepository.GetItems(user);
                    if (userItems.Count < settings.MaxBagSize)
                    {
                        List<Item> cardPacks = _itemsRepository.GetAllItems(ItemCategory.Pack);
                        Item item = rnd.Next(hasWizardGoldWand ? 5 : 10) == 1 ? rnd.Next(50) == 1 ? cardPacks.Last() : cardPacks[1] : cardPacks.First();
                        _itemsRepository.AddItem(user, item);

                        successMessage = $"The Young Wizard successfully added a Card Pack to your bag!";
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = "The Young Wizard tried to add a Card Pack to your bag but it was full!";
                    }
                }
                else
                {
                    List<Stock> stocks = _stocksRepository.GetAllStocks();
                    Stock stock = stocks[rnd.Next(stocks.Count)];
                    int amount = rnd.Next(0, 4) + 2;
                    _stocksRepository.AddStock(user, stock, amount, false, true);

                    successMessage = $"The Young Wizard successfully added {amount} shares of the stock \"{stock.Name}\" to your Stock Market portfolio!";
                    buttonText = "Head to your Portfolio!";
                    buttonUrl = "/StockMarket/Portfolio";
                }

                if (string.IsNullOrEmpty(successMessage))
                {
                    _wizardRepository.AddWizardHistory(user, WizardType.None, infoMessage);
                }
                else
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SuccessfulWizardVisit))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SuccessfulWizardVisit);
                    }

                    _wizardRepository.AddWizardHistory(user, WizardType.Positive, successMessage);

                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);
                }

                return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }
            else
            {
                string dangerMessage = "";
                string buttonText = "";
                string buttonUrl = "";
                if (settings.BagProtectExpiryDate > DateTime.UtcNow)
                {
                    randomNumber = rnd.Next(35, 100);
                }
                if (randomNumber < 20)
                {
                    List<Item> items = _itemsRepository.GetItems(user, true);
                    if (items.Count > 0)
                    {
                        int daysAdded = rnd.Next(7, 31);
                        Item item = items[rnd.Next(items.Count)];
                        item.RepairedDate = item.RepairedDate.AddDays(-daysAdded);
                        _itemsRepository.UpdateItem(user, item);

                        dangerMessage = $"The Young Wizard made a mistake and your item \"{item.Name}\" had its condition worsened by {daysAdded} days!";
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = "The Young Wizard tried to cast an effect on you but nothing happened! Try again next time.";
                    }
                }
                else if (randomNumber < 35)
                {
                    List<Item> items = _itemsRepository.GetItems(user).FindAll(x => x.Uses > 1);
                    if (items.Count > 0)
                    {
                        Item item = items[rnd.Next(items.Count)];
                        item.Uses = 1;
                        _itemsRepository.UpdateItem(user, item);

                        dangerMessage = $"The Young Wizard made a mistake and your item \"{item.Name}\" has only 1 use left!";
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                    }
                    else
                    {
                        infoMessage = "The Young Wizard tried to cast an effect on you but nothing happened! Try again next time.";
                    }
                }
                else if (randomNumber < 85)
                {
                    if (user.Points > 0)
                    {
                        int amount = rnd.Next(1, Math.Max(1, user.Points / 3));
                        _pointsRepository.SubtractPoints(user, amount);

                        dangerMessage = $"The Young Wizard made a mistake and {Helper.FormatNumber(amount)} of your {Resources.Currency} on hand vanished!";
                        _wizardRepository.AddWizardRecord(user, amount, WizardRecordType.PointsLoss);

                        List<WizardRecord> wizardLosses = _wizardRepository.GetBiggestWizardLosses(30).Take(50).ToList();
                        if (wizardLosses.Any(x => x.Username == user.Username && x.Amount == amount))
                        {
                            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.WizardBigLoss))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.WizardBigLoss);
                            }
                            infoMessage = $"You qualified for The Young Wizard's biggest losses by having {Helper.GetFormattedPoints(amount)} vanished!";
                        }
                    }
                    else
                    {
                        infoMessage = $"The Young Wizard made a mistake but you had no {Resources.Currency} on hand for him to make vanish!";
                    }
                }
                else
                {
                    int totalMinutes = rnd.Next(1, 7) * 5;
                    settings.CursePercent = rnd.Next(-20, -5);
                    settings.CurseExpiryDate = DateTime.UtcNow.AddMinutes(totalMinutes);
                    _settingsRepository.SetSetting(user, nameof(settings.CursePercent), settings.CursePercent.ToString());
                    _settingsRepository.SetSetting(user, nameof(settings.CurseExpiryDate), settings.CurseExpiryDate.ToString());

                    dangerMessage = $"The Young Wizard made a mistake and gave you a {settings.CursePercent}% Curse at the Stadium for {totalMinutes} minutes!";
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                }

                _wizardRepository.AddWizardHistory(user, WizardType.Negative, dangerMessage);

                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Negative, settings);

                return Json(new Status { Success = true, InfoMessage = infoMessage, DangerMessage = dangerMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }
        }

        [HttpGet]
        public IActionResult GetWizardPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            WizardViewModel wizardViewModel = GenerateModel(user, false);
            return PartialView("_WizardMainPartial", wizardViewModel);
        }

        private WizardViewModel GenerateModel(User user, bool isRecord)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            WizardViewModel wizardViewModel = new WizardViewModel
            {
                Title = "The Young Wizard",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.YoungWizard,
                CanSeeWizard = (DateTime.UtcNow - settings.LastWizardDate).TotalMinutes >= 180,
                HasBetterLuck = settings.HasWizardBetterLuck,
                CanSeeSecretCavern = settings.BeyondWorldLevel == 3,
                GuardiansDefeated = settings.SorceressGuardiansDefeated,
                Wands = new List<WizardWandColor> { WizardWandColor.Red, WizardWandColor.Orange, WizardWandColor.Yellow, WizardWandColor.Green, WizardWandColor.Blue, WizardWandColor.Purple }
            };

            if (isRecord)
            {
                wizardViewModel.WizardRecords = _wizardRepository.GetBiggestWizardLosses(30).Take(50).ToList();
            }
            else
            {
                if (wizardViewModel.CanSeeWizard && settings.HasWizardGoldWand)
                {
                    wizardViewModel.Wands.Add(WizardWandColor.Gold);
                }
                wizardViewModel.WizardHistories = _wizardRepository.GetWizardHistory(user).Take(15).ToList();
            }

            if (wizardViewModel.CanSeeSecretCavern)
            {
                wizardViewModel.Deck = _itemsRepository.GetCards(user, true);
            }

            return wizardViewModel;
        }
    }
}
