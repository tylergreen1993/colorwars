﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;
using RandomNameGeneratorLibrary;

namespace ColorWars.Controllers
{
    public class TopAdoptionsController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ICompanionsRepository _companionsRepository;
        private ICellarStoreRepository _cellarStoreRepository;
        private IItemsRepository _itemsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public TopAdoptionsController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ICompanionsRepository companionsRepository, IItemsRepository itemsRepository, ICellarStoreRepository cellarStoreRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _companionsRepository = companionsRepository;
            _itemsRepository = itemsRepository;
            _cellarStoreRepository = cellarStoreRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TopAdoptions" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Cellar", "TopAdoptions");
            }

            TopAdoptionsViewModel topAdoptionsViewModel = GenerateModel(user, false);
            topAdoptionsViewModel.UpdateViewData(ViewData);
            return View(topAdoptionsViewModel);
        }

        public IActionResult Cellar()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "TopAdoptions/Cellar" });
            }

            TopAdoptionsViewModel topAdoptionsViewModel = GenerateModel(user, true);
            topAdoptionsViewModel.UpdateViewData(ViewData);
            return View(topAdoptionsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Adopt(Guid companionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Companion companion = GetAdoptionCompanions().Find(x => x.Id == companionId);
            if (companion == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This Companion is no longer available." });
            }

            if (user.Points < companion.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            List<Companion> userCompanions = _companionsRepository.GetCompanions(user);
            Settings settings = _settingsRepository.GetSettings(user);
            if (userCompanions.Count >= settings.MaxCompanions)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You cannot have more than {Helper.FormatNumber(settings.MaxCompanions)} Companions on your profile." });
            }

            if (_pointsRepository.SubtractPoints(user, companion.Points))
            {
                if (_companionsRepository.AddCompanion(user, companion.Type, companion.Name, companion.Color, out Companion addedCompanion))
                {
                    addedCompanion.Level = companion.Level;
                    addedCompanion.CreatedDate = companion.CreatedDate;
                    _companionsRepository.UpdateCompanion(addedCompanion);
                    _companionsRepository.DeleteAdoptionCompanion(companion);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CompanionActivated))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CompanionActivated);
                    }

                    Messages.AddSnack($"{companion.Name} was successfully adopted!");

                    string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

                    return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{addedCompanion.Id}", SoundUrl = soundUrl });
                }
            }

            return Json(new Status { Success = false, ErrorMessage = $"{companion.Name} could not be adopted." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OpenCellarDoor()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (accomplishments.Any(x => x.Id == AccomplishmentId.CellarDoorUnlocked))
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already unlocked the Cellar." });
            }

            List<KeyCard> keyCards = _itemsRepository.GetKeyCards(user);
            List<IGrouping<int, KeyCard>> keys = keyCards.GroupBy(x => x.Piece).ToList();
            if (keys.Count < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = "You need all 6 Key Cards to unlock the Cellar." });
            }

            foreach (IGrouping<int, KeyCard> keyGrouping in keys)
            {
                KeyCard card = keyGrouping.First();
                _itemsRepository.RemoveItem(user, card);
            }

            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CellarDoorUnlocked))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CellarDoorUnlocked);
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = "You successfully unlocked the Cellar!", SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GetCellarItem(CellarStoreItemId itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.CellarDoorUnlocked))
            {
                return Json(new Status { Success = false, ErrorMessage = "You've haven't unlocked the Cellar yet." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if ((DateTime.UtcNow - settings.CellarStoreLastPurchaseDate).TotalHours < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought something too recently. Try again in {settings.CellarStoreLastPurchaseDate.AddHours(6).GetTimeUntil()}." });
            }

            CellarStoreItem cellarStoreItem = _cellarStoreRepository.GetItems().Find(x => x.Id == itemId);
            if (cellarStoreItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item doesn't exist." });
            }

            if (user.Points < cellarStoreItem.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Random rnd = new Random();
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            switch (cellarStoreItem.Id)
            {
                case CellarStoreItemId.CompanionColor:
                    List<Companion> companions = _companionsRepository.GetCompanions(user);
                    if (companions.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't have any Companions." });
                    }
                    Companion companion = companions[rnd.Next(companions.Count)];
                    List<CompanionColor> companionColors = Enum.GetValues(typeof(CompanionColor)).Cast<CompanionColor>().ToList();
                    companionColors.RemoveAll(x => x == companion.Color);
                    companion.Color = companionColors[rnd.Next(companionColors.Count)];
                    _companionsRepository.UpdateCompanion(companion);

                    Messages.AddSnack($"{companion.Name} successfully had a color change!");
                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{companion.Id}" });
                case CellarStoreItemId.CompanionLevel:
                    companions = _companionsRepository.GetCompanions(user);
                    if (companions.Count == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't have any Companions." });
                    }
                    companion = companions[rnd.Next(companions.Count)];
                    companion.Level++;
                    _companionsRepository.UpdateCompanion(companion);

                    Messages.AddSnack($"The level of your Companion, {companion.Name}, was successfully increased!");
                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, ReturnUrl = $"/Profile#companion-{companion.Id}" });
                case CellarStoreItemId.ExtraNoticeBoardDay:
                    if (settings.NoticeBoardExpiryDate <= DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't currently have an active Notice Board task." });
                    }

                    settings.NoticeBoardExpiryDate = settings.NoticeBoardExpiryDate.AddDays(1);
                    _settingsRepository.SetSetting(user, nameof(settings.NoticeBoardExpiryDate), settings.NoticeBoardExpiryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully extended your Notice Board task by 1 day!", Points = user.GetFormattedPoints(), ButtonText = "Head to the Notice Board!", ButtonUrl = "/NoticeBoard", SoundUrl = soundUrl });
                case CellarStoreItemId.MazeAccess:
                    if (!accomplishments.Any(x => x.Id == AccomplishmentId.MazeUnlocked))
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't currently have access to the Maze." });
                    }
                    if (settings.LastMazeEntryDate == DateTime.MinValue)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You haven't entered the Maze yet." });
                    }
                    settings.LastMazeEntryDate = settings.LastMazeEntryDate.AddDays(-1);
                    _settingsRepository.SetSetting(user, nameof(settings.LastMazeEntryDate), settings.LastMazeEntryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You can successfully visit the Maze again today!", ButtonText = "Head to the Maze!", Points = user.GetFormattedPoints(), ButtonUrl = "/SwapDeck/Maze", SoundUrl = soundUrl });
                case CellarStoreItemId.SummonMuseum:
                    if (settings.MuseumExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "The Museum is already opened!" });
                    }

                    settings.MuseumExpiryDate = DateTime.UtcNow.AddDays(1);
                    _settingsRepository.SetSetting(user, nameof(settings.MuseumExpiryDate), settings.MuseumExpiryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully summoned the Museum at the Plaza for the next 24 hours!", Points = user.GetFormattedPoints(), ButtonText = "Head to the Museum!", ButtonUrl = "/Museum", SoundUrl = soundUrl });
                case CellarStoreItemId.ThemeSpeedUp:
                    if (settings.ThemeCardSqueezeDate <= DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't have an item that is actively being squeezed at the Theme Squeezer." });
                    }
                    settings.ThemeCardSqueezeDate = settings.ThemeCardSqueezeDate.AddHours(-12);
                    _settingsRepository.SetSetting(user, nameof(settings.ThemeCardSqueezeDate), settings.ThemeCardSqueezeDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "Your item at the Theme Squeezer was successfully sped up by 12 hours!", Points = user.GetFormattedPoints(), ButtonText = "Head to the Theme Squeezer!", ButtonUrl = "/Villager/ThemeSqueezer", SoundUrl = soundUrl });
                case CellarStoreItemId.ExtendedStreakCardsStreakShort:
                    if ((DateTime.UtcNow - settings.LastDailyStreakCardsPlayDate).TotalHours >= 48 || settings.StreakCardsStreak == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't have a Streak Cards streak or it has already expired." });
                    }

                    settings.StreakCardsNoExpiryDate = DateTime.UtcNow.AddDays(1);
                    _settingsRepository.SetSetting(user, nameof(settings.StreakCardsNoExpiryDate), settings.StreakCardsNoExpiryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully extended your Streak Cards streak from expiring by 1 day!", Points = user.GetFormattedPoints(), ButtonText = "Head to Streak Cards!", ButtonUrl = "/StreakCards", SoundUrl = soundUrl });
                case CellarStoreItemId.ExtendedStreakCardsStreakLong:
                    if ((DateTime.UtcNow - settings.LastDailyStreakCardsPlayDate).TotalHours >= 48 || settings.StreakCardsStreak == 0)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "You don't have a Streak Cards streak or it has already expired." });
                    }

                    settings.StreakCardsNoExpiryDate = DateTime.UtcNow.AddDays(7);
                    _settingsRepository.SetSetting(user, nameof(settings.StreakCardsNoExpiryDate), settings.StreakCardsNoExpiryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully extended your Streak Cards streak from expiring by 1 week!", Points = user.GetFormattedPoints(), ButtonText = "Head to Streak Cards!", ButtonUrl = "/StreakCards", SoundUrl = soundUrl });
                case CellarStoreItemId.SummonRewardsStore:
                    if (settings.RewardsStoreExpiryDate > DateTime.UtcNow)
                    {
                        return Json(new Status { Success = false, ErrorMessage = "The Rewards Store is already opened!" });
                    }

                    settings.RewardsStoreExpiryDate = DateTime.UtcNow.AddHours(6);
                    _settingsRepository.SetSetting(user, nameof(settings.RewardsStoreExpiryDate), settings.RewardsStoreExpiryDate.ToString());

                    CompleteTransaction(user, cellarStoreItem, settings);

                    return Json(new Status { Success = true, SuccessMessage = "You successfully summoned the Rewards Store at the Plaza for the next 6 hours!", Points = user.GetFormattedPoints(), ButtonText = "Head to the Rewards Store!", ButtonUrl = "/RewardsStore", SoundUrl = soundUrl });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be bought." });
        }

        [HttpGet]
        public IActionResult GetTopAdoptionsPartialView(bool isCellar = false)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            TopAdoptionsViewModel topAdoptionsViewModel = GenerateModel(user, isCellar);
            if (isCellar)
            {
                return PartialView("_TopAdoptionsCellarPartial", topAdoptionsViewModel);
            }

            return PartialView("_TopAdoptionsMainPartial", topAdoptionsViewModel);
        }

        private TopAdoptionsViewModel GenerateModel(User user, bool isCellar)
        {
            TopAdoptionsViewModel topAdoptionsViewModel = new TopAdoptionsViewModel
            {
                Title = "Top Adoptions",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.TopAdoptions,
            };

            if (isCellar)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

                topAdoptionsViewModel.IsCellarUnlocked = accomplishments.Any(x => x.Id == AccomplishmentId.CellarDoorUnlocked);
                if (topAdoptionsViewModel.IsCellarUnlocked)
                {
                    Settings settings = _settingsRepository.GetSettings(user);

                    topAdoptionsViewModel.CellarStoreItems = _cellarStoreRepository.GetItems();
                    topAdoptionsViewModel.CanBuyCellarStoreItem = (DateTime.UtcNow - settings.CellarStoreLastPurchaseDate).TotalHours >= 6;
                }
                else
                {
                    topAdoptionsViewModel.KeyCards = _itemsRepository.GetKeyCards(user);
                }
            }
            else
            {
                topAdoptionsViewModel.Companions = GetAdoptionCompanions();

                Random rnd = new Random();
                if (topAdoptionsViewModel.Companions.Count == 0 && rnd.Next(5) == 1)
                {
                    Task.Run(() =>
                    {
                        List<CompanionType> companionTypes = Enum.GetValues(typeof(CompanionType)).Cast<CompanionType>().ToList();
                        companionTypes.Remove(CompanionType.None);
                        companionTypes.Remove(CompanionType.Dragon);
                        companionTypes.Remove(CompanionType.Panda);
                        companionTypes.Remove(CompanionType.Unicorn);
                        List<CompanionColor> colors = Enum.GetValues(typeof(CompanionColor)).Cast<CompanionColor>().ToList();

                        Companion companion = new Companion
                        {
                            Id = Guid.NewGuid(),
                            Type = companionTypes[rnd.Next(companionTypes.Count)],
                            Color = colors[rnd.Next(colors.Count)],
                            Level = rnd.Next(1, 5),
                            CreatedDate = DateTime.UtcNow
                        };

                        PersonNameGenerator personNameGenerator = new PersonNameGenerator();
                        companion.Name = personNameGenerator.GenerateRandomFirstName();

                        _companionsRepository.AddAdoptionCompanion(companion);
                    });
                }
            }

            return topAdoptionsViewModel;
        }

        private List<Companion> GetAdoptionCompanions()
        {
            List<Companion> adoptionCompanions = _companionsRepository.GetAdoptionCompanions();
            foreach (Companion companion in adoptionCompanions)
            {
                int points = 3500;
                points += companion.Level * 500;
                switch (companion.Type)
                {
                    case CompanionType.Dragon:
                        points *= 20;
                        break;
                    case CompanionType.Unicorn:
                        points *= 20;
                        break;
                    case CompanionType.Panda:
                        points *= 50;
                        break;
                }

                companion.Points = points;
            }

            return adoptionCompanions;
        }

        private void CompleteTransaction(User user, CellarStoreItem cellarStoreItem, Settings settings)
        {
            if (_pointsRepository.SubtractPoints(user, cellarStoreItem.Points))
            {
                settings.CellarStoreLastPurchaseDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.CellarStoreLastPurchaseDate), settings.CellarStoreLastPurchaseDate.ToString());
            }
        }
    }
}
