﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class CrystalBallController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public CrystalBallController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "CrystalBall" });
            }

            CrystalBallViewModel crystalBallViewModel = GenerateModel(user);
            crystalBallViewModel.UpdateViewData(ViewData);
            return View(crystalBallViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Start()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastCrystalBallPlayDate).TotalHours;
            if (hoursSinceLastGame < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played too recently. You can play this game again in {6 - hoursSinceLastGame} hour{(6 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            if (!string.IsNullOrEmpty(settings.CrystalBallColors))
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an active Crystal Ball game." });
            }

            Random rnd = new Random();
            List<CardColor> colors = GetAvailableColors();

            List<CardColor> crystalBallColors = new List<CardColor>();
            for (int i = 0; i < 5; i++)
            {
                crystalBallColors.Add(colors[rnd.Next(colors.Count)]);
            }

            string crystalBallColorsString = string.Join("-", crystalBallColors);
            settings.CrystalBallColors = crystalBallColorsString;
            _settingsRepository.SetSetting(user, nameof(settings.CrystalBallColors), settings.CrystalBallColors);

            return Json(new Status { Success = true, InfoMessage = crystalBallColorsString });
        }

        [HttpPost]
        public IActionResult ChooseColor(CardColor color)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (string.IsNullOrEmpty(settings.CrystalBallColors))
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't seen any colors yet." });
            }

            int hoursSinceLastGame = (int)(DateTime.UtcNow - settings.LastCrystalBallPlayDate).TotalHours;
            if (hoursSinceLastGame < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've played too recently. Try again in {6 - hoursSinceLastGame} hour{(6 - hoursSinceLastGame == 1 ? "" : "s")}." });
            }

            List<CardColor> colors = settings.CrystalBallColors.Split('-').Select(x => Enum.Parse<CardColor>(x)).ToList();
            if (colors.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You have no more colors to choose." });
            }

            string soundUrl = "";

            if (color == colors.FirstOrDefault())
            {
                colors.RemoveAt(0);

                settings.CrystalBallColors = string.Join("-", colors);
                _settingsRepository.SetSetting(user, nameof(settings.CrystalBallColors), settings.CrystalBallColors);

                if (colors.Count == 0)
                {
                    settings.CrystalBallStreak++;
                    _settingsRepository.SetSetting(user, nameof(settings.CrystalBallStreak), settings.CrystalBallStreak.ToString());

                    int points = Math.Min(25000, 500 * settings.CrystalBallStreak);

                    if (_pointsRepository.AddPoints(user, points))
                    {
                        settings.LastCrystalBallPlayDate = DateTime.UtcNow;
                        _settingsRepository.SetSetting(user, nameof(settings.LastCrystalBallPlayDate), settings.LastCrystalBallPlayDate.ToString());

                        List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CrystalBallWon))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CrystalBallWon);
                        }
                        if (settings.CrystalBallStreak >= 10)
                        {
                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CrystalBall10xStreak))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CrystalBall10xStreak);
                            }
                        }
                        if (settings.CrystalBallStreak >= 25)
                        {
                            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.CrystalBall25xStreak))
                            {
                                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.CrystalBall25xStreak);
                            }
                        }

                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = $"You successfully chose all the colors in the right order and earned {Helper.GetFormattedPoints(points)}!", Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
                    }
                }

                return Json(new Status { Success = true });
            }

            settings.CrystalBallColors = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.CrystalBallColors), settings.CrystalBallColors);

            settings.LastCrystalBallPlayDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastCrystalBallPlayDate), settings.LastCrystalBallPlayDate.ToString());

            if (settings.CrystalBallStreak > 0)
            {
                settings.CrystalBallStreak = 0;
                _settingsRepository.SetSetting(user, nameof(settings.CrystalBallStreak), settings.CrystalBallStreak.ToString());
            }

            soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);

            return Json(new Status { Success = true, DangerMessage = $"You didn't get the order of the colors right. You chose \"{color}\" but should've chosen \"{colors.FirstOrDefault()}\". Better luck next time!", SoundUrl = soundUrl });
        }

        [HttpGet]
        public IActionResult GetCrystalBallPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            CrystalBallViewModel crystalBallViewModel = GenerateModel(user);
            return PartialView("_CrystalBallMainPartial", crystalBallViewModel);
        }

        private CrystalBallViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            CrystalBallViewModel crystalBallViewModel = new CrystalBallViewModel
            {
                Title = "Crystal Ball",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                Colors = GetAvailableColors(),
                Streak = settings.CrystalBallStreak,
                HasSeenPattern = !string.IsNullOrEmpty(settings.CrystalBallColors),
                CanPlayCrystalBall = (DateTime.UtcNow - settings.LastCrystalBallPlayDate).TotalHours >= 6
            };

            return crystalBallViewModel;
        }

        private List<CardColor> GetAvailableColors()
        {
            List<CardColor> colors = Enum.GetValues(typeof(CardColor)).Cast<CardColor>().ToList();
            colors.Remove(CardColor.None);
            colors.Remove(CardColor.Club);
            colors.Remove(CardColor.Gold);

            return colors;
        }
    }
}
