﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class SpinSuccessController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IItemsRepository _itemsRepository;
        private ISpinSuccessRepository _spinSuccessRepository;
        private IMatchStateRepository _matchStateRepository;
        private IJunkyardRepository _junkyardRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public SpinSuccessController(ILoginRepository loginRepository, IPointsRepository pointsRepository, IItemsRepository itemsRepository,
        ISpinSuccessRepository spinSuccessRepository, IMatchStateRepository matchStateRepository, IJunkyardRepository junkyardRepository,
        ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _itemsRepository = itemsRepository;
            _spinSuccessRepository = spinSuccessRepository;
            _matchStateRepository = matchStateRepository;
            _junkyardRepository = junkyardRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "SpinSuccess" });
            }

            SpinSuccessViewModel spinSuccessViewModel = GenerateModel(user);
            spinSuccessViewModel.UpdateViewData(ViewData);
            return View(spinSuccessViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SpinWheel()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastSpin = (int)(DateTime.UtcNow - settings.LastSpinSuccessDate).TotalHours;
            if (hoursSinceLastSpin < 3)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've spun the wheel too recently. Try again {3 - hoursSinceLastSpin} hour{(3 - hoursSinceLastSpin == 1 ? "" : "s")}." });
            }


            Random rnd = new Random();
            if (rnd.Next(10) == 1)
            {
                settings.LuckLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LuckLevel), settings.LuckLevel.ToString());
            }

            string successMessage = null;
            string dangerMessage = null;
            string buttonText = null;
            string buttonUrl = null;
            string infoMessage = $"You spun the wheel but nothing happened. Better luck next time.";

            int odds = 3;
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds += 1;
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    odds -= 1;
                }
            }
            bool firstSpin = settings.LastSpinSuccessDate == DateTime.MinValue;
            bool isPositive = firstSpin || settings.HasBetterSpinLuck || rnd.Next(odds) > 0;

            List<SpinSuccessCategory> spinSuccessCategories = Enum.GetValues(typeof(SpinSuccessCategory)).Cast<SpinSuccessCategory>().ToList();
            if (isPositive || settings.BagProtectExpiryDate > DateTime.UtcNow)
            {
                spinSuccessCategories.RemoveAll(x => x == SpinSuccessCategory.StolenItem);
            }
            SpinSuccessCategory spinSuccessCategory = firstSpin ? SpinSuccessCategory.NewItem : spinSuccessCategories[rnd.Next(spinSuccessCategories.Count)];

            switch (spinSuccessCategory)
            {
                case SpinSuccessCategory.DeckChange:
                    if (_matchStateRepository.IsInMatch(user))
                    {
                        break;
                    }
                    List<Card> deck = _itemsRepository.GetCards(user, true);
                    if (deck.Count == 0)
                    {
                        break;
                    }
                    if (isPositive)
                    {
                        deck = deck.FindAll(x => x.Amount < 100);
                    }
                    else
                    {
                        deck = deck.FindAll(x => x.Amount > 1);
                    }
                    if (deck.Count == 0)
                    {
                        break;
                    }
                    Card card = deck[rnd.Next(deck.Count)];
                    Card newCard = _itemsRepository.SwapCard(user, card, isPositive ? 1 : -1);
                    if (isPositive)
                    {
                        successMessage = $"Your {Resources.DeckCard} \"{card.Name}\" was improved and successfully became \"{newCard.Name}\"!";
                    }
                    else
                    {
                        dangerMessage = $"Your {Resources.DeckCard} \"{card.Name}\" was worsened and became \"{newCard.Name}\"!";
                    }
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case SpinSuccessCategory.NewItem:
                    List<Item> items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        break;
                    }
                    List<Item> allItems = _itemsRepository.GetAllItems().FindAll(x => x.Points >= 100 && x.Points <= (rnd.Next(10) == 0 ? rnd.Next(10) == 0 ? 250000 : 50000 : 25000));
                    if (firstSpin)
                    {
                        allItems = allItems.FindAll(x => x.Category == ItemCategory.Key);
                    }
                    Item item = allItems[rnd.Next(allItems.Count)];
                    _itemsRepository.AddItem(user, item);
                    successMessage = $"The item \"{item.Name}\" was successfully added to your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case SpinSuccessCategory.PointsChange:
                    if (isPositive)
                    {
                        int points = rnd.Next(10) == 0 ? rnd.Next(25) == 0 ? rnd.Next(50000, 250000) + 1 : rnd.Next(5000, 15000) + 1 : rnd.Next(500, 2500);
                        _pointsRepository.AddPoints(user, points);
                        successMessage = $"You successfully had {Helper.GetFormattedPoints(points)} added to your on hand {Resources.Currency}!";
                    }
                    else
                    {
                        if (user.Points == 0)
                        {
                            break;
                        }
                        int points = rnd.Next(1, user.Points / 5);
                        _pointsRepository.SubtractPoints(user, points);
                        dangerMessage = $"You had {Helper.GetFormattedPoints(points)} stolen from your on hand {Resources.Currency}!";
                    }
                    break;
                case SpinSuccessCategory.RepairedItem:
                    items = _itemsRepository.GetItems(user).FindAll(x => x.Condition > ItemCondition.New);
                    if (items.Count == 0)
                    {
                        break;
                    }
                    item = items[rnd.Next(items.Count)];
                    item.RepairedDate = DateTime.UtcNow;
                    _itemsRepository.UpdateItem(user, item);
                    successMessage = $"Your item \"{item.Name}\" had its condition improved and it now has a New condition!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
                case SpinSuccessCategory.StadiumChange:
                    if (isPositive)
                    {
                        if (settings.StadiumBonusExpiryDate > DateTime.UtcNow)
                        {
                            break;
                        }
                        settings.StadiumBonusPercent = rnd.Next(10, 15) + 1;
                        settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(10);
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
                        _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());
                        successMessage = $"You successfully gained a {settings.StadiumBonusPercent}% Bonus for CPU matches for the next 10 minutes!";
                    }
                    else
                    {
                        if (settings.CurseExpiryDate > DateTime.UtcNow)
                        {
                            break;
                        }
                        settings.CursePercent = rnd.Next(-15, -10) - 1;
                        settings.CurseExpiryDate = DateTime.UtcNow.AddMinutes(10);
                        _settingsRepository.SetSetting(user, nameof(settings.CursePercent), settings.CursePercent.ToString());
                        _settingsRepository.SetSetting(user, nameof(settings.CurseExpiryDate), settings.CurseExpiryDate.ToString());
                        dangerMessage = $"You were given a {settings.CursePercent}% Curse at the Stadium for the next 10 minutes!";
                    }
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                    break;
                case SpinSuccessCategory.StolenItem:
                    items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
                    if (items.Count == 0)
                    {
                        break;
                    }
                    item = items.OrderBy(x => x.Points).First();
                    _itemsRepository.RemoveItem(user, item);
                    Task.Run(() =>
                    {
                        _junkyardRepository.AddJunkyardItem(item);
                    });
                    dangerMessage = $"You had the item \"{item.Name}\" taken from your bag!";
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    break;
            }

            settings.LastSpinSuccessDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastSpinSuccessDate), settings.LastSpinSuccessDate.ToString());

            if (settings.HasBetterSpinLuck)
            {
                settings.HasBetterSpinLuck = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSpinLuck), settings.HasBetterSpinLuck.ToString());
            }

            string message;
            SpinSuccessType type;
            if (!string.IsNullOrEmpty(successMessage))
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SpinSuccessPositive))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SpinSuccessPositive);
                }

                type = SpinSuccessType.Positive;
                message = successMessage;
            }
            else if (!string.IsNullOrEmpty(dangerMessage))
            {
                type = SpinSuccessType.Negative;
                message = dangerMessage;
            }
            else
            {
                type = SpinSuccessType.None;
                message = infoMessage;
            }

            _spinSuccessRepository.AddSpinSuccessHistory(user, type, message);

            string soundUrl = "";
            if (type == SpinSuccessType.Positive)
            {
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.Success, settings);
            }
            else if (type == SpinSuccessType.Negative)
            {
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeAlt, settings);
            }

            return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, DangerMessage = dangerMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, Amount = (int)spinSuccessCategory, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
        }

        private SpinSuccessViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            SpinSuccessViewModel spinSuccessViewModel = new SpinSuccessViewModel
            {
                Title = "Spin of Success",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                SpinSuccessHistories = _spinSuccessRepository.GetSpinSuccessHistory(user).Take(15).ToList(),
                CanSpin = (DateTime.UtcNow - settings.LastSpinSuccessDate).TotalHours >= 3,
                HasBetterLuck = settings.HasBetterSpinLuck
            };

            return spinSuccessViewModel;
        }
    }
}
