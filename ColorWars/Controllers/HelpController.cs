﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class HelpController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public HelpController(ILoginRepository loginRepository, IItemsRepository itemsRepository,
        IPowerMovesRepository powerMovesRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _powerMovesRepository = powerMovesRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user != null)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.HelpPageVisit))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.HelpPageVisit);
                }
            }

            HelpViewModel helpViewModel = GenerateModel(user);
            helpViewModel.UpdateViewData(ViewData);
            return View(helpViewModel);
        }

        private HelpViewModel GenerateModel(User user)
        {
            List<Item> items = _itemsRepository.GetAllItems();
            HelpViewModel helpViewModel = new HelpViewModel
            {
                Title = "Help Hub",
                User = user,
                Card = items.FindAll(x => x.Category == ItemCategory.Deck)[84],
                Enhancer = items.FindAll(x => x.Category == ItemCategory.Enhancer).Find(x => x.Name == "x2"),
                PowerMove = _powerMovesRepository.GetPowerMoves(null, null, 0).Find(x => x.Type == PowerMoveType.EnhancerSwap)
            };

            return helpViewModel;
        }
    }
}
