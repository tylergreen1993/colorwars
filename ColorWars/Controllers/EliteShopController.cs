﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class EliteShopController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public EliteShopController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IPointsRepository pointsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _pointsRepository = pointsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "EliteShop" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.EliteShopFound))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.EliteShopFound);
            }

            EliteShopViewModel eliteShopViewModel = GenerateModel(user);
            eliteShopViewModel.UpdateViewData(ViewData);
            return View(eliteShopViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BuyItem(Guid itemId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item item = GetItems().Find(x => x.Id == itemId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item does not exist." });
            }

            if (user.Points < item.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            int totalHoursSinceLastBuyCount = (int)(DateTime.UtcNow - settings.LastEliteShopPurchaseDate).TotalHours;
            if (totalHoursSinceLastBuyCount < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You've bought an item from the Elite Shop recently. Try again in {24 - totalHoursSinceLastBuyCount} hour{(24 - totalHoursSinceLastBuyCount == 1 ? "" : "s")}." });
            }

            if (_pointsRepository.SubtractPoints(user, item.Points))
            {
                _itemsRepository.AddItem(user, item);

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.EliteShopPurchase))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.EliteShopPurchase);
                }

                settings.LastEliteShopPurchaseDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.LastEliteShopPurchaseDate), settings.LastEliteShopPurchaseDate.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully bought the item \"{item.Name}\" for {Helper.GetFormattedPoints(item.Points)}!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points) });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be bought." });
        }

        [HttpGet]
        public IActionResult GetEliteShopPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            EliteShopViewModel eliteShopViewModel = GenerateModel(user);
            return PartialView("_EliteShopMainPartial", eliteShopViewModel);
        }

        private EliteShopViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            EliteShopViewModel eliteShopViewModel = new EliteShopViewModel
            {
                Title = "Elite Shop",
                User = user,
                Items = GetItems(),
                CanBuyEliteShopItem = (DateTime.UtcNow - settings.LastEliteShopPurchaseDate).TotalDays >= 1
            };

            return eliteShopViewModel;
        }

        private List<Item> GetItems()
        {
            return _itemsRepository.GetAllItems(ItemCategory.All, null, true).FindAll(x => x.IsExclusive && x.Points >= 1000000 && (x.Name.Contains("Blocker") || x.Name.Contains("Predictor") || x.Name.Contains("Panda") || x.Name.Contains("Lavender Stop") || x.Name.Contains("Crystal 4th") || x.Category == ItemCategory.Art)).ToList();
        }
    }
}
