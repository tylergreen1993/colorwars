﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class BankController : Controller
    {
        private ILoginRepository _loginRepository;
        private IBankRepository _bankRepository;
        private IPointsRepository _pointsRepository;
        private IUserRepository _userRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IMessagesRepository _messagesRepository;
        private ISoundsRepository _soundsRepository;

        public BankController(ILoginRepository loginRepository, IBankRepository bankRepository, IPointsRepository pointsRepository,
        IUserRepository userRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        IMessagesRepository messagesRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _bankRepository = bankRepository;
            _pointsRepository = pointsRepository;
            _userRepository = userRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _messagesRepository = messagesRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Bank" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Loans", "Bank");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.CurrentIntroStage != IntroStage.Completed)
            {
                return RedirectToAction("Index", "Intro");
            }

            BankViewModel bankViewModel = GenerateModel(user);
            bankViewModel.UpdateViewData(ViewData);
            return View(bankViewModel);
        }

        public IActionResult Loans()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Bank/Loans" });
            }

            BankViewModel bankViewModel = GenerateModel(user);
            bankViewModel.UpdateViewData(ViewData);
            return View(bankViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Deposit(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Bank bank = _bankRepository.GetBank(user);

            if (user.Points < amount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency}." });
            }
            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't deposit {Helper.GetFormattedPoints(amount)}." });
            }

            if (_bankRepository.Deposit(user, bank, amount))
            {
                _pointsRepository.SubtractPoints(user, amount);
            }

            AddBankAccomplishments(user, bank);

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"{Helper.GetFormattedPoints(amount)} was successfully deposited!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Withdraw(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Bank bank = _bankRepository.GetBank(user);
            if (bank == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't opened a bank account yet." });
            }

            if (bank.Points < amount)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} in your account." });
            }
            if (amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't withdraw {Helper.GetFormattedPoints(amount)}." });
            }

            _bankRepository.Withdraw(user, bank, amount);
            _pointsRepository.AddPoints(user, amount);

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"{Helper.GetFormattedPoints(amount)} was successfully withdrawn!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CollectInterest()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);

            Bank bank = _bankRepository.GetBank(user);
            if (bank == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't opened a bank account yet." });
            }

            int hoursSinceLastBankInterest = (int)(DateTime.UtcNow - settings.BankInterestDate).TotalHours;
            if (hoursSinceLastBankInterest < 24)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You already collected your daily interest today. Try again in {24 - hoursSinceLastBankInterest} hour{(24 - hoursSinceLastBankInterest == 1 ? "" : "s")}." });
            }

            int dailyInterest = bank.GetDailyInterest();
            if (dailyInterest <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't collect {Helper.GetFormattedPoints(dailyInterest)} as your daily interest." });
            }

            if (settings.BankLoanAmountOwed > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't collect your daily interest while you owe the Bank {Resources.Currency} for your loan." });
            }

            if (settings.HasDoubleBankInterest)
            {
                dailyInterest *= 2;
                settings.HasDoubleBankInterest = false;
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleBankInterest), settings.HasDoubleBankInterest.ToString());
            }

            if (_bankRepository.Deposit(user, bank, dailyInterest, true))
            {
                settings.BankInterestDate = DateTime.UtcNow;
                _settingsRepository.SetSetting(user, nameof(settings.BankInterestDate), settings.BankInterestDate.ToString());

                AddReferralBonus(user);
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"{Helper.GetFormattedPoints(dailyInterest)} was successfully added to your account!", SoundUrl = soundUrl });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeLoan(int amount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Bank bank = _bankRepository.GetBank(user);
            if (bank == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't take a loan until you've opened a bank account." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BankLoanAmountOwed > 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can't take a loan as you currently owe {Helper.GetFormattedPoints(settings.BankLoanAmountOwed)}." });
            }

            int maxAmount = GetMaxLoanAmount(bank);

            if (amount > maxAmount || amount <= 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't take this amount as a loan." });
            }

            if (_pointsRepository.AddPoints(user, amount))
            {
                settings.BankLoanAmountOwed = (int)Math.Round(amount * 1.075);
                _settingsRepository.SetSetting(user, nameof(settings.BankLoanAmountOwed), settings.BankLoanAmountOwed.ToString());

                if (amount == 1337)
                {
                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TrendyLoaner))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TrendyLoaner);
                    }

                    Messages.AddSnack("That's a 1337 amount to take!");
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took a loan and {Helper.GetFormattedPoints(amount)} was added to your {Resources.Currency} on hand!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = "You can't take a loan at this time." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PayLoan()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Bank bank = _bankRepository.GetBank(user);
            if (bank == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't pay back a loan until you've opened a bank account." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.BankLoanAmountOwed == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You have no loan to pay back." });
            }

            if (user.Points < settings.BankLoanAmountOwed)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand to pay back the loan." });
            }

            int amount = settings.BankLoanAmountOwed;

            if (_pointsRepository.SubtractPoints(user, settings.BankLoanAmountOwed))
            {
                settings.BankLoanAmountOwed = 0;
                _settingsRepository.SetSetting(user, nameof(settings.BankLoanAmountOwed), settings.BankLoanAmountOwed.ToString());

                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LoanPaidBack))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LoanPaidBack);
                }

                return Json(new Status { Success = true, SuccessMessage = $"You successfully paid back the loan of {Helper.GetFormattedPoints(amount)}!", Points = user.GetFormattedPoints() });
            }

            return Json(new Status { Success = false, ErrorMessage = $"You can't pay back the loan at this time." });
        }

        [HttpGet]
        public IActionResult GetBankPartialView(bool isBank)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            BankViewModel bankViewModel = GenerateModel(user);
            if (isBank)
            {
                return PartialView("_BankPartial", bankViewModel);
            }

            return PartialView("_BankLoansPartial", bankViewModel);
        }

        private BankViewModel GenerateModel(User user)
        {
            Bank bank = _bankRepository.GetBank(user);
            Settings settings = _settingsRepository.GetSettings(user);
            BankViewModel bankViewModel = new BankViewModel
            {
                Title = "Bank",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Bank,
                Bank = bank,
                HasDoubleBankInterest = settings.HasDoubleBankInterest
            };

            if (bank != null)
            {
                bankViewModel.BankAccountType = bank.GetAccountType();
                bankViewModel.NextBankAccountType = bank.GetNextAccountType();
                bankViewModel.BankInterest = bank.GetDailyInterest() * (settings.HasDoubleBankInterest ? 2 : 1);
                bankViewModel.BankTransactions = _bankRepository.GetBankTransactions(user).Take(15).ToList();
                bankViewModel.MaxLoanAmount = GetMaxLoanAmount(bank);
                bankViewModel.CanGetInterest = settings.BankLoanAmountOwed == 0 && (DateTime.UtcNow - settings.BankInterestDate).TotalDays >= 1;
                bankViewModel.LoanAmountOwed = settings.BankLoanAmountOwed;
            }
            return bankViewModel;
        }

        private void AddBankAccomplishments(User user, Bank bank)
        {
            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.OpenedBank))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.OpenedBank);
            }

            if (bank == null)
            {
                return;
            }

            if (bank.GetNextAccountType() == null)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MegaRich))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MegaRich);
                }
            }

            BankAccount currentAccountType = bank.GetAccountType();
            if (currentAccountType.Points >= 1000000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.MillionaireBank))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.MillionaireBank);

                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.EliteShopFound))
                    {
                        _messagesRepository.SendCourierMessage(user, $"An elite member of {Resources.SiteName} has a message for you: \"Congratulations! You've reached Millionaire status at the Bank! I suppose I should let you know about a somewhat secret store for people of our wealth. Just enter the promo code WEALTHY and you'll see what I mean.\"", Helper.GetDomain());
                    }
                }
            }
            if (currentAccountType.Points >= 250000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ExtremeEarnerBank))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ExtremeEarnerBank);
                }
            }
            if (currentAccountType.Points >= 100000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.SmallFortuneBank))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.SmallFortuneBank);
                }
            }
            if (currentAccountType.Points >= 20000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.RisingWealthBank))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.RisingWealthBank);
                }
            }
        }

        private void AddReferralBonus(User user)
        {
            if (!string.IsNullOrEmpty(user.Referral) && user.GetAgeInDays() <= 30)
            {
                User referralUser = _userRepository.GetUser(user.Referral);
                if (referralUser != null)
                {
                    _pointsRepository.AddPoints(referralUser, Helper.PointsPerReferralCollect());
                }
            }
        }

        private int GetMaxLoanAmount(Bank bank)
        {
            return Math.Min(100000, Math.Max(1, bank.Points / 10));
        }
    }
}
