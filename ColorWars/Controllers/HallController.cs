﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class HallController : Controller
    {
        private ILoginRepository _loginRepository;
        private IUserRepository _userRepository;

        public HallController(ILoginRepository loginRepository, IUserRepository userRepository)
        {
            _loginRepository = loginRepository;
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Hall" });
            }

            HallViewModel hallViewModel = GenerateModel(user);
            hallViewModel.UpdateViewData(ViewData);
            return View(hallViewModel);
        }

        private HallViewModel GenerateModel(User user)
        {
            List<User> adminUsers = _userRepository.GetUsersWithAdminRoles().FindAll(x => x.InactiveType == InactiveAccountType.None).OrderByDescending(x => x.Role).ThenByDescending(x => x.CreatedDate).ToList();
            HallViewModel hallViewModel = new HallViewModel
            {
                Title = "Hall of Helpers",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.HallOfHelpers,
                Helpers = adminUsers.FindAll(x => x.Role < UserRole.Admin),
                Staff = adminUsers.FindAll(x => x.Role >= UserRole.Admin)
            };

            return hallViewModel;
        }
    }
}
