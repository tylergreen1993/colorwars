﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class ShowcaseController : Controller
    {
        private ILoginRepository _loginRepository;
        private IItemsRepository _itemsRepository;
        private IShowcaseRepository _showcaseRepository;
        private IGroupsRepository _groupsRepository;
        private ISettingsRepository _settingsRepository;
        private IPointsRepository _pointsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public ShowcaseController(ILoginRepository loginRepository, IItemsRepository itemsRepository, IShowcaseRepository showcaseRepository,
        IGroupsRepository groupsRepository, ISettingsRepository settingsRepository, IPointsRepository pointsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _itemsRepository = itemsRepository;
            _showcaseRepository = showcaseRepository;
            _groupsRepository = groupsRepository;
            _settingsRepository = settingsRepository;
            _pointsRepository = pointsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Showcase" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("Ager", "Showcase");
            }

            ShowcaseViewModel showcaseViewModel = GenerateModel(user, false);
            showcaseViewModel.UpdateViewData(ViewData);
            return View(showcaseViewModel);
        }

        public IActionResult Ager()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Showcase/Ager" });
            }

            ShowcaseViewModel showcaseViewModel = GenerateModel(user, true);
            showcaseViewModel.UpdateViewData(ViewData);
            return View(showcaseViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Buy(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int timeSinceLastPurchase = (int)(DateTime.UtcNow - settings.TimedShowcasePurchaseDate).TotalHours;
            if (timeSinceLastPurchase < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You bought a Timed Showcase item too recently. Try again in {6 - timeSinceLastPurchase} hour{(6 - timeSinceLastPurchase == 1 ? "" : "s")}." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item item = _showcaseRepository.GetShowcaseItem();
            if (item == null || item.Id != selectionId)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item is no longer available.", ShouldRefresh = true });
            }

            GroupPower groupPower = _groupsRepository.GetGroupPower(user);
            bool hasBiggerDiscount = groupPower == GroupPower.BiggerTimedShowcaseDiscount;

            int reducedCost = (int)Math.Round(item.Points * (hasBiggerDiscount ? 0.7 : 0.75));
            item.UpdatePoints(reducedCost);
            if (user.Points < item.Points)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You don't have enough {Resources.Currency} on hand." });
            }

            settings.TimedShowcasePurchaseDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.TimedShowcasePurchaseDate), settings.TimedShowcasePurchaseDate.ToString());

            if (_pointsRepository.SubtractPoints(user, item.Points))
            {
                _itemsRepository.AddItem(user, item);
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.ShowcaseSpender))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.ShowcaseSpender);
            }
            if (item.Points >= 50000)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BigShowcaseSpender))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BigShowcaseSpender);
                }
            }

            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.Coins, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully bought the Timed Showcase item and it was added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgeItem(Guid selectionId)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item agedItem = _showcaseRepository.GetAgerItem(user);
            if (agedItem != null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You already have an item in the Ager." });
            }

            Item item = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None).Find(x => x.SelectionId == selectionId);
            if (item == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "This item no longer exists." });
            }

            if (_showcaseRepository.AddAgerItem(user, item))
            {
                _itemsRepository.RemoveItem(user, item);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{item.Name}\" was successfully added to the Ager!" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be added to the Ager." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RemoveAgerItem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Item agedItem = _showcaseRepository.GetAgerItem(user);
            if (agedItem == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You don't have an item in the Ager." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            int daysSinceAdded = (int)(DateTime.UtcNow - agedItem.AddedDate).TotalDays;
            agedItem.CreatedDate = agedItem.CreatedDate.AddDays(-7 * daysSinceAdded);
            agedItem.RepairedDate = agedItem.RepairedDate.AddDays(-7 * daysSinceAdded);

            if (_itemsRepository.AddItem(user, agedItem))
            {
                _showcaseRepository.DeleteAgerItem(agedItem);

                return Json(new Status { Success = true, SuccessMessage = $"The item \"{agedItem.Name}\" was successfully added back to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "This item could not be added back to your bag." });
        }

        [HttpGet]
        public IActionResult GetShowcasePartialView(bool isAger)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            ShowcaseViewModel showcaseViewModel = GenerateModel(user, isAger);
            if (isAger)
            {
                return PartialView("_ShowcaseAgerPartial", showcaseViewModel);
            }
            return PartialView("_ShowcaseMainPartial", showcaseViewModel);
        }

        private ShowcaseViewModel GenerateModel(User user, bool isAger)
        {
            Settings settings = _settingsRepository.GetSettings(user);
            ShowcaseViewModel showcaseViewModel = new ShowcaseViewModel
            {
                Title = "Timed Showcase",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.TimedShowcase
            };

            if (isAger)
            {
                Item item = _showcaseRepository.GetAgerItem(user);
                if (item == null)
                {
                    showcaseViewModel.Items = _itemsRepository.GetItems(user).FindAll(x => x.DeckType == DeckType.None);
                }
                else
                {
                    showcaseViewModel.Item = item;
                }
            }
            else
            {
                GroupPower groupPower = _groupsRepository.GetGroupPower(user);
                bool hasBiggerDiscount = groupPower == GroupPower.BiggerTimedShowcaseDiscount;
                Item showcaseItem = _showcaseRepository.GenerateShowcaseItem(hasBiggerDiscount);
                showcaseViewModel.Item = showcaseItem;
                showcaseViewModel.TimeLeft = Helper.GetShowcaseRefreshMinutes() - showcaseItem.GetAgeInMinutes();
                showcaseViewModel.CanBuyItem = user.Points >= showcaseItem.Points && (DateTime.UtcNow - settings.TimedShowcasePurchaseDate).TotalHours >= 6;
                showcaseViewModel.HasBiggerDiscount = hasBiggerDiscount;

                if (showcaseViewModel.TimeLeft < 0)
                {
                    showcaseViewModel.TimeLeft = Helper.GetShowcaseRefreshMinutes();
                }
            }

            return showcaseViewModel;
        }
    }
}
