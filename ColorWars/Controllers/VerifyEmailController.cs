﻿using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class VerifyEmailController : Controller
    {
        private ILoginRepository _loginRepository;
        private ISettingsRepository _settingsRepository;

        public VerifyEmailController(ILoginRepository loginRepository, ISettingsRepository settingsRepository)
        {
            _loginRepository = loginRepository;
            _settingsRepository = settingsRepository;
        }

        public IActionResult Index(string code)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null || string.IsNullOrEmpty(code))
            {
                return RedirectToAction("Index", "Home");
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VerifiedEmail)
            {
                Messages.AddSnack("You've already verified your email address.", true);
                return RedirectToAction("Index", "Home");
            }

            if (settings.EmailVerificationCode != code)
            {
                Messages.AddSnack("Your email address could not be verified.", true);
                return RedirectToAction("Index", "Home");
            }

            settings.VerifiedEmail = true;
            _settingsRepository.SetSetting(user, nameof(settings.VerifiedEmail), settings.VerifiedEmail.ToString());

            settings.EmailVerificationCode = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.EmailVerificationCode), settings.EmailVerificationCode);

            Messages.AddSnack("Your email address was successfully verified!");

            return RedirectToAction("Index", "Home");
        }
    }
}
