﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class LibraryController : Controller
    {
        private ILoginRepository _loginRepository;
        private ILibraryRepository _libraryRepository;
        private IItemsRepository _itemsRepository;
        private ISeekerRepository _seekerRepository;
        private IClubRepository _clubRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public LibraryController(ILoginRepository loginRepository, ILibraryRepository libraryRepository, IItemsRepository itemsRepository, ISeekerRepository seekerRepository,
        IClubRepository clubRepository, IPointsRepository pointsRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _libraryRepository = libraryRepository;
            _itemsRepository = itemsRepository;
            _seekerRepository = seekerRepository;
            _clubRepository = clubRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index(string tag)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Library" });
            }

            if (!string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("LibraryCards", "Library");
            }

            LibraryViewModel libraryViewModel = GenerateModel(user, true);
            libraryViewModel.UpdateViewData(ViewData);
            return View(libraryViewModel);
        }

        public IActionResult LibraryCards()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Library/LibraryCards" });
            }

            LibraryViewModel libraryViewModel = GenerateModel(user, false);
            libraryViewModel.UpdateViewData(ViewData);
            return View(libraryViewModel);
        }

        public IActionResult SecretPassage()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "Library/SecretPassage" });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            if (!_libraryRepository.CanPullLever(accomplishments))
            {
                return RedirectToAction("LibraryCards", "Library");
            }

            LibraryViewModel libraryViewModel = GenerateModel(user, false);
            libraryViewModel.UpdateViewData(ViewData);
            return View(libraryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UseKnowledge(LibraryKnowledgeType libraryKnowledgeType)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int hoursSinceLastIncreaseKnowledge = (int)(DateTime.UtcNow - settings.LastLibraryKnowledgeUseDate).TotalHours;
            if (hoursSinceLastIncreaseKnowledge < 12)
            {
                return Json(new Status { Success = false, ErrorMessage = $"You can only use your knowledge again in {12 - hoursSinceLastIncreaseKnowledge} hour{(12 - hoursSinceLastIncreaseKnowledge == 1 ? "" : "s")}." });
            }

            if (libraryKnowledgeType == LibraryKnowledgeType.None)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose a knowledge category to use." });
            }

            settings.LastLibraryKnowledgeUseDate = DateTime.UtcNow;
            _settingsRepository.SetSetting(user, nameof(settings.LastLibraryKnowledgeUseDate), settings.LastLibraryKnowledgeUseDate.ToString());

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LibraryVisit))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LibraryVisit);
            }

            Random rnd = new Random();

            bool hasGoldenKnowledge = settings.HasLibraryGoldenKnowledge;
            if (settings.HasLibraryGoldenKnowledge)
            {
                settings.HasLibraryGoldenKnowledge = false;
            }
            else if (!settings.HasLibraryGoldenKnowledge && rnd.Next(0, 5) == 1 && user.GetAgeInDays() > 1)
            {
                settings.HasLibraryGoldenKnowledge = true;
            }

            if (settings.HasLibraryGoldenKnowledge != hasGoldenKnowledge)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasLibraryGoldenKnowledge), settings.HasLibraryGoldenKnowledge.ToString());
            }

            int odds = hasGoldenKnowledge ? 5 : 0;
            int maxOdds = 10;
            if (!settings.HasLibraryGoldenKnowledge && settings.HoroscopeExpiryDate > DateTime.UtcNow)
            {
                if (settings.ActiveHoroscope == HoroscopeType.Good)
                {
                    odds = 3;
                }
                else if (settings.ActiveHoroscope == HoroscopeType.Bad)
                {
                    maxOdds = 8;
                }
            }
            int randomNumber = rnd.Next(odds, maxOdds);

            string successMessage = null;
            string infoMessage = null;
            string soundUrl = null;
            string buttonText = null;
            string buttonUrl = null;

            if (hasGoldenKnowledge)
            {
                Messages.AddSnack(IncreaseKnowledge(user, libraryKnowledgeType, settings, accomplishments));
            }

            if (randomNumber < 2)
            {
                infoMessage = $"You studied the category \"{libraryKnowledgeType}\" but nothing happened! Try again next time.";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, infoMessage);

                return Json(new Status { Success = true, InfoMessage = infoMessage });
            }
            if (randomNumber < 5)
            {
                successMessage = IncreaseKnowledge(user, libraryKnowledgeType, settings, accomplishments);
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = successMessage, SoundUrl = soundUrl });
            }

            List<Item> items;
            if (libraryKnowledgeType == LibraryKnowledgeType.Power)
            {
                if (randomNumber < 7)
                {
                    int stadiumBonusPercent = randomNumber == 4 ? rnd.Next(5, 10) : Math.Min(Math.Max(settings.LibraryPowerLevel * 2, 10), 40);
                    int stadiumBonusMinutes = randomNumber == 5 ? rnd.Next(5, 15) : rnd.Next(20, 30);

                    settings.StadiumBonusPercent = stadiumBonusPercent;
                    settings.StadiumBonusExpiryDate = DateTime.UtcNow.AddMinutes(stadiumBonusMinutes);
                    _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusPercent), settings.StadiumBonusPercent.ToString());
                    _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), settings.StadiumBonusExpiryDate.ToString());

                    successMessage = $"You successfully gained a {stadiumBonusPercent}% Stadium Bonus for CPU matches for the next {stadiumBonusMinutes} minutes!";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                    buttonText = "Head to the Stadium!";
                    buttonUrl = "/Stadium";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                }
                if (randomNumber < 8)
                {
                    items = _itemsRepository.GetItems(user).FindAll(x => x.TotalUses > 1 && x.Uses <= 5);
                    if (items.Count == 0)
                    {
                        randomNumber = 8;
                    }
                    else
                    {
                        Item item = items[rnd.Next(items.Count)];
                        int extraUses = rnd.Next(2, Math.Min(Math.Max(settings.LibraryPowerLevel, 4), 10));

                        item.Uses += extraUses;
                        _itemsRepository.UpdateItem(user, item);

                        successMessage = $"Your item \"{item.Name}\" successfully gained {extraUses} more uses!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }
                if (randomNumber < 9)
                {
                    items = _itemsRepository.GetItems(user).FindAll(x => x.Condition > ItemCondition.New);
                    if (items.Count == 0)
                    {
                        randomNumber = 9;
                    }
                    else
                    {
                        Item item = items[rnd.Next(items.Count)];
                        item.RepairedDate = DateTime.UtcNow;
                        _itemsRepository.UpdateItem(user, item);

                        successMessage = $"Your item \"{item.Name}\" was successfully repaired and now has a New condition!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }

                items = _itemsRepository.GetItems(user).FindAll(x => x.Uses < x.TotalUses);
                if (items.Count == 0)
                {
                    settings.TrainingTokens++;
                    _settingsRepository.SetSetting(user, nameof(settings.TrainingTokens), settings.TrainingTokens.ToString());

                    successMessage = $"You successfully gained 1 Training Token!";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                    buttonText = "Head to Training Center!";
                    buttonUrl = "/TrainingCenter";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                }
                else
                {
                    Item item = items[rnd.Next(items.Count)];
                    item.Uses = item.TotalUses;
                    _itemsRepository.UpdateItem(user, item);

                    successMessage = $"Your item \"{item.Name}\" successfully restored all of its original total uses!";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                }
            }
            if (libraryKnowledgeType == LibraryKnowledgeType.Opportunity)
            {
                if (randomNumber < 6)
                {
                    if (settings.MaxBagSize >= Helper.GetMaxBagSize())
                    {
                        randomNumber = 6;
                    }
                    else
                    {
                        settings.MaxBagSize++;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxBagSize), settings.MaxBagSize.ToString());

                        successMessage = $"Your maximum bag size was successfully increased to {settings.MaxBagSize} items!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }
                if (randomNumber < 7)
                {
                    if (settings.MaxMarketStallSize >= Helper.GetMaxStallSize())
                    {
                        randomNumber = 7;
                    }
                    else
                    {
                        settings.MaxMarketStallSize++;
                        _settingsRepository.SetSetting(user, nameof(settings.MaxMarketStallSize), settings.MaxMarketStallSize.ToString());

                        successMessage = $"Your maximum Market Stall size was successfully increased to {settings.MaxMarketStallSize} items!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = "Head to your Market Stall!";
                        buttonUrl = "/MarketStalls/Create";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }
                if (randomNumber < 8)
                {
                    settings.DividendsCollectDate = DateTime.UtcNow.AddDays(-1);
                    _settingsRepository.SetSetting(user, nameof(settings.DividendsCollectDate), settings.DividendsCollectDate.ToString());

                    successMessage = $"You can successfully collect your daily dividends from the Stock Market again if you haven't already!";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                    buttonText = "Head to your Portfolio!";
                    buttonUrl = "/StockMarket/Portfolio";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl });
                }

                items = _itemsRepository.GetItems(user);
                if (items.Count >= settings.MaxBagSize)
                {
                    infoMessage = $"You studied the category \"{libraryKnowledgeType}\" but nothing happened! Try again next time.";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, infoMessage);

                    return Json(new Status { Success = true, InfoMessage = infoMessage });
                }

                if (randomNumber == 8)
                {
                    List<SeekerChallenge> seekerItems = _seekerRepository.GetSeekerChallenges(user);
                    if (seekerItems.Count > 0)
                    {
                        List<SeekerChallenge> uncompletedSeekerItems = seekerItems.FindAll(x => !(items.Select(y => y.Id).Contains(x.ItemId)));
                        if (uncompletedSeekerItems.Count > 0)
                        {
                            List<Item> allItems = _itemsRepository.GetAllItems().FindAll(x => x.Points <= settings.LibraryOpportunityLevel * 10000);
                            Item seekerItem = allItems.Find(x => x.Id == uncompletedSeekerItems.First().ItemId);
                            if (seekerItem != null)
                            {
                                _itemsRepository.AddItem(user, seekerItem);

                                successMessage = $"The Seeker requested the item \"{seekerItem.Name}\" and it was successfully added to your bag!";
                                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                                buttonText = "Head to the Seeker!";
                                buttonUrl = "/Seeker";
                                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                                return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                            }
                        }
                    }
                }

                if (rnd.Next(Math.Max(100 - (2 * settings.LibraryOpportunityLevel), 50)) == 1)
                {
                    if (_clubRepository.GetCurrentMembership(user) == null)
                    {
                        _clubRepository.AddClubMembership(user, 1, DateTime.UtcNow);

                        successMessage = $"You successfully gained a free month of {Resources.SiteName} Club membership!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = $"Head to the {Resources.SiteName} Club!";
                        buttonUrl = "/Club";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }

                if (rnd.Next(2) == 1)
                {
                    List<Item> frozenItems = _itemsRepository.GetAllItems(ItemCategory.Frozen);
                    Item frozenItem = settings.LibraryOpportunityLevel < 10 ? frozenItems[0] : frozenItems[1];

                    _itemsRepository.AddItem(user, frozenItem);

                    successMessage = $"The item \"{frozenItem.Name}\" was successfully added to your bag!";
                    _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                    buttonText = "Head to your bag!";
                    buttonUrl = "/Items";
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                }

                List<Item> allCardPacks = _itemsRepository.GetAllItems(ItemCategory.Pack);
                Item item;
                if (settings.LibraryOpportunityLevel < 10)
                {
                    item = allCardPacks.First();
                }
                else if (settings.LibraryOpportunityLevel < 50 || rnd.Next(0, 10) != 1)
                {
                    item = allCardPacks[rnd.Next(0, 2)];
                }
                else
                {
                    item = allCardPacks[rnd.Next(allCardPacks.Count)];
                }

                _itemsRepository.AddItem(user, item);

                successMessage = $"A Card Pack was successfully added to your bag!";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                buttonText = "Head to your bag!";
                buttonUrl = "/Items";
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
            }
            if (libraryKnowledgeType == LibraryKnowledgeType.Wealth)
            {
                if (randomNumber < 8)
                {
                    items = _itemsRepository.GetItems(user);
                    if (items.Count >= settings.MaxBagSize)
                    {
                        randomNumber = 8;
                    }
                    else
                    {
                        List<Item> allItems = _itemsRepository.GetAllItems().FindAll(x => x.Points <= 1000 * settings.LibraryWealthLevel && x.Points >= 500);
                        Item item = allItems[rnd.Next(allItems.Count)];
                        _itemsRepository.AddItem(user, item);

                        successMessage = $"The item \"{item.Name}\" was successfully added to your bag!";
                        _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                        soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                        return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl, SoundUrl = soundUrl });
                    }
                }

                int points = rnd.Next(0, 50) == 1 ? 25000 : rnd.Next(50, Math.Min(settings.LibraryWealthLevel * 100, 5000));
                _pointsRepository.AddPoints(user, points);

                successMessage = $"You successfully gained {Helper.GetFormattedPoints(points)}!";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);
                soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                return Json(new Status { Success = true, SuccessMessage = successMessage, InfoMessage = infoMessage, Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
            }

            infoMessage = $"You studied the category \"{libraryKnowledgeType}\" but nothing happened! Try again next time.";
            _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, infoMessage);

            return Json(new Status { Success = true, InfoMessage = infoMessage, ButtonText = buttonText, ButtonUrl = buttonUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RedeemLibraryCards()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<LibraryCard> libraryCards = _itemsRepository.GetLibraryCards(user);
            List<IGrouping<CardColor, LibraryCard>> colors = libraryCards.GroupBy(x => x.Color).ToList();
            if (colors.Count < 6)
            {
                return Json(new Status { Success = false, ErrorMessage = "You do not have a full set of Library Cards." });
            }

            foreach (IGrouping<CardColor, LibraryCard> colorGrouping in colors)
            {
                LibraryCard card = colorGrouping.First();
                _itemsRepository.RemoveItem(user, card);
            }

            _pointsRepository.AddPoints(user, 150000);

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LibraryCardsCollector))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LibraryCardsCollector);
            }

            Settings settings = _settingsRepository.GetSettings(user);
            string soundUrl = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

            return Json(new Status { Success = true, SuccessMessage = $"You successfully redeemed your set of Library Cards and earned {Helper.GetFormattedPoints(150000)}!", Points = Helper.GetFormattedPoints(user.Points), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PullLever()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);

            if (!_libraryRepository.CanPullLever(accomplishments))
            {
                return Json(new Status { Success = false, ErrorMessage = "You aren't able to pull this lever yet." });
            }

            return Json(new Status { Success = true, ReturnUrl = "/Library/SecretPassage" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TakeUnknownItem()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't defeated The Unknown yet." });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.HasTakenUnknownItem)
            {
                return Json(new Status { Success = false, ErrorMessage = "You've already taken this item." });
            }

            List<Item> items = _itemsRepository.GetItems(user);
            if (items.Count >= settings.MaxBagSize)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your bag is too full to add another item." });
            }

            Item unknownItem = GetUnknownItem();
            if (_itemsRepository.AddItem(user, unknownItem))
            {
                settings.HasTakenUnknownItem = true;
                _settingsRepository.SetSetting(user, nameof(settings.HasTakenUnknownItem), settings.HasTakenUnknownItem.ToString());

                return Json(new Status { Success = true, SuccessMessage = $"You successfully took the item \"{unknownItem.Name}\" and it was added to your bag!", ButtonText = "Head to your bag!", ButtonUrl = "/Items" });
            }

            return Json(new Status { Success = false, ErrorMessage = "The item couldn't be taken." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendVoidMessage(string message)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't defeated The Unknown yet." });
            }

            if (string.IsNullOrEmpty(message))
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message cannot be empty." });
            }

            if (message.Length > 350)
            {
                return Json(new Status { Success = false, ErrorMessage = "Your message cannot be greater than 350 characters." });
            }

            if (_libraryRepository.AddVoidMessage(user, message))
            {
                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "Your message could not be added to The Void." });
        }

        [HttpGet]
        public IActionResult GetLibraryPartialView(bool isLibrary)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            LibraryViewModel libraryViewModel = GenerateModel(user, isLibrary);
            if (isLibrary)
            {
                return PartialView("_LibraryMainPartial", libraryViewModel);
            }
            return PartialView("_LibraryCardsPartial", libraryViewModel);
        }

        [HttpGet]
        public IActionResult GetLibrarySecretPassagePartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated))
            {
                return Json(new Status { Success = false, ErrorMessage = "You haven't defeated The Unknown yet." });
            }

            LibraryViewModel libraryViewModel = GenerateModel(user, false);

            return PartialView("_LibrarySecretPassagePartial", libraryViewModel);
        }

        private LibraryViewModel GenerateModel(User user, bool isLibrary)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            LibraryViewModel libraryViewModel = new LibraryViewModel
            {
                Title = "Library",
                User = user,
                Parent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Library,
                PowerLevel = settings.LibraryPowerLevel,
                OpportunityLevel = settings.LibraryOpportunityLevel,
                WealthLevel = settings.LibraryWealthLevel,
                CanUseLibrary = (DateTime.UtcNow - settings.LastLibraryKnowledgeUseDate).TotalHours >= 12
            };

            if (isLibrary)
            {
                libraryViewModel.HasGoldenKnowledge = settings.HasLibraryGoldenKnowledge && libraryViewModel.CanUseLibrary;
                libraryViewModel.LibraryHistories = _libraryRepository.GetLibraryHistory(user);
            }
            else
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                libraryViewModel.LibraryCards = _itemsRepository.GetLibraryCards(user);
                libraryViewModel.CanOpenPassage = accomplishments.Any(x => x.Id == AccomplishmentId.LibraryCardsCollector);
                libraryViewModel.CanPullLever = _libraryRepository.CanPullLever(accomplishments);
                libraryViewModel.CanSeeUnknown = !accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated);
                libraryViewModel.HasTakenUnknownItem = settings.HasTakenUnknownItem;

                if (libraryViewModel.CanPullLever)
                {
                    if (libraryViewModel.CanSeeUnknown)
                    {
                        List<Card> deck = _itemsRepository.GetCards(user, true);
                        libraryViewModel.Deck = deck;
                        libraryViewModel.CanChallengeUnknown = (DateTime.UtcNow - settings.LastUnknownStadiumChallengeDate).TotalMinutes >= 30 && deck.Count == Helper.GetMaxDeckSize() && deck.All(x => x.Condition < ItemCondition.Unusable);
                    }
                    else
                    {
                        libraryViewModel.VoidMessages = _libraryRepository.GetVoidMessages();
                        if (!libraryViewModel.HasTakenUnknownItem)
                        {
                            libraryViewModel.UnknownItem = GetUnknownItem();
                        }
                    }
                }
            }

            return libraryViewModel;
        }

        private void AddLevelAccomplishment(User user, int level, List<Accomplishment> accomplishments)
        {
            if (level >= 10)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LibraryLevelTen))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LibraryLevelTen);
                }
            }
            if (level >= 50)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LibraryLevelFifty))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LibraryLevelFifty);
                }
            }
            if (level >= 100)
            {
                if (!accomplishments.Exists(x => x.Id == AccomplishmentId.LibraryLevelOneHundred))
                {
                    _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.LibraryLevelOneHundred);
                }
            }
        }

        private string IncreaseKnowledge(User user, LibraryKnowledgeType libraryKnowledgeType, Settings settings, List<Accomplishment> accomplishments)
        {
            string successMessage = string.Empty;
            if (libraryKnowledgeType == LibraryKnowledgeType.Power)
            {
                settings.LibraryPowerLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LibraryPowerLevel), settings.LibraryPowerLevel.ToString());

                successMessage = $"Your Power Knowledge successfully increased to Level {Helper.FormatNumber(settings.LibraryPowerLevel)}!";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);

                AddLevelAccomplishment(user, settings.LibraryPowerLevel, accomplishments);
            }
            else if (libraryKnowledgeType == LibraryKnowledgeType.Opportunity)
            {
                settings.LibraryOpportunityLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LibraryOpportunityLevel), settings.LibraryOpportunityLevel.ToString());

                successMessage = $"Your Opportunity Knowledge successfully increased to Level {Helper.FormatNumber(settings.LibraryOpportunityLevel)}!";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);

                AddLevelAccomplishment(user, settings.LibraryOpportunityLevel, accomplishments);
            }
            else if (libraryKnowledgeType == LibraryKnowledgeType.Wealth)
            {
                settings.LibraryWealthLevel++;
                _settingsRepository.SetSetting(user, nameof(settings.LibraryWealthLevel), settings.LibraryWealthLevel.ToString());

                successMessage = $"Your Wealth Knowledge successfully increased to Level {Helper.FormatNumber(settings.LibraryWealthLevel)}!";
                _libraryRepository.AddLibraryHistory(user, libraryKnowledgeType, successMessage);

                AddLevelAccomplishment(user, settings.LibraryWealthLevel, accomplishments);
            }

            return successMessage;
        }

        private Item GetUnknownItem()
        {
            return _itemsRepository.GetAllItems(ItemCategory.Enhancer, null, true).Find(x => x.IsExclusive && x.Name.Equals("x20", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
