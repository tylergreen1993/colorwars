﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ColorWars.Controllers
{
    public class DoubtItController : Controller
    {
        private ILoginRepository _loginRepository;
        private IPointsRepository _pointsRepository;
        private IDoubtItRepository _doubtItRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISoundsRepository _soundsRepository;

        public DoubtItController(ILoginRepository loginRepository, IPointsRepository pointsRepository,
        IDoubtItRepository doubtItRepository, ISettingsRepository settingsRepository, IAccomplishmentsRepository accomplishmentsRepository,
        ISoundsRepository soundsRepository)
        {
            _loginRepository = loginRepository;
            _pointsRepository = pointsRepository;
            _doubtItRepository = doubtItRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _soundsRepository = soundsRepository;
        }

        public IActionResult Index()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return RedirectToAction("Index", "Login", new { ru = "DoubtIt" });
            }

            DoubtItViewModel doubtItViewModel = GenerateModel(user);
            doubtItViewModel.UpdateViewData(ViewData);
            return View(doubtItViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PlayCards(Guid card1, Guid card2, Guid card3, Guid card4, int chosenAmount)
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DoubtItTurn != DoubtItOwner.You)
            {
                return Json(new Status { Success = false, ErrorMessage = "It's not currently your turn." });
            }

            List<DoubtItCard> allDoubtItCards = GetDoubtItCards(user);
            List<DoubtItCard> doubtItCards = allDoubtItCards.FindAll(x => x.Id == card1 || x.Id == card2 || x.Id == card3 || x.Id == card4);
            if (doubtItCards.Count == 0)
            {
                return Json(new Status { Success = false, ErrorMessage = "You must choose at least one card." });
            }

            List<int> possibleAmounts = GetPossibleAmounts(GetLastMove(settings.DoubtItLastPlayed));
            if (!possibleAmounts.Any(x => x == chosenAmount))
            {
                return Json(new Status { Success = false, ErrorMessage = "You didn't choose a valid amount." });
            }

            foreach (DoubtItCard doubtItCard in doubtItCards)
            {
                doubtItCard.Owner = DoubtItOwner.Pile;
            }
            _doubtItRepository.UpdateDoubtItCards(doubtItCards);

            if (!string.IsNullOrEmpty(settings.DoubtItCheatMessage))
            {
                settings.DoubtItCheatMessage = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);
            }

            allDoubtItCards = GetDoubtItCards(user);
            if (CheckForCheat(allDoubtItCards, DoubtItOwner.You, chosenAmount, doubtItCards.Count, out DoubtItOwner callCheat, out bool isCheat))
            {
                List<DoubtItCard> pile = allDoubtItCards.FindAll(x => x.Owner == DoubtItOwner.Pile);

                if (isCheat)
                {
                    foreach (DoubtItCard doubtItCard in pile)
                    {
                        doubtItCard.Owner = DoubtItOwner.You;
                    }

                    settings.DoubtItCheatMessage = $"{callCheat.GetDisplayName()} called you out for cheating! The pile was added to your hand!";
                    settings.DoubtItCheatMessageSuccess = false;
                }
                else
                {
                    foreach (DoubtItCard doubtItCard in pile)
                    {
                        doubtItCard.Owner = callCheat;
                    }

                    settings.DoubtItCheatMessage = $"{callCheat.GetDisplayName()} called you out for cheating but they were wrong! The pile was added to their hand!";
                }

                _doubtItRepository.UpdateDoubtItCards(pile);

                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessageSuccess), settings.DoubtItCheatMessageSuccess.ToString());
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);

                settings.DoubtItLastPlayed = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);
            }
            else
            {
                settings.DoubtItLastPlayed = $"{doubtItCards.Count}-{chosenAmount}";
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);
            }

            settings.DoubtItTurn = DoubtItOwner.Opponent1;
            _settingsRepository.SetSetting(user, nameof(settings.DoubtItTurn), settings.DoubtItTurn.ToString());

            allDoubtItCards = GetDoubtItCards(user);
            DoubtItOwner winner = CheckForWinner(allDoubtItCards, settings.DoubtItTurn);

            string soundUrl = string.Empty;

            if (winner != DoubtItOwner.Pile)
            {
                if (winner == DoubtItOwner.You)
                {
                    _pointsRepository.AddPoints(user, 1000);

                    settings.DoubtItWins++;
                    _settingsRepository.SetSetting(user, nameof(settings.DoubtItWins), settings.DoubtItWins.ToString());

                    soundUrl  = _soundsRepository.GetSoundUrl(SoundType.SuccessShort, settings);

                    List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DoubtItWinner))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DoubtItWinner);
                    }
                    if (settings.DoubtItWins >= 5)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DoubtItChampion))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DoubtItChampion);
                        }
                    }
                    if (settings.DoubtItWins >= 10)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DoubtItTenWins))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DoubtItTenWins);
                        }
                    }
                }
            }
            else
            {
                PlayCards(user, allDoubtItCards, settings, settings.DoubtItTurn, GetPossibleAmounts(GetLastMove(settings.DoubtItLastPlayed)));
            }

            return Json(new Status { Success = true, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Doubt()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DoubtItTurn == DoubtItOwner.You)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't doubt yourself." });
            }

            DoubtItOwner currentTurn = settings.DoubtItTurn;
            (int amount, int type) lastMove = GetLastMove(settings.DoubtItLastPlayed);

            List<DoubtItCard> allCards = _doubtItRepository.GetDoubtItCards(user);
            List<DoubtItCard> pile = allCards.FindAll(x => x.Owner == DoubtItOwner.Pile);
            List<DoubtItCard> lastPlayedCards = _doubtItRepository.GetLastPlayedCards(allCards, DoubtItOwner.Pile);

            if (lastPlayedCards.Count != lastMove.amount || lastPlayedCards.Any(x => x.Amount != lastMove.type))
            {
                foreach (DoubtItCard doubtItCard in pile)
                {
                    doubtItCard.Owner = currentTurn;
                }

                settings.DoubtItCheatMessage = $"You successfully called out {currentTurn.GetDisplayName()} for cheating and earned {Helper.GetFormattedPoints(25)}! The pile was added to their hand!";
                settings.DoubtItCheatMessageSuccess = true;

                _pointsRepository.AddPoints(user, 25);
            }
            else
            {
                foreach (DoubtItCard doubtItCard in pile)
                {
                    doubtItCard.Owner = DoubtItOwner.You;
                }

                settings.DoubtItCheatMessage = $"You called out {currentTurn.GetDisplayName()} for cheating but they weren't! The pile was added to your hand!";
                settings.DoubtItCheatMessageSuccess = false;
            }

            _doubtItRepository.UpdateDoubtItCards(pile);

            _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessageSuccess), settings.DoubtItCheatMessageSuccess.ToString());
            _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);

            settings.DoubtItLastPlayed = string.Empty;
            _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);

            settings.DoubtItTurn = currentTurn == DoubtItOwner.Opponent1 ? DoubtItOwner.Opponent2 : currentTurn == DoubtItOwner.Opponent2 ? DoubtItOwner.Opponent3 : DoubtItOwner.You;
            _settingsRepository.SetSetting(user, nameof(settings.DoubtItTurn), settings.DoubtItTurn.ToString());

            string soundUrl = string.Empty;

            if (settings.DoubtItTurn != DoubtItOwner.You)
            {
                allCards = GetDoubtItCards(user);
                if (CheckForWinner(allCards, settings.DoubtItTurn) == DoubtItOwner.Pile)
                {
                    PlayCards(user, allCards, settings, settings.DoubtItTurn, GetPossibleAmounts(GetLastMove(settings.DoubtItLastPlayed)));
                }
                else
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);
                }
            }

            return Json(new Status { Success = true, Points = user.GetFormattedPoints(), SoundUrl = soundUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NextTurn()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            List<DoubtItCard> allCards = _doubtItRepository.GetDoubtItCards(user);

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.DoubtItTurn == DoubtItOwner.You)
            {
                return Json(new Status { Success = false, ErrorMessage = "You can't let yourself slide." });
            }

            DoubtItOwner currentTurn = settings.DoubtItTurn;
            (int amount, int type) lastMove = GetLastMove(settings.DoubtItLastPlayed);

            if (!string.IsNullOrEmpty(settings.DoubtItCheatMessage))
            {
                settings.DoubtItCheatMessage = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);
            }

            if (CheckForCheat(allCards, settings.DoubtItTurn, lastMove.type, lastMove.amount, out DoubtItOwner callCheat, out bool isCheat))
            {
                List<DoubtItCard> pile = allCards.FindAll(x => x.Owner == DoubtItOwner.Pile);

                if (isCheat)
                {
                    foreach (DoubtItCard doubtItCard in pile)
                    {
                        doubtItCard.Owner = currentTurn;
                    }

                    settings.DoubtItCheatMessage = $"{callCheat.GetDisplayName()} called out {currentTurn.GetDisplayName()} for cheating! The pile was added to their hand!";
                    settings.DoubtItCheatMessageSuccess = true;
                }
                else
                {
                    foreach (DoubtItCard doubtItCard in pile)
                    {
                        doubtItCard.Owner = callCheat;
                    }

                    settings.DoubtItCheatMessage = $"{callCheat.GetDisplayName()} called out {currentTurn.GetDisplayName()} for cheating but they were wrong! The pile was added to their hand!";
                    settings.DoubtItCheatMessageSuccess = false;
                }

                _doubtItRepository.UpdateDoubtItCards(pile);

                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessageSuccess), settings.DoubtItCheatMessageSuccess.ToString());
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);

                settings.DoubtItLastPlayed = string.Empty;
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);

                allCards = GetDoubtItCards(user);
            }

            settings.DoubtItTurn = currentTurn == DoubtItOwner.Opponent1 ? DoubtItOwner.Opponent2 : currentTurn == DoubtItOwner.Opponent2 ? DoubtItOwner.Opponent3 : DoubtItOwner.You;
            _settingsRepository.SetSetting(user, nameof(settings.DoubtItTurn), settings.DoubtItTurn.ToString());

            string soundUrl = string.Empty;

            if (settings.DoubtItTurn != DoubtItOwner.You)
            {
                if (CheckForWinner(allCards, settings.DoubtItTurn) == DoubtItOwner.Pile)
                {
                    PlayCards(user, allCards, settings, settings.DoubtItTurn, GetPossibleAmounts(GetLastMove(settings.DoubtItLastPlayed)));
                }
                else
                {
                    soundUrl = _soundsRepository.GetSoundUrl(SoundType.NegativeShort, settings);
                }
            }

            return Json(new Status { Success = true, SoundUrl = soundUrl });
        }

        [HttpPost]
        public IActionResult NewGame()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            if (Helper.IsThrottled(user))
            {
                return Json(new Status { Success = false, ErrorMessage = Resources.ThrottledMessage });
            }

            if (_doubtItRepository.DeleteDoubtItCards(user))
            {
                Settings settings = _settingsRepository.GetSettings(user);
                settings.DoubtItCheatMessage = string.Empty;
                settings.DoubtItLastPlayed = string.Empty;
                settings.DoubtItTurn = DoubtItOwner.You;

                _settingsRepository.SetSetting(user, nameof(settings.DoubtItCheatMessage), settings.DoubtItCheatMessage);
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);
                _settingsRepository.SetSetting(user, nameof(settings.DoubtItTurn), settings.DoubtItTurn.ToString());

                return Json(new Status { Success = true });
            }

            return Json(new Status { Success = false, ErrorMessage = "You could not start a new game. Please try again." });
        }

        [HttpGet]
        public IActionResult GetDoubtItPartialView()
        {
            User user = _loginRepository.AuthenticateUser();
            if (user == null)
            {
                return Json(new Status { Success = false, ErrorMessage = "You are not logged in." });
            }

            DoubtItViewModel doubtItViewModel = GenerateModel(user);
            return PartialView("_DoubtItMainPartial", doubtItViewModel);
        }

        private DoubtItViewModel GenerateModel(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user);

            List<DoubtItCard> doubtItCards = GetDoubtItCards(user);
            List<DoubtItOwner> owners = Enum.GetValues(typeof(DoubtItOwner)).Cast<DoubtItOwner>().ToList();
            owners.Remove(DoubtItOwner.Pile);

            DoubtItViewModel doubtItViewModel = new DoubtItViewModel
            {
                Title = "Doubt It",
                User = user,
                Parent = LocationArea.Fair.ToString(),
                TopParent = LocationArea.Plaza.ToString(),
                LocationId = LocationId.Fair,
                DoubtItCards = doubtItCards,
                UserCards = doubtItCards.FindAll(x => x.Owner == DoubtItOwner.You).OrderBy(x => x.Amount).ToList(),
                CurrentTurn = settings.DoubtItTurn,
                LastMove = GetLastMove(settings.DoubtItLastPlayed),
                Owners = owners,
                Winner = CheckForWinner(doubtItCards, settings.DoubtItTurn),
                DoubtItMessage = settings.DoubtItCheatMessage,
                DoubtItMessageSuccess = settings.DoubtItCheatMessageSuccess,
                TotalWins = settings.DoubtItWins
            };

            if (doubtItViewModel.CurrentTurn == DoubtItOwner.You)
            {

                doubtItViewModel.PossibleAmounts = GetPossibleAmounts(doubtItViewModel.LastMove);
            }

            return doubtItViewModel;
        }

        private List<DoubtItCard> GetDoubtItCards(User user)
        {
            List<DoubtItCard> doubtItCards = _doubtItRepository.GetDoubtItCards(user);
            if (doubtItCards.Count == 0)
            {
                List<int> amounts = new List<int>();
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        amounts.Add(i + 1);
                    }
                }

                amounts.Shuffle();

                List<DoubtItOwner> owners = Enum.GetValues(typeof(DoubtItOwner)).Cast<DoubtItOwner>().ToList();
                owners.Remove(DoubtItOwner.Pile);

                foreach(DoubtItOwner owner in owners)
                {
                    List<int> cards = amounts.Take(10).ToList();
                    amounts.RemoveRange(0, 10);
                    _doubtItRepository.AddDoubtItCards(user, cards, owner);
                }

                return _doubtItRepository.GetDoubtItCards(user);
            }

            return doubtItCards;
        }

        private (int, int) GetLastMove(string lastPlayed)
        {
            if (string.IsNullOrEmpty(lastPlayed))
            {
                return (0, 0);
            }

            List<int> lastPlayedDetails = lastPlayed.Split("-").Select(x => int.Parse(x)).ToList();
            return (lastPlayedDetails[0], lastPlayedDetails[1]);
        }

        private List<int> GetPossibleAmounts((int amount, int type) lastMove)
        {
            List<int> possibleAmounts = new List<int>();
            if (lastMove == (0, 0))
            {
                for (int i = 0; i < 10; i++)
                {
                    possibleAmounts.Add(i + 1);
                }
            }
            else
            {
                if (lastMove.type > 1 && lastMove.type < 10)
                {
                    possibleAmounts.Add(lastMove.type - 1);
                    possibleAmounts.Add(lastMove.type);
                    possibleAmounts.Add(lastMove.type + 1);
                }
                else if (lastMove.type == 1)
                {
                    possibleAmounts.Add(10);
                    possibleAmounts.Add(lastMove.type);
                    possibleAmounts.Add(lastMove.type + 1);
                }
                else
                {
                    possibleAmounts.Add(lastMove.type - 1);
                    possibleAmounts.Add(lastMove.type);
                    possibleAmounts.Add(1);
                }
            }

            return possibleAmounts;
        }

        private bool CheckForCheat(List<DoubtItCard> allCards, DoubtItOwner playedOwner, int chosenAmount, int count, out DoubtItOwner callCheat, out bool isCheat)
        {
            callCheat = DoubtItOwner.Pile;
            isCheat = false;

            List<DoubtItOwner> owners = Enum.GetValues(typeof(DoubtItOwner)).Cast<DoubtItOwner>().ToList();
            owners.Remove(DoubtItOwner.Pile);
            owners.Remove(playedOwner);
            owners.Remove(DoubtItOwner.You);

            List<DoubtItCard> pileCards = allCards.FindAll(x => x.Owner == DoubtItOwner.Pile).OrderBy(x => x.AddedDate).ToList();
            pileCards = pileCards.Take(pileCards.Count - count).ToList();

            Random rnd = new Random();

            foreach (DoubtItOwner owner in owners)
            {
                List<DoubtItCard> cards = allCards.FindAll(x => x.Owner == owner && x.Amount == chosenAmount);
                List<DoubtItCard> matchingPileCards = pileCards.FindAll(x => x.Amount == chosenAmount);
                if (cards.Count + matchingPileCards.Count + count > 4 && (cards.Count + count > 4 || rnd.Next(5) <= 3))
                {
                    callCheat = owner;
                    isCheat = true;
                    return true;
                }
            }

            if (pileCards.Count >= 5 && rnd.Next(count > 2 ? 3 : 5) == 1)
            {
                callCheat = owners[rnd.Next(owners.Count)];
                isCheat = !allCards.FindAll(x => x.Owner == DoubtItOwner.Pile).OrderBy(x => x.AddedDate).TakeLast(count).All(x => x.Amount == chosenAmount);

                return true;
            }

            int remainingCards = allCards.Count(x => x.Owner == playedOwner);
            if (remainingCards == 0)
            {
                callCheat = owners[rnd.Next(owners.Count)];
                isCheat = !allCards.FindAll(x => x.Owner == DoubtItOwner.Pile).OrderBy(x => x.AddedDate).TakeLast(count).All(x => x.Amount == chosenAmount);

                return true;
            }

            return false;
        }

        private void PlayCards(User user, List<DoubtItCard> allCards, Settings settings, DoubtItOwner playedOwner, List<int> possibleAmounts)
        {
            if (playedOwner == DoubtItOwner.You || playedOwner == DoubtItOwner.Pile)
            {
                return;
            }

            List<DoubtItCard> cards = allCards.FindAll(x => x.Owner == playedOwner);
            List<DoubtItCard> validCards = cards.FindAll(x => possibleAmounts.Any(y => y == x.Amount));
            List<DoubtItCard> cardsToPlay;

            if (validCards.Count > 0)
            {
                IOrderedEnumerable<IGrouping<int, DoubtItCard>> groupedValidCards = validCards.GroupBy(x => x.Amount).OrderByDescending(x => x.Count());
                cardsToPlay = groupedValidCards.First().ToList();
                settings.DoubtItLastPlayed = $"{cardsToPlay.Count}-{cardsToPlay.First().Amount}";
            }
            else
            {
                Random rnd = new Random();
                int cardsAmountToPlay = Math.Min(cards.Count, rnd.Next(4) + 1);
                cardsToPlay = cards.Take(cardsAmountToPlay).ToList();
                settings.DoubtItLastPlayed = $"{cardsAmountToPlay}-{possibleAmounts[rnd.Next(possibleAmounts.Count)]}";
            }

            foreach (DoubtItCard doubtItCard in cardsToPlay)
            {
                doubtItCard.Owner = DoubtItOwner.Pile;
            }
            _doubtItRepository.UpdateDoubtItCards(cardsToPlay);

            _settingsRepository.SetSetting(user, nameof(settings.DoubtItLastPlayed), settings.DoubtItLastPlayed);
        }

        private DoubtItOwner CheckForWinner(List<DoubtItCard> cards, DoubtItOwner currentTurn)
        {
            List<DoubtItOwner> owners = Enum.GetValues(typeof(DoubtItOwner)).Cast<DoubtItOwner>().ToList();
            owners.Remove(DoubtItOwner.Pile);
            owners.Remove(currentTurn);
            foreach (DoubtItOwner owner in owners)
            {
                if (!cards.Any(x => x.Owner == owner))
                {
                    return owner;
                }
            }

            return DoubtItOwner.Pile;
        }
    }
}
