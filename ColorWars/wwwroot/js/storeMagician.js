﻿function searchStoreMagician() {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/StoreMagician?searchQuery=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};