﻿function updateSecretShrineForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshSecretShrinePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshSecretShrinePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SecretShrine/GetSecretShrinePartialView",
        cache: false,
        success: function(data)
        {
            $("#secretShrinePartialView").html(data);
            onPageLoad();
        }
    });
}