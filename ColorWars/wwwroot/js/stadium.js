﻿var stadiumPageChanged = false;
var roundPowerIndex = 0;
var hasSeenEnhancersTutorial = false;
var hasSeenPowerMovesTutorial = false;
var successAudio = null;
var negativeAudio = null;

$(document).ready(function(){
    onStadiumLoad();
});

function onStadiumLoad(showModals = true) {
    stadiumPageChanged = !stadiumPageChanged;
    roundPowerIndex = 0;
    var secondsLeft = $('#match-secondsLeft').text();
    if (secondsLeft){
        countdownNumber(secondsLeft, stadiumPageChanged);
    }
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "true"){
        pollForOpponentsMove();
    }
    var shouldPollForOpponentWaitlist = $('#shouldPollForOpponentWaitlist');
    if (shouldPollForOpponentWaitlist && shouldPollForOpponentWaitlist.val() === "true"){
        pollForOpponentWaitlist(0, $('#pollForOpponentUsername').val());
        updateStadiumTip();
    }
    if ($('.stadium-counter').length) {
        $('#match-result').hide();
        $('#stadiumForm-duel').hide();
        $('#enhancers').hide();
        $('#power-moves').hide();
        $('#bonuses').hide();
        $('#bonuses-opponent').hide();
        if ($('#opponentRoundPower').length) {
            setTimeout(function () {
                $('#round-power').fadeIn(500, function () {
                    beginStadiumAnimation();
                });
            }, 500);
        }
    }
    if ($('.global-ranking-counter').length) {
        $('.global-ranking-counter').each(function() {
            var counter = this;
            var triggerAtY = $(counter).offset().top - $(window).outerHeight();
            var countTo = $(counter).attr('data-count');
            var countFrom = $(counter).attr('data-start-count');
            if (countTo == countFrom) {
                $(counter).text(countTo);
            }
            else {
                if (triggerAtY <= $(window).scrollTop()) {
                    countToNumber($(counter), countTo, countFrom);
                }
                else {
                    $(window).scroll(function(event) {
                        triggerAtY = $(counter).offset().top - $(window).outerHeight();
                        if (triggerAtY > $(window).scrollTop()) {
                            return;
                        }
                        countToNumber($(counter), countTo, countFrom);
                        $(this).off(event);
                    });
                }
            }
        });
    }
    if ($('.stadium-message').length) {
        $('.stadium-message').each(function () {
            $(this).hide();
        });
    }
    if ($('.global-level-counter').length) {
        $('#level-up').hide().slideDown(500);
        $('.global-level-counter').each(function () {
            var counter = this;
            var countTo = $(counter).attr('data-count');
            setTimeout(function () {
                countToNumber($(counter), countTo, 1, "", hideStadiumLevelUp);
            }, 250);
        });
    }
    else {
        setTimeout(function () {
            showStadiumFinalMessages();
        }, 350);
    }

    var tutorialWaitTime = 0;
    if ($('#stadium-match-intro').length) {
        hideAllMessages();
        $('#match-type').show("slide", { direction: "left" }, 250);
        $('#user-circle').slideDown(500);
        $('#opponent-circle').slideDown(500);
        $('#versus-label').fadeIn(500);
        $("#versus-label").effect("bounce", 500);
        setTimeout(function () {
            $('#match-type').fadeOut(500);
            $('#stadium-match-intro').slideUp(500);
            $('#stadium-select').slideDown(500);
        }, 2000);
        tutorialWaitTime = 2500;
    }

    if (showModals) {
        setTimeout(function () {
            if ($('#modal-stadium-tutorial').length) {
                $('#modal-stadium-tutorial').modal();
            }
            if ($('#modal-stadium-elite-zone-tutorial').length) {
                $('#modal-stadium-elite-zone-tutorial').modal();
            }
            if ($('#modal-relic-requirements').length) {
                $('#modal-relic-requirements').modal();
            }
        }, tutorialWaitTime);
        if ($('#modal-stadium-lost-tutorial').length) {
            $('#modal-stadium-lost-tutorial').modal();
        }
    }
}

function concedeStadiumMatch() {
    if (confirm("Are you sure you want to concede this match? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Stadium/ConcedeMatch",
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshStadiumPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshStadiumPartialView();
                    }
                }
            }
        });
    }
}

function hideStadiumLevelUp() {
    setTimeout(function() {
        $('#level-up').slideUp(500);
    }, 2500);
    showStadiumFinalMessages();
}

function showStadiumFinalMessages() {
    $('.stadium-message').each(function (index) {
        var stadiumMessage = this;
        setTimeout(function () {
            $(stadiumMessage).slideDown(500);
        }, (index + 1) * 250);
    });
}

function stadiumRechargeUses(e, form, hasEnoughPoints) {
    if (hasEnoughPoints) {
        submitStadiumForm(e, form, false, true)
    }
    else {
        e.preventDefault();
        stopLoadingButton();
        showSnackbarWarning("You don't have enough CD on hand to recharge this Enhancer.");
    }
}

function skipStadiumCPU(e, form, challengerName, level, cost) {
    if ($('#skip-button').hasClass('disabled') || confirm("Are you sure you want to skip " + unescape(challengerName) + " and move on to the Level " + level + " CPU for " + cost + "? You'll lose any rewards you'd get for winning the match.")) {
        submitStadiumForm(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitStadiumForm(e, form, scrollToTop = true, reselectCards = false) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.shouldRefresh) {
                refreshStadiumPartialView();
                if (data.success) {
                    return;
                }
            }
            if (data.returnUrl) {
                window.location.href = data.returnUrl;
                return;
            }
            if (data.canPlaySound) {
                successAudio = new Audio("/sounds/Success_Round.mp3");
                negativeAudio = new Audio("/sounds/Defeat_Round.mp3");
            }
            if (data.success) {
                refreshStadiumPartialView(scrollToTop, false, reselectCards);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                if (data.showSpecialMessage) {
                    stopLoadingButton();
                    $('#checkpoint-cpu-requirements').text(data.errorMessage);
                    if (data.buttonText) {
                        $('#checkpoint-cpu-button').show();
                        $('#checkpoint-cpu-button-text').text(data.buttonText);
                        $('#checkpoint-cpu-button-url').attr('href', data.buttonUrl);
                    }
                    $('#modal-checkpoint-cpu-requirements').modal();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
                }
            }
        }
    });

    e.preventDefault();
}

function beginStadiumAnimation() {
    var roundPowers = $('#roundPower').attr('data-round-powers').split("-");
    var opponentRoundPowers = $('#opponentRoundPower').attr('data-round-powers').split("-");
    var roundPower = parseInt(roundPowers[roundPowerIndex]);
    var opponentRoundPower = parseInt(opponentRoundPowers[roundPowerIndex]);
    var userFaded = JSON.parse($('#user-faded').val());
    var opponentFaded = JSON.parse($('#opponent-faded').val());
    var userBackfired = JSON.parse($('#user-backfired').val());
    var opponentBackfired = JSON.parse($('#opponent-backfired').val());
    if (roundPowerIndex == 1) {
        userFaded[1][0] ? $('#card').addClass('faded') : $('#card').removeClass('faded');
        opponentFaded[1][0] ? $('#opponent-card').addClass('faded') : $('#opponent-card').removeClass('faded');

        userBackfired[1][0] ? $('#card').addClass('lose-red-border') : $('#card').removeClass('lose-red-border');
        opponentBackfired[1][0] ? $('#opponent-card').addClass('lose-red-border') : $('#opponent-card').removeClass('lose-red-border');
    }
    else if (roundPowerIndex == 2) {
        userFaded[2][0] ? $('#card').addClass('faded') : $('#card').removeClass('faded');
        opponentFaded[2][0] ? $('#opponent-card').addClass('faded') : $('#opponent-card').removeClass('faded');
        userFaded[2][1] ? $('#enhancer').addClass('faded') : $('#enhancer').removeClass('faded');
        opponentFaded[2][1] ? $('#opponent-enhancer').addClass('faded') : $('#opponent-enhancer').removeClass('faded');

        userBackfired[2][0] ? $('#card').addClass('lose-red-border') : $('#card').removeClass('lose-red-border');
        opponentBackfired[2][0] ? $('#opponent-card').addClass('lose-red-border') : $('#opponent-card').removeClass('lose-red-border');
        userBackfired[2][1] ? $('#enhancer').addClass('lose-red-border') : $('#enhancer').removeClass('lose-red-border');
        opponentBackfired[2][1] ? $('#opponent-enhancer').addClass('lose-red-border') : $('#opponent-enhancer').removeClass('lose-red-border');
    }

    /* -1 = loss, 1 = win, 2 = draw */
    var roundResult = 2;
    if (roundPowerIndex < 2) {
        roundResult = roundPower > opponentRoundPower ? 1 : roundPower < opponentRoundPower ? -1 : 2;
    }
    else {
        roundResult = parseInt($('#roundResult').val());
    }
    var greatestDiffIndex = 1;
    if (roundPowerIndex == 0) {
        greatestDiffIndex = roundResult == 1 ? 0 : 1;
    }
    else if (Math.abs(roundPower - parseInt(roundPowers[roundPowerIndex - 1])) > Math.abs(opponentRoundPower - parseInt(opponentRoundPowers[roundPowerIndex - 1]))) {
        greatestDiffIndex = 0;
    }
    $('.stadium-counter').each(function (index) {
        incrementPower(this, index == greatestDiffIndex, roundResult == 2 ? 2 : index == 0 ? roundResult : -roundResult);
    });
}

function addStadiumPowerMoveAnimations() {
    if ($('#swapEnhancers').val() == "true") {
        if ($('#user-enhancer-label').is(":hidden") && $('#opponent-enhancer-label').is(":visible")) {
            $('#user-enhancer-label').show();
            $('#opponent-enhancer-label').hide();
        }
        else if ($('#opponent-enhancer-label').is(":hidden") && $('#user-enhancer-label').is(":visible")) {
            $('#opponent-enhancer-label').show();
            $('#user-enhancer-label').hide();
        }
        $('#user-enhancer-col').find("#enhancer").attr("id", "opponent-enhancer");
        var userEnhancer = $('#user-enhancer-col').html();
        $('#opponent-enhancer-col').find("#opponent-enhancer").attr("id", "enhancer");
        var opponentEnhancer = $('#opponent-enhancer-col').html();
        $('#user-enhancer-col').html(opponentEnhancer);
        $('#opponent-enhancer-col').html(userEnhancer);
    }
    if ($('#swapCards').val() == "true") {
        $('#user-card-col').find("#card").attr("id", "opponent-card");
        var userCard = $('#user-card-col').html();
        $('#opponent-card-col').find("#opponent-card").attr("id", "card");
        var opponentCard = $('#opponent-card-col').html();
        $('#user-card-col').html(opponentCard);
        $('#opponent-card-col').html(userCard);
    }
    if ($('#mirrorPowerMoves').val() != "0") {
        var userPowerMove = $('#user-power-move-col').html();
        var opponentPowerMove = $('#opponent-power-move-col').html();
        if ($('#mirrorPowerMoves').val() == "1") {
            if ($('#opponent-power-move-label').is(":hidden")) {
                $('user-power-move').addClass('faded');
            }
            else {
                $("#user-power-move-col").effect("bounce", { distance: 15 }, 1000, function () {
                    $('#user-power-move-col').html(opponentPowerMove);
                    var powerMove = $('#user-power-move-col').find('#opponent-power-move');
                    if (!$(powerMove).hasClass('win-green-border') && !$(powerMove).hasClass('lose-red-border') && !$(powerMove).hasClass('faded')) {
                        $(powerMove).addClass('primary-color-border');
                    }
                });
            }
        }
        else if ($('#mirrorPowerMoves').val() == "-1") {
            if ($('#user-power-move-label').is(":hidden")) {
                $('opponent-power-move').addClass('faded');
            }
            else {
                $("#opponent-power-move-col").effect("bounce", { distance: 15 }, 1000, function () {
                    $('#opponent-power-move-col').html(userPowerMove);
                    var powerMove = $('#opponent-power-move-col').find('#power-move');
                    if (!$(powerMove).hasClass('win-green-border') && !$(powerMove).hasClass('lose-red-border') && !$(powerMove).hasClass('faded')) {
                        $(powerMove).addClass('primary-color-border');
                    }
                });
            }
        }
    }
}

function incrementPower(counter, shouldContinue, roundResult) {
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "false") {
        var roundPowers = $(counter).attr('data-round-powers').split("-");
        var countTo = parseInt(roundPowers[roundPowerIndex]);
        var countFrom = parseInt(roundPowerIndex == 0 ? 0 : roundPowers[roundPowerIndex - 1]);
        if (countTo != countFrom) {
            countToNumber($(counter), countTo, countFrom, "", shouldContinue ? stadiumRoundResultLoaded : null);
        }
        else if (shouldContinue) {
            stadiumRoundResultLoaded();
        }
        $(counter).removeClassStartingWith('win-green-animated');
        $(counter).removeClassStartingWith('lose-red-animated');
        if (roundResult < 2) {
            var newClass = roundResult == 1 ? "win-green-animated" : "lose-red-animated";
            if (countTo < 20) {
                newClass += "-500ms";
            }
            else if (countTo < 40) {
                newClass += "-1000ms";
            }
            else {
                newClass += "-1500ms";
            }
            $(counter).addClass(newClass)
        };
    }
}

function stadiumRoundResultLoaded() {
    roundPowerIndex++;
    if (roundPowerIndex == 1) {
        if ($('#enhancers').attr('data-count') == 0) {
            stadiumRoundResultLoaded();
        }
        else {
            setTimeout(function () {
                $('#enhancers').fadeIn(500, function () {
                    $('html, body').animate({
                        scrollTop: $('#enhancers').offset().top - 100
                    }, 500).promise().then(function () {
                        beginStadiumAnimation();
                    });
                });
            }, 500);
        }
        return;
    }
    else if (roundPowerIndex == 2) {
        if ($('#power-moves').attr('data-count') == 0) {
            beginStadiumAnimation();
        }
        else {
            setTimeout(function () {
                $('#power-moves').fadeIn(500, function () {
                    $('html, body').animate({
                        scrollTop: $('#power-moves').offset().top - 100
                    }, 500).promise().then(function () {
                        addStadiumPowerMoveAnimations();
                        beginStadiumAnimation();
                    });
                });
            }, 500);
        }
        setTimeout(function () {
            $('#bonuses').fadeIn(500);
            $('#bonuses-opponent').fadeIn(500);
        }, 500);
        return;
    }
    else {
        var roundResult = parseInt($('#roundResult').val());
        if (roundResult == 1) {
            $('#round-power-user').addClass('animated tada');
        }
        else if (roundResult == -1) {
            $('#round-power-opponent').addClass('animated tada');
        }
        $('.round-power').addClass('animated tada');
        setTimeout(function () {
            $('#match-result').fadeIn(500, function () {
                $('#stadiumForm-duel').fadeIn(500);
                $('html, body').animate({
                    scrollTop: $('#match-result').offset().top - 100
                }, 500);
                if ($('#modal-stadium-tutorial-4').length) {
                    $('#next-round-button').addClass('disabled');
                    $('#next-round-button').attr('disabled', 'disabled');
                    setTimeout(function () {
                        $('#modal-stadium-tutorial-4').modal();
                        $('#next-round-button').removeClass('disabled');
                        $('#next-round-button').removeAttr('disabled');
                    }, 1500);
                }
                if (roundResult == 1 && successAudio) {
                    successAudio.play();
                }
                else if (roundResult == -1 && negativeAudio) {
                    negativeAudio.play();
                }
            });
        }, 500);
        return;
    }
}

function enhancerSelected(enhancer) {
    var enhancer = $(enhancer);
    var enhancerId = enhancer.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#enhancerId').val() != ''){
        $('#enhancerId').val("");
        $('.enhancer').fadeIn(250);
        $('#enhancer-selected-dot').attr('data-prefix', 'far');
        $('#enhancer-selected-dot').attr('data-icon', 'circle');
        $('#expand-enhancers').hide();
    }
    else{
        $('#enhancerId').val(enhancerId);
        enhancer.addClass('itemSelected');
        $('.enhancer').each(function () {
            if (!$(this).hasClass('itemSelected')) {
                $(this).hide();
            }
        });
        $('#enhancer-selected-dot').attr('data-prefix', 'fas');
        $('#enhancer-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-enhancers').show();
        $('html, body').animate({
            scrollTop: $('#power-moves').offset().top - 50
        }, 500);
    }

    if ($('#modal-stadium-tutorial-3').length && !hasSeenPowerMovesTutorial) {
        $('#modal-stadium-tutorial-3').modal();
        hasSeenPowerMovesTutorial = true;
    }
}

function powerMoveSelected(powerMove, scrollToPosition = true) {
    var powerMove = $(powerMove);
    var powerMoveId = powerMove.attr("id");

    $('.powerMoveSelected').removeClass('powerMoveSelected');

    if ($('#powerMoveId').val() != '') {
        $('#powerMoveId').val("");
        $('.power-move').fadeIn(250);
        $('#power-move-selected-dot').attr('data-prefix', 'far');
        $('#power-move-selected-dot').attr('data-icon', 'circle');
        $('#expand-power-moves').hide();
    }
    else {
        $('#powerMoveId').val(powerMoveId);
        powerMove.addClass('powerMoveSelected');
        $('.power-move').each(function () {
            if (!$(this).hasClass('powerMoveSelected')) {
                $(this).hide();
            }
        });
        $('#power-move-selected-dot').attr('data-prefix', 'fas');
        $('#power-move-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-power-moves').show();
        if (scrollToPosition) {
            $('html, body').animate({
                scrollTop: $('#stadiumForm').offset().top - 50
            }, 500);
        }
    }
}

function deckCardSelected(card, scrollToPosition = true) {
    var card = $(card);
    var cardId = card.attr("id");

    $('.cardSelected').removeClass('cardSelected');
    $('#play-round-button').addClass('disabled');

    if ($('#deckCardId').val() != ''){
        $('#deckCardId').val("");
        $('#enhancers').hide();
        $('#power-moves').hide();
        $('.deck-card').fadeIn(250);
        $('#card-selected-dot').attr('data-prefix', 'far');
        $('#card-selected-dot').attr('data-icon', 'circle');
        $('#expand-cards').hide();
    }
    else {
        $('#deckCardId').val(cardId);
        card.addClass('cardSelected');
        $('#enhancers').fadeIn(250);
        $('#power-moves').fadeIn(250);
        $('#play-round-button').removeClass('disabled');
        $('.deck-card').each(function () {
            if (!$(this).hasClass('cardSelected')) {
                $(this).hide();
            }
        });
        if ($('#modal-stadium-tutorial-2').length && !hasSeenEnhancersTutorial) {
            $('#modal-stadium-tutorial-2').modal();
            hasSeenEnhancersTutorial = true;
        }
        $('#card-selected-dot').attr('data-prefix', 'fas');
        $('#card-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-cards').show();
        if (scrollToPosition) {
            $('html, body').animate({
                scrollTop: $('#enhancers').offset().top - 50
            }, 500);
        }
    }
}

function pollForOpponentsMove(tries = 0) {
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (!(shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "true")){
        return;
    }
    hideAllMessages();
    if (tries == 15){
        showSnackbarInfo("Your opponent never made a move!");
        refreshStadiumPartialView();
        return;
    }
    if (tries >= 10){
        showInfoMessage("Your opponent still hasn't made a move...", tries == 10, tries == 10);
    }

    $.ajax({
        type: "GET",
        url: "/Stadium/PollForOpponentsMove",
        success: function(data)
        {
            if (data.success){
                refreshStadiumPartialView(false, true);
            }
            else{
                if (data.stopExecuting) {
                    hideAllMessages();
                    return;
                }
                tries += 1;
                setTimeout(function(){
                    pollForOpponentsMove(tries);
                }, tries * 500);
            }
        }
    });
}

function pollForOpponentWaitlist(tries, challengeUsername) {
    var shouldPollForOpponentWaitlist = $('#shouldPollForOpponentWaitlist');
    if (!(shouldPollForOpponentWaitlist && shouldPollForOpponentWaitlist.val() === "true")){
        return;
    }
    hideAllMessages();
    if (tries >= 60) {
        showSnackbarWarning("You timed out! Try searching for a new opponent.");
        refreshStadiumPartialView();
        return;
    }
    if (tries >= 10){
        showInfoMessage(challengeUsername == "" ? "Still looking for an opponent..." : "Still waiting for " + challengeUsername + " to accept your challenge request...", tries == 10, tries == 10);
    }
    $.ajax({
        type: "GET",
        url: "/Stadium/PollForOpponentWaitlist",
        data: { tries : tries },
        success: function(data)
        {
            if (data.success){
                refreshStadiumPartialView();
            }
            else{
                if (data.stopExecuting) {
                    hideAllMessages();
                    showSnackbarWarning("You timed out! Try searching for a new opponent.");
                    refreshStadiumPartialView();
                    return;
                }
                tries += 1;
                setTimeout(function(){
                    pollForOpponentWaitlist(tries, challengeUsername);
                }, 1500);
            }
        }
    });
}

function countdownNumber(number, pageChanged) {
    if (pageChanged != stadiumPageChanged){
        return;
    }
    var countDown =  $("#countdown-match");
    if(!countDown.length){
        return;
    }
    if (number <= 0){
        countDown.html("<span class='lose-red'>The match ran out of time!</span>");
        return;
    }
    else {
        if (number == 30) {
            showSnackbarWarning(($('#roundResult').length ? "There's only <b>30 seconds</b> left in this round." : "You only have <b>30 seconds</b> left to choose a card.") + " If the timer runs out, the match will end.");
        }
        $('#match-secondsLeftCountdown').html("<b class='" + (number <= 10 ? 'lose-red' : '') + "'>" + number + " second" + (number == 1 ? "" : "s") + "</b>");
    }
    setTimeout(function(){
        countdownNumber(number - 1, pageChanged);
    }, 1000);
}

function refreshStadiumPartialView(scrollToTop = true, scrollToTopAnimated = false, reselectCards = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Stadium/GetStadiumPartialView",
        success: function(data)
        {
            if (reselectCards) {
                var selectedCard = $('.cardSelected');
                var selectedPowerMove = $('.powerMoveSelected');
            }
            $("#stadiumPartialView").html(data);
            onPageLoad(scrollToTop, !reselectCards);
            onStadiumLoad(!reselectCards);
            if (reselectCards) {
                if ($(selectedCard).length) {
                    var card = $('#' + selectedCard.attr('id'));
                    deckCardSelected(card, !reselectCards);
                }
                if ($(selectedPowerMove).length) {
                    var powerMove = $('#' + selectedPowerMove.attr('id'));
                    powerMoveSelected(powerMove, !reselectCards);
                }
                $('html, body').animate({
                    scrollTop: $('#enhancers').offset().top - 50
                }, 500);
            }
            if ($('#challenge-tabs').length) {
                $('html, body').animate({
                    scrollTop: $('#challenge-tabs').offset().top - 50
                }, 500);
            }
            else if (!scrollToTop && scrollToTopAnimated) {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            }
        }
    });
}

function scrollToChallengesTab() {
    if ($('#challenge-tabs').length) {
        $('html, body').animate({
            scrollTop: $('#challenge-tabs').offset().top - 50
        }, 500);
    }
}

function updateStadiumTip(subsequentLoad = false){
    var stadiumTip = $('#stadiumTip');
    if (!stadiumTip.length){
        return;
    }
    if (subsequentLoad){
        $.ajax({
            type: "GET",
            url: "/Stadium/GetTip",
            success: function(data)
            {
                if (data.success) {
                    stadiumTip.fadeOut(250, function () {
                        stadiumTip.text(data.successMessage);
                        stadiumTip.fadeIn(250);
                    });
                }
            }
        });
    }
    setTimeout(function(){
        updateStadiumTip(true);
    }, 7500);
}