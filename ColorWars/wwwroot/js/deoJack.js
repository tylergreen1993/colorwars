﻿function updateDeoJack(e, form, isStand = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDeoJackPartialView(isStand);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshDeoJackPartialView(isStand = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/DeoJack/GetDeoJackPartialView",
        success: function(data)
        {
            $("#deoJackPartialView").html(data);
            if (isStand) {
                $('.flipped-card').hide();
                $('#result').hide();
                $('#dealer-result').text(0);
                $('#end-game-buttons').hide();
                $('#dealer-status').hide();
                var result = 0;
                if ($('#user-result').hasClass('win-green')) {
                    $('#user-result').removeClass('win-green');
                    result = 1;
                }
                else if ($('#user-result').hasClass('lose-red')) {
                    $('#user-result').removeClass('lose-red');
                    result = -1;
                }
                if (result != 0) {
                    $('#dealer-result').removeClass('win-green');
                    $('#dealer-result').removeClass('lose-red');
                }
                var fadeInTime = 0;
                if ($('.flipped-card').length) {
                    $('.flipped-card').each(function (index) {
                        var card = $(this);
                        setTimeout(function () {
                            $(card).fadeIn();
                            $(card).parent().scrollLeft($(card).offset().left - $(card).width() - 50);
                            $('#dealer-result').text($(card).attr('data-count'));
                        }, fadeInTime);
                        if (index < $('.flipped-card').length - 1) {
                            fadeInTime += 1000;
                        }
                    });
                    setTimeout(function () {
                        if (result != 0) {
                            $('#user-result').addClass(result == 1 ? 'win-green' : 'lose-red');
                            $('#dealer-result').addClass(result == 1 ? 'lose-red' : 'win-green');
                        }
                        $('#dealer-status').show();
                        $('#result').fadeIn();
                        $('#end-game-buttons').show();
                        $('.flipped-card').fadeIn();
                        $('html, body').animate({
                            scrollTop: $('#result').offset().top - 100
                        }, 250);
                    }, fadeInTime);
                }
            }
            onPageLoad(false);
        }
    });
}