﻿function searchMarketStalls(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/MarketStalls?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};

function deleteMarketStall(id){
    if (confirm("Are you sure you want to remove your Market Stall? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/MarketStalls/RemoveMarketStall",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/MarketStalls";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshMarketStallsIdPartialView(id);
                    }
                }
            }
        });
    }
}

function updateMarketStallDescriptionCountMessage() {
    var marketStallDescriptionLength = $('#description').val().length;
    var remainingLength = 500 - marketStallDescriptionLength;
    $('#marketStallDescriptionCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function submitMarketStallCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    var arr = addMarketStallItemsToForm(form);

    $.ajax({
        type: "POST",
        url: url,
        data: arr,
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                window.location = "/MarketStalls?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitMarketStallsMainForm(e, form, query, page) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMarketStallsMainPartialView(query, page);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshMarketStallMainPartialView(query, page);
                }
            }
        }
    });

    e.preventDefault();
}

function submitMarketStallsIdForm(e, form, id, isUpdate, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: isUpdate ? addMarketStallItemsToForm(form) : form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, scrollToTop);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMarketStallsIdPartialView(id, scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshMarketStallsIdPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshMarketStallsMainPartialView(query, page) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { query : query, page : page },
        url: "/MarketStalls/GetMarketStallsMainPartialView",
        success: function(data)
        {
            $("#marketStallsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMarketStallsIdPartialView(id, scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/MarketStalls/GetMarketStallsIdPartialView",
        success: function(data)
        {
            $("#marketStallsPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}

function addMarketStallItemsToForm(form) {
    var marketStallItems = [];

    var arr = form.serializeArray();
    for(var i = 0; i < arr.length; i++){ 
        if (arr[i].value == '' || arr[i].value == '0') {
            arr.splice(i, 1); 
            i--;
        }
        else if (arr[i].name.includes('cost')) {
            var itemId = arr[i].name.split('cost-')[1];
            var item = { "SelectionId" : itemId, "Cost" : arr[i].value };
            marketStallItems.push(item);
            arr.splice(i, 1); 
            i--;
        }
    }

    arr.push({ "name" : "marketStallItems", "value": JSON.stringify(marketStallItems) });

    return arr;
}