﻿function updateDonations(e, form, isTaking) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, isTaking, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDonationsPartialView(isTaking);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshDonationsPartialView(isTaking) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Donations/GetDonationsPartialView",
        data: {isTaking : isTaking},
        success: function(data)
        {
            $("#donationsPartialView").html(data);
            onPageLoad(isTaking);
        }
    });
}