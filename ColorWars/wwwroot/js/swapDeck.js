﻿function swapDeckCardSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedDeckCard').val() == itemId){
        $('#selectedDeckCard').val("");
        $('#swapDeckButton').addClass('disabled');
    }
    else{
        $('#selectedDeckCard').val(itemId);
        item.addClass('itemSelected');
        $('#swapDeckButton').removeClass('disabled');
    }
}

function updateSwapDeck(e, form, isSwapDeck) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (isSwapDeck) {
                    refreshSwapDeckPartialView(data.id);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            if (!isSwapDeck) {
                refreshSwapDeckMazePartialView();
            }
        }
    });

    e.preventDefault();
};

function refreshSwapDeckPartialView(selectionId){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SwapDeck/GetSwapDeckPartialView",
        success: function(data)
        {
            $("#swapDeckPartialView").html(data);
            if (selectionId) {
                var img = $('#' + selectionId).find('img')
                $('#' + selectionId).removeClass('fade-out-border');
                $(img).removeClass('animated bounceIn');
                $('#' + selectionId).addClass('fade-out-border');
                $(img).addClass('animated bounceIn');
            }
        }
    });
}

function refreshSwapDeckMazePartialView(){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SwapDeck/GetSwapDeckMazePartialView",
        success: function(data)
        {
            $("#swapDeckMazePartialView").html(data);
            onPageLoad(false);
        }
    });
}