﻿function companionSelected(companion) {
    var companion = $(companion);
    var companionId = companion.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedId').val() == companionId){
        $('#selectedId').val("");
    }
    else{
        $('#selectedId').val(companionId);
        companion.addClass('itemSelected');
    }
}

function updateCaverns(e, form, isSorceress) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (isSorceress) {
                    refreshCavernsPartialView();
                }
                else {
                    refreshCavernsSeedCardsPartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshCavernsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Caverns/GetCavernsPartialView",
        cache: false,
        success: function(data)
        {
            $("#cavernsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshCavernsSeedCardsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Caverns/GetCavernsSeedCardsPartialView",
        cache: false,
        success: function(data)
        {
            $("#cavernsSeedCardsPartialView").html(data);
            onPageLoad();
        }
    });
}