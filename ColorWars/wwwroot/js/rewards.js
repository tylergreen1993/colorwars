﻿function updateRewards(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshRewardsStorePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshRewardsStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/RewardsStore/GetRewardsStorePartialView",
        cache: false,
        success: function(data)
        {
            $("#rewardsStorePartialView").html(data);
            onPageLoad();
        }
    });
}