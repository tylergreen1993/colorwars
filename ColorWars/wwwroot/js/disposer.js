﻿function submitDisposerFormConfirmation(e, form, showConfirmation) {
    if (!showConfirmation) {
        submitDisposerForm(e, form);
        e.preventDefault();
        return;
    }

    if (confirm("Are you sure you want to permanently dispose of your item? (You can disable this confirmation in your Preferences)")) {
        submitDisposerForm(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitDisposerForm(e, form, scrollToTop = false) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDisposerPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshDisposerPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshDisposerPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Disposer/GetDisposerPartialView",
        success: function(data)
        {
            $("#disposerPartialView").html(data);
            onPageLoad(false);
        }
    });
}

