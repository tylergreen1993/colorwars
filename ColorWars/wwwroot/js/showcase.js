﻿function submitShowcaseForm(e, form, isAger = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshShowcasePartialView(isAger);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshShowcasePartialView(isAger = false) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isAger : isAger },
        url: "/Showcase/GetShowcasePartialView",
        success: function(data)
        {
            $("#showcasePartialView").html(data);
            onPageLoad();
        }
    });
}