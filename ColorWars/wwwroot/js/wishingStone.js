﻿$(document).ready(function(){
    onWishingStoneLoad();
});

function onWishingStoneLoad(){
    $('#wishingScrollContainer').scrollTop($('#wishingContainer').height());
    var makeWishContainerOffset = $('#makeWish-container').offset();
    if (makeWishContainerOffset){
        $(window).scrollTop(makeWishContainerOffset.top - window.innerHeight + $('#makeWish-container').height() * 2);
    }
}

function mineQuarryStone(position) {
    $.ajax({
        type: "POST",
        url: "/WishingStone/MineQuarryStone",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshWishingStoneQuarryPartialView(position, data.scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitWishingStoneForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshWishingStonePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWishingStonePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WishingStone/GetWishingStonePartialView",
        cache: false,
        success: function(data)
        {
            $("#wishingStonePartialView").html(data);
            $('#wishingScrollContainer').scrollTop($('#wishingContainer').height());
            onPageLoad();
        }
    });
}

function refreshWishingStoneQuarryPartialView(position, scrollToTop = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WishingStone/GetWishingStoneQuarryPartialView",
        cache: false,
        success: function(data)
        {
            $("#wishingStoneQuarryPartialView").html(data);
            deactivateAllQuarryStones();
            var quarryStoneId = '#quarry-stone-' + position;
            var countTo = $(quarryStoneId).attr('data-count');
            var currency = $(quarryStoneId).text().split(' ')[1];
            countToNumber($(quarryStoneId), countTo, 0, " " + currency, activateAllQuarryStones);
            onPageLoad(scrollToTop);
        }
    });
}

function activateAllQuarryStones() {
    $('.active-quarry-stone').each(function() {
        $(this).removeClass('faded');
    });
    $('.quarry-stone').each(function() {
        $(this).css('pointer-events', '');
    });
}

function deactivateAllQuarryStones() {
    $('.active-quarry-stone').each(function() {
        $(this).addClass('faded');
    });
    $('.quarry-stone').each(function() {
        $(this).css('pointer-events', 'none');
    });
}