﻿function submitClubForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshClubPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function submitClubIncubatorForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshClubIncubatorPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubIncubatorPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshClubPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Club/GetClubPartialView",
        success: function(data)
        {
            $("#clubPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshClubIncubatorPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Club/GetClubIncubatorPartialView",
        success: function(data)
        {
            $("#clubIncubatorPartialView").html(data);
            onPageLoad();
        }
    });
}