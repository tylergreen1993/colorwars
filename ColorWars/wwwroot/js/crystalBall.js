﻿function showCrystalBallColors(colors, i) {
    if (i < colors.length) {
        $('#crystal-ball-color').css("background-color", colors[i]);
        $('#color-name').text(colors[i]);
        $('#color-name').fadeIn(500);
        $('#crystal-ball-color').fadeIn(500, function () {
            setTimeout(function () {
                $('#color-name').fadeOut(500);
                $('#crystal-ball-color').fadeOut(500, function () {
                    showCrystalBallColors(colors, i + 1);
                });
            }, 1000);
        });
    }
    if (i == colors.length) {
        $('#crystal-ball-color').css("background-color", "white");
        $('#color-choices').fadeIn(500);
    }
}

function crystalBallColorSelected(color) {
    var chosenColor = $(color).attr('id');
    $('.crystal-ball-color-choice').addClass('faded');
    $('.grid-element').removeClass('fade-out-border-win-green');
    $.ajax({
        type: "POST",
        url: "/CrystalBall/ChooseColor",
        data: { "color": chosenColor },
        success: function (data) {
            $('.crystal-ball-color-choice').removeClass('faded');
            hideAllMessages();
            if (data.success) {
                $(color).addClass('fade-out-border-win-green');
                if (data.successMessage) {
                    showSnackbarSuccess("Nice! " + chosenColor + " was the right choice!");
                    showSuccessMessage(data.successMessage);
                    refreshCrystalBallPartialView();
                }
                else if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                    refreshCrystalBallPartialView();
                }
                else {
                    showSnackbarSuccess("Nice! " + chosenColor + " was the right choice! Keep going!");
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function updateCrystalBall(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#start-form').hide();
                var colors = data.infoMessage.split("-");
                showCrystalBallColors(colors, 0);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshCrystalBallPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/CrystalBall/GetCrystalBallPartialView",
        success: function(data)
        {
            $("#crystalBallPartialView").html(data);
            onPageLoad();
        }
    });
}