﻿var loadingMatchingColor = false;

function revealMatchingCard(position) {
    if (loadingMatchingColor) {
        showSnackbarWarning("You did that too fast.");
        return;
    }
    loadingMatchingColor = true;
    $.ajax({
        type: "POST",
        url: "/MatchingColors/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                var showMessage = false;
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                    showMessage = true;
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                    showMessage = true;
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMatchingColorsPartialView(showMessage);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                loadingMatchingColor = false;
            }
        }
    });
}

function refreshMatchingColorsPartialView(scrollToTop){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/MatchingColors/GetMatchingColorsPartialView",
        success: function(data)
        {
            $("#matchingColorsPartialView").html(data);
            onPageLoad(scrollToTop);
            loadingMatchingColor = false;
        }
    });
}