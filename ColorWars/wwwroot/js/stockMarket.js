﻿function updateSharePrice(cost, points){
    var shares = $('#shares').val();
    if (shares < 0){
        return;
    }
    var calculatedCost = Math.round(shares) * cost;
    var formattedCost = formatNumber(calculatedCost);
    if (calculatedCost <= points){
        $('#calculatedCost').html('<span class="win-green">' + formattedCost + '</span>');
        $('#currency').addClass('win-green');
        $('#currency').removeClass('lose-red');
    }
    else{
        $('#calculatedCost').html('<span class="lose-red">' + formattedCost + '</span>');
        $('#currency').addClass('lose-red');
        $('#currency').removeClass('win-green');
    }
}

function submitStockMarketForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                refreshStockMarketPartialView(id);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitPortfolioForm(e, form, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, scrollToTop);
                refreshPortfolioPartialView(scrollToTop);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshStockMarketPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StockMarket/GetStockMarketPartialView",
        data: { id : id },
        success: function(data)
        {
            $("#stockMarketPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshPortfolioPartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StockMarket/GetPortfolioPartialView",
        success: function(data)
        {
            $("#portfolioPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}