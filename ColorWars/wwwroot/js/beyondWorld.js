﻿function chooseBeyondWorldPath(path) {
    if (confirm("Are you sure you want to choose this path? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/BeyondWorld/ChoosePath",
            data: { path : path },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshBeyondWorldPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function closeBeyondWorld(e, form) {
    if (confirm("Are you sure you want to permanently close the Beyond World? You cannot return once you do.")) {
        updateBeyondWorld(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function updateBeyondWorld(e, form, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }

                refreshBeyondWorldPartialView(scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshBeyondWorldPartialView(scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/BeyondWorld/GetBeyondWorldPartialView",
        success: function(data)
        {
            $("#beyondWorldPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}