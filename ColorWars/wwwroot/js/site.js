var hideMessages = true;
var originalBtn = null;
var originalBtnTitle = "";
var preventBtnLoad = false;
var unreadMessagesCount = 0;
var unreadNotificationsCount = 0;
var sentCourierMessage = false;
var updateNavTimer = 0;
var resendNavTimer  = 0;
var courierTimer = 0;

var sdkInstance = "appInsightsSDK"; window[sdkInstance] = "appInsights"; var aiName = window[sdkInstance], aisdk = window[aiName] || function (e) { function n(e) { t[e] = function () { var n = arguments; t.queue.push(function () { t[e].apply(t, n) }) } } var t = { config: e }; t.initialize = !0; var i = document, a = window; setTimeout(function () { var n = i.createElement("script"); n.src = e.url || "https://az416426.vo.msecnd.net/scripts/b/ai.2.min.js", i.getElementsByTagName("script")[0].parentNode.appendChild(n) }); try { t.cookie = i.cookie } catch (e) { } t.queue = [], t.version = 2; for (var r = ["Event", "PageView", "Exception", "Trace", "DependencyData", "Metric", "PageViewPerformance"]; r.length;)n("track" + r.pop()); n("startTrackPage"), n("stopTrackPage"); var s = "Track" + r[0]; if (n("start" + s), n("stop" + s), n("setAuthenticatedUserContext"), n("clearAuthenticatedUserContext"), n("flush"), !(!0 === e.disableExceptionTracking || e.extensionConfig && e.extensionConfig.ApplicationInsightsAnalytics && !0 === e.extensionConfig.ApplicationInsightsAnalytics.disableExceptionTracking)) { n("_" + (r = "onerror")); var o = a[r]; a[r] = function (e, n, i, a, s) { var c = o && o(e, n, i, a, s); return !0 !== c && t["_" + r]({ message: e, url: n, lineNumber: i, columnNumber: a, error: s }), c }, e.autoExceptionInstrumented = !0 } return t }(
    {
        instrumentationKey: "fcc434e5-27e5-451b-8bf3-d28d4e7d6c4e"
    }
); window[aiName] = aisdk, aisdk.queue && 0 === aisdk.queue.length && aisdk.trackPageView({});

$(document).ready(function(){
    onPageLoad();
});

$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        stopLoadingButton();
    }
});

function onPageLoad(scrollToTop = true, scrollToHash = true, updatePushDown = true){
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    if (window.canRunAds === undefined) {
        $('.ad-feed').hide();
        $('.ad-feed').removeClass('stadium-message');
        $('.ad-feed').removeClass('home-feed');
        $('.enable-ad').show();
        $('.enable-ad').addClass('stadium-message');
        $('.enable-ad').addClass('home-feed');
    }
    stopLoadingButton();
    $('[data-toggle="tooltip"]').tooltip();
    $('[rel="tooltip"]').on('click', function () {
        $(this).tooltip('hide')
    })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        pushDownScrollClass();
    });
    if (window.location.hash) {
        var selection = $(window.location.hash)
        if (selection && !selection.hasClass('itemSelected') && !selection.hasClass('fade-out-border') && (selection.hasClass('grid-element') || selection.hasClass('scrolling-grid-element') || selection.hasClass('gray-container'))) {
            if (selection.hasClass('gray-container') && !selection.hasClass('gray-border')) {
                selection.addClass('gray-border');
            }
            selection.addClass('fade-out-border-light-blue');
            if (selection.parent()) {
                selection.parent().scrollLeft(selection.offset().left - selection.width() - 50);
            }
            setTimeout(function () {
                pushDownScrollClass();
                selection.removeClass('fade-out-border-light-blue');
            }, 5000);
        }
        else if (selection && selection.hasClass('form-group')) {
            selection.addClass('fade-out-highlight');
        }
        if (scrollToHash && $(window.location.hash).length) {
            $(window).scrollTop($(window.location.hash).offset().top - 50);
        }
        history.replaceState('', '', window.location.pathname);
    }
    $('[id^="colored-by-username"]').each(function (i, el) {
        var setColor = $(el).attr('data-user-color');
        var color = setColor ? setColor : stringToColor($(el).attr('value'));
        $(el).css("background-color", color);
        if ($('#user-color').length) {
            $('#user-color').val(color);
        }
    });
    $("[data-hide]").on("click", function(){
        $(this).closest("." + $(this).attr("data-hide")).hide();
    });
    $('select').selectpicker({
        style: 'btn-default',
        size: false
    });
    $(document).click(function (event) {
        var clickover = $(event.target);
        var opened = $(".navbar-collapse").hasClass("navbar-collapse collapse in");
        if (opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });
    $(".btn").unbind("click");
    $(".btn").off().on("click", function(e){
        if (preventBtnLoad){
            preventBtnLoad = false;
            return;
        }
        var form = $(this).closest('form');
        var isButton = $(this).is('button');
        if ($(this).hasClass('no-load-btn')){
            return;
        }
        if (isButton && $(this).hasClass('dropdown-toggle')) {
            return;
        }
        if (isButton && !(form && form[0])){
            return;
        }
        if (form && isButton && !form[0].checkValidity()){
            return;
        }
        originalBtn = $(this);
        originalBtnTitle = $(this).html();
        $(this).html("<span class='glyphicon glyphicon-repeat normal-right-spinner'></span>");
        $(this).prop("disabled", true);
        if (form && isButton){
            e.preventDefault();
            form.submit();
        }
    });
    $('.dropdown').on('hidden.bs.dropdown', function(e) {
        $(this).find('.caret').toggleClass('rotate-180');
    });
    $('.dropdown').on('shown.bs.dropdown', function(e) {
        $(this).find('.caret').toggleClass('rotate-180');
    });
    if ($('.progress-bar').length && $('.progress-bar').attr('aria-valuenow') != undefined) {
        setTimeout(function () {
            $('.progress-bar').width($('.progress-bar').attr('aria-valuenow') + "%");
        }, 500);
    }
    if ($('.scrolling-container').length) {
        $(window).on('resize', function () {
            showHideScrollingContainerIndicator();
        });
        $(".scrolling-container").scroll(function () {
            showHideScrollingContainerIndicator();
        });
        new ResizeSensor($('.scrolling-container'), function () {
            showHideScrollingContainerIndicator();
        });
        showHideScrollingContainerIndicator();
    }
    if (updatePushDown) {
        pushDownScrollClass();
    }
    initializeNumberCounter();
    showToastIfExists();
    var usernameCookie = getCookie("Username");
    var isAuthenticated = usernameCookie != null;
    if (isAuthenticated) {
        getUnreadMessages();
        clearTimeout(courierTimer);
        courierTimer = setTimeout(function(){
            sendCourierMessage();
        }, 2500);
    }
}

function initializeNumberCounter(currency = "", fn = null) {
    if ($('.number-counter').length) {
        $('.number-counter').each(function () {
            var countTo = $(this).attr('data-count');
            var countStart = $(this)[0].hasAttribute('data-count-start') ? $(this).attr('data-count-start') : 0;
            var delay = $(this)[0].hasAttribute('data-count-delay') ? parseInt($(this).attr('data-count-delay')) : 0;
            var count = $(this);
            setTimeout(function () {
                countToNumber($(count), countTo, countStart, currency, fn);
            }, delay);
        });
    }
}

function formatNumber(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatNumberShortened(number) {
     if (number >= 1000000) {
        return (number / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
     }
     if (number >= 1000) {
        return (number / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
     }
     return number;
}

function playSound(soundUrl) {
    new Audio(soundUrl).play();
}

function stopLoadingButton() {
    if (originalBtn){
        originalBtn.prop("disabled", false).html(originalBtnTitle);
    }
}

function showHideScrollingContainerIndicator() {
    if ($('.scrolling-wrapper').length) {
        $('.scrolling-wrapper').each(function () {
            var scrollingContainer = $(this).find(".scrolling-container");
            var scrollingContainerIndicatorLeft = $(this).find(".scrolling-container-indicator-left");
            var scrollingContainerIndicatorRight = $(this).find(".scrolling-container-indicator-right");
            if ($(scrollingContainer)[0].scrollLeft > 0) {
                $(scrollingContainerIndicatorLeft).fadeIn();
                $(this).removeClass('hidden-left');
            }
            else {
                $(scrollingContainerIndicatorLeft).hide();
                $(this).addClass('hidden-left');
            }
            if (scrollingContainer[0].scrollWidth - ($(scrollingContainer)[0].scrollLeft + $(scrollingContainer).innerWidth()) >= 10) {
                $(scrollingContainerIndicatorRight).fadeIn();
                $(this).removeClass('hidden-right');
            }
            else {
                $(scrollingContainerIndicatorRight).hide();
                $(this).addClass('hidden-right');
            }
        });
    }
}

function scrollContainerToLeft(container) {
    var scrollingContainer = $(container).find(".scrolling-container");
    $(scrollingContainer).animate({
        scrollLeft: Math.max(0, $(scrollingContainer)[0].scrollLeft - $(scrollingContainer).innerWidth())
    }, 500);
}

function scrollContainerToRight(container) {
    var scrollingContainer = $(container).find(".scrolling-container");
    $(scrollingContainer).animate({
        scrollLeft: Math.min(scrollingContainer[0].scrollWidth, $(scrollingContainer)[0].scrollLeft + $(scrollingContainer).innerWidth())
    }, 500);
}

function pushDownScrollClass() {
    if ($('.scrolling-wrapper').length) {
        $('.scrolling-wrapper').each(function () {
            var pushDownClass = $(this).find(".push-down-class");
            if ($(pushDownClass).length) {
                var bottomOffset = 0;
                $(pushDownClass).each(function () {
                    bottomOffset = Math.max(bottomOffset, $(this).offset().top + $(this).height());
                });
                $(pushDownClass).each(function () {
                    if ($(this).offset().top + $(this).height() < bottomOffset) {
                        $(this).css("padding-top", bottomOffset - ($(this).offset().top + $(this).height()));
                    }
                });
            }
        });
    }
}

function scrollToId(id) {
    $('html, body').animate({
        scrollTop: $('#' + id).offset().top - 10
    }, 500);
}

function resizeTextarea(textarea) {
    textarea.style.height = 'auto';
    textarea.style.height = textarea.scrollHeight + 'px';
}

function countToNumber(el, countTo, countNum = 0, currency = "", callback = null, useShortenedFormat = false){
    el.text(countNum);
    $({countNum: countNum == 0 ? el.text() : countNum}).animate({
            countNum: countTo
        },
        {
            duration: Math.min(Math.abs(countTo - countNum) * 50, 1250),
            easing: 'linear',
            step: function() {
                el.text(useShortenedFormat ? formatNumberShortened(Math.floor(this.countNum)) : formatNumber(Math.floor(this.countNum)) + currency);
            },
            complete: function() {
                el.text(useShortenedFormat ? formatNumberShortened(Math.floor(this.countNum)) : formatNumber(Math.floor(this.countNum)) + currency);
                if (callback) {
                    callback();
                }
            }
        }
    );
}

function updateNavIfUnreadNotifications() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Notifications/GetUnreadCount",
        success: function(data)
        {
            if (data.success) {
                var unreadNotificationsCount = data.object.notifications;
                var unreadMessagesCount = data.object.messages;
                if (unreadNotificationsCount > 0){
                    $('#unreadNotificationsCount').text(formatNumberShortened(unreadNotificationsCount));
                    $('.notifications').addClass('alert-color');
                }
                else {
                    $('#unreadNotificationsCount').text("");
                    $('.notifications').removeClass('alert-color');
                }
                if (unreadMessagesCount > 0) {
                    $('.unreadMessagesCount').text(formatNumberShortened(unreadMessagesCount));
                    $('.messages').addClass('alert-color');
                }
                else {
                    $('.unreadMessagesCount').text("");
                    $('.messages').removeClass('alert-color');
                }
                updateMenu(unreadNotificationsCount + unreadMessagesCount);
            }
        }
    });
}

function updateMenu(amount) {
    var documentTitle = document.title.split('(')[0].trim();
    if (amount > 0) {
        $('#navbar-toggle').addClass('alert-background');
        $('#navbar-toggle').html('<b><span class="glyphicon glyphicon-bell"></span> ' + formatNumberShortened(amount) + '</b>');
        $('#userDropdownIcon').hide();
        $('#userDropdownUnreadCount').text(formatNumberShortened(amount));
        document.title = documentTitle + " (" + formatNumberShortened(amount) + ")";
    }
    else {
        $('#navbar-toggle').removeClass('alert-background');
        $('#navbar-toggle').html('<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>');
        $('#userDropdownIcon').show();
        $('#userDropdownUnreadCount').text("");
        document.title = documentTitle;
    }
}

function getUnreadMessages(tries = 1) {
    clearTimeout(updateNavTimer);
    clearTimeout(resendNavTimer);
    if (tries > 10) {
        return;
    }
    updateNavTimer = setTimeout(function(){
        updateNavIfUnreadNotifications();
        resendNavTimer = setTimeout(function () {
            getUnreadMessages(tries + 1);
        }, (tries + 1) * 10000);
    }, 250);
}

function showToastIfExists() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetToast",
        success: function(data)
        {
            if (data.success){
                for (var i = 0; i < data.messages.length; i++) {
                    if (data.messages[i][3] == "True") {
                        if (data.messages[i][4] == "True") {
                            showSnackbarWarning(data.messages[i][1]);
                        }
                        else {
                            showSnackbarSuccess(data.messages[i][1]);
                        }
                    }
                    else {
                        showToastr(data.messages[i][0], data.messages[i][1], data.messages[i][2]);
                    }
                }
            }
        }
    });
}

function sendCourierMessage() {
    if (sentCourierMessage) {
        return;
    }
    $.ajax({
        type: "Post",
        cache: false,
        url: "/Messages/SendCourierMessage",
        data: { "__RequestVerificationToken" : $('input[name=__RequestVerificationToken]').val() }
    });
    sentCourierMessage = true;
}

function showToastr(title, message, url){
    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": true,
      "showDuration": "500"
    }
    if (url){
        toastr.options.onclick = function() { window.location.href = url; }
    }
    toastr.success(message, title);
}

function showSnackbarSuccess(text) {
    Snackbar.show({ text: text, backgroundColor: '#dff0d8', textColor: '#3c763d', actionTextColor: '#3c763d', pos: 'bottom-center' });
}

function showSnackbarInfo(text) {
    Snackbar.show({ text: text, backgroundColor: '#D9EDF7', textColor: '#0C5875', actionTextColor: '#0C5875', pos: 'bottom-center' });
}

function showSnackbarWarning(text) {
    Snackbar.show({ text: text, backgroundColor: '#fff3cd', textColor: '#7d6728', actionTextColor: '#7d6728', pos: 'bottom-center' });
}

function copyText(text){
    Clipboard.copy(text);
    showSnackbarSuccess('The link was successfully copied!');
}

function hideAllMessages() {
    $("#alertMessage").hide();
    $("#successMessage").hide();
    $("#infoMessage").hide();
}

function showErrorMessage(errorMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#alertMessage").fadeIn(fadeIn ? 250 : 0);
    $("#alertMessageText").text(errorMessage);
    if (buttonText) {
        $("#warningMessageButton").show();
        $("#warningMessageButtonText").text(buttonText)
        $("#warningMessageButtonText").attr("href", buttonUrl);
    }
}

function showSuccessMessage(successMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
        $("#successMessage").fadeIn(fadeIn ? 250 : 0);
        $("#successMessageText").text(successMessage);
        if (buttonText) {
            $("#successMessageButton").show();
            $("#successMessageButtonText").text(buttonText)
            $("#successMessageButtonText").attr("href", buttonUrl);
        }
    }
    else {
        showSnackbarSuccess(successMessage);
    }
}

function showInfoMessage(infoMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#infoMessage").fadeIn(fadeIn ? 250 : 0);
    $("#infoMessageText").text(infoMessage);
    if (buttonText) {
        $("#infoMessageButton").show();
        $("#infoMessageButtonText").text(buttonText)
        $("#infoMessageButtonText").attr("href", buttonUrl);
    }
}

function showDangerMessage(dangerMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#dangerMessage").fadeIn(fadeIn ? 250 : 0);
    $("#dangerMessageText").text(dangerMessage);
    if (buttonText) {
        $("#dangerMessageButton").show();
        $("#dangerMessageButtonText").text(buttonText)
        $("#dangerMessageButtonText").attr("href", buttonUrl);
    }
}

function updatePoints(points) {
    var mobilePoints = $('#totalPoints-mobile');
    var totalPoints = $('#totalPoints');
    var updatedPoints = getIntFromPoints(points);
    var currentPoints = getIntFromPoints(totalPoints.text());
    var currency =  totalPoints.text().split(' ')[1];

    countToNumber(mobilePoints, updatedPoints, currentPoints, " " + currency);
    countToNumber(totalPoints, updatedPoints, currentPoints, " " + currency);
}

function getIntFromPoints(points) {
    var pointsWithoutCurrency = points.split(' ')[0];
    return parseInt(pointsWithoutCurrency.replace(/,/g, ""));
}

function stringToColor(str){
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var color = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return color;
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }

    return decodeURI(dc.substring(begin + prefix.length, end));
}

function goToPointsLocation() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Preferences/GetPointsLocation",
        success: function(data)
        {
            if (data.success){
                window.location = data.returnUrl;
            }
            else {
                window.location = "/Items";
            }
        }
    });
}

$.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ')
    });
    return this;
};

function toggleMessagesBox() {
    if ($('#messages-box').is(":hidden")) {
        $('#messages-box').slideDown();
        updateMessagesBox();
    }
    else {
        $('#messages-box').slideUp();
    }
}

function updateMessagesBox(username, showSpinner) {
    if (showSpinner) {
        $("#messages-box").html('<div class="extra-large-padding-top"></div><div class="loader center-block"></div>');
    }
    $.ajax({
        type: "GET",
        url: "/Messages/GetMessagesBoxPartialView",
        data: { username: username },
        success: function (data) {
            $("#messages-box").html(data);
            onPageLoad(false, false, false);
            onMessagesLoad(false);
            pollForNewMessages(true);
        }
    });
}

$("#loginAccountForm").submit(function(e) {
    var form = $(this);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location.href = "/" + data.returnUrl;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
});

function onLoginForgotSubmit(e, form) {
    form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
    form[0].reset();
}
$(document).ready(function(){
    onItemsLoad();
});

function onItemsLoad(){
    if ($('#modal-items-tutorial').length) {
        $('#modal-items-tutorial').modal();
    }
    if ($('#modal-relic-requirements').length) {
        $('#modal-relic-requirements').modal();
    }
}

function companionColorSelected(color) {
    var color = $(color);
    var colorId = color.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedColor').val() == colorId){
        $('#selectedColor').val("");
    }
    else{
        $('#selectedColor').val(colorId);
        color.addClass('itemSelected');
    }
}

function updateItems(e, form, openCardPack = false, loadPowerMoves = false, container = null) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshItemsPartialView(openCardPack, loadPowerMoves, container);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function updateGuide(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshItemsGuidePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function highlightRecentItems() {
    $('html, body').animate({
        scrollTop: $('#items').offset().top - 50
    }, 500);
    $('.fade-out-border').each(function () {
        var recentItem = $(this);
        $(recentItem).removeClass('fade-out-border');
        setTimeout(function () {
            $(recentItem).addClass('fade-out-border');
        });
    });
}

function openCardPackItem() {
    if ($('#modal-items-open-card-pack').length) {
        $('#modal-items-open-card-pack').modal();
        var timer = 50;
        $('.item').each(function () {
            var item = $(this);
            setTimeout(function () {
                $(item).show("slide", { direction: "left" }, 250);
            }, timer)
            timer += 250;
        });
        setTimeout(function () {
            pushDownScrollClass();
        }, 2500);
    }
}

function refreshItemsPartialView(openCardPack, loadPowerMoves, container) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Items/GetItemsPartialView",
        data: { loadPowerMoves : loadPowerMoves },
        success: function(data)
        {
            var containerId = "";
            var lastPlacement = 0;
            if (container != null && $(container).length) {
                containerId = $(container).attr('id');
                lastPlacement = $(container).scrollLeft();
            }
            $("#itemsPartialView").html(data);
            onPageLoad(false);
            onItemsLoad();
            if (openCardPack) {
                openCardPackItem();
            }
            if (lastPlacement > 0) {
                $('#' + containerId).scrollLeft(lastPlacement);
            }
        }
    });
}

function refreshItemsGuidePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Items/GetItemsGuidePartialView",
        success: function(data)
        {
            $("#itemsGuidePartialView").html(data);
            onPageLoad();
        }
    });
}
var stadiumPageChanged = false;
var roundPowerIndex = 0;
var hasSeenEnhancersTutorial = false;
var hasSeenPowerMovesTutorial = false;
var successAudio = null;
var negativeAudio = null;

$(document).ready(function(){
    onStadiumLoad();
});

function onStadiumLoad(showModals = true) {
    stadiumPageChanged = !stadiumPageChanged;
    roundPowerIndex = 0;
    var secondsLeft = $('#match-secondsLeft').text();
    if (secondsLeft){
        countdownNumber(secondsLeft, stadiumPageChanged);
    }
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "true"){
        pollForOpponentsMove();
    }
    var shouldPollForOpponentWaitlist = $('#shouldPollForOpponentWaitlist');
    if (shouldPollForOpponentWaitlist && shouldPollForOpponentWaitlist.val() === "true"){
        pollForOpponentWaitlist(0, $('#pollForOpponentUsername').val());
        updateStadiumTip();
    }
    if ($('.stadium-counter').length) {
        $('#match-result').hide();
        $('#stadiumForm-duel').hide();
        $('#enhancers').hide();
        $('#power-moves').hide();
        $('#bonuses').hide();
        $('#bonuses-opponent').hide();
        if ($('#opponentRoundPower').length) {
            setTimeout(function () {
                $('#round-power').fadeIn(500, function () {
                    beginStadiumAnimation();
                });
            }, 500);
        }
    }
    if ($('.global-ranking-counter').length) {
        $('.global-ranking-counter').each(function() {
            var counter = this;
            var triggerAtY = $(counter).offset().top - $(window).outerHeight();
            var countTo = $(counter).attr('data-count');
            var countFrom = $(counter).attr('data-start-count');
            if (countTo == countFrom) {
                $(counter).text(countTo);
            }
            else {
                if (triggerAtY <= $(window).scrollTop()) {
                    countToNumber($(counter), countTo, countFrom);
                }
                else {
                    $(window).scroll(function(event) {
                        triggerAtY = $(counter).offset().top - $(window).outerHeight();
                        if (triggerAtY > $(window).scrollTop()) {
                            return;
                        }
                        countToNumber($(counter), countTo, countFrom);
                        $(this).off(event);
                    });
                }
            }
        });
    }
    if ($('.stadium-message').length) {
        $('.stadium-message').each(function () {
            $(this).hide();
        });
    }
    if ($('.global-level-counter').length) {
        $('#level-up').hide().slideDown(500);
        $('.global-level-counter').each(function () {
            var counter = this;
            var countTo = $(counter).attr('data-count');
            setTimeout(function () {
                countToNumber($(counter), countTo, 1, "", hideStadiumLevelUp);
            }, 250);
        });
    }
    else {
        setTimeout(function () {
            showStadiumFinalMessages();
        }, 350);
    }

    var tutorialWaitTime = 0;
    if ($('#stadium-match-intro').length) {
        hideAllMessages();
        $('#match-type').show("slide", { direction: "left" }, 250);
        $('#user-circle').slideDown(500);
        $('#opponent-circle').slideDown(500);
        $('#versus-label').fadeIn(500);
        $("#versus-label").effect("bounce", 500);
        setTimeout(function () {
            $('#match-type').fadeOut(500);
            $('#stadium-match-intro').slideUp(500);
            $('#stadium-select').slideDown(500);
        }, 2000);
        tutorialWaitTime = 2500;
    }

    if (showModals) {
        setTimeout(function () {
            if ($('#modal-stadium-tutorial').length) {
                $('#modal-stadium-tutorial').modal();
            }
            if ($('#modal-stadium-elite-zone-tutorial').length) {
                $('#modal-stadium-elite-zone-tutorial').modal();
            }
            if ($('#modal-relic-requirements').length) {
                $('#modal-relic-requirements').modal();
            }
        }, tutorialWaitTime);
        if ($('#modal-stadium-lost-tutorial').length) {
            $('#modal-stadium-lost-tutorial').modal();
        }
    }
}

function concedeStadiumMatch() {
    if (confirm("Are you sure you want to concede this match? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Stadium/ConcedeMatch",
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshStadiumPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshStadiumPartialView();
                    }
                }
            }
        });
    }
}

function hideStadiumLevelUp() {
    setTimeout(function() {
        $('#level-up').slideUp(500);
    }, 2500);
    showStadiumFinalMessages();
}

function showStadiumFinalMessages() {
    $('.stadium-message').each(function (index) {
        var stadiumMessage = this;
        setTimeout(function () {
            $(stadiumMessage).slideDown(500);
        }, (index + 1) * 250);
    });
}

function stadiumRechargeUses(e, form, hasEnoughPoints) {
    if (hasEnoughPoints) {
        submitStadiumForm(e, form, false, true)
    }
    else {
        e.preventDefault();
        stopLoadingButton();
        showSnackbarWarning("You don't have enough CD on hand to recharge this Enhancer.");
    }
}

function skipStadiumCPU(e, form, challengerName, level, cost) {
    if ($('#skip-button').hasClass('disabled') || confirm("Are you sure you want to skip " + unescape(challengerName) + " and move on to the Level " + level + " CPU for " + cost + "? You'll lose any rewards you'd get for winning the match.")) {
        submitStadiumForm(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitStadiumForm(e, form, scrollToTop = true, reselectCards = false) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.shouldRefresh) {
                refreshStadiumPartialView();
                if (data.success) {
                    return;
                }
            }
            if (data.returnUrl) {
                window.location.href = data.returnUrl;
                return;
            }
            if (data.canPlaySound) {
                successAudio = new Audio("/sounds/Success_Round.mp3");
                negativeAudio = new Audio("/sounds/Defeat_Round.mp3");
            }
            if (data.success) {
                refreshStadiumPartialView(scrollToTop, false, reselectCards);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                if (data.showSpecialMessage) {
                    stopLoadingButton();
                    $('#checkpoint-cpu-requirements').text(data.errorMessage);
                    if (data.buttonText) {
                        $('#checkpoint-cpu-button').show();
                        $('#checkpoint-cpu-button-text').text(data.buttonText);
                        $('#checkpoint-cpu-button-url').attr('href', data.buttonUrl);
                    }
                    $('#modal-checkpoint-cpu-requirements').modal();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
                }
            }
        }
    });

    e.preventDefault();
}

function beginStadiumAnimation() {
    var roundPowers = $('#roundPower').attr('data-round-powers').split("-");
    var opponentRoundPowers = $('#opponentRoundPower').attr('data-round-powers').split("-");
    var roundPower = parseInt(roundPowers[roundPowerIndex]);
    var opponentRoundPower = parseInt(opponentRoundPowers[roundPowerIndex]);
    var userFaded = JSON.parse($('#user-faded').val());
    var opponentFaded = JSON.parse($('#opponent-faded').val());
    var userBackfired = JSON.parse($('#user-backfired').val());
    var opponentBackfired = JSON.parse($('#opponent-backfired').val());
    if (roundPowerIndex == 1) {
        userFaded[1][0] ? $('#card').addClass('faded') : $('#card').removeClass('faded');
        opponentFaded[1][0] ? $('#opponent-card').addClass('faded') : $('#opponent-card').removeClass('faded');

        userBackfired[1][0] ? $('#card').addClass('lose-red-border') : $('#card').removeClass('lose-red-border');
        opponentBackfired[1][0] ? $('#opponent-card').addClass('lose-red-border') : $('#opponent-card').removeClass('lose-red-border');
    }
    else if (roundPowerIndex == 2) {
        userFaded[2][0] ? $('#card').addClass('faded') : $('#card').removeClass('faded');
        opponentFaded[2][0] ? $('#opponent-card').addClass('faded') : $('#opponent-card').removeClass('faded');
        userFaded[2][1] ? $('#enhancer').addClass('faded') : $('#enhancer').removeClass('faded');
        opponentFaded[2][1] ? $('#opponent-enhancer').addClass('faded') : $('#opponent-enhancer').removeClass('faded');

        userBackfired[2][0] ? $('#card').addClass('lose-red-border') : $('#card').removeClass('lose-red-border');
        opponentBackfired[2][0] ? $('#opponent-card').addClass('lose-red-border') : $('#opponent-card').removeClass('lose-red-border');
        userBackfired[2][1] ? $('#enhancer').addClass('lose-red-border') : $('#enhancer').removeClass('lose-red-border');
        opponentBackfired[2][1] ? $('#opponent-enhancer').addClass('lose-red-border') : $('#opponent-enhancer').removeClass('lose-red-border');
    }

    /* -1 = loss, 1 = win, 2 = draw */
    var roundResult = 2;
    if (roundPowerIndex < 2) {
        roundResult = roundPower > opponentRoundPower ? 1 : roundPower < opponentRoundPower ? -1 : 2;
    }
    else {
        roundResult = parseInt($('#roundResult').val());
    }
    var greatestDiffIndex = 1;
    if (roundPowerIndex == 0) {
        greatestDiffIndex = roundResult == 1 ? 0 : 1;
    }
    else if (Math.abs(roundPower - parseInt(roundPowers[roundPowerIndex - 1])) > Math.abs(opponentRoundPower - parseInt(opponentRoundPowers[roundPowerIndex - 1]))) {
        greatestDiffIndex = 0;
    }
    $('.stadium-counter').each(function (index) {
        incrementPower(this, index == greatestDiffIndex, roundResult == 2 ? 2 : index == 0 ? roundResult : -roundResult);
    });
}

function addStadiumPowerMoveAnimations() {
    if ($('#swapEnhancers').val() == "true") {
        if ($('#user-enhancer-label').is(":hidden") && $('#opponent-enhancer-label').is(":visible")) {
            $('#user-enhancer-label').show();
            $('#opponent-enhancer-label').hide();
        }
        else if ($('#opponent-enhancer-label').is(":hidden") && $('#user-enhancer-label').is(":visible")) {
            $('#opponent-enhancer-label').show();
            $('#user-enhancer-label').hide();
        }
        $('#user-enhancer-col').find("#enhancer").attr("id", "opponent-enhancer");
        var userEnhancer = $('#user-enhancer-col').html();
        $('#opponent-enhancer-col').find("#opponent-enhancer").attr("id", "enhancer");
        var opponentEnhancer = $('#opponent-enhancer-col').html();
        $('#user-enhancer-col').html(opponentEnhancer);
        $('#opponent-enhancer-col').html(userEnhancer);
    }
    if ($('#swapCards').val() == "true") {
        $('#user-card-col').find("#card").attr("id", "opponent-card");
        var userCard = $('#user-card-col').html();
        $('#opponent-card-col').find("#opponent-card").attr("id", "card");
        var opponentCard = $('#opponent-card-col').html();
        $('#user-card-col').html(opponentCard);
        $('#opponent-card-col').html(userCard);
    }
    if ($('#mirrorPowerMoves').val() != "0") {
        var userPowerMove = $('#user-power-move-col').html();
        var opponentPowerMove = $('#opponent-power-move-col').html();
        if ($('#mirrorPowerMoves').val() == "1") {
            if ($('#opponent-power-move-label').is(":hidden")) {
                $('user-power-move').addClass('faded');
            }
            else {
                $("#user-power-move-col").effect("bounce", { distance: 15 }, 1000, function () {
                    $('#user-power-move-col').html(opponentPowerMove);
                    var powerMove = $('#user-power-move-col').find('#opponent-power-move');
                    if (!$(powerMove).hasClass('win-green-border') && !$(powerMove).hasClass('lose-red-border') && !$(powerMove).hasClass('faded')) {
                        $(powerMove).addClass('primary-color-border');
                    }
                });
            }
        }
        else if ($('#mirrorPowerMoves').val() == "-1") {
            if ($('#user-power-move-label').is(":hidden")) {
                $('opponent-power-move').addClass('faded');
            }
            else {
                $("#opponent-power-move-col").effect("bounce", { distance: 15 }, 1000, function () {
                    $('#opponent-power-move-col').html(userPowerMove);
                    var powerMove = $('#opponent-power-move-col').find('#power-move');
                    if (!$(powerMove).hasClass('win-green-border') && !$(powerMove).hasClass('lose-red-border') && !$(powerMove).hasClass('faded')) {
                        $(powerMove).addClass('primary-color-border');
                    }
                });
            }
        }
    }
}

function incrementPower(counter, shouldContinue, roundResult) {
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "false") {
        var roundPowers = $(counter).attr('data-round-powers').split("-");
        var countTo = parseInt(roundPowers[roundPowerIndex]);
        var countFrom = parseInt(roundPowerIndex == 0 ? 0 : roundPowers[roundPowerIndex - 1]);
        if (countTo != countFrom) {
            countToNumber($(counter), countTo, countFrom, "", shouldContinue ? stadiumRoundResultLoaded : null);
        }
        else if (shouldContinue) {
            stadiumRoundResultLoaded();
        }
        $(counter).removeClassStartingWith('win-green-animated');
        $(counter).removeClassStartingWith('lose-red-animated');
        if (roundResult < 2) {
            var newClass = roundResult == 1 ? "win-green-animated" : "lose-red-animated";
            if (countTo < 20) {
                newClass += "-500ms";
            }
            else if (countTo < 40) {
                newClass += "-1000ms";
            }
            else {
                newClass += "-1500ms";
            }
            $(counter).addClass(newClass)
        };
    }
}

function stadiumRoundResultLoaded() {
    roundPowerIndex++;
    if (roundPowerIndex == 1) {
        if ($('#enhancers').attr('data-count') == 0) {
            stadiumRoundResultLoaded();
        }
        else {
            setTimeout(function () {
                $('#enhancers').fadeIn(500, function () {
                    $('html, body').animate({
                        scrollTop: $('#enhancers').offset().top - 100
                    }, 500).promise().then(function () {
                        beginStadiumAnimation();
                    });
                });
            }, 500);
        }
        return;
    }
    else if (roundPowerIndex == 2) {
        if ($('#power-moves').attr('data-count') == 0) {
            beginStadiumAnimation();
        }
        else {
            setTimeout(function () {
                $('#power-moves').fadeIn(500, function () {
                    $('html, body').animate({
                        scrollTop: $('#power-moves').offset().top - 100
                    }, 500).promise().then(function () {
                        addStadiumPowerMoveAnimations();
                        beginStadiumAnimation();
                    });
                });
            }, 500);
        }
        setTimeout(function () {
            $('#bonuses').fadeIn(500);
            $('#bonuses-opponent').fadeIn(500);
        }, 500);
        return;
    }
    else {
        var roundResult = parseInt($('#roundResult').val());
        if (roundResult == 1) {
            $('#round-power-user').addClass('animated tada');
        }
        else if (roundResult == -1) {
            $('#round-power-opponent').addClass('animated tada');
        }
        $('.round-power').addClass('animated tada');
        setTimeout(function () {
            $('#match-result').fadeIn(500, function () {
                $('#stadiumForm-duel').fadeIn(500);
                $('html, body').animate({
                    scrollTop: $('#match-result').offset().top - 100
                }, 500);
                if ($('#modal-stadium-tutorial-4').length) {
                    $('#next-round-button').addClass('disabled');
                    $('#next-round-button').attr('disabled', 'disabled');
                    setTimeout(function () {
                        $('#modal-stadium-tutorial-4').modal();
                        $('#next-round-button').removeClass('disabled');
                        $('#next-round-button').removeAttr('disabled');
                    }, 1500);
                }
                if (roundResult == 1 && successAudio) {
                    successAudio.play();
                }
                else if (roundResult == -1 && negativeAudio) {
                    negativeAudio.play();
                }
            });
        }, 500);
        return;
    }
}

function enhancerSelected(enhancer) {
    var enhancer = $(enhancer);
    var enhancerId = enhancer.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#enhancerId').val() != ''){
        $('#enhancerId').val("");
        $('.enhancer').fadeIn(250);
        $('#enhancer-selected-dot').attr('data-prefix', 'far');
        $('#enhancer-selected-dot').attr('data-icon', 'circle');
        $('#expand-enhancers').hide();
    }
    else{
        $('#enhancerId').val(enhancerId);
        enhancer.addClass('itemSelected');
        $('.enhancer').each(function () {
            if (!$(this).hasClass('itemSelected')) {
                $(this).hide();
            }
        });
        $('#enhancer-selected-dot').attr('data-prefix', 'fas');
        $('#enhancer-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-enhancers').show();
        $('html, body').animate({
            scrollTop: $('#power-moves').offset().top - 50
        }, 500);
    }

    if ($('#modal-stadium-tutorial-3').length && !hasSeenPowerMovesTutorial) {
        $('#modal-stadium-tutorial-3').modal();
        hasSeenPowerMovesTutorial = true;
    }
}

function powerMoveSelected(powerMove, scrollToPosition = true) {
    var powerMove = $(powerMove);
    var powerMoveId = powerMove.attr("id");

    $('.powerMoveSelected').removeClass('powerMoveSelected');

    if ($('#powerMoveId').val() != '') {
        $('#powerMoveId').val("");
        $('.power-move').fadeIn(250);
        $('#power-move-selected-dot').attr('data-prefix', 'far');
        $('#power-move-selected-dot').attr('data-icon', 'circle');
        $('#expand-power-moves').hide();
    }
    else {
        $('#powerMoveId').val(powerMoveId);
        powerMove.addClass('powerMoveSelected');
        $('.power-move').each(function () {
            if (!$(this).hasClass('powerMoveSelected')) {
                $(this).hide();
            }
        });
        $('#power-move-selected-dot').attr('data-prefix', 'fas');
        $('#power-move-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-power-moves').show();
        if (scrollToPosition) {
            $('html, body').animate({
                scrollTop: $('#stadiumForm').offset().top - 50
            }, 500);
        }
    }
}

function deckCardSelected(card, scrollToPosition = true) {
    var card = $(card);
    var cardId = card.attr("id");

    $('.cardSelected').removeClass('cardSelected');
    $('#play-round-button').addClass('disabled');

    if ($('#deckCardId').val() != ''){
        $('#deckCardId').val("");
        $('#enhancers').hide();
        $('#power-moves').hide();
        $('.deck-card').fadeIn(250);
        $('#card-selected-dot').attr('data-prefix', 'far');
        $('#card-selected-dot').attr('data-icon', 'circle');
        $('#expand-cards').hide();
    }
    else {
        $('#deckCardId').val(cardId);
        card.addClass('cardSelected');
        $('#enhancers').fadeIn(250);
        $('#power-moves').fadeIn(250);
        $('#play-round-button').removeClass('disabled');
        $('.deck-card').each(function () {
            if (!$(this).hasClass('cardSelected')) {
                $(this).hide();
            }
        });
        if ($('#modal-stadium-tutorial-2').length && !hasSeenEnhancersTutorial) {
            $('#modal-stadium-tutorial-2').modal();
            hasSeenEnhancersTutorial = true;
        }
        $('#card-selected-dot').attr('data-prefix', 'fas');
        $('#card-selected-dot').attr('data-icon', 'check-circle');
        $('#expand-cards').show();
        if (scrollToPosition) {
            $('html, body').animate({
                scrollTop: $('#enhancers').offset().top - 50
            }, 500);
        }
    }
}

function pollForOpponentsMove(tries = 0) {
    var shouldPollForOpponentsMove = $('#shouldPollForOpponentsMove');
    if (!(shouldPollForOpponentsMove && shouldPollForOpponentsMove.val() === "true")){
        return;
    }
    hideAllMessages();
    if (tries == 15){
        showSnackbarInfo("Your opponent never made a move!");
        refreshStadiumPartialView();
        return;
    }
    if (tries >= 10){
        showInfoMessage("Your opponent still hasn't made a move...", tries == 10, tries == 10);
    }

    $.ajax({
        type: "GET",
        url: "/Stadium/PollForOpponentsMove",
        success: function(data)
        {
            if (data.success){
                refreshStadiumPartialView(false, true);
            }
            else{
                if (data.stopExecuting) {
                    hideAllMessages();
                    return;
                }
                tries += 1;
                setTimeout(function(){
                    pollForOpponentsMove(tries);
                }, tries * 500);
            }
        }
    });
}

function pollForOpponentWaitlist(tries, challengeUsername) {
    var shouldPollForOpponentWaitlist = $('#shouldPollForOpponentWaitlist');
    if (!(shouldPollForOpponentWaitlist && shouldPollForOpponentWaitlist.val() === "true")){
        return;
    }
    hideAllMessages();
    if (tries >= 60) {
        showSnackbarWarning("You timed out! Try searching for a new opponent.");
        refreshStadiumPartialView();
        return;
    }
    if (tries >= 10){
        showInfoMessage(challengeUsername == "" ? "Still looking for an opponent..." : "Still waiting for " + challengeUsername + " to accept your challenge request...", tries == 10, tries == 10);
    }
    $.ajax({
        type: "GET",
        url: "/Stadium/PollForOpponentWaitlist",
        data: { tries : tries },
        success: function(data)
        {
            if (data.success){
                refreshStadiumPartialView();
            }
            else{
                if (data.stopExecuting) {
                    hideAllMessages();
                    showSnackbarWarning("You timed out! Try searching for a new opponent.");
                    refreshStadiumPartialView();
                    return;
                }
                tries += 1;
                setTimeout(function(){
                    pollForOpponentWaitlist(tries, challengeUsername);
                }, 1500);
            }
        }
    });
}

function countdownNumber(number, pageChanged) {
    if (pageChanged != stadiumPageChanged){
        return;
    }
    var countDown =  $("#countdown-match");
    if(!countDown.length){
        return;
    }
    if (number <= 0){
        countDown.html("<span class='lose-red'>The match ran out of time!</span>");
        return;
    }
    else {
        if (number == 30) {
            showSnackbarWarning(($('#roundResult').length ? "There's only <b>30 seconds</b> left in this round." : "You only have <b>30 seconds</b> left to choose a card.") + " If the timer runs out, the match will end.");
        }
        $('#match-secondsLeftCountdown').html("<b class='" + (number <= 10 ? 'lose-red' : '') + "'>" + number + " second" + (number == 1 ? "" : "s") + "</b>");
    }
    setTimeout(function(){
        countdownNumber(number - 1, pageChanged);
    }, 1000);
}

function refreshStadiumPartialView(scrollToTop = true, scrollToTopAnimated = false, reselectCards = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Stadium/GetStadiumPartialView",
        success: function(data)
        {
            if (reselectCards) {
                var selectedCard = $('.cardSelected');
                var selectedPowerMove = $('.powerMoveSelected');
            }
            $("#stadiumPartialView").html(data);
            onPageLoad(scrollToTop, !reselectCards);
            onStadiumLoad(!reselectCards);
            if (reselectCards) {
                if ($(selectedCard).length) {
                    var card = $('#' + selectedCard.attr('id'));
                    deckCardSelected(card, !reselectCards);
                }
                if ($(selectedPowerMove).length) {
                    var powerMove = $('#' + selectedPowerMove.attr('id'));
                    powerMoveSelected(powerMove, !reselectCards);
                }
                $('html, body').animate({
                    scrollTop: $('#enhancers').offset().top - 50
                }, 500);
            }
            if ($('#challenge-tabs').length) {
                $('html, body').animate({
                    scrollTop: $('#challenge-tabs').offset().top - 50
                }, 500);
            }
            else if (!scrollToTop && scrollToTopAnimated) {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            }
        }
    });
}

function scrollToChallengesTab() {
    if ($('#challenge-tabs').length) {
        $('html, body').animate({
            scrollTop: $('#challenge-tabs').offset().top - 50
        }, 500);
    }
}

function updateStadiumTip(subsequentLoad = false){
    var stadiumTip = $('#stadiumTip');
    if (!stadiumTip.length){
        return;
    }
    if (subsequentLoad){
        $.ajax({
            type: "GET",
            url: "/Stadium/GetTip",
            success: function(data)
            {
                if (data.success) {
                    stadiumTip.fadeOut(250, function () {
                        stadiumTip.text(data.successMessage);
                        stadiumTip.fadeIn(250);
                    });
                }
            }
        });
    }
    setTimeout(function(){
        updateStadiumTip(true);
    }, 7500);
}
function updateBank(e, form, isBank) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshBankPartialView(isBank)
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshBankPartialView(isBank) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isBank : isBank },
        url: "/Bank/GetBankPartialView",
        success: function(data)
        {
            $("#bankPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateStore(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success == false){
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            else{
                $("#storePartialView").html(data);
                onPageLoad();
            }
        }
    });

    e.preventDefault();
};

function submitBargainForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    refreshStorePartialView();
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshStorePartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Store/GetStorePartialView",
        success: function(data)
        {
            $("#storePartialView").html(data);
            onPageLoad();
        }
    });
}
function updateJunkyard(e, form, isBuy) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshJunkyardPartialView(isBuy);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshJunkyardPartialView(isBuy);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshJunkyardPartialView(isBuy) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Junkyard/GetJunkyardPartialView",
        data: {isBuy : isBuy},
        success: function(data)
        {
            $("#junkyardPartialView").html(data);
            onPageLoad(false);
        }
    });
}
function updateRepair(e, form, isRecharge, fromStadium) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshRepairPartialView(isRecharge, fromStadium);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshRepairPartialView(isRecharge, fromStadium);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshRepairPartialView(isRecharge, fromStadium) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isRecharge : isRecharge, fromStadium : fromStadium },
        url: "/Repair/GetRepairPartialView",
        success: function(data)
        {
            $("#repairPartialView").html(data);
            onPageLoad(false, false);
        }
    });
}
var locationsTimer = null;

$(document).ready(function () {
    onLocationsLoad();
});

function onLocationsLoad() {
    if ($('#modal-locations-tutorial').length) {
        $('#modal-locations-tutorial').modal();
    }
    if ($('#lastLocation').length) {
        var lastLocation = '#' + $('#lastLocation').val();
        if ($(lastLocation).length) {
            if ($(lastLocation).hasClass('display-none')) {
                showRemainingLocations($(lastLocation).parent().find('.flex-scrolling-element'));
            }
            $(window).scrollTop($(lastLocation).offset().top - 50);
            $(lastLocation).parent().scrollLeft($(lastLocation).offset().left - $(lastLocation).width() - 50);
        }
    }
    if ($('#Favorites-container').length) {
        $("#Favorites-container").sortable({
            items: ".location-element",
            handle: ".draggable-bar",
            axis: $('.grid-location-container').length ? "" : "x",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var locationId = el.item.attr("id");
                var url = "/Plaza/UpdatePosition";
                $('.location-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, locationId: locationId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshPlazaPartialView();
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.location-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $("draggable-bar").disableSelection();
}

function delayedKeyUpUpdateLocations(e, form, scrollToTop = true) {
    clearTimeout(locationsTimer);
    locationsTimer = setTimeout(function () {
        updateLocations(e, form, scrollToTop);
    }, 250);
};

function hideShowPlazaCategory(plazaCategoryId) {
    var plazaCategorySection = $('.' + plazaCategoryId + "-section");
    if ($(plazaCategorySection).is(":visible")) {
        $(plazaCategorySection).slideUp();
        $('#' + plazaCategoryId + '-chevron').attr('data-icon', 'chevron-down');
    }
    else {
        $(plazaCategorySection).slideDown();
        $('#' + plazaCategoryId + '-chevron').attr('data-icon', 'chevron-up');
    }
};

function showRemainingLocations(el) {
    var parent = $(el).parent();
    $(parent).children().each(function () {
        var child = $(this);
        if ($(child).hasClass('display-none')) {
            $(child).fadeIn();
        }
    });
    $(el).hide();
}

function togglePlazaView() {
    $.ajax({
        type: "POST",
        url: "/Plaza/ToggleView",
        success: function () {
            window.location.href = window.location.pathname;
        }
    });
}

function updateLocation(e, form, locationId, key){
    $.ajax({
        type: "POST",
        url: "/Plaza/UpdateLocation",
        data: { locationId : locationId },
        success: function()
        {
            hideAllMessages();
            updateLocations(e, form, false, key);
        }
    });

    e.preventDefault();
}

function updateLocations(e, form, scrollToTop = true, expandKey = "") {
    form = $(form);
    var url = "/Plaza/GetPlazaPartialView";
    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            var lastPlacement = 0;
            if (expandKey != "") {
                lastPlacement = $('#' + expandKey).find('.scrolling-container').scrollLeft();
            }
            $("#plazaPartialView").html(data);
            if (expandKey != "") {
                showRemainingLocations($('#' + expandKey + '-expand'));
                $('#' + expandKey).find('.scrolling-container').scrollLeft(lastPlacement);
            }
            onPageLoad(scrollToTop);
            onLocationsLoad();
        }
    });

    e.preventDefault();
};

function refreshPlazaPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Plaza/GetPlazaPartialView",
        success: function (data) {
            $("#plazaPartialView").html(data);
            onPageLoad();
            onLocationsLoad();
        }
    });
}
$(document).ready(function(){
    onPreferencesLoad();
});

function onPreferencesLoad() {
    var dropdownValue = $('#pointsLocationDropdownValue').val();
    if (dropdownValue){
        $('#pointsLocation').val(dropdownValue);
        $('#pointsLocation').selectpicker('val', dropdownValue);
    }
    var dropdownValueBadge = $('#activeBadgeLocation').val();
    if (dropdownValueBadge) {
        $('#activeBadge').val(dropdownValueBadge);
        $('#activeBadge').selectpicker('val', dropdownValueBadge);
    }
    if ($('#enableMessagesTab').length) {
        if ($('#enableMessagesTab').is(":checked")) {
            $('#messages-box-button').show();
        }
        else {
            $('#messages-box-button').hide();
        }
    }
}

function updateBioCountMessage(){
    var bioLength = $('#bio').val().length;
    var remainingLength = 350 - bioLength;
    $('#bioCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function updateNotesCountMessage() {
    var notesLength = $('#notes').val().length;
    var remainingLength = 2500 - notesLength;
    $('#notesCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function removeDisplayPic() {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveDisplayPic",
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                $('#upload-pic').show();
                $('#existing-pic').hide();
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}


function resendEmailVerification() {
    $.ajax({
        type: "POST",
        url: "/Preferences/ResendEmailVerification",
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function removeBlockedUser(username) {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveBlockedUser",
        data: { username : username },
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, false);
                }
                refreshPreferencesPartialView(false)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function updatePreferencesCircle(color) {
    $('.small-circle ').css('background-color', color.value);
}

function removeHiddenFeedCategory(category) {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveHiddenFeedCategory",
        data: { category : category },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                refreshPreferencesPartialView(false)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitPreferencesForm(e, form, scrollToTop = true, loadNotes = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                refreshPreferencesPartialView(scrollToTop, loadNotes)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage, false);
            }
        }
    });

    e.preventDefault();
};

function refreshPreferencesPartialView(scrollToTop = true, loadNotes = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Preferences/GetPreferencesPartialView",
        data: { loadNotes : loadNotes },
        success: function(data)
        {
            $("#preferencesPartialView").html(data);
            onPageLoad(scrollToTop);
            onPreferencesLoad();
        }
    });
}
$(document).ready(function(){
    onProfileLoad();
});

function onProfileLoad() {
    if ($('#modal-profile-accomplishments-tutorial').length){
        $('#modal-profile-accomplishments-tutorial').modal();
    }
    if ($('#companions-container').length) {
        $("#companions-container").sortable({
            items: ".scrolling-grid-element",
            handle: ".draggable-bar",
            axis: "x",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var companionId = el.item.attr("id").split('companion-')[1];
                var url = "/Profile/UpdateCompanionPosition";
                $('.scrolling-companion-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, companionId: companionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshProfilePartialView("", false, false);
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.scrolling-companion-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    if ($('#showroom-items').length) {
        $("#showroom-items").sortable({
            items: ".grid-element",
            handle: ".draggable-bar",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var selectionId = el.item.attr("id");
                var url = "/Profile/UpdatePosition";
                $('.grid-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, selectionId: selectionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshProfileShowroomPartialView();
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.grid-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $(".draggable-bar").disableSelection();
}

function companionInteract(username, companionId) {
    var url = '/Profile/CompanionInteract';
    $.ajax({
        type: "POST",
        url: url,
        data: { username : username, companionId : companionId },
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                else if (data.infoMessage) {
                    showSnackbarInfo(data.infoMessage, false);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.shouldRefresh) {
                    refreshProfilePartialView(username, false, false);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function showAllBadges() {
    $('#modal-all-badges').modal();
}

function submitRemoveProfileForm(e, form, username) {
    if (confirm("Are you sure you want to send this Companion away to be adopted? This cannot be undone.")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    if (data.successMessage) {
                        showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    }
                    refreshProfilePartialView(username);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function submitEndPartnershipForm(e, form, username) {
    if (confirm("Are you sure you want to end this partnership?")) {
        submitProfileForm(e, form, true, username);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function likeShowroom(showroomUsername){
    $.ajax({
        type: "POST",
        url: "/Profile/LikeShowroom",
        data: { showroomUsername : showroomUsername },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshProfileShowroomPartialView(showroomUsername);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitProfileForm(e, form, isProfile, username = '', shouldRefresh = true, launchPartnershipModal = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, isProfile);
                    if (data.points) {
                        updatePoints(data.points);
                    }
                    if (data.soundUrl) {
                        playSound(data.soundUrl);
                    }
                }
                if (shouldRefresh) {
                    if (isProfile) {
                        refreshProfilePartialView(username, launchPartnershipModal);
                        $('.modal.in').modal('hide');
                        $('.modal-backdrop').remove();
                        $('body').removeClass('modal-open'); 
                    }
                    else {
                        refreshProfileShowroomPartialView(username);
                    }
                }
            }
            else{
                if (data.hideMessage) {
                    stopLoadingButton();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
            if (data.shouldRefresh) {
               if (isProfile) {
                    refreshProfilePartialView(username, launchPartnershipModal);
                    $('.modal.in').modal('hide');
                    $('.modal-backdrop').remove();
                    $('body').removeClass('modal-open'); 
                }
                else {
                    refreshProfileShowroomPartialView(username);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshProfilePartialView(username, launchPartnershipModal = false, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { username : username },
        url: "/Profile/GetProfilePartialView",
        success: function(data)
        {
            $("#profilePartialView").html(data);
            onPageLoad(scrollToTop, false);
            onProfileLoad();
            if (launchPartnershipModal) {
                $('#modal-partnership-requests').modal();
            }
        }
    });
}

function refreshProfileShowroomPartialView(showroomUsername = "") {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isCollapsed : $('#collapseItems').hasClass('collapsed'), showroomUsername : showroomUsername },
        url: "/Profile/GetProfileShowroomPartialView",
        success: function(data)
        {
            $("#profileShowroomPartialView").html(data);
            onPageLoad(false);
            onProfileLoad();
        }
    });
}
$(document).ready(function(){
    onAuctionsLoad();
});

function onAuctionsLoad(){
    var dropdownCategoryValue = $('#itemCategoryDropdownValue').val();
    if (dropdownCategoryValue){
        $('#category').val(dropdownCategoryValue);
        $('#category').selectpicker('val', dropdownCategoryValue);
    }
    var dropdownConditionValue = $('#itemConditionDropdownValue').val();
    if (dropdownConditionValue){
        $('#condition').val(dropdownConditionValue);
        $('#condition').selectpicker('val', dropdownConditionValue);
    }
}

function searchAuction(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/Auction?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};

function deleteAuction(id) {
    if (confirm("Are you sure you want to end this auction? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Auction/EndAuction",
            data: { id: id },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    window.location.href = "/Auction";
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function updateAuctionCreate(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success == false){
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            else{
                $("#auctionCreatePartialView").html(data);
                onPageLoad();
            }
        }
    });

    e.preventDefault();
};

function submitBidAuctionForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshAuctionBidPartialView(id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshAuctionBidPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function submitCollectAuctionForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshAuctionCollectionPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitCreateAuctionForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/Auction?id=" + data.id;
            }
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshAuctionCreatePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Auction/GetAuctionCreatePartialView",
        success: function(data)
        {
            $("#auctionCreatePartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshAuctionBidPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: {id : id},
        url: "/Auction/GetAuctionBidPartialView",
        success: function(data)
        {
            $("#auctionPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshAuctionCollectionPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: {id : id},
        url: "/Auction/GetAuctionCollectionPartialView",
        success: function(data)
        {
            $("#auctionCollectionPartialView").html(data);
            onPageLoad();
        }
    });
}


var craftingItemsSelected = 0;

function craftingCardSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    if ($('#item1').val() == itemId || $('#item2').val() == itemId || $('#item3').val() == itemId) {
        if ($('#item1').val() == itemId) {
            $('#item1').val("");
        }
        else if ($('#item2').val() == itemId) {
            $('#item2').val("");
        }
        else if ($('#item3').val() == itemId) {
            $('#item3').val("");
        }
        item.removeClass('itemSelected');
        craftingItemsSelected--;
    }
    else if ($('#item1').val() == "" || $('#item2').val() == "" || $('#item3').val() == "") {
        if ($('#item1').val() == "") {
            $('#item1').val(itemId);
        }
        else if ($('#item2').val() == "") {
            $('#item2').val(itemId);
        }
        else if ($('#item3').val() == "") {
            $('#item3').val(itemId);
        }
        item.addClass('itemSelected');
        craftingItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only use 3 cards in your Recipe. Remove a card to add this one.");
    }
    if (craftingItemsSelected != 3) {
        $('.faded').removeClass('faded');
        $('#craftButton').addClass('disabled');
    }
    else {
        $('#craftButton').removeClass('disabled');
        $('.crafting-card').each(function () {
            if (!$(this).hasClass('itemSelected')) {
                $(this).addClass('faded');
            }
        });
        $('html, body').animate({
            scrollTop: $('#craftButton').offset().top - 100
        }, 500);
    }
}

function resetSelectedCraftingCards() {
    craftingItemsSelected = 0;
    $('#item1').val("");
    $('#item2').val("");
    $('#item3').val("");
    $('.faded').removeClass('faded');
    $('#craftButton').addClass('disabled');
    $('.itemSelected').removeClass('itemSelected');
}

function updateCrafting(e, form, isCrafting) {
    form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (isCrafting) {
                    refreshingCraftingPartialView();
                }
                else {
                    refreshingFurnacePartialView();
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshingCraftingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Crafting/GetCraftingPartialView",
        success: function(data)
        {
            $("#craftingPartialView").html(data);
            onPageLoad();
            craftingCards = [];
            recipeCards = [];
        }
    });
}

function refreshingFurnacePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Crafting/GetFurnacePartialView",
        success: function(data)
        {
            $("#craftingFurnacePartialView").html(data);
            onPageLoad();
        }
    });
}

function updateSeeker(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshSeekerPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshSeekerPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshSeekerPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Seeker/GetSeekerPartialView",
        success: function(data)
        {
            $("#seekerPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateDonations(e, form, isTaking) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, isTaking, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDonationsPartialView(isTaking);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshDonationsPartialView(isTaking) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Donations/GetDonationsPartialView",
        data: {isTaking : isTaking},
        success: function(data)
        {
            $("#donationsPartialView").html(data);
            onPageLoad(isTaking);
        }
    });
}
function submitPasswordResetForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                window.location.href = "/";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
function submitShowcaseForm(e, form, isAger = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshShowcasePartialView(isAger);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshShowcasePartialView(isAger = false) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isAger : isAger },
        url: "/Showcase/GetShowcasePartialView",
        success: function(data)
        {
            $("#showcasePartialView").html(data);
            onPageLoad();
        }
    });
}
$(document).ready(function () {
    onStorageLoad();
});


function onStorageLoad() {
    if ($('#storage-items').length){
        $("#storage-items").sortable({
            items: ".grid-element",
            handle: ".draggable-bar",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var selectionId = el.item.attr("id");
                var url = "Storage/UpdatePosition";
                $('.grid-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition : newPosition, selectionId : selectionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshStoragePartialView(true);
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.grid-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $("draggable-bar").disableSelection();
}

function updateStorage(e, form, isWithdraw) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshStoragePartialView(isWithdraw);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function disposeStorageItemConfirmation(e, form, showConfirmation) {
    if (!showConfirmation) {
        disposeStorageItem(e, form);
        e.preventDefault();
        return;
    }

    if (confirm("Are you sure you want to permanently dispose of your item? (You can disable this confirmation in your Preferences)")) {
        disposeStorageItem(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function disposeStorageItem(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                refreshStoragePartialView(true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshStoragePartialView(isWithdraw) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Storage/GetStoragePartialView",
        data: {isWithdraw : isWithdraw},
        success: function(data)
        {
            $("#storagePartialView").html(data);
            onPageLoad(false);
            onStorageLoad();
        }
    });
}
var messagesTimer = null;

$(document).ready(function () {
    onMessagesLoad();
    pollForNewMessages(false);
});

function onMessagesLoad(shouldScroll = true) {
    if ($('#scrollContainer').length) {
        $('#scrollContainer').scrollTop($('#conversationContainer').height());
        if (shouldScroll) {
            var messageContainerOffset = $('#message-container').offset();
            if (messageContainerOffset) {
                $(window).scrollTop(messageContainerOffset.top - window.innerHeight + $('#message-container').height() * 2);
            }
        }
    }
}

function removeAllMessages(username){
    if (confirm("Are you sure you want to remove your messages with " + username + "? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Messages/Remove",
            data: { username : username },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Messages";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function delayedKeyUpUpdateMessages(e, form, scrollToTop = true) {
    clearTimeout(messagesTimer);
    messagesTimer = setTimeout(function () {
        updateMessages(e, form, scrollToTop);
    }, 250);
};

function submitMessagesForm(e, form, username, isMessageBox) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#message').val("");
                refreshMessagesPartialView(username, isMessageBox, !data.successMessage);
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, false);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                }
                if (data.points){
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, false, true, data.buttonText, data.buttonUrl);
                $('html, body').animate({
                    scrollTop: 0
                }, 0);
            }
        }
    });

    e.preventDefault();
};

function submitNewMessagesForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
            }
            else{
                $('#username').val('');
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    $('.modal.in').modal('hide');
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open'); 

    e.preventDefault();
};

function pollForNewMessages(isMessageBox){
    var correspondent = $('#messageCorrespondent').val();
    if (!correspondent){
        return;
    }
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetUnreadMessages",
        data: { correspondent : correspondent },
        success: function(data)
        {
            if (data.success && data.amount > 0) {
                correspondent = $('#messageCorrespondent').val();
                if (!correspondent) {
                    return;
                }
                refreshMessagesPartialView(correspondent, isMessageBox);
            }

            setTimeout(function(){
                pollForNewMessages();
            }, 1500);
        }
    });
}

function updateMessages(e, form, scrollToTop = true) {
    form = $(form);
    var url = "/Messages/GetMessagesPartialView";
    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        data: form.serialize(),
        success: function (data) {
            $("#messagesPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });

    e.preventDefault();
};

function refreshMessagesPartialView(username, isMessageBox, scrollToMessage) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetMessagesPartialView",
        cache: false,
        data: { username: username, isMessageBox: isMessageBox },
        success: function(data)
        {
            $("#scrollContainer").html(data);
            onPageLoad(false);
            onMessagesLoad(scrollToMessage);
        }
    });
}
$(document).ready(function(){
    onWishingStoneLoad();
});

function onWishingStoneLoad(){
    $('#wishingScrollContainer').scrollTop($('#wishingContainer').height());
    var makeWishContainerOffset = $('#makeWish-container').offset();
    if (makeWishContainerOffset){
        $(window).scrollTop(makeWishContainerOffset.top - window.innerHeight + $('#makeWish-container').height() * 2);
    }
}

function mineQuarryStone(position) {
    $.ajax({
        type: "POST",
        url: "/WishingStone/MineQuarryStone",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshWishingStoneQuarryPartialView(position, data.scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitWishingStoneForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshWishingStonePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWishingStonePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WishingStone/GetWishingStonePartialView",
        cache: false,
        success: function(data)
        {
            $("#wishingStonePartialView").html(data);
            $('#wishingScrollContainer').scrollTop($('#wishingContainer').height());
            onPageLoad();
        }
    });
}

function refreshWishingStoneQuarryPartialView(position, scrollToTop = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WishingStone/GetWishingStoneQuarryPartialView",
        cache: false,
        success: function(data)
        {
            $("#wishingStoneQuarryPartialView").html(data);
            deactivateAllQuarryStones();
            var quarryStoneId = '#quarry-stone-' + position;
            var countTo = $(quarryStoneId).attr('data-count');
            var currency = $(quarryStoneId).text().split(' ')[1];
            countToNumber($(quarryStoneId), countTo, 0, " " + currency, activateAllQuarryStones);
            onPageLoad(scrollToTop);
        }
    });
}

function activateAllQuarryStones() {
    $('.active-quarry-stone').each(function() {
        $(this).removeClass('faded');
    });
    $('.quarry-stone').each(function() {
        $(this).css('pointer-events', '');
    });
}

function deactivateAllQuarryStones() {
    $('.active-quarry-stone').each(function() {
        $(this).addClass('faded');
    });
    $('.quarry-stone').each(function() {
        $(this).css('pointer-events', 'none');
    });
}
function submitReportForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location.href = "/";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
function updateDescriptionCountMessage(){
    var showroomDescriptionLength = $('#description').val().length;
    var remainingLength = 350 - showroomDescriptionLength;
    $('#descriptionCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function submitBuyersClubForm(e, form, isBuyersClub, showDiscount) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (isBuyersClub) {
                    refreshBuyersClubPartialView(showDiscount);
                }
                else {
                    refreshBuyersClubShowroomPartialView(data.shouldRefresh);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshBuyersClubPartialView(showDiscount);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshBuyersClubPartialView(showDiscount) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { showDiscount : showDiscount },
        url: "/BuyersClub/GetBuyersClubPartialView",
        success: function(data)
        {
            $("#buyersClubPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshBuyersClubShowroomPartialView(showTitleChange) {
    $.ajax({
        type: "GET",
        cache: false,
        data : { showTitleChange : showTitleChange },
        url: "/BuyersClub/GetBuyersClubShowroomPartialView",
        success: function(data)
        {
            $("#buyersClubShowroomPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateSharePrice(cost, points){
    var shares = $('#shares').val();
    if (shares < 0){
        return;
    }
    var calculatedCost = Math.round(shares) * cost;
    var formattedCost = formatNumber(calculatedCost);
    if (calculatedCost <= points){
        $('#calculatedCost').html('<span class="win-green">' + formattedCost + '</span>');
        $('#currency').addClass('win-green');
        $('#currency').removeClass('lose-red');
    }
    else{
        $('#calculatedCost').html('<span class="lose-red">' + formattedCost + '</span>');
        $('#currency').addClass('lose-red');
        $('#currency').removeClass('win-green');
    }
}

function submitStockMarketForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                refreshStockMarketPartialView(id);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitPortfolioForm(e, form, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, scrollToTop);
                refreshPortfolioPartialView(scrollToTop);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshStockMarketPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StockMarket/GetStockMarketPartialView",
        data: { id : id },
        success: function(data)
        {
            $("#stockMarketPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshPortfolioPartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StockMarket/GetPortfolioPartialView",
        success: function(data)
        {
            $("#portfolioPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}
var pastWandSelections = [];

function wizardWandSelected(wand) {
    var wand = $(wand);
    var wandId = wand.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedWandColor').val() == wandId){
        $('#selectedWandColor').val("");
    }
    else{
        $('#selectedWandColor').val(wandId);
        wand.addClass('itemSelected');
        if ($('#secret-cavern').val() == "true") {
            pastWandSelections.push(wandId);
            if (pastWandSelections[0] != "Orange" || (pastWandSelections.length > 1 && pastWandSelections[1] != "Red") || (pastWandSelections.length > 2 && pastWandSelections[2] != "Blue")) {
                pastWandSelections = [];
            }
            else if (pastWandSelections[0] == "Orange" && pastWandSelections[1] == "Red" && pastWandSelections[2] == "Blue") {
                window.location.href = '/Wizard/SecretCavern';
            }
        }
    }
}

function updateWizard(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                var selectedWandColor = $('#selectedWandColor').val();
                refreshWizardPartialView(selectedWandColor);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWizardPartialView(selectedWandColor) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Wizard/GetWizardPartialView",
        success: function(data)
        {
            $("#wizardPartialView").html(data);
            onPageLoad();
            $('#' + selectedWandColor).addClass('animated bounceIn');
        }
    });
}
function updateEffects(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshEffectsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshEffectsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Effects/GetEffectsPartialView",
        success: function(data)
        {
            $("#effectsPartialView").html(data);
            onPageLoad();
        }
    });
}
function checkIfPromoBoxIsEnabled(val){
    if (val === "") {
        $('#redeemPromoCodeButton').addClass('disabled');
    }
    else {
        $('#redeemPromoCodeButton').removeClass('disabled');
    }
}

function updatePromoCodes(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            $('#promoCode').val('');
            $('#redeemPromoCodeButton').addClass('disabled');
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshPromoCodesPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshPromoCodesPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PromoCodes/GetPromoCodesPartialView",
        success: function(data)
        {
            $("#promoCodesPartialView").html(data);
            onPageLoad();
        }
    });
}
function submitReferralsForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    refreshReferralsPartialView();
                }
                else {
                    $('#emailAddress').val('');
                    stopLoadingButton();
                    showSnackbarSuccess("Your email was successfully sent!");
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshReferralsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Referrals/GetReferralsPartialView",
        success: function (data) {
            $("#referralsPartialView").html(data);
            onPageLoad(false);
        }
    });
}
window.Clipboard = (function(window, document, navigator) {
    var textArea,
        copy;

    function isOS() {
        return navigator.userAgent.match(/ipad|iphone/i);
    }

    function createTextArea(text) {
        textArea = document.createElement('textArea');
        textArea.value = text;
        document.body.appendChild(textArea);
    }

    function selectText() {
        var range,
            selection;

        if (isOS()) {
            range = document.createRange();
            range.selectNodeContents(textArea);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
            textArea.setSelectionRange(0, 999999);
        } else {
            textArea.select();
        }
    }

    function copyToClipboard() {        
        document.execCommand('copy');
        document.body.removeChild(textArea);
    }

    copy = function(text) {
        createTextArea(text);
        selectText();
        copyToClipboard();
    };

    return {
        copy: copy
    };
})(window, document, navigator);
function removeArticle(id){
    if (confirm("Are you sure you want to remove this article? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/News/RemoveArticle",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/News";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function removeNewsComment(newsId, commentId){
    if (confirm("Are you sure you want to remove your comment? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/News/RemoveComment",
            data: { 
                newsId : newsId, 
                commentId : commentId,  
            },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    refreshNewsArticlePartialView(newsId, false, false, "", false);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function likeNewsComment(newsId, commentId){
    $.ajax({
        type: "POST",
        url: "/News/LikeComment",
        data: { 
            newsId : newsId, 
            commentId : commentId,  
        },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshNewsArticlePartialView(newsId, false, true, commentId);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitNewsCommentForm(e, form, newsId, isReply) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshNewsArticlePartialView(newsId, false, true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshNewsArticlePartialView(id, scrollToTop = true, showScroll = false, commentId = "", movePage = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/News/GetNewsArticlePartialView",
        cache: false,
        data: {id : id},
        success: function(data)
        {
            $("#newsPartialView").html(data);
            onPageLoad(scrollToTop);
            if (movePage) {
                $('html, body').animate({
                    scrollTop: $(commentId != "" ? "#" + commentId : ".last-comment").offset().top - (showScroll ? 50 : 100)
                }, showScroll ? 500 : 0);
            }
        }
    });
}
function searchUserAdmin() {
    var username = $('#username').val();
    window.location = '/Admin?lookup=' + username;
};

function searchAdmin(curQuery) {
    var searchQuery = $('#searchQuery').val();
    if (window.location.search.includes('query')) {
        window.location.search = window.location.search.replace('query=' + encodeURIComponent(curQuery), 'query=' + encodeURIComponent(searchQuery));
    }
    else {
        window.location.search += '&query=' + encodeURIComponent(searchQuery);
    }
}

function deleteGroupAdmin(id){
    if (confirm("Are you sure you want to remove this group? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveGroup",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function deleteMarketStallAdmin(id){
    if (confirm("Are you sure you want to remove this Market Stall? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/MarketStalls/RemoveMarketStall",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function adminItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#itemId').val() == itemId){
        $('#itemId').val("");
    }
    else{
        $('#itemId').val(itemId);
        item.addClass('itemSelected');
    }
}

function submitAdminForm(e, form, refresh = false, reset = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (refresh){
                    location.reload();
                }
                if (reset){
                    form[0].reset();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
$(document).ready(function(){
    onJobsLoad();
});

function onJobsLoad(){
    var dropdownValue = $('#jobsDropdownValue').val();
    if (dropdownValue){
        $('#position').val(dropdownValue);
        $('#position').selectpicker('val', dropdownValue);
    }
}

function updateJobsCountMessage(){
    var jobsMessageLength = $('#content').val().length;
    var remainingLength = 2500 - jobsMessageLength;
    $('#jobsCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function updateHelpersCountMessage(){
    var helpersMessageLength = $('#reason').val().length;
    var remainingLength = 2500 - helpersMessageLength;
    $('#helpersCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function updateJobsDropdown(val){
    $('#position').val(val);
}

function submitJobsForm(e, form, isIndex) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                isIndex ? refreshJobsPartialView() : refreshHelpersPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshJobsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Jobs/GetJobsPartialView",
        success: function(data)
        {
            $("#jobsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshHelpersPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Jobs/GetHelpersPartialView",
        success: function(data)
        {
            $("#helpersPartialView").html(data);
            onPageLoad();
        }
    });
}
var tradingItemsSelected = 0;

function searchTrades(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/Trading?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition
};

function updateTradingDescriptionCountMessage(){
    var tradingDescriptionLength = $('#request').val().length;
    var remainingLength = 500 - tradingDescriptionLength;
    $('#tradingDescriptionCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function editTradeDescription() {
    stopLoadingButton();
    if ($('#trade-request-edit').is(":visible")) {
        $('#trade-request-edit').hide();
        $('#trade-request-content').show();
    }
    else {
        $('#trade-request-edit').show();
        $('#trade-request-content').hide();
    }
}

function deleteTrade(id){
    if (confirm("Are you sure you want to remove your posting? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Trading/RemoveTrade",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Trading";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshTradingOfferPartialView(id);
                    }
                }
            }
        });
    }
}

function tradingItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");
    
    if ($('#item1').val() == itemId || $('#item2').val() == itemId || $('#item3').val() == itemId) {
        if ($('#item1').val() == itemId) {
            $('#item1').val("");
        }
        else if ($('#item2').val() == itemId) {
            $('#item2').val("");
        }
        else if ($('#item3').val() == itemId) {
            $('#item3').val("");
        }
        item.removeClass('itemSelected');
        tradingItemsSelected--;
    }
    else if ($('#item1').val() == "" || $('#item2').val() == "" || $('#item3').val() == "") {
        if ($('#item1').val() == "") {
            $('#item1').val(itemId);
        }
        else if ($('#item2').val() == "") {
            $('#item2').val(itemId);
        }
        else if ($('#item3').val() == "") {
            $('#item3').val(itemId);
        }
        item.addClass('itemSelected');
        tradingItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only select up to 3 items. Remove an item from your trade posting to add this one.");
    }
    $('#tradingItemsSelected').text(tradingItemsSelected);
    if (tradingItemsSelected == 0) {
        $('#tradeButton').addClass('disabled');
    }
    else {
        $('#tradeButton').removeClass('disabled');
    }
}

function submitTradingCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/Trading?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTradingCreatePartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function submitTradingOfferForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshTradingOfferPartialView(id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTradingOfferPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshTradingCreatePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Trading/GetTradingCreatePartialView",
        success: function(data)
        {
            $("#tradingCreatePartialView").html(data);
            onPageLoad();
            tradingItemsSelected = 0;
        }
    });
}

function refreshTradingOfferPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Trading/GetTradingOfferPartialView",
        data: { id : id },
        success: function(data)
        {
            $("#tradingPartialView").html(data);
            onPageLoad();
            tradingItemsSelected = 0;
        }
    });
}


function updateContactCountMessage(){
    var contactMessageLength = $('#message').val().length;
    var remainingLength = 2500 - contactMessageLength;
    $('#contactCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function showSendMessageContactOption() {
    if ($('#contact-message').length) {
        $('#contact-message').hide();
        var selectedReason = $('#category').val();
        if (selectedReason == "GeneralHelp" || selectedReason == "GeneralQuestion") {
            $('#contact-message').show();
        }
    }
}

function submitContactForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
$(document).ready(function(){
    onGroupsLoad();
    pollForNewGroupMessages();
});

function onGroupsLoad() {
    var dropdownValue = $('#groupPowerDropdownValue').val();
    if (dropdownValue) {
        $('#power').val(dropdownValue);
        $('#power').selectpicker('val', dropdownValue);
    }
    $('#groupsMessagesScrollContainer').scrollTop($('#groupMessagesContainer').height());
    var messageContainerOffset = $('#groups-messages-container').offset();
    if (messageContainerOffset){
        $(window).scrollTop(messageContainerOffset.top - window.innerHeight + $('#groups-messages-container').height() * 2);
    }
}

function updateGroupsDescriptionCountMessage(){
    var groupsDescriptionLength = $('#description').val().length;
    var remainingLength = 500 - groupsDescriptionLength;
    $('#groupsCreateCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function updateWhiteboardCountMessage(){
    var whiteboardMessageLength = $('#whiteboard').val().length;
    var remainingLength = 500 - whiteboardMessageLength;
    $('#whiteboardCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function searchGroups(page) {
    var searchQuery = $('#searchQuery').val();
    window.location = '/Groups/All?page=' + page + '&query=' + encodeURIComponent(searchQuery);
};

function editWhiteboard() {
    stopLoadingButton();
    if ($('#whiteboardEdit').is(":visible")) {
        $('#whiteboardEdit').hide();
        $('#whiteboardContent').show();
    }
    else {
        $('#whiteboardEdit').show();
        $('#whiteboardContent').hide();
    }
}

function removeGroupDisplayPic(groupId) {
    $.ajax({
        type: "POST",
        url: "/Groups/RemoveDisplayPic",
        data: { groupId: groupId },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                $('#upload-pic').show();
                $('#existing-pic').hide();
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function setAsPrimaryGroup(groupId) {
    $.ajax({
        type: "POST",
        url: "/Groups/SetPrimaryGroup",
        data: { groupId: groupId },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                    refreshUserGroupsPartialView();
                }
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function deleteGroup(id){
    if (confirm("Are you sure you want to remove your group? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveGroup",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Groups";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshGroupsPartialView(id);
                    }
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }
}

function deleteGroupMessage(id, groupId) {
    if (confirm("Are you sure you want to delete this message? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveMessage",
            data: { id : id, groupId : groupId },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshGroupMessagesPartialView(groupId);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function submitGroupsIdForm(e, form, id, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                showSuccessMessage(data.successMessage, scrollToTop);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshGroupsPartialView(id, scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function submitGroupCreateForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                window.location.href = "/Groups?id=" + data.id;
            }
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function submitGroupsMessageForm(e, form, groupId) {
    form = $(form);
    var url = form.attr('action');
    if ($('#message').val().includes("@here")) {
        if (confirm("Are you sure you want to send this message and notify all members of this group?")) {
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function (data) {
                    hideAllMessages();
                    if (data.success) {
                        $('#message').val("");
                        refreshGroupMessagesPartialView(groupId);
                    }
                    else {
                        showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    }
                }
            });
        }
        else {
            stopLoadingButton();
        }
    }
    else {
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function (data) {
                hideAllMessages();
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.success) {
                    $('#message').val("");
                    refreshGroupMessagesPartialView(groupId);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }

    e.preventDefault();
};

function refreshGroupsPartialView(id, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetGroupsPartialView",
        data: {id : id },
        success: function(data)
        {
            $("#groupsPartialView").html(data);
            onPageLoad(scrollToTop);
            onGroupsLoad();
        }
    });
}

function refreshUserGroupsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetUserGroupsPartialView",
        success: function (data) {
            $("#groupsUserPartialView").html(data);
            onPageLoad();
            onGroupsLoad();
        }
    });
}

function refreshGroupMessagesPartialView(groupId) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetGroupMessagesPartialView",
        data: { groupId: groupId },
        success: function(data)
        {
            $("#groupsMessagesScrollContainer").html(data);
            onPageLoad();
            onGroupsLoad();
        }
    });
}

function pollForNewGroupMessages(){
    var shouldPollForGroupMessages = $('#groupsMessagesScrollContainer');
    if (!shouldPollForGroupMessages.length){
        return;
    }

    var groupId = $('#group-id').val();

    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetUnreadGroupMessages",
        data: { groupId : groupId },
        success: function(data)
        {
            if (data.success && data.amount > 0){
                refreshGroupMessagesPartialView(groupId);
            }

            setTimeout(function(){
                pollForNewGroupMessages();
            }, 1500);
        }
    });
}
function updateLotto(e, form, isLotto) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                isLotto ? refreshLottoPartialView() : refreshLottoEggPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    isLotto ? refreshLottoPartialView() : refreshLottoEggPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshLottoPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Lotto/GetLottoPartialView",
        success: function(data)
        {
            $("#lottoPartialView").html(data);
            var countTo = $('#currentPot').attr('data-count');
            if (countTo > 50000) {
                var currency = $('#currentPot').text().split(' ')[1];
                countToNumber($('#currentPot'), countTo, countTo - 500, " " + currency);
            }
            else {
                $('#pyro-container').show();
            }
            $('#pot').addClass('animated tada');
            onPageLoad();
        }
    });
}

function refreshLottoEggPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Lotto/GetEggPartialView",
        success: function(data)
        {
            $("#lottoEggPartialView").html(data);
            onPageLoad();
        }
    });
}
var streakCardId = "";

function revealStreakCard(position) {
    $.ajax({
        type: "POST",
        url: "/StreakCards/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                var showMessage = false;
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                    showMessage = true;
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                    showMessage = true;
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshStreakCardsPartialView(showMessage, position);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshStreakCardsPartialView(scrollToTop, position){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StreakCards/GetStreakCardsPartialView",
        success: function(data)
        {
            $("#streakCardsPartialView").html(data);
            deactivateAllStreakCards();
            streakCardId = '#streak-card-' + position;
            var countTo = $(streakCardId).attr('data-count');
            var currency = $(streakCardId).text().split(' ')[1];
            countToNumber($(streakCardId), countTo, 0, " " + currency, activateAllStreakCards);
            onPageLoad(scrollToTop);
        }
    });
}

function activateAllStreakCards() {
    $('.active-streak-card').each(function() {
        $(this).removeClass('faded');
    });
    $('.streak-card').each(function() {
        $(this).css('pointer-events', '');
    });

    $(streakCardId).addClass('animated tada');
}
function deactivateAllStreakCards() {
    $('.active-streak-card').each(function() {
        $(this).addClass('faded');
    });
    $('.streak-card').each(function() {
        $(this).css('pointer-events', 'none');
    });
}
$(document).ready(function() {
    var sideHelpNav = $('#sideHelpNav');
    if (sideHelpNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sideHelpNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sideHelpNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#helpScrollSpy',
            offset: 40
        });
    }
});
function updateDeleteInfoCountMessage() {
    var infoLength = $('#info').val().length;
    var remainingLength = 500 - infoLength;
    $('#deleteInfoCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function submitDeleteAccountForm(e, form) {
    if (confirm("Are you sure you want to permanently delete your account?")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    window.location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function showDeleteAccountInfo() {
    $('#privacy').hide();
    $('#boring').hide();
    $('#confusing').hide();
    $('#distracting').hide();
    $('#other').hide();
    var selectedReason = $('#reason').val();
    if (selectedReason == "1") {
        $('#privacy').show();
    }
    else if (selectedReason == "2" || selectedReason == "4") {
        $('#boring').show();
    }
    else if (selectedReason == "3") {
        $('#confusing').show();
    }
    else if (selectedReason == "5") {
        $('#distracting').show();
    }
    else if (selectedReason == "0") {
        $('#other').show();
    }
}
function submitDisableAccountForm(e, form) {
    if (confirm("Are you sure you want to temporarily disable your account?")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    window.location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};
function swapDeckCardSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedDeckCard').val() == itemId){
        $('#selectedDeckCard').val("");
        $('#swapDeckButton').addClass('disabled');
    }
    else{
        $('#selectedDeckCard').val(itemId);
        item.addClass('itemSelected');
        $('#swapDeckButton').removeClass('disabled');
    }
}

function updateSwapDeck(e, form, isSwapDeck) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (isSwapDeck) {
                    refreshSwapDeckPartialView(data.id);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            if (!isSwapDeck) {
                refreshSwapDeckMazePartialView();
            }
        }
    });

    e.preventDefault();
};

function refreshSwapDeckPartialView(selectionId){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SwapDeck/GetSwapDeckPartialView",
        success: function(data)
        {
            $("#swapDeckPartialView").html(data);
            if (selectionId) {
                var img = $('#' + selectionId).find('img')
                $('#' + selectionId).removeClass('fade-out-border');
                $(img).removeClass('animated bounceIn');
                $('#' + selectionId).addClass('fade-out-border');
                $(img).addClass('animated bounceIn');
            }
        }
    });
}

function refreshSwapDeckMazePartialView(){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SwapDeck/GetSwapDeckMazePartialView",
        success: function(data)
        {
            $("#swapDeckMazePartialView").html(data);
            onPageLoad(false);
        }
    });
}
function searchMarketStalls(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/MarketStalls?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};

function deleteMarketStall(id){
    if (confirm("Are you sure you want to remove your Market Stall? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/MarketStalls/RemoveMarketStall",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/MarketStalls";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshMarketStallsIdPartialView(id);
                    }
                }
            }
        });
    }
}

function updateMarketStallDescriptionCountMessage() {
    var marketStallDescriptionLength = $('#description').val().length;
    var remainingLength = 500 - marketStallDescriptionLength;
    $('#marketStallDescriptionCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function submitMarketStallCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    var arr = addMarketStallItemsToForm(form);

    $.ajax({
        type: "POST",
        url: url,
        data: arr,
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                window.location = "/MarketStalls?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitMarketStallsMainForm(e, form, query, page) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMarketStallsMainPartialView(query, page);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshMarketStallMainPartialView(query, page);
                }
            }
        }
    });

    e.preventDefault();
}

function submitMarketStallsIdForm(e, form, id, isUpdate, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: isUpdate ? addMarketStallItemsToForm(form) : form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, scrollToTop);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMarketStallsIdPartialView(id, scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshMarketStallsIdPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshMarketStallsMainPartialView(query, page) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { query : query, page : page },
        url: "/MarketStalls/GetMarketStallsMainPartialView",
        success: function(data)
        {
            $("#marketStallsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMarketStallsIdPartialView(id, scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/MarketStalls/GetMarketStallsIdPartialView",
        success: function(data)
        {
            $("#marketStallsPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}

function addMarketStallItemsToForm(form) {
    var marketStallItems = [];

    var arr = form.serializeArray();
    for(var i = 0; i < arr.length; i++){ 
        if (arr[i].value == '' || arr[i].value == '0') {
            arr.splice(i, 1); 
            i--;
        }
        else if (arr[i].name.includes('cost')) {
            var itemId = arr[i].name.split('cost-')[1];
            var item = { "SelectionId" : itemId, "Cost" : arr[i].value };
            marketStallItems.push(item);
            arr.splice(i, 1); 
            i--;
        }
    }

    arr.push({ "name" : "marketStallItems", "value": JSON.stringify(marketStallItems) });

    return arr;
}
function submitDisposerFormConfirmation(e, form, showConfirmation) {
    if (!showConfirmation) {
        submitDisposerForm(e, form);
        e.preventDefault();
        return;
    }

    if (confirm("Are you sure you want to permanently dispose of your item? (You can disable this confirmation in your Preferences)")) {
        submitDisposerForm(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitDisposerForm(e, form, scrollToTop = false) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDisposerPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshDisposerPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshDisposerPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Disposer/GetDisposerPartialView",
        success: function(data)
        {
            $("#disposerPartialView").html(data);
            onPageLoad(false);
        }
    });
}


var secretPassageTimer = 0;
var libraryKnowledgeType = "";

$(document).ready(function(){
    onSecretPassageLoad();
});

function onSecretPassageLoad() {
    loadVoidMessage();
}

function loadVoidMessage(prevIndex = -1) {
    var voidMessages = $('#voidMessages');
    if (voidMessages.length) {
        var voidMessagesList = JSON.parse(voidMessages.val());
        if (voidMessagesList.length > 0) {
            clearTimeout(secretPassageTimer);
            var index = prevIndex;
            while (prevIndex == index) {
                index = Math.floor(Math.random() * voidMessagesList.length);
            }
            var voidMessage = voidMessagesList[index];
            $('#voidMessage').fadeOut(prevIndex == -1 ? 0 : 500, function () {
                $('#voidMessage')
                    .html("<h5>" + voidMessage.Message + "</h5> <small><a href='/User/" + voidMessage.Username + "'>" + voidMessage.Username + "</a></small>")
                    .fadeIn(500);
            });

            secretPassageTimer = setTimeout(function(){
                loadVoidMessage(index);
            }, 5000);
        }
    }
}

function knowledgeItemSelected(knowledgeItem) {
    var knowledgeItem = $(knowledgeItem);
    libraryKnowledgeType = knowledgeItem.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#libraryKnowledgeType').val() == libraryKnowledgeType){
        $('#libraryKnowledgeType').val("");
        $('#knowledgeButton').addClass('disabled');
    }
    else{
        $('#libraryKnowledgeType').val(libraryKnowledgeType);
        knowledgeItem.addClass('itemSelected');
        $('#knowledgeButton').removeClass('disabled');
    }
}

function updateLibrary(e, form, isLibrary, isSecretPassage = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (isSecretPassage) {
                    refreshLibrarySecretPassagePartialView(data.successMessage);
                }
                else {
                    refreshLibraryPartialView(isLibrary);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshLibraryPartialView(isLibrary); 
                }
            }
        }
    });

    e.preventDefault();
};

function refreshLibraryPartialView(isLibrary) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isLibrary : isLibrary },
        url: "/Library/GetLibraryPartialView",
        success: function(data)
        {
            $("#libraryPartialView").html(data);
            onPageLoad();
            if (isLibrary) {
                $('#' + libraryKnowledgeType.toLowerCase() + '-level').addClass('animated tada');
            }
        }
    });
}

function refreshLibrarySecretPassagePartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Library/GetLibrarySecretPassagePartialView",
        success: function(data)
        {
            $("#librarySecretPassagePartialView").html(data);
            onPageLoad(scrollToTop);
            onSecretPassageLoad();
        }
    });
}
$(document).ready(function() {
    onTermsLoad();
});

function onTermsLoad() {
    var sideTermsNav = $('#sideTermsNav');
    if (sideTermsNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sideTermsNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sideTermsNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#termsScrollSpy',
            offset: 40
        });
    }
}

function updateTerms(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshTermsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTermsPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshTermsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Terms/GetTermsPartialView",
        success: function(data)
        {
            $("#termsPartialView").html(data);
            onPageLoad();
            onTermsLoad();
        }
    });
}
function updateAttentionHallPostCountMessage(){
    var attentionHallPostLength = $('#post').val().length;
    var remainingLength = 2500 - attentionHallPostLength;
    $('#attentionHallPostCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function showAttentionHallPostContainer(type) {
    $('#accomplishmentContainer').hide();
    $('#itemContainer').hide();
    $('#matchTagsContainer').hide();
    $('#generalContainer').hide();

    $('#selectedAccomplishment').val('');
    $('#selectedItem').val('');
    $('#selectedMatchTag').val('');
    $('#flair').val('');

    $('.accomplishmentSelected').removeClass('accomplishmentSelected');
    $('.itemSelected').removeClass('itemSelected');
    $('.cardSelected').removeClass('cardSelected');

    if (type == 'General') {
        $('#generalContainer').fadeIn(250);
    }
    else if (type == 'Accomplishment') {
        $('#accomplishmentContainer').fadeIn(250);
    }
    else if (type == 'Item') {
        $('#itemContainer').fadeIn(250);
    }
    else if (type == 'MatchTag') {
        $('#matchTagsContainer').fadeIn(250);
    }
}

function attentionHallAccomplishmentSelected(accomplishment) {
    var accomplishment = $(accomplishment);
    var accomplishmentId = accomplishment.attr("id");

    $('.accomplishmentSelected').removeClass('accomplishmentSelected');

    if ($('#selectedAccomplishment').val() == accomplishmentId){
        $('#selectedAccomplishment').val("");
    }
    else{
        $('#selectedAccomplishment').val(accomplishmentId);
        accomplishment.addClass('accomplishmentSelected');
    }
}

function attentionHallItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedItem').val() == itemId){
        $('#selectedItem').val("");
    }
    else{
        $('#selectedItem').val(itemId);
        item.addClass('itemSelected');
    }
}

function attentionHallMatchTagSelected(matchTag) {
    var matchTag = $(matchTag);
    var matchTagId = matchTag.attr("id");

    $('.cardSelected').removeClass('cardSelected');

    if ($('#selectedMatchTag').val() == matchTagId){
        $('#selectedMatchTag').val("");
    }
    else{
        $('#selectedMatchTag').val(matchTagId);
        matchTag.addClass('cardSelected');
    }
}

function attentionHallVote(id, vote, page, isPost){
    $.ajax({
        type: "POST",
        url: "/AttentionHall/Vote",
        data: { id : id, vote: vote },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (isPost) {
                    refreshAttentionHallPostPartialView(id, false, false, "", false);
                }
                else {
                    refreshAttentionHallMainPartialView(page, id, false);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function removeAttentionHallPost(id){
    if (confirm("Are you sure you want to remove this post? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/AttentionHall/RemovePost",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/AttentionHall";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function removeAttentionHallComment(id, commentId){
    if (confirm("Are you sure you want to remove this comment? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/AttentionHall/RemoveComment",
            data: { 
                id : id,
                commentId : commentId,  
            },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    refreshAttentionHallPostPartialView(id, false, false, "", false);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function likeAttentionHallComment(id, commentId){
    $.ajax({
        type: "POST",
        url: "/AttentionHall/LikeComment",
        data: { 
            id : id,
            commentId : commentId,  
        },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshAttentionHallPostPartialView(id, false, true, commentId);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitAttentionHallCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/AttentionHall?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitAttentionHallCommentForm(e, form, id, isReply) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshAttentionHallPostPartialView(id, false, true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshAttentionHallMainPartialView(page, id, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { page : page },
        url: "/AttentionHall/GetAttentionHallMainPartialView",
        success: function(data)
        {
            $("#attentionHallPartialView").html(data);
            onPageLoad(scrollToTop);
            $('html, body').animate({
                scrollTop: $("#" + id).offset().top - 50
            }, 500);
        }
    });
}

function refreshAttentionHallPostPartialView(id, scrollToTop = true, showScroll = false, commentId = "", movePage = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/AttentionHall/GetAttentionHallPostPartialView",
        success: function(data)
        {
            $("#attentionHallPartialView").html(data);
            onPageLoad(scrollToTop);
            if (movePage) {
                $('html, body').animate({
                    scrollTop: $(commentId != "" ? "#" + commentId : ".last-comment").offset().top - (showScroll ? 50 : 100)
                }, showScroll ? 500 : 0);
            }
        }
    });
}

function submitClubForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshClubPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function submitClubIncubatorForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshClubIncubatorPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubIncubatorPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshClubPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Club/GetClubPartialView",
        success: function(data)
        {
            $("#clubPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshClubIncubatorPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Club/GetClubIncubatorPartialView",
        success: function(data)
        {
            $("#clubIncubatorPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateMerchantsForm(e, form, isContest) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (isContest) {
                    refreshMerchantsPartialView();
                }
                else {
                    refreshMerchantsJewelPartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshMerchantsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Merchants/GetMerchantsPartialView",
        success: function(data)
        {
            $("#merchantsPartialView").html(data);
            onPageLoad(false);
        }
    });
}

function refreshMerchantsJewelPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Merchants/GetMerchantsJewelPartialView",
        success: function(data)
        {
            $("#merchantsJewelPartialView").html(data);
            onPageLoad(false);
        }
    });
}
$(document).ready(function() {
    onPrivacyLoad();
});

function onPrivacyLoad() {
    var sidePolicyNav = $('#sidePolicyNav');
    if (sidePolicyNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sidePolicyNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sidePolicyNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#policyScrollSpy',
            offset: 40
        });
    }
}
function updateRewards(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshRewardsStorePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshRewardsStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/RewardsStore/GetRewardsStorePartialView",
        cache: false,
        success: function(data)
        {
            $("#rewardsStorePartialView").html(data);
            onPageLoad();
        }
    });
}
function updateNoticeBoard(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/Plaza";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};
function updateWarehouse(e, form, isBulkItems) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshWarehousePartialView(isBulkItems);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshWarehousePartialView(isBulkItems) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Warehouse/GetWarehousePartialView",
        data: { isBulk : isBulkItems },
        cache: false,
        success: function(data)
        {
            $("#warehousePartialView").html(data);
            onPageLoad();
        }
    });
}
var homeFeedContainers = [];
var usernameCheckTimer = null;
var feedBarScroll = 0;
var lastFeedBarScroll = 0;
var feedBarTopMargin = $('#side-feed-bar').length ? $('#side-feed-bar').offset().top : 0;

$(document).ready(function(){
    onHomeLoad();
});

$(document.body).click(function () {
    closeFeedContextMenu();
});

$(window).on('resize', function () {
    closeFeedContextMenu();
    updateSideFeedMenu();
});

$(window).on('scroll', function () {
    updateSideFeedMenu();
});

function onHomeLoad() {
    if ($('.stackable-card').length) {
        var slideTime = 0;
        $('.stackable-card').each(function () {
            var stackableCard = $(this);
            setTimeout(function () {
                $(stackableCard).show("slide", { direction: "left" }, 250);
            }, slideTime);
            slideTime += 200;
        });
        setTimeout(function () {
            $('#slogan').fadeIn(1000);
        }, 500);
        setTimeout(function () {
            $('#sign-up').slideDown(500);
        }, 1000);
        setTimeout(function () {
            $('#sign-up-button').fadeIn(500);
        }, 1500);
    }
    else if ($('.home-feed').length) {
        if ($('#modal-tutorial').length) {
            loadTutorialModal();
        }

        $('.home-feed').each(function () {
            homeFeedContainers.push(this);
        });

        if (homeFeedContainers.length <= 40) {
            return;
        }

        homeFeedContainers.splice(0, 40);

        for (var i = 0; i < homeFeedContainers.length; i++) {
            var feed = $(homeFeedContainers[i]);
            feed.hide();
        }

        $(window).scroll(function (event) {
            if (homeFeedContainers.length == 0) {
                $(this).off(event);
                return;
            }

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
                var amount = Math.min(20, homeFeedContainers.length);
                for (var i = 0; i < amount; i++) {
                    var feed = $(homeFeedContainers[i]);
                    feed.show();
                }
                homeFeedContainers.splice(0, amount);
            }
        });
    }
}

function updateSideFeedMenu() {
    if (!$('#side-feed-bar').length) {
        return;
    }

    if ($('#profile-panel').is(":visible")) {
        $('#feed').css('min-height', $('#side-feed-bar').height());
    }
    else {
        $('#feed').css('min-height', 0);
    }

    if (!$('#profile-panel').is(":visible") || $('#side-feed-bar')[0].scrollHeight + feedBarTopMargin < window.innerHeight) {
        $('#side-feed-bar')[0].style.marginTop = 0;
        return;
    }

    var delta = window.scrollY - lastFeedBarScroll;
    feedBarScroll += delta;
    lastFeedBarScroll = window.scrollY;

    if (feedBarScroll < 0) {
        feedBarScroll = 0;
    } else if (feedBarScroll > $('#side-feed-bar')[0].scrollHeight - window.innerHeight + feedBarTopMargin * 2) {
        feedBarScroll = $('#side-feed-bar')[0].scrollHeight - window.innerHeight + feedBarTopMargin * 2;
    }

    if (window.scrollY < feedBarTopMargin/2) {
        lastFeedBarScroll = 0;
        feedBarScroll = 0;
    }

    $('#side-feed-bar')[0].style.marginTop = -feedBarScroll + 'px';
}

function showAllChecklist() {
    $('#modal-checklist').modal();
}

function showLoginBonusModal() {
    $('#daily-bonus').modal();
    if ($('#showExpiredMessage').val() == "true") {
        showSnackbarWarning("Your previous daily bonus streak expired!");
    }
}

function showFeedContextMenu(id, offset) {
    var isMobile = !$('#profile-panel').is(":visible");
    var isOpened = $('#feed-menu-' + id).is(":visible");
    closeFeedContextMenu();
    if (!isOpened) {
        $('#feed-menu-' + id).css({ top: offset.top - $('#feed-menu-' + id).height() - (isMobile ? 150 : 35), left: offset.left - (isMobile ? 150 : $('#feed-menu-' + id).width() * 2 + 60) + $('#side-feed-bar').width(), position: 'absolute' });
        $('#feed-menu-' + id).fadeIn(250);
        $('#feed-ellipsis-' + id).addClass('primary-color');
    }
}

function closeFeedContextMenu() {
    $('.feed-menu').fadeOut(250);
    $('.feed-ellipsis').removeClass('primary-color');
}

function hideFromFeed(category, friendlyCategory) {
    if (confirm("Are you sure you want to hide all " + unescape(friendlyCategory) + " posts from your feed? You can always add them back from your Preferences.")) {
        $.ajax({
            type: "POST",
            url: "/Home/HideFeedCategory",
            data: { category : category },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    showToastIfExists();
                    $('.' + category + '-category').hide();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }

    closeFeedContextMenu();
}

function loadTutorialModal() {
    $('#modal-tutorial').modal();
}

function highlightChecklist(scrollToChecklist = true) {
    $('#checklist').removeClass('animated bounce');
    $('#checklist-body').removeClass('fade-out-highlight');
    if (scrollToChecklist) {
        $('html, body').animate({
            scrollTop: $('#checklist').offset().top - 100
        }, 50, function () {
            $('#checklist').addClass('animated bounce');
            $('#checklist-body').addClass('fade-out-highlight');
        });
    }
    else {
        $('#checklist').addClass('animated bounce');
        $('#checklist-body').addClass('fade-out-highlight');
    }
}

function enableEmailNotifications(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                showSnackbarSuccess('Your preferences were successfully updated!');

                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
}

function enableSoundEffects(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                showSnackbarSuccess('Your preferences were successfully updated!');

                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');

                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
}

function sendReferralEmail(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                window.location.href = '/Referrals';
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
};

function redeemDailyBonus(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }

                if ($('#currentDay').length) {
                    $('#redeem-form-' + $('#currentDay').val()).fadeOut(250, function () {
                        $('.hidden-reward').fadeIn();
                        initializeNumberCounter("", function () {
                            $('.hidden-reward').addClass('animated tada');
                            $('#prize-' + $('#currentDay').val()).removeClass('fadeInUp').addClass('tada');
                            if (parseInt($('#currentDay').val()) == 7) {
                                $('#modal-top-text').html('<span class="glyphicon glyphicon-star win-green"></span> Congratulations! You earned the top prize!');
                                $('#modal-top-text').removeClass('fadeInDown').addClass('tada');
                            }
                        });
                        $('#redeem-grid-' + $('#currentDay').val()).removeClass('light-blue-border-important').addClass('win-green-border small-thick-border');
                    });
                }

                $('#' + $('#dailyBonusFeedId').val()).fadeOut();
                showToastIfExists();
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function companionSelected(companion) {
    var companion = $(companion);
    var companionId = companion.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedId').val() == companionId){
        $('#selectedId').val("");
    }
    else{
        $('#selectedId').val(companionId);
        companion.addClass('itemSelected');
    }
}

function updateCaverns(e, form, isSorceress) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (isSorceress) {
                    refreshCavernsPartialView();
                }
                else {
                    refreshCavernsSeedCardsPartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshCavernsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Caverns/GetCavernsPartialView",
        cache: false,
        success: function(data)
        {
            $("#cavernsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshCavernsSeedCardsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Caverns/GetCavernsSeedCardsPartialView",
        cache: false,
        success: function(data)
        {
            $("#cavernsSeedCardsPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateRightSideUpWorld(e, form, isStore) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshRightSideUpWorldPartialView(isStore);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshRightSideUpWorldPartialView(isStore) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isStore: isStore },
        url: "/RightSideUpWorld/GetRightSideUpWorldPartialView",
        success: function(data)
        {
            $("#rightSideUpWorldPartialView").html(data);
            onPageLoad();
        }
    });
}
function secretGiftItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");
    
    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectionId').val() == itemId){
        $('#selectionId').val("");
        $('#submitSecretGiftButton').addClass('disabled');
    }
    else{
        $('#selectionId').val(itemId);
        item.addClass('itemSelected');
        $('#submitSecretGiftButton').removeClass('disabled');
    }
}

function submitSecretGifterForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/Plaza";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
}
$(document).ready(function(){
    onPreferencesLoad();
});

function submitSurveyForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/Plaza";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
function submitMuseumForm(e, form, wing) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (wing == 1) {
                    refreshMuseumEastWingPartialView();
                }
                else if (wing == 2) {
                    refreshMuseumWestWingPartialView();
                }
                else if (wing == 3) {
                    refreshMuseumSouthWingPartialView();
                }
                else if (wing == 4) {
                    refreshMuseumBoutiquePartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }

            $('.modal.in').modal('hide');
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open'); 
        }
    });

    e.preventDefault();
};

function refreshMuseumEastWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumEastWingPartialView",
        success: function(data)
        {
            $("#museumEastWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumWestWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumWestWingPartialView",
        success: function(data)
        {
            $("#museumWestWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumSouthWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumSouthWingPartialView",
        success: function(data)
        {
            $("#museumSouthWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumBoutiquePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumBoutiquePartialView",
        success: function(data)
        {
            $("#museumBoutiquePartialView").html(data);
            onPageLoad();
        }
    });
}
function updateEliteShop(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshEliteShopPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshEliteShopPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshEliteShopPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/EliteShop/GetEliteShopPartialView",
        success: function(data)
        {
            $("#eliteShopPartialView").html(data);
            onPageLoad();
        }
    });
}
$(document).ready(function(){
    onAboutLoad();
});

function onAboutLoad(){
    if ($('.about-counter').length) {
        $('.about-counter').each(function() {
            var counter = this;
            var countTo = $(counter).attr('data-count');
            setTimeout(function () {
                var triggerAtY = $(counter).offset().top - $(window).outerHeight();
                if (triggerAtY <= $(window).scrollTop()) {
                    countToNumber($(counter), countTo, 0, "", null, true);
                }
                else {
                    $(window).scroll(function (event) {
                        triggerAtY = $(counter).offset().top - $(window).outerHeight();
                        if (triggerAtY > $(window).scrollTop()) {
                            return;
                        }
                        countToNumber($(counter), countTo, 0, "", null, true);
                        $(this).off(event);
                    });
                }
            }, 1000);
        });
    }
}
function updateSecretShrineForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshSecretShrinePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshSecretShrinePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/SecretShrine/GetSecretShrinePartialView",
        cache: false,
        success: function(data)
        {
            $("#secretShrinePartialView").html(data);
            onPageLoad();
        }
    });
}
function submitWallForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshWallPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshWallPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Wall/GetWallPartialView",
        cache: false,
        success: function(data)
        {
            $("#wallPartialView").html(data);
            onPageLoad();
        }
    });
}
var loadingMatchingColor = false;

function revealMatchingCard(position) {
    if (loadingMatchingColor) {
        showSnackbarWarning("You did that too fast.");
        return;
    }
    loadingMatchingColor = true;
    $.ajax({
        type: "POST",
        url: "/MatchingColors/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                var showMessage = false;
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                    showMessage = true;
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                    showMessage = true;
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshMatchingColorsPartialView(showMessage);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                loadingMatchingColor = false;
            }
        }
    });
}

function refreshMatchingColorsPartialView(scrollToTop){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/MatchingColors/GetMatchingColorsPartialView",
        success: function(data)
        {
            $("#matchingColorsPartialView").html(data);
            onPageLoad(scrollToTop);
            loadingMatchingColor = false;
        }
    });
}
function submitHighLowForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            refreshHighLowPartialView();
        }
    });

    e.preventDefault();
};

function refreshHighLowPartialView(){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/HighLow/GetHighLowPartialView",
        success: function(data)
        {
            $("#highLowPartialView").html(data);
            onPageLoad(false);
            var highLowNumber = $('#high-low-number');
            var potAmount = $('#pot-amount');
            if (potAmount.length && $('#reveal-card').attr('data-revealed') === "true") {
                var countTo = $(potAmount).attr('data-count');
                if (countTo > 0) {
                    var currency = $(potAmount).text().split(' ')[1];
                    countToNumber($(potAmount), countTo, (countTo - 500)/1.25, " " + currency);
                }
            }
            if (highLowNumber.length && $('#reveal-card').attr('data-revealed') === "false") {
                var countTo = $(highLowNumber).attr('data-count');
                countToNumber($(highLowNumber), countTo);
            }
            $('#reveal-card').hide();
            $('#reveal-card').fadeIn(250);
        }
    });
}
function submitColorWarsForm(e, form, teamColor) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshColorWarsPartialView(teamColor);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshColorWarsPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshColorWarsPartialView(teamColor) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/ColorWars/GetColorWarsPartialView",
        success: function(data)
        {
            $("#colorWarsPartialView").html(data);
            onPageLoad();
            var teamColorPoints = '#points-' + teamColor;
            var countTo = $(teamColorPoints).attr('data-count');
            var currency = $(teamColorPoints).text().split(' ')[1];
            if (countTo > 0) {
                countToNumber($(teamColorPoints), countTo, countTo - 500, " " + currency, function () {
                    $('#points-header-' + teamColor).addClass('animated tada');
                });
            }
        }
    });
}
function spinWheel() {
    $('#wheel').css('pointer-events', 'none');
    $.ajax({
        type: "POST",
        url: "/SpinSuccess/SpinWheel",
        data: { "__RequestVerificationToken" : $('input[name=__RequestVerificationToken]').val() },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#wheel .sec').each(function(){
                    $('#inner-wheel').css({
                        'transform' : 'rotate(' + (-3240 + (60 * data.amount)) + 'deg)'
                    });
                });
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                setTimeout(function () {
                    if (data.successMessage) {
                        $('#success-spin').fadeIn(250);
                        $('#success-spin-message').text(data.successMessage);
                        if (data.buttonText) {
                            $("#spin-success-button").show();
                            $("#spin-success-button-text").text(data.buttonText)
                            $("#spin-success-button-text").attr("href", data.buttonUrl);
                        }
                    }
                    else if (data.dangerMessage) {
                        $('#danger-spin').fadeIn(250);
                        $('#danger-spin-message').text(data.dangerMessage);
                        if (data.buttonText) {
                            $("#spin-danger-button").show();
                            $("#spin-danger-button-text").text(data.buttonText)
                            $("#spin-danger-button-text").attr("href", data.buttonUrl);
                        }
                    }
                    else if (data.infoMessage) {
                        $('#info-spin').fadeIn(250);
                        $('#info-spin-message').text(data.infoMessage);
                    }
                    if (data.points) {
                        updatePoints(data.points);
                    }
                    showToastIfExists();
                    $('#wheel').addClass('faded');
                    $('#wheel').css('pointer-events', '');
                    $('html, body').animate({
                        scrollTop: $('.messages').offset().top - 100
                    }, 500);
                }, 6000);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
};
var offerCardId = "";

function revealOfferCard(position) {
    $.ajax({
        type: "POST",
        url: "/TakeOffer/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshOfferCardsPartialView(position);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitTakeOfferForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshOfferCardsPartialView(-1)
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshOfferCardsPartialView(position){
    var prevOffer = $('#offer-amount').attr('data-count');
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TakeOffer/GetTakeOfferPartialView",
        success: function(data)
        {
            $("#takeOfferPartialView").html(data);
            if (position >= 0) {
                deactivateAllOfferCards();
                offerCardId = '#offer-card-' + position;
                var countTo = $(offerCardId).attr('data-count');
                var currency = $(offerCardId).text().split(' ')[1];
                countToNumber($(offerCardId), countTo, 0, " " + currency, activateAllOfferCards);
                if ($('#offer-amount').length) {
                    countTo = $('#offer-amount').attr('data-count');
                    countToNumber($('#offer-amount'), countTo, prevOffer, " " + currency);
                }
            }
            onPageLoad(false);
        }
    });
}

function activateAllOfferCards() {
    $('.active-offer-card').each(function() {
        $(this).removeClass('faded');
    });
    $('.offer-card').each(function() {
        $(this).css('pointer-events', '');
    });
    $(offerCardId).addClass('animated tada');
    if ($('#offer-amount').length) {
        $('html, body').animate({
            scrollTop: $("#offer-amount").offset().top - 100
        }, 250);
        $('#offer-amount').removeClass('fade-out-highlight').addClass('fade-out-highlight');
    }
}
function deactivateAllOfferCards() {
    $('.active-offer-card').each(function() {
        $(this).addClass('faded');
    });
    $('.offer-card').each(function() {
        $(this).css('pointer-events', 'none');
    });
}
var notificationContainers = [];

$(document).ready(function(){
    onNotificationsLoad();
});

function onNotificationsLoad(){
    if ($('.notification-container').length) {
        $('.notification-container').each(function() {
            notificationContainers.push(this);
        });
       

        if (notificationContainers.length <= 40) {
            return;
        }

        notificationContainers.splice(0, 40);

        for (var i = 0; i < notificationContainers.length; i++) {
            var notification = $(notificationContainers[i]);
            notification.hide();
        }

        $(window).scroll(function(event) {
            if (notificationContainers.length == 0) {
                $(this).off(event);
                return;
            }

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
                var amount = Math.min(20, notificationContainers.length);
                for (var i = 0; i < amount; i++) {
                    var notification = $(notificationContainers[i]);
                    notification.show();
                }
                notificationContainers.splice(0, amount);
            }
        });
    }
}

function updateUnusableStore(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshUnusableStorePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshUnusableStorePartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshUnusableStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/UnusableStore/GetUnusableStorePartialView",
        success: function(data)
        {
            $("#unusableStorePartialView").html(data);
            onPageLoad();
        }
    });
}
var selectedStarterPackIndex = -1;

$(document).ready(function () {
    onIntroLoad();
});

function onIntroLoad() {
    if ($('#intro-currency').length) {
        setTimeout(function () {
            var introCurrency = $('#intro-currency');
            var currency = $(introCurrency).text().split(' ')[1];
            var triggerAtY = $(introCurrency).offset().top - $(window).outerHeight();
            if (triggerAtY <= $(window).scrollTop()) {
                var countTo = $(introCurrency).attr('data-count');
                countToNumber($(introCurrency), countTo, 0, " " + currency, addIntroCurrency);
            }
            else {
                $(window).scroll(function (event) {
                    triggerAtY = $(counter).offset().top - $(window).outerHeight();
                    if (triggerAtY > $(window).scrollTop()) {
                        return;
                    }
                    var countTo = $(introCurrency).attr('data-count');
                    countToNumber($(introCurrency), countTo, 0, " " + currency, addIntroCurrency);
                    $(this).off(event);
                });
            }
        }, 1500);
    }
}

function chooseStarterPack(index) {
    if (index == selectedStarterPackIndex) {
        return;
    }
    $('.starter-pack-label').removeClass('win-green-background');
    $('.starter-pack').removeClass('fixed-grow animated rubberBand');
    $('#starter-pack-label-' + index).addClass('win-green-background');
    $('#starter-pack-small-label-' + index).addClass('win-green-background ');
    $('#pack-' + index).addClass('fixed-grow animated rubberBand');
    $('.card-pack-details').fadeOut(500);
    $('#starter-cards-form').fadeOut(500);
    setTimeout(function () {
        $('#pack-info-' + index).fadeIn(500, function() {
            $('html, body').animate({
                scrollTop: $('#pack-info-' + index).offset().top - 100
            }, 500);
        });
        $('#starter-cards-form').fadeIn(500);
    }, selectedStarterPackIndex == -1 ? 0 : 500);
    selectedStarterPackIndex = index;
    $('#packId').val(selectedStarterPackIndex);
}

function addIntroCurrency() {
    $('#add-currency-form').show().addClass('animated fadeInDown');
}

function submitStarterCardsForm(e, form) {
    e.preventDefault();
    if (confirm("Are you sure you want to choose these cards to start? You'll be able to upgrade your cards and earn new ones later on.")) {
        submitIntroForm(e, form);
    }
    else {
        stopLoadingButton();
    }
}

function checkIfUsernameIsAvailable(username) {
    if (!username) {
        $("#availableUsernameText").text("");
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Intro/IsUsernameAvailable",
        data: { username: username },
        success: function (data) {
            $("#availableUsernameText").text("");
            if (data.success) {
                $("#availableUsernameText").append("<span class='win-green'><b><span class='fas fa-check-circle'></span> " + username + "</b> is available!</span>");
            }
            else {
                $("#availableUsernameText").append("<span class='lose-red'><b><span class='fas fa-times-circle'></span> " + username + "</b> isn't available</span>");
            }
        }
    });
}

function checkIfEmailIsAvailable(emailAddress) {
    if (!emailAddress) {
        $("#availableEmailText").text("");
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Intro/IsEmailAvailable",
        data: { emailAddress: emailAddress },
        success: function (data) {
            $("#availableEmailText").text("");
            if (data.success) {
                $("#availableEmailText").append("<span class='win-green'><b><span class='fas fa-check-circle'></span> " + emailAddress + "</b> is available!</span>");
            }
            else {
                $("#availableEmailText").append("<span class='lose-red'><b><span class='fas fa-times-circle'></span> " + emailAddress + "</b> is not available.</span>");
            }
        }
    });
}

function checkIfAllSignUpFormsAreFilled() {
    if ($('#username').val() != '' && $('#emailAddress').val() != '' && $('#password').val() != '') {
        $('#sign-up-button').removeClass('disabled');
    }
    else {
        $('#sign-up-button').addClass('disabled');
    }
}

function submitIntroForm(e, form, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                    $('#take-currency-intro').hide();
                    $('#start-exploring-intro').show();
                }
                else {
                    refreshIntroPartialView(scrollToTop);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshIntroPartialView(scrollToTop);
                }
            }
        }
    });

    e.preventDefault();
}


function refreshIntroPartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Intro/GetIntroPartialView",
        success: function(data)
        {
            if (data.returnUrl) {
                window.location.href = data.returnUrl;
                return;
            }
            $("#introPartialView").html(data);
            onPageLoad(scrollToTop);
            onIntroLoad();
            starterCardsSelected = 0;
            totalStarterCardBudget = 0;
        }
    });
}


function updateTrainingCenter(e, form, isTraining) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, !isTraining, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTrainingCenterPartialView(isTraining, data.id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function refreshTrainingCenterPartialView(isTraining, selectionId) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TrainingCenter/GetTrainingCenterPartialView",
        data: { isTraining: isTraining },
        success: function(data)
        {
            $("#trainingCenterPartialView").html(data);
            onPageLoad(false);
            if (isTraining) {
                var img = $('#' + selectionId).find('img')
                $('#' + selectionId).removeClass('fade-out-border');
                $(img).removeClass('animated bounceIn');
                $('#' + selectionId).addClass('fade-out-border');
                $(img).addClass('animated bounceIn');
            }
        }
    });
}
function showCrystalBallColors(colors, i) {
    if (i < colors.length) {
        $('#crystal-ball-color').css("background-color", colors[i]);
        $('#color-name').text(colors[i]);
        $('#color-name').fadeIn(500);
        $('#crystal-ball-color').fadeIn(500, function () {
            setTimeout(function () {
                $('#color-name').fadeOut(500);
                $('#crystal-ball-color').fadeOut(500, function () {
                    showCrystalBallColors(colors, i + 1);
                });
            }, 1000);
        });
    }
    if (i == colors.length) {
        $('#crystal-ball-color').css("background-color", "white");
        $('#color-choices').fadeIn(500);
    }
}

function crystalBallColorSelected(color) {
    var chosenColor = $(color).attr('id');
    $('.crystal-ball-color-choice').addClass('faded');
    $('.grid-element').removeClass('fade-out-border-win-green');
    $.ajax({
        type: "POST",
        url: "/CrystalBall/ChooseColor",
        data: { "color": chosenColor },
        success: function (data) {
            $('.crystal-ball-color-choice').removeClass('faded');
            hideAllMessages();
            if (data.success) {
                $(color).addClass('fade-out-border-win-green');
                if (data.successMessage) {
                    showSnackbarSuccess("Nice! " + chosenColor + " was the right choice!");
                    showSuccessMessage(data.successMessage);
                    refreshCrystalBallPartialView();
                }
                else if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                    refreshCrystalBallPartialView();
                }
                else {
                    showSnackbarSuccess("Nice! " + chosenColor + " was the right choice! Keep going!");
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function updateCrystalBall(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#start-form').hide();
                var colors = data.infoMessage.split("-");
                showCrystalBallColors(colors, 0);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshCrystalBallPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/CrystalBall/GetCrystalBallPartialView",
        success: function(data)
        {
            $("#crystalBallPartialView").html(data);
            onPageLoad();
        }
    });
}
$(document).ready(function () {
    onFairLoad();
});

function onFairLoad() {
    if ($('#modal-fair-tutorial').length) {
        $('#modal-fair-tutorial').modal();
    }
    if ($('.fade-out-highlight').length && $('.fair-location').length) {
        $('html, body').animate({
            scrollTop: $('.fade-out-highlight').offset().top - 100
        }, 0);
        $('.fade-out-highlight').parent().scrollLeft($('.fade-out-highlight').offset().left - $('.fade-out-highlight').width() - 50);
    }
}
function updateFairLocation(e, locationId) {
    $.ajax({
        type: "POST",
        url: "/Fair/UpdateLocation",
        data: { locationId: locationId },
        success: function () {
            hideAllMessages();
            var url = "/Fair/GetFairPartialView";
            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                success: function (data) {
                    $("#fairPartialView").html(data);
                    onPageLoad(false);
                    onFairLoad();
                }
            });
        }
    });

    e.preventDefault();
}
var loadingPrizeCup = false;

function revealPrizeCup(position) {
    if (loadingPrizeCup) {
        showSnackbarWarning("You did that too fast.");
        return;
    }
    loadingPrizeCup = true;
    $.ajax({
        type: "POST",
        url: "/PrizeCup/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSnackbarSuccess(data.successMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshPrizeCupPartialView(position);
            }
            else {
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true);
                    refreshPrizeCupPartialView(position);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    loadingPrizeCup = false;
                }
            }
        }
    });
}

function submitPrizeCupForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshPrizeCupPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshPrizeCupPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PrizeCup/GetPrizeCupPartialView",
        cache: false,
        success: function(data)
        {
            var countFrom = $('#pot').length ? $('#pot').attr('data-count') : 0;
            $("#prizeCupPartialView").html(data);
            onPageLoad(false);
            if ($('#pot').length) {
                var currency = $('#pot').text().split(' ')[1];
                var countTo = $('#pot').attr('data-count');
                countToNumber($('#pot'), countTo, countFrom, " " + currency);
            }
            if ($('#pot-form').length) {
                $('html, body').animate({
                    scrollTop: $("#pot-form").offset().top - 100
                }, 250);
            }
            loadingPrizeCup = false;
        }
    });
}
var map_orig_width;
var map_orig_height;
var map_orig_ratio;
var map_scale;
var map_target_scale = 0.5;

$(document).ready(function () {
    onTreasureMapLoad();
});

function onTreasureMapLoad() {
    if (!$('#map').length) {
        return;
    }

    var map = document.querySelector("#map");

    var mapRect = map.getBoundingClientRect();
    map_orig_width = mapRect.width;
    map_orig_height = mapRect.height;

    if (map_orig_width && map_orig_height) {
        map_orig_ratio = map_orig_height / map_orig_width;
    } else {
        map_orig_ratio = 0;
    }

    map.style.backgroundSize = "100% 100%";

    resizeTreasureMap();
}

function resizeTreasureMap() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var map = document.querySelector("#map");

    var target_width = width * map_target_scale;
    var target_height = height * map_target_scale;
    if (map_orig_height > target_height || map_orig_width > target_width) {
        return;
    }

    if (height / width > map_orig_ratio) {
        target_height = target_width * map_orig_ratio;
    } else {
        target_width = target_height / map_orig_ratio;
    }
    map_scale = target_height / map_orig_height;

    map.style.height = target_height + "px";
    map.style.width = target_width + "px";
}

function updateTreasureDive(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTreasureDivePartialView(false);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function treasureMapClicked(e, map) {
    var rect = map.getBoundingClientRect();
    var x = Math.floor(e.offsetX / rect.width * map_orig_width);
    var y = Math.floor(e.offsetY / rect.height * map_orig_height);

    $.ajax({
        type: "POST",
        url: "/TreasureDive/Reveal",
        data: { x: x, y: y },
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTreasureDivePartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshTreasureDivePartialView(isTreasureDive = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isTreasureDive: isTreasureDive },
        url: "/TreasureDive/GetTreasureDivePartialView",
        success: function (data) {
            $("#treasureDivePartialView").html(data);
            onPageLoad(false);
            if (isTreasureDive) {
                onTreasureMapLoad();
            }
        }
    });
}
function updateTower(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};
function buySponsorCoins(e, key, siteName, logoUrl, email, amount, coinAmount) {
    var handler = StripeCheckout.configure({
        key: key,
        image: logoUrl,
        email: email,
        currency: 'usd',
        token: function (token) {
            $.ajax({
                type: "POST",
                url: "SponsorSociety/GetCoinsStripe",
                data: { token: token.id, email: email, amount: amount },
                success: function (data) {
                    hideAllMessages();
                    if (data.success) {
                        if (data.successMessage) {
                            showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                            if (data.soundUrl) {
                                playSound(data.soundUrl);
                            }
                            refreshSponsorSocietyPartialView(true);
                        }
                    }
                    else {
                        showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    }
                }
            });
        }
    });

    handler.open({
        name: siteName + ' Sponsor',
        description: 'Buy ' + coinAmount + ' Coins',
        amount: amount
    });

    e.preventDefault();
}

function updateSponsorSociety(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshSponsorSocietyPartialView(false);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function refreshSponsorSocietyPartialView(isPurchase = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isPurchase : isPurchase },
        url: "/SponsorSociety/GetSponsorSocietyPartialView",
        success: function (data) {
            $("#sponsorSocietyPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateVillager(e, form, isVillager) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshVillagerPartialView(isVillager);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshVillagerPartialView(isVillager) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Villager/GetVillagerPartialView",
        data: { isVillager: isVillager },
        success: function(data)
        {
            $("#villagerPartialView").html(data);
            onPageLoad();
            $('#cards-given').addClass('animated bounceIn');
        }
    });
}
function searchStoreMagician() {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/StoreMagician?searchQuery=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};
function updateWealthyDonors(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshWealthyDonorsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWealthyDonorsPartialView() {
    var currentAmount = $('#currentPot').attr('data-amount');
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WealthyDonors/GetWealthyDonorsPartialView",
        success: function(data)
        {
            $("#wealthyDonorsPartialView").html(data);
            onPageLoad();
            var newAmount = $('#currentPot').attr('data-amount');
            var currency = $('#currentPot').text().split(' ')[1];
            countToNumber($('#currentPot'), newAmount, currentAmount, " " + currency);
            $('#pot').addClass('animated tada');
        }
    });
}
function updateChallengerDome(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};
function pressPhantomRedButton() {
    $.ajax({
        type: "POST",
        url: "/PhantomDoor/PressButton",
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                refreshPhantomDoorPartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function promptUpdatePhantomDoor(e, form) {
    if (confirm("Are you sure you want to permanently destroy the Phantom Door? You cannot return once you do.")) {
        updatePhantomDoor(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function updatePhantomDoor(e, form, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }

                refreshPhantomDoorPartialView(scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }

            $('.modal.in').modal('hide');
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
        }
    });

    e.preventDefault();
};

function refreshPhantomDoorPartialView(scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PhantomDoor/GetPhantomDoorPartialView",
        success: function(data)
        {
            $("#phantomDoorPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}
function deleteTourney(tourneyId) {
    if (confirm("Are you sure you want to delete this tourney? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Tourney/Delete",
            data: { tourneyId : tourneyId },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    window.location.href = "/Tourney";
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
                }
            }
        });
    }
}

function forfeitTourney(e, form, tourneyId) {
    if (confirm("Are you sure you want to forfeit from this tourney? This cannot be undone.")) {
        submitTourneyForm(e, form, tourneyId);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitTourneyForm(e, form, id = 0) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshTourneyPartialView(id);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshTourneyPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/Tourney/GetTourneyPartialView",
        success: function (data) {
            $("#tourneyPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}
function updateDeoJack(e, form, isStand = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDeoJackPartialView(isStand);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshDeoJackPartialView(isStand = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/DeoJack/GetDeoJackPartialView",
        success: function(data)
        {
            $("#deoJackPartialView").html(data);
            if (isStand) {
                $('.flipped-card').hide();
                $('#result').hide();
                $('#dealer-result').text(0);
                $('#end-game-buttons').hide();
                $('#dealer-status').hide();
                var result = 0;
                if ($('#user-result').hasClass('win-green')) {
                    $('#user-result').removeClass('win-green');
                    result = 1;
                }
                else if ($('#user-result').hasClass('lose-red')) {
                    $('#user-result').removeClass('lose-red');
                    result = -1;
                }
                if (result != 0) {
                    $('#dealer-result').removeClass('win-green');
                    $('#dealer-result').removeClass('lose-red');
                }
                var fadeInTime = 0;
                if ($('.flipped-card').length) {
                    $('.flipped-card').each(function (index) {
                        var card = $(this);
                        setTimeout(function () {
                            $(card).fadeIn();
                            $(card).parent().scrollLeft($(card).offset().left - $(card).width() - 50);
                            $('#dealer-result').text($(card).attr('data-count'));
                        }, fadeInTime);
                        if (index < $('.flipped-card').length - 1) {
                            fadeInTime += 1000;
                        }
                    });
                    setTimeout(function () {
                        if (result != 0) {
                            $('#user-result').addClass(result == 1 ? 'win-green' : 'lose-red');
                            $('#dealer-result').addClass(result == 1 ? 'lose-red' : 'win-green');
                        }
                        $('#dealer-status').show();
                        $('#result').fadeIn();
                        $('#end-game-buttons').show();
                        $('.flipped-card').fadeIn();
                        $('html, body').animate({
                            scrollTop: $('#result').offset().top - 100
                        }, 250);
                    }, fadeInTime);
                }
            }
            onPageLoad(false);
        }
    });
}
function updateTopCounter(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTopCounterPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshTopCounterPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TopCounter/GetTopCounterPartialView",
        success: function(data)
        {
            $("#topCounterPartialView").html(data);
            onPageLoad();
        }
    });
}
function chooseBeyondWorldPath(path) {
    if (confirm("Are you sure you want to choose this path? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/BeyondWorld/ChoosePath",
            data: { path : path },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshBeyondWorldPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function closeBeyondWorld(e, form) {
    if (confirm("Are you sure you want to permanently close the Beyond World? You cannot return once you do.")) {
        updateBeyondWorld(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function updateBeyondWorld(e, form, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }

                refreshBeyondWorldPartialView(scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshBeyondWorldPartialView(scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/BeyondWorld/GetBeyondWorldPartialView",
        success: function(data)
        {
            $("#beyondWorldPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}
function updateTopCompanions(e, form, isCellar = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                }
                refreshTopAdoptionsPartialView(isCellar);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshTopAdoptionsPartialView(isCellar = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TopAdoptions/GetTopAdoptionsPartialView",
        data: { isCellar : isCellar },
        success: function(data)
        {
            $("#topAdoptionsPartialView").html(data);
            onPageLoad(false);
        }
    });
}
var cardItemsSelected = 0;

function doubtItCardSelected(card) {
    var card = $(card);
    var cardId = card.attr("id");
    
    if ($('#card1').val() == cardId || $('#card2').val() == cardId || $('#card3').val() == cardId || $('#card4').val() == cardId) {
        if ($('#card1').val() == cardId) {
            $('#card1').val("");
        }
        else if ($('#card2').val() == cardId) {
            $('#card2').val("");
        }
        else if ($('#card3').val() == cardId) {
            $('#card3').val("");
        }
        else if ($('#card4').val() == cardId) {
            $('#card4').val("");
        }
        card.removeClass('itemSelected');
        cardItemsSelected--;
    }
    else if ($('#card1').val() == "" || $('#card2').val() == "" || $('#card3').val() == "" || $('#card4').val() == "") {
        if ($('#card1').val() == "") {
            $('#card1').val(cardId);
        }
        else if ($('#card2').val() == "") {
            $('#card2').val(cardId);
        }
        else if ($('#card3').val() == "") {
            $('#card3').val(cardId);
        }
        else if ($('#card4').val() == "") {
            $('#card4').val(cardId);
        }
        card.addClass('itemSelected');
        cardItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only select up to 4 cards. Unselect a card to play this one.");
    }
    if (cardItemsSelected == 0) {
        $('#play-cards-button').addClass('disabled');
    }
    else {
        $('#play-cards-button').removeClass('disabled');
    }
}

function restartDoubtItGame() {
    if (confirm("Do you want to flip the table and forfeit your Doubt It game?")) {
        $.ajax({
            type: "POST",
            url: "/DoubtIt/NewGame",
            data: { },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshDoubtItPartialView(); 
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function submitDoubtItForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDoubtItPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshDoubtItPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/DoubtIt/GetDoubtItPartialView",
        success: function(data)
        {
            $("#doubtItPartialView").html(data);
            onPageLoad(false);
            cardItemsSelected = 0;
            $('html, body').animate({
                scrollTop: $('#doubt-message').offset().top - 100
            }, 500);
        }
    });
}
function revealLuckyGuessChest(position) {
    $.ajax({
        type: "POST",
        url: "/LuckyGuess/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                refreshLuckyGuessPartialView(true);
            }
            else {
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true);
                    refreshLuckyGuessPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        }
    });
}

function refreshLuckyGuessPartialView(isWinner = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/LuckyGuess/GetLuckyGuessPartialView",
        cache: false,
        success: function(data)
        {
            $("#luckyGuessPartialView").html(data);
            onPageLoad(false);
            if (isWinner) {
                $('#pyro-container').show();
            }
        }
    });
}
function submitArtFactoryForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function (data) {
            hideAllMessages()
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                refreshArtFactoryPartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshArtFactoryPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/ArtFactory/GetArtFactoryPartialView",
        success: function (data) {
            $("#artFactoryPartialView").html(data);
            onPageLoad();
        }
    });
}
function updateClubhouse(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshClubhousePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshClubhousePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Clubhouse/GetClubhousePartialView",
        success: function(data)
        {
            $("#clubhousePartialView").html(data);
            onPageLoad();
        }
    });
}