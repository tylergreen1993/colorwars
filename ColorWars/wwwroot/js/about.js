﻿$(document).ready(function(){
    onAboutLoad();
});

function onAboutLoad(){
    if ($('.about-counter').length) {
        $('.about-counter').each(function() {
            var counter = this;
            var countTo = $(counter).attr('data-count');
            setTimeout(function () {
                var triggerAtY = $(counter).offset().top - $(window).outerHeight();
                if (triggerAtY <= $(window).scrollTop()) {
                    countToNumber($(counter), countTo, 0, "", null, true);
                }
                else {
                    $(window).scroll(function (event) {
                        triggerAtY = $(counter).offset().top - $(window).outerHeight();
                        if (triggerAtY > $(window).scrollTop()) {
                            return;
                        }
                        countToNumber($(counter), countTo, 0, "", null, true);
                        $(this).off(event);
                    });
                }
            }, 1000);
        });
    }
}