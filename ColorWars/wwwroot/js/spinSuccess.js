﻿function spinWheel() {
    $('#wheel').css('pointer-events', 'none');
    $.ajax({
        type: "POST",
        url: "/SpinSuccess/SpinWheel",
        data: { "__RequestVerificationToken" : $('input[name=__RequestVerificationToken]').val() },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#wheel .sec').each(function(){
                    $('#inner-wheel').css({
                        'transform' : 'rotate(' + (-3240 + (60 * data.amount)) + 'deg)'
                    });
                });
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                setTimeout(function () {
                    if (data.successMessage) {
                        $('#success-spin').fadeIn(250);
                        $('#success-spin-message').text(data.successMessage);
                        if (data.buttonText) {
                            $("#spin-success-button").show();
                            $("#spin-success-button-text").text(data.buttonText)
                            $("#spin-success-button-text").attr("href", data.buttonUrl);
                        }
                    }
                    else if (data.dangerMessage) {
                        $('#danger-spin').fadeIn(250);
                        $('#danger-spin-message').text(data.dangerMessage);
                        if (data.buttonText) {
                            $("#spin-danger-button").show();
                            $("#spin-danger-button-text").text(data.buttonText)
                            $("#spin-danger-button-text").attr("href", data.buttonUrl);
                        }
                    }
                    else if (data.infoMessage) {
                        $('#info-spin').fadeIn(250);
                        $('#info-spin-message').text(data.infoMessage);
                    }
                    if (data.points) {
                        updatePoints(data.points);
                    }
                    showToastIfExists();
                    $('#wheel').addClass('faded');
                    $('#wheel').css('pointer-events', '');
                    $('html, body').animate({
                        scrollTop: $('.messages').offset().top - 100
                    }, 500);
                }, 6000);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
};