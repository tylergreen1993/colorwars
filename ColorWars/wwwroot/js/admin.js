﻿function searchUserAdmin() {
    var username = $('#username').val();
    window.location = '/Admin?lookup=' + username;
};

function searchAdmin(curQuery) {
    var searchQuery = $('#searchQuery').val();
    if (window.location.search.includes('query')) {
        window.location.search = window.location.search.replace('query=' + encodeURIComponent(curQuery), 'query=' + encodeURIComponent(searchQuery));
    }
    else {
        window.location.search += '&query=' + encodeURIComponent(searchQuery);
    }
}

function deleteGroupAdmin(id){
    if (confirm("Are you sure you want to remove this group? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveGroup",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function deleteMarketStallAdmin(id){
    if (confirm("Are you sure you want to remove this Market Stall? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/MarketStalls/RemoveMarketStall",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function adminItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#itemId').val() == itemId){
        $('#itemId').val("");
    }
    else{
        $('#itemId').val(itemId);
        item.addClass('itemSelected');
    }
}

function submitAdminForm(e, form, refresh = false, reset = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (refresh){
                    location.reload();
                }
                if (reset){
                    form[0].reset();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};