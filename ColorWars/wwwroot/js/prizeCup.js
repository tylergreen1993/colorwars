﻿var loadingPrizeCup = false;

function revealPrizeCup(position) {
    if (loadingPrizeCup) {
        showSnackbarWarning("You did that too fast.");
        return;
    }
    loadingPrizeCup = true;
    $.ajax({
        type: "POST",
        url: "/PrizeCup/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSnackbarSuccess(data.successMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshPrizeCupPartialView(position);
            }
            else {
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true);
                    refreshPrizeCupPartialView(position);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    loadingPrizeCup = false;
                }
            }
        }
    });
}

function submitPrizeCupForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshPrizeCupPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshPrizeCupPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PrizeCup/GetPrizeCupPartialView",
        cache: false,
        success: function(data)
        {
            var countFrom = $('#pot').length ? $('#pot').attr('data-count') : 0;
            $("#prizeCupPartialView").html(data);
            onPageLoad(false);
            if ($('#pot').length) {
                var currency = $('#pot').text().split(' ')[1];
                var countTo = $('#pot').attr('data-count');
                countToNumber($('#pot'), countTo, countFrom, " " + currency);
            }
            if ($('#pot-form').length) {
                $('html, body').animate({
                    scrollTop: $("#pot-form").offset().top - 100
                }, 250);
            }
            loadingPrizeCup = false;
        }
    });
}