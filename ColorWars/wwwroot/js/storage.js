﻿$(document).ready(function () {
    onStorageLoad();
});


function onStorageLoad() {
    if ($('#storage-items').length){
        $("#storage-items").sortable({
            items: ".grid-element",
            handle: ".draggable-bar",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var selectionId = el.item.attr("id");
                var url = "Storage/UpdatePosition";
                $('.grid-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition : newPosition, selectionId : selectionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshStoragePartialView(true);
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.grid-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $("draggable-bar").disableSelection();
}

function updateStorage(e, form, isWithdraw) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshStoragePartialView(isWithdraw);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function disposeStorageItemConfirmation(e, form, showConfirmation) {
    if (!showConfirmation) {
        disposeStorageItem(e, form);
        e.preventDefault();
        return;
    }

    if (confirm("Are you sure you want to permanently dispose of your item? (You can disable this confirmation in your Preferences)")) {
        disposeStorageItem(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function disposeStorageItem(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                refreshStoragePartialView(true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshStoragePartialView(isWithdraw) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Storage/GetStoragePartialView",
        data: {isWithdraw : isWithdraw},
        success: function(data)
        {
            $("#storagePartialView").html(data);
            onPageLoad(false);
            onStorageLoad();
        }
    });
}