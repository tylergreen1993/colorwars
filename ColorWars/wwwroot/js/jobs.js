﻿$(document).ready(function(){
    onJobsLoad();
});

function onJobsLoad(){
    var dropdownValue = $('#jobsDropdownValue').val();
    if (dropdownValue){
        $('#position').val(dropdownValue);
        $('#position').selectpicker('val', dropdownValue);
    }
}

function updateJobsCountMessage(){
    var jobsMessageLength = $('#content').val().length;
    var remainingLength = 2500 - jobsMessageLength;
    $('#jobsCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function updateHelpersCountMessage(){
    var helpersMessageLength = $('#reason').val().length;
    var remainingLength = 2500 - helpersMessageLength;
    $('#helpersCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function updateJobsDropdown(val){
    $('#position').val(val);
}

function submitJobsForm(e, form, isIndex) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                isIndex ? refreshJobsPartialView() : refreshHelpersPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshJobsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Jobs/GetJobsPartialView",
        success: function(data)
        {
            $("#jobsPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshHelpersPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Jobs/GetHelpersPartialView",
        success: function(data)
        {
            $("#helpersPartialView").html(data);
            onPageLoad();
        }
    });
}