﻿function submitHighLowForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            refreshHighLowPartialView();
        }
    });

    e.preventDefault();
};

function refreshHighLowPartialView(){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/HighLow/GetHighLowPartialView",
        success: function(data)
        {
            $("#highLowPartialView").html(data);
            onPageLoad(false);
            var highLowNumber = $('#high-low-number');
            var potAmount = $('#pot-amount');
            if (potAmount.length && $('#reveal-card').attr('data-revealed') === "true") {
                var countTo = $(potAmount).attr('data-count');
                if (countTo > 0) {
                    var currency = $(potAmount).text().split(' ')[1];
                    countToNumber($(potAmount), countTo, (countTo - 500)/1.25, " " + currency);
                }
            }
            if (highLowNumber.length && $('#reveal-card').attr('data-revealed') === "false") {
                var countTo = $(highLowNumber).attr('data-count');
                countToNumber($(highLowNumber), countTo);
            }
            $('#reveal-card').hide();
            $('#reveal-card').fadeIn(250);
        }
    });
}