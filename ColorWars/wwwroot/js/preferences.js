﻿$(document).ready(function(){
    onPreferencesLoad();
});

function onPreferencesLoad() {
    var dropdownValue = $('#pointsLocationDropdownValue').val();
    if (dropdownValue){
        $('#pointsLocation').val(dropdownValue);
        $('#pointsLocation').selectpicker('val', dropdownValue);
    }
    var dropdownValueBadge = $('#activeBadgeLocation').val();
    if (dropdownValueBadge) {
        $('#activeBadge').val(dropdownValueBadge);
        $('#activeBadge').selectpicker('val', dropdownValueBadge);
    }
    if ($('#enableMessagesTab').length) {
        if ($('#enableMessagesTab').is(":checked")) {
            $('#messages-box-button').show();
        }
        else {
            $('#messages-box-button').hide();
        }
    }
}

function updateBioCountMessage(){
    var bioLength = $('#bio').val().length;
    var remainingLength = 350 - bioLength;
    $('#bioCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function updateNotesCountMessage() {
    var notesLength = $('#notes').val().length;
    var remainingLength = 2500 - notesLength;
    $('#notesCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function removeDisplayPic() {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveDisplayPic",
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                $('#upload-pic').show();
                $('#existing-pic').hide();
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}


function resendEmailVerification() {
    $.ajax({
        type: "POST",
        url: "/Preferences/ResendEmailVerification",
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function removeBlockedUser(username) {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveBlockedUser",
        data: { username : username },
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, false);
                }
                refreshPreferencesPartialView(false)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function updatePreferencesCircle(color) {
    $('.small-circle ').css('background-color', color.value);
}

function removeHiddenFeedCategory(category) {
    $.ajax({
        type: "POST",
        url: "/Preferences/RemoveHiddenFeedCategory",
        data: { category : category },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                refreshPreferencesPartialView(false)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitPreferencesForm(e, form, scrollToTop = true, loadNotes = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                refreshPreferencesPartialView(scrollToTop, loadNotes)
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage, false);
            }
        }
    });

    e.preventDefault();
};

function refreshPreferencesPartialView(scrollToTop = true, loadNotes = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Preferences/GetPreferencesPartialView",
        data: { loadNotes : loadNotes },
        success: function(data)
        {
            $("#preferencesPartialView").html(data);
            onPageLoad(scrollToTop);
            onPreferencesLoad();
        }
    });
}