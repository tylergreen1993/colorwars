﻿function submitReferralsForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    refreshReferralsPartialView();
                }
                else {
                    $('#emailAddress').val('');
                    stopLoadingButton();
                    showSnackbarSuccess("Your email was successfully sent!");
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshReferralsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Referrals/GetReferralsPartialView",
        success: function (data) {
            $("#referralsPartialView").html(data);
            onPageLoad(false);
        }
    });
}