﻿$(document).ready(function () {
    onFairLoad();
});

function onFairLoad() {
    if ($('#modal-fair-tutorial').length) {
        $('#modal-fair-tutorial').modal();
    }
    if ($('.fade-out-highlight').length && $('.fair-location').length) {
        $('html, body').animate({
            scrollTop: $('.fade-out-highlight').offset().top - 100
        }, 0);
        $('.fade-out-highlight').parent().scrollLeft($('.fade-out-highlight').offset().left - $('.fade-out-highlight').width() - 50);
    }
}
function updateFairLocation(e, locationId) {
    $.ajax({
        type: "POST",
        url: "/Fair/UpdateLocation",
        data: { locationId: locationId },
        success: function () {
            hideAllMessages();
            var url = "/Fair/GetFairPartialView";
            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                success: function (data) {
                    $("#fairPartialView").html(data);
                    onPageLoad(false);
                    onFairLoad();
                }
            });
        }
    });

    e.preventDefault();
}