﻿function updateDescriptionCountMessage(){
    var showroomDescriptionLength = $('#description').val().length;
    var remainingLength = 350 - showroomDescriptionLength;
    $('#descriptionCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function submitBuyersClubForm(e, form, isBuyersClub, showDiscount) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (isBuyersClub) {
                    refreshBuyersClubPartialView(showDiscount);
                }
                else {
                    refreshBuyersClubShowroomPartialView(data.shouldRefresh);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshBuyersClubPartialView(showDiscount);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshBuyersClubPartialView(showDiscount) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { showDiscount : showDiscount },
        url: "/BuyersClub/GetBuyersClubPartialView",
        success: function(data)
        {
            $("#buyersClubPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshBuyersClubShowroomPartialView(showTitleChange) {
    $.ajax({
        type: "GET",
        cache: false,
        data : { showTitleChange : showTitleChange },
        url: "/BuyersClub/GetBuyersClubShowroomPartialView",
        success: function(data)
        {
            $("#buyersClubShowroomPartialView").html(data);
            onPageLoad();
        }
    });
}