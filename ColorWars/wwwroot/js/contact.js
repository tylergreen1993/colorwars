﻿function updateContactCountMessage(){
    var contactMessageLength = $('#message').val().length;
    var remainingLength = 2500 - contactMessageLength;
    $('#contactCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function showSendMessageContactOption() {
    if ($('#contact-message').length) {
        $('#contact-message').hide();
        var selectedReason = $('#category').val();
        if (selectedReason == "GeneralHelp" || selectedReason == "GeneralQuestion") {
            $('#contact-message').show();
        }
    }
}

function submitContactForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};