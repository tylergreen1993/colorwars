﻿function pressPhantomRedButton() {
    $.ajax({
        type: "POST",
        url: "/PhantomDoor/PressButton",
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                refreshPhantomDoorPartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function promptUpdatePhantomDoor(e, form) {
    if (confirm("Are you sure you want to permanently destroy the Phantom Door? You cannot return once you do.")) {
        updatePhantomDoor(e, form);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function updatePhantomDoor(e, form, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }

                refreshPhantomDoorPartialView(scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }

            $('.modal.in').modal('hide');
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
        }
    });

    e.preventDefault();
};

function refreshPhantomDoorPartialView(scrollToTop) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PhantomDoor/GetPhantomDoorPartialView",
        success: function(data)
        {
            $("#phantomDoorPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}