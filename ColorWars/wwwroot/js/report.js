﻿function submitReportForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location.href = "/";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};