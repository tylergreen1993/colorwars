﻿$(document).ready(function(){
    onAuctionsLoad();
});

function onAuctionsLoad(){
    var dropdownCategoryValue = $('#itemCategoryDropdownValue').val();
    if (dropdownCategoryValue){
        $('#category').val(dropdownCategoryValue);
        $('#category').selectpicker('val', dropdownCategoryValue);
    }
    var dropdownConditionValue = $('#itemConditionDropdownValue').val();
    if (dropdownConditionValue){
        $('#condition').val(dropdownConditionValue);
        $('#condition').selectpicker('val', dropdownConditionValue);
    }
}

function searchAuction(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/Auction?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition;
};

function deleteAuction(id) {
    if (confirm("Are you sure you want to end this auction? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Auction/EndAuction",
            data: { id: id },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    window.location.href = "/Auction";
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function updateAuctionCreate(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success == false){
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            else{
                $("#auctionCreatePartialView").html(data);
                onPageLoad();
            }
        }
    });

    e.preventDefault();
};

function submitBidAuctionForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshAuctionBidPartialView(id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshAuctionBidPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function submitCollectAuctionForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshAuctionCollectionPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitCreateAuctionForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/Auction?id=" + data.id;
            }
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshAuctionCreatePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Auction/GetAuctionCreatePartialView",
        success: function(data)
        {
            $("#auctionCreatePartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshAuctionBidPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: {id : id},
        url: "/Auction/GetAuctionBidPartialView",
        success: function(data)
        {
            $("#auctionPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshAuctionCollectionPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: {id : id},
        url: "/Auction/GetAuctionCollectionPartialView",
        success: function(data)
        {
            $("#auctionCollectionPartialView").html(data);
            onPageLoad();
        }
    });
}

