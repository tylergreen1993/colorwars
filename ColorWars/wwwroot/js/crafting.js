﻿var craftingItemsSelected = 0;

function craftingCardSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    if ($('#item1').val() == itemId || $('#item2').val() == itemId || $('#item3').val() == itemId) {
        if ($('#item1').val() == itemId) {
            $('#item1').val("");
        }
        else if ($('#item2').val() == itemId) {
            $('#item2').val("");
        }
        else if ($('#item3').val() == itemId) {
            $('#item3').val("");
        }
        item.removeClass('itemSelected');
        craftingItemsSelected--;
    }
    else if ($('#item1').val() == "" || $('#item2').val() == "" || $('#item3').val() == "") {
        if ($('#item1').val() == "") {
            $('#item1').val(itemId);
        }
        else if ($('#item2').val() == "") {
            $('#item2').val(itemId);
        }
        else if ($('#item3').val() == "") {
            $('#item3').val(itemId);
        }
        item.addClass('itemSelected');
        craftingItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only use 3 cards in your Recipe. Remove a card to add this one.");
    }
    if (craftingItemsSelected != 3) {
        $('.faded').removeClass('faded');
        $('#craftButton').addClass('disabled');
    }
    else {
        $('#craftButton').removeClass('disabled');
        $('.crafting-card').each(function () {
            if (!$(this).hasClass('itemSelected')) {
                $(this).addClass('faded');
            }
        });
        $('html, body').animate({
            scrollTop: $('#craftButton').offset().top - 100
        }, 500);
    }
}

function resetSelectedCraftingCards() {
    craftingItemsSelected = 0;
    $('#item1').val("");
    $('#item2').val("");
    $('#item3').val("");
    $('.faded').removeClass('faded');
    $('#craftButton').addClass('disabled');
    $('.itemSelected').removeClass('itemSelected');
}

function updateCrafting(e, form, isCrafting) {
    form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (isCrafting) {
                    refreshingCraftingPartialView();
                }
                else {
                    refreshingFurnacePartialView();
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshingCraftingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Crafting/GetCraftingPartialView",
        success: function(data)
        {
            $("#craftingPartialView").html(data);
            onPageLoad();
            craftingCards = [];
            recipeCards = [];
        }
    });
}

function refreshingFurnacePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Crafting/GetFurnacePartialView",
        success: function(data)
        {
            $("#craftingFurnacePartialView").html(data);
            onPageLoad();
        }
    });
}
