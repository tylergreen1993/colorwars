﻿function updateBank(e, form, isBank) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshBankPartialView(isBank)
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshBankPartialView(isBank) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isBank : isBank },
        url: "/Bank/GetBankPartialView",
        success: function(data)
        {
            $("#bankPartialView").html(data);
            onPageLoad();
        }
    });
}