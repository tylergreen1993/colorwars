﻿function updateJunkyard(e, form, isBuy) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshJunkyardPartialView(isBuy);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshJunkyardPartialView(isBuy);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshJunkyardPartialView(isBuy) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Junkyard/GetJunkyardPartialView",
        data: {isBuy : isBuy},
        success: function(data)
        {
            $("#junkyardPartialView").html(data);
            onPageLoad(false);
        }
    });
}