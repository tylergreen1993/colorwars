﻿function updateRightSideUpWorld(e, form, isStore) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshRightSideUpWorldPartialView(isStore);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshRightSideUpWorldPartialView(isStore) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isStore: isStore },
        url: "/RightSideUpWorld/GetRightSideUpWorldPartialView",
        success: function(data)
        {
            $("#rightSideUpWorldPartialView").html(data);
            onPageLoad();
        }
    });
}