﻿function submitArtFactoryForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function (data) {
            hideAllMessages()
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                refreshArtFactoryPartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshArtFactoryPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/ArtFactory/GetArtFactoryPartialView",
        success: function (data) {
            $("#artFactoryPartialView").html(data);
            onPageLoad();
        }
    });
}