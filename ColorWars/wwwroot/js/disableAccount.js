﻿function submitDisableAccountForm(e, form) {
    if (confirm("Are you sure you want to temporarily disable your account?")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    window.location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};