﻿function checkIfPromoBoxIsEnabled(val){
    if (val === "") {
        $('#redeemPromoCodeButton').addClass('disabled');
    }
    else {
        $('#redeemPromoCodeButton').removeClass('disabled');
    }
}

function updatePromoCodes(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            $('#promoCode').val('');
            $('#redeemPromoCodeButton').addClass('disabled');
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshPromoCodesPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshPromoCodesPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/PromoCodes/GetPromoCodesPartialView",
        success: function(data)
        {
            $("#promoCodesPartialView").html(data);
            onPageLoad();
        }
    });
}