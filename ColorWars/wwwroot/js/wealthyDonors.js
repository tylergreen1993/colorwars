﻿function updateWealthyDonors(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshWealthyDonorsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWealthyDonorsPartialView() {
    var currentAmount = $('#currentPot').attr('data-amount');
    $.ajax({
        type: "GET",
        cache: false,
        url: "/WealthyDonors/GetWealthyDonorsPartialView",
        success: function(data)
        {
            $("#wealthyDonorsPartialView").html(data);
            onPageLoad();
            var newAmount = $('#currentPot').attr('data-amount');
            var currency = $('#currentPot').text().split(' ')[1];
            countToNumber($('#currentPot'), newAmount, currentAmount, " " + currency);
            $('#pot').addClass('animated tada');
        }
    });
}