﻿function updateTrainingCenter(e, form, isTraining) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, !isTraining, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTrainingCenterPartialView(isTraining, data.id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function refreshTrainingCenterPartialView(isTraining, selectionId) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TrainingCenter/GetTrainingCenterPartialView",
        data: { isTraining: isTraining },
        success: function(data)
        {
            $("#trainingCenterPartialView").html(data);
            onPageLoad(false);
            if (isTraining) {
                var img = $('#' + selectionId).find('img')
                $('#' + selectionId).removeClass('fade-out-border');
                $(img).removeClass('animated bounceIn');
                $('#' + selectionId).addClass('fade-out-border');
                $(img).addClass('animated bounceIn');
            }
        }
    });
}