﻿function updateTopCompanions(e, form, isCellar = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                }
                refreshTopAdoptionsPartialView(isCellar);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshTopAdoptionsPartialView(isCellar = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TopAdoptions/GetTopAdoptionsPartialView",
        data: { isCellar : isCellar },
        success: function(data)
        {
            $("#topAdoptionsPartialView").html(data);
            onPageLoad(false);
        }
    });
}