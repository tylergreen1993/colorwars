﻿function updateUnusableStore(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshUnusableStorePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshUnusableStorePartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshUnusableStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/UnusableStore/GetUnusableStorePartialView",
        success: function(data)
        {
            $("#unusableStorePartialView").html(data);
            onPageLoad();
        }
    });
}