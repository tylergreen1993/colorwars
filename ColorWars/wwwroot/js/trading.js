﻿var tradingItemsSelected = 0;

function searchTrades(page) {
    var searchQuery = $('#searchQuery').val();
    var matchExactSearch = $('#matchExactSearch').prop('checked');
    var category = $('#category').val();
    var condition = $('#condition').val();
    window.location = '/Trading?page=' + page + '&query=' + encodeURIComponent(searchQuery) + '&matchExactSearch=' + matchExactSearch + '&category=' + category + '&condition=' + condition
};

function updateTradingDescriptionCountMessage(){
    var tradingDescriptionLength = $('#request').val().length;
    var remainingLength = 500 - tradingDescriptionLength;
    $('#tradingDescriptionCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function editTradeDescription() {
    stopLoadingButton();
    if ($('#trade-request-edit').is(":visible")) {
        $('#trade-request-edit').hide();
        $('#trade-request-content').show();
    }
    else {
        $('#trade-request-edit').show();
        $('#trade-request-content').hide();
    }
}

function deleteTrade(id){
    if (confirm("Are you sure you want to remove your posting? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Trading/RemoveTrade",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Trading";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshTradingOfferPartialView(id);
                    }
                }
            }
        });
    }
}

function tradingItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");
    
    if ($('#item1').val() == itemId || $('#item2').val() == itemId || $('#item3').val() == itemId) {
        if ($('#item1').val() == itemId) {
            $('#item1').val("");
        }
        else if ($('#item2').val() == itemId) {
            $('#item2').val("");
        }
        else if ($('#item3').val() == itemId) {
            $('#item3').val("");
        }
        item.removeClass('itemSelected');
        tradingItemsSelected--;
    }
    else if ($('#item1').val() == "" || $('#item2').val() == "" || $('#item3').val() == "") {
        if ($('#item1').val() == "") {
            $('#item1').val(itemId);
        }
        else if ($('#item2').val() == "") {
            $('#item2').val(itemId);
        }
        else if ($('#item3').val() == "") {
            $('#item3').val(itemId);
        }
        item.addClass('itemSelected');
        tradingItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only select up to 3 items. Remove an item from your trade posting to add this one.");
    }
    $('#tradingItemsSelected').text(tradingItemsSelected);
    if (tradingItemsSelected == 0) {
        $('#tradeButton').addClass('disabled');
    }
    else {
        $('#tradeButton').removeClass('disabled');
    }
}

function submitTradingCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/Trading?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTradingCreatePartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function submitTradingOfferForm(e, form, id) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshTradingOfferPartialView(id);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTradingOfferPartialView(id);
                }
            }
        }
    });

    e.preventDefault();
}

function refreshTradingCreatePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Trading/GetTradingCreatePartialView",
        success: function(data)
        {
            $("#tradingCreatePartialView").html(data);
            onPageLoad();
            tradingItemsSelected = 0;
        }
    });
}

function refreshTradingOfferPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Trading/GetTradingOfferPartialView",
        data: { id : id },
        success: function(data)
        {
            $("#tradingPartialView").html(data);
            onPageLoad();
            tradingItemsSelected = 0;
        }
    });
}

