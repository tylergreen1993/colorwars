﻿var map_orig_width;
var map_orig_height;
var map_orig_ratio;
var map_scale;
var map_target_scale = 0.5;

$(document).ready(function () {
    onTreasureMapLoad();
});

function onTreasureMapLoad() {
    if (!$('#map').length) {
        return;
    }

    var map = document.querySelector("#map");

    var mapRect = map.getBoundingClientRect();
    map_orig_width = mapRect.width;
    map_orig_height = mapRect.height;

    if (map_orig_width && map_orig_height) {
        map_orig_ratio = map_orig_height / map_orig_width;
    } else {
        map_orig_ratio = 0;
    }

    map.style.backgroundSize = "100% 100%";

    resizeTreasureMap();
}

function resizeTreasureMap() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    var map = document.querySelector("#map");

    var target_width = width * map_target_scale;
    var target_height = height * map_target_scale;
    if (map_orig_height > target_height || map_orig_width > target_width) {
        return;
    }

    if (height / width > map_orig_ratio) {
        target_height = target_width * map_orig_ratio;
    } else {
        target_width = target_height / map_orig_ratio;
    }
    map_scale = target_height / map_orig_height;

    map.style.height = target_height + "px";
    map.style.width = target_width + "px";
}

function updateTreasureDive(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTreasureDivePartialView(false);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function treasureMapClicked(e, map) {
    var rect = map.getBoundingClientRect();
    var x = Math.floor(e.offsetX / rect.width * map_orig_width);
    var y = Math.floor(e.offsetY / rect.height * map_orig_height);

    $.ajax({
        type: "POST",
        url: "/TreasureDive/Reveal",
        data: { x: x, y: y },
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshTreasureDivePartialView();
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshTreasureDivePartialView(isTreasureDive = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isTreasureDive: isTreasureDive },
        url: "/TreasureDive/GetTreasureDivePartialView",
        success: function (data) {
            $("#treasureDivePartialView").html(data);
            onPageLoad(false);
            if (isTreasureDive) {
                onTreasureMapLoad();
            }
        }
    });
}