﻿$(document).ready(function(){
    onPreferencesLoad();
});

function submitSurveyForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                window.location.href = "/Plaza";
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};