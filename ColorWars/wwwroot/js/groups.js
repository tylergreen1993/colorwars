﻿$(document).ready(function(){
    onGroupsLoad();
    pollForNewGroupMessages();
});

function onGroupsLoad() {
    var dropdownValue = $('#groupPowerDropdownValue').val();
    if (dropdownValue) {
        $('#power').val(dropdownValue);
        $('#power').selectpicker('val', dropdownValue);
    }
    $('#groupsMessagesScrollContainer').scrollTop($('#groupMessagesContainer').height());
    var messageContainerOffset = $('#groups-messages-container').offset();
    if (messageContainerOffset){
        $(window).scrollTop(messageContainerOffset.top - window.innerHeight + $('#groups-messages-container').height() * 2);
    }
}

function updateGroupsDescriptionCountMessage(){
    var groupsDescriptionLength = $('#description').val().length;
    var remainingLength = 500 - groupsDescriptionLength;
    $('#groupsCreateCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function updateWhiteboardCountMessage(){
    var whiteboardMessageLength = $('#whiteboard').val().length;
    var remainingLength = 500 - whiteboardMessageLength;
    $('#whiteboardCountMessage').text(remainingLength >= 0 ? remainingLength + ' remaining' : 'Too long!');
}

function searchGroups(page) {
    var searchQuery = $('#searchQuery').val();
    window.location = '/Groups/All?page=' + page + '&query=' + encodeURIComponent(searchQuery);
};

function editWhiteboard() {
    stopLoadingButton();
    if ($('#whiteboardEdit').is(":visible")) {
        $('#whiteboardEdit').hide();
        $('#whiteboardContent').show();
    }
    else {
        $('#whiteboardEdit').show();
        $('#whiteboardContent').hide();
    }
}

function removeGroupDisplayPic(groupId) {
    $.ajax({
        type: "POST",
        url: "/Groups/RemoveDisplayPic",
        data: { groupId: groupId },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                $('#upload-pic').show();
                $('#existing-pic').hide();
            }
            else {
                stopLoadingButton();
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function setAsPrimaryGroup(groupId) {
    $.ajax({
        type: "POST",
        url: "/Groups/SetPrimaryGroup",
        data: { groupId: groupId },
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                    refreshUserGroupsPartialView();
                }
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function deleteGroup(id){
    if (confirm("Are you sure you want to remove your group? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveGroup",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Groups";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    if (data.shouldRefresh) {
                        refreshGroupsPartialView(id);
                    }
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }
}

function deleteGroupMessage(id, groupId) {
    if (confirm("Are you sure you want to delete this message? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Groups/RemoveMessage",
            data: { id : id, groupId : groupId },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshGroupMessagesPartialView(groupId);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function submitGroupsIdForm(e, form, id, scrollToTop = true) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                showSuccessMessage(data.successMessage, scrollToTop);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshGroupsPartialView(id, scrollToTop);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function submitGroupCreateForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                window.location.href = "/Groups?id=" + data.id;
            }
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function submitGroupsMessageForm(e, form, groupId) {
    form = $(form);
    var url = form.attr('action');
    if ($('#message').val().includes("@here")) {
        if (confirm("Are you sure you want to send this message and notify all members of this group?")) {
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function (data) {
                    hideAllMessages();
                    if (data.success) {
                        $('#message').val("");
                        refreshGroupMessagesPartialView(groupId);
                    }
                    else {
                        showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    }
                }
            });
        }
        else {
            stopLoadingButton();
        }
    }
    else {
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function (data) {
                hideAllMessages();
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.success) {
                    $('#message').val("");
                    refreshGroupMessagesPartialView(groupId);
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }

    e.preventDefault();
};

function refreshGroupsPartialView(id, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetGroupsPartialView",
        data: {id : id },
        success: function(data)
        {
            $("#groupsPartialView").html(data);
            onPageLoad(scrollToTop);
            onGroupsLoad();
        }
    });
}

function refreshUserGroupsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetUserGroupsPartialView",
        success: function (data) {
            $("#groupsUserPartialView").html(data);
            onPageLoad();
            onGroupsLoad();
        }
    });
}

function refreshGroupMessagesPartialView(groupId) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetGroupMessagesPartialView",
        data: { groupId: groupId },
        success: function(data)
        {
            $("#groupsMessagesScrollContainer").html(data);
            onPageLoad();
            onGroupsLoad();
        }
    });
}

function pollForNewGroupMessages(){
    var shouldPollForGroupMessages = $('#groupsMessagesScrollContainer');
    if (!shouldPollForGroupMessages.length){
        return;
    }

    var groupId = $('#group-id').val();

    $.ajax({
        type: "GET",
        cache: false,
        url: "/Groups/GetUnreadGroupMessages",
        data: { groupId : groupId },
        success: function(data)
        {
            if (data.success && data.amount > 0){
                refreshGroupMessagesPartialView(groupId);
            }

            setTimeout(function(){
                pollForNewGroupMessages();
            }, 1500);
        }
    });
}