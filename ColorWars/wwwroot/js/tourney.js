﻿function deleteTourney(tourneyId) {
    if (confirm("Are you sure you want to delete this tourney? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Tourney/Delete",
            data: { tourneyId : tourneyId },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    window.location.href = "/Tourney";
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
                }
            }
        });
    }
}

function forfeitTourney(e, form, tourneyId) {
    if (confirm("Are you sure you want to forfeit from this tourney? This cannot be undone.")) {
        submitTourneyForm(e, form, tourneyId);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
}

function submitTourneyForm(e, form, id = 0) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshTourneyPartialView(id);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshTourneyPartialView(id) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/Tourney/GetTourneyPartialView",
        success: function (data) {
            $("#tourneyPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });
}