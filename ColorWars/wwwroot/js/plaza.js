﻿var locationsTimer = null;

$(document).ready(function () {
    onLocationsLoad();
});

function onLocationsLoad() {
    if ($('#modal-locations-tutorial').length) {
        $('#modal-locations-tutorial').modal();
    }
    if ($('#lastLocation').length) {
        var lastLocation = '#' + $('#lastLocation').val();
        if ($(lastLocation).length) {
            if ($(lastLocation).hasClass('display-none')) {
                showRemainingLocations($(lastLocation).parent().find('.flex-scrolling-element'));
            }
            $(window).scrollTop($(lastLocation).offset().top - 50);
            $(lastLocation).parent().scrollLeft($(lastLocation).offset().left - $(lastLocation).width() - 50);
        }
    }
    if ($('#Favorites-container').length) {
        $("#Favorites-container").sortable({
            items: ".location-element",
            handle: ".draggable-bar",
            axis: $('.grid-location-container').length ? "" : "x",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var locationId = el.item.attr("id");
                var url = "/Plaza/UpdatePosition";
                $('.location-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, locationId: locationId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshPlazaPartialView();
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.location-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $("draggable-bar").disableSelection();
}

function delayedKeyUpUpdateLocations(e, form, scrollToTop = true) {
    clearTimeout(locationsTimer);
    locationsTimer = setTimeout(function () {
        updateLocations(e, form, scrollToTop);
    }, 250);
};

function hideShowPlazaCategory(plazaCategoryId) {
    var plazaCategorySection = $('.' + plazaCategoryId + "-section");
    if ($(plazaCategorySection).is(":visible")) {
        $(plazaCategorySection).slideUp();
        $('#' + plazaCategoryId + '-chevron').attr('data-icon', 'chevron-down');
    }
    else {
        $(plazaCategorySection).slideDown();
        $('#' + plazaCategoryId + '-chevron').attr('data-icon', 'chevron-up');
    }
};

function showRemainingLocations(el) {
    var parent = $(el).parent();
    $(parent).children().each(function () {
        var child = $(this);
        if ($(child).hasClass('display-none')) {
            $(child).fadeIn();
        }
    });
    $(el).hide();
}

function togglePlazaView() {
    $.ajax({
        type: "POST",
        url: "/Plaza/ToggleView",
        success: function () {
            window.location.href = window.location.pathname;
        }
    });
}

function updateLocation(e, form, locationId, key){
    $.ajax({
        type: "POST",
        url: "/Plaza/UpdateLocation",
        data: { locationId : locationId },
        success: function()
        {
            hideAllMessages();
            updateLocations(e, form, false, key);
        }
    });

    e.preventDefault();
}

function updateLocations(e, form, scrollToTop = true, expandKey = "") {
    form = $(form);
    var url = "/Plaza/GetPlazaPartialView";
    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            var lastPlacement = 0;
            if (expandKey != "") {
                lastPlacement = $('#' + expandKey).find('.scrolling-container').scrollLeft();
            }
            $("#plazaPartialView").html(data);
            if (expandKey != "") {
                showRemainingLocations($('#' + expandKey + '-expand'));
                $('#' + expandKey).find('.scrolling-container').scrollLeft(lastPlacement);
            }
            onPageLoad(scrollToTop);
            onLocationsLoad();
        }
    });

    e.preventDefault();
};

function refreshPlazaPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Plaza/GetPlazaPartialView",
        success: function (data) {
            $("#plazaPartialView").html(data);
            onPageLoad();
            onLocationsLoad();
        }
    });
}