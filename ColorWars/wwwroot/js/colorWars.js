﻿function submitColorWarsForm(e, form, teamColor) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshColorWarsPartialView(teamColor);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshColorWarsPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshColorWarsPartialView(teamColor) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/ColorWars/GetColorWarsPartialView",
        success: function(data)
        {
            $("#colorWarsPartialView").html(data);
            onPageLoad();
            var teamColorPoints = '#points-' + teamColor;
            var countTo = $(teamColorPoints).attr('data-count');
            var currency = $(teamColorPoints).text().split(' ')[1];
            if (countTo > 0) {
                countToNumber($(teamColorPoints), countTo, countTo - 500, " " + currency, function () {
                    $('#points-header-' + teamColor).addClass('animated tada');
                });
            }
        }
    });
}