﻿function updateEffects(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshEffectsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshEffectsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Effects/GetEffectsPartialView",
        success: function(data)
        {
            $("#effectsPartialView").html(data);
            onPageLoad();
        }
    });
}