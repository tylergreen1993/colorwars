﻿var selectedStarterPackIndex = -1;

$(document).ready(function () {
    onIntroLoad();
});

function onIntroLoad() {
    if ($('#intro-currency').length) {
        setTimeout(function () {
            var introCurrency = $('#intro-currency');
            var currency = $(introCurrency).text().split(' ')[1];
            var triggerAtY = $(introCurrency).offset().top - $(window).outerHeight();
            if (triggerAtY <= $(window).scrollTop()) {
                var countTo = $(introCurrency).attr('data-count');
                countToNumber($(introCurrency), countTo, 0, " " + currency, addIntroCurrency);
            }
            else {
                $(window).scroll(function (event) {
                    triggerAtY = $(counter).offset().top - $(window).outerHeight();
                    if (triggerAtY > $(window).scrollTop()) {
                        return;
                    }
                    var countTo = $(introCurrency).attr('data-count');
                    countToNumber($(introCurrency), countTo, 0, " " + currency, addIntroCurrency);
                    $(this).off(event);
                });
            }
        }, 1500);
    }
}

function chooseStarterPack(index) {
    if (index == selectedStarterPackIndex) {
        return;
    }
    $('.starter-pack-label').removeClass('win-green-background');
    $('.starter-pack').removeClass('fixed-grow animated rubberBand');
    $('#starter-pack-label-' + index).addClass('win-green-background');
    $('#starter-pack-small-label-' + index).addClass('win-green-background ');
    $('#pack-' + index).addClass('fixed-grow animated rubberBand');
    $('.card-pack-details').fadeOut(500);
    $('#starter-cards-form').fadeOut(500);
    setTimeout(function () {
        $('#pack-info-' + index).fadeIn(500, function() {
            $('html, body').animate({
                scrollTop: $('#pack-info-' + index).offset().top - 100
            }, 500);
        });
        $('#starter-cards-form').fadeIn(500);
    }, selectedStarterPackIndex == -1 ? 0 : 500);
    selectedStarterPackIndex = index;
    $('#packId').val(selectedStarterPackIndex);
}

function addIntroCurrency() {
    $('#add-currency-form').show().addClass('animated fadeInDown');
}

function submitStarterCardsForm(e, form) {
    e.preventDefault();
    if (confirm("Are you sure you want to choose these cards to start? You'll be able to upgrade your cards and earn new ones later on.")) {
        submitIntroForm(e, form);
    }
    else {
        stopLoadingButton();
    }
}

function checkIfUsernameIsAvailable(username) {
    if (!username) {
        $("#availableUsernameText").text("");
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Intro/IsUsernameAvailable",
        data: { username: username },
        success: function (data) {
            $("#availableUsernameText").text("");
            if (data.success) {
                $("#availableUsernameText").append("<span class='win-green'><b><span class='fas fa-check-circle'></span> " + username + "</b> is available!</span>");
            }
            else {
                $("#availableUsernameText").append("<span class='lose-red'><b><span class='fas fa-times-circle'></span> " + username + "</b> isn't available</span>");
            }
        }
    });
}

function checkIfEmailIsAvailable(emailAddress) {
    if (!emailAddress) {
        $("#availableEmailText").text("");
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Intro/IsEmailAvailable",
        data: { emailAddress: emailAddress },
        success: function (data) {
            $("#availableEmailText").text("");
            if (data.success) {
                $("#availableEmailText").append("<span class='win-green'><b><span class='fas fa-check-circle'></span> " + emailAddress + "</b> is available!</span>");
            }
            else {
                $("#availableEmailText").append("<span class='lose-red'><b><span class='fas fa-times-circle'></span> " + emailAddress + "</b> is not available.</span>");
            }
        }
    });
}

function checkIfAllSignUpFormsAreFilled() {
    if ($('#username').val() != '' && $('#emailAddress').val() != '' && $('#password').val() != '') {
        $('#sign-up-button').removeClass('disabled');
    }
    else {
        $('#sign-up-button').addClass('disabled');
    }
}

function submitIntroForm(e, form, scrollToTop = true) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success) {
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, scrollToTop);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                    $('#take-currency-intro').hide();
                    $('#start-exploring-intro').show();
                }
                else {
                    refreshIntroPartialView(scrollToTop);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshIntroPartialView(scrollToTop);
                }
            }
        }
    });

    e.preventDefault();
}


function refreshIntroPartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Intro/GetIntroPartialView",
        success: function(data)
        {
            if (data.returnUrl) {
                window.location.href = data.returnUrl;
                return;
            }
            $("#introPartialView").html(data);
            onPageLoad(scrollToTop);
            onIntroLoad();
            starterCardsSelected = 0;
            totalStarterCardBudget = 0;
        }
    });
}

