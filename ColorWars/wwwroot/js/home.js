﻿var homeFeedContainers = [];
var usernameCheckTimer = null;
var feedBarScroll = 0;
var lastFeedBarScroll = 0;
var feedBarTopMargin = $('#side-feed-bar').length ? $('#side-feed-bar').offset().top : 0;

$(document).ready(function(){
    onHomeLoad();
});

$(document.body).click(function () {
    closeFeedContextMenu();
});

$(window).on('resize', function () {
    closeFeedContextMenu();
    updateSideFeedMenu();
});

$(window).on('scroll', function () {
    updateSideFeedMenu();
});

function onHomeLoad() {
    if ($('.stackable-card').length) {
        var slideTime = 0;
        $('.stackable-card').each(function () {
            var stackableCard = $(this);
            setTimeout(function () {
                $(stackableCard).show("slide", { direction: "left" }, 250);
            }, slideTime);
            slideTime += 200;
        });
        setTimeout(function () {
            $('#slogan').fadeIn(1000);
        }, 500);
        setTimeout(function () {
            $('#sign-up').slideDown(500);
        }, 1000);
        setTimeout(function () {
            $('#sign-up-button').fadeIn(500);
        }, 1500);
    }
    else if ($('.home-feed').length) {
        if ($('#modal-tutorial').length) {
            loadTutorialModal();
        }

        $('.home-feed').each(function () {
            homeFeedContainers.push(this);
        });

        if (homeFeedContainers.length <= 40) {
            return;
        }

        homeFeedContainers.splice(0, 40);

        for (var i = 0; i < homeFeedContainers.length; i++) {
            var feed = $(homeFeedContainers[i]);
            feed.hide();
        }

        $(window).scroll(function (event) {
            if (homeFeedContainers.length == 0) {
                $(this).off(event);
                return;
            }

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
                var amount = Math.min(20, homeFeedContainers.length);
                for (var i = 0; i < amount; i++) {
                    var feed = $(homeFeedContainers[i]);
                    feed.show();
                }
                homeFeedContainers.splice(0, amount);
            }
        });
    }
}

function updateSideFeedMenu() {
    if (!$('#side-feed-bar').length) {
        return;
    }

    if ($('#profile-panel').is(":visible")) {
        $('#feed').css('min-height', $('#side-feed-bar').height());
    }
    else {
        $('#feed').css('min-height', 0);
    }

    if (!$('#profile-panel').is(":visible") || $('#side-feed-bar')[0].scrollHeight + feedBarTopMargin < window.innerHeight) {
        $('#side-feed-bar')[0].style.marginTop = 0;
        return;
    }

    var delta = window.scrollY - lastFeedBarScroll;
    feedBarScroll += delta;
    lastFeedBarScroll = window.scrollY;

    if (feedBarScroll < 0) {
        feedBarScroll = 0;
    } else if (feedBarScroll > $('#side-feed-bar')[0].scrollHeight - window.innerHeight + feedBarTopMargin * 2) {
        feedBarScroll = $('#side-feed-bar')[0].scrollHeight - window.innerHeight + feedBarTopMargin * 2;
    }

    if (window.scrollY < feedBarTopMargin/2) {
        lastFeedBarScroll = 0;
        feedBarScroll = 0;
    }

    $('#side-feed-bar')[0].style.marginTop = -feedBarScroll + 'px';
}

function showAllChecklist() {
    $('#modal-checklist').modal();
}

function showLoginBonusModal() {
    $('#daily-bonus').modal();
    if ($('#showExpiredMessage').val() == "true") {
        showSnackbarWarning("Your previous daily bonus streak expired!");
    }
}

function showFeedContextMenu(id, offset) {
    var isMobile = !$('#profile-panel').is(":visible");
    var isOpened = $('#feed-menu-' + id).is(":visible");
    closeFeedContextMenu();
    if (!isOpened) {
        $('#feed-menu-' + id).css({ top: offset.top - $('#feed-menu-' + id).height() - (isMobile ? 150 : 35), left: offset.left - (isMobile ? 150 : $('#feed-menu-' + id).width() * 2 + 60) + $('#side-feed-bar').width(), position: 'absolute' });
        $('#feed-menu-' + id).fadeIn(250);
        $('#feed-ellipsis-' + id).addClass('primary-color');
    }
}

function closeFeedContextMenu() {
    $('.feed-menu').fadeOut(250);
    $('.feed-ellipsis').removeClass('primary-color');
}

function hideFromFeed(category, friendlyCategory) {
    if (confirm("Are you sure you want to hide all " + unescape(friendlyCategory) + " posts from your feed? You can always add them back from your Preferences.")) {
        $.ajax({
            type: "POST",
            url: "/Home/HideFeedCategory",
            data: { category : category },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    showToastIfExists();
                    $('.' + category + '-category').hide();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }

    closeFeedContextMenu();
}

function loadTutorialModal() {
    $('#modal-tutorial').modal();
}

function highlightChecklist(scrollToChecklist = true) {
    $('#checklist').removeClass('animated bounce');
    $('#checklist-body').removeClass('fade-out-highlight');
    if (scrollToChecklist) {
        $('html, body').animate({
            scrollTop: $('#checklist').offset().top - 100
        }, 50, function () {
            $('#checklist').addClass('animated bounce');
            $('#checklist-body').addClass('fade-out-highlight');
        });
    }
    else {
        $('#checklist').addClass('animated bounce');
        $('#checklist-body').addClass('fade-out-highlight');
    }
}

function enableEmailNotifications(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                showSnackbarSuccess('Your preferences were successfully updated!');

                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
}

function enableSoundEffects(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                showSnackbarSuccess('Your preferences were successfully updated!');

                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');

                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
}

function sendReferralEmail(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                $('.modal.in').modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                window.location.href = '/Referrals';
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    e.preventDefault();
};

function redeemDailyBonus(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages();
            if (data.success) {
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }

                if ($('#currentDay').length) {
                    $('#redeem-form-' + $('#currentDay').val()).fadeOut(250, function () {
                        $('.hidden-reward').fadeIn();
                        initializeNumberCounter("", function () {
                            $('.hidden-reward').addClass('animated tada');
                            $('#prize-' + $('#currentDay').val()).removeClass('fadeInUp').addClass('tada');
                            if (parseInt($('#currentDay').val()) == 7) {
                                $('#modal-top-text').html('<span class="glyphicon glyphicon-star win-green"></span> Congratulations! You earned the top prize!');
                                $('#modal-top-text').removeClass('fadeInDown').addClass('tada');
                            }
                        });
                        $('#redeem-grid-' + $('#currentDay').val()).removeClass('light-blue-border-important').addClass('win-green-border small-thick-border');
                    });
                }

                $('#' + $('#dailyBonusFeedId').val()).fadeOut();
                showToastIfExists();
            }
            else {
                showSnackbarWarning(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};
