﻿var hideMessages = true;
var originalBtn = null;
var originalBtnTitle = "";
var preventBtnLoad = false;
var unreadMessagesCount = 0;
var unreadNotificationsCount = 0;
var sentCourierMessage = false;
var updateNavTimer = 0;
var resendNavTimer  = 0;
var courierTimer = 0;

var sdkInstance = "appInsightsSDK"; window[sdkInstance] = "appInsights"; var aiName = window[sdkInstance], aisdk = window[aiName] || function (e) { function n(e) { t[e] = function () { var n = arguments; t.queue.push(function () { t[e].apply(t, n) }) } } var t = { config: e }; t.initialize = !0; var i = document, a = window; setTimeout(function () { var n = i.createElement("script"); n.src = e.url || "https://az416426.vo.msecnd.net/scripts/b/ai.2.min.js", i.getElementsByTagName("script")[0].parentNode.appendChild(n) }); try { t.cookie = i.cookie } catch (e) { } t.queue = [], t.version = 2; for (var r = ["Event", "PageView", "Exception", "Trace", "DependencyData", "Metric", "PageViewPerformance"]; r.length;)n("track" + r.pop()); n("startTrackPage"), n("stopTrackPage"); var s = "Track" + r[0]; if (n("start" + s), n("stop" + s), n("setAuthenticatedUserContext"), n("clearAuthenticatedUserContext"), n("flush"), !(!0 === e.disableExceptionTracking || e.extensionConfig && e.extensionConfig.ApplicationInsightsAnalytics && !0 === e.extensionConfig.ApplicationInsightsAnalytics.disableExceptionTracking)) { n("_" + (r = "onerror")); var o = a[r]; a[r] = function (e, n, i, a, s) { var c = o && o(e, n, i, a, s); return !0 !== c && t["_" + r]({ message: e, url: n, lineNumber: i, columnNumber: a, error: s }), c }, e.autoExceptionInstrumented = !0 } return t }(
    {
        instrumentationKey: "fcc434e5-27e5-451b-8bf3-d28d4e7d6c4e"
    }
); window[aiName] = aisdk, aisdk.queue && 0 === aisdk.queue.length && aisdk.trackPageView({});

$(document).ready(function(){
    onPageLoad();
});

$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        stopLoadingButton();
    }
});

function onPageLoad(scrollToTop = true, scrollToHash = true, updatePushDown = true){
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    if (window.canRunAds === undefined) {
        $('.ad-feed').hide();
        $('.ad-feed').removeClass('stadium-message');
        $('.ad-feed').removeClass('home-feed');
        $('.enable-ad').show();
        $('.enable-ad').addClass('stadium-message');
        $('.enable-ad').addClass('home-feed');
    }
    stopLoadingButton();
    $('[data-toggle="tooltip"]').tooltip();
    $('[rel="tooltip"]').on('click', function () {
        $(this).tooltip('hide')
    })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        pushDownScrollClass();
    });
    if (window.location.hash) {
        var selection = $(window.location.hash)
        if (selection && !selection.hasClass('itemSelected') && !selection.hasClass('fade-out-border') && (selection.hasClass('grid-element') || selection.hasClass('scrolling-grid-element') || selection.hasClass('gray-container'))) {
            if (selection.hasClass('gray-container') && !selection.hasClass('gray-border')) {
                selection.addClass('gray-border');
            }
            selection.addClass('fade-out-border-light-blue');
            if (selection.parent()) {
                selection.parent().scrollLeft(selection.offset().left - selection.width() - 50);
            }
            setTimeout(function () {
                pushDownScrollClass();
                selection.removeClass('fade-out-border-light-blue');
            }, 5000);
        }
        else if (selection && selection.hasClass('form-group')) {
            selection.addClass('fade-out-highlight');
        }
        if (scrollToHash && $(window.location.hash).length) {
            $(window).scrollTop($(window.location.hash).offset().top - 50);
        }
        history.replaceState('', '', window.location.pathname);
    }
    $('[id^="colored-by-username"]').each(function (i, el) {
        var setColor = $(el).attr('data-user-color');
        var color = setColor ? setColor : stringToColor($(el).attr('value'));
        $(el).css("background-color", color);
        if ($('#user-color').length) {
            $('#user-color').val(color);
        }
    });
    $("[data-hide]").on("click", function(){
        $(this).closest("." + $(this).attr("data-hide")).hide();
    });
    $('select').selectpicker({
        style: 'btn-default',
        size: false
    });
    $(document).click(function (event) {
        var clickover = $(event.target);
        var opened = $(".navbar-collapse").hasClass("navbar-collapse collapse in");
        if (opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });
    $(".btn").unbind("click");
    $(".btn").off().on("click", function(e){
        if (preventBtnLoad){
            preventBtnLoad = false;
            return;
        }
        var form = $(this).closest('form');
        var isButton = $(this).is('button');
        if ($(this).hasClass('no-load-btn')){
            return;
        }
        if (isButton && $(this).hasClass('dropdown-toggle')) {
            return;
        }
        if (isButton && !(form && form[0])){
            return;
        }
        if (form && isButton && !form[0].checkValidity()){
            return;
        }
        originalBtn = $(this);
        originalBtnTitle = $(this).html();
        $(this).html("<span class='glyphicon glyphicon-repeat normal-right-spinner'></span>");
        $(this).prop("disabled", true);
        if (form && isButton){
            e.preventDefault();
            form.submit();
        }
    });
    $('.dropdown').on('hidden.bs.dropdown', function(e) {
        $(this).find('.caret').toggleClass('rotate-180');
    });
    $('.dropdown').on('shown.bs.dropdown', function(e) {
        $(this).find('.caret').toggleClass('rotate-180');
    });
    if ($('.progress-bar').length && $('.progress-bar').attr('aria-valuenow') != undefined) {
        setTimeout(function () {
            $('.progress-bar').width($('.progress-bar').attr('aria-valuenow') + "%");
        }, 500);
    }
    if ($('.scrolling-container').length) {
        $(window).on('resize', function () {
            showHideScrollingContainerIndicator();
        });
        $(".scrolling-container").scroll(function () {
            showHideScrollingContainerIndicator();
        });
        new ResizeSensor($('.scrolling-container'), function () {
            showHideScrollingContainerIndicator();
        });
        showHideScrollingContainerIndicator();
    }
    if (updatePushDown) {
        pushDownScrollClass();
    }
    initializeNumberCounter();
    showToastIfExists();
    var usernameCookie = getCookie("Username");
    var isAuthenticated = usernameCookie != null;
    if (isAuthenticated) {
        getUnreadMessages();
        clearTimeout(courierTimer);
        courierTimer = setTimeout(function(){
            sendCourierMessage();
        }, 2500);
    }
}

function initializeNumberCounter(currency = "", fn = null) {
    if ($('.number-counter').length) {
        $('.number-counter').each(function () {
            var countTo = $(this).attr('data-count');
            var countStart = $(this)[0].hasAttribute('data-count-start') ? $(this).attr('data-count-start') : 0;
            var delay = $(this)[0].hasAttribute('data-count-delay') ? parseInt($(this).attr('data-count-delay')) : 0;
            var count = $(this);
            setTimeout(function () {
                countToNumber($(count), countTo, countStart, currency, fn);
            }, delay);
        });
    }
}

function formatNumber(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatNumberShortened(number) {
     if (number >= 1000000) {
        return (number / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
     }
     if (number >= 1000) {
        return (number / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
     }
     return number;
}

function playSound(soundUrl) {
    new Audio(soundUrl).play();
}

function stopLoadingButton() {
    if (originalBtn){
        originalBtn.prop("disabled", false).html(originalBtnTitle);
    }
}

function showHideScrollingContainerIndicator() {
    if ($('.scrolling-wrapper').length) {
        $('.scrolling-wrapper').each(function () {
            var scrollingContainer = $(this).find(".scrolling-container");
            var scrollingContainerIndicatorLeft = $(this).find(".scrolling-container-indicator-left");
            var scrollingContainerIndicatorRight = $(this).find(".scrolling-container-indicator-right");
            if ($(scrollingContainer)[0].scrollLeft > 0) {
                $(scrollingContainerIndicatorLeft).fadeIn();
                $(this).removeClass('hidden-left');
            }
            else {
                $(scrollingContainerIndicatorLeft).hide();
                $(this).addClass('hidden-left');
            }
            if (scrollingContainer[0].scrollWidth - ($(scrollingContainer)[0].scrollLeft + $(scrollingContainer).innerWidth()) >= 10) {
                $(scrollingContainerIndicatorRight).fadeIn();
                $(this).removeClass('hidden-right');
            }
            else {
                $(scrollingContainerIndicatorRight).hide();
                $(this).addClass('hidden-right');
            }
        });
    }
}

function scrollContainerToLeft(container) {
    var scrollingContainer = $(container).find(".scrolling-container");
    $(scrollingContainer).animate({
        scrollLeft: Math.max(0, $(scrollingContainer)[0].scrollLeft - $(scrollingContainer).innerWidth())
    }, 500);
}

function scrollContainerToRight(container) {
    var scrollingContainer = $(container).find(".scrolling-container");
    $(scrollingContainer).animate({
        scrollLeft: Math.min(scrollingContainer[0].scrollWidth, $(scrollingContainer)[0].scrollLeft + $(scrollingContainer).innerWidth())
    }, 500);
}

function pushDownScrollClass() {
    if ($('.scrolling-wrapper').length) {
        $('.scrolling-wrapper').each(function () {
            var pushDownClass = $(this).find(".push-down-class");
            if ($(pushDownClass).length) {
                var bottomOffset = 0;
                $(pushDownClass).each(function () {
                    bottomOffset = Math.max(bottomOffset, $(this).offset().top + $(this).height());
                });
                $(pushDownClass).each(function () {
                    if ($(this).offset().top + $(this).height() < bottomOffset) {
                        $(this).css("padding-top", bottomOffset - ($(this).offset().top + $(this).height()));
                    }
                });
            }
        });
    }
}

function scrollToId(id) {
    $('html, body').animate({
        scrollTop: $('#' + id).offset().top - 10
    }, 500);
}

function resizeTextarea(textarea) {
    textarea.style.height = 'auto';
    textarea.style.height = textarea.scrollHeight + 'px';
}

function countToNumber(el, countTo, countNum = 0, currency = "", callback = null, useShortenedFormat = false){
    el.text(countNum);
    $({countNum: countNum == 0 ? el.text() : countNum}).animate({
            countNum: countTo
        },
        {
            duration: Math.min(Math.abs(countTo - countNum) * 50, 1250),
            easing: 'linear',
            step: function() {
                el.text(useShortenedFormat ? formatNumberShortened(Math.floor(this.countNum)) : formatNumber(Math.floor(this.countNum)) + currency);
            },
            complete: function() {
                el.text(useShortenedFormat ? formatNumberShortened(Math.floor(this.countNum)) : formatNumber(Math.floor(this.countNum)) + currency);
                if (callback) {
                    callback();
                }
            }
        }
    );
}

function updateNavIfUnreadNotifications() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Notifications/GetUnreadCount",
        success: function(data)
        {
            if (data.success) {
                var unreadNotificationsCount = data.object.notifications;
                var unreadMessagesCount = data.object.messages;
                if (unreadNotificationsCount > 0){
                    $('#unreadNotificationsCount').text(formatNumberShortened(unreadNotificationsCount));
                    $('.notifications').addClass('alert-color');
                }
                else {
                    $('#unreadNotificationsCount').text("");
                    $('.notifications').removeClass('alert-color');
                }
                if (unreadMessagesCount > 0) {
                    $('.unreadMessagesCount').text(formatNumberShortened(unreadMessagesCount));
                    $('.messages').addClass('alert-color');
                }
                else {
                    $('.unreadMessagesCount').text("");
                    $('.messages').removeClass('alert-color');
                }
                updateMenu(unreadNotificationsCount + unreadMessagesCount);
            }
        }
    });
}

function updateMenu(amount) {
    var documentTitle = document.title.split('(')[0].trim();
    if (amount > 0) {
        $('#navbar-toggle').addClass('alert-background');
        $('#navbar-toggle').html('<b><span class="glyphicon glyphicon-bell"></span> ' + formatNumberShortened(amount) + '</b>');
        $('#userDropdownIcon').hide();
        $('#userDropdownUnreadCount').text(formatNumberShortened(amount));
        document.title = documentTitle + " (" + formatNumberShortened(amount) + ")";
    }
    else {
        $('#navbar-toggle').removeClass('alert-background');
        $('#navbar-toggle').html('<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>');
        $('#userDropdownIcon').show();
        $('#userDropdownUnreadCount').text("");
        document.title = documentTitle;
    }
}

function getUnreadMessages(tries = 1) {
    clearTimeout(updateNavTimer);
    clearTimeout(resendNavTimer);
    if (tries > 10) {
        return;
    }
    updateNavTimer = setTimeout(function(){
        updateNavIfUnreadNotifications();
        resendNavTimer = setTimeout(function () {
            getUnreadMessages(tries + 1);
        }, (tries + 1) * 10000);
    }, 250);
}

function showToastIfExists() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetToast",
        success: function(data)
        {
            if (data.success){
                for (var i = 0; i < data.messages.length; i++) {
                    if (data.messages[i][3] == "True") {
                        if (data.messages[i][4] == "True") {
                            showSnackbarWarning(data.messages[i][1]);
                        }
                        else {
                            showSnackbarSuccess(data.messages[i][1]);
                        }
                    }
                    else {
                        showToastr(data.messages[i][0], data.messages[i][1], data.messages[i][2]);
                    }
                }
            }
        }
    });
}

function sendCourierMessage() {
    if (sentCourierMessage) {
        return;
    }
    $.ajax({
        type: "Post",
        cache: false,
        url: "/Messages/SendCourierMessage",
        data: { "__RequestVerificationToken" : $('input[name=__RequestVerificationToken]').val() }
    });
    sentCourierMessage = true;
}

function showToastr(title, message, url){
    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": true,
      "showDuration": "500"
    }
    if (url){
        toastr.options.onclick = function() { window.location.href = url; }
    }
    toastr.success(message, title);
}

function showSnackbarSuccess(text) {
    Snackbar.show({ text: text, backgroundColor: '#dff0d8', textColor: '#3c763d', actionTextColor: '#3c763d', pos: 'bottom-center' });
}

function showSnackbarInfo(text) {
    Snackbar.show({ text: text, backgroundColor: '#D9EDF7', textColor: '#0C5875', actionTextColor: '#0C5875', pos: 'bottom-center' });
}

function showSnackbarWarning(text) {
    Snackbar.show({ text: text, backgroundColor: '#fff3cd', textColor: '#7d6728', actionTextColor: '#7d6728', pos: 'bottom-center' });
}

function copyText(text){
    Clipboard.copy(text);
    showSnackbarSuccess('The link was successfully copied!');
}

function hideAllMessages() {
    $("#alertMessage").hide();
    $("#successMessage").hide();
    $("#infoMessage").hide();
}

function showErrorMessage(errorMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#alertMessage").fadeIn(fadeIn ? 250 : 0);
    $("#alertMessageText").text(errorMessage);
    if (buttonText) {
        $("#warningMessageButton").show();
        $("#warningMessageButtonText").text(buttonText)
        $("#warningMessageButtonText").attr("href", buttonUrl);
    }
}

function showSuccessMessage(successMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
        $("#successMessage").fadeIn(fadeIn ? 250 : 0);
        $("#successMessageText").text(successMessage);
        if (buttonText) {
            $("#successMessageButton").show();
            $("#successMessageButtonText").text(buttonText)
            $("#successMessageButtonText").attr("href", buttonUrl);
        }
    }
    else {
        showSnackbarSuccess(successMessage);
    }
}

function showInfoMessage(infoMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#infoMessage").fadeIn(fadeIn ? 250 : 0);
    $("#infoMessageText").text(infoMessage);
    if (buttonText) {
        $("#infoMessageButton").show();
        $("#infoMessageButtonText").text(buttonText)
        $("#infoMessageButtonText").attr("href", buttonUrl);
    }
}

function showDangerMessage(dangerMessage, scrollToTop = true, fadeIn = true, buttonText = "", buttonUrl = ""){
    stopLoadingButton();
    if (scrollToTop) {
        window.scrollTo(0, 0);
    }
    $("#dangerMessage").fadeIn(fadeIn ? 250 : 0);
    $("#dangerMessageText").text(dangerMessage);
    if (buttonText) {
        $("#dangerMessageButton").show();
        $("#dangerMessageButtonText").text(buttonText)
        $("#dangerMessageButtonText").attr("href", buttonUrl);
    }
}

function updatePoints(points) {
    var mobilePoints = $('#totalPoints-mobile');
    var totalPoints = $('#totalPoints');
    var updatedPoints = getIntFromPoints(points);
    var currentPoints = getIntFromPoints(totalPoints.text());
    var currency =  totalPoints.text().split(' ')[1];

    countToNumber(mobilePoints, updatedPoints, currentPoints, " " + currency);
    countToNumber(totalPoints, updatedPoints, currentPoints, " " + currency);
}

function getIntFromPoints(points) {
    var pointsWithoutCurrency = points.split(' ')[0];
    return parseInt(pointsWithoutCurrency.replace(/,/g, ""));
}

function stringToColor(str){
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var color = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return color;
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }

    return decodeURI(dc.substring(begin + prefix.length, end));
}

function goToPointsLocation() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Preferences/GetPointsLocation",
        success: function(data)
        {
            if (data.success){
                window.location = data.returnUrl;
            }
            else {
                window.location = "/Items";
            }
        }
    });
}

$.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ')
    });
    return this;
};

function toggleMessagesBox() {
    if ($('#messages-box').is(":hidden")) {
        $('#messages-box').slideDown();
        updateMessagesBox();
    }
    else {
        $('#messages-box').slideUp();
    }
}

function updateMessagesBox(username, showSpinner) {
    if (showSpinner) {
        $("#messages-box").html('<div class="extra-large-padding-top"></div><div class="loader center-block"></div>');
    }
    $.ajax({
        type: "GET",
        url: "/Messages/GetMessagesBoxPartialView",
        data: { username: username },
        success: function (data) {
            $("#messages-box").html(data);
            onPageLoad(false, false, false);
            onMessagesLoad(false);
            pollForNewMessages(true);
        }
    });
}
