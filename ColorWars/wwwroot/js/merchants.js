﻿function updateMerchantsForm(e, form, isContest) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (isContest) {
                    refreshMerchantsPartialView();
                }
                else {
                    refreshMerchantsJewelPartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshMerchantsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Merchants/GetMerchantsPartialView",
        success: function(data)
        {
            $("#merchantsPartialView").html(data);
            onPageLoad(false);
        }
    });
}

function refreshMerchantsJewelPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Merchants/GetMerchantsJewelPartialView",
        success: function(data)
        {
            $("#merchantsJewelPartialView").html(data);
            onPageLoad(false);
        }
    });
}