﻿function buySponsorCoins(e, key, siteName, logoUrl, email, amount, coinAmount) {
    var handler = StripeCheckout.configure({
        key: key,
        image: logoUrl,
        email: email,
        currency: 'usd',
        token: function (token) {
            $.ajax({
                type: "POST",
                url: "SponsorSociety/GetCoinsStripe",
                data: { token: token.id, email: email, amount: amount },
                success: function (data) {
                    hideAllMessages();
                    if (data.success) {
                        if (data.successMessage) {
                            showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                            if (data.soundUrl) {
                                playSound(data.soundUrl);
                            }
                            refreshSponsorSocietyPartialView(true);
                        }
                    }
                    else {
                        showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                    }
                }
            });
        }
    });

    handler.open({
        name: siteName + ' Sponsor',
        description: 'Buy ' + coinAmount + ' Coins',
        amount: amount
    });

    e.preventDefault();
}

function updateSponsorSociety(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            hideAllMessages()
            if (data.success) {
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshSponsorSocietyPartialView(false);
            }
            else {
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function refreshSponsorSocietyPartialView(isPurchase = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isPurchase : isPurchase },
        url: "/SponsorSociety/GetSponsorSocietyPartialView",
        success: function (data) {
            $("#sponsorSocietyPartialView").html(data);
            onPageLoad();
        }
    });
}