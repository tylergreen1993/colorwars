﻿$(document).ready(function(){
    onItemsLoad();
});

function onItemsLoad(){
    if ($('#modal-items-tutorial').length) {
        $('#modal-items-tutorial').modal();
    }
    if ($('#modal-relic-requirements').length) {
        $('#modal-relic-requirements').modal();
    }
}

function companionColorSelected(color) {
    var color = $(color);
    var colorId = color.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedColor').val() == colorId){
        $('#selectedColor').val("");
    }
    else{
        $('#selectedColor').val(colorId);
        color.addClass('itemSelected');
    }
}

function updateItems(e, form, openCardPack = false, loadPowerMoves = false, container = null) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                refreshItemsPartialView(openCardPack, loadPowerMoves, container);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }
        }
    });

    e.preventDefault();
};

function updateGuide(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshItemsGuidePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function highlightRecentItems() {
    $('html, body').animate({
        scrollTop: $('#items').offset().top - 50
    }, 500);
    $('.fade-out-border').each(function () {
        var recentItem = $(this);
        $(recentItem).removeClass('fade-out-border');
        setTimeout(function () {
            $(recentItem).addClass('fade-out-border');
        });
    });
}

function openCardPackItem() {
    if ($('#modal-items-open-card-pack').length) {
        $('#modal-items-open-card-pack').modal();
        var timer = 50;
        $('.item').each(function () {
            var item = $(this);
            setTimeout(function () {
                $(item).show("slide", { direction: "left" }, 250);
            }, timer)
            timer += 250;
        });
        setTimeout(function () {
            pushDownScrollClass();
        }, 2500);
    }
}

function refreshItemsPartialView(openCardPack, loadPowerMoves, container) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Items/GetItemsPartialView",
        data: { loadPowerMoves : loadPowerMoves },
        success: function(data)
        {
            var containerId = "";
            var lastPlacement = 0;
            if (container != null && $(container).length) {
                containerId = $(container).attr('id');
                lastPlacement = $(container).scrollLeft();
            }
            $("#itemsPartialView").html(data);
            onPageLoad(false);
            onItemsLoad();
            if (openCardPack) {
                openCardPackItem();
            }
            if (lastPlacement > 0) {
                $('#' + containerId).scrollLeft(lastPlacement);
            }
        }
    });
}

function refreshItemsGuidePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Items/GetItemsGuidePartialView",
        success: function(data)
        {
            $("#itemsGuidePartialView").html(data);
            onPageLoad();
        }
    });
}