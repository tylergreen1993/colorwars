﻿function updateVillager(e, form, isVillager) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshVillagerPartialView(isVillager);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshVillagerPartialView(isVillager) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Villager/GetVillagerPartialView",
        data: { isVillager: isVillager },
        success: function(data)
        {
            $("#villagerPartialView").html(data);
            onPageLoad();
            $('#cards-given').addClass('animated bounceIn');
        }
    });
}