﻿var notificationContainers = [];

$(document).ready(function(){
    onNotificationsLoad();
});

function onNotificationsLoad(){
    if ($('.notification-container').length) {
        $('.notification-container').each(function() {
            notificationContainers.push(this);
        });
       

        if (notificationContainers.length <= 40) {
            return;
        }

        notificationContainers.splice(0, 40);

        for (var i = 0; i < notificationContainers.length; i++) {
            var notification = $(notificationContainers[i]);
            notification.hide();
        }

        $(window).scroll(function(event) {
            if (notificationContainers.length == 0) {
                $(this).off(event);
                return;
            }

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
                var amount = Math.min(20, notificationContainers.length);
                for (var i = 0; i < amount; i++) {
                    var notification = $(notificationContainers[i]);
                    notification.show();
                }
                notificationContainers.splice(0, amount);
            }
        });
    }
}
