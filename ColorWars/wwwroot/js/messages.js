﻿var messagesTimer = null;

$(document).ready(function () {
    onMessagesLoad();
    pollForNewMessages(false);
});

function onMessagesLoad(shouldScroll = true) {
    if ($('#scrollContainer').length) {
        $('#scrollContainer').scrollTop($('#conversationContainer').height());
        if (shouldScroll) {
            var messageContainerOffset = $('#message-container').offset();
            if (messageContainerOffset) {
                $(window).scrollTop(messageContainerOffset.top - window.innerHeight + $('#message-container').height() * 2);
            }
        }
    }
}

function removeAllMessages(username){
    if (confirm("Are you sure you want to remove your messages with " + username + "? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/Messages/Remove",
            data: { username : username },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/Messages";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function delayedKeyUpUpdateMessages(e, form, scrollToTop = true) {
    clearTimeout(messagesTimer);
    messagesTimer = setTimeout(function () {
        updateMessages(e, form, scrollToTop);
    }, 250);
};

function submitMessagesForm(e, form, username, isMessageBox) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                $('#message').val("");
                refreshMessagesPartialView(username, isMessageBox, !data.successMessage);
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, false);
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);
                }
                if (data.points){
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, false, true, data.buttonText, data.buttonUrl);
                $('html, body').animate({
                    scrollTop: 0
                }, 0);
            }
        }
    });

    e.preventDefault();
};

function submitNewMessagesForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
            }
            else{
                $('#username').val('');
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                stopLoadingButton();
            }
        }
    });

    $('.modal.in').modal('hide');
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open'); 

    e.preventDefault();
};

function pollForNewMessages(isMessageBox){
    var correspondent = $('#messageCorrespondent').val();
    if (!correspondent){
        return;
    }
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetUnreadMessages",
        data: { correspondent : correspondent },
        success: function(data)
        {
            if (data.success && data.amount > 0) {
                correspondent = $('#messageCorrespondent').val();
                if (!correspondent) {
                    return;
                }
                refreshMessagesPartialView(correspondent, isMessageBox);
            }

            setTimeout(function(){
                pollForNewMessages();
            }, 1500);
        }
    });
}

function updateMessages(e, form, scrollToTop = true) {
    form = $(form);
    var url = "/Messages/GetMessagesPartialView";
    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        data: form.serialize(),
        success: function (data) {
            $("#messagesPartialView").html(data);
            onPageLoad(scrollToTop);
        }
    });

    e.preventDefault();
};

function refreshMessagesPartialView(username, isMessageBox, scrollToMessage) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Messages/GetMessagesPartialView",
        cache: false,
        data: { username: username, isMessageBox: isMessageBox },
        success: function(data)
        {
            $("#scrollContainer").html(data);
            onPageLoad(false);
            onMessagesLoad(scrollToMessage);
        }
    });
}