﻿function submitMuseumForm(e, form, wing) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (wing == 1) {
                    refreshMuseumEastWingPartialView();
                }
                else if (wing == 2) {
                    refreshMuseumWestWingPartialView();
                }
                else if (wing == 3) {
                    refreshMuseumSouthWingPartialView();
                }
                else if (wing == 4) {
                    refreshMuseumBoutiquePartialView();
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage, true, true, data.buttonText, data.buttonUrl);
            }

            $('.modal.in').modal('hide');
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open'); 
        }
    });

    e.preventDefault();
};

function refreshMuseumEastWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumEastWingPartialView",
        success: function(data)
        {
            $("#museumEastWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumWestWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumWestWingPartialView",
        success: function(data)
        {
            $("#museumWestWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumSouthWingPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumSouthWingPartialView",
        success: function(data)
        {
            $("#museumSouthWingPartialView").html(data);
            onPageLoad();
        }
    });
}

function refreshMuseumBoutiquePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Museum/GetMuseumBoutiquePartialView",
        success: function(data)
        {
            $("#museumBoutiquePartialView").html(data);
            onPageLoad();
        }
    });
}