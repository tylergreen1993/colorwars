﻿var pastWandSelections = [];

function wizardWandSelected(wand) {
    var wand = $(wand);
    var wandId = wand.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedWandColor').val() == wandId){
        $('#selectedWandColor').val("");
    }
    else{
        $('#selectedWandColor').val(wandId);
        wand.addClass('itemSelected');
        if ($('#secret-cavern').val() == "true") {
            pastWandSelections.push(wandId);
            if (pastWandSelections[0] != "Orange" || (pastWandSelections.length > 1 && pastWandSelections[1] != "Red") || (pastWandSelections.length > 2 && pastWandSelections[2] != "Blue")) {
                pastWandSelections = [];
            }
            else if (pastWandSelections[0] == "Orange" && pastWandSelections[1] == "Red" && pastWandSelections[2] == "Blue") {
                window.location.href = '/Wizard/SecretCavern';
            }
        }
    }
}

function updateWizard(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                var selectedWandColor = $('#selectedWandColor').val();
                refreshWizardPartialView(selectedWandColor);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshWizardPartialView(selectedWandColor) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Wizard/GetWizardPartialView",
        success: function(data)
        {
            $("#wizardPartialView").html(data);
            onPageLoad();
            $('#' + selectedWandColor).addClass('animated bounceIn');
        }
    });
}