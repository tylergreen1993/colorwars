﻿function updateAttentionHallPostCountMessage(){
    var attentionHallPostLength = $('#post').val().length;
    var remainingLength = 2500 - attentionHallPostLength;
    $('#attentionHallPostCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function showAttentionHallPostContainer(type) {
    $('#accomplishmentContainer').hide();
    $('#itemContainer').hide();
    $('#matchTagsContainer').hide();
    $('#generalContainer').hide();

    $('#selectedAccomplishment').val('');
    $('#selectedItem').val('');
    $('#selectedMatchTag').val('');
    $('#flair').val('');

    $('.accomplishmentSelected').removeClass('accomplishmentSelected');
    $('.itemSelected').removeClass('itemSelected');
    $('.cardSelected').removeClass('cardSelected');

    if (type == 'General') {
        $('#generalContainer').fadeIn(250);
    }
    else if (type == 'Accomplishment') {
        $('#accomplishmentContainer').fadeIn(250);
    }
    else if (type == 'Item') {
        $('#itemContainer').fadeIn(250);
    }
    else if (type == 'MatchTag') {
        $('#matchTagsContainer').fadeIn(250);
    }
}

function attentionHallAccomplishmentSelected(accomplishment) {
    var accomplishment = $(accomplishment);
    var accomplishmentId = accomplishment.attr("id");

    $('.accomplishmentSelected').removeClass('accomplishmentSelected');

    if ($('#selectedAccomplishment').val() == accomplishmentId){
        $('#selectedAccomplishment').val("");
    }
    else{
        $('#selectedAccomplishment').val(accomplishmentId);
        accomplishment.addClass('accomplishmentSelected');
    }
}

function attentionHallItemSelected(item) {
    var item = $(item);
    var itemId = item.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#selectedItem').val() == itemId){
        $('#selectedItem').val("");
    }
    else{
        $('#selectedItem').val(itemId);
        item.addClass('itemSelected');
    }
}

function attentionHallMatchTagSelected(matchTag) {
    var matchTag = $(matchTag);
    var matchTagId = matchTag.attr("id");

    $('.cardSelected').removeClass('cardSelected');

    if ($('#selectedMatchTag').val() == matchTagId){
        $('#selectedMatchTag').val("");
    }
    else{
        $('#selectedMatchTag').val(matchTagId);
        matchTag.addClass('cardSelected');
    }
}

function attentionHallVote(id, vote, page, isPost){
    $.ajax({
        type: "POST",
        url: "/AttentionHall/Vote",
        data: { id : id, vote: vote },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (isPost) {
                    refreshAttentionHallPostPartialView(id, false, false, "", false);
                }
                else {
                    refreshAttentionHallMainPartialView(page, id, false);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function removeAttentionHallPost(id){
    if (confirm("Are you sure you want to remove this post? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/AttentionHall/RemovePost",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/AttentionHall";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function removeAttentionHallComment(id, commentId){
    if (confirm("Are you sure you want to remove this comment? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/AttentionHall/RemoveComment",
            data: { 
                id : id,
                commentId : commentId,  
            },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    refreshAttentionHallPostPartialView(id, false, false, "", false);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function likeAttentionHallComment(id, commentId){
    $.ajax({
        type: "POST",
        url: "/AttentionHall/LikeComment",
        data: { 
            id : id,
            commentId : commentId,  
        },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshAttentionHallPostPartialView(id, false, true, commentId);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitAttentionHallCreateForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                window.location = "/AttentionHall?id=" + data.id;
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function submitAttentionHallCommentForm(e, form, id, isReply) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshAttentionHallPostPartialView(id, false, true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshAttentionHallMainPartialView(page, id, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { page : page },
        url: "/AttentionHall/GetAttentionHallMainPartialView",
        success: function(data)
        {
            $("#attentionHallPartialView").html(data);
            onPageLoad(scrollToTop);
            $('html, body').animate({
                scrollTop: $("#" + id).offset().top - 50
            }, 500);
        }
    });
}

function refreshAttentionHallPostPartialView(id, scrollToTop = true, showScroll = false, commentId = "", movePage = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { id : id },
        url: "/AttentionHall/GetAttentionHallPostPartialView",
        success: function(data)
        {
            $("#attentionHallPartialView").html(data);
            onPageLoad(scrollToTop);
            if (movePage) {
                $('html, body').animate({
                    scrollTop: $(commentId != "" ? "#" + commentId : ".last-comment").offset().top - (showScroll ? 50 : 100)
                }, showScroll ? 500 : 0);
            }
        }
    });
}
