﻿function updateDeleteInfoCountMessage() {
    var infoLength = $('#info').val().length;
    var remainingLength = 500 - infoLength;
    $('#deleteInfoCountMessage').text(remainingLength >= 0 ? formatNumber(remainingLength) + ' remaining' : 'Too long!');
}

function submitDeleteAccountForm(e, form) {
    if (confirm("Are you sure you want to permanently delete your account?")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    window.location.reload();
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function showDeleteAccountInfo() {
    $('#privacy').hide();
    $('#boring').hide();
    $('#confusing').hide();
    $('#distracting').hide();
    $('#other').hide();
    var selectedReason = $('#reason').val();
    if (selectedReason == "1") {
        $('#privacy').show();
    }
    else if (selectedReason == "2" || selectedReason == "4") {
        $('#boring').show();
    }
    else if (selectedReason == "3") {
        $('#confusing').show();
    }
    else if (selectedReason == "5") {
        $('#distracting').show();
    }
    else if (selectedReason == "0") {
        $('#other').show();
    }
}