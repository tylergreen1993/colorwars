﻿var offerCardId = "";

function revealOfferCard(position) {
    $.ajax({
        type: "POST",
        url: "/TakeOffer/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshOfferCardsPartialView(position);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitTakeOfferForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage){
                    showSuccessMessage(data.successMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshOfferCardsPartialView(-1)
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshOfferCardsPartialView(position){
    var prevOffer = $('#offer-amount').attr('data-count');
    $.ajax({
        type: "GET",
        cache: false,
        url: "/TakeOffer/GetTakeOfferPartialView",
        success: function(data)
        {
            $("#takeOfferPartialView").html(data);
            if (position >= 0) {
                deactivateAllOfferCards();
                offerCardId = '#offer-card-' + position;
                var countTo = $(offerCardId).attr('data-count');
                var currency = $(offerCardId).text().split(' ')[1];
                countToNumber($(offerCardId), countTo, 0, " " + currency, activateAllOfferCards);
                if ($('#offer-amount').length) {
                    countTo = $('#offer-amount').attr('data-count');
                    countToNumber($('#offer-amount'), countTo, prevOffer, " " + currency);
                }
            }
            onPageLoad(false);
        }
    });
}

function activateAllOfferCards() {
    $('.active-offer-card').each(function() {
        $(this).removeClass('faded');
    });
    $('.offer-card').each(function() {
        $(this).css('pointer-events', '');
    });
    $(offerCardId).addClass('animated tada');
    if ($('#offer-amount').length) {
        $('html, body').animate({
            scrollTop: $("#offer-amount").offset().top - 100
        }, 250);
        $('#offer-amount').removeClass('fade-out-highlight').addClass('fade-out-highlight');
    }
}
function deactivateAllOfferCards() {
    $('.active-offer-card').each(function() {
        $(this).addClass('faded');
    });
    $('.offer-card').each(function() {
        $(this).css('pointer-events', 'none');
    });
}