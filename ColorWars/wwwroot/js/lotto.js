﻿function updateLotto(e, form, isLotto) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                isLotto ? refreshLottoPartialView() : refreshLottoEggPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    isLotto ? refreshLottoPartialView() : refreshLottoEggPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshLottoPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Lotto/GetLottoPartialView",
        success: function(data)
        {
            $("#lottoPartialView").html(data);
            var countTo = $('#currentPot').attr('data-count');
            if (countTo > 50000) {
                var currency = $('#currentPot').text().split(' ')[1];
                countToNumber($('#currentPot'), countTo, countTo - 500, " " + currency);
            }
            else {
                $('#pyro-container').show();
            }
            $('#pot').addClass('animated tada');
            onPageLoad();
        }
    });
}

function refreshLottoEggPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Lotto/GetEggPartialView",
        success: function(data)
        {
            $("#lottoEggPartialView").html(data);
            onPageLoad();
        }
    });
}