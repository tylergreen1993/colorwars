﻿$(document).ready(function() {
    onPrivacyLoad();
});

function onPrivacyLoad() {
    var sidePolicyNav = $('#sidePolicyNav');
    if (sidePolicyNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sidePolicyNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sidePolicyNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#policyScrollSpy',
            offset: 40
        });
    }
}