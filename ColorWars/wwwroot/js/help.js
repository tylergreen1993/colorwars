﻿$(document).ready(function() {
    var sideHelpNav = $('#sideHelpNav');
    if (sideHelpNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sideHelpNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sideHelpNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#helpScrollSpy',
            offset: 40
        });
    }
});