﻿var cardItemsSelected = 0;

function doubtItCardSelected(card) {
    var card = $(card);
    var cardId = card.attr("id");
    
    if ($('#card1').val() == cardId || $('#card2').val() == cardId || $('#card3').val() == cardId || $('#card4').val() == cardId) {
        if ($('#card1').val() == cardId) {
            $('#card1').val("");
        }
        else if ($('#card2').val() == cardId) {
            $('#card2').val("");
        }
        else if ($('#card3').val() == cardId) {
            $('#card3').val("");
        }
        else if ($('#card4').val() == cardId) {
            $('#card4').val("");
        }
        card.removeClass('itemSelected');
        cardItemsSelected--;
    }
    else if ($('#card1').val() == "" || $('#card2').val() == "" || $('#card3').val() == "" || $('#card4').val() == "") {
        if ($('#card1').val() == "") {
            $('#card1').val(cardId);
        }
        else if ($('#card2').val() == "") {
            $('#card2').val(cardId);
        }
        else if ($('#card3').val() == "") {
            $('#card3').val(cardId);
        }
        else if ($('#card4').val() == "") {
            $('#card4').val(cardId);
        }
        card.addClass('itemSelected');
        cardItemsSelected++;
    }
    else {
        showSnackbarWarning("You can only select up to 4 cards. Unselect a card to play this one.");
    }
    if (cardItemsSelected == 0) {
        $('#play-cards-button').addClass('disabled');
    }
    else {
        $('#play-cards-button').removeClass('disabled');
    }
}

function restartDoubtItGame() {
    if (confirm("Do you want to flip the table and forfeit your Doubt It game?")) {
        $.ajax({
            type: "POST",
            url: "/DoubtIt/NewGame",
            data: { },
            success: function (data) {
                hideAllMessages();
                if (data.success) {
                    refreshDoubtItPartialView(); 
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function submitDoubtItForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, data.scrollToTop, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshDoubtItPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
}

function refreshDoubtItPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/DoubtIt/GetDoubtItPartialView",
        success: function(data)
        {
            $("#doubtItPartialView").html(data);
            onPageLoad(false);
            cardItemsSelected = 0;
            $('html, body').animate({
                scrollTop: $('#doubt-message').offset().top - 100
            }, 500);
        }
    });
}