﻿$(document).ready(function(){
    onProfileLoad();
});

function onProfileLoad() {
    if ($('#modal-profile-accomplishments-tutorial').length){
        $('#modal-profile-accomplishments-tutorial').modal();
    }
    if ($('#companions-container').length) {
        $("#companions-container").sortable({
            items: ".scrolling-grid-element",
            handle: ".draggable-bar",
            axis: "x",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var companionId = el.item.attr("id").split('companion-')[1];
                var url = "/Profile/UpdateCompanionPosition";
                $('.scrolling-companion-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, companionId: companionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshProfilePartialView("", false, false);
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.scrolling-companion-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    if ($('#showroom-items').length) {
        $("#showroom-items").sortable({
            items: ".grid-element",
            handle: ".draggable-bar",
            tolerance: "pointer",
            stop: function (e, el) {
                var newPosition = el.item.index();
                var selectionId = el.item.attr("id");
                var url = "/Profile/UpdatePosition";
                $('.grid-element').addClass('faded');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { newPosition: newPosition, selectionId: selectionId },
                    success: function (data) {
                        hideAllMessages();
                        if (data.success) {
                            refreshProfileShowroomPartialView();
                        }
                        else {
                            if (data.errorMessage) {
                                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                            }
                            $('.grid-element').removeClass('faded');
                        }
                    }
                });
            }
        });
    }
    $(".draggable-bar").disableSelection();
}

function companionInteract(username, companionId) {
    var url = '/Profile/CompanionInteract';
    $.ajax({
        type: "POST",
        url: url,
        data: { username : username, companionId : companionId },
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, false);
                }
                else if (data.infoMessage) {
                    showSnackbarInfo(data.infoMessage, false);
                }
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.shouldRefresh) {
                    refreshProfilePartialView(username, false, false);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function showAllBadges() {
    $('#modal-all-badges').modal();
}

function submitRemoveProfileForm(e, form, username) {
    if (confirm("Are you sure you want to send this Companion away to be adopted? This cannot be undone.")) {
        form = $(form);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                hideAllMessages()
                if (data.success){
                    if (data.successMessage) {
                        showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    }
                    refreshProfilePartialView(username);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function submitEndPartnershipForm(e, form, username) {
    if (confirm("Are you sure you want to end this partnership?")) {
        submitProfileForm(e, form, true, username);
    }
    else {
        stopLoadingButton();
    }

    e.preventDefault();
};

function likeShowroom(showroomUsername){
    $.ajax({
        type: "POST",
        url: "/Profile/LikeShowroom",
        data: { showroomUsername : showroomUsername },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshProfileShowroomPartialView(showroomUsername);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitProfileForm(e, form, isProfile, username = '', shouldRefresh = true, launchPartnershipModal = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages()
            if (data.success){
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, isProfile);
                    if (data.points) {
                        updatePoints(data.points);
                    }
                    if (data.soundUrl) {
                        playSound(data.soundUrl);
                    }
                }
                if (shouldRefresh) {
                    if (isProfile) {
                        refreshProfilePartialView(username, launchPartnershipModal);
                        $('.modal.in').modal('hide');
                        $('.modal-backdrop').remove();
                        $('body').removeClass('modal-open'); 
                    }
                    else {
                        refreshProfileShowroomPartialView(username);
                    }
                }
            }
            else{
                if (data.hideMessage) {
                    stopLoadingButton();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
            if (data.shouldRefresh) {
               if (isProfile) {
                    refreshProfilePartialView(username, launchPartnershipModal);
                    $('.modal.in').modal('hide');
                    $('.modal-backdrop').remove();
                    $('body').removeClass('modal-open'); 
                }
                else {
                    refreshProfileShowroomPartialView(username);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshProfilePartialView(username, launchPartnershipModal = false, scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { username : username },
        url: "/Profile/GetProfilePartialView",
        success: function(data)
        {
            $("#profilePartialView").html(data);
            onPageLoad(scrollToTop, false);
            onProfileLoad();
            if (launchPartnershipModal) {
                $('#modal-partnership-requests').modal();
            }
        }
    });
}

function refreshProfileShowroomPartialView(showroomUsername = "") {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isCollapsed : $('#collapseItems').hasClass('collapsed'), showroomUsername : showroomUsername },
        url: "/Profile/GetProfileShowroomPartialView",
        success: function(data)
        {
            $("#profileShowroomPartialView").html(data);
            onPageLoad(false);
            onProfileLoad();
        }
    });
}