﻿function updateSeeker(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshSeekerPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshSeekerPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshSeekerPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Seeker/GetSeekerPartialView",
        success: function(data)
        {
            $("#seekerPartialView").html(data);
            onPageLoad();
        }
    });
}