﻿var secretPassageTimer = 0;
var libraryKnowledgeType = "";

$(document).ready(function(){
    onSecretPassageLoad();
});

function onSecretPassageLoad() {
    loadVoidMessage();
}

function loadVoidMessage(prevIndex = -1) {
    var voidMessages = $('#voidMessages');
    if (voidMessages.length) {
        var voidMessagesList = JSON.parse(voidMessages.val());
        if (voidMessagesList.length > 0) {
            clearTimeout(secretPassageTimer);
            var index = prevIndex;
            while (prevIndex == index) {
                index = Math.floor(Math.random() * voidMessagesList.length);
            }
            var voidMessage = voidMessagesList[index];
            $('#voidMessage').fadeOut(prevIndex == -1 ? 0 : 500, function () {
                $('#voidMessage')
                    .html("<h5>" + voidMessage.Message + "</h5> <small><a href='/User/" + voidMessage.Username + "'>" + voidMessage.Username + "</a></small>")
                    .fadeIn(500);
            });

            secretPassageTimer = setTimeout(function(){
                loadVoidMessage(index);
            }, 5000);
        }
    }
}

function knowledgeItemSelected(knowledgeItem) {
    var knowledgeItem = $(knowledgeItem);
    libraryKnowledgeType = knowledgeItem.attr("id");

    $('.itemSelected').removeClass('itemSelected');

    if ($('#libraryKnowledgeType').val() == libraryKnowledgeType){
        $('#libraryKnowledgeType').val("");
        $('#knowledgeButton').addClass('disabled');
    }
    else{
        $('#libraryKnowledgeType').val(libraryKnowledgeType);
        knowledgeItem.addClass('itemSelected');
        $('#knowledgeButton').removeClass('disabled');
    }
}

function updateLibrary(e, form, isLibrary, isSecretPassage = false) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.returnUrl) {
                    window.location.href = data.returnUrl;
                    return;
                }
                if (isSecretPassage) {
                    refreshLibrarySecretPassagePartialView(data.successMessage);
                }
                else {
                    refreshLibraryPartialView(isLibrary);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshLibraryPartialView(isLibrary); 
                }
            }
        }
    });

    e.preventDefault();
};

function refreshLibraryPartialView(isLibrary) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isLibrary : isLibrary },
        url: "/Library/GetLibraryPartialView",
        success: function(data)
        {
            $("#libraryPartialView").html(data);
            onPageLoad();
            if (isLibrary) {
                $('#' + libraryKnowledgeType.toLowerCase() + '-level').addClass('animated tada');
            }
        }
    });
}

function refreshLibrarySecretPassagePartialView(scrollToTop = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Library/GetLibrarySecretPassagePartialView",
        success: function(data)
        {
            $("#librarySecretPassagePartialView").html(data);
            onPageLoad(scrollToTop);
            onSecretPassageLoad();
        }
    });
}