﻿function updateClubhouse(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshClubhousePartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshClubPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshClubhousePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Clubhouse/GetClubhousePartialView",
        success: function(data)
        {
            $("#clubhousePartialView").html(data);
            onPageLoad();
        }
    });
}