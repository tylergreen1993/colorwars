﻿function updateRepair(e, form, isRecharge, fromStadium) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, false);
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshRepairPartialView(isRecharge, fromStadium);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshRepairPartialView(isRecharge, fromStadium);
                }
            }
        }
    });

    e.preventDefault();
};

function refreshRepairPartialView(isRecharge, fromStadium) {
    $.ajax({
        type: "GET",
        cache: false,
        data: { isRecharge : isRecharge, fromStadium : fromStadium },
        url: "/Repair/GetRepairPartialView",
        success: function(data)
        {
            $("#repairPartialView").html(data);
            onPageLoad(false, false);
        }
    });
}