﻿var streakCardId = "";

function revealStreakCard(position) {
    $.ajax({
        type: "POST",
        url: "/StreakCards/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                var showMessage = false;
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                    showMessage = true;
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                    showMessage = true;
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                refreshStreakCardsPartialView(showMessage, position);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function refreshStreakCardsPartialView(scrollToTop, position){
    $.ajax({
        type: "GET",
        cache: false,
        url: "/StreakCards/GetStreakCardsPartialView",
        success: function(data)
        {
            $("#streakCardsPartialView").html(data);
            deactivateAllStreakCards();
            streakCardId = '#streak-card-' + position;
            var countTo = $(streakCardId).attr('data-count');
            var currency = $(streakCardId).text().split(' ')[1];
            countToNumber($(streakCardId), countTo, 0, " " + currency, activateAllStreakCards);
            onPageLoad(scrollToTop);
        }
    });
}

function activateAllStreakCards() {
    $('.active-streak-card').each(function() {
        $(this).removeClass('faded');
    });
    $('.streak-card').each(function() {
        $(this).css('pointer-events', '');
    });

    $(streakCardId).addClass('animated tada');
}
function deactivateAllStreakCards() {
    $('.active-streak-card').each(function() {
        $(this).addClass('faded');
    });
    $('.streak-card').each(function() {
        $(this).css('pointer-events', 'none');
    });
}