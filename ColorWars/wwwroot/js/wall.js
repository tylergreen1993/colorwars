﻿function submitWallForm(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshWallPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    window.location.reload();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshWallPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Wall/GetWallPartialView",
        cache: false,
        success: function(data)
        {
            $("#wallPartialView").html(data);
            onPageLoad();
        }
    });
}