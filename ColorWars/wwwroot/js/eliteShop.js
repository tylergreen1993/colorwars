﻿function updateEliteShop(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshEliteShopPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshEliteShopPartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshEliteShopPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/EliteShop/GetEliteShopPartialView",
        success: function(data)
        {
            $("#eliteShopPartialView").html(data);
            onPageLoad();
        }
    });
}