﻿function removeArticle(id){
    if (confirm("Are you sure you want to remove this article? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/News/RemoveArticle",
            data: { id : id },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    window.location.href = "/News";
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function removeNewsComment(newsId, commentId){
    if (confirm("Are you sure you want to remove your comment? This cannot be undone.")) {
        $.ajax({
            type: "POST",
            url: "/News/RemoveComment",
            data: { 
                newsId : newsId, 
                commentId : commentId,  
            },
            success: function(data)
            {
                hideAllMessages();
                if (data.success){
                    refreshNewsArticlePartialView(newsId, false, false, "", false);
                }
                else{
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        });
    }
}

function likeNewsComment(newsId, commentId){
    $.ajax({
        type: "POST",
        url: "/News/LikeComment",
        data: { 
            newsId : newsId, 
            commentId : commentId,  
        },
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshNewsArticlePartialView(newsId, false, true, commentId);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });
}

function submitNewsCommentForm(e, form, newsId, isReply) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                refreshNewsArticlePartialView(newsId, false, true);
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
        }
    });

    e.preventDefault();
};

function refreshNewsArticlePartialView(id, scrollToTop = true, showScroll = false, commentId = "", movePage = true) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/News/GetNewsArticlePartialView",
        cache: false,
        data: {id : id},
        success: function(data)
        {
            $("#newsPartialView").html(data);
            onPageLoad(scrollToTop);
            if (movePage) {
                $('html, body').animate({
                    scrollTop: $(commentId != "" ? "#" + commentId : ".last-comment").offset().top - (showScroll ? 50 : 100)
                }, showScroll ? 500 : 0);
            }
        }
    });
}