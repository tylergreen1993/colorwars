﻿function updateStore(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success == false){
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
            }
            else{
                $("#storePartialView").html(data);
                onPageLoad();
            }
        }
    });

    e.preventDefault();
};

function submitBargainForm(e, form) {
    var form = $(form);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage, true, true, data.buttonText, data.buttonUrl);
                    refreshStorePartialView();
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.soundUrl) {
                    playSound(data.soundUrl);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshStorePartialView();
                }
            }
        }
    });

    e.preventDefault();
}

function refreshStorePartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Store/GetStorePartialView",
        success: function(data)
        {
            $("#storePartialView").html(data);
            onPageLoad();
        }
    });
}