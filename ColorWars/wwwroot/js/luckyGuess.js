﻿function revealLuckyGuessChest(position) {
    $.ajax({
        type: "POST",
        url: "/LuckyGuess/Reveal",
        data: { position : position },
        success: function(data)
        {
            hideAllMessages();
            if (data.soundUrl) {
                playSound(data.soundUrl);
            }
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                refreshLuckyGuessPartialView(true);
            }
            else {
                if (data.dangerMessage) {
                    showDangerMessage(data.dangerMessage, true);
                    refreshLuckyGuessPartialView();
                }
                else {
                    showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                }
            }
        }
    });
}

function refreshLuckyGuessPartialView(isWinner = false) {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/LuckyGuess/GetLuckyGuessPartialView",
        cache: false,
        success: function(data)
        {
            $("#luckyGuessPartialView").html(data);
            onPageLoad(false);
            if (isWinner) {
                $('#pyro-container').show();
            }
        }
    });
}