﻿$(document).ready(function() {
    onTermsLoad();
});

function onTermsLoad() {
    var sideTermsNav = $('#sideTermsNav');
    if (sideTermsNav.length) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 100) {
                $('#sideTermsNav').addClass('fixed');
            }
            if ($(window).scrollTop() < 100) {
                $('#sideTermsNav').removeClass('fixed');
            }
        });
        $('body').scrollspy({
            target: '#termsScrollSpy',
            offset: 40
        });
    }
}

function updateTerms(e, form) {
    form = $(form);
    var url = form.attr('action');
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
            hideAllMessages();
            if (data.success){
                if (data.successMessage) {
                    showSuccessMessage(data.successMessage);
                }
                if (data.infoMessage) {
                    showInfoMessage(data.infoMessage);
                }
                if (data.points) {
                    updatePoints(data.points);
                }
                refreshTermsPartialView();
            }
            else{
                showErrorMessage(data.loggedMessage ? data.loggedMessage : data.errorMessage);
                if (data.shouldRefresh) {
                    refreshTermsPartialView();
                }
            }
        }
    });

    e.preventDefault();
};

function refreshTermsPartialView() {
    $.ajax({
        type: "GET",
        cache: false,
        url: "/Terms/GetTermsPartialView",
        success: function(data)
        {
            $("#termsPartialView").html(data);
            onPageLoad();
            onTermsLoad();
        }
    });
}