﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Classes
{
    public static class Log
    {
        public static void Debug(string message)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                DateTime currentTime = DateTime.UtcNow;
                string logMessage = $"{currentTime} : {message} : END";

                string filePath = "Logs/Logs.txt";
                IRequestCookieCollection requestCookies = GlobalHttpContext.Current.Request.Cookies;
                if (requestCookies.ContainsKey("Username") && !string.IsNullOrEmpty(requestCookies["Username"]))
                {
                    string username = requestCookies["Username"];
                    filePath = $"Logs/{username}_Logs.txt";
                }

                File.AppendAllTextAsync(filePath, logMessage + Environment.NewLine);
            }
        }
    }
}
