﻿using System;

namespace ColorWars.Classes
{
    public static class ServicesLocator
    {
        public static IServiceProvider services;

        public static IServiceProvider Services
        {
            get { return services; }
            set
            {
                if (services != null)
                {
                    throw new Exception("Can't set services once a value has already been set.");
                }
                services = value;
            }
        }

        public static T GetService<T>()
        where T : class
        {
            return services.GetService(typeof(T)) as T;
        }
    }
}
