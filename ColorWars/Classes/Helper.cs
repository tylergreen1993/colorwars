﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using ColorWars.Models;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using UAParser;

namespace ColorWars.Classes
{
    public static class Helper
    {
        public static string Hash(string input)
        {
            byte[] hash = NewMethod(input);
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        private static byte[] NewMethod(string input)
        {
            return new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
        }

        public static string GetFormattedPoints(int amount)
        {
            return $"{FormatNumber(amount)} {Resources.Currency}";
        }

        public static string GetFormattedPoints(double amount)
        {
            return $"{FormatNumber((int)Math.Round(amount))} {Resources.Currency}";
        }

        public static string FormatNumber(int number)
        {
            return string.Format("{0:n0}", number);
        }

        public static string FormatNumberShortened(double number)
        {
            if (number > 1000000)
                return Math.Round(number / 1000000, 1).ToString() + "M";
            if (number > 1000)
                return Math.Round(number / 1000, 1).ToString() + "K";
            return number.ToString();
        }

        public static string GetImageUrl(string imageName)
        {
            if (string.IsNullOrEmpty(imageName))
            {
                return string.Empty;
            }

            if (imageName.Contains("https://"))
            {
                return imageName;
            }

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                return $"/images/{imageName}";
            }

            return $"{GetCDNRoot()}/images/{imageName}";
        }

        public static string GetSoundUrl(string soundName)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                return $"/sounds/{soundName}";
            }

            return $"{GetCDNRoot()}/sounds/{soundName}";
        }

        public static string GetCDNRoot()
        {
            return $"https://cdn.{Resources.SiteName.ToLower()}.com";
        }

        public static string GetDiscordUrl()
        {
            return "https://discord.gg/9XzujH6";
        }

        public static string GetRandomString(int size)
        {
            char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[size];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public static string ShiftText(string input, int shiftAmount)
        {
            string shiftedText = string.Empty;
            foreach (char ch in input)
            {
                if (!char.IsLetter(ch) || ch == ' ')
                {

                    shiftedText += ch;
                }
                else
                {
                    char cased = char.IsUpper(ch) ? 'A' : 'a';
                    shiftedText += (char)(((ch + shiftAmount - cased) % 26) + cased);
                }
            }

            return shiftedText;
        }

        public static string GetCourierUsername()
        {
            return "Courier";
        }

        public static bool IsCourier(User user)
        {
            return user.Username == GetCourierUsername();
        }

        public static User GetAdminUser()
        {
            return ServicesLocator.GetService<IUserRepository>().GetUser("Admin");
        }

        public static bool IsThrottled(User user)
        {
            if (user == null)
            {
                return false;
            }

            IMemoryCache memoryCache = ServicesLocator.GetService<IMemoryCache>();

            if (memoryCache.TryGetValue($"{user.Username}-throttled", out object value))
            {
                return (bool)value;
            }
            memoryCache.Set($"{user.Username}-throttled", true, DateTime.UtcNow.AddSeconds(0.25));
            return false;
        }

        public static int GetMaxDeckSize()
        {
            return 5;
        }

        public static int GetMaxBagSize()
        {
            return 50;
        }

        public static int GetMaxStallSize()
        {
            return 50;
        }

        public static int GetMaxStorageSize()
        {
            return 100;
        }

        public static int GetMaxShowroomSize()
        {
            return 50;
        }

        public static int GetMaxMatchSeconds()
        {
            return 90;
        }

        public static int GetMinMatchPoints()
        {
            return 50;
        }

        public static int GetStoreRefreshMinutes()
        {
            return 30;
        }

        public static int GetBuyersClubRefreshMinutes()
        {
            return 30;
        }

        public static int GetShowcaseRefreshMinutes()
        {
            return 15;
        }

        public static int GetBuyersClubPointsRequired()
        {
            return 5000;
        }

        public static int MaxSharesPerDay()
        {
            return 400;
        }

        public static int PointsPerReferralCollect()
        {
            return 1000;
        }

        public static int WinningColorWarsPotSize()
        {
            return 25000;
        }

        public static int GetMaxDisposerTrainingTokenProgess()
        {
            return 400;
        }

        public static ItemCondition GetItemCondition(int days)
        {
            if (days < 30)
            {
                return ItemCondition.New;
            }
            if (days < 60)
            {
                return ItemCondition.Good;
            }
            if (days < 120)
            {
                return ItemCondition.Poor;
            }
            return ItemCondition.Unusable;
        }

        public static string GetItemConditionColor(ItemCondition condition)
        {
            switch (condition)
            {
                case ItemCondition.Good:
                    return "off-green-color";
                case ItemCondition.Poor:
                    return "orange-color";
                case ItemCondition.Unusable:
                    return "lose-red";
                default:
                    return "win-green";
            }
        }

        public static string ExtractUserReply(string message, out List<string> mentionedUsernames, bool keepAtSymbol = false, bool keepBold = true)
        {
            mentionedUsernames = new List<string>();
            if (!message.Contains("@"))
            {
                return message;
            }
            string[] lines = message.Split("\n");
            for (int i = 0; i < lines.Length; i++)
            {
                string[] words = lines[i].Split(" ");
                for (int j = 0; j < words.Length; j++)
                {
                    if (words[j].StartsWith("@", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (words[j].Length == 1)
                        {
                            continue;
                        }
                        string username = words[j][1..];
                        char[] trimChars = { '.', '!', '?', ',' };
                        string trimmedUsername = username.TrimEnd(trimChars).TrimStart('\r', '\n').TrimEnd('\r', '\n');
                        mentionedUsernames.Add(trimmedUsername);
                        words[j] = $"{(keepBold ? "<b>" : "")}<a href='/User/{trimmedUsername}'>{(keepAtSymbol ? "@" : "")}{username}</a>{(keepBold ? "</b>" : "")}";
                    }
                }

                lines[i] = string.Join(" ", words);
            }

            mentionedUsernames = mentionedUsernames.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

            return string.Join("\n", lines);
        }

        public static string ExtractUserReply(string message, bool keepAtSymbol = false)
        {
            _ = new List<string>();
            return ExtractUserReply(message, out _, keepAtSymbol);
        }

        public static string MakeLinkClickable(string message)
        {
            return Regex.Replace(message, @"((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)",
                "<a href='$1'>$1</a>");
        }

        public static string AddSpoilerTag(string text)
        {
            string pattern = "&lt;spoiler&gt;(.*?)&lt;\\/spoiler&gt;";
            MatchCollection matches = Regex.Matches(text, pattern);
            foreach (Match match in matches)
            {
                string spoilerString = $"<details><summary>Spoiler</summary>{match.Groups[1]}</details>";
                text = text.Replace(match.Groups[0].Value, spoilerString);
            }

            return text;
        }

        public static string GetDomain(HttpContext httpContext = null)
        {
            if (httpContext == null)
            {
                httpContext = GlobalHttpContext.Current;
            }
            return $"{httpContext.Request.Scheme}://{httpContext.Request.Host.Value}";
        }

        public static string MakeUserMessageClickable(string username, string message)
        {
            if (username == GetCourierUsername())
            {
                return Regex.Replace(message, @"(\b[A-Z0-9]{8,}\b)", x => $"<b><a href='/PromoCodes?code={x.Groups[0].Value}'>{x.Groups[0].Value}</a></b>");
            }

            return message;
        }

        public static string MakeUserMessageItalicized(string username, string message)
        {
            if (username == GetCourierUsername())
            {
                return ItalicizeQuotes(message);
            }

            return message;
        }

        public static string BoldQuotes(string message)
        {
            return Regex.Replace(message, @"""(.+?)""", x => "<b>\"" + x.Groups[1].Value + "\"</b>");
        }

        public static string ItalicizeQuotes(string message)
        {
            return Regex.Replace(HttpUtility.HtmlDecode(message), @"""(.+?)""", x => "<i>\"" + x.Groups[1].Value + "\"</i>");
        }

        public static string BoldPoints(string message)
        {
            return Regex.Replace(message, $@"(\b(?<!<[^>]*)\d+([\d,]?\d)*(\.\d+)?|\%)\s*(\b{Resources.Currency}\b)", x => $"<b>{x.Groups[0].Value}</b>");
        }

        public static string BoldNumbers(string message)
        {
            return Regex.Replace(message, $@"(\b(?<!<[^>]*)\d+([\d,]?\d)*(\.\d+)?|\%)", x => $"<b>{x.Groups[0].Value}</b>");
        }

        public static string BoldTimes(string message)
        {
            message = Regex.Replace(message, $@"\s*(\bminute\b)", x => $"<b>{x.Groups[0].Value}</b>");
            message = Regex.Replace(message, $@"\s*(\bminutes\b)", x => $"<b>{x.Groups[0].Value}</b>");
            message = Regex.Replace(message, $@"\s*(\bhour\b)", x => $"<b>{x.Groups[0].Value}</b>");
            message = Regex.Replace(message, $@"\s*(\bhours\b)", x => $"<b>{x.Groups[0].Value}</b>");
            message = Regex.Replace(message, $@"\s*(\bday\b)", x => $"<b>{x.Groups[0].Value}</b>");
            message = Regex.Replace(message, $@"\s*(\bdays\b)", x => $"<b>{x.Groups[0].Value}</b>");

            return message;
        }

        public static string GetUserAgentInfo(string userAgent)
        {
            if (string.IsNullOrEmpty(userAgent))
            {
                return string.Empty;
            }

            Parser uaParser = Parser.GetDefault();
            ClientInfo clientInfo = uaParser.Parse(userAgent);
            if (clientInfo == null)
            {
                return string.Empty;
            }

            return $"{clientInfo.UA.Family} {clientInfo.UA.Major}.{clientInfo.UA.Minor} {clientInfo.OS.Family} {clientInfo.OS.Major}.{clientInfo.OS.Minor}";
        }

        public static Occasion GetCurrentOccasion()
        {
            DateTime currentDate = DateTime.UtcNow;
            if (currentDate.Month == 2 && currentDate.Day <= 15)
            {
                return Occasion.Love;
            }
            if (currentDate.Month == 4 && currentDate.Day >= 20 && currentDate.Day <= 25)
            {
                return Occasion.Earth;
            }
            if (currentDate.Month == 7)
            {
                return Occasion.Sun;
            }
            if (currentDate.Month == 8)
            {
                return Occasion.Coastal;
            }
            if (currentDate.Month == 10)
            {
                return Occasion.Pumpkin;
            }
            if (currentDate.Month == 12 && currentDate.Day <= 26)
            {
                return Occasion.Holidays;
            }
            if (currentDate.Month == 12 || (currentDate.Month == 1 && currentDate.Day <= 5))
            {
                return Occasion.Celebration;
            }
            return Occasion.None;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rnd = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void SetObject(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            string value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }

        public static string GetTimeSince(this DateTime dateTime)
        {
            int totalMinutes = (int)(DateTime.UtcNow - dateTime).TotalMinutes;
            if (totalMinutes < 60)
            {
                return $"{totalMinutes} min{(totalMinutes == 1 ? "" : "s")} ago";
            }
            if (totalMinutes < 60 * 24)
            {
                return $"{totalMinutes / 60} hour{(totalMinutes / 60 == 1 ? "" : "s")} ago";
            }
            if (dateTime.AddYears(1) > DateTime.UtcNow && dateTime.DayOfYear != DateTime.UtcNow.DayOfYear)
            {
                return dateTime.ToString("MMM d");
            }
            return dateTime.ToString("MMM d, yyyy");
        }

        public static string GetTimeUntil(this DateTime dateTime)
        {
            TimeSpan timeUntilEnd = dateTime - DateTime.UtcNow;
            int totalMinutes = (int)Math.Max(timeUntilEnd.TotalMinutes, 1);
            if (totalMinutes >= 60 * 24)
            {
                int totalDays = (int)Math.Round(timeUntilEnd.TotalDays);
                return $"{FormatNumber(totalDays)} day{(totalDays == 1 ? "" : "s")}";
            }
            if (totalMinutes >= 60)
            {
                int totalHours = (int)Math.Round(timeUntilEnd.TotalHours);
                return $"{totalHours} hour{(totalHours == 1 ? "" : "s")}";
            }
            return $"{totalMinutes} min{(totalMinutes == 1 ? "" : "s")}";
        }
    }
}

public enum Occasion
{
    [Display(Name = "None")]
    None = 0,
    [Display(Name = "Celebration")]
    Celebration = 1,
    [Display(Name = "Love")]
    Love = 2,
    [Display(Name = "Sun")]
    Sun = 3,
    [Display(Name = "Pumpkin")]
    Pumpkin = 4,
    [Display(Name = "Holidays")]
    Holidays = 5,
    [Display(Name = "Earth")]
    Earth = 6,
    [Display(Name = "Coastal")]
    Coastal = 7
}
