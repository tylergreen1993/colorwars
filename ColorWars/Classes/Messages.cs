﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Classes
{
    public static class Messages
    {
        public static void AddMessage(string message)
        {
            GlobalHttpContext.Current.Session.SetString("Message", message);

        }

        public static void SetSuccessMessage(string message)
        {
            GlobalHttpContext.Current.Session.SetString("SuccessMessage", message);
        }

        public static void AddSnack(string message, bool isWarning = false)
        {
            AddToast(string.Empty, message, string.Empty, true, isWarning);
        }

        public static void AddToast(string title, string message, string url, bool isSnack = false, bool isWarning = false)
        {
            List<string[]> toastSuccessMessages = GlobalHttpContext.Current.Session.GetObject<List<string[]>>("ToastSuccessMessages");
            if (toastSuccessMessages == null)
            {
                toastSuccessMessages = new List<string[]>();
            }
            string[] messageArray = { title, message, url, isSnack.ToString(), isWarning.ToString() };
            toastSuccessMessages.Add(messageArray);
            GlobalHttpContext.Current.Session.SetObject("ToastSuccessMessages", toastSuccessMessages);
        }

        public static string GetMessage()
        {
            string message = GlobalHttpContext.Current.Session.GetString("Message");
            GlobalHttpContext.Current.Session.Remove("Message");
            return message;
        }

        public static string GetSuccessMessage()
        {
            string message = GlobalHttpContext.Current.Session.GetString("SuccessMessage");
            GlobalHttpContext.Current.Session.Remove("SuccessMessage");
            return message;
        }

        public static List<string[]> GetToast()
        {
            List<string[]> toastSuccessMessages = GlobalHttpContext.Current.Session.GetObject<List<string[]>>("ToastSuccessMessages");
            GlobalHttpContext.Current.Session.Remove("ToastSuccessMessages");
            return toastSuccessMessages;
        }
    }
}
