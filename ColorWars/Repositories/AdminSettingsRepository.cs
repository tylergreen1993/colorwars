﻿using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class AdminSettingsRepository : IAdminSettingsRepository
    {
        private IAdminSettingsPersistence _adminSettingsPersistence;

        public AdminSettingsRepository(IAdminSettingsPersistence adminSettingsPersistence)
        {
            _adminSettingsPersistence = adminSettingsPersistence;
        }

        public AdminSettings GetSettings(bool isCached = true)
        {
            return _adminSettingsPersistence.GetAdminSettings(isCached);
        }

        public bool SetSetting(string name, string value)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            return _adminSettingsPersistence.AddAdminSetting(name, value);
        }
    }
}
