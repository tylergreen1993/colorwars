﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;
using RandomNameGeneratorLibrary;

namespace ColorWars.Repositories
{
    public class CPURepository : ICPURepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IItemsRepository _itemsRepository;
        private IPowerMovesRepository _powerMovesRepository;
        private IProfileRepository _profileRepository;
        private ICompanionsRepository _companionsRepository;
        private IBankRepository _bankRepository;
        private IChecklistRepository _checklistRepository;
        private ISession _session;
        private List<CPU> _CPUs;

        public CPURepository(IHttpContextAccessor httpContextAccessor, IItemsRepository itemsRepository,
        IPowerMovesRepository powerMovesRepository, IProfileRepository profileRepository,
        ICompanionsRepository companionsRepository, IBankRepository bankRepository, IChecklistRepository checklistRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _itemsRepository = itemsRepository;
            _powerMovesRepository = powerMovesRepository;
            _profileRepository = profileRepository;
            _companionsRepository = companionsRepository;
            _bankRepository = bankRepository;
            _checklistRepository = checklistRepository;
            _session = _httpContextAccessor.HttpContext.Session;
            _CPUs = new List<CPU>();

            PopulateCPUs();
        }

        public CPU GetCPU(int level, bool isShuffled = false, bool forceCache = false)
        {
            if (forceCache)
            {
                PopulateCPUs(false);
            }
            CPU cpu = _CPUs.Find(x => x.Level == (level % _CPUs.Count));
            int intensity = level / _CPUs.Count;
            CPU cpuCopy = new CPU
            {
                Name = cpu.Name,
                Deck = cpu.Deck,
                Enhancers = cpu.Enhancers,
                Level = cpu.Level,
                Intensity = intensity,
                Reward = (int)Math.Round(cpu.Reward * (2 + (intensity * 0.25))),
                ImageUrl = cpu.ImageUrl
            };

            if (cpuCopy.Intensity > 0)
            {
                Random rnd = new Random();
                List<ItemTheme> themes = new List<ItemTheme> { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Club, ItemTheme.Elegant, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                ItemTheme theme = themes[Math.Min(themes.Count - 1, cpuCopy.Intensity - 1)];

                foreach (Card card in cpuCopy.Deck)
                {
                    card.Theme = theme;
                }
            }

            if (isShuffled && level > 2)
            {
                return Shuffle(cpuCopy);
            }

            return cpuCopy;
        }

        public int GetCPUCount()
        {
            return _CPUs.Count;
        }

        private void PopulateCPUs(bool isCached = true)
        {
            List<CPU> sessionCPUs = isCached ? _session.GetObject<List<CPU>>("CPUs") : null;
            if (sessionCPUs != null)
            {
                _CPUs = sessionCPUs;
                return;
            }

            _CPUs = new List<CPU>();

            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers();
            List<Enhancer> cpuEnhancers = allEnhancers.FindAll(x => x.TotalUses > 0 && !x.IsFiller && x.SpecialType != EnhancerSpecialType.Luck && x.SpecialType != EnhancerSpecialType.Blocker);
            List<Enhancer> allFillers = allEnhancers.FindAll(x => x.TotalUses > 0 && x.IsFiller);
            Enhancer blocker = allEnhancers.Find(x => x.TotalUses > 0 && x.SpecialType == EnhancerSpecialType.Blocker);

            _CPUs.Add(new CPU
            {
                Name = "Tutorial Tim",
                Deck = new List<Card> { allCards[10], allCards[14], allCards[20], allCards[9], allCards[6] },
                Enhancers = new List<Enhancer> { cpuEnhancers[0], null, cpuEnhancers[3], allEnhancers[2], cpuEnhancers[0] },
                Level = 0,
                Reward = 500,
                ImageUrl = "CPU/Tim.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Elegant Elena",
                Deck = new List<Card> { allCards[12], allCards[9], allCards[13], allCards[8], allCards[14] },
                Enhancers = new List<Enhancer> { cpuEnhancers[3], cpuEnhancers[1], blocker, blocker, cpuEnhancers[0] },
                Level = 1,
                Reward = 1000,
                ImageUrl = "CPU/Elena.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Chipper Charlie",
                Deck = new List<Card> { allCards[9], allCards[12], allCards[11], allCards[13], allCards[11] },
                Enhancers = new List<Enhancer> { allFillers[1], cpuEnhancers[0], cpuEnhancers[1], cpuEnhancers[1], blocker },
                Level = 2,
                Reward = 1500,
                ImageUrl = "CPU/Charlie.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Smart Sandra",
                Deck = new List<Card> { allCards[8], allCards[12], allCards[12], allCards[14], allCards[11] },
                Enhancers = new List<Enhancer> { cpuEnhancers[0], cpuEnhancers[4], cpuEnhancers[2], cpuEnhancers[1], blocker },
                Level = 3,
                Reward = 2000,
                ImageUrl = "CPU/Sandra.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Macho Mike",
                Deck = new List<Card> { allCards[17], allCards[18], allCards[18], allCards[19], allCards[17] },
                Enhancers = new List<Enhancer> { cpuEnhancers[3], blocker, cpuEnhancers[4], cpuEnhancers[4], allFillers[2] },
                Level = 4,
                Reward = 2500,
                ImageUrl = "CPU/Mike.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Clever Crystal",
                Deck = new List<Card> { allCards[19], allCards[19], allCards[21], allCards[18], allCards[20] },
                Enhancers = new List<Enhancer> { cpuEnhancers[4], cpuEnhancers[4], cpuEnhancers[3], cpuEnhancers[1], allFillers[2] },
                Level = 5,
                Reward = 3000,
                ImageUrl = "CPU/Crystal.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Facetious Frank",
                Deck = new List<Card> { allCards[20], allCards[21], allCards[20], allCards[18], allCards[19] },
                Enhancers = new List<Enhancer> { allFillers[2], cpuEnhancers[3], cpuEnhancers[2], cpuEnhancers[5], blocker },
                Level = 6,
                Reward = 3500,
                ImageUrl = "CPU/Frank.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Aggravated Annabelle",
                Deck = new List<Card> { allCards[19], allCards[19], allCards[20], allCards[21], allCards[19] },
                Enhancers = new List<Enhancer> { cpuEnhancers[6], cpuEnhancers[3], blocker, allFillers[2], cpuEnhancers[3] },
                Level = 7,
                Reward = 4000,
                ImageUrl = "CPU/Annabelle.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Powerful Pete",
                Deck = new List<Card> { allCards[19], allCards[20], allCards[22], allCards[20], allCards[21] },
                Enhancers = new List<Enhancer> { cpuEnhancers[5], allFillers[2], cpuEnhancers[3], blocker, cpuEnhancers[2] },
                Level = 8,
                Reward = 4500,
                ImageUrl = "CPU/Pete.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Talkative Tina",
                Deck = new List<Card> { allCards[22], allCards[22], allCards[25], allCards[23], allCards[20] },
                Enhancers = new List<Enhancer> { blocker, cpuEnhancers[2], cpuEnhancers[3], allFillers[2], cpuEnhancers[7] },
                Level = 9,
                Reward = 5000,
                ImageUrl = "CPU/Tina.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Baritone Barry",
                Deck = new List<Card> { allCards[23], allCards[23], allCards[22], allCards[24], allCards[22] },
                Enhancers = new List<Enhancer> { cpuEnhancers[6], blocker, cpuEnhancers[5], cpuEnhancers[3], cpuEnhancers[3] },
                Level = 10,
                Reward = 5500,
                ImageUrl = "CPU/Barry.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Shady Sylvia",
                Deck = new List<Card> { allCards[24], allCards[25], allCards[21], allCards[26], allCards[24] },
                Enhancers = new List<Enhancer> { cpuEnhancers[3], cpuEnhancers[3], cpuEnhancers[4], cpuEnhancers[6], cpuEnhancers[3] },
                Level = 11,
                Reward = 6000,
                ImageUrl = "CPU/Sylvia.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Galactic George",
                Deck = new List<Card> { allCards[26], allCards[23], allCards[26], allCards[29], allCards[27] },
                Enhancers = new List<Enhancer> { cpuEnhancers[2], cpuEnhancers[6], cpuEnhancers[5], cpuEnhancers[2], allFillers[2] },
                Level = 12,
                Reward = 6500,
                ImageUrl = "CPU/George.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Meddlin' Madison",
                Deck = new List<Card> { allCards[28], allCards[27], allCards[29], allCards[30], allCards[31] },
                Enhancers = new List<Enhancer> { cpuEnhancers[5], cpuEnhancers[4], blocker, cpuEnhancers[2], cpuEnhancers[4] },
                Level = 13,
                Reward = 7000,
                ImageUrl = "CPU/Madison.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Jammin' Joshua",
                Deck = new List<Card> { allCards[32], allCards[31], allCards[35], allCards[32], allCards[29] },
                Enhancers = new List<Enhancer> { cpuEnhancers[10], cpuEnhancers[3], allFillers[3], cpuEnhancers[6], blocker },
                Level = 14,
                Reward = 7500,
                ImageUrl = "CPU/Joshua.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Creepy Christina",
                Deck = new List<Card> { allCards[35], allCards[35], allCards[36], allCards[32], allCards[38] },
                Enhancers = new List<Enhancer> { cpuEnhancers[6], cpuEnhancers[13], allFillers[3], blocker, cpuEnhancers[4] },
                Level = 15,
                Reward = 8000,
                ImageUrl = "CPU/Christina.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Silent Sam",
                Deck = new List<Card> { allCards[32], allCards[36], allCards[35], allCards[39], allCards[37] },
                Enhancers = new List<Enhancer> { cpuEnhancers[8], cpuEnhancers[14], blocker, cpuEnhancers[3], cpuEnhancers[12] },
                Level = 16,
                Reward = 8500,
                ImageUrl = "CPU/Sam.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Valiant Victoria",
                Deck = new List<Card> { allCards[38], allCards[37], allCards[37], allCards[40], allCards[37] },
                Enhancers = new List<Enhancer> { cpuEnhancers[6], cpuEnhancers[8], blocker, cpuEnhancers[8], allFillers[3] },
                Level = 17,
                Reward = 9000,
                ImageUrl = "CPU/Victoria.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Scattered Steven",
                Deck = new List<Card> { allCards[30], allCards[40], allCards[42], allCards[50], allCards[60] },
                Enhancers = new List<Enhancer> { blocker, cpuEnhancers[2], allFillers[3], blocker, cpuEnhancers[12] },
                Level = 18,
                Reward = 9500,
                ImageUrl = "CPU/Steven.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Hostile Heather",
                Deck = new List<Card> { allCards[43], allCards[45], allCards[40], allCards[44], allCards[46] },
                Enhancers = new List<Enhancer> { cpuEnhancers[15], blocker, allFillers[3], cpuEnhancers[6], cpuEnhancers[9] },
                Level = 19,
                Reward = 10000,
                ImageUrl = "CPU/Heather.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Gregarious Greg",
                Deck = new List<Card> { allCards[40], allCards[44], allCards[50], allCards[47], allCards[45] },
                Enhancers = new List<Enhancer> { cpuEnhancers[14], cpuEnhancers[5], cpuEnhancers[6], cpuEnhancers[8], cpuEnhancers[13] },
                Level = 20,
                Reward = 10500,
                ImageUrl = "CPU/Greg.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Misplaced Meryl",
                Deck = new List<Card> { allCards[5], allCards[5], allCards[3], allCards[6], allCards[10] },
                Enhancers = new List<Enhancer> { cpuEnhancers[5], cpuEnhancers[7], allFillers[4], blocker, cpuEnhancers[2] },
                Level = 21,
                Reward = 11000,
                ImageUrl = "CPU/Meryl.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Bashful Brandon",
                Deck = new List<Card> { allCards[43], allCards[46], allCards[50], allCards[52], allCards[49] },
                Enhancers = new List<Enhancer> { cpuEnhancers[10], cpuEnhancers[16], blocker, allFillers[3], cpuEnhancers[12] },
                Level = 22,
                Reward = 11500,
                ImageUrl = "CPU/Brandon.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Controlling Clarissa",
                Deck = new List<Card> { allCards[52], allCards[50], allCards[52], allCards[56], allCards[53] },
                Enhancers = new List<Enhancer> { cpuEnhancers[14], cpuEnhancers[8], cpuEnhancers[13], cpuEnhancers[6], blocker },
                Level = 23,
                Reward = 12000,
                ImageUrl = "CPU/Clarissa.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Sweet Simon",
                Deck = new List<Card> { allCards[51], allCards[60], allCards[55], allCards[58], allCards[53] },
                Enhancers = new List<Enhancer> { cpuEnhancers[8], cpuEnhancers[16], allFillers[3], blocker, cpuEnhancers[14] },
                Level = 24,
                Reward = 13000,
                ImageUrl = "CPU/Simon.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Blockin' Brandy",
                Deck = new List<Card> { allCards[59], allCards[53], allCards[51], allCards[60], allCards[60] },
                Enhancers = new List<Enhancer> { cpuEnhancers[13], cpuEnhancers[15], blocker, blocker, blocker },
                Level = 25,
                Reward = 14000,
                ImageUrl = "CPU/Brandy.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Devilish Danny",
                Deck = new List<Card> { allCards[65], allCards[65], allCards[65], allCards[65], allCards[65] },
                Enhancers = new List<Enhancer> { cpuEnhancers[10], cpuEnhancers[15], blocker, cpuEnhancers[7], allFillers[3] },
                Level = 26,
                Reward = 15000,
                ImageUrl = "CPU/Danny.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Polythene Pam",
                Deck = new List<Card> { allCards[58], allCards[57], allCards[63], allCards[64], allCards[63] },
                Enhancers = new List<Enhancer> { cpuEnhancers[9], cpuEnhancers[10], allFillers[3], cpuEnhancers[8], cpuEnhancers[14] },
                Level = 27,
                Reward = 16000,
                ImageUrl = "CPU/Pam.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Stupendous Stefano",
                Deck = new List<Card> { allCards[60], allCards[63], allCards[63], allCards[68], allCards[62] },
                Enhancers = new List<Enhancer> { blocker, cpuEnhancers[14], cpuEnhancers[14], blocker, cpuEnhancers[10] },
                Level = 28,
                Reward = 17000,
                ImageUrl = "CPU/Stefano.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Wild Wendy",
                Deck = new List<Card> { allCards[64], allCards[68], allCards[69], allCards[70], allCards[69] },
                Enhancers = new List<Enhancer> { blocker, cpuEnhancers[14], allFillers[4], cpuEnhancers[15], cpuEnhancers[15] },
                Level = 29,
                Reward = 18000,
                ImageUrl = "CPU/Wendy.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Righteous Richie",
                Deck = new List<Card> { allCards[74], allCards[74], allCards[69], allCards[72], allCards[70] },
                Enhancers = new List<Enhancer> { cpuEnhancers[8], cpuEnhancers[16], cpuEnhancers[16], allFillers[4], cpuEnhancers[12] },
                Level = 30,
                Reward = 19000,
                ImageUrl = "CPU/Richie.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Third Tammy",
                Deck = new List<Card> { allCards[32], allCards[32], allCards[32], allCards[32], allCards[32] },
                Enhancers = new List<Enhancer> { cpuEnhancers[16], blocker, blocker, cpuEnhancers[16], cpuEnhancers[17] },
                Level = 31,
                Reward = 20000,
                ImageUrl = "CPU/Tammy.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Feared Fred",
                Deck = new List<Card> { allCards[70], allCards[71], allCards[69], allCards[74], allCards[78] },
                Enhancers = new List<Enhancer> { cpuEnhancers[17], blocker, cpuEnhancers[16], allFillers[4], cpuEnhancers[15] },
                Level = 32,
                Reward = 25000,
                ImageUrl = "CPU/Fred.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Nitpicky Nicole",
                Deck = new List<Card> { allCards[71], allCards[72], allCards[78], allCards[79], allCards[81] },
                Enhancers = new List<Enhancer> { cpuEnhancers[18], blocker, cpuEnhancers[16], cpuEnhancers[15], blocker },
                Level = 33,
                Reward = 30000,
                ImageUrl = "CPU/Nicole.png"
            });
            _CPUs.Add(new CPU
            {
                Name = "Terminating Tyler",
                Deck = new List<Card> { allCards[99], allCards[85], allCards[78], allCards[89], allCards[90] },
                Enhancers = new List<Enhancer> { cpuEnhancers[18], blocker, allFillers[4], cpuEnhancers[19], blocker },
                Level = 34,
                Reward = 35000,
                ImageUrl = "CPU/Tyler.png"
            });

            _session.SetObject("CPUs", _CPUs);
        }

        public CPU MakeChallenge(CPUChallenge challenge, int challengesWon)
        {
            int reward = Math.Min(15000, (challengesWon + 1) * 1000);
            Random rnd = new Random();
            List<Card> allCards = _itemsRepository.GetAllCards().FindAll(x => x.Points <= reward * 5 * (challengesWon + 1) && (challenge == CPUChallenge.OnlyRedDeckCards || challenge == CPUChallenge.OnlySameColorDeckCards || x.Points >= Math.Min(50000, challengesWon * 5000))).OrderBy(x => rnd.Next()).ToList();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers().FindAll(x => x.TotalUses > 0 && x.SpecialType != EnhancerSpecialType.Luck && x.Points <= reward * 10 * (challengesWon + 1) && x.Points >= Math.Min(20000, 1000 * challengesWon)).OrderBy(x => rnd.Next()).ToList();

            int deckSize = Helper.GetMaxDeckSize();
            int enhancersSize = rnd.Next(Math.Min(Helper.GetMaxDeckSize() - 1, challengesWon / 5), Helper.GetMaxDeckSize() + 1);

            List<Card> deck = new List<Card>();
            List<Enhancer> enhancers = new List<Enhancer>();
            switch (challenge)
            {
                case CPUChallenge.DeckCardsWorthDouble:
                    deck = allCards.Take(deckSize).ToList();
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.EnhancersWorthDouble:
                    deck = allCards.Take(deckSize).ToList();
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.NoEnhancers:
                    deck = allCards.Take(deckSize).ToList();
                    break;
                case CPUChallenge.OnlyAdditionEnhancers:
                    deck = allCards.Take(deckSize).ToList();
                    enhancers = allEnhancers.FindAll(x => x.IsMultiplied == false && x.IsPercent == false && x.IsFiller == false && x.SpecialType == EnhancerSpecialType.None && x.Amount > 0).Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.OnlyEvenDeckCards:
                    deck = allCards.FindAll(x => x.Amount % 2 == 0).Take(deckSize).ToList();
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.OnlyOddDeckCards:
                    deck = allCards.FindAll(x => x.Amount % 2 != 0).Take(deckSize).ToList();
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.OnlyMultiplicationEnhancers:
                    deck = allCards.Take(deckSize).ToList();
                    enhancers = allEnhancers.FindAll(x => x.IsMultiplied && x.SpecialType == EnhancerSpecialType.None).Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.OnlyRedDeckCards:
                    deck = allCards.FindAll(x => x.Color == CardColor.Red).Take(deckSize).ToList();
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
                case CPUChallenge.OnlySameColorDeckCards:
                    List<CardColor> cardColors = Enum.GetValues(typeof(CardColor)).Cast<CardColor>().ToList();
                    cardColors.RemoveAll(x => x == CardColor.None);
                    cardColors.RemoveAll(x => x == CardColor.Club);
                    cardColors.RemoveAll(x => x == CardColor.Gold);
                    while (deck.Count < deckSize)
                    {
                        CardColor color = cardColors[rnd.Next(cardColors.Count)];
                        if (challengesWon < 5 || color != CardColor.Red)
                        {
                            deck = allCards.FindAll(x => x.Color == color).Take(deckSize).ToList();
                        }
                    }
                    enhancers = allEnhancers.Take(enhancersSize).ToList();
                    break;
            }

            if (enhancers.Count < Helper.GetMaxDeckSize())
            {
                int amountToAdd = Helper.GetMaxDeckSize() - enhancers.Count;
                for (int i = 0; i < amountToAdd; i++)
                {
                    enhancers.Add(null);
                }
            }

            foreach (Card card in deck)
            {
                if (rnd.Next(20 - Math.Min(15, challengesWon)) == 1)
                {
                    List<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>().ToList();
                    themes.Remove(ItemTheme.Club);
                    card.Theme = themes[rnd.Next(themes.Count)];
                }
            }

            PersonNameGenerator personNameGenerator = new PersonNameGenerator();

            CPU cpu = new CPU
            {
                Name = $"Challenger {personNameGenerator.GenerateRandomFirstName()}",
                Deck = deck,
                Enhancers = enhancers,
                Reward = reward,
                ImageUrl = "CPU/RandomChallenger.png"
            };

            return Shuffle(cpu);
        }

        public PowerMoveType GetPowerMove(User user, List<MatchState> matchStates, Settings settings)
        {
            List<PowerMove> powerMoves = _powerMovesRepository.GetPowerMoves(user, matchStates, settings.MatchLevel, true);
            if (powerMoves.Count == 0)
            {
                return PowerMoveType.None;
            }

            if (settings.MatchLevel == 0)
            {
                return GetTutorialPowerMoves(matchStates);
            }
            if (settings.MatchLevel == 1)
            {
                return GetSecondChallengerPowerMoves(matchStates);
            }
            if (settings.MatchLevel == 2)
            {
                return GetThirdChallengerPowerMoves(matchStates);
            }

            if (settings.StadiumIsConfused)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.SelfDestruct || x.Type == PowerMoveType.SilencedEnhancers || x.Type == PowerMoveType.EnhancerSwap || x.Type == PowerMoveType.BlockersBane || x.Type == PowerMoveType.CounterStop);
            }
            if (settings.OpponentStadiumIsConfused)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.DoubledEnhancers);
            }
            if (settings.StadiumChallenge == CPUChallenge.NoEnhancers)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.DoubledEnhancers || x.Type == PowerMoveType.SilencedEnhancers || x.Type == PowerMoveType.EnhancerSwap || x.Type == PowerMoveType.BlockersBane || x.Type == PowerMoveType.Confusion);
            }

            if (powerMoves.Any(x => x.Type == PowerMoveType.LoseControl))
            {
                List<Card> userDeck = _itemsRepository.GetCards(user, true);
                if (userDeck.All(x => x.Color == userDeck.First().Color))
                {
                    powerMoves.RemoveAll(x => x.Type == PowerMoveType.LoseControl);
                }
            }

            MatchState currentMatchState = matchStates.Find(x => x.Status == MatchStatus.NotPlayed);
            if (currentMatchState == matchStates.Last())
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.LuckUp || x.Type == PowerMoveType.PoisonTouch || x.Type == PowerMoveType.Confusion);
            }
            if (currentMatchState.Position > 0)
            {
                PowerMoveType lastPowerMoveType = matchStates[currentMatchState.Position - 1].CPUPowerMove;
                if (lastPowerMoveType == PowerMoveType.PoisonTouch || lastPowerMoveType == PowerMoveType.PlannedStrike || lastPowerMoveType == PowerMoveType.LuckUp)
                {
                    powerMoves.RemoveAll(x => x.Type == PowerMoveType.Revival);
                }
            }
            if (matchStates.Any(x => x.Position < currentMatchState.Position && x.PowerMove == PowerMoveType.PowerStop))
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.CounterStop);
            }

            Random rnd = new Random();
            PowerMove powerMove = null;
            List<PowerMove> luckPowerMoves = powerMoves.FindAll(x => x.Type == PowerMoveType.AllOutAttack || x.Type == PowerMoveType.LadyLuck || x.Type == PowerMoveType.BlockersBane || x.Type == PowerMoveType.BigShot);
            if (settings.OpponentStadiumRevivalRound > -1 && currentMatchState.Position == settings.OpponentStadiumRevivalRound + 2 && rnd.Next(5) < 3)
            {
                powerMove = powerMoves.Find(x => x.Type == matchStates[settings.OpponentStadiumRevivalRound].CPUPowerMove);
            }
            else if (settings.StadiumRevivalRound > -1 && currentMatchState.Position == settings.StadiumRevivalRound + 2 && powerMoves.Any(x => x.Type == PowerMoveType.PowerStop) && rnd.Next(2) == 0)
            {
                powerMove = powerMoves.Find(x => x.Type == PowerMoveType.PowerStop);
            }
            else if (settings.OpponentStadiumHasLuckUp && luckPowerMoves.Count > 0 && rnd.Next(3) < 2)
            {
                powerMove = luckPowerMoves[rnd.Next(luckPowerMoves.Count)];
            }
            else if (currentMatchState.Position >= 2 && currentMatchState.CPUEnhancerId != Guid.Empty && _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == currentMatchState.CPUEnhancerId).IsFiller && powerMoves.Any(x => x.Type == PowerMoveType.EnhancerSwap) && rnd.Next(3) < 2)
            {
                powerMove = powerMoves.Find(x => x.Type == PowerMoveType.EnhancerSwap);
            }
            else if (currentMatchState.CPUEnhancerId != Guid.Empty && _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == currentMatchState.CPUEnhancerId).SpecialType == EnhancerSpecialType.Dark && powerMoves.Any(x => x.Type == PowerMoveType.DoubledEnhancers) && rnd.Next(3) < 2)
            {
                powerMove = powerMoves.Find(x => x.Type == PowerMoveType.DoubledEnhancers);
            }
            else if (powerMoves.Count > 0)
            {
                powerMove = powerMoves[rnd.Next(powerMoves.Count)];

                if (powerMove.Type == PowerMoveType.AllOutAttack && !settings.OpponentStadiumHasLuckUp && rnd.Next(2) == 1)
                {
                    return PowerMoveType.AllOutAttackNegative;
                }
                if (powerMove.Type == PowerMoveType.LadyLuck && !settings.OpponentStadiumHasLuckUp && rnd.Next(2) == 1)
                {
                    return PowerMoveType.LadyLuckNegative;
                }
                if (powerMove.Type == PowerMoveType.BlockersBane && !settings.OpponentStadiumHasLuckUp && rnd.Next(4) == 1)
                {
                    return PowerMoveType.BlockersBaneNegative;
                }
                if (powerMove.Type == PowerMoveType.BigShot && !settings.OpponentStadiumHasLuckUp && rnd.Next(2) == 1)
                {
                    return PowerMoveType.BigShotNegative;
                }
            }

            return powerMove?.Type ?? PowerMoveType.None;
        }

        public MatchState EnhanceCPULogic(User user, List<MatchState> matchStates, MatchState matchState, Settings settings)
        {
            if (!matchState.IsCPU)
            {
                return matchState;
            }

            if (matchState.CPUPowerMove == PowerMoveType.SilencedEnhancers || matchState.CPUPowerMove == PowerMoveType.SelfDestruct || settings.OpponentStadiumIsConfused || (matchState.IsCPUChallenge && settings.StadiumChallenge == CPUChallenge.NoEnhancers))
            {
                matchState.CPUEnhancerId = Guid.Empty;
            }

            if (matchState.CPUPowerMove != PowerMoveType.EnhancerSwap)
            {
                if (matchState.CPUEnhancerId != Guid.Empty && matchStates.Count(x => x.Status == MatchStatus.Won) == Helper.GetMaxDeckSize() / 2)
                {
                    Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                    if (enhancer.IsFiller)
                    {
                        matchState.CPUEnhancerId = Guid.Empty;
                    }
                }

                if (matchState.CPUEnhancerId != Guid.Empty && settings.OpponentStadiumSelfDestructMultiplier > 0 && matchState.Position > 0 && matchStates[matchState.Position - 1].CPUPowerMove == PowerMoveType.SelfDestruct)
                {
                    Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                    if (enhancer.IsFiller)
                    {
                        matchState.CPUEnhancerId = Guid.Empty;
                    }
                }

                if (matchState.CPUEnhancerId != Guid.Empty && settings.OpponentFillerPercent > 0)
                {
                    Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                    if (enhancer.IsFiller && enhancer.Amount <= settings.OpponentFillerPercent)
                    {
                        matchState.CPUEnhancerId = Guid.Empty;
                    }
                }
            }

            if (matchState.CPUEnhancerId != Guid.Empty && matchState.CPUPowerMove == PowerMoveType.EnhancerSwap)
            {
                Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                if (!enhancer.IsFiller)
                {
                    matchState.CPUEnhancerId = Guid.Empty;
                }
            }

            if (settings.StadiumIsConfused)
            {
                if (matchState.CPUEnhancerId != Guid.Empty)
                {
                    List<Enhancer> enhancers = _itemsRepository.GetAllEnhancers(true);
                    Enhancer enhancer = enhancers.Find(x => x.Id == matchState.CPUEnhancerId);
                    if (enhancer.SpecialType == EnhancerSpecialType.Blocker || enhancer.SpecialType == EnhancerSpecialType.Predictor || (enhancer.IsFiller && matchState.CPUPowerMove != PowerMoveType.EnhancerSwap))
                    {
                        bool foundNewEnhancer = false;
                        foreach (Guid enhancerId in matchStates.Select(x => x.CPUEnhancerId))
                        {
                            enhancer = enhancers.Find(x => x.Id == enhancerId);
                            if (enhancer != null && enhancer.SpecialType != EnhancerSpecialType.Blocker && enhancer.SpecialType != EnhancerSpecialType.Predictor && !enhancer.IsFiller)
                            {
                                matchState.CPUEnhancerId = enhancer.Id;
                                foundNewEnhancer = true;
                                break;
                            }
                        }
                        if (!foundNewEnhancer)
                        {
                            matchState.CPUEnhancerId = Guid.Empty;
                        }
                    }
                }
            }

            if (matchState.CPUPowerMove == PowerMoveType.DoubledEnhancers && matchState.CPUEnhancerId == Guid.Empty)
            {
                matchState.CPUPowerMove = PowerMoveType.None;
            }

            if ((matchState.CPUPowerMove == PowerMoveType.DoubledEnhancers || matchState.CPUPowerMove == PowerMoveType.BlockersBane || matchState.CPUPowerMove == PowerMoveType.BlockersBaneNegative || matchState.CPUPowerMove == PowerMoveType.CounterStop) && matchState.CPUEnhancerId != Guid.Empty)
            {
                Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                if (enhancer.SpecialType == EnhancerSpecialType.Blocker)
                {
                    matchState.CPUPowerMove = PowerMoveType.None;
                }
            }

            if (matchState.CPUEnhancerId != Guid.Empty && matchState.CPUPowerMove != PowerMoveType.DoubledEnhancers && matchState.CPUPowerMove != PowerMoveType.EnhancerSwap && matchState.CPUPowerMove != PowerMoveType.PoisonTouch && matchState.CPUPowerMove != PowerMoveType.PowerStop && matchState.CPUPowerMove != PowerMoveType.LuckUp && matchState.CPUPowerMove != PowerMoveType.Revival && matchState.CPUPowerMove != PowerMoveType.DoubleDip)
            {
                Enhancer enhancer = _itemsRepository.GetAllEnhancers(true).Find(x => x.Id == matchState.CPUEnhancerId);
                if (enhancer.IsFiller)
                {
                    matchState.CPUPowerMove = PowerMoveType.None;
                }
            }

            if (matchState.CPUPowerMove == PowerMoveType.DeckSwap || matchState.CPUPowerMove == PowerMoveType.WinningLoser)
            {
                List<Card> userDeck = _itemsRepository.GetCards(user, true);
                Card card = _itemsRepository.GetAllCards().Find(x => x.Id == matchState.CPUCardId);
                if (card.Amount == 100)
                {
                    matchState.CPUPowerMove = PowerMoveType.None;
                }
                else if (userDeck.All(x => x.Color == userDeck.First().Color))
                {
                    if (userDeck.First().Color < card.Color)
                    {
                        matchState.CPUPowerMove = PowerMoveType.None;
                    }
                }
            }

            return matchState;
        }

        public bool CanChallengeCheckpointCPU(User user, List<Accomplishment> accomplishments, CPU cpu, out DateTime date, out string reason, out string buttonText, out string buttonUrl)
        {
            date = DateTime.MinValue;
            reason = string.Empty;
            buttonText = string.Empty;
            buttonUrl = string.Empty;

            if (!cpu.IsCheckpoint)
            {
                return true;
            }

            int level = cpu.Level + 1;
            switch (level)
            {
                case 5:
                    List<AccomplishmentId> fairAccomplishments = new List<AccomplishmentId>
                    {
                        AccomplishmentId.ColorWarsWinner,
                        AccomplishmentId.CrystalBallWon,
                        AccomplishmentId.HighLowPotEarned,
                        AccomplishmentId.MatchingColorsMatch,
                        AccomplishmentId.PrizeCupTookPot,
                        AccomplishmentId.SpinSuccessPositive,
                        AccomplishmentId.StreakCardsWin,
                        AccomplishmentId.ImprovedDeckCardSwap,
                        AccomplishmentId.TookOffer,
                        AccomplishmentId.GotTopOfferPrize,
                        AccomplishmentId.FoundTreasureDive,
                        AccomplishmentId.OnTheHour,
                        AccomplishmentId.DeoJackWon,
                        AccomplishmentId.TopCounterWon,
                        AccomplishmentId.DoubtItWinner,
                        AccomplishmentId.LuckyGuessWinner
                    };
                    List<Accomplishment> fairAccomplishmentsEarned = accomplishments.FindAll(x => fairAccomplishments.Any(y => x.Id == y));
                    if (fairAccomplishmentsEarned.Count < 3)
                    {
                        reason = $"To challenge your first Checkpoint CPU, you'll need to complete at least 3 games at the Fair! You've completed {fairAccomplishmentsEarned.Count}/3 so far.";
                        buttonText = "Head to the Fair!";
                        buttonUrl = "/Fair";
                        return false;
                    }
                    reason = "Complete at least 3 games at the Fair";
                    date = fairAccomplishmentsEarned.OrderBy(x => x.ReceivedDate).Take(3).Last().ReceivedDate;
                    return true;
                case 10:
                    if (!accomplishments.Any(x => x.Id == AccomplishmentId.ShowroomAccess))
                    {
                        reason = $"To challenge the second Checkpoint CPU, you'll need a Showroom!";
                        buttonText = "Head to the Showroom Store!";
                        buttonUrl = "/BuyersClub/Showroom";
                        return false;
                    }
                    List<Item> showroomArtCards = _profileRepository.GetShowroomItems(user).FindAll(x => x.Category == ItemCategory.Art);
                    if (showroomArtCards.Count == 0)
                    {
                        reason = $"To challenge {cpu.Name}, you'll need to have at least 1 official Art Card in your Showroom!";
                        buttonText = "Head to your Showroom!";
                        buttonUrl = "/Profile/Showroom";
                        return false;
                    }
                    reason = "Have at least 1 Art Card in your Showroom";
                    date = showroomArtCards.OrderBy(x => x.AddedDate).First().AddedDate;
                    return true;
                case 15:
                    List<Companion> companions = _companionsRepository.GetCompanions(user);
                    if (companions.Count == 0)
                    {
                        reason = $"To challenge {cpu.Name}, you'll need to have at least 1 active Companion on your profile.";
                        return false;
                    }
                    reason = "Have at least 1 active Companion on your profile";
                    date = companions.OrderBy(x => x.CreatedDate).First().CreatedDate;
                    return true;
                case 20:
                    List<Card> deck = _itemsRepository.GetCards(user, true);
                    if (deck.Count == 0 || !deck.All(x => x.Color == deck.First().Color))
                    {
                        reason = $"To challenge the fourth Checkpoint CPU, all the cards in your Deck need to be the same color.";
                        buttonText = "Head to your bag!";
                        buttonUrl = "/Items";
                        return false;
                    }
                    reason = "All the cards in your Deck are the same color";
                    date = deck.OrderBy(x => x.AddedDate).Last().AddedDate;
                    return true;
                case 25:
                    if (!accomplishments.Any(x => x.Id == AccomplishmentId.MazeUnlocked))
                    {
                        reason = $"To challenge the fifth Checkpoint CPU, you'll need to unlock the Maze. How can that be done?";
                        buttonText = "Head to the Maze!";
                        buttonUrl = "/SwapDeck/Maze";
                        return false;
                    }
                    reason = "Unlock the Maze";
                    date = accomplishments.Find(x => x.Id == AccomplishmentId.MazeUnlocked).ReceivedDate;
                    return true;
                case 30:
                    Bank bank = _bankRepository.GetBank(user);
                    if (bank == null)
                    {
                        reason = $"To challenge the sixth Checkpoint CPU, you'll need to open an account at the Bank.";
                        buttonText = "Head to the Bank!";
                        buttonUrl = "/Bank";
                        return false;
                    }
                    if (bank.Points < 100000)
                    {
                        reason = $"To challenge {cpu.Name}, you'll need to have at least {Helper.GetFormattedPoints(100000)} in your Bank account! You have {Helper.GetFormattedPoints(bank.Points)}.";
                        buttonText = "Head to the Bank!";
                        buttonUrl = "/Bank";
                        return false;
                    }
                    reason = $"Have {Helper.GetFormattedPoints(100000)} in your Bank account";
                    date = accomplishments.Find(x => x.Id == AccomplishmentId.SmallFortuneBank).ReceivedDate;
                    return true;
                case 35:
                    List<Checklist> checklist = _checklistRepository.GetChecklist(accomplishments);
                    if (checklist != null && checklist.First().Rank < ChecklistRank.Expert)
                    {
                        reason = $"You can't challenge the final Checkpoint CPU until you reach the Expert rank. You'll need to complete more of your checklist!";
                        buttonText = "See your checklist!";
                        buttonUrl = "/?highlightChecklist=true";
                        return false;
                    }
                    Checklist recentChecklist = _checklistRepository.GetAllEarnedChecklist(accomplishments).FindAll(x => x.Rank == ChecklistRank.Expert).OrderBy(x => x.EarnedDate).FirstOrDefault();
                    if (checklist == null)
                    {
                        recentChecklist = _checklistRepository.GetLastEarnedChecklist(accomplishments);
                    }
                    reason = $"Reach the Expert rank in your Checklist";
                    date = recentChecklist.EarnedDate;
                    return true;
            }

            return false;
        }

        public CPU GetRightSideUpGuard()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0);
            List<Enhancer> darkEnhancers = allEnhancers.FindAll(x => x.IsExclusive && x.SpecialType == EnhancerSpecialType.Dark);
            Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.Guard.GetDisplayName(),
                Deck = new List<Card> { allCards[95], allCards[99], allCards[95], allCards[94], allCards[97] },
                Enhancers = new List<Enhancer> { darkEnhancers[3], darkEnhancers[4], blocker, blocker, allEnhancers.Last() },
                Reward = 150000,
                IsSpecial = true,
                ImageUrl = "CPU/Guard.png"
            };

            return Shuffle(cpu);
        }

        public CPU GetShowroomOwner()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0 && x.SpecialType != EnhancerSpecialType.Luck);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.ShowroomOwner.GetDisplayName(),
                Deck = new List<Card> { allCards[15], allCards[11], allCards[15], allCards[12], allCards[14] },
                Enhancers = new List<Enhancer> { allEnhancers[2], allEnhancers[7], allEnhancers[3], allEnhancers[4], allEnhancers[5] },
                Reward = 5000,
                IsSpecial = true,
                ImageUrl = "CPU/ShowroomOwner.png"
            };

            return Shuffle(cpu);
        }

        public CPU GetPhantomSpirit()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0 && x.SpecialType != EnhancerSpecialType.Luck && !x.IsFiller);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.PhantomSpirit.GetDisplayName(),
                Deck = new List<Card> { allCards[31], allCards[32], allCards[30], allCards[30], allCards[35] },
                Enhancers = new List<Enhancer> { allEnhancers[8], allEnhancers[10], allEnhancers[8], allEnhancers[4], allEnhancers[12] },
                Reward = 5000,
                IsSpecial = true
            };

            return Shuffle(cpu);
        }

        public CPU GetEvena()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0 && x.SpecialType != EnhancerSpecialType.Luck && !x.IsFiller);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.Evena.GetDisplayName(),
                Deck = new List<Card> { allCards[51], allCards[49], allCards[53], allCards[55], allCards[55] },
                Enhancers = new List<Enhancer> { allEnhancers[15], allEnhancers[14], allEnhancers[3], allEnhancers[14], allEnhancers[9] },
                Reward = 5000,
                IsSpecial = true
            };

            foreach (Card card in cpu.Deck)
            {
                card.Theme = ItemTheme.Neon;
            }

            return Shuffle(cpu);
        }

        public CPU GetOddna()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0 && x.SpecialType != EnhancerSpecialType.Luck && !x.IsFiller);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.Oddna.GetDisplayName(),
                Deck = new List<Card> { allCards[50], allCards[48], allCards[54], allCards[56], allCards[56] },
                Enhancers = new List<Enhancer> { allEnhancers[15], allEnhancers[14], allEnhancers[3], allEnhancers[14], allEnhancers[9] },
                Reward = 5000,
                IsSpecial = true
            };

            foreach (Card card in cpu.Deck)
            {
                card.Theme = ItemTheme.Neon;
            }

            return Shuffle(cpu);
        }

        public CPU GetPhantomBoss()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0);
            Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);
            Enhancer filler = allEnhancers.FindAll(x => x.IsFiller).Last();

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.PhantomBoss.GetDisplayName(),
                Deck = new List<Card> { allCards.Last(), allCards[94], allCards[95], allCards[98], allCards.Last() },
                Enhancers = new List<Enhancer> { filler, allEnhancers[allEnhancers.Count - 3], blocker, allEnhancers[allEnhancers.Count - 2], allEnhancers[allEnhancers.Count - 3] },
                Reward = 250000,
                IsSpecial = true,
                Intensity = 2
            };

            foreach (Card card in cpu.Deck)
            {
                card.Theme = ItemTheme.Astral;
            }

            return Shuffle(cpu);
        }

        public CPU GetUnknown()
        {
            List<Card> allCards = _itemsRepository.GetAllCards();
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0);
            Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);
            Enhancer filler = allEnhancers.FindAll(x => x.IsFiller).Last();

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.Unknown.GetDisplayName(),
                Deck = new List<Card> { allCards.Last(), allCards.Last(), allCards.Last(), allCards.Last(), allCards.Last() },
                Enhancers = new List<Enhancer> { filler, allEnhancers.Last(), blocker, allEnhancers.Last(), allEnhancers[allEnhancers.Count - 2] },
                Reward = 500000,
                Intensity = 2,
                ImageUrl = "CPU/Unknown.png",
                IsSpecial = true
            };

            foreach (Card card in cpu.Deck)
            {
                card.Theme = ItemTheme.Astral;
            }

            return Shuffle(cpu);
        }

        public CPU GetTowerCPU(int floor)
        {
            floor++;
            Random rnd = new Random();
            List<Card> allCards = _itemsRepository.GetAllCards();
            allCards = allCards.FindAll(x => x.Amount >= 40 && x.Points >= Math.Min(floor * 30000, allCards.Last().Points) && x.Points <= (floor * 50000));
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0).ToList();
            Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);
            Enhancer filler = floor < 10 ? allEnhancers.FindAll(x => x.IsFiller).TakeLast(3).ToList()[rnd.Next(3)] : allEnhancers.Last();
            Enhancer predictor = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Predictor);
            allEnhancers = allEnhancers.FindAll(x => !x.IsFiller && x.Points >= Math.Min(floor * 15000, allEnhancers.Last().Points) && x.Points <= (floor * 50000));
            allEnhancers.Add(blocker);
            allEnhancers.Add(predictor);

            CPU cpu = new CPU
            {
                Name = $"Floor {Helper.FormatNumber(floor)}",
                Deck = new List<Card> { allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)] },
                Enhancers = new List<Enhancer> { allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], filler },
                Reward = floor <= 7 ? (int)Math.Pow(2, floor) * 250 : 40000,
                Intensity = floor / 25 + 1,
                ImageUrl = "Locations/Tower.png",
                IsSpecial = true
            };

            if (floor >= 10 || rnd.Next(2) == 1)
            {
                List<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>().ToList();
                themes.Remove(ItemTheme.Club);
                ItemTheme theme = floor >= 25 ? ItemTheme.Astral : themes[rnd.Next(themes.Count)];
                foreach (Card card in cpu.Deck)
                {
                    card.Theme = theme;
                }
            }

            return Shuffle(cpu);
        }


        public CPU GetChallengerDomeCPU(Settings settings)
        {
            int matchLevel = settings.MatchLevel + 1;
            Random rnd = new Random();
            List<Card> allCards = _itemsRepository.GetAllCards();
            allCards = allCards.FindAll(x => x.Points >= Math.Min(matchLevel * 200, allCards.Last().Points) && x.Points <= (matchLevel * 3000));
            List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0).ToList();
            Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);
            Enhancer filler = matchLevel < 30 ? allEnhancers.FindAll(x => x.IsFiller).TakeLast(3).ToList()[rnd.Next(3)] : allEnhancers.Last();
            Enhancer predictor = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Predictor);
            allEnhancers = allEnhancers.FindAll(x => x.Points >= Math.Min(matchLevel * 200, allEnhancers.Last().Points) && x.Points <= (matchLevel * 4000));
            allEnhancers.Add(blocker);
            allEnhancers.Add(filler);
            allEnhancers.Add(predictor);

            CPU cpu = new CPU
            {
                Name = SpecialOpponents.ChallengerDome.GetDisplayName(),
                Deck = new List<Card> { allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)], allCards[rnd.Next(allCards.Count)] },
                Enhancers = new List<Enhancer> { allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)], allEnhancers[rnd.Next(allEnhancers.Count)] },
                Reward = Math.Min(5000, 500 + settings.ChallengerDomeStreak * 100),
                ImageUrl = "Locations/ChallengerDome.png",
                IsSpecial = true
            };

            if (matchLevel >= 30 || rnd.Next(5) == 1)
            {
                List<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>().ToList();
                themes.Remove(ItemTheme.Club);
                ItemTheme theme = matchLevel >= 50 ? ItemTheme.Astral : themes[rnd.Next(themes.Count)];
                foreach (Card card in cpu.Deck)
                {
                    card.Theme = theme;
                }
            }

            return Shuffle(cpu);
        }

        public CPU GetBeyondWorldCPU(User user, Settings settings)
        {
            CPU cpu = null;
            Random rnd = new Random();
            if (settings.BeyondWorldLevel == 1)
            {
                List<Card> cards = _itemsRepository.GetCards(user, true);
                List<Enhancer> enhancers = _itemsRepository.GetEnhancers(user).OrderBy(x => x.Points).ToList();
                if (enhancers.Count == 0)
                {
                    enhancers = new List<Enhancer> { null, null, null, null, null };
                }

                cpu = new CPU
                {
                    Name = $"Mirror {user.Username.First().ToString().ToUpper() + user.Username.Substring(1)}",
                    Deck = new List<Card> { cards[0], cards[1], cards[2], cards[3], cards[4] },
                    Enhancers = new List<Enhancer> { enhancers[rnd.Next(enhancers.Count)], enhancers[rnd.Next(enhancers.Count)], enhancers.Last(), enhancers.Last(), enhancers.Count > 1 ? enhancers[enhancers.Count - 2] : enhancers.Last() },
                    Reward = 10000,
                    IsSpecial = true,
                    ImageUrl = user.DisplayPicUrl
                };

                return Shuffle(cpu);
            }

            if (settings.BeyondWorldLevel == 3)
            {
                List<Card> allCards = _itemsRepository.GetAllCards();
                List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0 && x.Points >= 5000);
                Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);
                Enhancer filler = allEnhancers.FindAll(x => x.IsFiller).Last();

                cpu = new CPU
                {
                    Name = SpecialOpponents.Guardian.GetDisplayName(),
                    Deck = new List<Card> { allCards[33], allCards[50], allCards[67], allCards[84], allCards[99] },
                    Enhancers = new List<Enhancer> { allEnhancers[rnd.Next(allEnhancers.Count)], filler, allEnhancers[rnd.Next(allEnhancers.Count)], blocker, blocker },
                    Reward = 15000 * (settings.SorceressGuardiansDefeated + 1),
                    Intensity = settings.SorceressGuardiansDefeated + 1,
                    IsSpecial = true
                };

                foreach (Card card in cpu.Deck)
                {
                    card.Theme = ItemTheme.Neon;
                }

                return Shuffle(cpu);
            }

            if (settings.BeyondWorldLevel == 5)
            {
                List<Card> allCards = _itemsRepository.GetAllCards();
                List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true).FindAll(x => x.TotalUses > 0);
                Enhancer blocker = allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker);

                cpu = new CPU
                {
                    Name = SpecialOpponents.BeyondCreature.GetDisplayName(),
                    Deck = new List<Card> { allCards.Last(), allCards.Last(), allCards.Last(), allCards.Last(), allCards.Last() },
                    Enhancers = new List<Enhancer> { blocker, blocker, blocker, blocker, blocker },
                    Reward = 150000,
                    Intensity = 2,
                    IsSpecial = true
                };

                foreach (Card card in cpu.Deck)
                {
                    card.Theme = ItemTheme.Astral;
                }

                return Shuffle(cpu);
            }

            return cpu;
        }

        private CPU Shuffle(CPU cpu)
        {
            cpu.Deck.Shuffle();
            do
            {
                cpu.Enhancers.Shuffle();
            }
            while (!cpu.Enhancers.All(x => x != null && x.IsFiller) && (cpu.Enhancers.LastOrDefault()?.IsFiller ?? false));

            return cpu;
        }

        private PowerMoveType GetTutorialPowerMoves(List<MatchState> matchStates)
        {
            MatchState currentMatchState = matchStates?.Find(x => x.Status == MatchStatus.NotPlayed);
            if (currentMatchState.Position == 0)
            {
                return PowerMoveType.FlowingPower;
            }
            if (currentMatchState.Position == 1)
            {
                if (currentMatchState.PowerMove == PowerMoveType.DoubleDip)
                {
                    return PowerMoveType.EnhancerSwap;
                }
                return PowerMoveType.SelfDestruct;
            }
            if (currentMatchState.Position == 2)
            {
                return PowerMoveType.EnhancerSwap;
            }
            if (currentMatchState.Position == 3)
            {
                return PowerMoveType.PowerStop;
            }
            if (currentMatchState.Position == 4)
            {
                return PowerMoveType.LadyLuckNegative;
            }

            return PowerMoveType.SilencedEnhancers;
        }

        private PowerMoveType GetSecondChallengerPowerMoves(List<MatchState> matchStates)
        {
            MatchState currentMatchState = matchStates?.Find(x => x.Status == MatchStatus.NotPlayed);
            if (currentMatchState.Position == 0)
            {
                return PowerMoveType.SelfDestruct;
            }
            if (currentMatchState.Position == 1)
            {
                return PowerMoveType.MirrorMirror;
            }
            if (currentMatchState.Position == 2)
            {
                return PowerMoveType.EnhancerSwap;
            }
            if (currentMatchState.Position == 3)
            {
                return PowerMoveType.DoubleDip;
            }
            if (currentMatchState.Position == 4)
            {
                return PowerMoveType.LadyLuckNegative;
            }

            return PowerMoveType.FlowingPower;
        }

        private PowerMoveType GetThirdChallengerPowerMoves(List<MatchState> matchStates)
        {
            MatchState currentMatchState = matchStates?.Find(x => x.Status == MatchStatus.NotPlayed);
            if (currentMatchState.Position == 0)
            {
                return PowerMoveType.EnhancerSwap;
            }
            if (currentMatchState.Position == 1)
            {
                return PowerMoveType.LadyLuckNegative;
            }
            if (currentMatchState.Position == 2)
            {
                return PowerMoveType.MirrorMirror;
            }
            if (currentMatchState.Position == 3)
            {
                return PowerMoveType.DoubleDip;
            }
            if (currentMatchState.Position == 4)
            {
                return PowerMoveType.EnhancerSwap;
            }

            return PowerMoveType.SilencedEnhancers;
        }
    }
}
