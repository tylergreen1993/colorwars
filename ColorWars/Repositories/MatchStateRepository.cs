﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class MatchStateRepository : IMatchStateRepository
    {
        private IMatchStatePersistence _matchStatePersistence;

        public MatchStateRepository(IMatchStatePersistence matchStatePersistence)
        {
            _matchStatePersistence = matchStatePersistence;
        }

        public List<MatchState> GetMatchStates(User user, bool getInactive = false, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _matchStatePersistence.GetAllMatchStatesWithUsername(user.Username, getInactive, isCached);
        }

        public List<MatchState> UpdateMatchState(User user, MatchState matchState, bool returnList = true)
        {
            if (user == null || matchState == null)
            {
                return null;
            }

            if (_matchStatePersistence.UpdateMatchStateForId(matchState))
            {
                return returnList ? GetMatchStates(user) : null;
            }

            return null;
        }

        public bool AddMatchState(User user, MatchState matchState)
        {
            if (user == null || matchState == null)
            {
                return false;
            }

            return _matchStatePersistence.AddMatchState(matchState);
        }

        public MatchWaitlist GetMatchWaitlist(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _matchStatePersistence.GetMatchWaitlistWithUsername(user.Username);
        }

        public List<MatchWaitlist> GetMatchWaitlists(User user, bool isElite)
        {
            if (user == null)
            {
                return null;
            }

            return _matchStatePersistence.GetMatchWaitlists(user.Username, isElite);
        }

        public MatchWaitlist AddOrUpdateMatchWaitlist(User user, int points, bool isElite, string challengeUser, Guid tourneyMatchId)
        {
            if (user == null)
            {
                return null;
            }

            return _matchStatePersistence.AddOrUpdateMatchWaitlist(user.Username, points, isElite, challengeUser, tourneyMatchId);
        }

        public bool DeleteMatchWaitlist(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _matchStatePersistence.DeleteFromMatchWaitlistWithUsername(user.Username);
        }

        public bool DeleteMatchState(User user, string opponent, bool isCPU, bool isExpired)
        {
            if (user == null)
            {
                return false;
            }

            return _matchStatePersistence.DeleteFromMatchStateWithUsername(user.Username, opponent, isCPU, isExpired);
        }

        public bool IsInMatch(User user, bool includeWaitlist = true, bool isUser = true)
        {
            List<MatchState> matchStates = GetMatchStates(user, false, isUser);

            if (matchStates.Count == 0)
            {
                if (includeWaitlist)
                {
                    MatchWaitlist matchWaitlist = GetMatchWaitlist(user);
                    return matchWaitlist != null;
                }
                return false;
            }

            MatchStatus finalStatus = GetFinalStatus(matchStates.Select(x => x.Status).ToList());
            if (finalStatus == MatchStatus.Won || finalStatus == MatchStatus.Lost)
            {
                return false;
            }

            return matchStates.Any(x => x.Status == MatchStatus.NotPlayed && (x.IsCPU || x.GetSecondsLeft() > 0));
        }

        public int GetWinsNeeded(List<MatchStatus> matchStatuses)
        {
            int maxDeckSize = Helper.GetMaxDeckSize();
            int winsNeeded = (maxDeckSize / 2) + 1;
            int drawsCount = matchStatuses.Take(maxDeckSize).ToList().FindAll(x => x == MatchStatus.Draw).Count;
            return Math.Max(1, winsNeeded - drawsCount / 2);
        }

        public MatchStatus GetFinalStatus(List<MatchStatus> matchStatuses)
        {
            if (matchStatuses.Count == 0)
            {
                return MatchStatus.NotPlayed;
            }

            int winsNeeded = GetWinsNeeded(matchStatuses);

            int winCount = matchStatuses.FindAll(x => x == MatchStatus.Won).Count;
            if (winCount == winsNeeded)
            {
                return MatchStatus.Won;
            }

            int lossCount = matchStatuses.FindAll(x => x == MatchStatus.Lost).Count;
            if (lossCount == winsNeeded)
            {
                return MatchStatus.Lost;
            }

            if (matchStatuses.FindAll(x => x != MatchStatus.NotPlayed).Count >= Helper.GetMaxDeckSize())
            {
                return MatchStatus.Draw;
            }

            return MatchStatus.NotPlayed;
        }
    }
}
