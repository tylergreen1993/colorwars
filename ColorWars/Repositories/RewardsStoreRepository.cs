﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class RewardsStoreRepository : IRewardsStoreRepository
    {
        private List<Reward> _rewards;

        public RewardsStoreRepository()
        {
            _rewards = new List<Reward>();
            PopulateRewards();
        }

        public List<Reward> GetAllRewards()
        {
            return _rewards.OrderBy(x => x.Cost).ThenBy(x => x.Name).ToList();
        }

        private void PopulateRewards()
        {
            _rewards.Add(new Reward
            {
                Id = RewardId.OneMonthClub,
                Name = $"{Resources.SiteName} Club",
                Description = $"You'll get a month of {Resources.SiteName} Club membership.",
                Category = AccomplishmentCategory.Blue,
                Cost = 50
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ExtraWizardVisit,
                Name = $"Young Wizard",
                Description = $"The Young Wizard's powers will recharge so you can visit him again.",
                Category = AccomplishmentCategory.Red,
                Cost = 10
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ExtraLibraryVisit,
                Name = $"Library",
                Description = $"You can visit the Library again if you've already visited recently.",
                Category = AccomplishmentCategory.Orange,
                Cost = 15
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.StadiumBonus,
                Name = $"Stadium Bonus",
                Description = $"You'll get a bonus at the Stadium for CPU matches for 15 minutes.",
                Category = AccomplishmentCategory.Yellow,
                Cost = 20
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ExtraSwapDeckVisit,
                Name = $"Swap Deck",
                Description = $"You can swap one of your {Resources.DeckCards} again if you already have today.",
                Category = AccomplishmentCategory.Orange,
                Cost = 15
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.RepairAllDeck,
                Name = $"Deck Repair",
                Description = $"Your entire Deck will be repaired and have a New condition.",
                Category = AccomplishmentCategory.Purple,
                Cost = 100
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ImproveDeckCard,
                Name = $"{Resources.DeckCard} Improvement",
                Description = $"Randomly improve one of your {Resources.DeckCards}.",
                Category = AccomplishmentCategory.Yellow,
                Cost = 25
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ExtraWishingStoneVisit,
                Name = $"Wishing Stone",
                Description = $"You can make another wish at the Wishing Stone if you already have recently.",
                Category = AccomplishmentCategory.Red,
                Cost = 5
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.MysteryPointsBag,
                Name = $"Mystery {Resources.Currency} Bag",
                Description = $"You'll gain a mystery amount of {Resources.Currency}.",
                Category = AccomplishmentCategory.Green,
                Cost = 35
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.MysteryStockBag,
                Name = $"Mystery Stock Bag",
                Description = $"You'll get 100 mystery stock shares.",
                Category = AccomplishmentCategory.Green,
                Cost = 30
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.ThemeChange,
                Name = $"Theme Change",
                Description = $"Randomly change one of your {Resources.DeckCards}' themes.",
                Category = AccomplishmentCategory.Blue,
                Cost = 40
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.MaxBagSize,
                Name = $"Maximum Bag Size",
                Description = $"Your maximum bag size will be increased by up to 15 items.",
                Category = AccomplishmentCategory.Purple,
                Info = $"Your bag size cannot be greater than {Helper.GetMaxBagSize()} items.",
                Cost = 75
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.DoubleBankInterest,
                Name = $"Bank Interest",
                Description = $"You can collect double your daily Bank interest next time.",
                Category = AccomplishmentCategory.Orange,
                Cost = 15
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.DoubleCPUReward,
                Name = $"Stadium Match",
                Description = $"You'll get double the reward against your next CPU Challenger.",
                Category = AccomplishmentCategory.Blue,
                Cost = 40
            });
            _rewards.Add(new Reward
            {
                Id = RewardId.UnlockPartnership,
                Category = AccomplishmentCategory.Red,
                Cost = 10,
                IsHidden = true
            });
        }
    }
}
