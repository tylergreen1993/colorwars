﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SafetyDepositRepository : ISafetyDepositRepository
    {
        private ISafetyDepositPersistence _safetyDepositPersistence;
        private ISettingsRepository _settingsRepository;

        public SafetyDepositRepository(ISafetyDepositPersistence safetyDepositPersistence, ISettingsRepository settingsRepository)
        {
            _safetyDepositPersistence = safetyDepositPersistence;
            _settingsRepository = settingsRepository;
        }

        public List<Item> GetSafetyDepositItems(User user, bool isCached = true)
        {
            return _safetyDepositPersistence.GetSafetyDepositItemsWithUsername(user.Username, isCached);
        }

        public bool AddSafetyDepositItem(User user, Item item)
        {
            List<Item> safetyDepositItems = GetSafetyDepositItems(user).OrderBy(x => x.Position).ToList();
            int newPosition = safetyDepositItems.LastOrDefault()?.Position + 1 ?? 0;
            item.Username = user.Username;
            return _safetyDepositPersistence.AddSafetyDepositItem(item, newPosition);
        }

        public bool UpdateSafetyDepositItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _safetyDepositPersistence.UpdateSafetyDepositItemWithSelectionId(item.SelectionId, item.Position);
        }

        public bool DeleteSafetyDepositItem(User user, Item item)
        {
            return _safetyDepositPersistence.DeleteSafetyDepositItem(item.SelectionId, user.Username);
        }

        public bool RepairAllItems(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _safetyDepositPersistence.RepairAllSafetyDepositItems(user.Username);
        }
    }
}
