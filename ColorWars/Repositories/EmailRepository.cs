﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISettingsRepository _settingsRepository;

        public EmailRepository(IHttpContextAccessor httpContextAccessor, ISettingsRepository settingsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _settingsRepository = settingsRepository;
        }

        public async Task<bool> SendEmail(User user, string subject, string title, string body, string buttonUrl, bool overrideAllowNotifications = false, string domain = null, string fromOverride = null)
        {
            if (user == null)
            {
                return false;
            }

            if (user.IsSuspended || user.InactiveType != InactiveAccountType.None || (!overrideAllowNotifications && !user.AllowNotifications))
            {
                return false;
            }

            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(user.EmailAddress);
                mail.From = new MailAddress(ConfigurationManager.AppSettings.Get("EmailAddress"), Resources.SiteName);
                mail.Subject = subject + $" - {Resources.SiteName}";
                mail.Body = GetEmailMessage(user, title, body, buttonUrl, domain, !overrideAllowNotifications, fromOverride);
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings.Get("EmailHost"),
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("EmailUsername"), ConfigurationManager.AppSettings.Get("EmailPassword")),
                    Port = 587,
                    EnableSsl = true
                };

                await smtp.SendMailAsync(mail);
                return true;
            }
            catch (Exception e)
            {
                Log.Debug(e.Message);
                return false;
            }
        }

        public async Task<bool> SendEmail(string emailAddress, string subject, string title, string body, string buttonUrl, bool overrideAllowNotifications = false, string domain = null, string fromOverride = null)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                return false;
            }

            User user = new User
            {
                EmailAddress = emailAddress,
                AllowNotifications = true
            };

            return await SendEmail(user, subject, title, body, buttonUrl, overrideAllowNotifications, domain, fromOverride);
        }

        public async Task<bool> SendEmailVerification(User user)
        {
            if (user == null)
            {
                return false;
            }

            Settings settings = _settingsRepository.GetSettings(user);
            if (settings.VerifiedEmail)
            {
                return false;
            }

            settings.EmailVerificationCode = Guid.NewGuid().ToString();
            _settingsRepository.SetSetting(user, nameof(settings.EmailVerificationCode), settings.EmailVerificationCode);

            string subject = "Verify your email";
            string title = "Email Verification";
            string body = "Please click the button below to verify your email so we know that it's you.";
            string buttonUrl = $"/VerifyEmail?code={settings.EmailVerificationCode}";

            await SendEmail(user, subject, title, body, buttonUrl, true);

            return true;
        }

        private string GetEmailMessage(User user, string title, string body, string buttonUrl, string domain, bool showdisableNotificationsText, string fromOverride)
        {
            string footer = string.IsNullOrEmpty(fromOverride) ? $"The {Resources.SiteName} Team" : fromOverride;
            body = $"Hey {(string.IsNullOrEmpty(user.GetFriendlyName()) ? "there" : user.GetFriendlyName())},<p>" + body;
            body += $"<p>Take care,<p>{footer}";
            HttpContext httpContext = _httpContextAccessor.HttpContext;
            string urlPrefix = string.IsNullOrEmpty(domain) ? Helper.GetDomain(httpContext) : domain;
            buttonUrl = buttonUrl.Replace("+", "%2B");
            buttonUrl = $"{urlPrefix}{HttpUtility.HtmlEncode(buttonUrl)}";
            string contactUrl = $"{urlPrefix}/Contact";
            string disableNotificationsText = showdisableNotificationsText && !string.IsNullOrEmpty(user.GetFriendlyName()) ? $"<p style='font-size: 10px;'>You received this email because you turned on email notifications. <a href='{urlPrefix}/Preferences#email-notifications' target='_blank'>Disable email notifications</a></p>" : "";
            string imageUrl = Helper.GetImageUrl("Global/Logo.png");
            string logoUrl = Helper.GetImageUrl("Global/Logo_Full.png");
            string fullImageUrl = imageUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase) ? imageUrl : $"{urlPrefix}{imageUrl}";
            string fullLogoUrl = imageUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase) ? logoUrl : $"{urlPrefix}{logoUrl}";
            string message = $"<body style='background-color: #f4f4f4'><table width='100%'><tbody><tr><td><table align='center' style='background-color: white; color: black; padding: 20px; border: 3px solid #337ab7; border-radius:3px;'><tr align='center' style='padding-bottom: 25px; background-color: #337ab7; background-image: radial-gradient(circle, #337ab7 38.5%, #11c6d3);'><td style='padding: 10px; border-radius: 3px;'><img src='{fullImageUrl}' width='45px' height='45' style='filter: drop-shadow(1px 1px 1px rgba(0,0,0,0.25));'><img src='{fullLogoUrl}' width='181px' height='35' style='margin-left: 5px; filter: drop-shadow(1px 1px 1px rgba(0,0,0,0.25));'><h2 style='margin-top: 18px; margin-bottom: 0px; color: white; filter: drop-shadow(1px 1px 1px rgba(0,0,0,0.25));'>{title}</h2></td></tr><tr><td align='center' style='padding-top: 15px; padding-bottom: 15px;'><span style='font-size: 14.5px;'>{body}</span></td></tr><tr><td bgcolor='#337ab7' style='padding: 12px 18px 12px 18px; border-radius: 3px;' align='center'><a href='{buttonUrl}' target='_blank' style='font-size: 15px; color: #ffffff; text-decoration: none; display: inline-block'><b>Head to {Resources.SiteName}!</b></a></td></tr><tr><td align='center' style='padding-top: 15px;'><p style='font-size: 10px;'>Can't click the button? Go to this link instead: <a href='{buttonUrl}' target='_blank'>{buttonUrl}</a></p><p style='font-size: 10px;'>Have a question? <a href='{contactUrl}' target='_blank'>Contact Us</a></p>{disableNotificationsText}</td></tr></table></td></tr></tbody></table></body>";
            return message;
        }
    }
}
