﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ClubRepository : IClubRepository
    {
        private IClubPersistence _clubPersistence;
        private IItemsRepository _itemsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;

        public ClubRepository(IClubPersistence clubPersistence, IItemsRepository itemsRepository, IAccomplishmentsRepository accomplishmentsRepository)
        {
            _clubPersistence = clubPersistence;
            _itemsRepository = itemsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
        }

        public ClubMembership GetCurrentMembership(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<ClubMembership> clubMemberships = GetAllClubMemberships(user, isCached).FindAll(x => x.EndDate > DateTime.UtcNow && x.StartDate <= DateTime.UtcNow);
            return clubMemberships?.FirstOrDefault();
        }

        public Item GetIncubatorItem(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _clubPersistence.GetIncubatorItemWithUsername(user.Username, isCached);
        }

        public List<ClubMembership> GetAllClubMemberships(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _clubPersistence.GetAllClubMembershipsWithUsername(user.Username, isCached);
        }

        public List<Item> GetUnclaimedItems(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<ClubMembership> clubMemberships = GetAllClubMemberships(user, isCached).FindAll(x => !x.ClaimedItem && x.StartDate <= DateTime.UtcNow);

            List<Item> items = GetItems();

            List<Item> unclaimedItems = new List<Item>();
            foreach (ClubMembership clubMembership in clubMemberships)
            {
                unclaimedItems.Add(items[(clubMembership.Month - 1) % items.Count]);
            }

            return unclaimedItems;
        }

        public Item GetNextItem(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            ClubMembership clubMembership = GetCurrentMembership(user, isCached);
            List<Item> items = GetItems();

            return items[clubMembership.Month % items.Count];
        }

        public DateTime GetMembershipEndDate(User user, bool isCached = true)
        {
            if (user == null)
            {
                return DateTime.MinValue;
            }

            return GetAllClubMemberships(user, isCached)?.LastOrDefault()?.EndDate ?? DateTime.MinValue;
        }

        public bool AddClubMembership(User user, int months, DateTime startDate)
        {
            if (user == null || months <= 0 || months > 12)
            {
                return false;
            }

            DateTime endDate = startDate.AddDays(30);

            for (int i = 0; i < months; i++)
            {
                if (!_clubPersistence.AddClubMembership(user.Username, startDate, endDate))
                {
                    Log.Debug($"Something went wrong adding {Resources.SiteName} Club membership month {i + 1}/{months} for user {user.Username}.");
                    return false;
                }
                startDate = endDate;
                endDate = startDate.AddDays(30);
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.JoinedClub))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.JoinedClub);
            }

            return true;
        }

        public bool AddIncubatorItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            return _clubPersistence.AddIncubatorItem(item.SelectionId, item.Id, user.Username, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool ClaimItems(User user, bool isCached = true)
        {
            if (user == null)
            {
                return false;
            }

            List<ClubMembership> clubMemberships = GetAllClubMemberships(user, isCached).FindAll(x => !x.ClaimedItem && x.StartDate <= DateTime.UtcNow);
            foreach (ClubMembership clubMembership in clubMemberships)
            {
                clubMembership.ClaimedItem = true;
                if (!_clubPersistence.UpdateClubMembershipWithId(clubMembership))
                {
                    Log.Debug($"Unable to claim the item for {Resources.SiteName} Club membership id: {clubMembership.Id}.");
                    return false;
                }
            }

            return true;
        }

        public bool DeleteIncubatorItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _clubPersistence.DeleteIncubatorItemWithSelectionId(item.SelectionId);
        }

        private List<Item> GetItems()
        {
            List<Item> allClubItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true).FindAll(x => x.IsExclusive && x.Name.Contains("Club"));

            Item clubTheme = allClubItems.Find(x => x.Category == ItemCategory.Theme);
            List<Item> craftingCards = allClubItems.FindAll(x => x.Category == ItemCategory.Crafting);
            Item egg = allClubItems.Find(x => x.Category == ItemCategory.Egg);
            Item candy = allClubItems.Find(x => x.Category == ItemCategory.Candy);
            Item sleeve = allClubItems.Find(x => x.Category == ItemCategory.Sleeve);
            List<Item> upgraders = allClubItems.FindAll(x => x.Category == ItemCategory.Upgrader);
            Item coupon = allClubItems.Find(x => x.Category == ItemCategory.Coupon);
            Item cardPack = allClubItems.Find(x => x.Category == ItemCategory.Pack);

            List<Item> items = new List<Item> { clubTheme, craftingCards[0], egg, upgraders[0], clubTheme, candy, sleeve, craftingCards[1], clubTheme, upgraders[1], coupon, cardPack };

            return items;
        }
    }
}
