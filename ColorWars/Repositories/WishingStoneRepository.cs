﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class WishingStoneRepository : IWishingStoneRepository
    {
        private IWishingStonePersistence _wishingStonePersistence;
        private ISettingsRepository _settingsRepository;

        public WishingStoneRepository(IWishingStonePersistence wishingStonePersistence, ISettingsRepository settingsRepository)
        {
            _wishingStonePersistence = wishingStonePersistence;
            _settingsRepository = settingsRepository;
        }

        public List<Wish> GetWishes()
        {
            List<Wish> wishes = _wishingStonePersistence.GetWishes();
            wishes.Reverse();
            return wishes;
        }

        public int[] GetSecretQuarryStonePattern(User user, Settings settings)
        {
            if (user == null || settings == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(settings.SecretQuarryStonePattern))
            {
                return AddSecretQuarryStonePattern(user, settings).OrderBy(x => x).ToArray();
            }

            return settings.SecretQuarryStonePattern.Split('_').Select(x => int.Parse(x)).OrderBy(x => x).ToArray();
        }

        public bool AddWish(User user, string content, bool isGranted)
        {
            if (user == null|| string.IsNullOrEmpty(content))
            {
                return false;
            }

            return _wishingStonePersistence.AddWish(user.Username, content, isGranted);
        }

        public bool DeleteWishes(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _wishingStonePersistence.DeleteWishesWithUsername(user.Username);
        }

        private int[] AddSecretQuarryStonePattern(User user, Settings settings)
        {
            Random rnd = new Random();
            List<int> secretPositions = new List<int>();
            while (secretPositions.Count < 10)
            {
                int nextPosition = rnd.Next(30) + 1;
                if (!secretPositions.Any(x => x == nextPosition))
                {
                    secretPositions.Add(nextPosition);
                }
            }

            settings.SecretQuarryStonePattern = string.Join("_", secretPositions);
            _settingsRepository.SetSetting(user, nameof(settings.SecretQuarryStonePattern), settings.SecretQuarryStonePattern);

            return secretPositions.ToArray();
        }
    }
}
