﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class LuckyGuessRepository : ILuckyGuessRepository
    {
        private ILuckyGuessPersistence _luckyGuessPersistence;

        public LuckyGuessRepository(ILuckyGuessPersistence luckyGuessPersistence)
        {
            _luckyGuessPersistence = luckyGuessPersistence;
        }

        public List<LuckyGuess> GetLuckyGuesses()
        {
            return _luckyGuessPersistence.GetLuckyGuesses();
        }

        public List<LuckyGuess> GetLuckyGuessTreasures()
        {
            List<LuckyGuess> luckyGuesses = _luckyGuessPersistence.GetLuckyGuessTreasures();
            if (luckyGuesses.Count == 0)
            {
                Random rnd = new Random();
                _luckyGuessPersistence.AddLuckyGuessTreasure(rnd.Next(50));
                return _luckyGuessPersistence.GetLuckyGuessTreasures();
            }

            return luckyGuesses;
        }

        public bool AddLuckyGuess(User user, int position, bool isWinner)
        {
            if (user == null || position < 0)
            {
                return false;
            }

            if (isWinner)
            {
                if (_luckyGuessPersistence.DeleteLuckyGuesses())
                {
                    if (_luckyGuessPersistence.AddLuckyGuessWinner(user.Username))
                    {
                        Random rnd = new Random();
                        return _luckyGuessPersistence.AddLuckyGuessTreasure(rnd.Next(50));
                    }
                }
            }
            else
            {
                return _luckyGuessPersistence.AddLuckyGuess(position, user.Username);
            }

            return false;
        }
    }
}
