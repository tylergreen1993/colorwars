﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class TakeOfferRepository : ITakeOfferRepository
    {
        private ITakeOfferPersistence _takeOfferPersistence;

        public TakeOfferRepository(ITakeOfferPersistence takeOfferPersistence)
        {
            _takeOfferPersistence = takeOfferPersistence;
        }

        public List<OfferCard> GetOfferCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _takeOfferPersistence.GetOfferCardsWithUsername(user.Username, isCached);
        }

        public bool AddOfferCard(User user, int position, int points)
        {
            if (user == null)
            {
                return false;
            }

            return _takeOfferPersistence.AddOfferCard(user.Username, position, points);
        }

        public bool DeleteOfferCards(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _takeOfferPersistence.DeleteOfferCards(user.Username);
        }
    }
}
