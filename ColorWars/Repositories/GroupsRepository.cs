﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class GroupsRepository : IGroupsRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IGroupsPersistence _groupsPersistence;
        private IUserRepository _userRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private INotificationsRepository _notificationsRepository;
        private ISession _session;

        public GroupsRepository(IHttpContextAccessor httpContextAccessor, IGroupsPersistence groupsPersistence, IUserRepository userRepository, IPointsRepository pointsRepository,
        ISettingsRepository settingsRepository, INotificationsRepository notificationsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _groupsPersistence = groupsPersistence;
            _userRepository = userRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _notificationsRepository = notificationsRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<Group> GetTopGroups(bool isCached = true)
        {
            return _groupsPersistence.GetTopGroups(isCached);
        }

        public List<Group> GetGroupsWithSearchQuery(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                return null;
            }

            query = query.Replace("%", "[%]");

            return _groupsPersistence.GetGroupsWithSearchQuery(query);
        }

        public List<Group> GetGroups(User user, bool loadMembers = true, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Group> groups = _groupsPersistence.GetGroupsWithUsername(user.Username, isCached);
            foreach (Group group in groups)
            {
                if (loadMembers)
                {
                    group.Members = _groupsPersistence.GetGroupMembersWithGroupId(group.Id);
                }
                group.IsUserMember = true;
            }
            return groups;
        }

        public Group GetGroup(User user, int id)
        {
            if (user == null)
            {
                return null;
            }

            Group group = _groupsPersistence.GetGroupWithId(id);
            if (group == null)
            {
                return null;
            }

            List<GroupMember> groupMembers = _groupsPersistence.GetGroupMembersWithGroupId(group.Id);
            GroupMember groupMember = groupMembers.Find(x => x.Username == user.Username);
            if (groupMember != null)
            {
                if (groupMember.Role == GroupRole.Banned)
                {
                    group.IsUserBanned = true;
                }
                else
                {
                    group.IsUserMember = true;
                    group.IsPrimary = groupMember.IsPrimary;
                    group.LastReadMessageDate = groupMember.LastReadMessageDate;
                }
            }
            group.Members = groupMembers;
            return group;
        }

        public List<GroupMessage> GetGroupMessages(Group group)
        {
            if (group == null)
            {
                return null;
            }

            List<GroupMessage> groupMessages = _groupsPersistence.GetGroupMessagesWithGroupId(group.Id);
            groupMessages.Reverse();

            return groupMessages;
        }

        public GroupPower GetGroupPower(User user, bool isCached = true)
        {
            GroupPower power = isCached ? _session.GetObject<GroupPower>("GroupPower") : GroupPower.None;
            if (power != GroupPower.None)
            {
                if (power == GroupPower.Unset)
                {
                    return GroupPower.None;
                }
                return power;
            }

            Group currentGroup = GetGroups(user).Find(x => x.IsPrimary);
            if (currentGroup == null)
            {
                if (isCached)
                {
                    _session.SetObject("GroupPower", GroupPower.Unset);
                }
                return GroupPower.None;
            }

            GroupMember member = currentGroup.Members.Find(x => x.Username == user.Username);
            if (member == null)
            {
                if (isCached)
                {
                    _session.SetObject("GroupPower", GroupPower.Unset);
                }
                return GroupPower.None;
            }

            if ((DateTime.UtcNow - member.JoinDate).TotalHours < 24)
            {
                return GroupPower.None;
            }

            power = currentGroup.Power;

            if (isCached)
            {
                _session.SetObject("GroupPower", power);
            }
            return power;
        }

        public List<GroupRequest> GetGroupRequests(Group group)
        {
            if (group == null)
            {
                return null;
            }

            return _groupsPersistence.GetGroupRequestsWithGroupId(group.Id);
        }

        public List<GroupMessage> GetUnreadGroupMessages(User user, Group currentGroup)
        {
            if (currentGroup == null)
            {
                return null;
            }

            List<GroupMessage> groupMessages = GetGroupMessages(currentGroup);
            if (groupMessages.Count == 0)
            {
                return null;
            }

            GroupMember groupMember = currentGroup.Members.Find(x => x.Username == user.Username);
            if (groupMember == null)
            {
                return null;
            }

            DateTime lastMessageDate = groupMember.JoinDate;
            if (currentGroup.LastReadMessageDate > lastMessageDate)
            {
                lastMessageDate = currentGroup.LastReadMessageDate;
            }

            return groupMessages.FindAll(x => x.CreatedDate > lastMessageDate);
        }

        public bool AddGroup(User user, string name, string displayPicUrl, GroupPower power, string description, bool isPrivateGroup, out Group group)
        {
            group = null;
            if (user == null || string.IsNullOrEmpty(name) || power <= GroupPower.None || string.IsNullOrEmpty(description))
            {
                return false;
            }

            group = _groupsPersistence.AddGroup(user.Username, name, displayPicUrl, power, description, isPrivateGroup);
            return group != null;
        }

        public bool AddGroupMember(User user, Group group, GroupRole role, bool isPrimary)
        {
            if (user == null || group == null)
            {
                return false;
            }

            if (_groupsPersistence.AddGroupMember(user.Username, group.Id, role, isPrimary))
            {
                ClearCache();
                return true;
            }

            return false;
        }

        public bool AddGroupMessage(User user, Group group, string content)
        {
            if (user == null || group == null || string.IsNullOrEmpty(content))
            {
                return false;
            }

            return _groupsPersistence.AddGroupMessage(group.Id, user.Username, HttpUtility.HtmlEncode(content));
        }

        public bool AddGroupRequest(User user, Group group)
        {
            if (user == null || group == null)
            {
                return false;
            }

            return _groupsPersistence.AddGroupRequest(user.Username, group.Id);
        }

        public bool UpdateGroup(Group group)
        {
            if (group == null)
            {
                return false;
            }

            return _groupsPersistence.UpdateGroup(group);
        }

        public bool UpdateGroupMember(GroupMember groupMember)
        {
            if (groupMember == null)
            {
                return false;
            }

            return _groupsPersistence.UpdateGroupMember(groupMember);
        }

        public bool UpdateGroupMessage(GroupMessage groupMessage)
        {
            if (groupMessage == null)
            {
                return false;
            }

            return _groupsPersistence.UpdateGroupMessage(groupMessage);
        }

        public bool UpdateGroupRequest(GroupRequest groupRequest)
        {
            if (groupRequest == null)
            {
                return false;
            }

            return _groupsPersistence.UpdateGroupRequest(groupRequest);
        }

        public bool DeleteGroup(Group group)
        {
            if (group == null)
            {
                return false;
            }

            return _groupsPersistence.DeleteGroup(group.Id);
        }

        public bool DeleteGroupMember(GroupMember groupMember)
        {
            if (groupMember == null)
            {
                return false;
            }

            if (_groupsPersistence.DeleteGroupMember(groupMember.GroupId, groupMember.Username))
            {
                ClearCache();
                return true;
            }

            return false;
        }

        public bool DeleteGroupRequest(User user, Group group)
        {
            if (user == null || group == null)
            {
                return false;
            }

            return _groupsPersistence.DeleteGroupRequest(user.Username, group.Id);
        }

        public bool BanGroupMember(GroupMember groupMember)
        {
            if (groupMember == null)
            {
                return false;
            }

            groupMember.Role = GroupRole.Banned;
            return UpdateGroupMember(groupMember);
        }

        public bool UnbanGroupMember(GroupMember groupMember)
        {
            if (groupMember == null)
            {
                return false;
            }

            return DeleteGroupMember(groupMember);
        }

        public int ClaimPrize(GroupMember member, Group group)
        {
            List<GroupMember> allMembers = group.Members.FindAll(x => x.Role != GroupRole.Banned);
            List<GroupMember> prizeMembers = group.Members.FindAll(x => x.Role > GroupRole.Standard || x.Username == member.Username);

            Random rnd = new Random();
            int amount = rnd.Next(50, 50 * Math.Min(1000, allMembers.Count));

            string domain = Helper.GetDomain(_httpContextAccessor.HttpContext);
            Task.Run(() =>
            {
                foreach (GroupMember prizeMember in prizeMembers)
                {
                    User groupUser = _userRepository.GetUser(prizeMember.Username);
                    if (groupUser != null)
                    {
                        _pointsRepository.AddPoints(groupUser, amount);
                        if (groupUser.Username != member.Username)
                        {
                            _notificationsRepository.AddNotification(groupUser, $"@{member.Username} claimed your group's daily prize and earned you {Helper.GetFormattedPoints(amount)}!", NotificationLocation.Groups, $"/Groups?id={group.Id}", domain: domain);
                        }
                    }
                }
            });

            return amount;
        }

        private void ClearCache()
        {
            _session.Remove("GroupPower");
        }
    }
}
