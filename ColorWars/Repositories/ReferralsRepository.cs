﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ReferralsRepository : IReferralsRepository
    {
        private IReferralsPersistence _referralsPersistence;

        public ReferralsRepository(IReferralsPersistence referralsPersistence)
        {
            _referralsPersistence = referralsPersistence;
        }

        public List<Referral> GetReferrals(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _referralsPersistence.GetReferralsWithUsername(user.Username);
        }


        public List<TopReferrer> GetTopReferrers(int days)
        {
            if (days < 0)
            {
                return null;
            }

            return _referralsPersistence.GetTopReferrers(days);
        }
    }
}
