﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class TradeRepository : ITradeRepository
    {
        private ITradePersistence _tradePersistence;
        private IItemsRepository _itemsRepository;
        private IUserRepository _userRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public TradeRepository(ITradePersistence tradePersistence, IItemsRepository itemsRepository, IUserRepository userRepository,
        IAdminHistoryRepository adminHistoryRepository)
        {
            _tradePersistence = tradePersistence;
            _itemsRepository = itemsRepository;
            _userRepository = userRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public List<Trade> GetAllTrades(string query = "", bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            List<Trade> trades;

            if (string.IsNullOrEmpty(query))
            {
                trades = _tradePersistence.GetTrades();
            }
            else
            {
                query = query?.Replace("%", "[%]");
                ItemTheme searchTheme = ItemTheme.Default;
                if (query.Contains("-"))
                {
                    IEnumerable<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>();
                    foreach (ItemTheme theme in themes)
                    {
                        if (query.ToLower().Contains(theme.ToString().ToLower()))
                        {
                            query = query.Split("-").First().Trim();
                            searchTheme = theme;
                            break;
                        }
                    }
                }

                trades = _tradePersistence.GetTradesWithSearch(query, searchTheme);
            }

            foreach (Trade trade in trades)
            {
                AddItemsToTrade(trade);
                if (shouldMatchExactSearch)
                {
                    query = query?.Replace("[%]", "%");
                    trades = trades.FindAll(x => x.Items.Any(y => y.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase)));
                }
                if (category != ItemCategory.All)
                {
                    trades = trades.FindAll(x => x.Items.Any(y => y.Category == category));
                }
                if (condition != ItemCondition.All)
                {
                    trades = trades.FindAll(x => x.Items.Any(y => y.Condition == condition));
                }
            }
            return trades;
        }

        public List<Trade> GetTrades(User user, bool onlyActive = false, bool getTradeItems = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Trade> trades = _tradePersistence.GetTradesWithUsername(user.Username);
            if (onlyActive)
            {
                trades = trades.FindAll(x => !x.IsAccepted);
            }
            foreach (Trade trade in trades)
            {
                AddItemsToTrade(trade, getTradeItems);
            }
            return trades;
        }

        public List<Trade> GetOffers(User user, bool onlyActive = false, bool getTradeItems = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Trade> trades = _tradePersistence.GetTradesUserLeftOfferWithUsername(user.Username);
            if (onlyActive)
            {
                trades = trades.FindAll(x => !x.IsAccepted);
            }
            foreach (Trade trade in trades)
            {
                AddItemsToTrade(trade, getTradeItems);
            }
            return trades;
        }

        public Trade GetTrade(int id)
        {
            Trade trade = _tradePersistence.GetTradeWithId(id);
            if (trade != null)
            {
                AddItemsToTrade(trade);
                foreach (TradeOffer tradeOffer in trade.Offers)
                {
                    UpdateTradeOffer(tradeOffer);
                }
            }

            return trade;
        }

        public bool AddTrade(User user, Item item1, Item item2, Item item3, string request, out Trade trade)
        {
            trade = null;
            if (user == null)
            {
                return false;
            }

            if (item1 == null && item2 == null && item3 == null)
            {
                return false;
            }

            AddItems(item1, item2, item3);

            trade = _tradePersistence.AddTrade(user.Username, item1?.SelectionId ?? null, item2?.SelectionId ?? null, item3?.SelectionId ?? null, request);
            return trade != null;
        }

        public bool AddTradeOffer(User user, Trade trade, Item item1, Item item2, Item item3)
        {
            if (user == null || trade == null)
            {
                return false;
            }

            if (item1 == null && item2 == null && item3 == null)
            {
                return false;
            }

            AddItems(item1, item2, item3);

            return _tradePersistence.AddTradeOffer(user.Username, trade.Id, item1?.SelectionId ?? null, item2?.SelectionId ?? null, item3?.SelectionId ?? null);
        }

        public bool CompleteTrade(Trade trade, TradeOffer tradeOffer)
        {
            if (trade == null || tradeOffer == null || trade.Id != tradeOffer.TradeId)
            {
                return false;
            }

            Task.Run(() =>
            {
                foreach (TradeOffer offer in trade.Offers)
                {
                    if (offer.Id != tradeOffer.Id)
                    {
                        User user = _userRepository.GetUser(offer.Username);
                        if (user == null)
                        {
                            Log.Debug($"Offer user {offer.Username} was null.");
                        }

                        foreach (Item item in offer.Items)
                        {
                            _itemsRepository.AddItem(user, item, false);
                        }
                    }
                }
            });

            User tradeUser = _userRepository.GetUser(trade.Username);
            if (tradeUser == null)
            {
                Log.Debug($"Trade user {trade.Username} was null.");
                return false;
            }

            int offerAmount = 0;
            foreach (Item item in tradeOffer.Items)
            {
                _itemsRepository.AddItem(tradeUser, item, false);
                offerAmount += item.Points;
            }

            User offerUser = _userRepository.GetUser(tradeOffer.Username);
            if (offerUser == null)
            {
                Log.Debug($"Offer user {tradeOffer.Username} was null.");
                return false;
            }

            int tradeAmount = 0;
            foreach (Item item in trade.Items)
            {
                _itemsRepository.AddItem(offerUser, item, false);
                tradeAmount += item.Points;
            }

            int difference = Math.Abs(tradeAmount - offerAmount);
            if ((double)Math.Max(tradeAmount, offerAmount) / Math.Min(tradeAmount, offerAmount) >= 1.5 && difference >= 10000)
            {
                _adminHistoryRepository.AddTransferHistory(trade.Id, TransferType.Trading, tradeUser, offerUser, difference);
            }

            return _tradePersistence.CompleteTrade(trade.Id, tradeOffer.Id);
        }

        public bool UpdateTrade(Trade trade)
        {
            if (trade == null)
            {
                return false;
            }

            if (trade.IsAccepted)
            {
                return false;
            }

            return _tradePersistence.UpdateTrade(trade);
        }

        public bool DeleteTrade(Trade trade)
        {
            if (trade == null)
            {
                return false;
            }

            if (trade.IsAccepted)
            {
                return false;
            }

            User user;
            foreach (TradeOffer tradeOffer in trade.Offers)
            {
                user = _userRepository.GetUser(tradeOffer.Username);
                if (user == null)
                {
                    Log.Debug($"Offer user {tradeOffer.Username} was null.");
                    return false;
                }
                foreach (Item item in tradeOffer.Items)
                {
                    _itemsRepository.AddItem(user, item);
                }
            }

            user = _userRepository.GetUser(trade.Username);
            if (user == null)
            {
                Log.Debug($"Trade user {trade.Username} was null.");
                return false;
            }
            foreach (Item item in trade.Items)
            {
                _itemsRepository.AddItem(user, item);
            }

            return _tradePersistence.DeleteTrade(trade.Id);
        }

        public bool DeleteTradeOffer(TradeOffer tradeOffer)
        {
            if (tradeOffer == null)
            {
                return false;
            }

            if (tradeOffer.IsAccepted)
            {
                return false;
            }

            User user = _userRepository.GetUser(tradeOffer.Username);
            if (user == null)
            {
                Log.Debug($"Offer user {tradeOffer.Username} was null.");
                return false;
            }
            foreach (Item item in tradeOffer.Items)
            {
                _itemsRepository.AddItem(user, item);
            }

            return _tradePersistence.DeleteTradeOffer(tradeOffer.Id);
        }

        private void AddItems(Item item1, Item item2, Item item3)
        {
            if (item1 != null)
            {
                _tradePersistence.AddTradeItem(item1);
            }
            if (item2 != null)
            {
                _tradePersistence.AddTradeItem(item2);
            }
            if (item3 != null)
            {
                _tradePersistence.AddTradeItem(item3);
            }
        }

        private void AddItemsToTrade(Trade trade, bool getTradeItems = true)
        {
            if (getTradeItems)
            {
                List<Item> items = new List<Item>();
                if (trade.Item1Id != Guid.Empty)
                {
                    items.Add(_tradePersistence.GetTradeItemWithSelectionId(trade.Item1Id));
                }
                if (trade.Item2Id != Guid.Empty)
                {
                    items.Add(_tradePersistence.GetTradeItemWithSelectionId(trade.Item2Id));
                }
                if (trade.Item3Id != Guid.Empty)
                {
                    items.Add(_tradePersistence.GetTradeItemWithSelectionId(trade.Item3Id));
                }
                trade.Items = items;
            }
            trade.Offers = _tradePersistence.GetTradeOffersWithTradeId(trade.Id);
        }

        private void UpdateTradeOffer(TradeOffer tradeOffer)
        {
            List<Item> items = new List<Item>();
            if (tradeOffer.Item1Id != Guid.Empty)
            {
                items.Add(_tradePersistence.GetTradeItemWithSelectionId(tradeOffer.Item1Id));
            }
            if (tradeOffer.Item2Id != Guid.Empty)
            {
                items.Add(_tradePersistence.GetTradeItemWithSelectionId(tradeOffer.Item2Id));
            }
            if (tradeOffer.Item3Id != Guid.Empty)
            {
                items.Add(_tradePersistence.GetTradeItemWithSelectionId(tradeOffer.Item3Id));
            }
            tradeOffer.Items = items;
        }
    }
}
