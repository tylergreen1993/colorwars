﻿using System;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class RepairRepository : IRepairRepository
    {

        public int GetRepairPoints(Item item, int discountPercent)
        {
            if (item.Condition == ItemCondition.New)
            {
                return 0;
            }

            int points = (int)Math.Round((item.OriginalPoints - item.Points) * 0.5);

            if (discountPercent > 0)
            {
                points = (int)Math.Round(points * ((double)(100 - discountPercent) / 100));
            }

            return Math.Max(1, points);
        }

        public int GetRechargePoints(Item item, int discountPercent)
        {
            if (item.Uses >= item.TotalUses)
            {
                return 0;
            }

            int points = (int)Math.Round(item.OriginalPoints * (1 - (double)item.Uses / item.TotalUses) * (item.OriginalPoints >= 10000 ? 0.75 : 0.5));

            if (discountPercent > 0)
            {
                points = (int)Math.Round(points * ((double)(100 - discountPercent) / 100));
            }

            return Math.Max(1, points);
        }
    }
}
