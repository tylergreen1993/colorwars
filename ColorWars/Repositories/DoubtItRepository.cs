﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class DoubtItRepository : IDoubtItRepository
    {
        private IDoubtItPersistence _doubtItPersistence;

        public DoubtItRepository(IDoubtItPersistence doubtItPersistence)
        {
            _doubtItPersistence = doubtItPersistence;
        }

        public List<DoubtItCard> GetDoubtItCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

           return _doubtItPersistence.GetDoubtItCards(user.Username, isCached);
        }

        public List<DoubtItCard> GetLastPlayedCards(List<DoubtItCard> doubtItCards, DoubtItOwner owner)
        {
            return doubtItCards.FindAll(x => x.Owner == owner).GroupBy(x => x.AddedDate).OrderByDescending(x => x.Key).FirstOrDefault()?.ToList() ?? null;
        }

        public bool AddDoubtItCards(User user, List<int> amounts, DoubtItOwner owner)
        {
            if (user == null || amounts == null || amounts.Count == 0 || owner == DoubtItOwner.Pile)
            {
                return false;
            }

            string amountsString = string.Join(",", amounts.Select(x => x));

            return _doubtItPersistence.AddDoubtItCards(user.Username, amountsString, owner);
        }

        public bool UpdateDoubtItCards(List<DoubtItCard> doubtItCards)
        {
            if (doubtItCards == null || doubtItCards.Count == 0)
            {
                return false;
            }

            DateTime addedDate = DateTime.UtcNow;
            foreach (DoubtItCard doubtItCard in doubtItCards)
            {
                if (!_doubtItPersistence.UpdateDoubtItCard(doubtItCard.Id, doubtItCard.Username, doubtItCard.Owner, addedDate))
                {
                    return false;
                }
            }

            return true;
        }

        public bool DeleteDoubtItCards(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _doubtItPersistence.DeleteDoubtItCards(user.Username);
        }
    }
}
