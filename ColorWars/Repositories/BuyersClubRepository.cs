﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class BuyersClubRepository : IBuyersClubRepository
    {
        private IBuyersClubPersistence _buyersClubPersistence;
        private IItemsRepository _itemsRepository;

        public BuyersClubRepository(IBuyersClubPersistence buyersClubPersistence, IItemsRepository itemsRepository)
        {
            _buyersClubPersistence = buyersClubPersistence;
            _itemsRepository = itemsRepository;
        }

        public List<StoreItem> GetItems()
        {
            List<StoreItem> buyersClubItems = _buyersClubPersistence.GetBuyersClubItems();
            if (buyersClubItems != null && buyersClubItems.Any(x => x.GetAgeInMinutes() >= Helper.GetBuyersClubRefreshMinutes()))
            {
                return _buyersClubPersistence.GetBuyersClubItems(true);
            }

            return buyersClubItems;
        }

        public List<StoreItem> SearchStoreItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All)
        {
            List<StoreItem> storeItems = GenerateBuyersClubItems();
            if (!string.IsNullOrEmpty(query))
            {
                storeItems = matchExactSearch ? storeItems.FindAll(x => x.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase)) : storeItems.FindAll(x => x.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
            }

            if (category != ItemCategory.All)
            {
                storeItems = storeItems.FindAll(x => x.Category == category);
            }
            if (condition != ItemCondition.All)
            {
                storeItems = storeItems.FindAll(x => x.Condition == condition);
            }

            return storeItems;
        }

        public List<StoreItem> GenerateBuyersClubItems()
        {
            List<StoreItem> buyersClubItems = GetItems();
            if (buyersClubItems == null || buyersClubItems.Count == 0 || buyersClubItems.First().GetAgeInMinutes() >= Helper.GetBuyersClubRefreshMinutes())
            {
                if (buyersClubItems.Count > 0)
                {
                    DeleteItems();
                }

                Random rnd = new Random();
                ItemCategory[] excludedItems = { ItemCategory.Egg, ItemCategory.Library, ItemCategory.Companion, ItemCategory.Seed, ItemCategory.Fairy, ItemCategory.Beyond, ItemCategory.Key };
                List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, excludedItems).FindAll(x => x.Points <= 1000000);
                List<Item> pickedItems = new List<Item>();

                int maxStoreSize = 25;
                for (int i = 0; i < maxStoreSize; i++)
                {
                    int allItemsIndex = (int)Math.Round((allItems.Count - 1) * Math.Pow(rnd.NextDouble(), 1.65));
                    Item item = allItems[allItemsIndex];
                    pickedItems.Add(item);
                }

                IEnumerable<IGrouping<ItemCategory, Item>> groupedItems = pickedItems.OrderBy(x => x.Points).GroupBy(x => x.Category);
                pickedItems = groupedItems.SelectMany(x =>
                {
                    if (x.Key == ItemCategory.Theme)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Candy)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Jewel)
                    {
                        return x.Take(1);
                    }
                    if (x.Key == ItemCategory.Deck)
                    {
                        return x.Take(8);
                    }
                    if (x.Key == ItemCategory.Coupon)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Crafting)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Upgrader)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Art)
                    {
                        return x.Take(1);
                    }
                    return x;
                }).ToList();

                foreach (Item item in pickedItems.Distinct())
                {
                    int cost = (int)Math.Round((item.Points + (double)rnd.Next(0, item.Points / 8)) / 10) * 10;
                    AddItem(item, cost);
                }

                return GetItems();
            }

            return buyersClubItems;
        }

        public List<ShowroomEnhancement> GetShowroomEnhancements()
        {
            return new List<ShowroomEnhancement>
            {
                new ShowroomEnhancement
                {
                    Id = ShowroomEnhancementId.ShowroomAssessment,
                    Name = "Showroom Assessment",
                    Description = "Once every 2 days, you can have your Showroom assessed to receive a certification.",
                    Points = 2500,
                    ImageUrl = "Showroom/Red.png"
                },
                new ShowroomEnhancement
                {
                    Id = ShowroomEnhancementId.IncreaseSize,
                    Name = "Upgrade Size",
                    Description = "Increase your maximum Showroom size by 1 item.",
                    Info = $"Your Showroom cannot have more than {Helper.GetMaxShowroomSize()} items.",
                    Points = 5000,
                    ImageUrl = "Showroom/Yellow.png"
                },
                new ShowroomEnhancement
                {
                    Id = ShowroomEnhancementId.RandomArtCard,
                    Name = "Art Card",
                    Description = "Add a random Art Card to your Showroom.",
                    Points = 5000,
                    ImageUrl = "Showroom/Green.png"
                },
                new ShowroomEnhancement
                {
                    Id = ShowroomEnhancementId.RechargeItems,
                    Name = "Recharge Items",
                    Description = "Recharge all limited-use items that have been in your Showroom for 3 or more days.",
                    Points = 7500,
                    ImageUrl = "Showroom/Blue.png"
                },
                new ShowroomEnhancement
                {
                    Id = ShowroomEnhancementId.RepairItems,
                    Name = "Repair Items",
                    Description = "Repair all items that have been in your Showroom for 30 or more days.",
                    Points = 15000,
                    ImageUrl = "Showroom/Purple.png"
                },
            };
        }

        public bool AddItem(Item item, int cost)
        {
            if (item == null)
            {
                return false;
            }

            return _buyersClubPersistence.AddBuyersClubItem(item.Id, cost);
        }

        public bool DeleteItems()
        {
            return _buyersClubPersistence.DeleteBuyersClubItems();
        }
    }
}
