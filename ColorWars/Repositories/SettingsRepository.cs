﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SettingsRepository : ISettingsRepository
    {
        private ISettingsPersistence _settingsPersistence;
        private IUserRepository _userRepository;

        public SettingsRepository(ISettingsPersistence settingsPersistence, IUserRepository userRepository)
        {
            _settingsPersistence = settingsPersistence;
            _userRepository = userRepository;
        }

        public Settings GetSettings(User user, bool isCached = true)
        {
            return _settingsPersistence.GetSettingsWithUsername(user.Username, isCached);
        }

        public List<User> GetUsersByNameAndValue(string name, string value)
        {
            return _settingsPersistence.GetUsersBySettingNameAndValue(name, value);
        }

        public bool SetSetting(User user, string name, string value, bool isUser = true)
        {
            if (user == null || string.IsNullOrEmpty(name))
            {
                return false;
            }

            if (!isUser)
            {
                user.FlushSession = true;
                _userRepository.UpdateUser(user);
            }

            Type settings = typeof(Settings);
            PropertyInfo settingsInfo = settings.GetProperty(name);
            if (settingsInfo.GetValue(new Settings()).ToString() == value)
            {
                return _settingsPersistence.DeleteSettingForUsername(user, name);
            }

            return _settingsPersistence.AddSettingForUsername(user, name, value);
        }
    }
}
