﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class JobsRepository : IJobsRepository
    {
        private IJobsPersistence _jobsPersistence;
        private IAttentionHallRepository _attentionHallRepository;

        public JobsRepository(IJobsPersistence jobsPersistence, IAttentionHallRepository attentionHallRepository)
        {
            _jobsPersistence = jobsPersistence;
            _attentionHallRepository = attentionHallRepository;
        }

        public List<JobSubmission> GetJobSubmissions(User currentUser, bool showOnlyOpen = false, bool showOnlyAccepted = false, JobPosition jobPosition = JobPosition.None)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            List<JobSubmission> jobSubmissions = _jobsPersistence.GetJobSubmissions();
            if (showOnlyOpen)
            {
                jobSubmissions = jobSubmissions.FindAll(x => x.State == JobState.Open);
            }
            else if (showOnlyAccepted)
            {
                jobSubmissions = jobSubmissions.FindAll(x => x.State == JobState.Accepted);
            }
            if (jobPosition != JobPosition.None)
            {
                jobSubmissions = jobSubmissions.FindAll(x => x.Position == jobPosition);
            }

            return jobSubmissions;
        }

        public List<JobSubmission> GetJobSubmissionsForUser(User user)
        {
            if (user == null)
            {
                return null;
            }

            List<JobSubmission> jobSubmissions = _jobsPersistence.GetJobSubmissionsWithUsername(user.Username);

            foreach (JobSubmission jobSubmission in jobSubmissions)
            {
                if (jobSubmission.AttentionHallPostId > 0)
                {
                    jobSubmission.AttentionHallPost = _attentionHallRepository.GetAttentionHallPost(user, jobSubmission.AttentionHallPostId);
                }
            }

            return jobSubmissions;
        }

        public JobSubmission GetJobSubmission(Guid id)
        {
            if (id == Guid.Empty)
            {
                return null;
            }

            return _jobsPersistence.GetJobSubmissionWithId(id);
        }

        public bool AddJobSubmission(User user, JobPosition jobPosition, string title, string content, bool addAttentionHallPost)
        {
            if (user == null || (string.IsNullOrEmpty(title) && jobPosition != JobPosition.Helper) || string.IsNullOrEmpty(content))
            {
                return false;
            }

            int attentionHallPostId = 0;
            Guid jobSubmissionId = Guid.NewGuid();
            if (addAttentionHallPost && jobPosition != JobPosition.Helper)
            {
                AttentionHallFlair attentionHallFlair = AttentionHallFlair.None;
                if (jobPosition == JobPosition.Bugs)
                {
                    attentionHallFlair = AttentionHallFlair.Bug;
                }
                else if (jobPosition == JobPosition.Visionary)
                {
                    attentionHallFlair = AttentionHallFlair.Idea;
                }
                if (_attentionHallRepository.AddAttentionHallPost(user, title, content, null, null, null, jobSubmissionId, attentionHallFlair, out AttentionHallPost attentionHallPost))
                {
                    attentionHallPostId = attentionHallPost.Id;
                }
            }

            return _jobsPersistence.AddJobSubmission(jobSubmissionId, user.Username, jobPosition, title, content, attentionHallPostId);
        }

        public bool UpdateJobSubmission(User currentUser, JobSubmission job)
        {
            if (currentUser == null || job == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _jobsPersistence.UpdateJobSubmission(job);
        }

        public bool DeleteHelperSubmission(User currentUser, User user)
        {
            if (currentUser == null || user == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _jobsPersistence.DeleteHelperSubmissionWithUsername(user.Username);
        }
    }
}
