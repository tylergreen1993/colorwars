﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class PointsRepository : IPointsRepository
    {
        private IPointsPersistence _pointsPersistence;

        public PointsRepository(IPointsPersistence pointsPersistence)
        {
            _pointsPersistence = pointsPersistence;
        }

        public bool AddPoints(User user, int amount)
        {
            if (user == null)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }

            return ModifyPoints(user, amount);
        }

        public bool SubtractPoints(User user, int amount)
        {
            if (user == null)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }
            if (user.Points - amount < 0)
            {
                return false;
            }

            return ModifyPoints(user, -amount);
        }

        public WealthRanking GetWealthRanking(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _pointsPersistence.GetWealthRanking(user.Username);
        }

        public List<WealthRanking> GetTopWealthRankings()
        {
            return _pointsPersistence.GetTopWealthRankings();
        }

        private bool ModifyPoints(User user, int amount)
        {
            if (amount == 0)
            {
                return true;
            }

            user.Points += amount;
            return _pointsPersistence.UpdatePoints(user);
        }
    }
}
