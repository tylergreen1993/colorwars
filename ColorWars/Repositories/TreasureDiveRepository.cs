﻿using System;
using System.Drawing;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class TreasureDiveRepository : ITreasureDiveRepository
	{
        private ITreasureDivePersistence _treasureDivePersistence;

        public TreasureDiveRepository(ITreasureDivePersistence treasureDivePersistence)
        {
            _treasureDivePersistence = treasureDivePersistence;
        }

        public TreasureDive GetTreasureDive(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _treasureDivePersistence.GetTreasureDiveWithUsername(user.Username, isCached);
        }

        public bool AddTreasureDive(User user)
        {
            if (user == null)
            {
                return false;
            }

            Random rnd = new Random();
            Point point = new Point
            {
                X = rnd.Next(0, 260),
                Y = rnd.Next(0, 240)
            };

            return _treasureDivePersistence.AddTreasureDive(user.Username, $"{point.X}-{point.Y}");
        }

        public bool UpdateTreasureDive(TreasureDive treasureDive)
        {
            if (treasureDive == null)
            {
                return false;
            }

            return _treasureDivePersistence.UpdateTreasureDive(treasureDive.Username, treasureDive.Attempts, treasureDive.LastGuessCoordinates);
        }

        public bool DeleteTreasureDive(TreasureDive treasureDive)
        {
            if (treasureDive == null)
            {
                return false;
            }

            return _treasureDivePersistence.RemoveTreasureDiveWithUsername(treasureDive.Username);
        }
    }
}
