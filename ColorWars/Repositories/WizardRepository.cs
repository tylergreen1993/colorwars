﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class WizardRepository : IWizardRepository
    {
        private IWizardPersistence _wizardPersistence;

        public WizardRepository(IWizardPersistence wizardPersistence)
        {
            _wizardPersistence = wizardPersistence;
        }

        public List<WizardHistory> GetWizardHistory(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }
            return _wizardPersistence.GetWizardHistoryWithUsername(user.Username, isCached);
        }

        public List<WizardRecord> GetBiggestWizardLosses(int days)
        {
            return _wizardPersistence.GetBiggestWizardLossesInLastDays(days);
        }

        public bool AddWizardHistory(User user, WizardType type, string message)
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _wizardPersistence.AddWizardHistory(user.Username, type, message);
        }

        public bool AddWizardRecord(User user, int amount, WizardRecordType type)
        {
            if (user == null)
            {
                return false;
            }

            return _wizardPersistence.AddWizardRecord(user.Username, amount, type);
        }
    }
}
