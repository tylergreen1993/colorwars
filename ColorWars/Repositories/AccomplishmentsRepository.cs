﻿using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class AccomplishmentsRepository : IAccomplishmentsRepository
    {
        private IAccomplishmentsPersistence _accomplishmentsPersistence;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationsRepository;
        private ISettingsRepository _settingsRepository;
        private IUserRepository _userRepository;

        public AccomplishmentsRepository(IAccomplishmentsPersistence accomplishmentsPersistence, IEmailRepository emailRepository,
        INotificationsRepository notificationsRepository, ISettingsRepository settingsRepository, IUserRepository userRepository)
        {
            _accomplishmentsPersistence = accomplishmentsPersistence;
            _emailRepository = emailRepository;
            _notificationsRepository = notificationsRepository;
            _settingsRepository = settingsRepository;
            _userRepository = userRepository;
        }

        public List<Accomplishment> GetAllAccomplishments(bool isCached = true)
        {
            return _accomplishmentsPersistence.GetAllAccomplishments(isCached);
        }

        public List<Accomplishment> GetAccomplishments(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _accomplishmentsPersistence.GetAccomplishmentsWithUsername(user.Username, isCached);
        }

        public List<Accomplishment> GetAccomplishmentsRankings()
        {
            return _accomplishmentsPersistence.GetAccomplishmentsRankings();
        }

        public int GetAccomplishmentPoints(User user, bool isCached = true)
        {
            if (user == null)
            {
                return 0;
            }

            List<Accomplishment> accomplishments = GetAccomplishments(user, isCached);
            int totalPoints = 0;
            foreach (Accomplishment accomplishment in accomplishments)
            {
                switch (accomplishment.Category)
                {
                    case AccomplishmentCategory.Yellow:
                        totalPoints += 3;
                        break;
                    case AccomplishmentCategory.Green:
                        totalPoints += 5;
                        break;
                    case AccomplishmentCategory.Blue:
                        totalPoints += 8;
                        break;
                    case AccomplishmentCategory.Purple:
                        totalPoints += 13;
                        break;
                }
            }

            if (totalPoints > 0)
            {
                Settings settings = _settingsRepository.GetSettings(user, isCached);
                totalPoints -= settings.AccomplishmentPointsSpent;
            }

            return totalPoints;
        }

        public bool AddAccomplishment(User user, AccomplishmentId accomplishmentId, bool isUser = true, string domain = "")
        {
            if (user == null)
            {
                return false;
            }

            if (_accomplishmentsPersistence.AddAccomplishment(user.Username, accomplishmentId))
            {
                Accomplishment accomplishment = GetAccomplishments(user, isUser).Find(x => x.Id == accomplishmentId);
                if (accomplishment == null)
                {
                    return false;
                }

                if (accomplishment.Category > AccomplishmentCategory.Orange)
                {
                    string url = $"/Profile#accomplishment-{(int)accomplishment.Id}";
                    if (isUser)
                    {
                        Messages.AddToast("New Accomplishment", $"You got the \"{accomplishment.Name}\" accomplishment! Click here to see it on your profile.", url);
                    }
                    else
                    {
                        string subject = "You got a new accomplishment!";
                        string title = "New Accomplishment";
                        string body = $"You got the \"{accomplishment.Name}\" accomplishment! Click the button below to see it on your profile.";

                        _emailRepository.SendEmail(user, subject, title, body, url, domain: string.IsNullOrEmpty(domain) ? null : domain);

                        _notificationsRepository.AddNotification(user, $"You got the \"{accomplishment.Name}\" accomplishment!", NotificationLocation.Accomplishment, url, accomplishment.ImageUrl, domain: domain);

                        user.FlushSession = true;
                        _userRepository.UpdateUser(user);
                    }

                }

                return true;
            }

            return false;
        }
    }
}
