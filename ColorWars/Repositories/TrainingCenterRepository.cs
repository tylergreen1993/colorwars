﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class TrainingCenterRepository : ITrainingCenterRepository
    {
        public List<TrainingCenterUpgrade> GetUpgrades(Settings settings)
        {
            List<TrainingCenterUpgrade> trainingCenterUpgrades = GetAllUpgrades(settings).OrderBy(x => x.Tokens).ThenBy(x => x.Name).ToList();
            return trainingCenterUpgrades;
        }

        private List<TrainingCenterUpgrade> GetAllUpgrades(Settings settings)
        {
            List<TrainingCenterUpgrade> trainingCenterUpgrades = new List<TrainingCenterUpgrade>();

            if (settings.ThemeBonusBoost < 20)
            {
                trainingCenterUpgrades.Add(new TrainingCenterUpgrade
                {
                    Id = TrainingCenterUpgradeId.ThemeBonusIncrease,
                    Name = $"Theme Bonus Increase",
                    Description = $"Increase all Theme Bonuses at the Stadium by 5% (up to 35%).",
                    ImageUrl = "TrainingCenter/ThemeBonusIncrease.png",
                    Tokens = 10
                });
            }
            if (settings.MaxPowerMoves < 10)
            {
                trainingCenterUpgrades.Add(new TrainingCenterUpgrade
                {
                    Id = TrainingCenterUpgradeId.ExtraPowerMoves,
                    Name = $"Extra Power Moves",
                    Description = $"Permanently take up to 10 Power Moves into a Stadium match.",
                    ImageUrl = "TrainingCenter/ExtraPowerMoves.png",
                    Tokens = 35
                });
            }
            if (!settings.UnlockedExtraDeck)
            {
                trainingCenterUpgrades.Add(new TrainingCenterUpgrade
                {
                    Id = TrainingCenterUpgradeId.ExtraDeckSlot,
                    Name = $"Extra Deck Slot",
                    Description = $"Gain an extra slot in your bag to quickly switch between a primary and secondary Deck.",
                    ImageUrl = "TrainingCenter/ExtraDeckSlot.png",
                    Tokens = 20
                });
            }
            trainingCenterUpgrades.Add(new TrainingCenterUpgrade
            {
                Id = TrainingCenterUpgradeId.StadiumBonus,
                Name = $"Stadium Bonus",
                Description = $"Get a 15% bonus at the Stadium against CPUs for the next 10 minutes.",
                ImageUrl = "TrainingCenter/StadiumBonus.png",
                Tokens = 5
            });

            return trainingCenterUpgrades;
        }
    }
}
