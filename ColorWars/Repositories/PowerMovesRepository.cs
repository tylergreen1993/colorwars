﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class PowerMovesRepository : IPowerMovesRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IPowerMovesPersistence _powerMovesPersistence;
        private IMatchStateRepository _matchStateRepository;
        private ISettingsRepository _settingsRepository;
        private ISession _session;
        private List<PowerMove> _powerMoves;

        public PowerMovesRepository(IHttpContextAccessor httpContextAccessor, IPowerMovesPersistence powerMovesPersistence,
        IMatchStateRepository matchStateRepository, ISettingsRepository settingsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _powerMovesPersistence = powerMovesPersistence;
            _matchStateRepository = matchStateRepository;
            _settingsRepository = settingsRepository;
            _session = _httpContextAccessor.HttpContext.Session;
            _powerMoves = new List<PowerMove>();

            PopulatePowerMoves();
        }

        public List<PowerMove> GetPowerMoves(User user, List<MatchState> matchStates, int matchLevel, bool isCPU = false, bool returnAll = false, bool isUser = true, int matchStatePosition = -1)
        {
            if (user == null)
            {
                return ClonePowerMoves();
            }

            List<PowerMove> powerMoves = GetUserPowerMoves(user, matchLevel, isUser);
            if (isCPU)
            {
                powerMoves.ForEach(x => x.IsActive = true);
                List<PowerMove> allCPUPowerMoves = ClonePowerMoves().FindAll(x => x.MinCPULevel >= 0 && x.MinCPULevel <= matchLevel + 1);
                allCPUPowerMoves.RemoveAll(x => powerMoves.Any(y => y.Type == x.Type));
                powerMoves.AddRange(allCPUPowerMoves);
            }

            MatchState currentMatchState = matchStatePosition >= 0 ? matchStates?.Find(x => x.Position == matchStatePosition) : matchStates?.Find(x => x.Status == MatchStatus.NotPlayed);
            if (currentMatchState == null)
            {
                return powerMoves;
            }

            if (returnAll)
            {
                PowerMoveType currentPowerMoveType = isCPU ? currentMatchState.CPUPowerMove : currentMatchState.PowerMove;
                if (currentPowerMoveType < 0)
                {
                    PowerMove powerMove = powerMoves.Find(x => (int)x.Type == -(int)currentPowerMoveType);
                    powerMove.Type = currentPowerMoveType;
                    powerMove.IsNegative = true;
                }
                return powerMoves;
            }

            UpdatePowerMoves(user, powerMoves, matchStates, currentMatchState, isCPU);
            if (!isCPU)
            {
                powerMoves = powerMoves.OrderBy(x => x.Name).ToList();
                List<PowerMove> disabledPowerMoves = ClonePowerMoves().FindAll(x => x.MinCPULevel > matchLevel).ToList();
                if (disabledPowerMoves.Count > 0)
                {
                    PowerMove disabledPowerMove = disabledPowerMoves.OrderBy(x => x.MinCPULevel).First();
                    disabledPowerMove.IsDisabled = true;
                    powerMoves.Add(disabledPowerMove);
                }
            }

            return powerMoves;
        }

        public List<PowerMove> GetAllPowerMoves()
        {
            return ClonePowerMoves();
        }

        public List<PowerMove> GetEarnablePowerMoves()
        {
            return ClonePowerMoves().FindAll(x => x.MinCPULevel == -1 && x.Tokens > 0);
        }

        public bool AddPowerMove(User user, PowerMoveType powerMoveType)
        {
            if (user == null)
            {
                return false;
            }

            return _powerMovesPersistence.AddPowerMove(user.Username, powerMoveType);
        }

        public bool UpdatePowerMove(User user, PowerMove powerMove)
        {
            if (user == null || powerMove == null)
            {
                return false;
            }

            return _powerMovesPersistence.UpdatePowerMove(user.Username, powerMove.Type, powerMove.IsActive);
        }

        private List<PowerMove> ClonePowerMoves()
        {
            List<PowerMove> powerMoves = new List<PowerMove>();
            foreach (PowerMove powerMove in _powerMoves)
            {
                PowerMove powerMoveCopy = new PowerMove
                {
                    Name = powerMove.Name,
                    Description = powerMove.Description,
                    ImageUrl = powerMove.ImageUrl,
                    Type = powerMove.Type,
                    CanBeNegative = powerMove.CanBeNegative,
                    IsNegative = powerMove.IsNegative,
                    IsActive = powerMove.IsActive,
                    MinCPULevel = powerMove.MinCPULevel,
                    Tokens = powerMove.Tokens
                };
                powerMoves.Add(powerMoveCopy);
            }
            return powerMoves;
        }


        private List<PowerMove> GetUserPowerMoves(User user, int matchLevel, bool isUser)
        {
            List<PowerMove> powerMoves = _powerMovesPersistence.GetPowerMovesWithUsername(user.Username, isUser);
            List<PowerMove> clonedPowerMoves = ClonePowerMoves();

            List<PowerMove> userPowerMoves = new List<PowerMove>();
            foreach (PowerMove powerMove in clonedPowerMoves)
            {
                PowerMove userPowerMove = powerMoves.Find(x => x.Type == powerMove.Type);
                if (userPowerMove != null)
                {
                    powerMove.IsActive = userPowerMove.IsActive;
                    powerMove.CreatedDate = userPowerMove.CreatedDate;
                    userPowerMoves.Add(powerMove);
                }
                else if (powerMove.MinCPULevel >= 0 && powerMove.MinCPULevel <= matchLevel)
                {
                    userPowerMoves.Add(powerMove);
                }
            }

            Settings settings = _settingsRepository.GetSettings(user);
            int inactivePowerMoves = settings.MaxPowerMoves - userPowerMoves.Count(x => x.IsActive);
            if (inactivePowerMoves > 0)
            {
                foreach (PowerMove powerMove in userPowerMoves.FindAll(x => x.CreatedDate == DateTime.MinValue).Take(inactivePowerMoves).ToList())
                {
                    powerMove.IsActive = true;
                }
            }

            return userPowerMoves;
        }

        private void UpdatePowerMoves(User user, List<PowerMove> powerMoves, List<MatchState> matchStates, MatchState currentMatchState, bool isCPU)
        {
            if (!isCPU)
            {
                powerMoves.RemoveAll(x => !x.IsActive);
            }
            int winsNeeded = _matchStateRepository.GetWinsNeeded(matchStates.Select(x => x.Status).ToList());

            if (matchStates.FindAll(x => x.Status == (isCPU ? MatchStatus.Won : MatchStatus.Lost)).Count != winsNeeded - 1)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.AllOutAttack);
            }
            if (matchStates.FindAll(x => x.Status == (isCPU ? MatchStatus.Won : MatchStatus.Lost)).Count == winsNeeded - 1)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.SelfDestruct);
            }
            if (currentMatchState?.Position > 3)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.PlannedStrike);
            }
            if (currentMatchState == matchStates.First() || currentMatchState == matchStates.Last())
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.Revival);
            }
            if (currentMatchState.Position >= Helper.GetMaxDeckSize() - 2)
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.LoseControl);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.DoubleDip);
            }

            if (matchStates.Count <= Helper.GetMaxDeckSize())
            {
                Settings settings = _settingsRepository.GetSettings(user);
                powerMoves.RemoveAll(x => matchStates.Any(y => y.Position != (isCPU ? settings.OpponentStadiumRevivalRound : settings.StadiumRevivalRound) && (PowerMoveType)Math.Abs((int)(isCPU ? y.CPUPowerMove : y.PowerMove)) == x.Type));
            }
            else
            {
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.Confusion);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.LuckUp);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.PoisonTouch);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.Revival);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.PowerStop);
                powerMoves.RemoveAll(x => x.Type == PowerMoveType.CounterStop);
            }
        }

        private void PopulatePowerMoves()
        {
            List<PowerMove> sessionPowerMoves = _session.GetObject<List<PowerMove>>("PowerMoves");
            if (sessionPowerMoves != null)
            {
                _powerMoves = sessionPowerMoves;
                return;
            }

            _powerMoves.Add(new PowerMove
            {
                Name = "Silenced Enhancers",
                Description = $"Enhancers have no effect. (Affects Everyone)",
                Type = PowerMoveType.SilencedEnhancers,
                ImageUrl = $"PowerMoves/{PowerMoveType.SilencedEnhancers}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Double Dip",
                Description = $"Force your opponent to play the same Power Move next round. (Affects Opponent)",
                Type = PowerMoveType.DoubleDip,
                ImageUrl = $"PowerMoves/{PowerMoveType.DoubleDip}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Lady Luck",
                Description = $"Either double or half your opponent's Round Power. (Affects Opponent)",
                Type = PowerMoveType.LadyLuck,
                ImageUrl = $"PowerMoves/{PowerMoveType.LadyLuck}.png",
                CanBeNegative = true
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Flowing Power",
                Description = $"Increase your Round Power by 25%. (Affects You)",
                Type = PowerMoveType.FlowingPower,
                ImageUrl = $"PowerMoves/{PowerMoveType.FlowingPower}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Power Stop",
                Description = $"Block your opponent's Power Move. (Affects Opponent)",
                Type = PowerMoveType.PowerStop,
                ImageUrl = $"PowerMoves/{PowerMoveType.PowerStop}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Enhancer Swap",
                Description = $"Swap your Enhancer with your opponent's. (Affects Everyone)",
                Type = PowerMoveType.EnhancerSwap,
                ImageUrl = $"PowerMoves/{PowerMoveType.EnhancerSwap}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Self-Destruct",
                Description = $"Forfeit the round but double your Round Power for the next one. (Affects You)",
                Type = PowerMoveType.SelfDestruct,
                ImageUrl = $"PowerMoves/{PowerMoveType.SelfDestruct}.png"
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Mirror Mirror",
                Description = $"Copy your opponent's Power Move. (Affects You)",
                Type = PowerMoveType.MirrorMirror,
                ImageUrl = $"PowerMoves/{PowerMoveType.MirrorMirror}.png",
                MinCPULevel = 2
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Revival",
                Description = $"Get back your last played Power Move. (Affects You)",
                Type = PowerMoveType.Revival,
                ImageUrl = $"PowerMoves/{PowerMoveType.Revival}.png",
                MinCPULevel = 5
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Poison Touch",
                Description = $"Give your opponent a 15% curse for all future rounds. (Affects Opponent)",
                Type = PowerMoveType.PoisonTouch,
                ImageUrl = $"PowerMoves/{PowerMoveType.PoisonTouch}.png",
                MinCPULevel = 8
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Planned Strike",
                Description = $"Use it early on to double your Round Power in Round {Helper.GetMaxDeckSize()}. (Affects You)",
                Type = PowerMoveType.PlannedStrike,
                ImageUrl = $"PowerMoves/{PowerMoveType.PlannedStrike}.png",
                MinCPULevel = 10
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Doubled Enhancers",
                Description = $"Double the values of all played Enhancers. (Affects Everyone)",
                Type = PowerMoveType.DoubledEnhancers,
                ImageUrl = $"PowerMoves/{PowerMoveType.DoubledEnhancers}.png",
                MinCPULevel = 15
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Blocker's Bane",
                Description = $"High chance to block your opponent's Enhancer but may block yours. (Affects Someone)",
                Type = PowerMoveType.BlockersBane,
                ImageUrl = $"PowerMoves/{PowerMoveType.BlockersBane}.png",
                MinCPULevel = 20,
                CanBeNegative = true
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Winning Loser",
                Description = $"Double whichever {Resources.DeckCard} is lower between you and your opponent. (Affects Someone)",
                Type = PowerMoveType.WinningLoser,
                ImageUrl = $"PowerMoves/{PowerMoveType.WinningLoser}.png",
                MinCPULevel = 25,
                CanBeNegative = true
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Color Reduce",
                Description = $"Half your opponent's {Resources.DeckCard} - and yours if it's the same color. (Affects Everyone)",
                Type = PowerMoveType.ColorReduce,
                ImageUrl = $"PowerMoves/{PowerMoveType.ColorReduce}.png",
                MinCPULevel = 30
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Lose Control",
                Description = $"Your opponent's next {Resources.DeckCard} is randomly chosen for them. (Affects Opponent)",
                Type = PowerMoveType.LoseControl,
                ImageUrl = $"PowerMoves/{PowerMoveType.LoseControl}.png",
                MinCPULevel = 35
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Big Shot",
                Description = $"Either double yours or your opponent's Round Power. (Affects Someone)",
                Type = PowerMoveType.BigShot,
                ImageUrl = $"PowerMoves/{PowerMoveType.BigShot}.png",
                MinCPULevel = -1,
                CanBeNegative = true
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Counter Stop",
                Description = $"If your opponent plays a Power Stop, their Enhancer will be blocked. (Affects Opponent)",
                Type = PowerMoveType.CounterStop,
                ImageUrl = $"PowerMoves/{PowerMoveType.CounterStop}.png",
                MinCPULevel = -1,
                Tokens = 5
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Confusion",
                Description = $"Your opponent cannot play an Enhancer for their next turn. (Affects Opponent)",
                Type = PowerMoveType.Confusion,
                ImageUrl = $"PowerMoves/{PowerMoveType.Confusion}.png",
                MinCPULevel = -1,
                Tokens = 10
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Color Boost",
                Description = $"Double your {Resources.DeckCard} - and, if it's the same color, your opponent's. (Affects Everyone)",
                Type = PowerMoveType.ColorBoost,
                ImageUrl = $"PowerMoves/{PowerMoveType.ColorBoost}.png",
                MinCPULevel = -1,
                Tokens = 10
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Topsy Turvy",
                Description = $"Double your {Resources.DeckCard} - and your opponent's Enhancer. (Affects Everyone)",
                Type = PowerMoveType.TopsyTurvy,
                ImageUrl = $"PowerMoves/{PowerMoveType.TopsyTurvy}.png",
                MinCPULevel = -1,
                Tokens = 15
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Deck Swap",
                Description = $"Swap your {Resources.DeckCard} with your opponent's. (Affects Everyone)",
                Type = PowerMoveType.DeckSwap,
                ImageUrl = $"PowerMoves/{PowerMoveType.DeckSwap}.png",
                MinCPULevel = -1,
                Tokens = 20
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "All Out Attack",
                Description = "Either double or half your Round Power if you're one round from losing. (Affects You)",
                Type = PowerMoveType.AllOutAttack,
                ImageUrl = $"PowerMoves/{PowerMoveType.AllOutAttack}.png",
                MinCPULevel = -1,
                Tokens = 20,
                CanBeNegative = true
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Psych Up",
                Description = $"Double your Round Power but have a 15% curse for the rest of the match. (Affects You)",
                Type = PowerMoveType.PsychUp,
                ImageUrl = $"PowerMoves/{PowerMoveType.PsychUp}.png",
                MinCPULevel = -1,
                Tokens = 25
            });
            _powerMoves.Add(new PowerMove
            {
                Name = "Luck Up",
                Description = $"All luck Power Moves, played by you, work in your favor for the remaining rounds. (Affects You)",
                Type = PowerMoveType.LuckUp,
                ImageUrl = $"PowerMoves/{PowerMoveType.LuckUp}.png",
                MinCPULevel = -1,
                Tokens = 50
            });

            _session.SetObject("PowerMoves", _powerMoves);
        }
    }
}
