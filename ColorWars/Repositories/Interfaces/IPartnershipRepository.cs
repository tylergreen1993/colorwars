﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IPartnershipRepository
    {
        List<PartnershipRequest> GetPartnershipRequests(User user);
        List<PartnershipEarning> GetEarningsForPartnership(User user, User partnerUser);
        List<PartnershipEarning> GetEarningsFromPartnership(User user, bool isCached = true);
        bool AddPartnershipRequest(User user, User partnerUser);
        bool AddPartnershipEarning(User user, User partnerUser, int amount);
        bool DeletePartnershipRequest(User user, User requestUser);
    }
}