﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IStocksRepository
    {
        List<Stock> GetStocks(User user);
        List<Stock> GetAllStocks();
        List<Stock> GetLastTransactions(StockTicker id);
        List<StockHistory> GetStockHistory(StockTicker id);
        bool AddStock(User user, Stock stock, int amount, bool hasDiscount, bool isGift);
        bool AddStockHistory(User user, Stock stock, int amount, bool hasDiscount, StockHistoryType type);
        bool RemoveStock(User user, Stock stock, int amount, out bool isCrashed);
    }
}
