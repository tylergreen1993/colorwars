﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IItemsRepository
    {
        List<Item> GetItems(User user, bool onlySellable = false, bool isCached = true);
        List<Card> GetCards(User user, bool onlyInDeck = false, bool isCached = true);
        List<Enhancer> GetEnhancers(User user, bool isCached = true);
        List<CardPack> GetCardPacks(User user, bool isCached = true);
        List<Crafting> GetCrafting(User user, bool isCached = true);
        List<Candy> GetCandy(User user, bool isCached = true);
        List<Coupon> GetCoupons(User user, bool isCached = true);
        List<Egg> GetEggCards(User user, bool isCached = true);
        List<LibraryCard> GetLibraryCards(User user, bool isCached = true);
        List<Theme> GetThemeCards(User user, bool isCached = true);
        List<Sleeve> GetSleeves(User user, bool isCached = true);
        List<Upgrader> GetUpgraders(User user, bool isCached = true);
        List<Jewel> GetJewels(User user, bool isCached = true);
        List<Stealth> GetStealthCards(User user, bool isCached = true);
        List<Frozen> GetFrozenItems(User user, bool isCached = true);
        List<CompanionItem> GetCompanions(User user, bool isCached = true);
        List<Seed> GetSeedCards(User user, bool isCached = true);
        List<Art> GetArtCards(User user, bool isCached = true);
        List<Fairy> GetFairyCards(User user, bool isCached = true);
        List<Beyond> GetBeyondCards(User user, bool isCached = true);
        List<KeyCard> GetKeyCards(User user, bool isCached = true);
        List<Item> GetAllItems(ItemCategory onlyItemCategory = ItemCategory.All, ItemCategory[] excludeItems = null, bool includeExclusiveItems = false);
        List<Card> GetAllCards(bool includeExclusiveItems = false);
        List<Enhancer> GetAllEnhancers(bool includeExclusiveItems = false);
        List<Candy> GetAllCandyCards(bool includeExclusiveItems = false);
        List<Item> GetTopOldestItems(bool isCached = true);
        List<Item> GetTopLongestOwnedItems(bool isCached = true);
        bool AddItem(User user, Item item, bool isUser = true);
        bool RemoveItem(User user, Item item);
        bool UpdateItem(User user, Item item);
        bool RepairAllItems(User user);
        bool RemoveUsedItems(User user);
        Card SwapCard(User user, Card card, int amount);
    }
}
