﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISecretGifterRepository
    {
        List<Item> GetSecretGiftItems(User user);
        SecretGiftUser GetSecretGiftUser(User user);
        bool AddSecretGiftItem(User giftReceiver, Item item);
        bool DeleteSecretGiftItem(Item item);
    }
}
