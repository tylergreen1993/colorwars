﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IRightSideUpWorldRepository
    {
        List<HallOfFameEntry> GetRightSideUpGuardHallOfFame();
        bool AddRightSideUpGuardHallOfFame(User user);
    }
}
