﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IPromoCodesRepository
    {
        PromoCode GetPromoCode(string code);
        List<PromoCode> GetPreviousPromoCodes(User user, bool isCached = true);
        List<PromoCode> GetAllUnexpiredGlobalPromoCodes(User currentUser);
        List<PromoCode> GetAllUnexpiredSponsorCodes(bool isCached = true);
        List<PromoCodeHistory> GetPromoCodeHistory(User user, bool isCached = true);
        PromoCode RedeemPromoCode(User user, string code);
        bool AddPromoCode(User user, string code, PromoEffect effect, bool oneUserUse, bool isSponsor, Item item, DateTime expiryDate);
        bool AddPromoCodeHistory(User user, string code, string message);
        string GenerateRandomPromoCode(int daysToExpire, PromoEffect promoEffect = PromoEffect.None, Item item = null);
    }
}
