﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISoundsRepository
    {
		string GetSoundUrl(SoundType soundType, Settings settings);
	}
}