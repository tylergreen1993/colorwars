﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IRepairRepository
    {
        int GetRepairPoints(Item item, int discountPercent);
        int GetRechargePoints(Item item, int discountPercent);
    }
}
