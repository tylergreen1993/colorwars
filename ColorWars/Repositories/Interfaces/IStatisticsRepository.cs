﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IStatisticsRepository
    {
        Statistics GetStatistics(bool isCached = true);
    }
}
