﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITrainingCenterRepository
    {
        List<TrainingCenterUpgrade> GetUpgrades(Settings settings);
    }
}