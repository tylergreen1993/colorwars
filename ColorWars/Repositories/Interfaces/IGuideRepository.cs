﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IGuideRepository
    {
        List<ItemSection> GetItemSections(List<Guid> itemIds, List<int> redeemedIds, bool isCached = true);
    }
}
