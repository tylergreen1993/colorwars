﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IShowcaseRepository
    {
        Item GetShowcaseItem();
        Item GetAgerItem(User user, bool isCached = true);
        Item SearchShowcaseItem(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All);
        Item GenerateShowcaseItem(bool hasBiggerDiscount);
        bool AddShowcaseItem(Item item);
        bool AddAgerItem(User user, Item item);
        bool DeleteShowcaseItem();
        bool DeleteAgerItem(Item item);
    }
}
