﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IDisposerRepository
    {
        List<DisposerRecord> GetBiggestDisposerRecords(int days);
        bool AddDisposerRecord(User user);
        void DisposeItem(User user, Item item, bool removeItem, out bool recordQualifier);
    }
}
