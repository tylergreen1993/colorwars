﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IArtFactoryRepository
    {
        List<Custom> GetRecentUserCustomCards();
        bool AddUserCustomCard(User user, string name, string imageUrl);
    }
}
