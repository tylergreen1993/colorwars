﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IDoubtItRepository
    {
        List<DoubtItCard> GetDoubtItCards(User user, bool isCached = true);
        List<DoubtItCard> GetLastPlayedCards(List<DoubtItCard> doubtItCards, DoubtItOwner owner);
        bool AddDoubtItCards(User user, List<int> amounts, DoubtItOwner owner);
        bool UpdateDoubtItCards(List<DoubtItCard> doubtItCards);
        bool DeleteDoubtItCards(User user);
    }
}
