﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IUserRepository
    {
        User GetUser(string username, bool onlyActive = true);
        User GetUserWithEmail(string emailAddress, bool onlyActive = true);
        double GetDAUMAUStatistic(User user);
        List<User> GetUsersByIPAddress(string ipAddress, bool includeSessions = false);
        List<User> GetUsersWithAdminRoles();
        List<User> GetRecentUsers(User user, Gender gender);
        List<User> GetReturningUsers(User user, Gender gender);
        List<User> GetAllUsers(User user, Gender gender, int days);
        List<User> GetActiveUsers(User user, int minutes);
        List<User> GetRecentlyReturningUsers(User user);
        List<User> GetNonReturningUsers(User user);
        List<ActiveSession> GetActiveSessions(User user);
        List<InactiveAccount> GetInactiveUsers(User user, InactiveAccountType type);
        List<BannedIPAddress> GetBannedIPAddresses();
        bool AddBannedIPAddress(string ipAddress);
        bool UpdateUser(User user, string password = "");
        bool EnableUser(User user);
        bool DisableUser(User user, InactiveAccountReason reason);
        bool DeleteUser(User user, InactiveAccountReason reason, string info);
        bool DeleteActiveSessions(User user);
        bool DeleteBannedIPAddress(string ipAddress);
        bool IsUsernameInvalid(string username);
        bool IsEmailAddressInvalid(string emailAddress);
        bool IsPasswordValid(User user, string password);
    }
}
