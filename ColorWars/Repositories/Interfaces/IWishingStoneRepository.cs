﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IWishingStoneRepository
    {
        List<Wish> GetWishes();
        int[] GetSecretQuarryStonePattern(User user, Settings settings);
        bool AddWish(User user, string content, bool isGranted);
        bool DeleteWishes(User user);
    }
}
