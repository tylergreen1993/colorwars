﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IBuyersClubRepository
    {
        List<StoreItem> GetItems();
        List<StoreItem> SearchStoreItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All);
        List<StoreItem> GenerateBuyersClubItems();
        List<ShowroomEnhancement> GetShowroomEnhancements();
        bool AddItem(Item item, int cost);
        bool DeleteItems();
    }
}
