﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IMessagesRepository
    {
        List<Message> GetMessagesBetweenUsernames(User user, User correspondent, bool updateRead = true);
        List<Message> GetAllMessages(User user, int topCount = 50);
        int GetUnreadMessagesCount(User user, string correspondent = null);
        bool AddMessage(User sender, User recipient, string message, string domain = "");
        bool SendCourierMessage(User recipient, string message, string domain = "");
        bool SendPersonalMessage(User recipient, string domain, string message = null);
        bool DeleteMessagesBetweenUsernames(User user, User correspondent);
    }
}