﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICraftingRepository
    {
        Item GetFurnaceItem(User user, bool isCached = true);
        bool AddFurnaceItem(User user, Item item);
        bool DeleteFurnaceItem(Item item);
        Item DefrostItem(User user, Item item);
    }
}
