﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISettingsRepository
    {
        Settings GetSettings(User user, bool isCached = true);
        List<User> GetUsersByNameAndValue(string name, string value);
        bool SetSetting(User user, string name, string value, bool isUser = true);
    }
}
