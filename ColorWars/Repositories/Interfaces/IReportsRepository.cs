﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IReportsRepository
    {
        List<Report> GetAllUnclaimedReports(User currentUser);
        List<Report> GetAllCompletedReports(User currentUser);
        List<Report> GetClaimedReports(User user);
        List<Report> GetReports(User currentUser, User reportUser, bool isReported);
        bool AddReport(User user, User reportedUser, string reason, int attentionHallPostId = 0);
        bool UpdateReport(User currentUser, Report report);
    }
}
