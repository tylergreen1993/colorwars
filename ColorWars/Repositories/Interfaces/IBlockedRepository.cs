﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IBlockedRepository
    {
        List<Blocked> GetBlocked(User user, bool isCached = true);
        bool AddBlocked(User user, User blockedUser);
        bool DeleteFromBlocked(User user, User blockedUser);
        bool HasBlockedUser(User user, User blockedUser, bool isCached = true);
    }
}
