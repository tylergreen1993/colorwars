﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IPointsRepository
    {
        bool AddPoints(User user, int amount);
        bool SubtractPoints(User user, int amount);
        WealthRanking GetWealthRanking(User user);
        List<WealthRanking> GetTopWealthRankings();
    }
}
