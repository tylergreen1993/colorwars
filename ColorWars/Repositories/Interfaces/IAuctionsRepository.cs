﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IAuctionsRepository
    {
        List<Auction> GetAllAuctions(string query = "", bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All);
        List<Auction> GetAuctions(User user, bool onlyActive = false, bool onlyUncollected = false, bool getBidDetails = true);
        List<Auction> GetPastAuctions();
        Auction GetAuction(int auctionId);
        Item GetItemWithAuctionId(int auctionId);
        bool AddBid(User user, Auction auction, int points);
        bool AddAuction(User user, int points, int minBidIncrement, Item item, DateTime endDate, out Auction auction);
        bool UpdateAuction(Auction auction);
    }
}
