﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IReferralsRepository
    {
        List<Referral> GetReferrals(User user);
        List<TopReferrer> GetTopReferrers(int days);
    }
}
