﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IClubRepository
    {
        ClubMembership GetCurrentMembership(User user, bool isCached = true);
        List<ClubMembership> GetAllClubMemberships(User user, bool isCached = true);
        List<Item> GetUnclaimedItems(User user, bool isCached = true);
        Item GetNextItem(User user, bool isCached = true);
        Item GetIncubatorItem(User user, bool isCached = true);
        DateTime GetMembershipEndDate(User user, bool isCached = true);
        bool AddClubMembership(User user, int months, DateTime startDate);
        bool AddIncubatorItem(User user, Item item);
        bool ClaimItems(User user, bool isCached = true);
        bool DeleteIncubatorItem(Item item);
    }
}
