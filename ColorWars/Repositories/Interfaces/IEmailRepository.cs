﻿using System.Threading.Tasks;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IEmailRepository
    {
        Task<bool> SendEmail(User user, string subject, string title, string body, string buttonUrl, bool overrideAllowNotifications = false, string domain = null, string fromOverride = null);
        Task<bool> SendEmail(string emailAddress, string subject, string title, string body, string buttonUrl, bool overrideAllowNotifications = false, string domain = null, string fromOverride = null);
        Task<bool> SendEmailVerification(User user);
    }
}
