﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IDonationsRepository
    {
        List<Item> SearchItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All);
        List<Item> GetDonationItems(bool getAllItems = false);
        bool AddDonationItem(User user, Item item);
        bool AddSpecialDonationItem(Item item);
        bool DeleteDonationItem(Item item);
    }
}
