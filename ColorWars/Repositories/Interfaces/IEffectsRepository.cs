﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IEffectsRepository
    {
        List<Effect> GetActiveEffects(User user, bool isCached = true);
        void RemoveAllActiveEffects(User user);
    }
}
