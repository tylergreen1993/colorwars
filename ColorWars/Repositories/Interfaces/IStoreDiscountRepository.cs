﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IStoreDiscountRepository
    {
        ItemCategory GetDiscountCategory(User user);
    }
}
