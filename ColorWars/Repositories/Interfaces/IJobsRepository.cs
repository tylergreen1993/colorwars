﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IJobsRepository
    {
        List<JobSubmission> GetJobSubmissions(User currentUser, bool showOnlyOpen = false, bool showOnlyAccepted = false, JobPosition jobPosition = JobPosition.None);
        List<JobSubmission> GetJobSubmissionsForUser(User user);
        JobSubmission GetJobSubmission(Guid id);
        bool AddJobSubmission(User user, JobPosition jobPosition, string title, string content, bool addAttentionHallPost);
        bool UpdateJobSubmission(User currentUser, JobSubmission job);
        bool DeleteHelperSubmission(User currentUser, User user);
    }
}
