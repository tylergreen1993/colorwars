﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ILocationsRepository
    {
        List<Location> GetFavoritedLocations(User user, bool isCached = true);
        List<Location> GetFavoritedPlazaList(User user, Settings settings, bool isCached = true);
        List<Location> GetLocations(Settings settings, List<Accomplishment> accomplishments, LocationArea locationArea, bool getAllSpecialLocations);
        bool AddFavoritedLocation(User user, LocationId locationId);
        bool UpdateFavoritedLocation(User user, Location location);
        bool RemoveFavoritedLocation(User user, LocationId locationId);
    }
}
