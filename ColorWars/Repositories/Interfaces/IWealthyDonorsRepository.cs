﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IWealthyDonorsRepository
    {
		List<WealthyDonor> GetWealthyDonors();
		bool AddWealthyDonor(User user, int amount);
	}
}
