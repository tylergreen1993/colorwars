﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISpinSuccessRepository
    {
        List<SpinSuccessHistory> GetSpinSuccessHistory(User user, bool isCached = true);
        bool AddSpinSuccessHistory(User user, SpinSuccessType type, string message);
    }
}
