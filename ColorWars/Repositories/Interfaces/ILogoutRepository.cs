﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ILogoutRepository
    {
        void Logout(User user);
    }
}
