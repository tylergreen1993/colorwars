﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IStreakCardsRepository
    {
        List<StreakCard> GetStreakCards(User user, bool isCached = true);
        bool AddStreakCard(User user, int position, int points);
        bool DeleteStreakCards(User user);
    }
}
