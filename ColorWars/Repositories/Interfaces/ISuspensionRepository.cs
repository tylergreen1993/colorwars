﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISuspensionRepository
    {
        List<SuspensionHistory> GetSuspensionHistory(User user, User suspendingUser);
        bool SuspendUser(User user, User suspendingUser, string reason, DateTime suspendedDate);
        bool UnsuspendUser(User user, User suspendingUser, string reason);
    }
}
