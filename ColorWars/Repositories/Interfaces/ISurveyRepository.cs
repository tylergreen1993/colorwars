﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISurveyRepository
    {
        List<SurveyQuestion> GetSurveyQuestions(User user, bool isCached = true);
        List<SurveyAnswer> GetSurveyAnswers(User user = null, User adminUser = null);
        bool AddSurveyAnswer(User user, SurveyQuestionId surveyQuestionId, string answer);
        bool SaveNewSurveyQuestions(User user, bool isCached = true);
    }
}
