﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IRegisterRepository
    {
        User CreateNewUser(string username, string password, string emailAddress, bool allowNotifications, string referral);
        bool IsUsernameAvailable(string username);
        bool IsEmailAvailable(string email);
    }
}
