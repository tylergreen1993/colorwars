﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IGroupsRepository
    {
        List<Group> GetTopGroups(bool isCached = true);
        List<Group> GetGroupsWithSearchQuery(string query);
        List<Group> GetGroups(User user, bool loadMembers = true, bool isCached = true);
        Group GetGroup(User user, int id);
        List<GroupMessage> GetGroupMessages(Group group);
        GroupPower GetGroupPower(User user, bool isCached = true);
        List<GroupRequest> GetGroupRequests(Group group);
        List<GroupMessage> GetUnreadGroupMessages(User user, Group currentGroup);
        bool AddGroup(User user, string name, string displayPicUrl, GroupPower power, string description, bool isPrivateGroup, out Group group);
        bool AddGroupMember(User user, Group group, GroupRole role, bool isPrimary);
        bool AddGroupMessage(User user, Group group, string content);
        bool AddGroupRequest(User user, Group group);
        bool UpdateGroup(Group group);
        bool UpdateGroupMember(GroupMember groupMember);
        bool UpdateGroupMessage(GroupMessage groupMessage);
        bool UpdateGroupRequest(GroupRequest groupRequest);
        bool DeleteGroup(Group group);
        bool DeleteGroupMember(GroupMember groupMember);
        bool DeleteGroupRequest(User user, Group group);
        bool BanGroupMember(GroupMember groupMember);
        bool UnbanGroupMember(GroupMember groupMember);
        int ClaimPrize(GroupMember member, Group group);
    }
}
