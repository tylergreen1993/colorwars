﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IBankRepository
    {
        Bank GetBank(User user, bool isCached = true);
        List<BankTransaction> GetBankTransactions(User user, bool isCached = true);
        bool AddBankTransaction(User user, BankTransactionType type, int amount);
        bool Withdraw(User user, Bank bank, int amount);
        bool Deposit(User user, Bank bank, int amount, bool isInterest = false);
    }
}
