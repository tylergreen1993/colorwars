﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IAdminHistoryRepository
    {
        List<AdminHistory> GetAdminHistory(User user, User adminUser);
        List<TransferHistory> GetTransferHistory(User adminUser, User user = null);
        bool AddAdminHistory(User user, User adminUser, string reason);
        bool AddTransferHistory(int transferId, TransferType type, User transferer, User transferee, int differenceAmount);
    }
}
