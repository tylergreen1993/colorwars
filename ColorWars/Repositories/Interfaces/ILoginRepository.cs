﻿using System;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public interface ILoginRepository
    {
        User LoginUser(string username, string password);
        User AuthenticateUser();
    }
}
