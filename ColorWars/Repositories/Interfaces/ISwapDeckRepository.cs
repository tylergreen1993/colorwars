﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISwapDeckRepository
    {
        List<SwapDeckHistory> GetSwapDeckHistory(User user, bool isCached = true);
        bool AddSwapDeckHistory(User user, SwapDeckType type, string message);
    }
}
