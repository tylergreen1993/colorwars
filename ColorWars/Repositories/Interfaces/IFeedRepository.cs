﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IFeedRepository
    {
        List<HiddenFeedCategory> GetHiddenFeedCategories(User user, bool isCached = true);
        bool AddHiddenFeedCategory(User user, FeedCategory feedCategory);
        bool DeleteHiddenFeedCategory(User user, FeedCategory feedCategory);
        List<Feed> ConvertFrom(News news);
        List<Feed> ConvertFrom(User user, List<Accomplishment> accomplishments, Settings settings);
        List<Feed> ConvertFrom(Referral referral);
        List<Feed> ConvertFrom(User user, Auction auction, Settings settings);
        List<Feed> ConvertFrom(User user, Trade trade);
        List<Feed> ConvertFrom(JobSubmission jobSubmission);
        List<Feed> ConvertFrom(Group group, List<GroupRequest> groupRequests, bool showUnreadGroupMessages);
        List<Feed> ConvertFrom(User user, Settings settings);
        List<Feed> ConvertFrom(Item item);
        List<Feed> ConvertFrom(List<MarketStallHistory> marketStallHistories, List<MarketStallItem> marketStallItems);
        List<Feed> ConvertFrom(IGrouping<int, AttentionHallComment> attentionHallComment);
        List<Feed> ConvertFrom(AttentionHallPost attentionHallPost);
        List<Feed> ConvertFrom(ClubMembership clubMembership);
        List<Feed> ConvertFrom(ItemCategory itemCategory);
        List<Feed> ConvertFrom(User user, Companion companion);
        List<Feed> ConvertFrom(PartnershipRequest partnershipRequest);
        List<Feed> ConvertFrom(PartnershipEarning partnershipEarning);
        List<Feed> ConvertFrom(List<PowerMove> powerMoves);
        List<Feed> ConvertFrom(User user, MatchHistory matchHistory, Settings settings, List<Accomplishment> accomplishments);
        List<Feed> ConvertFrom(MatchTag matchTag);
        List<Feed> ConvertFrom(User user, List<MatchWaitlist> matchWaitlists);
        List<Feed> ConvertFrom(List<Stock> stocks);
        List<Feed> ConvertFrom(User user, List<Tourney> tourneys);
        List<Feed> ConvertFrom(Tourney tourney);
        List<Feed> ConvertFrom(List<Donor> donors, AdminSettings adminSettings);
        List<Feed> ConvertFrom(List<PromoCode> promoCodes);
        List<Feed> ConvertFrom(User user, Checklist checklist);
        List<Feed> ConvertFrom(FeedCategory category, DateTime dateTime);
        List<Feed> ConvertFrom(User user);
    }
}
