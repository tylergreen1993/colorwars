﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IMuseumRepository
    {
        List<MuseumDisplay> GetMuseumDisplays(User user, Settings settings, MuseumWing museumWing);
    }
}
