﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IPasswordResetRepository
    {
        PasswordReset GetPasswordReset(string username, Guid id);
        PasswordReset CreateOrUpdatePasswordReset(User user);
    }
}
