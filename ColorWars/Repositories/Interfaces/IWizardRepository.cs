﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IWizardRepository
    {
        List<WizardHistory> GetWizardHistory(User user, bool isCached = true);
        List<WizardRecord> GetBiggestWizardLosses(int days);
        bool AddWizardHistory(User user, WizardType type, string message);
        bool AddWizardRecord(User user, int amount, WizardRecordType type);
    }
}
