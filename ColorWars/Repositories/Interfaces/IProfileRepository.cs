﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IProfileRepository
    {
        List<Item> GetShowroomItems(User user, bool isCached = true);
        List<ShowroomLike> GetShowroomLikes(User showroomUser);
        bool AddShowroomItem(User user, Item item);
        bool SetShowroomLike(User user, User showroomUser);
        bool UpdateShowroomItem(Item showroomItem);
        bool DeleteShowroomItem(Item showroomItem);
    }
}
