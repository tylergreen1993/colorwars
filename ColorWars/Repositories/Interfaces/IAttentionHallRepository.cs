﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IAttentionHallRepository
    {
        List<AttentionHallPost> GetAllAttentionHallPosts(User user, AttentionHallPostsFilter filter, AttentionHallPostType type);
        AttentionHallPost GetAttentionHallPost(User user, int id);
        AttentionHallPost GetMostPopularAttentionHallPost(User user, bool isCached = true);
        List<AttentionHallComment> GetAllAttentionHallComments(User user, AttentionHallPost attentionHallPost, bool sortWithChildren = false);
        List<AttentionHallComment> GetAttentionHallCommentsFromPosts(User user);
        int GetAttentionHallPoints(User user);
        List<UserStatistic> GetTopAttentionHallPosters(bool isCached = true);
        List<UserStatistic> GetTopAttentionHallCommenters(bool isCached = true);
        bool AddAttentionHallPost(User user, string title, string content, Accomplishment accomplishment, Item item, MatchTag matchTag, Guid jobSubmissionId, AttentionHallFlair attentionHallFlair, out AttentionHallPost attentionHallPost);
        bool AddAttentionHallComment(User user, AttentionHallComment parentAttentionHallComment, AttentionHallPost attentionHallPost, string comment, out AttentionHallComment attentionHallComment);
        bool SetAttentionHallPoint(User user, AttentionHallPost attentionHallPost, AttentionHallPointType attentionHallPointType);
        bool SetAttentionHallCommentLike(User user, AttentionHallComment attentionHallComment);
        bool UpdateAttentionHallPost(AttentionHallPost attentionHallPost);
        bool DeleteAttentionHallPost(AttentionHallPost attentionHallPost);
        bool DeleteAttentionHallComment(AttentionHallComment attentionHallComment);
    }
}
