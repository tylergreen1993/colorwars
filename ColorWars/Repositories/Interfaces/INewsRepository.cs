﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface INewsRepository
    {
        List<News> GetNews();
        List<NewsComment> GetNewsComments(User user, News news, bool sortWithChildren = false);
        bool AddNews(User currentUser, User author, string title, string content, out News news);
        bool AddNewsComment(User user, NewsComment parentNewsComment, News news, string message, out NewsComment newsComment);
        bool SetNewsCommentLike(User user, NewsComment newsComment);
        bool UpdateNews(User currentUser, News news);
        bool DeleteNews(User currentUser, News news);
        bool DeleteNewsComment(News news, NewsComment newsComment);
    }
}
