﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICaptchaRepository
    {
        Captcha GenerateCaptcha(string key);
        bool VerifyCaptcha(string key, int answer);
    }
}