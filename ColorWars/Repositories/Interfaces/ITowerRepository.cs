﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITowerRepository
    {
        List<TowerRanking> GetTowerRankings(int days);
        List<TowerRanking> GetTopTowerRankings();
        bool AddTowerRanking(User user, int topFloorLevel);
    }
}
