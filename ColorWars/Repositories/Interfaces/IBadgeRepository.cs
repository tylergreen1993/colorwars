﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IBadgeRepository
    {
        Badge GetActiveBadge(BadgeType badgeType);
        List<Badge> GetAvailableBadges(User user, Settings settings, List<Accomplishment> accomplishments, bool isClub);
    }
}