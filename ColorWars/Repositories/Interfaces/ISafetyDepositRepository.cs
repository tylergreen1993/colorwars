﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISafetyDepositRepository
    {
        List<Item> GetSafetyDepositItems(User user, bool isCached = true);
        bool AddSafetyDepositItem(User user, Item item);
        bool UpdateSafetyDepositItem(Item item);
        bool DeleteSafetyDepositItem(User user, Item item);
        bool RepairAllItems(User user);
    }
}
