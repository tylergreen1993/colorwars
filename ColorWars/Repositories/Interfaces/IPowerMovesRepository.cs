﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IPowerMovesRepository
    {
        List<PowerMove> GetPowerMoves(User user, List<MatchState> matchStates, int matchLevel, bool isCPU = false, bool returnAll = false, bool isUser = true, int matchStatePosition = -1);
        List<PowerMove> GetAllPowerMoves();
        List<PowerMove> GetEarnablePowerMoves();
        bool AddPowerMove(User user, PowerMoveType powerMoveType);
        bool UpdatePowerMove(User user, PowerMove powerMove);
    }
}
