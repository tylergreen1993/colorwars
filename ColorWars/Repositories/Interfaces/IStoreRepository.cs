﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IStoreRepository
    {
        List<StoreItem> GetStoreItems();
        List<ItemCategory> GetExcludedCategories();
        List<StoreItem> GenerateStoreItems();
        List<StoreItem> SearchStoreItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All);
        bool AddStoreItem(Item item, int cost, int minCost);
        bool DeleteStoreItems();
    }
}