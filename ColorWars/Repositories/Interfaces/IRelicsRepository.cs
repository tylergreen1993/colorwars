﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IRelicsRepository
    {
        List<Relic> GetAllRelics(int level);
        Relic GetRelic(int level);
        bool HasEarned(RelicId relicId, int level);
    }
}
