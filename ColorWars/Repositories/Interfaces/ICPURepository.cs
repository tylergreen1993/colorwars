﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICPURepository
    {
        CPU GetCPU(int level, bool isShuffled = false, bool forceCache = false);
        int GetCPUCount();
        CPU MakeChallenge(CPUChallenge challenge, int reward);
        PowerMoveType GetPowerMove(User user, List<MatchState> matchStates, Settings settings);
        MatchState EnhanceCPULogic(User user, List<MatchState> matchStates, MatchState matchState, Settings settings);
        bool CanChallengeCheckpointCPU(User user, List<Accomplishment> accomplishments, CPU cpu, out DateTime date, out string reason, out string buttonText, out string buttonUrl);
        CPU GetRightSideUpGuard();
        CPU GetShowroomOwner();
        CPU GetPhantomSpirit();
        CPU GetEvena();
        CPU GetOddna();
        CPU GetPhantomBoss();
        CPU GetUnknown();
        CPU GetTowerCPU(int floor);
        CPU GetChallengerDomeCPU(Settings settings);
        CPU GetBeyondWorldCPU(User user, Settings settings);
    }
}
