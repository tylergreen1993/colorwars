﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICavernsRepository
    {
        List<SorceressEffect> GetAllSorceressEffects(Settings settings);
        Item GetPlantedItem(User user, bool isCached = true);
        bool AddPlantedItem(User user, Item item);
        bool DeletePlantedItem(Item item);
        Item GetItem(User user, Item item, CardColor color);
    }
}
