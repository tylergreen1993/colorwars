﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ISeekerRepository
    {
        List<SeekerChallenge> GetSeekerChallenges(User user, bool isCached = true);
        bool AddSeekerChallenge(User user, Item item, Item reward);
        bool DeleteSeekerChallenges(User user);
    }
}
