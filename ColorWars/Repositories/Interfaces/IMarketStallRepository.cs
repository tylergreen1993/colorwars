﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IMarketStallRepository
    {
        List<MarketStall> GetNewMarketStalls(bool isCached = true);
        int GetUserMarketStallId(User user, bool isCached = true);
        MarketStall GetMarketStall(User user);
        MarketStall GetMarketStall(int id);
        List<MarketStallItem> GetMarketStallItems(string query, bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All);
        List<MarketStallHistory> GetMarketStallHistory(MarketStall marketStall);
        List<MarketStallHistory> GetMarketStallHistory(User user);
        bool AddMarketStall(User user, string name, string color, string description, out MarketStall marketStall);
        bool AddMarketStallItem(User user, MarketStall marketStall, Item item, int cost);
        bool AddMarketStallHistory(User user, MarketStall marketStall, MarketStallItem item);
        bool UpdateMarketStall(MarketStall marketStall);
        bool RepairAllMarketStallItems(MarketStall marketStall);
        bool DeleteMarketStall(MarketStall marketStall);
        bool DeleteMarketStallItem(MarketStallItem marketStallItem);
    }
}
