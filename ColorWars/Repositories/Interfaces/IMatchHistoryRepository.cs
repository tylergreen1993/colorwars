﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IMatchHistoryRepository
    {
        List<MatchHistory> GetMatchHistory(User user, bool onlyUsers = false, bool isCached = true);
        MatchRanking GetMatchRanking(User user);
        List<MatchRanking> GetTopMatchRankings();
        List<MatchTag> GetMatchTags(User user, bool isCached = true);
        bool AddMatchHistory(User user, MatchHistory matchHistory, bool isUser = true);
        bool IsElite(User user, MatchRanking matchRanking = null);
    }
}