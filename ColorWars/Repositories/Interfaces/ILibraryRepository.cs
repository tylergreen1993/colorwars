﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ILibraryRepository
    {
        List<LibraryHistory> GetLibraryHistory(User user, bool isCached = true);
        List<VoidMessage> GetVoidMessages();
        bool AddLibraryHistory(User user, LibraryKnowledgeType type, string message);
        bool AddVoidMessage(User user, string message);
        bool CanPullLever(List<Accomplishment> accomplishments);
    }
}
