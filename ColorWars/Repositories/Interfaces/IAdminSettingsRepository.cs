﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IAdminSettingsRepository
    {
        AdminSettings GetSettings(bool isCached = true);
        bool SetSetting(string name, string value);
    }
}
