﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITourneyRepository
    {
        List<Tourney> GetAllTourneys();
        List<Tourney> GetUserTourneys(User user, bool onlyActive = false, bool orderBy = true);
        Tourney GetTourneyWithId(int id);
        Tourney GetTrendingTourney(User user, bool isElite, bool isCached = true);
        bool AddTourney(User user, int entranceCost, int matchesPerRound, int requiredParticipants, bool isSponsored, string tourneyName, TourneyCaliber tourneyCaliber, out Tourney tourney);
        bool AddTourneyMatch(User user, Tourney tourney, int round);
        bool UpdateTourneyMatch(TourneyMatch tourneyMatch);
        bool DeleteTourney(Tourney tourney, bool isExpired = false);
        void AddTourneyProgress(User user, Guid tourneyMatchId, MatchStatus finalStatus, bool isUser);
    }
}
