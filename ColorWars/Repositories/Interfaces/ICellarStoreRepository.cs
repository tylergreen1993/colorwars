﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICellarStoreRepository
    {
		List<CellarStoreItem> GetItems();
	}
}
