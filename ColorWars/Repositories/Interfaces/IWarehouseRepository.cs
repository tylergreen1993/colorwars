﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IWarehouseRepository
    {
        List<StoreItem> GetBulkItems(User user);
        void GenerateNewItems(User user);
    }
}
