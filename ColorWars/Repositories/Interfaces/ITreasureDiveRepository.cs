﻿using System.Drawing;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITreasureDiveRepository
    {
		TreasureDive GetTreasureDive(User user, bool isCached = true);
        bool AddTreasureDive(User user);
        bool UpdateTreasureDive(TreasureDive treasureDive);
        bool DeleteTreasureDive(TreasureDive treasureDive);
    }
}
