﻿using System.IO;
using System.Threading.Tasks;

namespace ColorWars.Repositories
{
    public interface IGoogleStorageRepository
    {
        Task<string> UploadFileAsync(MemoryStream memoryStream, string fileNameForStorage, string contentType);
        Task DeleteFileAsync(string fileNameForStorage);
	}
}