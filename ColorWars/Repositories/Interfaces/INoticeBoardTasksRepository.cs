﻿using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface INoticeBoardTasksRepository
    {
        NoticeBoardTask GetNoticeBoardTask(User user, bool isCached = true);
        bool AddNoticeBoardTask(User user, Item item, ItemCondition itemCondition, bool isExactCondition, bool hasTotalUses, int reward, int itemAgeInDays);
        bool DeleteNoticeBoardTask(User user);
    }
}
