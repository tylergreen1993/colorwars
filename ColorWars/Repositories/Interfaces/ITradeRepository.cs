﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITradeRepository
    {
        List<Trade> GetAllTrades(string query = "", bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All);
        List<Trade> GetTrades(User user, bool onlyActive = false, bool getTradeItems = true);
        List<Trade> GetOffers(User user, bool onlyActive = false, bool getTradeItems = true);
        Trade GetTrade(int id);
        bool AddTrade(User user, Item item1, Item item2, Item item3, string request, out Trade trade);
        bool AddTradeOffer(User user, Trade trade, Item item1, Item item2, Item item3);
        bool CompleteTrade(Trade trade, TradeOffer tradeOffer);
        bool UpdateTrade(Trade trade);
        bool DeleteTrade(Trade trade);
        bool DeleteTradeOffer(TradeOffer tradeOffer);
    }
}