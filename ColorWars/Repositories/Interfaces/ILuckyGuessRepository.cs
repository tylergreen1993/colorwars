﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ILuckyGuessRepository
    {
		List<LuckyGuess> GetLuckyGuesses();
        List<LuckyGuess> GetLuckyGuessTreasures();
        bool AddLuckyGuess(User user, int position, bool isWinner);
    }
}
