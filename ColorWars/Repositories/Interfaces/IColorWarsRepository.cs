﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IColorWarsRepository
    {
        List<ColorWarsSubmission> GetColorWarsSubmissions(ColorWarsColor teamColor);
        List<ColorWarsSubmission> GetColorWarsSubmissions(User user);
        List<ColorWarsHistory> GetColorWarsHistory();
        bool AddColorWarsSubmission(User user, int amount, ColorWarsColor teamColor);
        bool AddColorWarsHistory(ColorWarsColor teamColor, int amount);
        bool CompleteColorWars(User user, out int amountEarned);
        ColorWarsColor GetTeamColor(User user);
    }
}
