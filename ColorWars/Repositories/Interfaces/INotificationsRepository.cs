﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface INotificationsRepository
    {
        List<Notification> GetNotifications(User user, bool markAsRead = true);
        UnreadCount GetUnreadCount(User user);
        int GetUnreadNotificationsCount(User user);
        bool AddNotification(User user, string message, NotificationLocation location, string href, string imageUrl = "", string domain = "");
    }
}
