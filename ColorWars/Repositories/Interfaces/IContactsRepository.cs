﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IContactsRepository
    {
        List<Contact> GetAllContactSubmissions(User currentUser, User user);
        List<Contact> GetAllUnclaimedContacts(User currentUser);
        List<Contact> GetAllCompletedContacts(User currentUser);
        List<Contact> GetClaimedContacts(User user);
        bool AddContact(User user, string emailAddress, string message, ContactCategory category, bool replyViaMessages);
        bool UpdateContact(User currentUser, Contact contact);
    }
}
