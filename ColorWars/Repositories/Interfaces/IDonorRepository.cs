﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IDonorRepository
    {
        List<Donor> GetDonorsWithDays(int days, bool isCached = true);
        List<SponsorCoinPackage> GetSponsorCoinPackages();
        List<SponsorPerk> GetSponsorPerks(User user, Settings settings);
        bool AddDonor(User user, int amount);
	}
}
