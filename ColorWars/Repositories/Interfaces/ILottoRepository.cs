﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ILottoRepository
    {
        List<Lotto> GetLottos();
        bool AddLotto(int currentPot, int winningNumber);
        bool UpdateLotto(Lotto lotto);
    }
}
