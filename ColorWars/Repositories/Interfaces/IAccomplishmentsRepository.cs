﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IAccomplishmentsRepository
    {
        List<Accomplishment> GetAllAccomplishments(bool isCached = true);
        List<Accomplishment> GetAccomplishments(User user, bool isCached = true);
        List<Accomplishment> GetAccomplishmentsRankings();
        int GetAccomplishmentPoints(User user, bool isCached = true);
        bool AddAccomplishment(User user, AccomplishmentId accomplishmentId, bool isUser = true, string domain = "");
    }
}
