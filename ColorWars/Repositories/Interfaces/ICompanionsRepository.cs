﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ICompanionsRepository
    {
        List<Companion> GetCompanions(User user, bool isCached = true);
        List<Companion> GetAdoptionCompanions();
        bool AddCompanion(User user, CompanionType type, string name, CompanionColor color, out Companion companion);
        bool AddAdoptionCompanion(Companion companion);
        bool UpdateCompanion(Companion companion);
        bool DeleteCompanion(User user, Companion companion);
        bool DeleteAdoptionCompanion(Companion companion);
    }
}
