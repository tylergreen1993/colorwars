﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface ITakeOfferRepository
    {
        List<OfferCard> GetOfferCards(User user, bool isCached = true);
        bool AddOfferCard(User user, int position, int points);
        bool DeleteOfferCards(User user);
    }
}
