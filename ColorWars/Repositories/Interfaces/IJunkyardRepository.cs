﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IJunkyardRepository
    {
        List<Item> GetJunkyardItems(bool getAllItems = false);
        List<Item> SearchItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All);
        bool AddJunkyardItem(Item item);
        bool AddSpecialJunkyardItem(Item item);
        bool DeleteJunkyardItem(Item item);
    }
}
