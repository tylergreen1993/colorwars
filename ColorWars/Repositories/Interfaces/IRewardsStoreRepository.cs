﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IRewardsStoreRepository
    {
        List<Reward> GetAllRewards();
    }
}
