﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IChecklistRepository
    {
        List<Checklist> GetChecklist(List<Accomplishment> accomplishments);
        List<Checklist> GetAllEarnedChecklist(List<Accomplishment> accomplishments);
        Checklist GetLastEarnedChecklist(List<Accomplishment> accomplishments);
    }
}
