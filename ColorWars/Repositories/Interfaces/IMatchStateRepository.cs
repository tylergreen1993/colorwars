﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public interface IMatchStateRepository
    {
        List<MatchState> GetMatchStates(User user, bool getInactive = false, bool isCached = true);
        List<MatchState> UpdateMatchState(User user, MatchState matchState, bool returnList = true);
        bool AddMatchState(User user, MatchState matchState);
        MatchWaitlist GetMatchWaitlist(User user);
        List<MatchWaitlist> GetMatchWaitlists(User user, bool isElite);
        MatchWaitlist AddOrUpdateMatchWaitlist(User user, int points, bool isElite, string challengeUser, Guid tourneyMatchId);
        bool DeleteMatchWaitlist(User user);
        bool DeleteMatchState(User user, string opponent, bool isCPU, bool isExpired);
        bool IsInMatch(User user, bool includeWaitlist = true, bool isUser = true);
        int GetWinsNeeded(List<MatchStatus> matchStatuses);
        MatchStatus GetFinalStatus(List<MatchStatus> matchStatuses);
    }
}
