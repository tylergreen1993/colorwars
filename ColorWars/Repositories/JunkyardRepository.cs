﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class JunkyardRepository : IJunkyardRepository
    {
        private IJunkyardPersistence _junkyardPersistence;

        public JunkyardRepository(IJunkyardPersistence junkyardPersistence)
        {
            _junkyardPersistence = junkyardPersistence;
        }

        public List<Item> GetJunkyardItems(bool getAllItems = false)
        {
            return _junkyardPersistence.GetJunkyardItems().FindAll(x => getAllItems || x.Condition < ItemCondition.Unusable);
        }

        public List<Item> SearchItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All)
        {
            List<Item> junkyardItems = GetJunkyardItems().Take(40).ToList();
            if (!string.IsNullOrEmpty(query))
            {
                junkyardItems = matchExactSearch ? junkyardItems.FindAll(x => x.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase)) : junkyardItems.FindAll(x => x.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
            }

            if (category != ItemCategory.All)
            {
                junkyardItems = junkyardItems.FindAll(x => x.Category == category);
            }
            if (condition != ItemCondition.All)
            {
                junkyardItems = junkyardItems.FindAll(x => x.Condition == condition);
            }

            junkyardItems.ForEach(x => x.UpdatePoints((int)Math.Round(0.75 * x.Points)));

            return junkyardItems;
        }

        public bool AddJunkyardItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _junkyardPersistence.AddJunkyardItem(item.Id, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool AddSpecialJunkyardItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            if (item.TotalUses > 0)
            {
                item.Uses = item.TotalUses;
            }
            item.CreatedDate = DateTime.UtcNow;
            item.RepairedDate = DateTime.UtcNow;

            return AddJunkyardItem(item);
        }

        public bool DeleteJunkyardItem(Item item)
        {
            return _junkyardPersistence.DeleteJunkyardItem(item.SelectionId);
        }
    }
}
