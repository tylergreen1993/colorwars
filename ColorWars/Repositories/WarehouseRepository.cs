﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class WarehouseRepository : IWarehouseRepository
    {
        private IWarehousePersistence _warehousePersistence;
        private IItemsRepository _itemsRepository;

        public WarehouseRepository(IWarehousePersistence warehousePersistence, IItemsRepository itemsRepository)
        {
            _warehousePersistence = warehousePersistence;
            _itemsRepository = itemsRepository;
        }

        public List<StoreItem> GetBulkItems(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _warehousePersistence.GetBulkItemsWithUsername(user.Username);
        }

        public void GenerateNewItems(User user)
        {
            if (user == null)
            {
                return;
            }

            if (DeleteBulkItems(user))
            {
                ItemCategory[] excludedItems = { ItemCategory.Egg, ItemCategory.Library, ItemCategory.Companion };
                List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, excludedItems).FindAll(x => x.TotalUses > 0);

                List<Item> bulkItems = new List<Item>();
                Random rnd = new Random();
                while(bulkItems.Count < 12)
                {
                    Item bulkItem = items[rnd.Next(items.Count)];
                    if (!bulkItems.Any(x => x.Id == bulkItem.Id))
                    {
                        bulkItems.Add(bulkItem);
                    }
                }

                foreach (Item item in bulkItems.Distinct())
                {
                    AddBulkItem(user, item);
                }
            }
        }

        private bool AddBulkItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            int cost = (int)Math.Round(item.Points * 5 * 0.85);

            return _warehousePersistence.AddBulkItem(user.Username, item.Id, cost);
        }

        private bool DeleteBulkItems(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _warehousePersistence.DeleteBulkItemsWithUsername(user.Username);
        }
    }
}
