﻿using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ContactsRepository : IContactsRepository
    {
        private IContactsPersistence _contactsPersistence;

        public ContactsRepository(IContactsPersistence contactsPersistence)
        {
            _contactsPersistence = contactsPersistence;
        }

        public List<Contact> GetAllContactSubmissions(User currentUser, User user)
        {
            if (user == null || currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _contactsPersistence.GetAllContactSubmissionsWithUsername(user.Username);
        }

        public List<Contact> GetAllUnclaimedContacts(User currentUser)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _contactsPersistence.GetAllUnclaimedContacts();
        }

        public List<Contact> GetAllCompletedContacts(User currentUser)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _contactsPersistence.GetAllClaimedContacts();
        }

        public List<Contact> GetClaimedContacts(User user)
        {
            if (user == null)
            {
                return null;
            }

            if (user.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _contactsPersistence.GetClaimedContactsWithUsername(user.Username);
        }

        public bool AddContact(User user, string emailAddress, string message, ContactCategory category, bool replyViaMessages)
        {
            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(message))
            {
                return false;
            }

            string ipAddress = GlobalHttpContext.Current.Connection.RemoteIpAddress.ToString();

            return _contactsPersistence.AddContact(user?.Username ?? string.Empty, emailAddress, ipAddress, message, category, replyViaMessages);
        }

        public bool UpdateContact(User currentUser, Contact contact)
        {
            if (currentUser == null || contact == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _contactsPersistence.UpdateContact(contact);
        }
    }
}
