﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class ChecklistRepository : IChecklistRepository
    {
        private List<Checklist> _checklist = new List<Checklist>();

        public ChecklistRepository()
        {
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.VisitBag,
                Href = "/Items",
                Description = $"visit your bag",
                Rank = ChecklistRank.Beginner
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.VisitPlaza,
                Href = "/Plaza",
                Description = $"visit the Plaza",
                Rank = ChecklistRank.Beginner
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.VisitFair,
                Href = "/Fair",
                Description = "explore the Fair",
                Rank = ChecklistRank.Beginner
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.SpinSuccessPositive,
                Href = "/SpinSuccess",
                Description = "spin the wheel at Spin of Success",
                Rank = ChecklistRank.Beginner
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CPULevelTwo,
                Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                Description = "defeat the Level 2 CPU Challenger",
                Rank = ChecklistRank.Beginner
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.UpgradedCard,
                Href = "/TrainingCenter",
                Description = $"upgrade a {Resources.DeckCard} at the Training Center",
                Rank = ChecklistRank.Novice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.StreakCardsWin,
                Href = "/StreakCards",
                Description = $"earn {Resources.Currency} playing Streak Cards",
                Rank = ChecklistRank.Novice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.SetUpProfile,
                Href = "/Preferences",
                Description = "update your profile",
                Rank = ChecklistRank.Novice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.ImprovedDeckCardSwap,
                Href = "/SwapDeck",
                Description = $"swap your {Resources.DeckCard} for something better",
                Rank = ChecklistRank.Novice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.StockBuy,
                Href = "/StockMarket",
                Description = "buy a stock",
                Rank = ChecklistRank.Novice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.SuccessfulBargain,
                Href = "/Store",
                Description = "bargain an item's cost at the General Store",
                Rank = ChecklistRank.Apprentice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.OpenedBank,
                Href = "/Bank",
                Description = "open a bank account",
                Rank = ChecklistRank.Apprentice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.OpenedMarketStall,
                Href = "/MarketStalls/Create",
                Description = "create a Market Stall",
                Rank = ChecklistRank.Apprentice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.GroupJoined,
                Href = "/Groups",
                Description = "join or form a group",
                Rank = ChecklistRank.Apprentice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CPULevelFive,
                Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                Description = "defeat the Level 5 Checkpoint CPU",
                Rank = ChecklistRank.Apprentice
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.ShowroomAccess,
                Href = "/BuyersClub/Showroom",
                Description = "gain access to the Showroom Store",
                Rank = ChecklistRank.Intermediate
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.AuctionParticipant,
                Href = "/Auction",
                Description = "create or place a bid on an auction",
                Rank = ChecklistRank.Intermediate
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.JoinedClub,
                Href = "/Club",
                Description = $"join the {Resources.SiteName} Club",
                Rank = ChecklistRank.Intermediate
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.FirstCrafting,
                Href = "/Crafting",
                Description = "craft an item",
                Rank = ChecklistRank.Intermediate
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CPULevelTen,
                Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                Description = "defeat the Level 10 Checkpoint CPU",
                Rank = ChecklistRank.Intermediate
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.FirstSeeker,
                Href = "/Seeker",
                Description = "complete a task for The Seeker",
                Rank = ChecklistRank.Advanced
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.UsedTheme,
                Href = "/Items",
                Description = $"change a {Resources.DeckCard}'s theme using a Theme Card",
                Rank = ChecklistRank.Advanced
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.SmallFortuneBank,
                Href = "/Bank",
                Description = $"reach {Helper.GetFormattedPoints(100000)} in your bank account",
                Rank = ChecklistRank.Advanced
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CompanionActivated,
                Href = "/Profile",
                Description = "add a Companion to your profile",
                Rank = ChecklistRank.Advanced
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CPULevelTwenty,
                Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                Description = "defeat the Level 20 Checkpoint CPU",
                Rank = ChecklistRank.Advanced
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CompletedGuideSection,
                Href = "/Items/Guide",
                Description = "complete a section of the Collector's Guide",
                Rank = ChecklistRank.Expert
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.MazeUnlocked,
                Href = "/SwapDeck/Maze",
                Description = "unlock the Maze",
                Rank = ChecklistRank.Expert
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.LibraryCardsCollector,
                Href = $"/Library/LibraryCards",
                Description = "collect a full set of 6 Library Cards",
                Rank = ChecklistRank.Expert
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.CPUCompleted,
                Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                Description = "defeat all the CPU Challengers",
                Rank = ChecklistRank.Expert
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.TowerFloorFive,
                Href = $"/Tower",
                Description = "reach Floor 5 of the Tower",
                Rank = ChecklistRank.Expert
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.ShowroomPlatinum,
                Href = $"/Profile/Showroom",
                Description = $"get a Platinum certified Showroom",
                Rank = ChecklistRank.Elite
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.PhantomBossDefeated,
                Href = $"/PhantomDoor",
                Description = "defeat the Phantom Boss",
                Rank = ChecklistRank.Elite
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.TowerFloorTwentyFive,
                Href = $"/Tower",
                Description = "reach Floor 25 of the Tower",
                Rank = ChecklistRank.Elite
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.DefeatBeyondCreature,
                Href = $"/BeyondWorld",
                Description = "close the Beyond World",
                Rank = ChecklistRank.Elite
            });
            _checklist.Add(new Checklist
            {
                AccomplishmentId = AccomplishmentId.WealthyDonorMillionaireDonation,
                Href = $"/WealthyDonors",
                Description = $"donate {Helper.GetFormattedPoints(1000000)} as a Wealthy Donor",
                Rank = ChecklistRank.Elite
            });
        }

        public List<Checklist> GetChecklist(List<Accomplishment> accomplishments)
        {
            UpdateChecklist(accomplishments);

            if (_checklist.All(x => x.Completed))
            {
                return null;
            }

            foreach (Checklist checklist in _checklist)
            {
                if (!checklist.Completed)
                {
                    return _checklist.FindAll(x => x.Rank == checklist.Rank).OrderBy(x => x.Completed).ToList();
                }
            }

            return null;
        }

        public List<Checklist> GetAllEarnedChecklist(List<Accomplishment> accomplishments)
        {
            UpdateChecklist(accomplishments);

            return _checklist.FindAll(x => x.Completed);
        }

        public Checklist GetLastEarnedChecklist(List<Accomplishment> accomplishments)
        {
            UpdateChecklist(accomplishments);

            return _checklist.OrderBy(x => x.EarnedDate).LastOrDefault();
        }

        private void UpdateChecklist(List<Accomplishment> accomplishments)
        {
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.UnknownDefeated))
            {
                _checklist.RemoveAll(x => x.Rank == ChecklistRank.Elite);
            }

            foreach (Checklist checklist in _checklist)
            {
                if (checklist.Completed)
                {
                    continue;
                }
                foreach (Accomplishment accomplishment in accomplishments)
                {
                    if (accomplishment.Id == checklist.AccomplishmentId)
                    {
                        checklist.Completed = true;
                        checklist.EarnedDate = accomplishment.ReceivedDate;
                        break;
                    }
                }
            }
        }
    }
}