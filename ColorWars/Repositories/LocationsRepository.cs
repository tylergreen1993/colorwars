﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace ColorWars.Repositories
{
    public class LocationsRepository : ILocationsRepository
    {
        private ILocationsPersistence _locationsPersistence;
        private IActionContextAccessor _actionContextAccessor;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private UrlHelper _urlHelper;

        public LocationsRepository(ILocationsPersistence locationsPersistence, IActionContextAccessor actionContextAccessor,
        IAccomplishmentsRepository accomplishmentsRepository)
        {
            _locationsPersistence = locationsPersistence;
            _actionContextAccessor = actionContextAccessor;
            _accomplishmentsRepository = accomplishmentsRepository;
            _urlHelper = new UrlHelper(_actionContextAccessor.ActionContext);
        }

        public List<Location> GetFavoritedLocations(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _locationsPersistence.GetFavoritedLocations(user.Username, isCached);
        }

        public List<Location> GetFavoritedPlazaList(User user, Settings settings, bool isCached = true)
        {
            List<Location> locations = GetFavoritedLocations(user, isCached);
            if (locations.Count == 0)
            {
                return locations;
            }

            List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user, isCached);
            List<Location> allLocations = GetLocations(settings, accomplishments, LocationArea.None, true);
            List<Location> favoritedLocations = new List<Location>();
            foreach (Location location in locations)
            {
                Location favoritedLocation = allLocations.Find(x => x.LocationId == location.LocationId);
                if (favoritedLocation != null)
                {
                    favoritedLocation.Position = location.Position;
                    favoritedLocations.Add(favoritedLocation);
                }
            }
            return favoritedLocations.OrderBy(x => x.Position).ToList();
        }

        public List<Location> GetLocations(Settings settings, List<Accomplishment> accomplishments, LocationArea locationArea, bool getAllSpecialLocations)
        {
            List<Location> locations = new List<Location>
            {
                new Location
                {
                    Name = "Bank",
                    Description = $"Withdraw or deposit {Resources.Currency} and collect interest.",
                    ImageUrl = Helper.GetImageUrl("Locations/Bank.png"),
                    Url = _urlHelper.Action("Index", "Bank"),
                    WidthToHeightRatio = 1.01,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Bank,
                    LocationType = LocationType.Improvements,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Loans" }
                },
                new Location
                {
                    Name = "Auction House",
                    Description = $"Bid on items up for auction or put up your own.",
                    ImageUrl = Helper.GetImageUrl("Locations/Auction.png"),
                    Url = _urlHelper.Action("Index", "Auction"),
                    WidthToHeightRatio = 1.28,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.AuctionHouse,
                    LocationType = LocationType.Shops
                },
                new Location
                {
                    Name = "General Store",
                    Description = $"For all your Deck improvement and item needs!",
                    ImageUrl = Helper.GetImageUrl("Locations/Store.png"),
                    Url = _urlHelper.Action("Index", "Store"),
                    WidthToHeightRatio = 1.13,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.GeneralStore,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = "Junkyard",
                    Description = $"One man's trash is... well, you know the saying.",
                    ImageUrl = Helper.GetImageUrl("Locations/Junk.png"),
                    Url = _urlHelper.Action("Index", "Junkyard"),
                    WidthToHeightRatio = 0.71,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Junkyard,
                    LocationType = LocationType.Shops
                },
                new Location
                {
                    Name = "Repair Shop",
                    Description = $"Get your worn out items looking like brand new!",
                    ImageUrl = Helper.GetImageUrl("Locations/Repair.png"),
                    Url = _urlHelper.Action("Index", "Repair"),
                    WidthToHeightRatio = 1.66,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.RepairShop,
                    LocationType = LocationType.Improvements,
                    Tags = { "Recharge" }
                },
                new Location
                {
                    Name = "Crafting",
                    Description = $"Use Crafting Cards to make new Deck Cards and Enhancers.",
                    ImageUrl = Helper.GetImageUrl("Locations/Crafting.png"),
                    Url = _urlHelper.Action("Index", "Crafting"),
                    WidthToHeightRatio = 1.32,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Crafting,
                    LocationType = LocationType.Improvements,
                    Tags = { "Furnace" }
                },
                new Location
                {
                    Name = "Stock Market",
                    Description = $"Invest your {Resources.Currency} in all sorts of stocks.",
                    ImageUrl = Helper.GetImageUrl("Locations/Stocks.png"),
                    Url = _urlHelper.Action("Index", "StockMarket"),
                    WidthToHeightRatio = 1.03,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.StockMarket,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Portfolio" }
                },
                new Location
                {
                    Name = "Storage",
                    Description = $"Safely store your extra items.",
                    ImageUrl = Helper.GetImageUrl("Locations/Storage.png"),
                    Url = _urlHelper.Action("Index", "Storage"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Storage,
                    LocationType = LocationType.Improvements,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = "News",
                    Description = $"All your {Resources.SiteName} news and updates!",
                    ImageUrl = Helper.GetImageUrl("Locations/News.png"),
                    Url = _urlHelper.Action("Index", "News"),
                    WidthToHeightRatio = 1.18,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.News,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Horoscope" }
                },
                new Location
                {
                    Name = "Trading Square",
                    Description = $"Trade your items with other users.",
                    ImageUrl = Helper.GetImageUrl("Locations/Trading.png"),
                    Url = _urlHelper.Action("Index", "Trading"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.TradingSquare,
                    LocationType = LocationType.Shops
                },
                new Location
                {
                    Name = "Donation Zone",
                    Description = $"Donate items or pick up freebies kindly donated by others.",
                    ImageUrl = Helper.GetImageUrl("Locations/Donate.png"),
                    Url = _urlHelper.Action("Index", "Donations"),
                    WidthToHeightRatio = 0.94,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.DonationZone,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = "Lotto",
                    Description = $"Enter the lotto for a chance to win it all!",
                    ImageUrl = Helper.GetImageUrl("Locations/Lotto.png"),
                    Url = _urlHelper.Action("Index", "Lotto"),
                    WidthToHeightRatio = 0.92,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Lotto,
                    LocationType = LocationType.Games,
                    Tags = { "Egg Cards" }
                },
                new Location
                {
                    Name = "Seeker's Hut",
                    Description = $"You never know what they might want...",
                    ImageUrl = Helper.GetImageUrl("Locations/Seeker.png"),
                    Url = _urlHelper.Action("Index", "Seeker"),
                    WidthToHeightRatio = 1.13,
                    LocationArea = LocationArea.Plaza,
                    LocationType = LocationType.Games,
                    LocationId = LocationId.SeekersHut
                },
                new Location
                {
                    Name = "Wishing Stone",
                    Description = $"Make a wish to the Stone and it might come true!",
                    ImageUrl = Helper.GetImageUrl("Locations/WishingStone.png"),
                    Url = _urlHelper.Action("Index", "WishingStone"),
                    WidthToHeightRatio = 1.27,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.WishingStone,
                    LocationType = LocationType.Games,
                    Tags = { "Quarry" }
                },
                new Location
                {
                    Name = "Young Wizard",
                    Description = $"His powers range from really good to sometimes bad.",
                    ImageUrl = Helper.GetImageUrl("Locations/Wizard.png"),
                    Url = _urlHelper.Action("Index", "Wizard"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.YoungWizard,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Records" }
                },
                new Location
                {
                    Name = "Groups",
                    Description = $"Join a group or form your own and recruit others!",
                    ImageUrl = Helper.GetImageUrl("Locations/Groups.png"),
                    Url = _urlHelper.Action("Index", "Groups"),
                    WidthToHeightRatio = 0.83,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Groups,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = "Rankings",
                    Description = $"See how the top users rank financially and at the Stadium.",
                    ImageUrl = Helper.GetImageUrl("Locations/Ranking.png"),
                    Url = _urlHelper.Action("Index", "Rankings"),
                    WidthToHeightRatio = 0.77,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Rankings,
                    LocationType = LocationType.Knowledge
                },
                new Location
                {
                    Name = "Timed Showcase",
                    Description = $"A discounted item is showcased for a brief time.",
                    ImageUrl = Helper.GetImageUrl("Locations/Showcase.png"),
                    Url = _urlHelper.Action("Index", "Showcase"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.TimedShowcase,
                    LocationType = LocationType.Shops,
                    Tags = { "Ager" }
                },
                new Location
                {
                    Name = "Job Center",
                    Description = $"Improve {Resources.SiteName} and earn rewards or apply to become a Helper!",
                    ImageUrl = Helper.GetImageUrl("Locations/JobCenter.png"),
                    Url = _urlHelper.Action("Index", "Jobs"),
                    WidthToHeightRatio = 1.2,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.JobCenter,
                    LocationType = LocationType.Knowledge,
                    Tags = { "Helpers" }
                },
                new Location
                {
                    Name = "Buyers Club",
                    Description = $"Earn discounts when you buy at this {Resources.SiteName} store!",
                    ImageUrl = Helper.GetImageUrl("Locations/Buyers.png"),
                    Url = _urlHelper.Action("Index", "BuyersClub"),
                    WidthToHeightRatio = 1.31,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.BuyersClub,
                    LocationType = LocationType.Shops,
                    Tags = { "Showroom Store" }
                },
                new Location
                {
                    Name = "Referrals",
                    Description = $"Refer new users and earn {Resources.Currency} when they sign up!",
                    ImageUrl = Helper.GetImageUrl("Locations/Referral.png"),
                    Url = _urlHelper.Action("Index", "Referrals"),
                    WidthToHeightRatio = 1.08,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Referrals,
                    LocationType = LocationType.Knowledge,
                    Tags = { "Leaders" }
                },
                new Location
                {
                    Name = "Active Effects",
                    Description = $"See all your active effects and optionally remove them.",
                    ImageUrl = Helper.GetImageUrl("Locations/ActiveEffects.png"),
                    Url = _urlHelper.Action("Index", "Effects"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.ActiveEffects,
                    LocationType = LocationType.Knowledge
                },
                new Location
                {
                    Name = "Promo Codes",
                    Description = $"Redeem any promo codes you've earned.",
                    ImageUrl = Helper.GetImageUrl("Locations/PromoCodes.png"),
                    Url = _urlHelper.Action("Index", "PromoCodes"),
                    WidthToHeightRatio = 1.14,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.PromoCodes,
                    LocationType = LocationType.Games
                },
                new Location
                {
                    Name = "Market Stalls",
                    Description = $"Buy items from other users' stalls or create your own.",
                    ImageUrl = Helper.GetImageUrl("Locations/MarketStalls.png"),
                    Url = _urlHelper.Action("Index", "MarketStalls"),
                    WidthToHeightRatio = 0.85,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.MarketStalls,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = "Disposer",
                    Description = $"Permanently dispose of your items and earn Training Tokens.",
                    ImageUrl = Helper.GetImageUrl("Locations/Disposer.png"),
                    Url = _urlHelper.Action("Index", "Disposer"),
                    WidthToHeightRatio = 1.02,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Disposer,
                    LocationType = LocationType.Improvements,
                    Tags = { "Records" }
                },
                new Location
                {
                    Name = "Library",
                    Description = $"Gain knowledge and earn rewards.",
                    ImageUrl = Helper.GetImageUrl("Locations/Library.png"),
                    Url = _urlHelper.Action("Index", "Library"),
                    WidthToHeightRatio = 1.2,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Library,
                    LocationType = LocationType.Games,
                    Tags = { "Library Cards" }
                },
                new Location
                {
                    Name = "Attention Hall",
                    Description = $"Boast about your successes or comment on other posts!",
                    ImageUrl = Helper.GetImageUrl("Locations/AttentionHall.png"),
                    Url = _urlHelper.Action("Index", "AttentionHall"),
                    WidthToHeightRatio = 0.97,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.AttentionHall,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Popular
                },
                new Location
                {
                    Name = $"{Resources.SiteName} Club",
                    Description = $"Become a member and gain exclusive perks!",
                    ImageUrl = Helper.GetImageUrl("Locations/Club.png"),
                    Url = _urlHelper.Action("Index", "Club"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Club,
                    LocationType = LocationType.Improvements,
                    Tags = { "Incubator" }
                },
                new Location
                {
                    Name = "Fair",
                    Description = $"Visit booths and play games to earn {Resources.Currency}!",
                    ImageUrl = Helper.GetImageUrl("Locations/Fair.png"),
                    Url = _urlHelper.Action("Index", "Fair"),
                    WidthToHeightRatio = 1.35,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Fair,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Streak Cards", "Matching Colors", "High Low", "Color Wars", "Spin of Success", "Take the Offer", "Swap Deck", "The Maze", "Crystal Ball", "Prize Cup", "Treasure Dive", "On The Hour", "Wealthy Donors", "Challenger Dome", "DeoJack", "Top Counter", "Doubt It", "Lucky Guess" }
                },
                new Location
                {
                    Name = "Hall of Helpers",
                    Description = $"See who's part of the {Resources.SiteName} Helpers and Staff team.",
                    ImageUrl = Helper.GetImageUrl("Locations/Hall.png"),
                    Url = _urlHelper.Action("Index", "Hall"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.HallOfHelpers,
                    LocationType = LocationType.Knowledge
                },
                new Location
                {
                    Name = $"Unusable Store",
                    Description = $"Buy unwanted items at a discount.",
                    ImageUrl = Helper.GetImageUrl("Locations/UnusableStore.png"),
                    Url = _urlHelper.Action("Index", "UnusableStore"),
                    WidthToHeightRatio = 0.59,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.UnusableStore,
                    LocationType = LocationType.Shops
                },
                new Location
                {
                    Name = $"Training Center",
                    Description = $"Use Training Tokens to upgrade your {Resources.DeckCards}.",
                    ImageUrl = Helper.GetImageUrl("Locations/TrainingCenter.png"),
                    Url = _urlHelper.Action("Index", "TrainingCenter"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.TrainingCenter,
                    LocationType = LocationType.Improvements,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "Ancient Plateau" }
                },
                new Location
                {
                    Name = $"Weary Villager",
                    Description = $"Earn rewards for collecting red {Resources.DeckCards}.",
                    ImageUrl = Helper.GetImageUrl("Locations/Villager.png"),
                    Url = _urlHelper.Action("Index", "Villager"),
                    WidthToHeightRatio = 1.2,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Villager,
                    LocationType = LocationType.Games,
                    Tags = { "Theme Squeezer" }
                },
                new Location
                {
                    Name = $"Top Adoptions",
                    Description = $"Adopt a Companion and give them a new home!",
                    ImageUrl = Helper.GetImageUrl("Locations/TopAdoptions.png"),
                    Url = _urlHelper.Action("Index", "TopAdoptions"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.TopAdoptions,
                    LocationType = LocationType.Shops,
                    Tags = { "Cellar" }
                },
                new Location
                {
                    Name = $"Secret Shrine",
                    Description = $"A hidden shrine that rewards those who donate.",
                    ImageUrl = Helper.GetImageUrl("Locations/SecretShrine.png"),
                    Url = _urlHelper.Action("Index", "SecretShrine"),
                    WidthToHeightRatio = 0.73,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.SecretShrine,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Secret
                },
                new Location
                {
                    Name = "Streak Cards",
                    Description = $"The more days you play, the more you'll win!",
                    ImageUrl = Helper.GetImageUrl("Locations/StreakCards.png"),
                    Url = _urlHelper.Action("Index", "StreakCards"),
                    WidthToHeightRatio = 0.64,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.StreakCards,
                    LocationCategory = LocationCategory.Popular,
                    IsFaded = (DateTime.UtcNow - settings.LastStreakCardsPlayDate).TotalHours < 8
                },
                new Location
                {
                    Name = "Matching Colors",
                    Description = $"Match three of the same color and earn {Resources.Currency}!",
                    ImageUrl = Helper.GetImageUrl("Locations/MatchingColors.png"),
                    Url = _urlHelper.Action("Index", "MatchingColors"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.MatchingColors,
                    IsFaded = (DateTime.UtcNow - settings.LastMatchingColorsPlayDate).TotalHours < 8
                },
                new Location
                {
                    Name = "High Low",
                    Description = $"Bet on whether a number will be higher or lower!",
                    ImageUrl = Helper.GetImageUrl("Locations/HighLow.png"),
                    Url = _urlHelper.Action("Index", "HighLow"),
                    WidthToHeightRatio = 1.01,
                    LocationId = LocationId.HighLow,
                    LocationCategory = LocationCategory.NoLimit,
                    LocationArea = LocationArea.Fair
                },
                new Location
                {
                    Name = "Color Wars",
                    Description = $"Add {Resources.Currency} to your color's pot and beat the other team!",
                    ImageUrl = Helper.GetImageUrl("Locations/ColorWars.png"),
                    Url = _urlHelper.Action("Index", "ColorWars"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.ColorWars,
                    IsFaded = (DateTime.UtcNow - settings.LastColorWarsContributeDate).TotalHours < 3
                },
                new Location
                {
                    Name = "Spin of Success",
                    Description = $"Spin the wheel and you might get lucky!",
                    ImageUrl = Helper.GetImageUrl("Locations/SpinSuccess.png"),
                    Url = _urlHelper.Action("Index", "SpinSuccess"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.SpinSuccess,
                    LocationCategory = LocationCategory.Popular,
                    IsFaded = (DateTime.UtcNow - settings.LastSpinSuccessDate).TotalHours < 3
                },
                new Location
                {
                    Name = "Take the Offer",
                    Description = $"Try to take the best {Resources.Currency} offer you're given!",
                    ImageUrl = Helper.GetImageUrl("Locations/TakeOffer.png"),
                    Url = _urlHelper.Action("Index", "TakeOffer"),
                    WidthToHeightRatio = 1.02,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.TakeOffer,
                    LocationCategory = LocationCategory.Popular,
                    IsFaded = (DateTime.UtcNow - settings.LastTakeOfferPlayDate).TotalHours < 12
                },
                new Location
                {
                    Name = "Swap Deck",
                    Description = $"One of your {Resources.DeckCards} is swapped for something new!",
                    ImageUrl = Helper.GetImageUrl("Locations/SwapDeck.png"),
                    Url = _urlHelper.Action("Index", "SwapDeck"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.SwapDeck,
                    Tags = { "The Maze" },
                    IsFaded = (DateTime.UtcNow - settings.LastSwapDeckDate).TotalDays < 1
                },
                new Location
                {
                    Name = "Crystal Ball",
                    Description = $"Remember the order of the colors to earn {Resources.Currency}!",
                    ImageUrl = Helper.GetImageUrl("Locations/CrystalBall.png"),
                    Url = _urlHelper.Action("Index", "CrystalBall"),
                    WidthToHeightRatio = 0.77,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.CrystalBall,
                    LocationCategory = LocationCategory.Popular,
                    IsFaded = (DateTime.UtcNow - settings.LastCrystalBallPlayDate).TotalHours < 6
                },
                new Location
                {
                    Name = "Prize Cup",
                    Description = $"Flip the cups and get the right colored marbles to win big!",
                    ImageUrl = Helper.GetImageUrl("Locations/PrizeCup.png"),
                    Url = _urlHelper.Action("Index", "PrizeCup"),
                    WidthToHeightRatio = 0.74,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.PrizeCup,
                    IsFaded = (DateTime.UtcNow - settings.LastPrizeCupPlayDate).TotalHours < 5
                },
                new Location
                {
                    Name = "Treasure Dive",
                    Description = $"Find the hidden treasure on the map!",
                    ImageUrl = Helper.GetImageUrl("Locations/TreasureDive.png"),
                    Url = _urlHelper.Action("Index", "TreasureDive"),
                    WidthToHeightRatio = 1.28,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.TreasureDive,
                    LocationCategory = LocationCategory.Popular,
                    Tags = { "On The Hour" },
                    IsFaded = (DateTime.UtcNow - settings.LastTreasureDivePlayDate).TotalHours < 6
                },
                new Location
                {
                    Name = "Wealthy Donors",
                    Description = $"Earn free {Resources.Currency} from {Resources.SiteName} philanthropists!",
                    ImageUrl = Helper.GetImageUrl("Locations/WealthyDonors.png"),
                    Url = _urlHelper.Action("Index", "WealthyDonors"),
                    WidthToHeightRatio = 0.56,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.WealthyDonors,
                    IsFaded = (DateTime.UtcNow - settings.LastWealthyDonorsTakeDate).TotalHours < 8
                },
                new Location
                {
                    Name = "DeoJack",
                    Description = $"Beat the dealer's hand without going over 21!",
                    ImageUrl = Helper.GetImageUrl("Locations/DeoJack.png"),
                    Url = _urlHelper.Action("Index", "DeoJack"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.DeoJack,
                    LocationCategory = LocationCategory.NoLimit
                },
                new Location
                {
                    Name = "Top Counter",
                    Description = $"Count the total value of the cards to earn {Resources.Currency}!",
                    ImageUrl = Helper.GetImageUrl("Locations/TopCounter.png"),
                    Url = _urlHelper.Action("Index", "TopCounter"),
                    WidthToHeightRatio = 0.8,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.TopCounter,
                    IsFaded = (DateTime.UtcNow - settings.LastTopCounterPlayDate).TotalHours < 1
                },
                new Location
                {
                    Name = "Doubt It",
                    Description = $"Get rid of your cards before everyone else!",
                    ImageUrl = Helper.GetImageUrl("Locations/DoubtIt.png"),
                    Url = _urlHelper.Action("Index", "DoubtIt"),
                    WidthToHeightRatio = 1.05,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.DoubtIt,
                    LocationCategory = LocationCategory.NoLimit
                },
                new Location
                {
                    Name = "Lucky Guess",
                    Description = $"Be the first to find the hidden {Resources.Currency} to win!",
                    ImageUrl = Helper.GetImageUrl("Locations/LuckyGuess.png"),
                    Url = _urlHelper.Action("Index", "LuckyGuess"),
                    WidthToHeightRatio = 1.1,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.LuckyGuess,
                    LocationCategory = LocationCategory.Popular,
                    IsFaded = (DateTime.UtcNow - settings.LuckyGuessLastPlayDate).TotalHours < 6
                }
            };

            locations.Add(new Location
            {
                Name = "Sponsor Society",
                Description = $"Buy Sponsor Coins to get perks and support {Resources.SiteName}!",
                ImageUrl = Helper.GetImageUrl("Locations/SponsorSociety.png"),
                Url = _urlHelper.Action("Index", "SponsorSociety"),
                WidthToHeightRatio = 1.13,
                LocationArea = LocationArea.Plaza,
                LocationId = LocationId.SponsorSociety,
                LocationType = LocationType.Knowledge,
                LocationCategory = LocationCategory.Special,
                Tags = new List<string> { "Coin Catalog" }
            });
            if (accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                locations.Add(new Location
                {
                    Name = "The Tower",
                    Description = $"Face off against as many CPUs as you can to climb the Tower!",
                    ImageUrl = Helper.GetImageUrl("Locations/Tower.png"),
                    Url = _urlHelper.Action("Index", "Tower"),
                    WidthToHeightRatio = 0.96,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Tower,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.MatchLevel >= 2)
            {
                locations.Add(new Location
                {
                    Name = "Tourney",
                    Description = $"Join a tourney or create your own and emerge as victor!",
                    ImageUrl = Helper.GetImageUrl("Locations/Tourney.png"),
                    Url = _urlHelper.Action("Index", "Tourney"),
                    WidthToHeightRatio = 0.84,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Tourney,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.MatchLevel >= 5)
            {
                locations.Add(new Location
                {
                    Name = "Challenger Dome",
                    Description = $"Challenge CPUs and earn more {Resources.Currency} each day you play!",
                    ImageUrl = Helper.GetImageUrl("Locations/ChallengerDome.png"),
                    Url = _urlHelper.Action("Index", "ChallengerDome"),
                    WidthToHeightRatio = 1.7,
                    LocationArea = LocationArea.Fair,
                    LocationId = LocationId.ChallengerDome,
                    IsFaded = (DateTime.UtcNow - settings.LastChallengerDomeMatchDate).TotalMinutes < 15
                });
            }
            if (settings.MatchLevel >= 8 && settings.PhantomDoorLevel <= 10)
            {
                locations.Add(new Location
                {
                    Name = "Phantom Door",
                    Description = $"Great mysteries lie beyond this door...",
                    ImageUrl = Helper.GetImageUrl("Locations/PhantomDoor.png"),
                    Url = _urlHelper.Action("Index", "PhantomDoor"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.PhantomDoor,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.MatchLevel >= 15)
            {
                locations.Add(new Location
                {
                    Name = "Art Factory",
                    Description = $"Make your own custom Art Cards for display or to sell to others!",
                    ImageUrl = Helper.GetImageUrl("Locations/ArtFactory.png"),
                    Url = _urlHelper.Action("Index", "ArtFactory"),
                    WidthToHeightRatio = 1.06,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.ArtFactory,
                    LocationType = LocationType.Improvements,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.MatchLevel >= 25)
            {
                locations.Add(new Location
                {
                    Name = "Clubhouse",
                    Description = $"A special club for the richest {Resources.SiteName} members!",
                    ImageUrl = Helper.GetImageUrl("Locations/Clubhouse.png"),
                    Url = _urlHelper.Action("Index", "Clubhouse"),
                    WidthToHeightRatio = 0.97,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Clubhouse,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated) && settings.BeyondWorldLevel <= 7)
            {
                locations.Add(new Location
                {
                    Name = "Beyond World",
                    Description = $"The Young Wizard accidentally opened it up...",
                    ImageUrl = Helper.GetImageUrl("Locations/BeyondWorld.png"),
                    Url = _urlHelper.Action("Index", "BeyondWorld"),
                    WidthToHeightRatio = 1.06,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.BeyondWorld,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.StoreMagicianExpiryDate > DateTime.UtcNow)
            {
                locations.Add(new Location
                {
                    Name = "Store Magician",
                    Description = $"Quickly search for an item across all stores at once!",
                    ImageUrl = Helper.GetImageUrl("Locations/StoreMagician.png"),
                    Url = _urlHelper.Action("Index", "StoreMagician"),
                    WidthToHeightRatio = 1.16,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.StoreMagician,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Special
                });
            }
            if (settings.MerchantsExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Traveling Merchants",
                    Description = $"Enter a contest or sell your Jewel Cards.",
                    ImageUrl = Helper.GetImageUrl("Locations/Merchants.png"),
                    Url = _urlHelper.Action("Index", "Merchants"),
                    WidthToHeightRatio = 1.04,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Merchants,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Limited,
                    Tags = { "Jewel Cards" }
                });
            }
            if (settings.RewardsStoreExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Rewards Store",
                    Description = $"Redeem points earned from accomplishments.",
                    ImageUrl = Helper.GetImageUrl("Locations/RewardsStore.png"),
                    Url = _urlHelper.Action("Index", "RewardsStore"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.RewardsStore,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Limited
                });
            }
            if (settings.NoticeBoardExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Notice Board",
                    Description = $"Complete posted tasks and earn {Resources.Currency}.",
                    ImageUrl = Helper.GetImageUrl("Locations/NoticeBoard.png"),
                    Url = _urlHelper.Action("Index", "NoticeBoard"),
                    WidthToHeightRatio = 1.2,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.NoticeBoard,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Limited
                });
            }
            if (settings.WarehouseExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Warehouse",
                    Description = $"Get access to special item offers.",
                    ImageUrl = Helper.GetImageUrl("Locations/Warehouse.png"),
                    Url = _urlHelper.Action("Index", "Warehouse"),
                    WidthToHeightRatio = 1.35,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Warehouse,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Limited
                });
            }
            if (settings.DeepCavernsExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Deep Caverns",
                    Description = $"Strange things live in these dark depths.",
                    ImageUrl = Helper.GetImageUrl("Locations/Caverns.png"),
                    Url = _urlHelper.Action("Index", "Caverns"),
                    WidthToHeightRatio = 1.12,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Caverns,
                    LocationType = LocationType.Improvements,
                    LocationCategory = LocationCategory.Limited,
                    Tags = { "Sorceress", "Seed Cards" }
                });
            }
            if (settings.RightSideUpWorldExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Enchanted Portal",
                    Description = $"A portal to the Right-Side Up World.",
                    ImageUrl = Helper.GetImageUrl("Locations/Portal.png"),
                    Url = _urlHelper.Action("Index", "RightSideUpWorld"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.RightSideUpWorld,
                    LocationType = LocationType.Shops,
                    LocationCategory = LocationCategory.Limited,
                    Tags = { "Enchanted Store", "Right-Side Up Guard" }
                });
            }
            if (settings.SecretGifterExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Secret Gifter",
                    Description = $"Give gifts to users and you might get one back!",
                    ImageUrl = Helper.GetImageUrl("Locations/SecretGifter.png"),
                    Url = _urlHelper.Action("Index", "SecretGifter"),
                    WidthToHeightRatio = 1,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.SecretGifter,
                    LocationType = LocationType.Games,
                    LocationCategory = LocationCategory.Limited
                });
            }
            if (settings.SurveyExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Survey",
                    Description = $"Share your thoughts and earn {Resources.Currency}.",
                    ImageUrl = Helper.GetImageUrl("Locations/Survey.png"),
                    Url = _urlHelper.Action("Index", "Survey"),
                    WidthToHeightRatio = 0.7,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Survey,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Limited
                });
            }
            if (settings.MuseumExpiryDate > DateTime.UtcNow || getAllSpecialLocations)
            {
                locations.Add(new Location
                {
                    Name = "Museum",
                    Description = $"You might discover a thing or two.",
                    ImageUrl = Helper.GetImageUrl("Locations/Museum.png"),
                    Url = _urlHelper.Action("Index", "Museum"),
                    WidthToHeightRatio = 0.97,
                    LocationArea = LocationArea.Plaza,
                    LocationId = LocationId.Museum,
                    LocationType = LocationType.Knowledge,
                    LocationCategory = LocationCategory.Limited,
                    Tags = { "Boutique", "Records" }
                });
            }

            if (locationArea != LocationArea.None)
            {
                return locations.FindAll(x => x.LocationArea == locationArea);
            }

            return locations;
        }

        public bool AddFavoritedLocation(User user, LocationId locationId)
        {
            if (user == null || locationId == LocationId.None)
            {
                return false;
            }

            List<Location> locations = GetFavoritedLocations(user);
            int position = locations.LastOrDefault()?.Position + 1 ?? 0;

            return _locationsPersistence.AddFavoritedLocation(user.Username, locationId, position);
        }

        public bool UpdateFavoritedLocation(User user, Location location)
        {
            if (user == null || location == null)
            {
                return false;
            }

            return _locationsPersistence.UpdateFavoritedLocation(user.Username, location);
        }

        public bool RemoveFavoritedLocation(User user, LocationId locationId)
        {
            if (user == null || locationId == LocationId.None)
            {
                return false;
            }

            List<Location> locations = GetFavoritedLocations(user);
            int position = locations.Find(x => x.LocationId == locationId).Position;
            foreach (Location location in locations)
            {
                if (location.Position > position)
                {
                    location.Position--;
                    UpdateFavoritedLocation(user, location);
                }
            }

            return _locationsPersistence.RemoveFavoritedLocation(user.Username, locationId);
        }
    }
}
