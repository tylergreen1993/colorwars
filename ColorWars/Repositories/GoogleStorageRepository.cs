﻿using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;

namespace ColorWars.Repositories
{
    public class GoogleStorageRepository : IGoogleStorageRepository
    {
        private GoogleCredential _googleCredential;
        private StorageClient _storageClient;
        private string _bucketName;

        public GoogleStorageRepository()
        {
            _googleCredential = GoogleCredential.FromFile(ConfigurationManager.AppSettings.Get("GoogleStorageCredentialFile"));
            _storageClient = StorageClient.Create(_googleCredential);
            _bucketName = ConfigurationManager.AppSettings.Get("GoogleStorageBucket");

        }

        public async Task<string> UploadFileAsync(MemoryStream memoryStream, string fileNameForStorage, string contentType)
        {
            Google.Apis.Storage.v1.Data.Object dataObject = await _storageClient.UploadObjectAsync(_bucketName, fileNameForStorage, contentType, memoryStream);
            return $"https://storage.googleapis.com/deodeck-images/{dataObject.Name}";
        }

        public async Task DeleteFileAsync(string fileNameForStorage)
        {
            await _storageClient.DeleteObjectAsync(_bucketName, fileNameForStorage);
        }
    }
}
