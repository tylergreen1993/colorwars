﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class StocksRepository : IStocksRepository
    {
        private IStocksPersistence _stocksPersistence;
        private IEmailRepository _emailRepository;
        private INotificationsRepository _notificationRepository;

        public StocksRepository(IStocksPersistence stocksPersistence, IEmailRepository emailRepository, INotificationsRepository notificationsRepository)
        {
            _stocksPersistence = stocksPersistence;
            _emailRepository = emailRepository;
            _notificationRepository = notificationsRepository;
        }

        public List<Stock> GetStocks(User user)
        {
            if (user == null)
            {
                return null;
            }
            return _stocksPersistence.GetStocksWithUsername(user.Username);
        }

        public List<Stock> GetAllStocks()
        {
            return _stocksPersistence.GetAllStocks();
        }

        public List<Stock> GetLastTransactions(StockTicker id)
        {
            return _stocksPersistence.GetLastTransactionsForStockId(id);
        }

        public List<StockHistory> GetStockHistory(StockTicker id)
        {
            return _stocksPersistence.GetStockHistory(id);
        }

        public bool AddStock(User user, Stock stock, int amount, bool hasDiscount, bool isGift)
        {
            if (user == null || stock == null)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }

            if (_stocksPersistence.AddStock(stock.Id, user.Username, isGift ? 1 : (int)Math.Round(stock.CurrentCost * (hasDiscount ? 0.95 : 1)), amount))
            {
                if (!isGift)
                {
                    AddStockHistory(user, stock, amount, hasDiscount, StockHistoryType.Buy);
                }
                return true;
            }

            return false;
        }

        public bool AddStockHistory(User user, Stock stock, int amount, bool hasDiscount, StockHistoryType type)
        {
            if (user == null || stock == null || type == StockHistoryType.None)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }

            int cost = (int)Math.Round(stock.CurrentCost * (hasDiscount && type == StockHistoryType.Buy ? 0.95 : 1));

            return _stocksPersistence.AddStockHistory(stock.Id, user.Username, cost, amount, type);
        }

        public bool RemoveStock(User user, Stock stock, int amount, out bool isCrashed)
        {
            isCrashed = false;

            if (user == null || stock == null)
            {
                return false;
            }

            if (stock.UserAmount < amount)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }

            Stock mainStock = _stocksPersistence.RemoveStock(stock.SelectionId, user.Username, amount);
            if (mainStock == null)
            {
                return false;
            }

            AddStockHistory(user, stock, amount, false, StockHistoryType.Sold);

            if (mainStock.CurrentCost < 1)
            {
                isCrashed = true;
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    List<User> affectedUsers = _stocksPersistence.GetUsersAndUpdateCrashedStock(mainStock.Id);
                    List<string> messagedUsers = new List<string>();
                    foreach (User affectedUser in affectedUsers)
                    {
                        if (messagedUsers.Any(x => x == affectedUser.Username))
                        {
                            continue;
                        }

                        string subject = "Uh oh! Your stock crashed";
                        string title = "Crashed Stock";
                        string body = $"Oh no! Your stock \"{mainStock.Name}\" crashed and it was removed from your portfolio. The stock's price has been reset in the Stock Market. You can see your stock portfolio by clicking the button below.";
                        string url = "/StockMarket/Portfolio";

                        _emailRepository.SendEmail(affectedUser, subject, title, body, url, domain: domain);

                        _notificationRepository.AddNotification(affectedUser, $"Your stock \"{mainStock.Name}\" crashed and it was removed from your portfolio!", NotificationLocation.Stocks, url, domain: domain);

                        messagedUsers.Add(affectedUser.Username);
                    }
                });
            }

            return true;
        }
    }
}
