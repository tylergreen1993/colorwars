﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SecretGifterRepository : ISecretGifterRepository
    {
        private ISecretGifterPersistence _secretGifterPersistence;

        public SecretGifterRepository(ISecretGifterPersistence secretGifterPersistence)
        {
            _secretGifterPersistence = secretGifterPersistence;
        }

        public List<Item> GetSecretGiftItems(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _secretGifterPersistence.GetSecretGiftItemsWithUsername(user.Username);
        }

        public SecretGiftUser GetSecretGiftUser(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _secretGifterPersistence.GetSecretGiftUsername(user.Username);
        }

        public bool AddSecretGiftItem(User giftReceiver, Item item)
        {
            if (giftReceiver == null || item  == null)
            {
                return false;
            }

            item.Username = giftReceiver.Username;

            return _secretGifterPersistence.AddSecretGiftItem(item);
        }

        public bool DeleteSecretGiftItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _secretGifterPersistence.DeleteSecretGiftItemWithSelectionId(item.SelectionId);
        }
    }
}
