﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class DisposerRepository : IDisposerRepository
    {
        private IDisposerPersistence _disposerPersistence;
        private IItemsRepository _itemsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private ISettingsRepository _settingsRepository;

        public DisposerRepository(IDisposerPersistence disposerPersistence, IItemsRepository itemsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, ISettingsRepository settingsRepository)
        {
            _disposerPersistence = disposerPersistence;
            _itemsRepository = itemsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _settingsRepository = settingsRepository;
        }

        public List<DisposerRecord> GetBiggestDisposerRecords(int days)
        {
            return _disposerPersistence.GetDisposerRecordsInLastDays(days);
        }

        public bool AddDisposerRecord(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _disposerPersistence.AddDisposerRecord(user.Username);
        }

        public void DisposeItem(User user, Item item, bool removeItem, out bool recordQualifier)
        {
            recordQualifier = false;
            if (user == null || item == null)
            {
                return;
            }

            if (removeItem)
            {
                _itemsRepository.RemoveItem(user, item);
            }

            Settings settings = _settingsRepository.GetSettings(user);
            settings.DisposerTrainingTokenProgress += Math.Max(1, item.Points / 100);
            _settingsRepository.SetSetting(user, nameof(settings.DisposerTrainingTokenProgress), settings.DisposerTrainingTokenProgress.ToString());

            List <Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user);
            if (!accomplishments.Exists(x => x.Id == AccomplishmentId.DisposedItem))
            {
                _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.DisposedItem);
            }

            if (item.Condition == ItemCondition.Unusable)
            {
                List<DisposerRecord> disposerRecords = GetBiggestDisposerRecords(30);

                AddDisposerRecord(user);

                if (!disposerRecords.Take(50).Any(x => x.Username == user.Username) && (disposerRecords.Count < 50 || disposerRecords.Any(x => x.Username == user.Username)))
                {
                    if (disposerRecords.Count < 50 || GetBiggestDisposerRecords(30).Take(50).Any(x => x.Username == user.Username))
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.BiggestDisposer))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.BiggestDisposer);
                        }
                        recordQualifier = true;
                    }
                }
            }
        }
    }
}
