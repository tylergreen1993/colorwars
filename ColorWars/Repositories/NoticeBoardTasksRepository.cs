﻿using System;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class NoticeBoardTasksRepository : INoticeBoardTasksRepository
    {
        private INoticeBoardTasksPersistence _noticeBoardTasksPersistence;

        public NoticeBoardTasksRepository(INoticeBoardTasksPersistence noticeBoardTasksPersistence)
        {
            _noticeBoardTasksPersistence = noticeBoardTasksPersistence;
        }

        public NoticeBoardTask GetNoticeBoardTask(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _noticeBoardTasksPersistence.GetNoticeBoardTaskWithUsername(user.Username, isCached);
        }

        public bool AddNoticeBoardTask(User user, Item item, ItemCondition itemCondition, bool isExactCondition, bool hasTotalUses, int reward, int itemAgeInDays)
        {
            if (user == null || item == null || reward <= 0)
            {
                return false;
            }

            DateTime itemCreatedDate = DateTime.UtcNow.AddDays(-itemAgeInDays);

            return _noticeBoardTasksPersistence.AddNoticeBoardTask(user.Username, item.Id, itemCondition, isExactCondition, hasTotalUses, reward, itemCreatedDate);
        }


        public bool DeleteNoticeBoardTask(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _noticeBoardTasksPersistence.DeleteNoticeBoardTasksWithUsername(user.Username);
        }
    }
}
