﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class AttentionHallRepository : IAttentionHallRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IAttentionHallPersistence _attentionHallPersistence;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private IItemsRepository _itemsRepository;
        private IMatchHistoryRepository _matchHistoryRepository;
        private IUserRepository _userRepository;
        private ISession _session;

        public AttentionHallRepository(IHttpContextAccessor httpContextAccessor, IAttentionHallPersistence attentionHallPersistence, IAccomplishmentsRepository accomplishmentsRepository,
        IItemsRepository itemsRepository, IMatchHistoryRepository matchHistoryRepository, IUserRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _attentionHallPersistence = attentionHallPersistence;
            _accomplishmentsRepository = accomplishmentsRepository;
            _itemsRepository = itemsRepository;
            _matchHistoryRepository = matchHistoryRepository;
            _userRepository = userRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }


        public List<AttentionHallPost> GetAllAttentionHallPosts(User user, AttentionHallPostsFilter filter, AttentionHallPostType type)
        {
            if (user == null)
            {
                return null;
            }

            List<AttentionHallPost> attentionHallPosts = _attentionHallPersistence.GetAllAttentionHallPosts(filter, type, user.Username);
            List<Accomplishment> allAccomplishments = _accomplishmentsRepository.GetAllAccomplishments();
            List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
            foreach (AttentionHallPost attentionHallPost in attentionHallPosts)
            {
                UpdateAttentionHallPost(user, attentionHallPost, allAccomplishments, allItems);
            }

            return attentionHallPosts;
        }

        public AttentionHallPost GetAttentionHallPost(User user, int id)
        {
            if (user == null)
            {
                return null;
            }

            AttentionHallPost attentionHallPost = _attentionHallPersistence.GetAttentionHallPost(id, user.Username);
            if (attentionHallPost != null)
            {
                List<Accomplishment> allAccomplishments = _accomplishmentsRepository.GetAllAccomplishments();
                List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
                UpdateAttentionHallPost(user, attentionHallPost, allAccomplishments, allItems);
            }

            return attentionHallPost;
        }

        public int GetAttentionHallPoints(User user)
        {
            if (user == null)
            {
                return 0;
            }

            return _attentionHallPersistence.GetTotalAttentionHallPointsWithUsername(user.Username).TotalPoints;
        }

        public AttentionHallPost GetMostPopularAttentionHallPost(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<AttentionHallPost> popularAttentionHallPosts = isCached ? _session.GetObject<List<AttentionHallPost>>("PopularAttentionHallPosts") : null;
            if (popularAttentionHallPosts != null)
            {
                AttentionHallPost attentionHallPost = popularAttentionHallPosts?.FirstOrDefault();
                if (attentionHallPost != null && (DateTime.UtcNow - attentionHallPost.LastRetrievedFeedDate).TotalHours <= 3)
                {
                    return attentionHallPost;
                }
            }

            popularAttentionHallPosts = GetAllAttentionHallPosts(user, AttentionHallPostsFilter.Popular, AttentionHallPostType.None);
            if (popularAttentionHallPosts.FirstOrDefault() != null)
            {
                popularAttentionHallPosts.First().LastRetrievedFeedDate = DateTime.UtcNow;
            }

            if (isCached)
            {
                _session.SetObject("PopularAttentionHallPosts", popularAttentionHallPosts);
            }

            return popularAttentionHallPosts?.FirstOrDefault();
        }

        public List<AttentionHallComment> GetAllAttentionHallComments(User user, AttentionHallPost attentionHallPost, bool sortWithChildren = false)
        {
            if (user == null)
            {
                return null;
            }

            if (attentionHallPost == null)
            {
                return null;
            }

            List<AttentionHallComment> attentionHallComments = _attentionHallPersistence.GetAttentionHallComments(attentionHallPost.Id, user.Username);
            if (sortWithChildren)
            {

                List<AttentionHallComment> sortedAttentionHallComments = new List<AttentionHallComment>();
                List<AttentionHallComment> parentComments = attentionHallComments.FindAll(x => x.ParentCommentId == Guid.Empty);
                foreach (AttentionHallComment attentionHallComment in parentComments)
                {
                    attentionHallComment.Level = 0;
                    sortedAttentionHallComments.Add(attentionHallComment);
                    RecursivelyFindCommentChildren(attentionHallComment, sortedAttentionHallComments, attentionHallComments);
                }

                return sortedAttentionHallComments;
            }

            return attentionHallComments;
        }

        public List<AttentionHallComment> GetAttentionHallCommentsFromPosts(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _attentionHallPersistence.GetAttentionHallCommentsFromPostsWithUsername(user.Username);
        }

        public List<UserStatistic> GetTopAttentionHallPosters(bool isCached = true)
        {
            return _attentionHallPersistence.GetTopAttentionHallPosters(isCached);
        }

        public List<UserStatistic> GetTopAttentionHallCommenters(bool isCached = true)
        {
            return _attentionHallPersistence.GetTopAttentionHallCommenters(isCached);
        }

        public bool AddAttentionHallPost(User user, string title, string content, Accomplishment accomplishment, Item item, MatchTag matchTag, Guid jobSubmissionId, AttentionHallFlair attentionHallFlair, out AttentionHallPost attentionHallPost)
        {
            attentionHallPost = null;
            if (user == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content))
            {
                return false;
            }

            attentionHallPost = _attentionHallPersistence.AddAttentionHallPost(user.Username, title, content, accomplishment?.Id, item, matchTag?.TagUsername, jobSubmissionId, attentionHallFlair);

            return attentionHallPost != null;
        }

        public bool AddAttentionHallComment(User user, AttentionHallComment parentAttentionHallComment, AttentionHallPost attentionHallPost, string comment, out AttentionHallComment attentionHallComment)
        {
            attentionHallComment = null;
            if (user == null || attentionHallPost == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(comment))
            {
                return false;
            }

            attentionHallComment = _attentionHallPersistence.AddAttentionHallComment(parentAttentionHallComment?.Id, attentionHallPost.Id, user.Username, comment);

            return attentionHallComment != null;
        }

        public bool SetAttentionHallPoint(User user, AttentionHallPost attentionHallPost, AttentionHallPointType attentionHallPointType)
        {
            if (user == null || attentionHallPost == null)
            {
                return false;
            }

            if (attentionHallPost.UserVote == AttentionHallPointType.None)
            {
                if (attentionHallPointType == AttentionHallPointType.None)
                {
                    return false;
                }

                return _attentionHallPersistence.AddAttentionHallPoint(attentionHallPost.Id, user.Username, attentionHallPointType);
            }

            if (attentionHallPost.UserVote == attentionHallPointType)
            {
                attentionHallPointType = AttentionHallPointType.None;
            }

            return _attentionHallPersistence.UpdateAttentionHallPoint(attentionHallPost.Id, user.Username, attentionHallPointType);
        }

        public bool SetAttentionHallCommentLike(User user, AttentionHallComment attentionHallComment)
        {
            if (user == null || attentionHallComment == null)
            {
                return false;
            }

            return _attentionHallPersistence.AddOrDeleteAttentionHallCommentLike(attentionHallComment.PostId, attentionHallComment.Id, user.Username);
        }

        public bool UpdateAttentionHallPost(AttentionHallPost attentionHallPost)
        {
            if (attentionHallPost == null || string.IsNullOrEmpty(attentionHallPost.Title) || string.IsNullOrEmpty(attentionHallPost.Content))
            {
                return false;
            }

            return _attentionHallPersistence.UpdateAttentionHallPost(attentionHallPost.Id, attentionHallPost.Title, attentionHallPost.Content);
        }

        public bool DeleteAttentionHallPost(AttentionHallPost attentionHallPost)
        {
            if (attentionHallPost == null)
            {
                return false;
            }

            return _attentionHallPersistence.DeleteAttentionHallPost(attentionHallPost.Id);
        }

        public bool DeleteAttentionHallComment(AttentionHallComment attentionHallComment)
        {
            if (attentionHallComment == null)
            {
                return false;
            }

            return _attentionHallPersistence.DeleteAttentionHallComment(attentionHallComment.PostId, attentionHallComment.Id);
        }

        private void UpdateAttentionHallPost(User user, AttentionHallPost attentionHallPost, List<Accomplishment> allAccomplishments, List<Item> allItems)
        {
            if (attentionHallPost.AccomplishmentId != null)
            {
                attentionHallPost.Accomplishment = allAccomplishments.Find(x => x.Id == attentionHallPost.AccomplishmentId);
                attentionHallPost.Type = AttentionHallPostType.Accomplishment;
            }
            else if (attentionHallPost.ItemId != Guid.Empty)
            {
                Item item = allItems.Find(x => x.Id == attentionHallPost.ItemId);
                if (item != null && attentionHallPost.ItemTheme != null)
                {
                    item.Theme = attentionHallPost.ItemTheme.Value;
                    if (!string.IsNullOrEmpty(attentionHallPost.ItemName) && !string.IsNullOrEmpty(attentionHallPost.ItemImageUrl))
                    {
                        item.Name = attentionHallPost.ItemName;
                        item.ImageUrl = attentionHallPost.ItemImageUrl;
                    }
                }
                attentionHallPost.Item = item;
                attentionHallPost.Type = AttentionHallPostType.Item;
            }
            else if (!string.IsNullOrEmpty(attentionHallPost.MatchTagUsername))
            {
                User attentionHallPostUser = attentionHallPost.Username == user.Username ? user : _userRepository.GetUser(attentionHallPost.Username);
                if (attentionHallPostUser == null)
                {
                    attentionHallPost.Type = AttentionHallPostType.General;
                }
                else
                {
                    List<MatchTag> matchTags = _matchHistoryRepository.GetMatchTags(attentionHallPostUser, user.Username == attentionHallPostUser.Username);
                    attentionHallPost.MatchTag = matchTags.Find(x => x.TagUsername == attentionHallPost.MatchTagUsername);
                    attentionHallPost.Type = AttentionHallPostType.MatchTag;
                }
            }
            if (attentionHallPost.Accomplishment == null && attentionHallPost.Item == null && attentionHallPost.MatchTag == null)
            {
                attentionHallPost.Type = AttentionHallPostType.General;
            }
        }

        private void RecursivelyFindCommentChildren(AttentionHallComment attentionHallComment, List<AttentionHallComment> sortedAttentionHallComments, List<AttentionHallComment> attentionHallComments)
        {
            List<AttentionHallComment> childrenAttentionHallComments = attentionHallComments.FindAll(x => x.ParentCommentId == attentionHallComment.Id);
            foreach (AttentionHallComment childAttentionHallComment in childrenAttentionHallComments)
            {
                childAttentionHallComment.Level = attentionHallComment.Level + 1;
                sortedAttentionHallComments.Add(childAttentionHallComment);
                RecursivelyFindCommentChildren(childAttentionHallComment, sortedAttentionHallComments, attentionHallComments);
            }
        }
    }
}
