﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class CellarStoreRepository : ICellarStoreRepository
    {
        private List<CellarStoreItem> _cellarStoreItems;

        public CellarStoreRepository()
        {
            _cellarStoreItems = new List<CellarStoreItem>();
            PopulateItems();
        }

        public List<CellarStoreItem> GetItems()
        {
            return _cellarStoreItems.OrderBy(x => x.Points).ThenBy(x => x.Name).ToList();
        }

        private void PopulateItems()
        {
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.MazeAccess,
                Name = $"Maze Access",
                Description = $"Visit the Maze again if you've already visited it today.",
                Points = 5000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.ThemeSpeedUp,
                Name = $"Quicker Squeeze",
                Description = $"Reduce the waiting time for an item currently in the Theme Squeezer by 12 hours.",
                Points = 5000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.CompanionColor,
                Name = $"Companion Color Change",
                Description = $"Randomly change one of your Companion's colors.",
                Points = 1000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.CompanionLevel,
                Name = $"Companion Upgrade",
                Description = $"Randomly increase one of your Companion's levels.",
                Points = 2500
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.SummonMuseum,
                Name = $"Summon Museum",
                Description = $"Summon the Museum to show up if it's not currently available.",
                Points = 10000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.SummonRewardsStore,
                Name = $"Summon Rewards Store",
                Description = $"Summon the Rewards Store to show up if it's not currently available.",
                Points = 10000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.ExtraNoticeBoardDay,
                Name = $"Extended Notice Board",
                Description = $"If you have an active Notice Board task, gain an extra day to complete it.",
                Points = 7500
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.ExtendedStreakCardsStreakShort,
                Name = $"Streak Cards Streak Hold (Short)",
                Description = $"Have an extra day before your Streak Cards streak expires.",
                Points = 5000
            });
            _cellarStoreItems.Add(new CellarStoreItem
            {
                Id = CellarStoreItemId.ExtendedStreakCardsStreakLong,
                Name = $"Streak Cards Streak Hold (Long)",
                Description = $"Have an extra week before your Streak Cards streak expires.",
                Points = 30000
            });
        }
    }
}