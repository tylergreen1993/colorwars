﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class PartnershipRepository : IPartnershipRepository
    {
        private IPartnershipPersistence _partnershipPersistence;
        private ISettingsRepository _settingsRepository;

        public PartnershipRepository(IPartnershipPersistence partnershipPersistence, ISettingsRepository settingsRepository)
        {
            _partnershipPersistence = partnershipPersistence;
            _settingsRepository = settingsRepository;
        }

        public List<PartnershipRequest> GetPartnershipRequests(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _partnershipPersistence.GetPartnershipRequests(user.Username);
        }

        public List<PartnershipEarning> GetEarningsForPartnership(User user, User partnerUser)
        {
            if (user == null || partnerUser == null)
            {
                return null;
            }

            return _partnershipPersistence.GetPartnershipEarnings(user.Username, partnerUser.Username);
        }

        public List<PartnershipEarning> GetEarningsFromPartnership(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            Settings settings = _settingsRepository.GetSettings(user);

            string partnershipUsername = settings.PartnershipUsername;
            if (string.IsNullOrEmpty(partnershipUsername))
            {
                return null;
            }

            return _partnershipPersistence.GetPartnershipEarnings(partnershipUsername, user.Username, isCached);
        }

        public bool AddPartnershipRequest(User user, User partnerUser)
        {
            if (user == null || partnerUser == null)
            {
                return false;
            }

            return _partnershipPersistence.AddPartnershipRequest(partnerUser.Username, user.Username);
        }

        public bool AddPartnershipEarning(User user, User partnerUser, int amount)
        {
            if (user == null || partnerUser == null || amount <= 0)
            {
                return false;
            }

            return _partnershipPersistence.AddPartnershipEarning(user.Username, partnerUser.Username, amount);
        }

        public bool DeletePartnershipRequest(User user, User requestUser)
        {
            if (user == null || requestUser == null)
            {
                return false;
            }

            return _partnershipPersistence.DeletePartnershipRequest(user.Username, requestUser.Username);
        }
    }
}
