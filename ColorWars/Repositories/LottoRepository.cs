﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class LottoRepository : ILottoRepository
    {
        private ILottoPersistence _lottoPersistence;

        public LottoRepository(ILottoPersistence lottoPersistence)
        {
            _lottoPersistence = lottoPersistence;
        }

        public List<Lotto> GetLottos()
        {
            return _lottoPersistence.GetLottos();
        }

        public bool AddLotto(int currentPot, int winningNumber)
        {
            return _lottoPersistence.AddLotto(currentPot, winningNumber);
        }

        public bool UpdateLotto(Lotto lotto)
        {
            if (lotto == null)
            {
                return false;
            }

            return _lottoPersistence.UpdateLotto(lotto.Id, lotto.CurrentPot, lotto.WinnerUsername, lotto.WonDate);
        }
    }
}
