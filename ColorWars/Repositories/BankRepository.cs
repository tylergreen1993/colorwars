﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class BankRepository : IBankRepository
    {
        private IBankPersistence _bankPersistence;

        public BankRepository(IBankPersistence bankPersistence)
        {
            _bankPersistence = bankPersistence;
        }

        public Bank GetBank(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _bankPersistence.GetBankInfoWithUsername(user.Username, isCached);
        }

        public List<BankTransaction> GetBankTransactions(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _bankPersistence.GetBankTransactionsWithUsername(user.Username, isCached);
        }

        public bool AddBankTransaction(User user, BankTransactionType type, int amount)
        {
            if (user == null || amount <= 0)
            {
                return false;
            }

            return _bankPersistence.AddBankTransaction(user.Username, type, amount);
        }

        public bool Withdraw(User user, Bank bank, int amount)
        {
            if (user == null)
            {
                return false;
            }

            if (bank == null)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }
            if (bank.Points - amount < 0)
            {
                return false;
            }

            bank.Points -= amount;

            if (_bankPersistence.UpdateBank(user.Username, bank.Points))
            {
                AddBankTransaction(user, BankTransactionType.Withdraw, amount);
                return true;
            }
            return false;
        }

        public bool Deposit(User user, Bank bank, int amount, bool isInterest = false)
        {
            if (user == null)
            {
                return false;
            }

            if (amount <= 0)
            {
                return false;
            }

            if (bank != null)
            {
                bank.Points += amount;
            }

            if (_bankPersistence.UpdateBank(user.Username, bank?.Points ?? amount))
            {
                AddBankTransaction(user, isInterest ? BankTransactionType.Interest : BankTransactionType.Deposit, amount);
                return true;
            }
            return false;
        }
    }
}
