﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class DonationsRepository : IDonationsRepository
    {
        private IDonationsPersistence _donationsPersistence;

        public DonationsRepository(IDonationsPersistence donationsPersistence)
        {
            _donationsPersistence = donationsPersistence;
        }

        public List<Item> SearchItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All)
        {
            List<Item> donationItems = GetDonationItems().Take(40).ToList();
            if (!string.IsNullOrEmpty(query))
            {
                donationItems = matchExactSearch ? donationItems.FindAll(x => x.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase)) : donationItems.FindAll(x => x.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
            }

            if (category != ItemCategory.All)
            {
                donationItems = donationItems.FindAll(x => x.Category == category);
            }
            if (condition != ItemCondition.All)
            {
                donationItems = donationItems.FindAll(x => x.Condition == condition);
            }

            return donationItems;
        }

        public List<Item> GetDonationItems(bool getAllItems = false)
        {
            return _donationsPersistence.GetDonationItems().FindAll(x => getAllItems || x.Condition < ItemCondition.Unusable);
        }

        public bool AddDonationItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            return _donationsPersistence.AddDonationItem(user.Username, item.Id, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool AddSpecialDonationItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            if (item.TotalUses > 0)
            {
                item.Uses = item.TotalUses;
            }
            item.CreatedDate = DateTime.UtcNow;
            item.RepairedDate = DateTime.UtcNow;

            return _donationsPersistence.AddDonationItem(Helper.GetCourierUsername(), item.Id, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool DeleteDonationItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _donationsPersistence.DeleteDonationItem(item.SelectionId);
        }
    }
}
