﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SeekerRepository : ISeekerRepository
    {
        private ISeekerPersistence _seekerPersistence;

        public SeekerRepository(ISeekerPersistence seekerPersistence)
        {
            _seekerPersistence = seekerPersistence;
        }

        public List<SeekerChallenge> GetSeekerChallenges(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }
            return _seekerPersistence.GetSeekerChallengesWithUsername(user.Username, isCached);
        }

        public bool AddSeekerChallenge(User user, Item item, Item reward)
        {
            if (user == null || item == null || reward == null)
            {
                return false;
            }

            return _seekerPersistence.AddSeekerChallenge(user.Username, item.Id, item.Theme, reward.Id, reward.Theme);
        }

        public bool DeleteSeekerChallenges(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _seekerPersistence.DeleteSeekerChallengesWithUsername(user.Username);
        }
    }
}
