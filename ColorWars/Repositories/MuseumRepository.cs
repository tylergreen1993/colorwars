﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class MuseumRepository : IMuseumRepository
    {
        private IWishingStoneRepository _wishingStoneRepository;

        public MuseumRepository(IWishingStoneRepository wishingStoneRepository)
        {
            _wishingStoneRepository = wishingStoneRepository;
        }

        public List<MuseumDisplay> GetMuseumDisplays(User user, Settings settings, MuseumWing museumWing)
        {
            int[] quarryStonePositions = _wishingStoneRepository.GetSecretQuarryStonePattern(user, settings);

            return new List<MuseumDisplay>{
                new MuseumDisplay
                {
                    Name = "Coat of Arms",
                    Description = "The history of this Coat of Arms has been lost in time. It's believed to have come from the Right-Side Up World although its creation remains a mystery.",
                    ImageUrl = "Museum/GuardCoat.png",
                    WidthToHeightRatio = 0.89,
                    MuseumWing = MuseumWing.North
                },
                new MuseumDisplay
                {
                    Name = "Wizard's Hat",
                    Description = "A magical hat formerly worn by the Young Wizard. More recently, he's chosen to wear a blue variant given to him by his sister, the Sorceress.",
                    ImageUrl = "Museum/WizardsHat.png",
                    WidthToHeightRatio = 1,
                    MuseumWing = MuseumWing.North
                },
                new MuseumDisplay
                {
                    Name = "Courier's Badge",
                    Description = $"A special, silver badge owned by the Courier. There are only two copies of this badge: one belonging to the Courier and the other in this Museum.",
                    ImageUrl = "Museum/CourierBadge.png",
                    WidthToHeightRatio = 0.99,
                    MuseumWing = MuseumWing.North
                },
                new MuseumDisplay
                {
                    Name = "Strange Stone",
                    Description = $"A strange stone found at the Wishing Stone Quarry. There are numbers etched on the back of it: {string.Join(", ", quarryStonePositions).TrimEnd()}. What could it mean?",
                    ImageUrl = "Museum/StrangeStone.png",
                    WidthToHeightRatio = 1.38,
                    MuseumWing = MuseumWing.North
                },
                new MuseumDisplay
                {
                    Name = "Red Book",
                    Description = $"This book has a small poem: \"Collect 5 Library Cards and then one more! Lo and behold, you've stumbled upon a secret door!\"",
                    ImageUrl = "Museum/RedBook.png",
                    WidthToHeightRatio = 1.08,
                    MuseumWing = MuseumWing.East
                },
                new MuseumDisplay
                {
                    Name = "Green Book",
                    Description = $"This book talks about a shrine that used to exist at the Plaza. Rumors say that it's still around but you have to search for it.",
                    ImageUrl = "Museum/GreenBook.png",
                    WidthToHeightRatio = 1.08,
                    MuseumWing = MuseumWing.East
                },
                new MuseumDisplay
                {
                    Name = "Purple Book",
                    Description = $"This book reads: \"The Traveling Merchants sometimes come to the Plaza. If you want to score big with them and get better prizes, you should show them a valuable item that has a New condition.\"",
                    ImageUrl = "Museum/PurpleBook.png",
                    WidthToHeightRatio = 1.08,
                    MuseumWing = MuseumWing.East
                },
                new MuseumDisplay
                {
                    Name = "Orange Book",
                    Description = $"This book mentions the arrival of an evil being beyond a door.",
                    ImageUrl = "Museum/OrangeBook.png",
                    WidthToHeightRatio = 1.08,
                    MuseumWing = MuseumWing.East
                },
                new MuseumDisplay
                {
                    Name = "Buddy Plaque",
                    Description = $"A plaque commemorating a Companion, Buddy, who found over {Helper.GetFormattedPoints(100000)} for his owner.",
                    ImageUrl = "Museum/Companion.png",
                    WidthToHeightRatio = 1,
                    MuseumWing = MuseumWing.West
                },
                new MuseumDisplay
                {
                    Name = "Ice Block",
                    Description = $"A piece of ice from the Frozen Lake. Often, by mistake, items are frozen in the lake and have to be defrosted at the Furnace.",
                    ImageUrl = "Museum/IceBlock.png",
                    WidthToHeightRatio = 1,
                    MuseumWing = MuseumWing.West
                },
                new MuseumDisplay
                {
                    Name = "Treasure Map",
                    Description = $"It points to a hidden treasure: \"Start at About and head right until you reach the end. There you'll find your spot, enter, and head to the bottom. Treasure awaits!\"",
                    ImageUrl = "Museum/TreasureMap.png",
                    WidthToHeightRatio = 1.1,
                    MuseumWing = MuseumWing.West
                },
                new MuseumDisplay
                {
                    Name = "Gold Briefcase",
                    Description = settings.GoldBriefcasePinCode >= 0 ?
                    $"A briefcase that was anonymously donated to the Museum. It's been locked for many years and believed to be filled with rare treasures. If only there was a way to open it."
                    : $"This briefcase has already been opened. It's empty now.",
                    ImageUrl = "Museum/GoldBriefcase.png",
                    WidthToHeightRatio = 1.17,
                    PinCode = settings.GoldBriefcasePinCode,
                    MuseumWing = MuseumWing.West
                },
                new MuseumDisplay
                {
                    Name = "Mined Ore",
                    Description = $"This ore was mined from the Deep Caverns. If someone had enough light, they could explore the Deep Caverns themselves.",
                    ImageUrl = "Museum/Ore.png",
                    WidthToHeightRatio = 1.48,
                    MuseumWing = MuseumWing.South
                },
                new MuseumDisplay
                {
                    Name = "Rare Jewel",
                    Description = $"This jewel was donated to the Museum by the exclusive Elite Shop. The word WEALTHY is engraved on the back of it. Could that word be used somewhere?",
                    ImageUrl = "Museum/Jewel.png",
                    WidthToHeightRatio = 1,
                    MuseumWing = MuseumWing.South
                },
                new MuseumDisplay
                {
                    Name = "Sunburst Clock",
                    Description = $"This clock can tell anyone how long ago their account was created. It says: \"Your account was created {Helper.FormatNumber(user.GetAgeInHours())} hour{(user.GetAgeInHours() == 1 ? "" : "s")} ago.\"",
                    ImageUrl = "Museum/Clock.png",
                    WidthToHeightRatio = 1,
                    MuseumWing = MuseumWing.South
                },
                new MuseumDisplay
                {
                    Name = "Seeker's Chest",
                    Description = $"A chest formerly owned by The Seeker. It contained many rare treasures and rewards that were given to users who successfully completed tasks.",
                    ImageUrl = "Museum/Chest.png",
                    WidthToHeightRatio = 0.88,
                    MuseumWing = MuseumWing.South
                }
            }.FindAll(x => x.MuseumWing == museumWing).OrderBy(x => x.Name).ToList();
        }
    }
}
