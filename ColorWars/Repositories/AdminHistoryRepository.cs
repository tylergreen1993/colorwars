﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class AdminHistoryRepository : IAdminHistoryRepository
    {
        private IAdminHistoryPersistence _adminHistoryPersistence;

        public AdminHistoryRepository(IAdminHistoryPersistence adminHistoryPersistence)
        {
            _adminHistoryPersistence = adminHistoryPersistence;
        }

        public List<AdminHistory> GetAdminHistory(User user, User adminUser)
        {
            if (user == null || adminUser == null)
            {
                return null;
            }

            if (adminUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _adminHistoryPersistence.GetAdminHistoryWithUsername(user.Username);
        }

        public List<TransferHistory> GetTransferHistory(User adminUser, User user = null)
        {
            if (adminUser == null)
            {
                return null;
            }

            if (adminUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return user == null ? _adminHistoryPersistence.GetTransferHistory() : _adminHistoryPersistence.GetTransferHistoryWithUsername(user.Username);
        }

        public bool AddAdminHistory(User user, User adminUser, string reason)
        {
            if (user == null || adminUser == null)
            {
                return false;
            }

            if (adminUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _adminHistoryPersistence.AddAdminHistory(user.Username, adminUser.Username, reason);
        }

        public bool AddTransferHistory(int transferId, TransferType type, User transferer, User transferee, int differenceAmount)
        {
            if (type == TransferType.None || transferer == null || transferee == null || differenceAmount <= 0)
            {
                return false;
            }

            return _adminHistoryPersistence.AddTransferHistory(transferId, type, transferer.Username, transferee.Username, differenceAmount);
        }
    }
}
