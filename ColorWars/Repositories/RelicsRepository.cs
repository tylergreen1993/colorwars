﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class RelicsRepository : IRelicsRepository
	{
        private IHttpContextAccessor _httpContextAccessor;
        private ISession _session;
        private List<Relic> _relics;

        public RelicsRepository(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
            _relics = new List<Relic>();

            PopulateRelics();
        }

        public Relic GetRelic(int level)
        {
            return _relics.OrderByDescending(x => x.Level).ToList().Find(x => x.Level <= level);
        }

        public bool HasEarned(RelicId relicId, int level)
        {
            if (relicId == RelicId.None || level >= 35)
            {
                return true;
            }

            return GetAllRelics(level).Find(x => x.Id == relicId)?.Earned ?? false;
        }

        public List<Relic> GetAllRelics(int level)
        {
            List<Relic> relics = _relics;
            foreach (Relic relic in relics)
            {
                relic.Earned = level >= relic.Level;
            }

            return relics;
        }

        private void PopulateRelics()
        {
            List<Relic> sessionRelics = _session.GetObject<List<Relic>>("Relics");
            if (sessionRelics != null)
            {
                _relics = sessionRelics;
                return;
            }

            _relics.Add(new Relic
            {
                Id = RelicId.Moon,
                Name = "Moon",
                Level = 5
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Sun,
                Name = "Sun",
                Level = 10
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Fire,
                Name = "Fire",
                Level = 15
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Wind,
                Name = "Wind",
                Level = 20
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Water,
                Name = "Water",
                Level = 25
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Shadow,
                Name = "Shadow",
                Level = 30
            });
            _relics.Add(new Relic
            {
                Id = RelicId.Eternal,
                Name = "Eternal",
                Level = 35
            });

            _session.SetObject("Relics", _relics);
        }
    }
}
