﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private INewsPersistence _newsPersistence;

        public NewsRepository(INewsPersistence newsPersistence)
        {
            _newsPersistence = newsPersistence;
        }

        public List<News> GetNews()
        {
            return _newsPersistence.GetNews();
        }

        public List<NewsComment> GetNewsComments(User user, News news, bool sortWithChildren = false)
        {
            if (user == null || news == null)
            {
                return null;
            }

            List<NewsComment> newsComments = _newsPersistence.GetNewsCommentsWithId(news.Id, user.Username);
            if (sortWithChildren)
            {

                List<NewsComment> sortedNewsComments = new List<NewsComment>();
                List<NewsComment> parentComments = newsComments.FindAll(x => x.ParentCommentId == Guid.Empty);
                foreach (NewsComment newsComment in parentComments)
                {
                    newsComment.Level = 0;
                    sortedNewsComments.Add(newsComment);
                    RecursivelyFindCommentChildren(newsComment, sortedNewsComments, newsComments);
                }

                return sortedNewsComments;
            }

            return newsComments;
        }

        public bool AddNews(User currentUser, User author, string title, string content, out News news)
        {
            news = null;
            if (currentUser == null || author == null || string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content))
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            news = _newsPersistence.AddNews(author.Username, title, content);

            return news != null;
        }

        public bool AddNewsComment(User user, NewsComment parentNewsComment, News news, string message, out NewsComment newsComment)
        {
            newsComment = null;
            if (user == null || news == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            newsComment = _newsPersistence.AddNewsComment(news.Id, parentNewsComment?.Id, user.Username, message);

            return newsComment != null;
        }

        public bool SetNewsCommentLike(User user, NewsComment newsComment)
        {
            if (user == null || newsComment == null)
            {
                return false;
            }

            return _newsPersistence.AddOrDeleteNewsCommentLike(newsComment.NewsId, newsComment.Id, user.Username);
        }

        public bool UpdateNews(User currentUser, News news)
        {
            if (currentUser == null || news == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _newsPersistence.UpdateNews(news);
        }

        public bool DeleteNews(User currentUser, News news)
        {
            if (currentUser == null || news == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _newsPersistence.DeleteNews(news.Id);
        }

        public bool DeleteNewsComment(News news, NewsComment newsComment)
        {
            if (news == null || newsComment == null)
            {
                return false;
            }

            return _newsPersistence.DeleteNewsComment(news.Id, newsComment.Id);
        }

        private void RecursivelyFindCommentChildren(NewsComment newsComment, List<NewsComment> sortedNewsComments, List<NewsComment> newsComments)
        {
            List<NewsComment> childrenNewsComment = newsComments.FindAll(x => x.ParentCommentId == newsComment.Id);
            foreach (NewsComment childNewsComment in childrenNewsComment)
            {
                childNewsComment.Level = newsComment.Level + 1;
                sortedNewsComments.Add(childNewsComment);
                RecursivelyFindCommentChildren(childNewsComment, sortedNewsComments, newsComments);
            }
        }
    }
}
