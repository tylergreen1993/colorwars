﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class StreakCardsRepository : IStreakCardsRepository
    {
        private IStreakCardsPersistence _streakCardsPersistence;

        public StreakCardsRepository(IStreakCardsPersistence streakCardsPersistence)
        {
            _streakCardsPersistence = streakCardsPersistence;
        }

        public List<StreakCard> GetStreakCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _streakCardsPersistence.GetStreakCardsWithUsername(user.Username, isCached);
        }

        public bool AddStreakCard(User user, int position, int points)
        {
            if (user == null)
            {
                return false;
            }

            return _streakCardsPersistence.AddStreakCard(user.Username, position, points);
        }

        public bool DeleteStreakCards(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _streakCardsPersistence.DeleteStreakCards(user.Username);
        }
    }
}
