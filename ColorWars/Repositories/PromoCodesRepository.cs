﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class PromoCodesRepository : IPromoCodesRepository
    {
        private IPromoCodesPersistence _promoCodesPersistence;

        public PromoCodesRepository(IPromoCodesPersistence promoCodesPersistence)
        {
            _promoCodesPersistence = promoCodesPersistence;
        }

        public PromoCode GetPromoCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }

            return _promoCodesPersistence.GetPromoCode(code);
        }

        public List<PromoCode> GetPreviousPromoCodes(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _promoCodesPersistence.GetPreviousPromoCodes(user.Username, isCached);
        }

        public List<PromoCode> GetAllUnexpiredGlobalPromoCodes(User currentUser)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _promoCodesPersistence.GetAllUnexpiredGlobalPromoCodes(false);
        }

        public List<PromoCode> GetAllUnexpiredSponsorCodes(bool isCached = true)
        {
            return _promoCodesPersistence.GetAllUnexpiredGlobalPromoCodes(isCached).FindAll(x => x.IsSponsor);
        }

        public List<PromoCodeHistory> GetPromoCodeHistory(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _promoCodesPersistence.GetPromoCodeHistoryWithUsername(user.Username, isCached);
        }

        public PromoCode RedeemPromoCode(User user, string code)
        {
            if (user == null || string.IsNullOrEmpty(code))
            {
                return null;
            }

            return _promoCodesPersistence.RedeemPromoCode(user.Username, code);
        }

        public bool AddPromoCode(User user, string code, PromoEffect effect, bool oneUserUse, bool isSponsor, Item item, DateTime expiryDate)
        {
            if (string.IsNullOrEmpty(code))
            {
                return false;
            }

            Guid itemId = Guid.Empty;
            if (item != null)
            {
                if (effect == PromoEffect.SecretGift)
                {
                    itemId = item.SelectionId;
                }
                else
                {
                    itemId = item.Id;
                }
            }

            return _promoCodesPersistence.AddPromoCode(code, user?.Username ?? null, effect, oneUserUse, isSponsor, itemId, expiryDate);
        }

        public bool AddPromoCodeHistory(User user, string code, string message)
        {
            if (user == null || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _promoCodesPersistence.AddPromoCodeHistory(user.Username, code, message);
        }

        public string GenerateRandomPromoCode(int daysToExpire, PromoEffect promoEffect = PromoEffect.None, Item item = null)
        {
            if (promoEffect == PromoEffect.None)
            {
                Random rnd = new Random();
                List<PromoEffect> promoEffects = Enum.GetValues(typeof(PromoEffect)).Cast<PromoEffect>().ToList();
                promoEffects.Remove(PromoEffect.SecretGift);
                promoEffects.Remove(PromoEffect.ClubTrial);
                promoEffects.Remove(PromoEffect.RareCardPack);
                promoEffects.Remove(PromoEffect.StoreMagicianTrial);
                promoEffect = promoEffects[rnd.Next(promoEffects.Count)];
            }

            bool uniqueStringFound = false;
            string uniqueString = string.Empty;
            while (!uniqueStringFound)
            {
                uniqueString = Helper.GetRandomString(8);
                uniqueStringFound = AddPromoCode(null, uniqueString, promoEffect, true, false, item, DateTime.UtcNow.AddDays(daysToExpire));
            }

            return uniqueString;
        }
    }
}
