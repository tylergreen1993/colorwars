﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class NotificationsRepository : INotificationsRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private INotificationsPersistence _notificationsPersistence;
        private IEmailRepository _emailRepository;
        private ISettingsRepository _settingsRepository;

        public NotificationsRepository(IHttpContextAccessor httpContextAccessor, INotificationsPersistence notificationsPersistence,
        IEmailRepository emailRepository, ISettingsRepository settingsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _notificationsPersistence = notificationsPersistence;
            _emailRepository = emailRepository;
            _settingsRepository = settingsRepository;
        }

        public List<Notification> GetNotifications(User user, bool markAsRead = true)
        {
            if (user == null)
            {
                return null;
            }

            return _notificationsPersistence.GetNotificationsWithUsername(user.Username, markAsRead);
        }

        public UnreadCount GetUnreadCount(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _notificationsPersistence.GetUnreadCountWithUsername(user.Username);
        }

        public int GetUnreadNotificationsCount(User user)
        {
            if (user == null)
            {
                return 0;
            }

            return _notificationsPersistence.GetUnreadNotificationsCountWithUsername(user.Username)?.Amount ?? 0;
        }

        public bool AddNotification(User user, string message, NotificationLocation location, string href, string imageUrl = "", string domain = "")
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            Notification notification = _notificationsPersistence.AddNotification(user.Username, message, location, href, imageUrl);

            if (notification != null)
            {
                if (user.AllowNotifications)
                {
                    if (string.IsNullOrEmpty(domain))
                    {
                        HttpContext httpContext = _httpContextAccessor.HttpContext;
                        domain = Helper.GetDomain(httpContext);
                    }

                    Settings settings = _settingsRepository.GetSettings(user, false);
                    Task result = SendNewNotificationEmail(notification.Id, user, domain, settings.BatchNotificationEmailMinutes);
                }

                return true;
            }

            return false;
        }

        private async Task SendNewNotificationEmail(Guid notificationId, User user, string domain, int minutes)
        {
            await Task.Delay(TimeSpan.FromMinutes(minutes));

            List<Notification> notifications = GetNotifications(user, false);
            Notification notification = notifications.FirstOrDefault();
            if (notification != null && notification.Id == notificationId && notification.SeenDate == DateTime.MinValue)
            {
                int unreadNotifications = notifications.Count(x => x.SeenDate == DateTime.MinValue);
                string subject = $"You have {(unreadNotifications == 1 ? "a new notification" : $"{Helper.FormatNumber(unreadNotifications)} new notifications")}!";
                string title = $"New Notification{(unreadNotifications == 1 ? "" : "s")}";
                string body = $"You have <b>{(unreadNotifications == 1 ? "a new notification" : $"{Helper.FormatNumber(unreadNotifications)} new notifications")}</b>! Click the button below to see {(unreadNotifications == 1 ? "it" : "them")}.";

                Task result = _emailRepository.SendEmail(user, subject, title, body, $"/Notifications", false, domain);
            }
        }
    }
}
