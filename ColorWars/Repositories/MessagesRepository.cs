﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class MessagesRepository : IMessagesRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IMessagesPersistence _messagesPersistence;
        private IEmailRepository _emailRepository;
        private IUserRepository _userRepository;

        public MessagesRepository(IHttpContextAccessor httpContextAccessor, IMessagesPersistence messagesPersistence,
        IEmailRepository emailRepository, IUserRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _messagesPersistence = messagesPersistence;
            _emailRepository = emailRepository;
            _userRepository = userRepository;
        }

        public List<Message> GetMessagesBetweenUsernames(User user, User correspondent, bool updateRead = true)
        {
            if (user == null || correspondent == null || user.Username == correspondent.Username)
            {
                return null;
            }

            List<Message> messages = _messagesPersistence.GetMessagesBetweenUsernames(user.Username, correspondent.Username, updateRead);
            messages.Reverse();
            return messages;
        }

        public List<Message> GetAllMessages(User user, int topCount = 50)
        {
            if (user == null)
            {
                return null;
            }

            return _messagesPersistence.GetAllMessagesWithUsername(user.Username, topCount);
        }

        public int GetUnreadMessagesCount(User user, string correspondent = null)
        {
            if (user == null)
            {
                return 0;
            }

            return _messagesPersistence.GetUnreadMessagesCountWithUsername(user.Username, string.IsNullOrEmpty(correspondent) ? null : correspondent);
        }

        public bool AddMessage(User sender, User recipient, string message, string domain = "")
        {
            if (sender == null || recipient == null || sender.Username == recipient.Username || string.IsNullOrEmpty(message))
            {
                return false;
            }

            Message savedMessage = _messagesPersistence.AddMessage(sender.Username, recipient.Username, HttpUtility.HtmlEncode(message));

            if (savedMessage != null)
            {
                if (recipient.AllowNotifications)
                {
                    if (string.IsNullOrEmpty(domain))
                    {
                        HttpContext httpContext = _httpContextAccessor.HttpContext;
                        domain = Helper.GetDomain(httpContext);
                    }
                    Task result = SendNewMessageEmail(savedMessage.Id, sender, recipient, domain);
                }

                return true;
            }

            return false;
        }

        public bool SendCourierMessage(User recipient, string message, string domain = "")
        {
            if (recipient == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(domain))
            {
                HttpContext httpContext = _httpContextAccessor.HttpContext;
                domain = Helper.GetDomain(httpContext);
            }

            User user = _userRepository.GetUser(Helper.GetCourierUsername());
            return AddMessage(user, recipient, message, domain);
        }

        public bool SendPersonalMessage(User recipient, string domain, string message = null)
        {
            if (recipient == null)
            {
                return false;
            }

            Task result = SendPersonalMessageTask(recipient, message, domain);
            return true;
        }

        public bool DeleteMessagesBetweenUsernames(User user, User correspondent)
        {
            if (user == null || correspondent == null || user.Username == correspondent.Username)
            {
                return false;
            }

            return _messagesPersistence.DeleteMessagesBetweenUsernames(user.Username, correspondent.Username);
        }

        private async Task SendNewMessageEmail(Guid messageId, User sender, User recipient, string domain)
        {
            await Task.Delay(TimeSpan.FromMinutes(15));

            List<Message> messages = GetMessagesBetweenUsernames(sender, recipient);
            Message mostRecentMessage = messages?.LastOrDefault();
            if (mostRecentMessage != null && mostRecentMessage.Recipient == recipient.Username && mostRecentMessage.IsRead == false && mostRecentMessage.Id == messageId)
            {
                string subject = $"{sender.GetFriendlyName()} sent you a message!";
                string title = $"New Message";
                string body = $"You have a new message from <b><a href='{domain}/User/{sender.Username}' target='_blank'>{sender.GetFriendlyName()}</a></b>! Click the button below to check it out.";

                Task result = _emailRepository.SendEmail(recipient, subject, title, body, $"/Messages?username={sender.Username}", false, domain);
            }
        }

        private async Task SendPersonalMessageTask(User recipient, string message, string domain)
        {
            await Task.Delay(TimeSpan.FromMinutes(2));

            User welcomeUser = _userRepository.GetUser("tyler");
            recipient = _userRepository.GetUser(recipient.Username);
            if (recipient != null && welcomeUser != null)
            {
                message ??= $"Hey {recipient.GetFriendlyName()}! I recommend following the checklist on your Feed to start and if you have any questions, I'll be happy to answer them! {Resources.SiteName} is in beta so please let me know if anything isn't working as it should :)";
                AddMessage(welcomeUser, recipient, message, domain);
            }
        }
    }
}
