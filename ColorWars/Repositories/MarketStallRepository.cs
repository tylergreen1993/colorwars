﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class MarketStallRepository : IMarketStallRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IMarketStallPersistence _marketStallPersistence;
        private IItemsRepository _itemsRepository;
        private IUserRepository _userRepository;
        private ISession _session;

        public MarketStallRepository(IHttpContextAccessor httpContextAccessor, IMarketStallPersistence marketStallPersistence, IItemsRepository itemsRepository, IUserRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _marketStallPersistence = marketStallPersistence;
            _itemsRepository = itemsRepository;
            _userRepository = userRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<MarketStall> GetNewMarketStalls(bool isCached = true)
        {
            List<MarketStall> marketStalls = _marketStallPersistence.GetNewMarketStalls();

            foreach (MarketStall marketStall in marketStalls)
            {
                marketStall.Items = _marketStallPersistence.GetMarketStallItemsWithMarketStallId(marketStall.Id, marketStall.Discount);
                marketStall.Items.ForEach(x => x.Cost = Math.Max(1, x.Cost));
            }

            if (isCached)
            {
                _session.SetObject("NewMarketStalls", marketStalls);
            }

            return marketStalls;
        }

        public int GetUserMarketStallId(User user, bool isCached = true)
        {
            if (user == null)
            {
                return 0;
            }

            MarketStall marketStall = isCached ? _session.GetObject<MarketStall>("UserMarketStall") : null;
            if (marketStall != null)
            {
                return marketStall.Id;
            }

            marketStall = _marketStallPersistence.GetMarketStallWithUsername(user.Username);

            if (isCached)
            {
                _session.SetObject("UserMarketStall", marketStall);
            }

            return marketStall?.Id ?? 0;
        }

        public MarketStall GetMarketStall(User user)
        {
            if (user == null)
            {
                return null;
            }

            MarketStall marketStall = _marketStallPersistence.GetMarketStallWithUsername(user.Username);
            if (marketStall != null)
            {
                marketStall.Items = _marketStallPersistence.GetMarketStallItemsWithMarketStallId(marketStall.Id, marketStall.Discount);
                marketStall.Items.ForEach(x => x.Cost = Math.Max(1, x.Cost));
            }

            return marketStall;
        }

        public MarketStall GetMarketStall(int id)
        {
            MarketStall marketStall = _marketStallPersistence.GetMarketStallWithId(id);
            if (marketStall != null)
            {
                marketStall.Items = _marketStallPersistence.GetMarketStallItemsWithMarketStallId(marketStall.Id, marketStall.Discount);
                marketStall.Items.ForEach(x => x.Cost = Math.Max(1, x.Cost));
            }

            return marketStall;
        }

        public List<MarketStallItem> GetMarketStallItems(string query, bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            if (string.IsNullOrEmpty(query))
            {
                return null;
            }

            query = query.Replace("%", "[%]");
            ItemTheme searchTheme = ItemTheme.Default;
            if (query.Contains("-"))
            {
                IEnumerable<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>();
                foreach (ItemTheme theme in themes)
                {
                    if (query.ToLower().Contains(theme.ToString().ToLower()))
                    {
                        query = query.Split("-").First().Trim();
                        searchTheme = theme;
                        break;
                    }
                }
            }

            List<MarketStallItem> marketStallItems = _marketStallPersistence.GetMarketStallItemsWithSearchQuery(query, searchTheme);
            if (shouldMatchExactSearch)
            {
                query = query.Replace("[%]", "%");
                marketStallItems.RemoveAll(x => !x.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase));
            }
            if (category != ItemCategory.All)
            {
                marketStallItems.RemoveAll(x => x.Category != category);
            }
            if (condition != ItemCondition.All)
            {
                marketStallItems.RemoveAll(x => x.Condition != condition);
            }

            return marketStallItems;
        }

        public List<MarketStallHistory> GetMarketStallHistory(MarketStall marketStall)
        {
            if (marketStall == null)
            {
                return null;
            }

            return _marketStallPersistence.GetMarketStallsHistoryWithMarketStallId(marketStall.Id);
        }

        public List<MarketStallHistory> GetMarketStallHistory(User user)
        {
            if (user == null)
            {
                return null;
            }

            int marketStallId = GetUserMarketStallId(user);
            if (marketStallId <= 0)
            {
                return null;
            }

            return _marketStallPersistence.GetMarketStallsHistoryWithMarketStallId(marketStallId);
        }

        public bool AddMarketStall(User user, string name, string color, string description, out MarketStall marketStall)
        {
            marketStall = null;
            if (user == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            marketStall = _marketStallPersistence.AddMarketStall(user.Username, name, description, color);

            ClearCache();

            return marketStall != null;
        }

        public bool AddMarketStallItem(User user, MarketStall marketStall, Item item, int cost)
        {
            if (user == null || marketStall == null || item == null)
            {
                return false;
            }

            if (cost <= 0)
            {
                return false;
            }

            return _marketStallPersistence.AddMarketStallItem(item, marketStall.Id, cost);
        }

        public bool AddMarketStallHistory(User user, MarketStall marketStall, MarketStallItem item)
        {
            if (user == null || marketStall == null || item == null)
            {
                return false;
            }

            return _marketStallPersistence.AddMarketStallsHistory(marketStall.Id, user.Username, item);
        }

        public bool UpdateMarketStall(MarketStall marketStall)
        {
            if (marketStall == null)
            {
                return false;
            }

            return _marketStallPersistence.UpdateMarketStallWithId(marketStall);
        }

        public bool RepairAllMarketStallItems(MarketStall marketStall)
        {
            if (marketStall == null)
            {
                return false;
            }

            return _marketStallPersistence.RepairAllMarketStallItems(marketStall);
        }

        public bool DeleteMarketStall(MarketStall marketStall)
        {
            if (marketStall == null)
            {
                return false;
            }

            User user = _userRepository.GetUser(marketStall.Username);
            if (user == null)
            {
                Log.Debug($"Market Stall user was null.");
                return false;
            }
            foreach (Item item in marketStall.Items)
            {
                _itemsRepository.AddItem(user, item);
            }

            ClearCache();

            return _marketStallPersistence.DeleteMarketStallWithId(marketStall.Id);
        }

        public bool DeleteMarketStallItem(MarketStallItem marketStallItem)
        {
            if (marketStallItem == null)
            {
                return false;
            }

            return _marketStallPersistence.DeleteMarketStallItemWithSelectionId(marketStallItem.SelectionId);
        }

        private void ClearCache()
        {
            _session.Remove("UserMarketStall");
        }
    }
}
