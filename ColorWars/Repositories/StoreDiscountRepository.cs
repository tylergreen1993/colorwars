﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class StoreDiscountRepository : IStoreDiscountRepository
    {
        private IStoreRepository _storeRepository;

        public StoreDiscountRepository(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public ItemCategory GetDiscountCategory(User user)
        {
            List<ItemCategory> itemCategories = Enum.GetValues(typeof(ItemCategory)).Cast<ItemCategory>().ToList();
            List<ItemCategory> excludedCategories = _storeRepository.GetExcludedCategories();
            itemCategories.RemoveAll(x => x < 0 || excludedCategories.Any(y => y == x));

            int currentDay = DateTime.UtcNow.DayOfYear;
            if (currentDay % 3 != 1 || DateTime.UtcNow.Date <= user.CreatedDate.Date)
            {
                return ItemCategory.None;
            }
            return itemCategories[currentDay % itemCategories.Count];
        }
    }
}