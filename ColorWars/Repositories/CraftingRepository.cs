﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class CraftingRepository : ICraftingRepository
    {
        private ICraftingPersistence _craftingPersistence;
        private IItemsRepository _itemsRepository;

        public CraftingRepository(ICraftingPersistence craftingPersistence, IItemsRepository itemsRepository)
        {
            _craftingPersistence = craftingPersistence;
            _itemsRepository = itemsRepository;
        }

        public Item GetFurnaceItem(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _craftingPersistence.GetFurnaceItemWithUsername(user.Username, isCached);
        }

        public bool AddFurnaceItem(User user, Item item)
        {
            if (user == null || item  == null)
            {
                return false;
            }

            return _craftingPersistence.AddFurnaceItem(item.SelectionId, item.Id, user.Username, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool DeleteFurnaceItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _craftingPersistence.DeleteFurnaceItemWithSelectionId(item.SelectionId);
        }

        public Item DefrostItem(User user, Item item)
        {
            if (item.Category != ItemCategory.Frozen)
            {
                return item;
            }

            ItemCategory[] excludedItems = { ItemCategory.Frozen, ItemCategory.Companion, ItemCategory.Beyond };
            List<Item> items = _itemsRepository.GetAllItems(ItemCategory.All, excludedItems).FindAll(x => x.Points >= item.Points && x.Points <= item.Points * 3);

            Random rnd = new Random();
            Item defrostedItem = items[rnd.Next(items.Count)];
            defrostedItem.CreatedDate = item.CreatedDate;
            defrostedItem.RepairedDate = item.RepairedDate;

            if (defrostedItem.Category == ItemCategory.Deck)
            {
                if (rnd.Next(100000) <= item.Points)
                {
                    ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    ItemTheme theme = themes[rnd.Next(themes.Length)];
                    defrostedItem.Theme = theme;
                }
            }

            if (DeleteFurnaceItem(item))
            {
                AddFurnaceItem(user, defrostedItem);
            }

            return defrostedItem;
        }
    }
}
