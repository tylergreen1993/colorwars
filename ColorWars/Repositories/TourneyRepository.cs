﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class TourneyRepository : ITourneyRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ITourneyPersistence _tourneyPersistence;
        private IUserRepository _userRepository;
        private IPointsRepository _pointsRepository;
        private ISettingsRepository _settingsRepository;
        private IAccomplishmentsRepository _accomplishmentsRepository;
        private INotificationsRepository _notificationsRepository;
        private IEmailRepository _emailRepository;
        private ISession _session;

        public TourneyRepository(IHttpContextAccessor httpContextAccessor, ITourneyPersistence tourneyPersistence,
        IUserRepository userRepository, IPointsRepository pointsRepository, ISettingsRepository settingsRepository,
        IAccomplishmentsRepository accomplishmentsRepository, INotificationsRepository notificationsRepository,
        IEmailRepository emailRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _tourneyPersistence = tourneyPersistence;
            _userRepository = userRepository;
            _pointsRepository = pointsRepository;
            _settingsRepository = settingsRepository;
            _accomplishmentsRepository = accomplishmentsRepository;
            _notificationsRepository = notificationsRepository;
            _emailRepository = emailRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<Tourney> GetAllTourneys()
        {
            List<Tourney> tourneys = _tourneyPersistence.GetAllTourneys();
            foreach (Tourney tourney in tourneys)
            {
                UpdateTourneyWithMatches(tourney);
            }

            return tourneys.OrderBy(x => x.GetStatus()).ToList();
        }

        public List<Tourney> GetUserTourneys(User user, bool onlyActive = false, bool orderBy = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Tourney> tourneys = _tourneyPersistence.GetTourneysWithUsername(user.Username);
            if (onlyActive)
            {
                tourneys.RemoveAll(x => (DateTime.UtcNow - x.CreatedDate).TotalHours >= 96);
            }
            foreach (Tourney tourney in tourneys)
            {
                UpdateTourneyWithMatches(tourney);
            }
            if (onlyActive)
            {
                tourneys.RemoveAll(x => x.GetStatus() == TourneyStatus.Completed);
            }
            if (orderBy)
            {
                return tourneys.OrderByDescending(x => x.GetStatus() == TourneyStatus.InProgress).ThenByDescending(x => x.GetStatus()).ToList();
            }

            return tourneys;
        }

        public Tourney GetTourneyWithId(int id)
        {
            Tourney tourney = _tourneyPersistence.GetTourneyWithId(id);
            if (tourney == null)
            {
                return null;
            }

            UpdateTourneyWithMatches(tourney);

            return tourney;
        }

        public Tourney GetTrendingTourney(User user, bool isElite, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Tourney> allTourneys = isCached ? _session.GetObject<List<Tourney>>("AllJoinableTourneys") : null;
            if (allTourneys != null)
            {
                Tourney tourney = allTourneys.FirstOrDefault();
                if (tourney != null && (DateTime.UtcNow - tourney.LastRetrievedDate).TotalHours <= 3)
                {
                    if (tourney.Id == 0)
                    {
                        return null;
                    }

                    return tourney;
                }
            }

            allTourneys = GetAllTourneys().FindAll(x => (x.TourneyCaliber == TourneyCaliber.All || x.TourneyCaliber == (isElite ? TourneyCaliber.EliteOnly : TourneyCaliber.NonEliteOnly)) && x.GetStatus() == TourneyStatus.NotStarted && !x.GetEntrants(0).Any(x => x == user.Username) && (DateTime.UtcNow - x.CreatedDate).TotalDays <= 3).OrderBy(x => ((DateTime.UtcNow - x.CreatedDate).TotalHours + 1) / ((double)x.GetEntrantsCount() / x.RequiredParticipants)).ToList();
            if (allTourneys.FirstOrDefault() == null)
            {
                allTourneys = new List<Tourney>
                {
                    new Tourney
                    {
                        LastRetrievedDate = DateTime.UtcNow
                    }
                };
            }
            else
            {
                allTourneys.First().LastRetrievedDate = DateTime.UtcNow;
            }

            if (isCached)
            {
                _session.SetObject("AllJoinableTourneys", allTourneys);
            }

            if (allTourneys?.FirstOrDefault()?.Id == 0)
            {
                return null;
            }

            return allTourneys?.FirstOrDefault();
        }

        public bool AddTourney(User user, int entranceCost, int matchesPerRound, int requiredParticipants, bool isSponsored, string tourneyName, TourneyCaliber tourneyCaliber, out Tourney tourney)
        {
            tourney = null;
            if (user == null || matchesPerRound < 1 || requiredParticipants < 4 || string.IsNullOrEmpty(tourneyName))
            {
                return false;
            }

            tourney = _tourneyPersistence.AddTourney(entranceCost, matchesPerRound, requiredParticipants, isSponsored, user.Username, tourneyName, tourneyCaliber);
            if (tourney == null)
            {
                return false;
            }

            return true;
        }

        public bool AddTourneyMatch(User user, Tourney tourney, int round)
        {
            if (user == null || tourney == null || round < 0)
            {
                return false;
            }

            if (_tourneyPersistence.AddTourneyMatch(tourney.Id, round, user.Username))
            {
                ClearCache();

                return true;
            }

            return false;
        }

        public bool UpdateTourneyMatch(TourneyMatch tourneyMatch)
        {
            if (tourneyMatch == null)
            {
                return false;
            }

            return _tourneyPersistence.UpdateTourneyMatch(tourneyMatch);
        }

        public bool DeleteTourney(Tourney tourney, bool isExpired = false)
        {
            if (tourney == null)
            {
                return false;
            }

            if (_tourneyPersistence.DeleteTourney(tourney.Id))
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    foreach (string username in tourney.GetEntrants(0))
                    {
                        User user = _userRepository.GetUser(username);
                        if (user != null)
                        {
                            if (!tourney.IsSponsored)
                            {
                                _pointsRepository.AddPoints(user, tourney.EntranceCost);
                            }
                            if (isExpired)
                            {
                                _notificationsRepository.AddNotification(user, $"The tourney \"{tourney.Name}\" ran out of time to complete and was cancelled. If there was an entry cost, it was returned to you.", NotificationLocation.Tourney, "#", domain: domain);
                            }
                            else
                            {
                                if (user.Username != tourney.Creator)
                                {
                                    _notificationsRepository.AddNotification(user, $"The tourney \"{tourney.Name}\" has been cancelled. If there was an entry cost, it was returned to you.", NotificationLocation.Tourney, "#", domain: domain);
                                }
                            }
                        }
                    }
                    if (tourney.IsSponsored)
                    {
                        User tourneyCreator = _userRepository.GetUser(tourney.Creator);
                        if (tourneyCreator != null)
                        {
                            _pointsRepository.AddPoints(tourneyCreator, tourney.EntranceCost);
                            if (!tourney.GetEntrants(0).Any(x => x == tourneyCreator.Username))
                            {
                                _notificationsRepository.AddNotification(tourneyCreator, $"Your tourney \"{tourney.Name}\" ended before it finished. If there was a sponsored prize, it was returned to you.", NotificationLocation.Tourney, "#", domain: domain);
                            }
                        }
                    }
                });

                return true;
            }

            return false;
        }

        public void AddTourneyProgress(User user, Guid tourneyMatchId, MatchStatus finalStatus, bool isUser)
        {
            Tourney tourney = GetUserTourneys(user).Find(x => x.TourneyMatches.Any(x => x.Id == tourneyMatchId));
            if (tourney == null)
            {
                return;
            }
            if (tourney.GetStatus() == TourneyStatus.Completed)
            {
                List<Accomplishment> accomplishments = _accomplishmentsRepository.GetAccomplishments(user, isUser);
                int reward = 0;
                if (finalStatus == MatchStatus.Won)
                {
                    reward = tourney.IsSponsored ? tourney.EntranceCost : (int)Math.Round(tourney.EntranceCost * tourney.RequiredParticipants * 0.8);
                    Settings settings = _settingsRepository.GetSettings(user, isUser);
                    settings.TotalTourneysWon++;
                    _settingsRepository.SetSetting(user, nameof(settings.TotalTourneysWon), settings.TotalTourneysWon.ToString(), isUser);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TourneyWinner))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TourneyWinner, isUser);
                    }
                    if (settings.TotalTourneysWon == 5)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FiveTourneyWins))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FiveTourneyWins, isUser);
                        }
                    }
                    if (settings.TotalTourneysWon == 10)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TenTourneyWins))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TenTourneyWins, isUser);
                        }
                    }
                    if (settings.TotalTourneysWon == 20)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TwentyTourneyWins))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TwentyTourneyWins, isUser);
                        }
                    }
                    if (settings.TotalTourneysWon == 50)
                    {
                        if (!accomplishments.Exists(x => x.Id == AccomplishmentId.FiftyTourneyWins))
                        {
                            _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.FiftyTourneyWins, isUser);
                        }
                    }
                }
                else
                {
                    reward = tourney.IsSponsored ? tourney.EntranceCost : (int)Math.Round(tourney.EntranceCost * tourney.RequiredParticipants * 0.2);
                    if (!accomplishments.Exists(x => x.Id == AccomplishmentId.TourneySecond))
                    {
                        _accomplishmentsRepository.AddAccomplishment(user, AccomplishmentId.TourneySecond, isUser);
                    }
                }
                _pointsRepository.AddPoints(user, reward);
                return;
            }
            if (finalStatus != MatchStatus.Won)
            {
                return;
            }
            TourneyMatch tourneyMatch = tourney.TourneyMatches.Find(x => x.Id == tourneyMatchId);
            if (string.IsNullOrEmpty(tourneyMatch.GetWinner()))
            {
                return;
            }
            AddTourneyMatch(user, tourney, tourneyMatch.Round + 1);
            TourneyMatch newTourneyMatch = GetTourneyWithId(tourney.Id).TourneyMatches.Find(x => x.Round == tourneyMatch.Round + 1 && x.Username2 == user.Username);
            if (newTourneyMatch != null)
            {
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    User challengeUser = _userRepository.GetUser(newTourneyMatch.Username1);
                    if (challengeUser != null)
                    {
                        _notificationsRepository.AddNotification(challengeUser, $"@{user.Username} is your next opponent in the \"{tourney.Name}\" tourney! You should play your match as soon as possible!", NotificationLocation.Tourney, $"/Tourney?id={tourney.Id}", domain: domain);

                        string subject = "Your next tourney challenger!";
                        string title = $"{tourney.Name} Tourney";
                        string body = $"<b><a href='{domain}/User/{user.Username}'>{user.Username}</a></b> is your next opponent in the <b>{tourney.Name}</b> tourney! Click the button below to challenge them! ";
                        _emailRepository.SendEmail(challengeUser, subject, title, body, $"/Tourney?id={tourney.Id}", domain: domain);
                    }
                });
            }
        }

        private void UpdateTourneyWithMatches(Tourney tourney)
        {
            List<TourneyMatch> tourneyMatches = _tourneyPersistence.GetTourneyMatchesWithId(tourney.Id);
            tourney.TourneyMatches = tourneyMatches;
        }

        private void ClearCache()
        {
            _session.Remove("AllJoinableTourneys");
        }
    }
}
