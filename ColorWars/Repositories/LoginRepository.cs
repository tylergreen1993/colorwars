﻿using System;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class LoginRepository : ILoginRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IUserPersistence _userPersistence;
        private IUserRepository _userRepository;
        private ISession _session;

        public LoginRepository(IHttpContextAccessor httpContextAccessor, IUserPersistence userPersistence, IUserRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _userPersistence = userPersistence;
            _userRepository = userRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public User LoginUser(string username, string password)
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;
            string ipAddress = httpContext.Connection.RemoteIpAddress.ToString();
            string userAgent = httpContext.Request.Headers["User-Agent"].ToString();
            string clientInfo = Helper.GetUserAgentInfo(userAgent);

            User user = _userPersistence.GetUserWithUsernameAndPassword(username, password, ipAddress, clientInfo);
            if (user == null)
            {
                return null;
            }

            if (user.IsSuspended)
            {
                Messages.AddMessage($"Your account has been suspended until {user.SuspendedDate.ToString("MMMM d, yyyy")} for violating the Terms of Service.");
                return null;
            }
            if (user.InactiveType == InactiveAccountType.Deleted)
            {
                Messages.AddMessage($"This account was permanently deleted.");
                return null;
            }

            _session.Clear();

            if (user.InactiveType == InactiveAccountType.Disabled)
            {
                if (_userRepository.EnableUser(user))
                {
                    Messages.AddSnack("Welcome back! By logging in, you've enabled your account again!");
                }
            }

            IResponseCookies cookies = httpContext.Response.Cookies;
            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddYears(1)
            };
            cookies.Append("Username", user.Username, options);
            cookies.Append("AccessToken", user.AccessToken.ToString(), options);

            return user;
        }

        public User AuthenticateUser()
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;

            IResponseCookies responseCookies = httpContext.Response.Cookies;
            IRequestCookieCollection requestCookies = httpContext.Request.Cookies;

            if (!string.IsNullOrEmpty(requestCookies["AccessToken"]) && !string.IsNullOrEmpty(requestCookies["Username"]))
            {
                string username = requestCookies["Username"];
                Guid accessToken = Guid.Parse(requestCookies["AccessToken"]);

                User user = _userPersistence.GetUserWithAccessToken(username, accessToken);
                if (user == null || user.IsSuspended)
                {
                    responseCookies.Delete("Username");
                    responseCookies.Delete("AccessToken");

                    if (user != null && user.IsSuspended)
                    {
                        Messages.AddMessage($"Your account has been suspended until {user.SuspendedDate.ToString("MMMM d, yyyy")} for violating the Terms of Service.");
                        return null;
                    }
                    if (user?.InactiveType == InactiveAccountType.Deleted)
                    {
                        Messages.AddMessage($"This account was permanently deleted.");
                        return null;
                    }
                }

                if (user != null && user.FlushSession)
                {
                    _session.Clear();

                    user.FlushSession = false;
                    _userRepository.UpdateUser(user);
                }

                return user;
            }

            return null;
        }
    }
}
