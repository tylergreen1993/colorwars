﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class StoreRepository : IStoreRepository
    {
        private IStorePersistence _storePersistence;
        private IItemsRepository _itemsRepository;

        public StoreRepository(IStorePersistence storePersistence, IItemsRepository itemsRepository)
        {
            _storePersistence = storePersistence;
            _itemsRepository = itemsRepository;
        }

        public List<StoreItem> GetStoreItems()
        {
            List<StoreItem> storeItems = _storePersistence.GetStoreItems();
            if (storeItems != null && storeItems.Any(x => x.GetAgeInMinutes() >= Helper.GetStoreRefreshMinutes()))
            {
                return _storePersistence.GetStoreItems(true);
            }
            return storeItems;
        }

        public List<StoreItem> SearchStoreItems(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All)
        {
            List<StoreItem> storeItems = GenerateStoreItems();
            if (!string.IsNullOrEmpty(query))
            {
                storeItems = matchExactSearch ? storeItems.FindAll(x => x.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase)) : storeItems.FindAll(x => x.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
            }

            if (category != ItemCategory.All)
            {
                storeItems = storeItems.FindAll(x => x.Category == category);
            }
            if (condition != ItemCondition.All)
            {
                storeItems = storeItems.FindAll(x => x.Condition == condition);
            }

            return storeItems;
        }

        public List<StoreItem> GenerateStoreItems()
        {
            List<StoreItem> storeItems = GetStoreItems();
            if (storeItems == null || storeItems.Count == 0 || storeItems.First().GetAgeInMinutes() >= Helper.GetStoreRefreshMinutes())
            {
                if (storeItems.Count > 0)
                {
                    DeleteStoreItems();
                }

                Random rnd = new Random();
                ItemCategory[] excludedCategories = GetExcludedCategories().ToArray();
                List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, excludedCategories).FindAll(x => x.Points <= 1000000 && x.Points > 1);

                int averagePoints = (int)Math.Round(allItems.Select(x => x.Points).Average());
                List<Item> lowerTierItems = allItems.FindAll(x => x.Points < averagePoints / 3);
                List<Item> higherTierItems = allItems.FindAll(x => x.Points >= averagePoints / 3);

                List<Item> pickedItems = new List<Item>();
                int maxStoreSize = 25;
                for (int i = 0; i < maxStoreSize; i++)
                {
                    Item item = rnd.Next(6) == 1 ? higherTierItems[rnd.Next(higherTierItems.Count)] : lowerTierItems[rnd.Next(lowerTierItems.Count)];
                    pickedItems.Add(item);
                }

                List<Item> cardPacks = allItems.FindAll(x => x.Category == ItemCategory.Pack);
                pickedItems.Add(cardPacks[rnd.Next(cardPacks.Count)]);

                Item starterEnhancer = allItems.Find(x => x.Name.Equals("+10", StringComparison.InvariantCultureIgnoreCase) && x.Category == ItemCategory.Enhancer && x.TotalUses > 0);
                pickedItems.Add(starterEnhancer);

                List<Item> defaultThemes = pickedItems.FindAll(x => x.Category == ItemCategory.Theme && x.Name.Contains("Default", StringComparison.InvariantCultureIgnoreCase));
                if (defaultThemes.Count > 0 && rnd.Next(2) == 1)
                {
                    pickedItems.RemoveAll(x => x.Id == defaultThemes.First().Id);
                    List<Item> allThemes = allItems.FindAll(x => x.Category == ItemCategory.Theme);
                    pickedItems.Add(allThemes[rnd.Next(allThemes.Count)]);
                }

                IEnumerable<IGrouping<ItemCategory, Item>> groupedItems = pickedItems.OrderBy(x => x.Points).GroupBy(x => x.Category);
                pickedItems = groupedItems.SelectMany(x =>
                {
                    if (x.Key == ItemCategory.Candy)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Theme)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Jewel)
                    {
                        return x.Take(1);
                    }
                    if (x.Key == ItemCategory.Deck)
                    {
                        return x.Take(8);
                    }
                    if (x.Key == ItemCategory.Coupon)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Stealth)
                    {
                        return x.Take(1);
                    }
                    if (x.Key == ItemCategory.Crafting)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Upgrader)
                    {
                        return x.Take(2);
                    }
                    if (x.Key == ItemCategory.Art)
                    {
                        return x.Take(2);
                    }
                    return x;
                }).ToList();

                foreach (Item item in pickedItems.Distinct())
                {
                    int cost = (int)Math.Round((item.Points + (double)rnd.Next(0, item.Points / 6)) / 10) * 10;
                    int minCost = Math.Max(1, cost - Math.Max(1, rnd.Next(0, cost / 4)));
                    AddStoreItem(item, cost, minCost);
                }

                return GetStoreItems();
            }

            return storeItems;
        }

        public List<ItemCategory> GetExcludedCategories()
        {
            return new List<ItemCategory> { ItemCategory.Egg, ItemCategory.Sleeve, ItemCategory.Frozen, ItemCategory.Companion, ItemCategory.Seed, ItemCategory.Fairy, ItemCategory.Beyond, ItemCategory.Key, ItemCategory.Custom };
        }

        public bool AddStoreItem(Item item, int cost, int minCost)
        {
            if (item == null)
            {
                return false;
            }
            return _storePersistence.AddStoreItem(item.Id, cost, minCost);
        }

        public bool DeleteStoreItems()
        {
            return _storePersistence.DeleteStoreItems();
        }
    }
}
