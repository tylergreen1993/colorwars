﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class FeedRepository : IFeedRepository
    {
        private IFeedPersistence _feedPersistence;
        private ICPURepository _cpuRepository;
        private IRelicsRepository _relicsRepository;
        private IUserRepository _userRepository;

        public FeedRepository(IFeedPersistence feedPersistence, ICPURepository cpuRepository,
        IRelicsRepository relicsRepository, IUserRepository userRepository)
        {
            _feedPersistence = feedPersistence;
            _cpuRepository = cpuRepository;
            _relicsRepository = relicsRepository;
            _userRepository = userRepository;
        }

        public List<HiddenFeedCategory> GetHiddenFeedCategories(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _feedPersistence.GetHiddenFeedCategories(user.Username, isCached);
        }

        public bool AddHiddenFeedCategory(User user, FeedCategory feedCategory)
        {
            if (user == null || feedCategory <= FeedCategory.All)
            {
                return false;
            }

            return _feedPersistence.AddHiddenFeedCategory(user.Username, feedCategory);
        }

        public bool DeleteHiddenFeedCategory(User user, FeedCategory feedCategory)
        {
            if (user == null || feedCategory <= FeedCategory.All)
            {
                return false;
            }

            return _feedPersistence.DeleteHiddenFeedCategory(user.Username, feedCategory);
        }

        public List<Feed> ConvertFrom(News news)
        {
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Id = news.Id,
                    Description = "A new article was posted,",
                    Object = $"\"{news.Title}\"",
                    Category = FeedCategory.News,
                    Href = $"/News?id={news.Id}",
                    CreatedDate = news.PublishDate
                }
            };
            return feeds;
        }

        public List<Feed> ConvertFrom(User user, List<Accomplishment> accomplishments, Settings settings)
        {
            List<Feed> feeds = new List<Feed>();
            Dictionary<DateTime, Relic> relics = new Dictionary<DateTime, Relic>();
            foreach (Accomplishment accomplishment in accomplishments)
            {
                if (accomplishment.Category > AccomplishmentCategory.Orange)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Description = "You got the accomplishment,",
                        Object = $"\"{accomplishment.Name}\"",
                        Category = FeedCategory.Accomplishments,
                        Href = $"/Profile#accomplishment-{(int)accomplishment.Id}",
                        CreatedDate = accomplishment.ReceivedDate,
                        ImageUrl = accomplishment.ImageUrl
                    });
                }
                if (accomplishment.Id == AccomplishmentId.CPULevelFive)
                {
                    feeds.Add(new Feed
                    {
                        Description = "You can now access the",
                        Object = "Challenger Dome",
                        Category = FeedCategory.ChallengerDome,
                        Href = $"/ChallengerDome",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(2)
                    });
                    feeds.Add(new Feed
                    {
                        Description = $"You can now unlock new upgrades and Power Moves using your Training Tokens at the",
                        Object = $"Training Center",
                        Category = FeedCategory.TrainingCenter,
                        Href = $"/TrainingCenter#training-tokens",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(3)
                    });

                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(5));
                }
                else if (accomplishment.Id == AccomplishmentId.CPULevelTen)
                {
                    if (settings.PhantomDoorLevel == 0)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You now have the necessary Relics to enter the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(3)
                        });
                    }
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Description = "You can now make trades at the",
                        Object = "Trading Square",
                        Category = FeedCategory.Trades,
                        Href = $"/Trading",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(2)
                    });
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(10));
                }
                else if (accomplishment.Id == AccomplishmentId.CPULevelFifteen)
                {
                    if (settings.PhantomDoorLevel == 2)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now enter the next phase of the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(2)
                        });
                    }
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Description = "You can now access the",
                        Object = "Art Factory",
                        Category = FeedCategory.ArtFactory,
                        Href = $"/ArtFactory",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(15));
                }
                else if (accomplishment.Id == AccomplishmentId.CPULevelTwenty)
                {
                    if (settings.PhantomDoorLevel == 4)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now enter the next phase of the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                        });
                    }
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(20));
                }
                else if (accomplishment.Id == AccomplishmentId.CPULevelTwentyFive)
                {
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(25));
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Description = "You can now visit the",
                        Object = "Clubhouse",
                        Category = FeedCategory.Clubhouse,
                        Href = $"/Clubhouse",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.CPULevelThirty)
                {
                    if (settings.PhantomDoorLevel == 6)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now enter the next phase of the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                        });
                    }
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(30));
                }
                else if (accomplishment.Id == AccomplishmentId.CPUCompleted)
                {
                    if (settings.PhantomDoorLevel == 8)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now enter the next phase of the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(3)
                        });
                    }
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Ancient Plateau",
                        Description = "You earned all the Relics and can now access the",
                        Object = "Ancient Plateau",
                        Category = FeedCategory.TrainingCenter,
                        Href = $"/TrainingCenter/Plateau",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(2)
                    });
                    relics.Add(accomplishment.ReceivedDate, _relicsRepository.GetRelic(35));
                    if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
                    {
                        int hoursSince = (int)(DateTime.UtcNow - accomplishment.ReceivedDate).TotalHours;
                        int hoursToAdd = hoursSince - (hoursSince % 12);
                        if (hoursToAdd > 0)
                        {
                            feeds.Add(new Feed
                            {
                                Guid = accomplishment.SelectionId,
                                Title = "Ancient Plateau",
                                Description = "The Right-Side Up Guard is waiting to challenge you at the",
                                Object = "Ancient Plateau",
                                ImageUrl = "CPU/Guard.png",
                                Category = FeedCategory.TrainingCenter,
                                Href = $"/TrainingCenter/Plateau",
                                CreatedDate = accomplishment.ReceivedDate.AddHours(hoursToAdd)
                            });
                        }
                    }
                }
                else if (accomplishment.Id == AccomplishmentId.ColorWarsContribute)
                {
                    feeds.Add(new Feed
                    {
                        Title = "Badges",
                        Description = $"You can now set the {(user.Id % 2 == 0 ? "Orange Team" : "Blue Team")} badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.ClubhouseMember)
                {
                    feeds.Add(new Feed
                    {
                        Title = "Badges",
                        Description = $"You can now set the Clubhouse badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.RightSideUpGuardDefeated)
                {
                    feeds.Add(new Feed
                    {
                        Description = "You can now rematch all of the CPU Challengers at",
                        Object = "Intensity 2",
                        Category = FeedCategory.Stadium,
                        Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                    feeds.Add(new Feed
                    {
                        Description = "You now have access to",
                        Object = "The Tower",
                        Category = FeedCategory.Tower,
                        Href = $"/Tower",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(2)
                    });
                    if (settings.BeyondWorldLevel <= 7)
                    {
                        feeds.Add(new Feed
                        {
                            Description = "Uh oh! The Young Wizard accidentally opened up an entrance to the",
                            Object = "Beyond World",
                            Category = FeedCategory.BeyondWorld,
                            Href = $"/BeyondWorld",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(3)
                        });
                    }
                }
                else if (accomplishment.Id == AccomplishmentId.DefeatBeyondCreature)
                {
                    feeds.Add(new Feed
                    {
                        Title = "Badges",
                        Description = "You can now set the Beyond badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.UnknownDefeated)
                {
                    feeds.Add(new Feed
                    {
                        Description = "You can now complete the secret",
                        Object = "Elite Checklist",
                        Category = FeedCategory.Checklist,
                        OnClick = "highlightChecklist();",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.EliteZone)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Description = "You rank in the top 25% of users at the Stadium and are now part of the",
                        Object = "Elite Zone",
                        Category = FeedCategory.Stadium,
                        Href = $"/Stadium?stadiumTab={StadiumTab.User}#challenge-tabs",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.ShowroomAccess)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Showroom",
                        Description = "Your Showroom is open on your profile. You should add an Art Card to it.",
                        Category = FeedCategory.All,
                        Href = $"/Profile/Showroom",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.Helper)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Badges",
                        Description = "You can now set the Helper badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.CommunityHelper)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Badges",
                        Description = "You can now set the Community Contributor badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.JoinedClub)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Badges",
                        Description = "You can now set the Club badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.SponsorSocietyDonor)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Badges",
                        Description = "You can now set the Sponsor Society badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.ReferredUser)
                {
                    feeds.Add(new Feed
                    {
                        Guid = accomplishment.SelectionId,
                        Title = "Badges",
                        Description = "You can now set the Ambassador badge from your Preferences.",
                        Category = FeedCategory.All,
                        Href = $"/Preferences",
                        CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                    });
                }
                else if (accomplishment.Id == AccomplishmentId.PhantomSpiritDefeated)
                {
                    if (settings.PhantomDoorLevel == 2)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now continue your journey beyond the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                        });
                    }
                }
                else if (accomplishment.Id == AccomplishmentId.SpiritSistersDefeated)
                {
                    if (settings.PhantomDoorLevel == 5)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You can now continue further along in your journey beyond the",
                            Object = "Phantom Door",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                        });
                    }
                }
                else if (accomplishment.Id == AccomplishmentId.PhantomBossDefeated)
                {
                    if (settings.PhantomDoorLevel == 10)
                    {
                        feeds.Add(new Feed
                        {
                            Guid = accomplishment.SelectionId,
                            Description = "You defeated the Phantom Boss and freed the spirits from the <b>Phantom Door</b>!",
                            Category = FeedCategory.PhantomDoor,
                            Href = $"/PhantomDoor",
                            CreatedDate = accomplishment.ReceivedDate.AddSeconds(1)
                        });
                    }
                }
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.ReferredUser))
            {
                int ageInDays = user.GetAgeInDays();
                if (ageInDays > 0 && ageInDays % 4 == 2)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"Enjoying {Resources.SiteName}? Consider referring a friend and you can earn {Helper.GetFormattedPoints(Helper.PointsPerReferralCollect() * 30)} per new user!",
                        Category = FeedCategory.Referrals,
                        Href = $"/Referrals",
                        CreatedDate = DateTime.UtcNow.Date
                    });
                }
            }

            foreach (KeyValuePair<DateTime, Relic> relic in relics)
            {
                CPU cpu = _cpuRepository.GetCPU(relic.Value.Level - 1);
                feeds.Add(new Feed
                {
                    Title = "Ancient Plateau",
                    Description = $"You defeated the Checkpoint CPU, {cpu.Name}, at the Stadium and earned the",
                    Object = $"{relic.Value.Name} Relic",
                    Category = FeedCategory.TrainingCenter,
                    ImageUrl = relic.Value.ImageUrl,
                    Href = $"/TrainingCenter/Plateau#{relic.Value.Level}",
                    CreatedDate = relic.Key.AddSeconds(1)
                });
            }

            if ((accomplishments.Count >= 15 && user.GetAgeInMinutes() >= 30 && settings.MatchLevel >= 2) || settings.MatchLevel == 9)
            {
                DateTime showroomStoreDate = new DateTime(Math.Max(user.CreatedDate.AddMinutes(30).Ticks, accomplishments.Take(15).Last().ReceivedDate.Ticks));
                feeds.Add(new Feed
                {
                    Title = "Showroom Store",
                    Description = "You were invited to the",
                    ImageUrl = "CPU/ShowroomOwner.png",
                    Object = "Showroom Store",
                    Category = FeedCategory.BuyersClub,
                    Href = $"/BuyersClub/Showroom",
                    CreatedDate = showroomStoreDate.AddSeconds(1)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(Referral referral)
        {
            List<Feed> feeds = new List<Feed>();

            if (referral.InterestTimes > 0)
            {
                feeds.Add(new Feed
                {
                    Username = referral.Username,
                    Description = $"earned you {Helper.GetFormattedPoints(Helper.PointsPerReferralCollect())} from your referral.",
                    Category = FeedCategory.Referrals,
                    Href = $"/Referrals#{referral.Username}",
                    CreatedDate = referral.TransactionDate
                });
            }
            feeds.Add(new Feed
            {
                Username = referral.Username,
                Description = $"signed up from your referral.",
                Category = FeedCategory.Referrals,
                Href = $"/Referrals#total-earned-title",
                CreatedDate = referral.CreatedDate
            });

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, Auction auction, Settings settings)
        {
            List<Feed> feeds = new List<Feed>();

            if (auction.Username == user.Username)
            {
                if (settings.ReceiveAuctionBidNotifications && auction.Bids.Count > 0)
                {
                    IEnumerable<IGrouping<string, Bid>> bidsByUsername = auction.Bids.GroupBy(x => x.Username);
                    int totalBidders = bidsByUsername.Count();
                    feeds.Add(new Feed
                    {
                        Id = auction.Id,
                        Username = bidsByUsername.First().Key,
                        Description = totalBidders == 1 ? "placed a bid on your auction." : $"and {Helper.FormatNumber(totalBidders - 1)} other{(totalBidders - 1 == 1 ? "" : "s")} placed a bid on your auction.",
                        Category = FeedCategory.Auctions,
                        Href = $"/Auction?id={auction.Id}#{bidsByUsername.First().Key}",
                        CreatedDate = bidsByUsername.First().First().BidDate
                    });
                }
                if (auction.IsFinished())
                {
                    Feed feed = new Feed
                    {
                        Id = auction.Id,
                        Category = FeedCategory.Auctions,
                        Href = $"/Auction?id={auction.Id}",
                        CreatedDate = auction.EndDate
                    };
                    if (auction.Bids.Count > 0)
                    {
                        feed.Description = "Your auction has finished. You can collect";
                        feed.Object = $"{Helper.GetFormattedPoints(Math.Round(auction.HighestBid * 0.9))}";
                    }
                    else
                    {
                        feed.Description = "Your auction has finished. No one placed any bids.";
                    }
                    feeds.Add(feed);
                }
            }
            else
            {
                if (auction.IsFinished())
                {
                    if (auction.Bids.FirstOrDefault()?.Username == user.Username)
                    {
                        feeds.Add(new Feed
                        {
                            Id = auction.Id,
                            Description = "You won an auction! You can collect the item,",
                            Object = $"\"{auction.AuctionItem.Name}\"",
                            Category = FeedCategory.Auctions,
                            Href = $"/Auction?id={auction.Id}",
                            CreatedDate = auction.EndDate
                        });
                    }
                }
                else
                {
                    Bid topBid = auction.Bids.FirstOrDefault();
                    if (topBid != null && topBid.Username != user.Username)
                    {
                        feeds.Add(new Feed
                        {
                            Id = auction.Id,
                            Description = $"You were outbid in an auction. You have {auction.EndDate.GetTimeUntil()} left to make a new bid.",
                            Category = FeedCategory.Auctions,
                            Href = $"/Auction?id={auction.Id}#{topBid.Username}",
                            CreatedDate = topBid.BidDate
                        });
                    }
                }
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, Trade trade)
        {
            List<Feed> feeds = new List<Feed>();
            if (trade.Username == user.Username)
            {
                if (trade.Offers.Count > 0)
                {
                    IEnumerable<IGrouping<string, TradeOffer>> offersByUsername = trade.Offers.GroupBy(x => x.Username);
                    int totalOffers = offersByUsername.Count();
                    feeds.Add(new Feed
                    {
                        Id = trade.Id,
                        Username = offersByUsername.First().Key,
                        Description = totalOffers == 1 ? "put an offer on your trade posting." : $"and {Helper.FormatNumber(totalOffers - 1)} other{(totalOffers - 1 == 1 ? "" : "s")} put an offer on your trade posting.",
                        Category = FeedCategory.Trades,
                        Href = $"/Trading?id={trade.Id}#{offersByUsername.First().First().Username}",
                        CreatedDate = offersByUsername.First().First().CreatedDate
                    });
                }
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(JobSubmission jobSubmission)
        {
            List<Feed> feeds = new List<Feed>();
            if (jobSubmission.State != JobState.Accepted)
            {
                return feeds;
            }

            string jobType = jobSubmission.Position == JobPosition.Helper ? "Helper application" : $"job submission \"{jobSubmission.Title}\"";
            feeds.Add(new Feed
            {
                Guid = jobSubmission.Id,
                Description = $"Your {jobType} was accepted!",
                Category = FeedCategory.Jobs,
                Href = $"/Jobs?expand=true#jobSubmissions",
                CreatedDate = jobSubmission.UpdatedDate
            });

            return feeds;
        }

        public List<Feed> ConvertFrom(Group group, List<GroupRequest> groupRequests, bool showUnreadGroupMessages)
        {
            List<Feed> feeds = new List<Feed>();

            if (groupRequests != null)
            {
                foreach (GroupRequest groupRequest in groupRequests)
                {
                    feeds.Add(new Feed
                    {
                        Id = group.Id,
                        Username = groupRequest.Username,
                        Description = $"requested to join your group,",
                        Object = $"\"{group.Name}\"",
                        Category = FeedCategory.Groups,
                        Href = $"/Groups?id={groupRequest.GroupId}#requests",
                        CreatedDate = groupRequest.CreatedDate
                    });
                }
            }
            if (group.IsPrizeMember)
            {
                double timeSinceLastPrizeDate = (DateTime.UtcNow - group.LastPrizeDate).TotalDays;
                if (timeSinceLastPrizeDate >= 1 && timeSinceLastPrizeDate < 2)
                {
                    feeds.Add(new Feed
                    {
                        Id = group.Id,
                        Description = $"It's your turn to collect your group's daily prize! You have",
                        Object = $"1 day left",
                        Category = FeedCategory.Groups,
                        Href = $"/Groups?id={group.Id}#group-daily-prize",
                        CreatedDate = group.LastPrizeDate.AddDays(1)
                    });
                }
            }
            if (showUnreadGroupMessages && group.UnreadMessages > 0)
            {
                feeds.Add(new Feed
                {
                    Id = group.Id,
                    Description = $"You have {(group.UnreadMessages == 1 ? "an unread message" : "unread messages")} in your group,",
                    Object = $"\"{group.Name}\"",
                    Category = FeedCategory.Groups,
                    Href = $"/Groups/Messages?id={group.Id}",
                    CreatedDate = group.LastGroupMessageDate
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, Settings settings)
        {
            List<Feed> feeds = new List<Feed>();
            if (settings.DividendsCollectDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can collect your daily dividends.",
                    Category = FeedCategory.Stocks,
                    Href = $"/StockMarket/Portfolio#earnings-container",
                    CreatedDate = settings.DividendsCollectDate.AddDays(1)
                });
            }
            if (settings.BankInterestDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can collect your daily bank interest.",
                    Category = FeedCategory.Bank,
                    Href = $"/Bank#bank-daily-interest",
                    CreatedDate = settings.BankInterestDate.AddDays(1)
                });
            }
            if (settings.LastWizardDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Young Wizard's powers have recharged.",
                    Category = FeedCategory.YoungWizard,
                    Href = $"/Wizard#wizard-wands",
                    CreatedDate = settings.LastWizardDate.AddHours(3)
                });
            }
            if (settings.LastWishDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can make another wish at the Wishing Stone.",
                    Category = FeedCategory.WishingStone,
                    Href = $"/WishingStone",
                    CreatedDate = settings.LastWishDate.AddHours(2)
                });
            }
            if (settings.LastLottoEntry > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can try your luck at winning the pot.",
                    Category = FeedCategory.Lotto,
                    Href = $"/Lotto#lotto-enter-form",
                    CreatedDate = settings.LastLottoEntry.AddHours(3)
                });
            }
            if (settings.LastStreakCardsPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Streak Cards again.",
                    Category = FeedCategory.StreakCards,
                    Href = $"/StreakCards#streak-cards-streak",
                    CreatedDate = settings.LastStreakCardsPlayDate.AddHours(8)
                });
            }
            if (settings.LastDailyStreakCardsPlayDate > DateTime.MinValue)
            {
                if (DateTime.UtcNow < settings.LastDailyStreakCardsPlayDate.AddHours(48) && settings.StreakCardsNoExpiryDate <= DateTime.UtcNow)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"Your Streak Cards streak will expire in",
                        Object = "3 hours",
                        Category = FeedCategory.StreakCards,
                        Href = $"/StreakCards#streak-cards-streak",
                        CreatedDate = settings.LastDailyStreakCardsPlayDate.AddHours(45)
                    });
                }
                else if (settings.StreakCardsNoExpiryDate <= DateTime.UtcNow)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"Your Streak Cards streak expired.",
                        Category = FeedCategory.StreakCards,
                        Href = $"/StreakCards#streak-cards-streak",
                        CreatedDate = settings.LastDailyStreakCardsPlayDate.AddHours(48)
                    });
                }
            }
            if (settings.StadiumChallengeExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"You have a new Stadium Challenge! It will expire in",
                    Object = "2 days",
                    Category = FeedCategory.Stadium,
                    ImageUrl = "CPU/RandomChallenger.png",
                    Href = $"/Stadium/Challenge",
                    CreatedDate = settings.StadiumChallengeExpiryDate.AddDays(-2)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your active Stadium Challenge will expire in",
                    Object = "1 day",
                    Category = FeedCategory.Stadium,
                    ImageUrl = "CPU/RandomChallenger.png",
                    Href = $"/Stadium/Challenge",
                    CreatedDate = settings.StadiumChallengeExpiryDate.AddDays(-1)
                });
            }
            if (settings.LastSwapDeckDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can swap your daily {Resources.DeckCard}.",
                    Category = FeedCategory.SwapDeck,
                    Href = $"/SwapDeck#swap-deck-label",
                    CreatedDate = settings.LastSwapDeckDate.AddDays(1)
                });
            }
            if (settings.LastLibraryKnowledgeUseDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can use your knowledge again at the Library.",
                    Category = FeedCategory.Library,
                    Href = $"/Library#library-knowledge-levels",
                    CreatedDate = settings.LastLibraryKnowledgeUseDate.AddHours(12)
                });
            }
            if (settings.SeekerChallengeExpiryDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your active Seeker Challenge will expire in",
                    Object = "1 day",
                    Category = FeedCategory.Seeker,
                    Href = $"/Seeker",
                    CreatedDate = settings.SeekerChallengeExpiryDate.AddDays(-1)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your active Seeker Challenge expired.",
                    Category = FeedCategory.Seeker,
                    Href = $"/Seeker",
                    CreatedDate = settings.SeekerChallengeExpiryDate
                });
            }
            if (settings.LastClubBonusDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can get your daily Stadium Bonus from the {Resources.SiteName} Club.",
                    Category = FeedCategory.Club,
                    Href = $"/Club#daily-bonus",
                    CreatedDate = settings.LastClubBonusDate.AddDays(1)
                });
            }
            if (settings.IncubationStartDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"A copy of your incubated Enhancer has been created.",
                    Category = FeedCategory.Club,
                    Href = $"/Club/Incubator",
                    CreatedDate = settings.IncubationStartDate.AddDays(3)
                });
                feeds.Add(new Feed
                {
                    Description = $"A copy of your incubated Enhancer now has all its uses.",
                    Category = FeedCategory.Club,
                    Href = $"/Club/Incubator",
                    CreatedDate = settings.IncubationStartDate.AddDays(5)
                });
            }
            if (settings.MerchantsExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Traveling Merchants have arrived! They will leave in",
                    Object = "6 hours",
                    Category = FeedCategory.Merchants,
                    Href = $"/Merchants",
                    CreatedDate = settings.MerchantsExpiryDate.AddHours(-6)
                });
                if (settings.MerchantsContestEnteredDate > settings.MerchantsExpiryDate.AddHours(-6))
                {
                    feeds.Add(new Feed
                    {
                        Description = $"You can enter the Traveling Merchants' contest again.",
                        Category = FeedCategory.Merchants,
                        Href = $"/Merchants",
                        CreatedDate = settings.MerchantsContestEnteredDate.AddHours(3)
                    });
                }
            }
            if (settings.TimedShowcasePurchaseDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can buy another Timed Showcase item.",
                    Category = FeedCategory.Showcase,
                    Href = $"/Showcase",
                    CreatedDate = settings.TimedShowcasePurchaseDate.AddHours(6)
                });
            }
            if (settings.RewardsStoreExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Rewards Store is open! It will close in",
                    Object = "6 hours",
                    Category = FeedCategory.RewardsStore,
                    Href = $"/RewardsStore",
                    CreatedDate = settings.RewardsStoreExpiryDate.AddHours(-6)
                });
                feeds.Add(new Feed
                {
                    Description = $"The Rewards Store will close in",
                    Object = "1 hour",
                    Category = FeedCategory.RewardsStore,
                    Href = $"/RewardsStore",
                    CreatedDate = settings.RewardsStoreExpiryDate.AddHours(-1)
                });
            }
            if (settings.NoticeBoardExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Notice Board is open! Your new task will expire in",
                    Object = "3 days",
                    Category = FeedCategory.NoticeBoard,
                    Href = $"/NoticeBoard",
                    CreatedDate = settings.NoticeBoardExpiryDate.AddDays(-3)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your Notice Board task will expire in",
                    Object = "12 hours",
                    Category = FeedCategory.NoticeBoard,
                    Href = $"/NoticeBoard",
                    CreatedDate = settings.NoticeBoardExpiryDate.AddHours(-12)
                });
            }
            if (settings.StealthProtectExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your Stealth Protect will expire in",
                    Object = "1 day",
                    Category = FeedCategory.Effects,
                    Href = $"/Effects",
                    CreatedDate = settings.StealthProtectExpiryDate.AddDays(-1)
                });
            }
            if (settings.FurnaceDefrostDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Furnace",
                    Description = $"Your frozen item has been defrosted at the Furnace.",
                    Category = FeedCategory.Crafting,
                    Href = $"/Crafting/Furnace",
                    CreatedDate = settings.FurnaceDefrostDate
                });
            }
            if (settings.HoroscopeExpiryDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can get your new daily horoscope.",
                    Category = FeedCategory.News,
                    Href = $"/News/Horoscope",
                    CreatedDate = settings.HoroscopeExpiryDate
                });
            }
            if (settings.WarehouseExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Warehouse is open! It will close in",
                    Object = "6 hours",
                    Category = FeedCategory.Warehouse,
                    Href = $"/Warehouse",
                    CreatedDate = settings.WarehouseExpiryDate.AddHours(-6)
                });
                feeds.Add(new Feed
                {
                    Description = $"The Warehouse will close in",
                    Object = "1 hour",
                    Category = FeedCategory.Warehouse,
                    Href = $"/Warehouse",
                    CreatedDate = settings.WarehouseExpiryDate.AddHours(-1)
                });
            }
            if (settings.DeepCavernsExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your light to explore the Deep Caverns will fade out in",
                    Object = "12 hours",
                    Category = FeedCategory.DeepCaverns,
                    Href = $"/Caverns",
                    CreatedDate = settings.DeepCavernsExpiryDate.AddHours(-12)
                });
                if (settings.LastSorceressEffectDate > settings.DeepCavernsExpiryDate.AddDays(-3))
                {
                    feeds.Add(new Feed
                    {
                        Description = $"You can visit the Sorceress in the Deep Caverns again.",
                        Category = FeedCategory.DeepCaverns,
                        Href = $"/Caverns",
                        CreatedDate = settings.LastSorceressEffectDate.AddHours(2)
                    });
                }
                if (settings.SeedCardTakeDate > DateTime.MinValue)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"Your planted Seed Card has blossomed into a new {Resources.DeckCard}.",
                        Category = FeedCategory.DeepCaverns,
                        Href = $"/Caverns/SeedCards",
                        CreatedDate = settings.SeedCardTakeDate
                    });
                }
            }
            if (settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your Curse Protection will expire in",
                    Object = "1 day",
                    Category = FeedCategory.Effects,
                    Href = $"/Effects",
                    CreatedDate = settings.SorceressStadiumCurseProtectExpiryDate.AddDays(-1)
                });
            }
            if (settings.RightSideUpWorldExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The portal to the Right-Side Up World will close in",
                    Object = "6 hours",
                    Category = FeedCategory.RightSideUpWorld,
                    Href = $"/RightSideUpWorld",
                    CreatedDate = settings.RightSideUpWorldExpiryDate.AddHours(-6)
                });
            }
            if (settings.LastQuarryStoneMineDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Wishing Stone Quarry",
                    Description = $"You can mine the Wishing Stone Quarry again.",
                    Category = FeedCategory.WishingStone,
                    Href = $"/WishingStone/Quarry#quarry-current-haul",
                    CreatedDate = settings.LastQuarryStoneMineDate.AddMinutes(10)
                });
            }
            if (settings.DonationsTakeDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can take another donation from the Donation Zone.",
                    Category = FeedCategory.DonationZone,
                    Href = $"/Donations",
                    CreatedDate = settings.DonationsTakeDate.AddMinutes(60)
                });
            }
            if (settings.SecretGifterExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can be someone's Secret Gifter! You have 1 day to send someone a gift.",
                    Category = FeedCategory.SecretGifter,
                    Href = $"/SecretGifter",
                    CreatedDate = settings.SecretGifterExpiryDate.AddDays(-1)
                });
                feeds.Add(new Feed
                {
                    Description = $"You have 6 hours left to send someone a Secret Gift.",
                    Category = FeedCategory.SecretGifter,
                    Href = $"/SecretGifter",
                    CreatedDate = settings.SecretGifterExpiryDate.AddHours(-6)
                });
            }
            if (settings.SurveyExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"You have 3 days to complete a new survey.",
                    Category = FeedCategory.Survey,
                    Href = $"/Survey",
                    CreatedDate = settings.SurveyExpiryDate.AddDays(-3)
                });
                feeds.Add(new Feed
                {
                    Description = $"You have 12 hours left to complete a survey.",
                    Category = FeedCategory.Survey,
                    Href = $"/Survey",
                    CreatedDate = settings.SurveyExpiryDate.AddHours(-12)
                });
            }
            if (settings.MuseumExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Museum is open! It will close in",
                    Object = "24 hours",
                    Category = FeedCategory.Museum,
                    Href = $"/Museum",
                    CreatedDate = settings.MuseumExpiryDate.AddDays(-1)
                });
                feeds.Add(new Feed
                {
                    Description = $"The Museum will close in",
                    Object = "6 hours",
                    Category = FeedCategory.Museum,
                    Href = $"/Museum",
                    CreatedDate = settings.MuseumExpiryDate.AddHours(-6)
                });
                if (settings.LastBoutiquePurchaseDate > DateTime.MinValue)
                {
                    feeds.Add(new Feed
                    {
                        Title = "Boutique",
                        Description = $"You can buy another item from the Museum Boutique.",
                        Category = FeedCategory.Museum,
                        Href = $"/Museum/Boutique",
                        CreatedDate = settings.LastBoutiquePurchaseDate.AddHours(1)
                    });
                }
            }
            if (settings.LastSecretShrineItemGivenDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can give the Secret Shrine another item.",
                    Category = FeedCategory.SecretShrine,
                    Href = $"/SecretShrine",
                    CreatedDate = settings.LastSecretShrineItemGivenDate.AddDays(1)
                });
            }
            if (settings.LastMazeEntryDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Maze",
                    Description = $"You can enter the Maze again.",
                    Category = FeedCategory.SwapDeck,
                    Href = $"/SwapDeck/Maze#maze-start",
                    CreatedDate = settings.LastMazeEntryDate.AddDays(1)
                });
            }
            if (settings.LastMatchingColorsPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Matching Colors again.",
                    Category = FeedCategory.MatchingColors,
                    Href = $"/MatchingColors#matching-cards-seen",
                    CreatedDate = settings.LastMatchingColorsPlayDate.AddHours(8)
                });
            }
            if (settings.LastColorWarsContributeDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can make another contribution to your team.",
                    Category = FeedCategory.ColorWars,
                    Href = $"/ColorWars#team-points",
                    CreatedDate = settings.LastColorWarsContributeDate.AddHours(3)
                });
            }
            if (settings.LastSpinSuccessDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can spin the wheel again.",
                    Category = FeedCategory.SpinSuccess,
                    Href = $"/SpinSuccess#wheel",
                    CreatedDate = settings.LastSpinSuccessDate.AddHours(3)
                });
            }
            if (settings.LastTakeOfferPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Take the Offer again.",
                    Category = FeedCategory.TakeOffer,
                    Href = $"/TakeOffer#cards",
                    CreatedDate = settings.LastTakeOfferPlayDate.AddHours(12)
                });
            }
            if (settings.LastCrystalBallPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can visit the Crystal Ball again.",
                    Category = FeedCategory.CrystalBall,
                    Href = $"/CrystalBall#current-streak",
                    CreatedDate = settings.LastCrystalBallPlayDate.AddHours(6)
                });
            }
            if (settings.LastPrizeCupPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Prize Cup again.",
                    Category = FeedCategory.PrizeCup,
                    Href = $"/PrizeCup#total-prize-cups",
                    CreatedDate = settings.LastPrizeCupPlayDate.AddHours(5)
                });
            }
            if (settings.LastTreasureDivePlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can search for hidden treasure again.",
                    Category = FeedCategory.TreasureDive,
                    Href = $"/TreasureDive#map",
                    CreatedDate = settings.LastTreasureDivePlayDate.AddHours(6)
                });
            }
            if (settings.LastShowroomAssessmentDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Showroom Store",
                    Description = $"You can have your Showroom assessed again.",
                    Category = FeedCategory.BuyersClub,
                    Href = $"/BuyersClub/Showroom",
                    CreatedDate = settings.LastShowroomAssessmentDate.AddDays(2)
                });
            }
            if (settings.PurchaseMoreStocksDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can buy more stocks again.",
                    Category = FeedCategory.Stocks,
                    Href = $"/StockMarket",
                    CreatedDate = settings.PurchaseMoreStocksDate
                });
            }
            if (settings.ThemeCardSqueezeDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Theme Squeezer",
                    Description = $"Your can now retrieve your extracted Theme Card,",
                    Object = $"{settings.ThemeSqueezeType} Theme",
                    Category = FeedCategory.WearyVillager,
                    Href = $"/Villager/ThemeSqueezer",
                    CreatedDate = settings.ThemeCardSqueezeDate
                });
            }
            if (settings.LastWealthyDonorsTakeDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You take from the Wealthy Donors' pot again.",
                    Category = FeedCategory.WealthyDonors,
                    Href = $"/WealthyDonors#pot",
                    CreatedDate = settings.LastWealthyDonorsTakeDate.AddHours(8)
                });
            }
            if (settings.LastLottoBuyDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can buy Egg Cards from the Lotto again.",
                    Category = FeedCategory.Lotto,
                    Href = $"/Lotto/Egg",
                    CreatedDate = settings.LastLottoBuyDate.AddHours(24)
                });
            }
            if (settings.LastTopCounterPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Top Counter again.",
                    Category = FeedCategory.TopCounter,
                    Href = $"/TopCounter#value",
                    CreatedDate = settings.LastTopCounterPlayDate.AddHours(1)
                });
            }
            if (settings.LastChallengerDomeMatchDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can practice at the Challenger Dome again.",
                    Category = FeedCategory.ChallengerDome,
                    Href = $"/ChallengerDome",
                    CreatedDate = settings.LastChallengerDomeMatchDate.AddMinutes(15)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your Challenger Dome streak is about to expire.",
                    Category = FeedCategory.ChallengerDome,
                    Href = $"/ChallengerDome",
                    CreatedDate = settings.LastChallengerDomeMatchDate.AddHours(45)
                });
            }
            if (settings.StoreMagicianExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"The Store Magician is open! It will close in",
                    Object = "30 days",
                    Category = FeedCategory.StoreMagician,
                    Href = $"/StoreMagician",
                    CreatedDate = settings.StoreMagicianExpiryDate.AddDays(-30)
                });
                feeds.Add(new Feed
                {
                    Description = $"The Store Magician will close in 1 day. You can extend it for another",
                    Object = "30 days",
                    Category = FeedCategory.SponsorSociety,
                    Href = $"/SponsorSociety/CoinCatalog#{SponsorPerkId.StoreMagician}",
                    CreatedDate = settings.StoreMagicianExpiryDate.AddDays(-1)
                });
            }
            if (user.AdFreeExpiryDate > DateTime.UtcNow)
            {
                feeds.Add(new Feed
                {
                    Description = $"Thanks for your support! You are now ad free for the next",
                    Object = "30 days",
                    Category = FeedCategory.SponsorSociety,
                    Href = $"/SponsorSociety",
                    CreatedDate = user.AdFreeExpiryDate.AddDays(-30)
                });
                feeds.Add(new Feed
                {
                    Description = $"You'll start seeing ads again in 1 day. You can remove ads for another",
                    Object = "30 days",
                    Category = FeedCategory.SponsorSociety,
                    Href = $"/SponsorSociety/CoinCatalog#{SponsorPerkId.RemoveAds}",
                    CreatedDate = user.AdFreeExpiryDate.AddDays(-1)
                });
            }
            if (settings.CellarStoreLastPurchaseDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Title = "Cellar",
                    Description = $"You can buy another item from the Cellar Store again.",
                    Category = FeedCategory.TopAdoptions,
                    Href = $"/TopAdoptions/Cellar",
                    CreatedDate = settings.CellarStoreLastPurchaseDate.AddHours(6)
                });
            }
            if (settings.LuckyGuessLastPlayDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can play Lucky Guess again.",
                    Category = FeedCategory.LuckyGuess,
                    Href = $"/LuckyGuess#available-chests",
                    CreatedDate = settings.LuckyGuessLastPlayDate.AddHours(6)
                });
            }
            if (settings.LastClubhouseBuyDate > DateTime.MinValue)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can buy another Art Card from the Clubhouse again.",
                    Category = FeedCategory.Clubhouse,
                    Href = $"/Clubhouse#art-card",
                    CreatedDate = settings.LastClubhouseBuyDate.AddDays(1)
                });
            }
            if (!settings.VerifiedEmail && user.GetAgeInDays() % 2 == 1)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your email address isn't verified. Please verify it or resend your verification email.",
                    Category = FeedCategory.All,
                    Href = $"/Preferences#settingsForm",
                    CreatedDate = DateTime.UtcNow.Date.AddHours(12)
                });
            }
            int totalHoursSinceDailyBonus = (int)(DateTime.UtcNow - settings.LastLoginBonusDate).TotalHours;
            if (totalHoursSinceDailyBonus >= 24)
            {
                string dailyBonusDescription = $"Claim your daily bonus and earn your reward for today!";
                if (totalHoursSinceDailyBonus >= 43 && totalHoursSinceDailyBonus < 48 && settings.LoginBonusStreak > 0)
                {
                    dailyBonusDescription = $"Your streak is about to expire! Claim your daily bonus and earn your reward for today!";
                }
                feeds.Add(new Feed
                {
                    Description = dailyBonusDescription,
                    Category = FeedCategory.DailyBonus,
                    OnClick = "showLoginBonusModal();",
                    CreatedDate = settings.LastLoginBonusDate == DateTime.MinValue ? user.GetAgeInDays() < 1 ? user.CreatedDate.AddMinutes(30) : DateTime.UtcNow.Date : new DateTime(Math.Max(DateTime.UtcNow.Date.Ticks, settings.LastLoginBonusDate.AddDays(1).Ticks))
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(Item item)
        {
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Guid = item.SelectionId,
                    Description = $"Your {item.Category.GetDisplayName()} \"{item.Name}\" no longer has a New condition and should be repaired soon.",
                    Category = FeedCategory.RepairShop,
                    Href = $"/Repair#{item.SelectionId}",
                    CreatedDate = item.RepairedDate.AddDays(30)
                },
                new Feed
                {
                    Guid = item.SelectionId,
                    Description = $"Your {item.Category.GetDisplayName()} \"{item.Name}\" has a Poor condition and should be repaired.",
                    Category = FeedCategory.RepairShop,
                    Href = $"/Repair#{item.SelectionId}",
                    CreatedDate = item.RepairedDate.AddDays(60)
                },
                new Feed
                {
                    Guid = item.SelectionId,
                    Description = $"Your {item.Category.GetDisplayName()} \"{item.Name}\" has an Unusable condition and needs to be repaired.",
                    Category = FeedCategory.RepairShop,
                    Href = $"/Repair#{item.SelectionId}",
                    CreatedDate = item.RepairedDate.AddDays(90)
                }
            };

            return feeds;
        }

        public List<Feed> ConvertFrom(List<MarketStallHistory> marketStallHistories, List<MarketStallItem> marketStallItems)
        {
            List<Feed> feeds = new List<Feed>();
            foreach (MarketStallHistory marketStallHistory in marketStallHistories)
            {
                feeds.Add(new Feed
                {
                    Guid = marketStallHistory.Id,
                    Description = $"Your item \"{marketStallHistory.ItemName}\" was bought from your Market Stall for ",
                    Object = $"{Helper.GetFormattedPoints(marketStallHistory.Cost)}",
                    Category = FeedCategory.MarketStalls,
                    Href = $"/MarketStalls?id={marketStallHistory.MarketStallId}&openLedger=true#marketStallLedgers",
                    CreatedDate = marketStallHistory.CreatedDate
                });
            }

            if (marketStallItems.Count == 0 && marketStallHistories.Count > 0)
            {
                feeds.Add(new Feed
                {
                    Description = $"You have no more items in your Market Stall. You should add some more!",
                    Category = FeedCategory.MarketStalls,
                    Href = $"/MarketStalls?id={marketStallHistories.First().MarketStallId}",
                    CreatedDate = marketStallHistories.First().CreatedDate.AddSeconds(1)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(IGrouping<int, AttentionHallComment> attentionHallComment)
        {
            int totalUniqueComments = attentionHallComment.GroupBy(x => x.Username).Count();
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Id = attentionHallComment.Key,
                    Username = attentionHallComment.First().Username,
                    Description = totalUniqueComments == 1 ? "commented on your Attention Hall post." : $"and {Helper.FormatNumber(totalUniqueComments - 1)} other{(totalUniqueComments - 1 == 1 ? "" : "s")} commented on your Attention Hall post.",
                    Category = FeedCategory.AttentionHall,
                    Href = $"/AttentionHall?id={attentionHallComment.Key}#{attentionHallComment.First().Id}",
                    CreatedDate = attentionHallComment.First().CreatedDate
                }
            };

            return feeds;
        }

        public List<Feed> ConvertFrom(AttentionHallPost attentionHallPost)
        {
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Id = attentionHallPost.Id,
                    Description = $"The Attention Hall post \"{attentionHallPost.Title}\" is currently trending.",
                    Category = FeedCategory.AttentionHall,
                    Href = $"/AttentionHall?id={attentionHallPost.Id}",
                    CreatedDate = attentionHallPost.CreatedDate
                }
            };

            return feeds;
        }

        public List<Feed> ConvertFrom(ClubMembership clubMembership)
        {
            List<Feed> feeds = new List<Feed>();
            if (!clubMembership.ClaimedItem)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can claim your monthly {Resources.SiteName} Club item!",
                    Category = FeedCategory.Club,
                    Href = $"/Club#unclaimed-monthly-item",
                    CreatedDate = clubMembership.StartDate
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(FeedCategory category, DateTime dateTime)
        {
            List<Feed> feeds = new List<Feed>();
            if (category == FeedCategory.Club)
            {
                feeds.Add(new Feed
                {
                    Description = $"Your {Resources.SiteName} Club membership will expire in",
                    Object = "1 week",
                    Category = FeedCategory.Club,
                    Href = $"/Club#extend-club-membership",
                    CreatedDate = dateTime.AddDays(-7)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your {Resources.SiteName} Club membership will expire in",
                    Object = "1 day",
                    Category = FeedCategory.Club,
                    Href = $"/Club#extend-club-membership",
                    CreatedDate = dateTime.AddDays(-1)
                });
                feeds.Add(new Feed
                {
                    Description = $"Your {Resources.SiteName} Club membership expired.",
                    Category = FeedCategory.Club,
                    Href = $"/Club",
                    CreatedDate = dateTime
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(ItemCategory itemCategory)
        {
            List<Feed> feeds = new List<Feed>();
            if (itemCategory > ItemCategory.All)
            {
                feeds.Add(new Feed
                {
                    Description = $"The General Store has a limited time discount on",
                    Object = $"{itemCategory.GetDisplayName()}s",
                    Category = FeedCategory.Store,
                    Href = $"/Store",
                    CreatedDate = DateTime.UtcNow.Date
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, Companion companion)
        {
            List<Feed> feeds = new List<Feed>();
            if (companion.CanGiveGift(user))
            {
                feeds.Add(new Feed
                {
                    Description = $"{companion.Name} has something to give you.",
                    Category = FeedCategory.Companions,
                    Href = $"/Profile#companion-{companion.Id}",
                    ImageUrl = companion.GetImageUrl(),
                    CreatedDate = companion.LastGiftGivenDate.AddDays(1)
                });
            }
            else if (companion.GetHappiness() <= 40)
            {
                feeds.Add(new Feed
                {
                    Description = $"{companion.Name} really misses you and needs some attention.",
                    Category = FeedCategory.Companions,
                    Href = $"/Profile#companion-{companion.Id}",
                    ImageUrl = companion.GetImageUrl(),
                    CreatedDate = companion.HappinessDate.AddHours(240)
                });
            }
            else if (companion.GetHappiness() <= 60)
            {
                feeds.Add(new Feed
                {
                    Description = $"{companion.Name} misses you and needs some attention.",
                    Category = FeedCategory.Companions,
                    Href = $"/Profile#companion-{companion.Id}",
                    ImageUrl = companion.GetImageUrl(),
                    CreatedDate = companion.HappinessDate.AddHours(160)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(PartnershipRequest partnershipRequest)
        {
            return new List<Feed>
            {
                new Feed
                {
                    Username = partnershipRequest.Username,
                    Description = $"requested to form a partnership with you.",
                    Category = FeedCategory.Partnership,
                    Href = $"/Profile#partnership-requests",
                    CreatedDate = partnershipRequest.CreatedDate
                }
            };
        }

        public List<Feed> ConvertFrom(PartnershipEarning partnershipEarning)
        {
            return new List<Feed>
            {
                new Feed
                {
                    Username = partnershipEarning.Username,
                    Description = $"earned you {Helper.GetFormattedPoints(partnershipEarning.Amount)} from your partnership.",
                    Category = FeedCategory.Partnership,
                    Href = $"/Profile/Partnership",
                    CreatedDate = partnershipEarning.CreatedDate
                }
            };
        }

        public List<Feed> ConvertFrom(List<PowerMove> powerMoves)
        {
            List<Feed> feeds = new List<Feed>();
            foreach (PowerMove powerMove in powerMoves)
            {
                if (powerMove.CreatedDate == DateTime.MinValue || powerMove.MinCPULevel == 0)
                {
                    continue;
                }
                feeds.Add(new Feed
                {
                    Title = "Power Moves",
                    Description = $"You {(powerMove.MinCPULevel >= 0 ? $"defeated a {(powerMove.MinCPULevel % 5 == 0 ? "Checkpoint CPU" : "CPU")} at the Stadium and" : "")} unlocked the <b>{powerMove.Name} Power Move</b>. Set it in your bag.",
                    Category = FeedCategory.Stadium,
                    ImageUrl = powerMove.ImageUrl,
                    Href = $"/Items?showPowerMoves=true#{powerMove.Type}",
                    CreatedDate = powerMove.CreatedDate.AddSeconds(1)
                });
                if (powerMove.MinCPULevel == 2)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"You can now create and join tourneys!",
                        Category = FeedCategory.Tourney,
                        Href = $"/Tourney",
                        CreatedDate = powerMove.CreatedDate.AddSeconds(2)
                    });
                }
                if (powerMove.MinCPULevel == 8)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"A strange door has appeared at the Plaza!",
                        Category = FeedCategory.PhantomDoor,
                        Href = $"/PhantomDoor",
                        CreatedDate = powerMove.CreatedDate.AddSeconds(2)
                    });
                }
            }
            return feeds;
        }

        public List<Feed> ConvertFrom(User user, MatchHistory matchHistory, Settings settings, List<Accomplishment> accomplishments)
        {
            List<Feed> feeds = new List<Feed>();
            CPU cpu = _cpuRepository.GetCPU(settings.MatchLevel);
            if (matchHistory.Result == MatchHistoryResult.Won)
            {
                if (cpu.IsCheckpoint)
                {
                    if (_cpuRepository.CanChallengeCheckpointCPU(user, accomplishments, cpu, out DateTime date, out _, out _, out _))
                    {
                        feeds.Add(new Feed
                        {
                            Title = "Stadium",
                            Description = $"You're now able to challenge {(cpu.Level + 1 == 5 ? "your first" : "the next")} Checkpoint CPU,",
                            Object = cpu.Name,
                            Category = FeedCategory.Stadium,
                            Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                            ImageUrl = "Stadium/Checkpoint.png",
                            CreatedDate = new DateTime(Math.Max(date.Ticks, matchHistory.CreatedDate.Ticks))
                        });
                    }
                    else if (cpu.Level + 1 == 5)
                    {
                        feeds.Add(new Feed
                        {
                            Description = $"Looking for a way to challenge your first Checkpoint CPU? Complete 3 games at the",
                            Object = "Fair",
                            Category = FeedCategory.Fair,
                            Href = $"/Fair",
                            CreatedDate = matchHistory.CreatedDate
                        });
                    }
                }
            }

            if (!cpu.IsCheckpoint || _cpuRepository.CanChallengeCheckpointCPU(user, accomplishments, cpu, out _, out _, out _, out _))
            {
                int hours = cpu.Level < 15 && cpu.Intensity == 0 ? 4 : cpu.Level < 25 && cpu.Intensity == 0 ? 8 : cpu.Intensity <= 1 ? 24 : 36;
                int totalHours = (int)(DateTime.UtcNow - matchHistory.CreatedDate).TotalHours;
                int hoursToAdd = Math.Max(hours, totalHours - (totalHours % hours));
                feeds.Add(new Feed
                {
                    Description = $"Defeat {(matchHistory.Result == MatchHistoryResult.Won ? "the next" : "the")} {(cpu.IsCheckpoint ? "Checkpoint CPU" : "CPU Challenger")}, <b>{cpu.Name}</b>, and earn",
                    Object = Helper.GetFormattedPoints(cpu.Reward),
                    Category = FeedCategory.Stadium,
                    ImageUrl = cpu.ImageUrl,
                    Href = $"/Stadium?stadiumTab={StadiumTab.CPU}#challenge-tabs",
                    CreatedDate = matchHistory.CreatedDate.AddHours(hoursToAdd)
                });
            }

            if (settings.TrainingTokens > 0)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can improve your {Resources.DeckCards} using your",
                    Object = $"{(settings.TrainingTokens == 1 ? "Training Token" : $"{Helper.FormatNumber(settings.TrainingTokens)} Training Tokens")}",
                    Category = FeedCategory.TrainingCenter,
                    Href = $"/TrainingCenter#training-tokens",
                    CreatedDate = matchHistory.CreatedDate.AddHours(3)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(MatchTag matchTag)
        {
            return new List<Feed>
            {
                new Feed
                {
                    Description = $"You earned a new Match Tag by defeating @{matchTag.TagUsername} at the Stadium.",
                    Category = FeedCategory.MatchTags,
                    Href = $"/Profile#matchtag-{matchTag.TagUsername}",
                    CreatedDate = matchTag.FirstWinDate
                }
            };
        }

        public List<Feed> ConvertFrom(User user, List<MatchWaitlist> matchWaitlists)
        {
            List<Feed> feeds = new List<Feed>();
            if (matchWaitlists.Count(x => string.IsNullOrEmpty(x.ChallengeUser)) > 0)
            {
                feeds.Add(new Feed
                {
                    Description = $"There's {(matchWaitlists.Count(x => x.Username != user.Username) == 1 ? "someone" : $"{Helper.FormatNumberShortened(matchWaitlists.Count(x => x.Username != user.Username))} users")} waiting in the lobby for someone to challenge at the Stadium.",
                    Category = FeedCategory.StadiumLobby,
                    Href = $"/Stadium?stadiumTab={StadiumTab.User}#challenge-tabs",
                    CreatedDate = matchWaitlists.Find(x => x.Username != user.Username).CreatedDate
                });
            }
            if (matchWaitlists.Any(x => x.ChallengeUser == user.Username))
            {
                MatchWaitlist challengeWaitlist = matchWaitlists.Find(x => x.ChallengeUser == user.Username);
                feeds.Add(new Feed
                {
                    Username = challengeWaitlist.Username,
                    Description = "is waiting to challenge you to a friendly match at the Stadium",
                    Category = FeedCategory.StadiumLobby,
                    Href = $"/Stadium/ChallengeUser?username={challengeWaitlist.Username}",
                    CreatedDate = challengeWaitlist.CreatedDate
                });
            }
            return feeds;
        }

        public List<Feed> ConvertFrom(List<Stock> stocks)
        {
            List<Feed> feeds = new List<Feed>();
            foreach (Stock stock in stocks)
            {
                feeds.Add(new Feed
                {
                    Description = $"You can now sell your {Helper.FormatNumber(stock.UserAmount)} share{(stock.UserAmount == 1 ? "" : "s")} of {stock.Name} without being taxed.",
                    Category = FeedCategory.Stocks,
                    Href = $"/StockMarket/Portfolio",
                    CreatedDate = stock.PurchaseDate.AddDays(30)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(List<Donor> donors, AdminSettings adminSettings)
        {
            double weeklyPercent = donors.Sum(x => (double)x.Amount / 100);
            string percentText = string.Empty;
            if (weeklyPercent < 100)
            {
                percentText = $" We're at {Math.Round(weeklyPercent/adminSettings.TotalSponsorAmount * 100, 1)}% of the weekly goal!";
            }
            IEnumerable<IGrouping<string, Donor>> donorsByUsername = donors.GroupBy(x => x.Username);
            List <Feed> feeds = new List<Feed>();
            foreach (IGrouping<string, Donor> donorGrouping in donorsByUsername)
            {
                Donor donor = donorGrouping.First();
                feeds.Add(new Feed
                {
                    Username = donor.Username,
                    Description = $"bought Sponsor Coins and helped support {Resources.SiteName}!{percentText}",
                    Category = FeedCategory.SponsorSociety,
                    Href = $"/SponsorSociety",
                    CreatedDate = donor.CreatedDate
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(List<PromoCode> promoCodes)
        {
            List<Feed> feeds = new List<Feed>();
            foreach (PromoCode promoCode in promoCodes)
            {
                feeds.Add(new Feed
                {
                    Username = promoCode.Creator,
                    Description = $"used Sponsor Coins to get you a free Promo Code! Redeem it within the next",
                    Object  = "24 hours",
                    Category = FeedCategory.SponsorSociety,
                    Href = $"/SponsorSociety/CoinCatalog#sponsor-codes",
                    CreatedDate = promoCode.ExpiryDate.AddDays(-1)
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, List<Tourney> tourneys)
        {
            List<Feed> feeds = new List<Feed>();
            foreach (Tourney tourney in tourneys)
            {
                if ((DateTime.UtcNow - tourney.CreatedDate).TotalHours >= 72)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"The tourney \"{tourney.Name}\" will expire in 24 hours. Encourage everyone to finish their matches soon or it will be cancelled.",
                        Category = FeedCategory.Tourney,
                        Href = $"/Tourney?id={tourney.Id}",
                        CreatedDate = tourney.CreatedDate.AddHours(72)
                    });
                }
            }
            foreach (Tourney tourney in tourneys.FindAll(x => x.GetStatus() == TourneyStatus.InProgress))
            {
                TourneyMatch mostRecentTourneyMatch = tourney.TourneyMatches.FindAll(x => x.Username1 == user.Username || x.Username2 == user.Username).LastOrDefault();
                if (mostRecentTourneyMatch != null && !string.IsNullOrEmpty(mostRecentTourneyMatch.Username1) && !string.IsNullOrEmpty(mostRecentTourneyMatch.Username2) && string.IsNullOrEmpty(mostRecentTourneyMatch.GetWinner()))
                {
                    int hours = 8;
                    int totalHours = (int)(DateTime.UtcNow - mostRecentTourneyMatch.ModifiedDate).TotalHours;
                    int hoursToAdd = totalHours - (totalHours % hours);
                    feeds.Add(new Feed
                    {
                        Username = mostRecentTourneyMatch.Username1 == user.Username ? mostRecentTourneyMatch.Username2 : mostRecentTourneyMatch.Username1,
                        Description = $"is your {(mostRecentTourneyMatch.Round == 0 ? "first" : "next")} opponent in the \"{tourney.Name}\" tourney! Challenge them as soon as possible.",
                        Category = FeedCategory.Tourney,
                        Href = $"/Tourney?id={mostRecentTourneyMatch.TourneyId}",
                        CreatedDate = mostRecentTourneyMatch.ModifiedDate.AddHours(hoursToAdd)
                    });
                }
            }
            foreach (Tourney tourney in tourneys.FindAll(x => x.Creator == user.Username && x.GetStatus() == TourneyStatus.NotStarted))
            {
                foreach (TourneyMatch tourneyMatch in tourney.TourneyMatches)
                {
                    if (tourneyMatch.Username1 != user.Username)
                    {
                        feeds.Add(new Feed
                        {
                            Username = tourneyMatch.Username1,
                            Description = $"signed up for your \"{tourney.Name}\" tourney!",
                            Category = FeedCategory.Tourney,
                            Href = $"/Tourney?id={tourneyMatch.TourneyId}",
                            CreatedDate = tourneyMatch.CreatedDate
                        });
                    }
                    if (!string.IsNullOrEmpty(tourneyMatch.Username2))
                    {
                        feeds.Add(new Feed
                        {
                            Username = tourneyMatch.Username2,
                            Description = $"signed up for your \"{tourney.Name}\" tourney!",
                            Category = FeedCategory.Tourney,
                            Href = $"/Tourney?id={tourneyMatch.TourneyId}",
                            CreatedDate = tourneyMatch.ModifiedDate
                        });
                    }
                }
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(Tourney tourney)
        {
            int hours = 24;
            int totalHours = (int)(DateTime.UtcNow - tourney.CreatedDate).TotalHours;
            int hoursToAdd = totalHours - (totalHours % hours);
            string description = $"Show off your skills! Join the \"{tourney.Name}\" tourney.";
            if (tourney.IsSponsored)
            {
                description = $"There's still space to join the \"{tourney.Name}\" tourney. It's free to enter and first place gets {Helper.GetFormattedPoints(tourney.EntranceCost * 0.8)}!";
            }
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Id = tourney.Id,
                    Description = description,
                    Category = FeedCategory.Tourney,
                    Href = $"/Tourney?id={tourney.Id}",
                    CreatedDate = tourney.CreatedDate.AddHours(hoursToAdd)
                }
            };

            return feeds;
        }

        public List<Feed> ConvertFrom(User user, Checklist checklist)
        {
            List<Feed> feeds = new List<Feed>();
            if (checklist?.Rank == ChecklistRank.Beginner)
            {
                feeds.Add(new Feed
                {
                    Description = $"Not sure where to start? Complete the first <b>5 tasks</b> on your checklist to graduate from the Beginner rank!",
                    Category = FeedCategory.Checklist,
                    OnClick = "highlightChecklist();",
                    CreatedDate = user.CreatedDate
                });
            }

            return feeds;
        }

        public List<Feed> ConvertFrom(User user)
        {
            List<Feed> feeds = new List<Feed>
            {
                new Feed
                {
                    Title = "Help Hub",
                    Description = $"Welcome to {Resources.SiteName}! Click here to check out the Help Hub or start exploring on your own!",
                    Category = FeedCategory.All,
                    Href = $"/Help",
                    CreatedDate = user.CreatedDate
                },
                new Feed
                {
                    Description = $"Nice! You've been a member of {Resources.SiteName} for",
                    Object = "1 week",
                    Category = FeedCategory.All,
                    Href = $"/Profile",
                    CreatedDate = user.CreatedDate.AddDays(7)
                },
                new Feed
                {
                    Description = $"Awesome! You've been a member of {Resources.SiteName} for",
                    Object = "1 month",
                    Category = FeedCategory.All,
                    Href = $"/Profile",
                    CreatedDate = user.CreatedDate.AddMonths(1)
                },
                new Feed
                {
                    Description = $"Congratulations! You've been a member of {Resources.SiteName} for",
                    Object = "6 months",
                    Category = FeedCategory.All,
                    Href = $"/Profile",
                    CreatedDate = user.CreatedDate.AddMonths(6)
                }
            };
            if (user.CreatedDate.Year != DateTime.UtcNow.Year && user.CreatedDate.DayOfYear == DateTime.UtcNow.DayOfYear)
            {
                int years = DateTime.UtcNow.Year - user.CreatedDate.Year;
                feeds.Add(new Feed
                {
                    Description = $"Happy Join Day! You've been a member of {Resources.SiteName} for",
                    Object = $"{Helper.FormatNumber(years)} year{(years == 1 ? "" : "s")}",
                    Category = FeedCategory.All,
                    Href = $"/Profile",
                    CreatedDate = DateTime.UtcNow.Date
                });
            }
            int ageInDays = user.GetAgeInDays();
            if (ageInDays > 0 && ageInDays % 2 == 0)
            {
                if (DateTime.UtcNow >= user.PremiumExpiryDate)
                {
                    feeds.Add(new Feed
                    {
                        Description = $"Help support {Resources.SiteName}'s monthly costs and remove ads, get exclusive items, and more! Get Sponsor Coins and unlock awesome perks!",
                        Category = FeedCategory.SponsorSociety,
                        Href = $"/SponsorSociety",
                        CreatedDate = DateTime.UtcNow.Date
                    });
                }
            }

            List<ActiveSession> activeSessions = _userRepository.GetActiveSessions(user);
            if (activeSessions.Count >= 15)
            {
                feeds.Add(new Feed
                {
                    Title = "Active Sessions",
                    Description = $"You are logged in to your account with too many devices. Clear them or you won't be able to sign in soon.",
                    Category = FeedCategory.All,
                    Href = $"/Preferences#active-sessions",
                    CreatedDate = activeSessions.First().CreatedDate
                });
            }

            return feeds;
        }
    }
}