﻿using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ReportsRepository : IReportsRepository
    {
        private IReportsPersistence _reportsPersistence;

        public ReportsRepository(IReportsPersistence reportsPersistence)
        {
            _reportsPersistence = reportsPersistence;
        }

        public List<Report> GetAllUnclaimedReports(User currentUser)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Helper)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _reportsPersistence.GetAllUnclaimedReports();
        }

        public List<Report> GetAllCompletedReports(User currentUser)
        {
            if (currentUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Helper)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _reportsPersistence.GetAllClaimedReports();
        }

        public List<Report> GetClaimedReports(User user)
        {
            if (user == null)
            {
                return null;
            }

            if (user.Role < UserRole.Helper)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _reportsPersistence.GetClaimedReportsWithUsername(user.Username);
        }

        public List<Report> GetReports(User currentUser, User reportUser, bool isReported)
        {
            if (currentUser == null || reportUser == null)
            {
                return null;
            }

            if (currentUser.Role < UserRole.Helper)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _reportsPersistence.GetReportsWithUsername(reportUser.Username, isReported);
        }

        public bool AddReport(User user, User reportedUser, string reason, int attentionHallPostId = 0)
        {
            if (reportedUser == null || string.IsNullOrEmpty(reason))
            {
                return false;
            }

            string ipAddress = GlobalHttpContext.Current.Connection.RemoteIpAddress.ToString();

            return _reportsPersistence.AddReport(user?.Username ?? string.Empty, reportedUser.Username, ipAddress, reason, attentionHallPostId);
        }

        public bool UpdateReport(User currentUser, Report report)
        {
            if (currentUser == null || report == null)
            {
                return false;
            }

            if (currentUser.Role < UserRole.Helper)
            {
                throw new System.Exception(Resources.InvalidPermissions);
            }

            return _reportsPersistence.UpdateReport(report);
        }
    }
}
