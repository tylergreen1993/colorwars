﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ArtFactoryRepository : IArtFactoryRepository
    {
        private IArtFactoryPersistence _artFactoryPersistence;

        public ArtFactoryRepository(IArtFactoryPersistence artFactoryPersistence)
        {
			_artFactoryPersistence = artFactoryPersistence;
        }

        public List<Custom> GetRecentUserCustomCards()
        {
            return _artFactoryPersistence.GetRecentUserCustomCards();
        }

        public bool AddUserCustomCard(User user, string name, string imageUrl)
        {
            if (user == null || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(imageUrl))
            {
                return false;
            }

            return _artFactoryPersistence.AddUserCustomCard(user.Username, name, imageUrl);
        }
    }
}
