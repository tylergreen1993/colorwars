﻿using System;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class CaptchaRepository : ICaptchaRepository
    {
        private IHttpContextAccessor _httpContextAccessor;

        public CaptchaRepository(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Captcha GenerateCaptcha(string key)
        {
            Random rnd = new Random();
            int number1 = rnd.Next(10, 51);
            int number2 = rnd.Next(10, 51);

            Captcha captcha = new Captcha
            {
                Number1 = number1,
                Number2 = number2,
                Answer = number1 + number2
            };

            HttpContext httpContext = _httpContextAccessor.HttpContext;
            IResponseCookies cookies = httpContext.Response.Cookies;

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(30)
            };

            cookies.Append($"{key}_MathAnswer", captcha.Answer.ToString(), options);

            return captcha;
        }

        public bool VerifyCaptcha(string key, int answer)
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;
            IRequestCookieCollection cookies = httpContext.Request.Cookies;

            if (cookies[$"{key}_MathAnswer"] == null)
            {
                return false;
            }

            if (int.TryParse(cookies[$"{key}_MathAnswer"], out int result))
            {
                return answer == result;
            }

            return false;
        }
    }
}
