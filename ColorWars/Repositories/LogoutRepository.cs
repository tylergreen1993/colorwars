﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class LogoutRepository : ILogoutRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IUserPersistence _userPersistence;
        private ISession _session;

        public LogoutRepository(IHttpContextAccessor httpContextAccessor, IUserPersistence userPersistence)
        {
            _httpContextAccessor = httpContextAccessor;
            _userPersistence = userPersistence;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public void Logout(User user)
        {
            if (user == null)
            {
                return;
            }

            HttpContext httpContext = _httpContextAccessor.HttpContext;
            ICollection<string> allCookieKeys = httpContext.Request.Cookies.Keys;
            IResponseCookies cookies = httpContext.Response.Cookies;

            foreach (string cookieKey in allCookieKeys)
            {
                cookies.Delete(cookieKey);
            }

            _userPersistence.LogoutUserWithAccessToken(user);

            _session.Clear();
        }
    }
}
