﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class GuideRepository : IGuideRepository
    {
        private IItemsRepository _itemsRepository;
        private IHttpContextAccessor _httpContextAccessor;
        private ISession _session;

        public GuideRepository(IHttpContextAccessor httpContextAccessor, IItemsRepository itemsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _itemsRepository = itemsRepository;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<ItemSection> GetItemSections(List<Guid> itemIds, List<int> redeemedIds, bool isCached = true)
        {
            List<ItemSection> itemSections = isCached ? _session.GetObject<List<ItemSection>>("ItemSections") : null;

            if (itemSections == null)
            {
                itemSections = new List<ItemSection>();
                List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, null, true);
                List<Card> allCards = _itemsRepository.GetAllCards(true);
                List<Enhancer> allEnhancers = _itemsRepository.GetAllEnhancers(true);
                List<Candy> allCandyCards = _itemsRepository.GetAllCandyCards(true);

                itemSections.Add(new ItemSection
                {
                    Id = 0,
                    Name = "Top Color",
                    Items = new List<Item>
                    {
                        allCards.FindAll(x => x.Color == CardColor.Red).Last(),
                        allCards.FindAll(x => x.Color == CardColor.Orange).Last(),
                        allCards.FindAll(x => x.Color == CardColor.Yellow).Last(),
                        allCards.FindAll(x => x.Color == CardColor.Green).Last(),
                        allCards.FindAll(x => x.Color == CardColor.Blue).Last(),
                        allCards.FindAll(x => x.Color == CardColor.Purple).Last()
                    }
                });
                itemSections.Add(new ItemSection
                {
                    Id = 1,
                    Name = "Enhancer Sampling",
                    Items = new List<Item>
                    {
                        allEnhancers.First(),
                        allEnhancers.Find(x => x.IsFiller && x.SpecialType == EnhancerSpecialType.None),
                        allEnhancers.Find(x => x.IsPercent && x.SpecialType == EnhancerSpecialType.None),
                        allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Luck),
                        allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Dark),
                        allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Predictor),
                        allEnhancers.Find(x => x.IsMultiplied && x.SpecialType == EnhancerSpecialType.None),
                        allEnhancers.Find(x => x.SpecialType == EnhancerSpecialType.Blocker)
                    }
                });
                itemSections.Add(new ItemSection
                {
                    Id = 2,
                    Name = "Candy Shop",
                    Items = new List<Item>
                    {
                        allCandyCards.Find(x => x.Type == CandyType.Chocolate),
                        allCandyCards.Find(x => x.Type == CandyType.Strawberry),
                        allCandyCards.Find(x => x.Type == CandyType.Sour),
                        allCandyCards.Find(x => x.Type == CandyType.Wizard),
                        allCandyCards.Find(x => x.Type == CandyType.Light),
                        allCandyCards.Find(x => x.Type == CandyType.Club)
                    }
                });
                itemSections.Add(new ItemSection
                {
                    Id = 3,
                    Name = "Form a Companionship",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Companion)
                });
                itemSections.Add(new ItemSection
                {
                    Id = 4,
                    Name = "Style Your Deck",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Theme && !x.Name.Contains("-"))
                });
                itemSections.Add(new ItemSection
                {
                    Id = 5,
                    Name = "Upgrade It",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Upgrader)
                });
                itemSections.Add(new ItemSection
                {
                    Id = 6,
                    Name = "Steal and Protect",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Stealth)
                });
                itemSections.Add(new ItemSection
                {
                    Id = 8,
                    Name = "Magic in the Air",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Fairy)
                });
                itemSections.Add(new ItemSection
                {
                    Id = 7,
                    Name = "Feeling Artsy",
                    Items = allItems.FindAll(x => x.Category == ItemCategory.Art).Take(12).ToList()
                });
            }

            foreach (ItemSection itemSection in itemSections)
            {
                itemSection.AmountOwned = itemSection.Items.FindAll(x => itemIds.Any(y => y == x.Id)).Count;
                itemSection.RequiredAmount = (int)Math.Round(itemSection.Items.Count * 0.75);
                itemSection.IsRedeemed = redeemedIds.Any(x => x == itemSection.Id);
            }

            return itemSections;
        }
    }
}
