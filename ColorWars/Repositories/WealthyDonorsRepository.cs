﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class WealthyDonorsRepository : IWealthyDonorsRepository
    {
        private IWealthyDonorsPersistence _wealthyDonorsPersistence;

        public WealthyDonorsRepository(IWealthyDonorsPersistence wealthyDonorsPersistence)
        {
            _wealthyDonorsPersistence = wealthyDonorsPersistence;
        }

        public List<WealthyDonor> GetWealthyDonors()
        {
            return _wealthyDonorsPersistence.GetWealthyDonors();
        }

        public bool AddWealthyDonor(User user, int amount)
        {
            if (user == null)
            {
                return false;
            }

            return _wealthyDonorsPersistence.AddWealthyDonor(user.Username, amount);
        }
    }
}
