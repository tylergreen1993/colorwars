﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class StatisticsRepository : IStatisticsRepository
    {
        private IStatisticsPersistence _statisticsPersistence;

        public StatisticsRepository(IStatisticsPersistence statisticsPersistence)
        {
            _statisticsPersistence = statisticsPersistence;
        }

        public Statistics GetStatistics(bool isCached = true)
        {

            return _statisticsPersistence.GetStatistics(isCached);
        }
    }
}
