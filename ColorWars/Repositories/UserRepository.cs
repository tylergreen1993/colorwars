﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IUserPersistence _userPersistence;
        private IInactiveAccountsPersistence _inactiveAccountsPersistence;
        private ILogoutRepository _logoutRepository;

        public UserRepository(IUserPersistence userPersistence, IInactiveAccountsPersistence inactiveAccountsPersistence,
        ILogoutRepository logoutRepository)
        {
            _userPersistence = userPersistence;
            _inactiveAccountsPersistence = inactiveAccountsPersistence;
            _logoutRepository = logoutRepository;
        }

        public User GetUser(string username, bool onlyActive = true)
        {
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }

            username = username.Trim();

            User user = _userPersistence.GetUserWithUsername(username);
            if (user == null || (onlyActive && user.InactiveType != InactiveAccountType.None))
            {
                return null;
            }

            return user;
        }

        public User GetUserWithEmail(string emailAddress, bool onlyActive = true)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                return null;
            }

            User user = _userPersistence.GetUserWithEmail(emailAddress);
            if (user == null || (onlyActive && user.InactiveType != InactiveAccountType.None))
            {
                return null;
            }

            return user;
        }

        public double GetDAUMAUStatistic(User user)
        {
            if (user == null || user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return Math.Round(_userPersistence.GetDAUMAUStatistic()?.TotalPercent ?? 0, 2);
        }

        public List<User> GetUsersByIPAddress(string ipAddress, bool includeSessions = false)
        {
            if (string.IsNullOrEmpty(ipAddress))
            {
                return null;
            }

            return _userPersistence.GetUsersByIPAddress(ipAddress, includeSessions);
        }

        public List<User> GetUsersWithAdminRoles()
        {
            return _userPersistence.GetUsersWithAdminRoles();
        }

        public List<User> GetRecentUsers(User user, Gender gender)
        {
            if (user == null || user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _userPersistence.GetRecentUsers(gender);
        }

        public List<User> GetReturningUsers(User user, Gender gender)
        {
            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _userPersistence.GetReturningUsers(gender);
        }

        public List<User> GetAllUsers(User user, Gender gender, int days)
        {
            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _userPersistence.GetAllUsers(gender, days);
        }

        public List<User> GetActiveUsers(User user, int minutes)
        {
            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            if (minutes < 0)
            {
                return null;
            }

            return _userPersistence.GetActiveUsers(minutes);
        }

        public List<User> GetRecentlyReturningUsers(User user)
        {
            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _userPersistence.GetRecentlyReturningUsers();
        }

        public List<User> GetNonReturningUsers(User user)
        {
            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _userPersistence.GetNonReturningUsers();
        }

        public List<ActiveSession> GetActiveSessions(User user)
        {
            if (user == null)
            {
                return null;
            }
            return _userPersistence.GetAccessTokensWithUsername(user.Username);
        }

        public List<InactiveAccount> GetInactiveUsers(User user, InactiveAccountType type)
        {
            if (user == null || type == InactiveAccountType.None)
            {
                return null;
            }

            if (user.Role < UserRole.Admin)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _inactiveAccountsPersistence.GetInactiveAccounts(type);
        }

        public List<BannedIPAddress> GetBannedIPAddresses()
        {
            return _userPersistence.GetBannedIPAddresses();
        }

        public bool AddBannedIPAddress(string ipAddress)
        {
            if (string.IsNullOrEmpty(ipAddress))
            {
                return false;
            }

            return _userPersistence.AddBannedIPAddress(ipAddress);
        }

        public bool UpdateUser(User user, string password = "")
        {
            if (user == null)
            {
                return false;
            }
            return _userPersistence.UpdateUserWithUsername(user, password);
        }

        public bool EnableUser(User user)
        {
            if (user == null || user.InactiveType != InactiveAccountType.Disabled)
            {
                return false;
            }

            user.InactiveType = InactiveAccountType.None;
            if (UpdateUser(user))
            {
                return _inactiveAccountsPersistence.RemoveInactiveAccount(user.Username);
            }

            return false;
        }

        public bool DisableUser(User user, InactiveAccountReason reason)
        {
            if (user == null || user.InactiveType != InactiveAccountType.None || reason <= InactiveAccountReason.None)
            {
                return false;
            }

            user.InactiveType = InactiveAccountType.Disabled;
            if (UpdateUser(user))
            {
                _logoutRepository.Logout(user);
                return _inactiveAccountsPersistence.AddInactiveAccount(user.Username, reason, InactiveAccountType.Disabled, string.Empty);
            }

            return false;
        }

        public bool DeleteUser(User user, InactiveAccountReason reason, string info)
        {
            if (user == null || user.InactiveType == InactiveAccountType.Deleted || reason <= InactiveAccountReason.None)
            {
                return false;
            }

            user.InactiveType = InactiveAccountType.Deleted;
            if (UpdateUser(user))
            {
                _userPersistence.DeleteInformationWithUsername(user.Username);
                _logoutRepository.Logout(user);
                return _inactiveAccountsPersistence.AddInactiveAccount(user.Username, reason, InactiveAccountType.Deleted, info);
            }

            return false;
        }

        public bool DeleteActiveSessions(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _userPersistence.DeleteAccessTokens(user);
        }


        public bool DeleteBannedIPAddress(string ipAddress)
        {
            if (string.IsNullOrEmpty(ipAddress))
            {
                return false;
            }

            return _userPersistence.RemoveBannedIPAddress(ipAddress);
        }

        public bool IsUsernameInvalid(string username)
        {
            return string.IsNullOrEmpty(username) || username.Length > 15 || username.EndsWith('.') || !Regex.IsMatch(username, "^[a-zA-Z0-9_.-]*$", RegexOptions.IgnoreCase);
        }

        public bool IsEmailAddressInvalid(string emailAddress)
        {
            return string.IsNullOrEmpty(emailAddress) || emailAddress.Length > 35 || !emailAddress.Contains("@") || !emailAddress.Contains(".");
        }

        public bool IsPasswordValid(User user, string password)
        {
            if (user == null || string.IsNullOrEmpty(password))
            {
                return false;
            }

            User returnedUser = _userPersistence.GetUserWithUsernameAndPassword(user.Username, password, string.Empty, string.Empty, false);
            if (returnedUser == null)
            {
                return false;
            }
            return user.Username == returnedUser.Username;
        }
    }
}
