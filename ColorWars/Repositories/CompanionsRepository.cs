﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class CompanionsRepository : ICompanionsRepository
    {
        private ICompanionsPersistence _companionsPersistence;

        public CompanionsRepository(ICompanionsPersistence companionsPersistence)
        {
            _companionsPersistence = companionsPersistence;
        }

        public List<Companion> GetCompanions(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _companionsPersistence.GetUserCompanionsWithUsername(user.Username, isCached).OrderBy(x => x.Position).ToList();
        }

        public List<Companion> GetAdoptionCompanions()
        {
            return _companionsPersistence.GetAdoptionCompanions();
        }

        public bool AddCompanion(User user, CompanionType type, string name, CompanionColor color, out Companion companion)
        {
            companion = null;

            if (user == null)
            {
                return false;
            }

            List<Companion> companions = GetCompanions(user);
            int position = companions.LastOrDefault()?.Position + 1 ?? 0;

            companion = _companionsPersistence.AddUserCompanion(user.Username, type, name, color, position);

            if (companion == null)
            {
                return false;
            }

            return true;
        }

        public bool AddAdoptionCompanion(Companion companion)
        {
            if (companion == null)
            {
                return false;
            }

            return _companionsPersistence.AddAdoptionCompanion(companion.Id, companion.Type, companion.Name, companion.Color, companion.Level, companion.CreatedDate);
        }

        public bool UpdateCompanion(Companion companion)
        {
            if (companion == null)
            {
                return false;
            }

            return _companionsPersistence.UpdateUserCompanionWithId(companion);
        }

        public bool DeleteCompanion(User user, Companion companion)
        {
            if (user == null || companion == null)
            {
                return false;
            }

            List<Companion> companions = GetCompanions(user);
            int position = companions.Find(x => x.Id == companion.Id).Position;
            foreach (Companion existingCompanion in companions)
            {
                if (existingCompanion.Position > position)
                {
                    existingCompanion.Position--;
                    UpdateCompanion(existingCompanion);
                }
            }

            return _companionsPersistence.DeleteUserCompanionWithId(companion.Id);
        }

        public bool DeleteAdoptionCompanion(Companion companion)
        {
            if (companion == null)
            {
                return false;
            }

            return _companionsPersistence.DeleteAdoptionCompanion(companion.Id);
        }
    }
}
