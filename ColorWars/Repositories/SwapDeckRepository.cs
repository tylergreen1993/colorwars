﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SwapDeckRepository : ISwapDeckRepository
    {
        private ISwapDeckPersistence _swapDeckPersistence;

        public SwapDeckRepository(ISwapDeckPersistence swapDeckPersistence)
        {
            _swapDeckPersistence = swapDeckPersistence;
        }

        public List<SwapDeckHistory> GetSwapDeckHistory(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }
            return _swapDeckPersistence.GetSwapDeckHistoryWithUsername(user.Username, isCached);
        }

        public bool AddSwapDeckHistory(User user, SwapDeckType type, string message)
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _swapDeckPersistence.AddSwapDeckHistory(user.Username, type, message);
        }
    }
}
