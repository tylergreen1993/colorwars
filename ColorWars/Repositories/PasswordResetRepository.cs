﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class PasswordResetRepository : IPasswordResetRepository
    {
        private IPasswordResetPersistence _passwordResetPersistence;

        public PasswordResetRepository(IPasswordResetPersistence passwordResetPersistence)
        {
            _passwordResetPersistence = passwordResetPersistence;
        }

        public PasswordReset GetPasswordReset(string username, Guid id)
        {
            if (id == Guid.Empty || string.IsNullOrEmpty(username))
            {
                return null;
            }

            return _passwordResetPersistence.GetPasswordReset(id, username);
        }

        public PasswordReset CreateOrUpdatePasswordReset(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _passwordResetPersistence.CreateOrUpdatePasswordReset(user.Username);
        }
    }
}
