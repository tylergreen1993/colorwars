﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ProfileRepository : IProfileRepository
    {
        private IProfilePersistence _profilePersistence;

        public ProfileRepository(IProfilePersistence profilePersistence)
        {
            _profilePersistence = profilePersistence;
        }

        public List<Item> GetShowroomItems(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _profilePersistence.GetShowroomItemsWithUsername(user.Username, isCached);
        }

        public List<ShowroomLike> GetShowroomLikes(User showroomUser)
        {
            if (showroomUser == null)
            {
                return null;
            }

            return _profilePersistence.GetShowroomLikes(showroomUser.Username);
        }

        public bool AddShowroomItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            int position = GetShowroomItems(user).LastOrDefault()?.Position + 1 ?? 0;

            if (item.SelectionId == Guid.Empty)
            {
                item.SelectionId = Guid.NewGuid();
                item.Uses = item.TotalUses;
                item.CreatedDate = DateTime.UtcNow;
                item.RepairedDate = DateTime.UtcNow;
            }

            return _profilePersistence.AddShowroomItem(item, position);
        }

        public bool SetShowroomLike(User user, User showroomUser)
        {
            if (user == null || showroomUser == null)
            {
                return false;
            }

            return _profilePersistence.AddOrDeleteShowroomLike(user.Username, showroomUser.Username);
        }

        public bool UpdateShowroomItem(Item showroomItem)
        {
            if (showroomItem == null)
            {
                return false;
            }

            return _profilePersistence.UpdateShowroomItemWithSelectionId(showroomItem.SelectionId, showroomItem.Position, showroomItem.Uses, showroomItem.RepairedDate);
        }

        public bool DeleteShowroomItem(Item showroomItem)
        {
            if (showroomItem == null)
            {
                return false;
            }

            return _profilePersistence.DeleteShowroomItemWithSelectionId(showroomItem.SelectionId);
        }
    }
}
