﻿using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class SoundsRepository : ISoundsRepository
    {
        private List<Sound> _sounds;

        public SoundsRepository()
        {
            PopulateSounds();
        }

        public string GetSoundUrl(SoundType soundType, Settings settings)
        {
            if (!settings.EnableSoundEffects)
            {
                return string.Empty;
            }

            Sound sound = _sounds.Find(x => x.Type == soundType);
            if (sound == null)
            {
                return string.Empty;
            }

            return sound.Url;
        }

        private void PopulateSounds()
        {
            _sounds = new List<Sound>
            {
                new Sound
                {
                    Type = SoundType.Plop,
                    Url = Helper.GetSoundUrl("Plop.mp3")
                },
                new Sound
                {
                    Type = SoundType.Victory,
                    Url = Helper.GetSoundUrl("Victory.mp3")
                },
                new Sound
                {
                    Type = SoundType.Defeat,
                    Url = Helper.GetSoundUrl("Defeat.mp3")
                },
                new Sound
                {
                    Type = SoundType.Success,
                    Url = Helper.GetSoundUrl("Success.mp3")
                },
                new Sound
                {
                    Type = SoundType.Negative,
                    Url = Helper.GetSoundUrl("Negative.mp3")
                },
                new Sound
                {
                    Type = SoundType.StadiumIntro,
                    Url = Helper.GetSoundUrl("StadiumIntro.mp3")
                },
                new Sound
                {
                    Type = SoundType.Notification,
                    Url = Helper.GetSoundUrl("Notification.mp3")
                },
                new Sound
                {
                    Type = SoundType.VictoryAlt,
                    Url = Helper.GetSoundUrl("VictoryAlt.mp3")
                },
                new Sound
                {
                    Type = SoundType.Coins,
                    Url = Helper.GetSoundUrl("Coins.mp3")
                },
                new Sound
                {
                    Type = SoundType.NegativeAlt,
                    Url = Helper.GetSoundUrl("NegativeAlt.mp3")
                },
                new Sound
                {
                    Type = SoundType.SuccessShort,
                    Url = Helper.GetSoundUrl("Success_Short.mp3")
                },
                new Sound
                {
                    Type = SoundType.NegativeShort,
                    Url = Helper.GetSoundUrl("Negative_Short.mp3")
                },
                new Sound
                {
                    Type = SoundType.Mine,
                    Url = Helper.GetSoundUrl("Mine.mp3")
                },
                new Sound
                {
                    Type = SoundType.Scissors,
                    Url = Helper.GetSoundUrl("Scissors.mp3")
                },
                new Sound
                {
                    Type = SoundType.Shred,
                    Url = Helper.GetSoundUrl("Shred.mp3")
                },
                new Sound
                {
                    Type = SoundType.SuccessShortAlt,
                    Url = Helper.GetSoundUrl("Success_Short_Alt.mp3")
                }
            };
        }
    }
}