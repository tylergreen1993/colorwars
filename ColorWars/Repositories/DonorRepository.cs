﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class DonorRepository : IDonorRepository
    {
        private IDonorPersistence _donorPersistence;

        public DonorRepository(IDonorPersistence donorPersistence)
        {
            _donorPersistence = donorPersistence;
        }

        public List<Donor> GetDonorsWithDays(int days, bool isCached = true)
        {
            return _donorPersistence.GetDonorsWithDays(days, isCached);
        }


        public List<SponsorCoinPackage> GetSponsorCoinPackages()
        {
            return new List<SponsorCoinPackage>
            {
                new SponsorCoinPackage
                {
                    CoinAmount = 10,
                    MoneyAmount = 1.99,
                    ImageUrl = "SponsorSociety/Coins.png"
                },
                new SponsorCoinPackage
                {
                    CoinAmount = 20,
                    MoneyAmount = 3.49,
                    ImageUrl = "SponsorSociety/Coins.png",
                    Label = "15% Bonus"
                },
                new SponsorCoinPackage
                {
                    CoinAmount = 30,
                    MoneyAmount = 4.99,
                    ImageUrl = "SponsorSociety/Coins.png",
                    Label = "20% Bonus"
                },
                new SponsorCoinPackage
                {
                    CoinAmount = 65,
                    MoneyAmount = 9.99,
                    ImageUrl = "SponsorSociety/CoinBag.png",
                    Label = "30% Bonus"
                },
                new SponsorCoinPackage
                {
                    CoinAmount = 150,
                    MoneyAmount = 19.99,
                    ImageUrl = "SponsorSociety/CoinBag.png",
                    Label = "50% Bonus"
                },
                new SponsorCoinPackage
                {
                    CoinAmount = 450,
                    MoneyAmount = 49.99,
                    ImageUrl = "SponsorSociety/CoinBag.png",
                    Label = "80% Bonus"
                }
            };
        }

        public List<SponsorPerk> GetSponsorPerks(User user, Settings settings)
        {
            List<SponsorPerk> sponsorPerks = new List<SponsorPerk>
            {
                new SponsorPerk
                {
                    Id = SponsorPerkId.CoinGift,
                    Name = "Coin Gift",
                    Description = $"Make someone’s day and send them Sponsor Coins as a gift!",
                    ImageUrl = "SponsorSociety/Coins_Big.png",
                    Coins= 1
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.ElegantTheme,
                    Name = "Elegant Theme",
                    Description = $"Give one of your {Resources.DeckCards} the exclusive Elegant Theme!",
                    ImageUrl = "Cards/Elegant/12.png",
                    Coins= 5
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.StoreMagician,
                    Name = "Store Magician",
                    Description = $"Gain access to the Store Magician for {(settings.StoreMagicianExpiryDate > DateTime.UtcNow ? "30 more days" : "30 days")} and search all stores at once!",
                    ImageUrl = "SponsorSociety/StoreMagician.png",
                    Coins= 30
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.SpecialArtCard,
                    Name = "Premium Art Card",
                    Description = $"Get 1 of 10 premium Art Cards!",
                    ImageUrl = "Art/26.png",
                    Coins= 5
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.UnicornCompanion,
                    Name = "Unicorn Companion",
                    Description = $"Get a premium, magical Unicorn Companion!",
                    ImageUrl = "SponsorSociety/Unicorn.png",
                    Coins= 15
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.RemoveAds,
                    Name = "Remove Ads",
                    Description = $"Remove all ads for {(user.AdFreeExpiryDate > DateTime.UtcNow ? "30 more days" : "30 days")}!",
                    ImageUrl = "SponsorSociety/RemoveAds.png",
                    Coins= 20
                },
                new SponsorPerk
                {
                    Id = SponsorPerkId.SponsorCardPack,
                    Name = "Sponsor Card Pack",
                    Description = $"Everyone will have 24 hours to redeem a promo code for a free Card Pack - Rare sponsored by you!",
                    ImageUrl = "CardPacks/RarePack.png",
                    Coins= 20
                }
            };

            if (settings.MaxCompanions < 10)
            {
                sponsorPerks.Add(new SponsorPerk
                {
                    Id = SponsorPerkId.CompanionUpgrade,
                    Name = "Companion Upgrade",
                    Description = $"Permanently have 4 more Companions on your profile!",
                    ImageUrl = "SponsorSociety/CompanionUpgrade.png",
                    Coins = 15
                });
            }
            if (settings.MaxStorageSize < 200)
            {
                sponsorPerks.Add(new SponsorPerk
                {
                    Id = SponsorPerkId.MaximumStorageSize,
                    Name = "Maximum Storage",
                    Description = $"Permanently increase your maximum Storage size to 200 items!",
                    ImageUrl = "SponsorSociety/MaximumStorage.png",
                    Coins = 25
                });
            }
            if (settings.StoreMagicianExpiryDate > DateTime.UtcNow)
            {
                SponsorPerk sponsorPerk = sponsorPerks.Find(x => x.Id == SponsorPerkId.StoreMagician);
                sponsorPerk.Description += $" ({settings.StoreMagicianExpiryDate.GetTimeUntil()} left)";
            }
            if (user.AdFreeExpiryDate > DateTime.UtcNow)
            {
                SponsorPerk sponsorPerk = sponsorPerks.Find(x => x.Id == SponsorPerkId.RemoveAds);
                sponsorPerk.Description += $" ({user.AdFreeExpiryDate.GetTimeUntil()} left)";
            }

            return sponsorPerks;
        }

        public bool AddDonor(User user, int amount)
        {
            if (user == null || amount <= 0)
            {
                return false;
            }

            return _donorPersistence.AddDonor(user.Username, amount);
        }
    }
}
