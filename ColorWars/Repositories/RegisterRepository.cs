﻿using System;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class RegisterRepository : IRegisterRepository
    {
        private IUserPersistence _userPersistence;
        private IHttpContextAccessor _httpContextAccessor;

        public RegisterRepository(IHttpContextAccessor httpContextAccessor, IUserPersistence userPersistence)
        {
            _httpContextAccessor = httpContextAccessor;
            _userPersistence = userPersistence;
        }

        public User CreateNewUser(string username, string password, string emailAddress, bool allowNotifications, string referral)
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;
            string ipAddress = httpContext.Connection.RemoteIpAddress.ToString();
            string userAgent = httpContext.Request.Headers["User-Agent"].ToString();
            string clientInfo = Helper.GetUserAgentInfo(userAgent);

            User user = _userPersistence.CreateNewUser(username, password, emailAddress, allowNotifications, referral, ipAddress, clientInfo);
            if (user == null)
            {
                return null;
            }

            IResponseCookies cookies = httpContext.Response.Cookies;
            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddYears(1)
            };

            cookies.Append("Username", user.Username, options);
            cookies.Append("AccessToken", user.AccessToken.ToString(), options);

            return user;
        }

        public bool IsUsernameAvailable(string username)
        {
            User user = _userPersistence.GetUserWithUsername(username);
            return user == null;
        }

        public bool IsEmailAvailable(string email)
        {
            User user = _userPersistence.GetUserWithEmail(email);
            return user == null;
        }
    }
}
