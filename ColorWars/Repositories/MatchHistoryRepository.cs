﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class MatchHistoryRepository : IMatchHistoryRepository
    {
        private IMatchHistoryPersistence _matchHistoryPersistence;
        private IUserRepository _userRepository;

        public MatchHistoryRepository(IMatchHistoryPersistence matchHistoryPersistence, IUserRepository userRepository)
        {
            _matchHistoryPersistence = matchHistoryPersistence;
            _userRepository = userRepository;
        }

        public List<MatchHistory> GetMatchHistory(User user, bool onlyUsers = false, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<MatchHistory> matchHistories = _matchHistoryPersistence.GetMatchHistoryWithUsername(user.Username, isCached);

            if (onlyUsers)
            {
                return matchHistories.FindAll(x => x.CPUType == CPUType.None);
            }

            return matchHistories;
        }

        public MatchRanking GetMatchRanking(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _matchHistoryPersistence.GetMatchRanking(user.Username);
        }

        public List<MatchRanking> GetTopMatchRankings()
        {
            return _matchHistoryPersistence.GetTopMatchRankings();
        }

        public List<MatchTag> GetMatchTags(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<MatchHistory> matchHistories = GetMatchHistory(user, true, isCached).FindAll(x => x.Result == MatchHistoryResult.Won);
            IEnumerable<IGrouping<string, MatchHistory>> groupedMatchHistories = matchHistories.GroupBy(x => x.Opponent);
            List<MatchTag> matchTags = new List<MatchTag>();
            foreach (IGrouping<string, MatchHistory> groupedMatchHistory in groupedMatchHistories)
            {
                matchTags.Add(new MatchTag
                {
                    Username = user.Username,
                    TagUsername = groupedMatchHistory.Key,
                    Color = groupedMatchHistory.First().OpponentColor,
                    Wins = groupedMatchHistory.Count(),
                    FirstWinDate = groupedMatchHistory.Last().CreatedDate
                });
            }

            return matchTags.OrderByDescending(x => x.Wins).ToList();
        }

        public bool AddMatchHistory(User user, MatchHistory matchHistory, bool isUser = true)
        {
            if (user == null)
            {
                return false;
            }

            if (matchHistory == null)
            {
                return false;
            }

            if (!isUser)
            {
                user.FlushSession = true;
                _userRepository.UpdateUser(user);
            }

            return _matchHistoryPersistence.AddMatchHistory(user.Username, matchHistory.Opponent, matchHistory.Points, matchHistory.Level, matchHistory.Intensity, matchHistory.CPUType, matchHistory.TourneyMatchId, matchHistory.Result);
        }

        public bool IsElite(User user, MatchRanking matchRanking = null)
        {
            if (user == null && matchRanking == null)
            {
                return false;
            }

            matchRanking ??= GetMatchRanking(user);
            if (matchRanking != null)
            {
                return matchRanking.Percentile <= 0.25 && matchRanking.TotalMatches >= 5;
            }

            return false;
        }
    }
}
