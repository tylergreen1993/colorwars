﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class LibraryRepository : ILibraryRepository
    {
        private ILibraryPersistence _libraryPersistence;

        public LibraryRepository(ILibraryPersistence libraryPersistence)
        {
            _libraryPersistence = libraryPersistence;
        }

        public List<LibraryHistory> GetLibraryHistory(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }
            return _libraryPersistence.GetLibraryHistoryWithUsername(user.Username, isCached);
        }

        public List<VoidMessage> GetVoidMessages()
        {
            return _libraryPersistence.GetVoidMessages();
        }

        public bool AddLibraryHistory(User user, LibraryKnowledgeType type, string message)
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _libraryPersistence.AddLibraryHistory(user.Username, type, message);
        }

        public bool AddVoidMessage(User user, string message)
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _libraryPersistence.AddVoidMessage(user.Username, message);
        }

        public bool CanPullLever(List<Accomplishment> accomplishments)
        {
            if (!accomplishments.Any(x => x.Id == AccomplishmentId.LibraryCardsCollector))
            {
                return false;
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.AllChecklist))
            {
                return false;
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.RightSideUpGuardDefeated))
            {
                return false;
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.MuseumEastWingUnlocked))
            {
                return false;
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.MuseumWestWingUnlocked))
            {
                return false;
            }

            if (!accomplishments.Any(x => x.Id == AccomplishmentId.MuseumSouthWingUnlocked))
            {
                return false;
            }

            return true;
        }
    }
}
