﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SurveyRepository : ISurveyRepository
    {
        private ISurveyPersistence _surveyPersistence;
        private ISettingsRepository _settingsRepository;
        private List<SurveyQuestion> _surveyQuestions;

        public SurveyRepository(ISurveyPersistence surveyPersistence, ISettingsRepository settingsRepository)
        {
            _surveyPersistence = surveyPersistence;
            _settingsRepository = settingsRepository;
            _surveyQuestions = GenerateSurveyQuestions();
        }

        public List<SurveyQuestion> GetSurveyQuestions(User user, bool isCached = true)
        {
            Settings settings = _settingsRepository.GetSettings(user, isCached);
            if (string.IsNullOrEmpty(settings.SurveyQuestionIds))
            {
                return null;
            }

            List<SurveyQuestionId> surveyQuestionIds = settings.SurveyQuestionIds.Split("-").Select(x => (SurveyQuestionId)int.Parse(x)).ToList();
            return surveyQuestionIds.Select(x => _surveyQuestions.Find(y => y.Id == x)).ToList();
        }

        public List<SurveyAnswer> GetSurveyAnswers(User user = null, User adminUser = null)
        {
            if (user == null)
            {
                if (adminUser?.Role < UserRole.Admin)
                {
                    throw new Exception(Resources.InvalidPermissions);
                }
            }

            List<SurveyAnswer> surveyAnswers = user == null ? _surveyPersistence.GetSurveyAnswers() : _surveyPersistence.GetSurveyAnswersWithUsername(user.Username);
            UpdateSurveyAnswers(surveyAnswers);

            return surveyAnswers;
        }

        public bool AddSurveyAnswer(User user, SurveyQuestionId surveyQuestionId, string answer)
        {
            if (user == null || surveyQuestionId == SurveyQuestionId.None || string.IsNullOrEmpty(answer))
            {
                return false;
            }

            return _surveyPersistence.AddSurveyAnswer(user.Username, surveyQuestionId, answer);
        }

        public bool SaveNewSurveyQuestions(User user, bool isCached = true)
        {
            if (user == null)
            {
                return false;
            }

            Settings settings = _settingsRepository.GetSettings(user, isCached);

            List<SurveyQuestionId> surveyQuestionIds = Enum.GetValues(typeof(SurveyQuestionId)).Cast<SurveyQuestionId>().ToList();
            surveyQuestionIds.Remove(SurveyQuestionId.None);
            surveyQuestionIds.Remove(SurveyQuestionId.AnythingElse);
            surveyQuestionIds.Remove(SurveyQuestionId.Age);
            surveyQuestionIds.Remove(SurveyQuestionId.RecommendedToOthers);
            surveyQuestionIds.Remove(SurveyQuestionId.DiscoverySource);

            List<SurveyQuestionId> surveyQuestions = new List<SurveyQuestionId>();
            if (string.IsNullOrEmpty(settings.SurveyQuestionIds) || GetSurveyAnswers(user).Count == 0)
            {
                surveyQuestions.Add(SurveyQuestionId.Age);
                surveyQuestions.Add(SurveyQuestionId.RecommendedToOthers);
                surveyQuestions.Add(SurveyQuestionId.DiscoverySource);
            }

            int totalSurveyQuestions = 5;
            Random rnd = new Random();

            while (surveyQuestions.Count < totalSurveyQuestions)
            {
                SurveyQuestionId surveyQuestionId = surveyQuestionIds[rnd.Next(surveyQuestionIds.Count)];
                surveyQuestions.Add(surveyQuestionId);
                surveyQuestionIds.Remove(surveyQuestionId);
            }

            surveyQuestions.Add(SurveyQuestionId.AnythingElse);
            string surveyQuestionIdIndices = string.Join("-", surveyQuestions.Select(x => (int)x));

            settings.SurveyQuestionIds = surveyQuestionIdIndices;
            return _settingsRepository.SetSetting(user, nameof(settings.SurveyQuestionIds), settings.SurveyQuestionIds);
        }

        private List<SurveyQuestion> GenerateSurveyQuestions()
        {
            return new List<SurveyQuestion>
            {
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.Age,
                    Question = "How old are you?",
                    Options = new List<string> { "Under 15", "15 - 17", "18 - 21", "22 - 25", "26 - 30", "31 - 35", "36 - 40", "41 - 45", "46 - 55", "Over 55" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.AnythingElse,
                    Question = "Is there anything else you'd like to add?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.BooksReadPerYear,
                    Question = "How many books do you read per year?",
                    Options = new List<string> { "None", "1 - 3", "5 - 9", "10 - 15", "16 - 20", "21 -  30", "31 - 50", "Over 50" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.ComputerTime,
                    Question = "How many hours do you spend on your computer daily?",
                    Options = new List<string> { "None", "Under 1", "1 - 2", "3 - 5", "6 - 10", "11 - 15", "16 - 20", "Over 20" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.CurrentJob,
                    Question = "What is your current job? (Or what you are studying if you're in school)",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.YoungWizardEffect,
                    Question = "What new effect would you like to see the Young Wizard cast?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.EverSubmittedJobCenter,
                    Question = "Do you know about the Job Center?",
                    Options = new List<string> { "Yes and I've made a submission", "Yes but I've never made a submission", "I've heard about it but don't understand it", "No" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FaveGames,
                    Question = "What are some of your favorite video games?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FaveGamingSystem,
                    Question = "Which is your preferred device to play games on?",
                    Options = new List<string> { "Computer", "Phone", "Gaming Console", "Other" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FaveInterests,
                    Question = "What are some of your non-gaming interests?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FavoriteLocation,
                    Question = $"What is your favorite {Resources.SiteName} location?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FirstHearAbout,
                    Question = $"How did you first hear about {Resources.SiteName}?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.GamesPlayedPerWeek,
                    Question = $"How many hours do you spend playing video games weekly?",
                    Options = new List<string> { "None", "Under 1", "1 - 2", "3 - 5", "6 - 10", "11 - 15", "16 - 20", "21 - 25", "26 - 30", "31 - 40", "41 - 50", "Over 50" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.EntertainmentSpending,
                    Question = $"Which form of entertainment do you spend the most money on yearly?",
                    Options = new List<string> { "Video Games", "Music", "Live Shows", "Movies / Film", "Exhibitions / Museums", "Television", "Social", "Other" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.GamePurchases,
                    Question = $"How many video game purchases do you make per year?",
                    Options = new List<string> { "None", "1 - 2", "3 - 5", "6 - 9", "10 - 15", "16 - 20", "21 - 25", "26 - 30", "Over 30" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.JoinReason,
                    Question = $"What made you decide to join {Resources.SiteName}?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.FavoriteMoment,
                    Question = $"What is your favorite {Resources.SiteName} moment that you've experienced so far?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.LeastFavoriteLocation,
                    Question = $"What is your least favorite {Resources.SiteName} location?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.MobileOperatingSystem,
                    Question = $"Which mobile operating system does your phone use?",
                    Options = new List<string> { "Don't have a mobile phone", "Android", "iOS", "Windows", "Other" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.MoneySpentOnGamesYearly,
                    Question = $"How much money do you spend on video games yearly?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.MostAttachedToFeature,
                    Question = $"What {Resources.SiteName} feature are you most attached to?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.NextFeature,
                    Question = $"What {Resources.SiteName} feature would you like to see added next?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.PayToUse,
                    Question = $"Would you still use {Resources.SiteName} if it cost money? (We don't intend to do this)",
                    Options = new List<string> { "No", "I'd consider it", "Yes, if it wasn't expensive", "Absolutely" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.PhoneTime,
                    Question = $"How many hours do you spend on your mobile phone daily?",
                    Options = new List<string> { "Don't own a mobile phone", "Under 1", "1 - 2", "3 - 4", "5 - 8", "9 - 12", "13 - 16", "17 - 20", "Over 20" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.DeviceUsage,
                    Question = $"Which device do you mainly use {Resources.SiteName} with?",
                    Options = new List<string> { "Computer", "Tablet", "Phone", "Other" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.RateFun,
                    Question = $"How fun would you rate {Resources.SiteName} on a scale from 1 (worst) to 5 (best)?",
                    Options = new List<string> { "1", "2", "3", "4", "5" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.RateOverall,
                    Question = $"How would you rate {Resources.SiteName} overall on a scale from 1 (worst) to 5 (best)?",
                    Options = new List<string> { "1", "2", "3", "4", "5" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.RecommendedToOthers,
                    Question = $"How likely would you be to recommend {Resources.SiteName} to your friends or family from 1 (not at all) to 10 (absolutely)?",
                    Options = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.SomethingThatCanBeImproved,
                    Question = $"What is something about {Resources.SiteName} that can be improved?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.SuggestedItem,
                    Question = $"If you were to create a new {Resources.SiteName} item, what would it do?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.SuggestedLocation,
                    Question = $"If you were to add a new {Resources.SiteName} location, what would it be?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.ThoughtsOnAds,
                    Question = $"What are your thoughts on {Resources.SiteName} having ads?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.TvWatchedPerDay,
                    Question = $"How many hours of television do you watch daily?",
                    Options = new List<string> { "None", "Under 1", "1 - 2", "3 - 5", "6 - 9", "10 - 12", "13 -  15", "16 - 20", "Over 20" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.NextGoal,
                    Question = $"Are you working towards any goals on {Resources.SiteName}? If yes, which?",
                    Options = null
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.VisitFrequency,
                    Question = $"How often do you visit {Resources.SiteName}?",
                    Options = new List<string> { "Hourly", "Several times a day", "Daily", "Every few days", "Weekly", "Monthly", "Other" }
                },
                new SurveyQuestion
                {
                    Id = SurveyQuestionId.DiscoverySource,
                    Question = $"How did you first learn about {Resources.SiteName}?",
                    Options = null
                }
            };
        }

        private List<SurveyAnswer> UpdateSurveyAnswers(List<SurveyAnswer> surveyAnswers)
        {
            foreach (SurveyAnswer surveyAnswer in surveyAnswers)
            {
                surveyAnswer.Question = _surveyQuestions.Find(x => x.Id == surveyAnswer.SurveyQuestionId).Question;
            }

            return surveyAnswers;
        }
    }
}
