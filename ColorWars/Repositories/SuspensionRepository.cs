﻿using System;
using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SuspensionRepository : ISuspensionRepository
    {
        private ISuspensionPersistence _suspensionPersistence;
        private IUserRepository _userRepository;

        public SuspensionRepository(ISuspensionPersistence suspensionPersistence, IUserRepository userRepository)
        {
            _suspensionPersistence = suspensionPersistence;
            _userRepository = userRepository;
        }

        public List<SuspensionHistory> GetSuspensionHistory(User user, User suspendingUser)
        {
            if (user == null)
            {
                return null;
            }

            if (suspendingUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            return _suspensionPersistence.GetSuspensionHistoryWithUsername(user.Username);
        }

        public bool SuspendUser(User user, User suspendingUser, string reason, DateTime suspendedDate)
        {
            if (user == null || suspendingUser == null || string.IsNullOrEmpty(reason))
            {
                return false;
            }

            if (suspendingUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            user.SuspendedDate = suspendedDate;
            if (_userRepository.UpdateUser(user))
            {
                return _suspensionPersistence.AddSuspensionHistory(user.Username, suspendingUser.Username, reason, suspendedDate);
            }

            return false;
        }

        public bool UnsuspendUser(User user, User suspendingUser, string reason)
        {
            if (user == null || suspendingUser == null || string.IsNullOrEmpty(reason))
            {
                return false;
            }

            if (suspendingUser.Role < UserRole.Helper)
            {
                throw new Exception(Resources.InvalidPermissions);
            }

            if (user.SuspendedDate <= DateTime.UtcNow)
            {
                return false;
            }

            user.SuspendedDate = DateTime.UtcNow;
            if (_userRepository.UpdateUser(user))
            {
                return _suspensionPersistence.AddSuspensionHistory(user.Username, suspendingUser.Username, reason, DateTime.UtcNow);
            }

            return false;
        }
    }
}
