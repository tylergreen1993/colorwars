﻿using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class BadgeRepository : IBadgeRepository
    {
        private List<Badge> _badges;
        private IColorWarsRepository _colorWarsRepository;

        public BadgeRepository(IColorWarsRepository colorWarsRepository)
        {
            LoadBadges();
            _colorWarsRepository = colorWarsRepository;
        }

        public Badge GetActiveBadge(BadgeType badgeType)
        {
            return _badges.Find(x => x.Type == badgeType);
        }

        public List<Badge> GetAvailableBadges(User user, Settings settings, List<Accomplishment> accomplishments, bool isClub)
        {
            List<Badge> availableBadges = new List<Badge>();
            availableBadges.Add(new Badge { Type = BadgeType.None });

            if (user.Role == UserRole.Helper)
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Helper));
            }
            if (user.Role > UserRole.Helper)
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Staff));
            }
            if (isClub)
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Club));
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.SponsorSocietyDonor))
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Donor));
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.ReferredUser))
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Ambassador));
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.CommunityHelper))
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Contributor));
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.ColorWarsContribute))
            {
                if (_colorWarsRepository.GetTeamColor(user) == ColorWarsColor.Orange)
                {
                    availableBadges.Add(_badges.Find(x => x.Type == BadgeType.OrangeTeam));
                }
                else
                {
                    availableBadges.Add(_badges.Find(x => x.Type == BadgeType.BlueTeam));
                }
            }
            if (settings.BeyondWorldPath != BeyondWorldPath.None)
            {
                if (settings.BeyondWorldPath == BeyondWorldPath.Honor)
                {
                    availableBadges.Add(_badges.Find(x => x.Type == BadgeType.BeyondHonor));
                }
                else
                {
                    availableBadges.Add(_badges.Find(x => x.Type == BadgeType.BeyondGreed));
                }
            }
            if (accomplishments.Any(x => x.Id == AccomplishmentId.ClubhouseMember))
            {
                availableBadges.Add(_badges.Find(x => x.Type == BadgeType.Clubhouse));
            }

            return availableBadges;
        }

        private void LoadBadges()
        {
            _badges = new List<Badge>
            {
                new Badge
                {
                    Type = BadgeType.Staff,
                    ShortName = "Staff",
                    Label = "staff",
                    Icon = "fas fa-star"
                },
                new Badge
                {
                    Type = BadgeType.Helper,
                    ShortName = "Helper",
                    Label = "helper",
                    Icon = "fas fa-star"
                },
                new Badge
                {
                    Type = BadgeType.Club,
                    ShortName = "Club",
                    Label = "club",
                    Icon = "fas fa-shield-alt"
                },
                new Badge
                {
                    Type = BadgeType.Donor,
                    ShortName = "Sponsor",
                    Label = "donor",
                    Icon = "fas fa-gift"
                },
                new Badge
                {
                    Type = BadgeType.Ambassador,
                    ShortName = "Ambassador",
                    Label = "ambassador",
                    Icon = "fas fa-sync-alt"
                },
                new Badge
                {
                    Type = BadgeType.BeyondHonor,
                    ShortName = "Beyond Honor",
                    Label = "gold",
                    Icon = "fas fa-hat-wizard"
                },
                new Badge
                {
                    Type = BadgeType.BeyondGreed,
                    ShortName = "Beyond Greed",
                    Label = "purple",
                    Icon = "fas fa-wallet"
                },
                new Badge
                {
                    Type = BadgeType.Contributor,
                    ShortName = "Contributor",
                    Label = "contributor",
                    Icon = "fas fa-hands-helping"
                },
                new Badge
                {
                    Type = BadgeType.OrangeTeam,
                    ShortName = "Orange Team",
                    Label = "orange",
                    Icon = "fas fa-square"
                },
                new Badge
                {
                    Type = BadgeType.BlueTeam,
                    ShortName = "Blue Team",
                    Label = "primary",
                    Icon = "fas fa-square"
                },
                new Badge
                {
                    Type = BadgeType.Clubhouse,
                    ShortName = "Clubhouse",
                    Label = "elite",
                    Icon = "fas fa-coins"
                }
            };
        }
    }
}
