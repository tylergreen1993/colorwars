﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class TowerRepository : ITowerRepository
    {
        private ITowerPersistence _towerPersistence;

        public TowerRepository(ITowerPersistence towerPersistence)
        {
            _towerPersistence = towerPersistence;
        }

        public List<TowerRanking> GetTowerRankings(int days)
        {
            return _towerPersistence.GetTowerRankingsInPastDays(days);
        }

        public List<TowerRanking> GetTopTowerRankings()
        {
            return _towerPersistence.GetTopTowerRankings();
        }

        public bool AddTowerRanking(User user, int topFloorLevel)
        {
            if (user == null || topFloorLevel <= 0)
            {
                return false;
            }

            return _towerPersistence.AddTowerRanking(user.Username, topFloorLevel);
        }
    }
}
