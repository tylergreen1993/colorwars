﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Repositories
{
    public class AuctionsRepository : IAuctionsRepository
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IAuctionsPersistence _auctionsPersistence;
        private IEmailRepository _emailRepository;
        private IUserRepository _userRepository;
        private INotificationsRepository _notificationsRepository;
        private IAdminHistoryRepository _adminHistoryRepository;

        public AuctionsRepository(IHttpContextAccessor httpContextAccessor, IAuctionsPersistence auctionsPersistence, IEmailRepository emailRepository,
        IUserRepository userRepository, INotificationsRepository notificationsRepository, IAdminHistoryRepository adminHistoryRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _auctionsPersistence = auctionsPersistence;
            _emailRepository = emailRepository;
            _userRepository = userRepository;
            _notificationsRepository = notificationsRepository;
            _adminHistoryRepository = adminHistoryRepository;
        }

        public List<Auction> GetAllAuctions(string query = "", bool shouldMatchExactSearch = false, ItemCategory category = ItemCategory.All, ItemCondition condition = ItemCondition.All)
        {
            List<Auction> auctions;
            if (string.IsNullOrEmpty(query))
            {
                auctions = _auctionsPersistence.GetAuctions();
            }
            else
            {
                query = query.Replace("%", "[%]");
                ItemTheme searchTheme = ItemTheme.Default;
                if (query.Contains("-"))
                {
                    IEnumerable<ItemTheme> themes = Enum.GetValues(typeof(ItemTheme)).Cast<ItemTheme>();
                    foreach (ItemTheme theme in themes)
                    {
                        if (query.ToLower().Contains(theme.ToString().ToLower()))
                        {
                            query = query.Split("-").First().Trim();
                            searchTheme = theme;
                            break;
                        }
                    }
                }

                auctions = _auctionsPersistence.GetAuctionsWithSearch(query, searchTheme);
            }

            foreach (Auction auction in auctions)
            {
                UpdateAuctionDetails(auction, false);
                if (shouldMatchExactSearch)
                {
                    query = query.Replace("[%]", "%");
                    auctions = auctions.FindAll(x => x.AuctionItem.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase));
                }
                if (category != ItemCategory.All)
                {
                    auctions = auctions.FindAll(x => x.AuctionItem.Category == category);
                }
                if (condition != ItemCondition.All)
                {
                    auctions = auctions.FindAll(x => x.AuctionItem.Condition == condition);
                }
            }

            return auctions;
        }

        public List<Auction> GetPastAuctions()
        {
            List<Auction> auctions = _auctionsPersistence.GetPastAuctions();
            foreach (Auction auction in auctions)
            {
                UpdateAuctionDetails(auction, false);
            }

            return auctions;
        }

        public List<Auction> GetAuctions(User user, bool onlyActive = false, bool onlyUncollected = false, bool getBidDetails = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Auction> auctions = _auctionsPersistence.GetAuctionsWithUsername(user.Username);

            if (onlyActive)
            {
                auctions.RemoveAll(x => x.IsFinished());
            }
            if (onlyUncollected)
            {
                auctions.RemoveAll(x => (x.Username == user.Username && x.CreatorCollected) || (x.Username != user.Username && x.WinnerCollected) || (x.IsFinished() && (x.BidCount == 0 || (x.Username != user.Username && x.TopBidder != user.Username))));
            }
            foreach (Auction auction in auctions)
            {
                UpdateAuctionDetails(auction, getBidDetails);
            }

            return auctions;
        }

        public Auction GetAuction(int auctionId)
        {
            Auction auction = _auctionsPersistence.GetAuctionWithAuctionId(auctionId);
            if (auction != null)
            {
                UpdateAuctionDetails(auction);
            }
            return auction;
        }

        public Item GetItemWithAuctionId(int auctionId)
        {
            return _auctionsPersistence.GetAuctionItemWithAuctionId(auctionId);
        }

        public bool AddBid(User user, Auction auction, int points)
        {
            if (user == null || auction == null || points <= 0)
            {
                return false;
            }

            int lastBid = auction.Bids.FirstOrDefault()?.Points ?? auction.AuctionItem.Points;
            int differenceAmount = points - lastBid;
            if ((double)points / auction.AuctionItem.Points >= 1.5 && differenceAmount >= 5000)
            {
                User auctionUser = _userRepository.GetUser(auction.Username);
                if (auctionUser != null)
                {
                    _adminHistoryRepository.AddTransferHistory(auction.Id, TransferType.Auction, user, auctionUser, differenceAmount);
                }
            }

            if (auction.HighestBidder == user.Username)
            {
                points += auction.HighestBid;
            }

            return _auctionsPersistence.AddBid(auction.Id, user.Username, points);
        }

        public bool AddAuction(User user, int points, int minBidIncrement, Item item, DateTime endDate, out Auction auction)
        {
            auction = null;
            if (user == null || item == null || points <= 0 || minBidIncrement <= 0)
            {
                return false;
            }
            if (_auctionsPersistence.AddAuctionItem(item))
            {
                auction = _auctionsPersistence.AddAuction(user.Username, points, minBidIncrement, item.SelectionId, endDate);
                if (auction == null)
                {
                    Messages.AddMessage("Your auction couldn't be created. Please try again.");
                    return false;
                }
                HttpContext httpContext = _httpContextAccessor.HttpContext;
                Task result = SendAuctionEmail(auction, Helper.GetDomain(httpContext));
                return true;
            }
            return false;
        }

        public bool UpdateAuction(Auction auction)
        {
            if (auction == null)
            {
                return false;
            }

            return _auctionsPersistence.UpdateAuction(auction.Id, auction.StartPoints, auction.CreatorCollected, auction.WinnerCollected, auction.SentNotification, auction.EndDate);
        }

        private void UpdateAuctionDetails(Auction auction, bool getBidDetails = true)
        {
            auction.AuctionItem = GetItemWithAuctionId(auction.Id);
            if (getBidDetails)
            {
                auction.Bids = _auctionsPersistence.GetBidsWithAuctionId(auction.Id);
            }
        }

        private async Task SendAuctionEmail(Auction auction, string domain)
        {
            TimeSpan timeUntilAuctionEnds = auction.EndDate - DateTime.UtcNow;
            await Task.Delay(timeUntilAuctionEnds);

            Auction completedAuction = GetAuction(auction.Id);
            if (completedAuction == null || completedAuction.SentNotification || !completedAuction.IsFinished())
            {
                return;
            }
            bool anyBids = completedAuction.Bids.Count > 0;
            string url = $"/Auction?id={completedAuction.Id}";
            if (!completedAuction.CreatorCollected)
            {
                string subject = "Your auction has finished!";
                string title = $"Auction Finished";
                string body = $"Your auction has finished! You can click the button below to see the auction or click <a href='{domain}/Auction/Collection'>here</a> to collect your earnings!";

                User user = _userRepository.GetUser(auction.Username);
                if (user != null)
                {
                    Task result = _emailRepository.SendEmail(user, subject, title, body, url, false, domain);
                }

                _notificationsRepository.AddNotification(user, "Your auction has finished!", NotificationLocation.Auction, url);
            }
            System.Threading.Thread.Sleep(2500);
            if (!completedAuction.WinnerCollected && anyBids)
            {
                string subject = "You won an auction!";
                string title = $"Auction Winner";
                string body = $"Congratulations! You won an auction! You can click the button below to see the auction or click <a href='{domain}/Auction/Collection'>here</a> to collect your item!";

                User user = _userRepository.GetUser(completedAuction.Bids.First().Username);
                if (user != null)
                {
                    Task result = _emailRepository.SendEmail(user, subject, title, body, url, false, domain);

                    _notificationsRepository.AddNotification(user, "Your won an auction!", NotificationLocation.Auction, url);
                }
            }
            completedAuction.SentNotification = true;
            UpdateAuction(completedAuction);
        }
    }
}
