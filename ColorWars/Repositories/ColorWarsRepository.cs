﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ColorWarsRepository : IColorWarsRepository
    {
        private IColorWarsPersistence _colorWarsPersistence;
        private IUserRepository _userRepository;
        private INotificationsRepository _notificationsRepository;

        public ColorWarsRepository(IColorWarsPersistence colorWarsPersistence, IUserRepository userRepository, INotificationsRepository notificationsRepository)
        {
            _colorWarsPersistence = colorWarsPersistence;
            _userRepository = userRepository;
            _notificationsRepository = notificationsRepository;
        }

        public List<ColorWarsSubmission> GetColorWarsSubmissions(ColorWarsColor teamColor)
        {
            if (teamColor == ColorWarsColor.None)
            {
                return null;
            }

            return _colorWarsPersistence.GetAllColorWarsSubmissionsWithTeamColor(teamColor);
        }

        public List<ColorWarsSubmission> GetColorWarsSubmissions(User user)
        {
            if (user == null)
            {
                return null;
            }

            return _colorWarsPersistence.GetAllColorWarsSubmissionsWithUsername(user.Username);
        }

        public List<ColorWarsHistory> GetColorWarsHistory()
        {
            return _colorWarsPersistence.GetAllColorWarsHistory();
        }

        public bool AddColorWarsSubmission(User user, int amount, ColorWarsColor teamColor)
        {
            if (user == null || amount <= 0 || teamColor == ColorWarsColor.None)
            {
                return false;
            }

            return _colorWarsPersistence.AddColorWarsSubmission(user.Username, amount, teamColor);
        }

        public bool AddColorWarsHistory(ColorWarsColor teamColor, int amount)
        {
            if (teamColor == ColorWarsColor.None || amount <= 0)
            {
                return false;
            }

            return _colorWarsPersistence.AddColorWarsHistory(teamColor, amount);
        }

        public bool CompleteColorWars(User user, out int amountEarned)
        {
            amountEarned = 0;

            if (user == null)
            {
                return false;
            }

            List<ColorWarsSubmission> colorWarsSubmissionsBlue = GetColorWarsSubmissions(ColorWarsColor.Blue);
            List<ColorWarsSubmission> colorWarsSubmissionsOrange = GetColorWarsSubmissions(ColorWarsColor.Orange);

            int colorWarsSubmissionsEarningsBlue = colorWarsSubmissionsBlue.Sum(x => x.Amount);
            int colorWarsSubmissionsEarningsOrange = colorWarsSubmissionsOrange.Sum(x => x.Amount);

            if (colorWarsSubmissionsEarningsBlue == colorWarsSubmissionsEarningsOrange)
            {
                return false;
            }

            ColorWarsColor winningTeamColor;
            List<string> winningUsernames = new List<string>();
            List<string> losingUsernames = new List<string>();
            if (colorWarsSubmissionsEarningsBlue > colorWarsSubmissionsEarningsOrange)
            {
                winningTeamColor = ColorWarsColor.Blue;
                winningUsernames = colorWarsSubmissionsBlue.Select(x => x.Username).Distinct().ToList();
                losingUsernames = colorWarsSubmissionsOrange.Select(x => x.Username).Distinct().ToList();
            }
            else
            {
                winningTeamColor = ColorWarsColor.Orange;
                winningUsernames = colorWarsSubmissionsOrange.Select(x => x.Username).Distinct().ToList();
                losingUsernames = colorWarsSubmissionsBlue.Select(x => x.Username).Distinct().ToList();
            }

            amountEarned = (int)Math.Round((double)(colorWarsSubmissionsEarningsBlue + colorWarsSubmissionsEarningsOrange) / winningUsernames.Count);

            if (_colorWarsPersistence.CompleteColorWars(winningTeamColor, amountEarned))
            {
                int amountReceived = amountEarned;
                string domain = Helper.GetDomain();
                Task.Run(() =>
                {
                    foreach (string username in winningUsernames)
                    {
                        if (username == user.Username)
                        {
                            continue;
                        }

                        User winningUser = _userRepository.GetUser(username);
                        if (winningUser != null)
                        {
                            _notificationsRepository.AddNotification(winningUser, $"Congratulations! Your team won Color Wars thanks to @{user.Username} and earned you {Helper.GetFormattedPoints(amountReceived)}!", NotificationLocation.ColorWars, "/ColorWars", domain: domain);
                        }
                    }
                    foreach (string username in losingUsernames)
                    {
                        User losingUser = _userRepository.GetUser(username);
                        if (losingUser != null)
                        {
                            _notificationsRepository.AddNotification(losingUser, $"Your team unfortunately lost Color Wars to the {winningTeamColor} Team! Help your team out in the new game!", NotificationLocation.ColorWars, "/ColorWars", domain: domain);
                        }
                    }
                });

                return true;
            }

            return false;
        }

        public ColorWarsColor GetTeamColor(User user)
        {
            if (user.Id % 2 == 0)
            {
                return ColorWarsColor.Orange;
            }

            return ColorWarsColor.Blue;
        }
    }
}
