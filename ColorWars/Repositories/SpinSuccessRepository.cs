﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class SpinSuccessRepository : ISpinSuccessRepository
    {
        private ISpinSuccessPersistence _spinSuccessPersistence;

        public SpinSuccessRepository(ISpinSuccessPersistence spinSuccessPersistence)
        {
            _spinSuccessPersistence = spinSuccessPersistence;
        }

        public List<SpinSuccessHistory> GetSpinSuccessHistory(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _spinSuccessPersistence.GetSpinSuccessHistoryWithUsername(user.Username, isCached);
        }

        public bool AddSpinSuccessHistory(User user, SpinSuccessType type, string message)
        {
            if (user == null || string.IsNullOrEmpty(message))
            {
                return false;
            }

            return _spinSuccessPersistence.AddSpinSuccessHistory(user.Username, type, message);
        }
    }
}
