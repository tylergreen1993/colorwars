﻿using System;
using System.Collections.Generic;
using ColorWars.Classes;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ShowcaseRepository : IShowcaseRepository
    {
        private IShowcasePersistence _showcasePersistence;
        private IItemsRepository _itemsRepository;

        public ShowcaseRepository(IShowcasePersistence showcasePersistence, IItemsRepository itemsRepository)
        {
            _showcasePersistence = showcasePersistence;
            _itemsRepository = itemsRepository;
        }

        public Item GetShowcaseItem()
        {
            Item showcaseItem = _showcasePersistence.GetTimedShowcase();
            if (showcaseItem != null && showcaseItem.GetAgeInMinutes() >= Helper.GetShowcaseRefreshMinutes())
            {
                return _showcasePersistence.GetTimedShowcase(true);
            }
            return showcaseItem;
        }

        public Item GetAgerItem(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _showcasePersistence.GetAgerItemWithUsername(user.Username, isCached);
        }

        public Item SearchShowcaseItem(string query = "", bool matchExactSearch = false, ItemCondition condition = ItemCondition.All, ItemCategory category = ItemCategory.All)
        {
            Item showcaseItem = GenerateShowcaseItem(false);
            if (!string.IsNullOrEmpty(query))
            {
                showcaseItem = matchExactSearch ? showcaseItem.Name.Equals(query, StringComparison.InvariantCultureIgnoreCase) ? showcaseItem : null : showcaseItem.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase) ? showcaseItem : null;
            }

            if (category != ItemCategory.All)
            {
                showcaseItem = showcaseItem?.Category == category ? showcaseItem : null;
            }
            if (condition != ItemCondition.All)
            {
                showcaseItem = showcaseItem?.Condition == condition ? showcaseItem : null;
            }

            return showcaseItem;
        }

        public Item GenerateShowcaseItem(bool hasBiggerDiscount)
        {
            Item showcaseItem = GetShowcaseItem();
            if (showcaseItem == null || showcaseItem.GetAgeInMinutes() >= Helper.GetShowcaseRefreshMinutes())
            {
                if (showcaseItem != null)
                {
                    DeleteShowcaseItem();
                }

                Random rnd = new Random();
                ItemCategory[] excludedItems = { ItemCategory.Egg, ItemCategory.Frozen, ItemCategory.Companion, ItemCategory.Seed };
                List<Item> allItems = _itemsRepository.GetAllItems(ItemCategory.All, excludedItems).FindAll(x => x.Points >= 20000);
                showcaseItem = allItems[rnd.Next(allItems.Count)];
                AddShowcaseItem(showcaseItem);
            }

            int reducedCost = (int)Math.Round(showcaseItem.Points * (hasBiggerDiscount ? 0.7 : 0.75));
            showcaseItem.UpdatePoints(reducedCost);
            return showcaseItem;
        }

        public bool AddShowcaseItem(Item item)
        {
            if (item == null)
            {
                return false;
            }
            return _showcasePersistence.AddTimedShowcase(item.Id);
        }

        public bool AddAgerItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            item.Username = user.Username;

            return _showcasePersistence.AddAgerItem(item);
        }

        public bool DeleteShowcaseItem()
        {
            return _showcasePersistence.DeleteTimedShowcase();
        }


        public bool DeleteAgerItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _showcasePersistence.DeleteAgerItemWithSelectionId(item.SelectionId);
        }
    }
}
