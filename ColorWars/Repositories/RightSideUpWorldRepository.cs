﻿using System.Collections.Generic;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class RightSideUpWorldRepository : IRightSideUpWorldRepository
    {
        private IRightSideUpWorldPersistence _rightSideUpWorldPersistence;

        public RightSideUpWorldRepository(IRightSideUpWorldPersistence rightSideUpWorldPersistence)
        {
            _rightSideUpWorldPersistence = rightSideUpWorldPersistence;
        }

        public List<HallOfFameEntry> GetRightSideUpGuardHallOfFame()
        {
            return _rightSideUpWorldPersistence.GetRightSideUpGuardHallOfFame();
        }

        public bool AddRightSideUpGuardHallOfFame(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _rightSideUpWorldPersistence.AddRightSideUpGuardHallOfFame(user.Username);
        }
    }
}
