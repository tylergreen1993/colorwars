﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;

namespace ColorWars.Repositories
{
    public class EffectsRepository : IEffectsRepository
    {
        private ISettingsRepository _settingsRepository;

        public EffectsRepository(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        public List<Effect> GetActiveEffects(User user, bool isCached = true)
        {
            List<Effect> activeEffects = new List<Effect>();
            Settings settings = _settingsRepository.GetSettings(user, isCached);

            if (settings.CandyExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Candy Card Bonus",
                    Amount = settings.CandyPercent,
                    ExpiryDate = settings.CandyExpiryDate,
                    IsPercent = true,
                    Type = settings.CandyPercent > 0 ? EffectType.Positive : EffectType.Negative,
                    Url = "/Stadium"
                });
            }
            if (settings.StadiumBonusExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Stadium Bonus",
                    Amount = settings.StadiumBonusPercent,
                    ExpiryDate = settings.StadiumBonusExpiryDate,
                    IsPercent = true,
                    Type = EffectType.Positive,
                    Url = "/Stadium"
                });
            }
            if (settings.CurseExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Stadium Curse",
                    Amount = settings.CursePercent,
                    ExpiryDate = settings.CurseExpiryDate,
                    IsPercent = true,
                    Type = EffectType.Negative,
                    Url = "/Stadium"
                });
            }
            if (settings.JunkyardCouponPercent > 0)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Junkyard Coupon Discount",
                    Amount = settings.JunkyardCouponPercent,
                    IsPercent = true,
                    Type = EffectType.Positive,
                    Url = "/Junkyard"
                });
            }
            if (settings.RepairCouponPercent > 0)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Repair Shop Coupon Discount",
                    Amount = settings.RepairCouponPercent,
                    IsPercent = true,
                    Type = EffectType.Positive,
                    Url = "/Repair"
                });
            }
            if (settings.GeneralStoreCouponPercent > 0)
            {
                activeEffects.Add(new Effect
                {
                    Name = "General Store Coupon Discount",
                    Amount = settings.GeneralStoreCouponPercent,
                    IsPercent = true,
                    Type = EffectType.Positive,
                    Url = "/Store"
                });
            }
            if (settings.HasWizardBetterLuck)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Better Young Wizard Luck",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/Wizard"
                });
            }
            if (settings.HasDoubleQuarryHaul)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Double Wishing Stone Quarry Earnings",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/WishingStone/Quarry"
                });
            }
            if (settings.HasDoubleBankInterest)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Bank Interest Reward",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/Bank"
                });
            }
            if (settings.HasDoubleStadiumCPUReward)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Stadium Match Reward",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/Stadium"
                });
            }
            if (settings.StealthProtectExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Stealth Protect",
                    ExpiryDate = settings.StealthProtectExpiryDate,
                    Type = EffectType.Positive
                });
            }
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow && settings.ActiveHoroscope != HoroscopeType.None)
            {
                activeEffects.Add(new Effect
                {
                    Name = $"{settings.ActiveHoroscope} Fortune",
                    ExpiryDate = settings.HoroscopeExpiryDate,
                    Type = settings.ActiveHoroscope == HoroscopeType.Good ? EffectType.Positive : EffectType.Negative
                });
            }
            if (settings.DeepCavernsExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Deep Caverns Light",
                    ExpiryDate = settings.DeepCavernsExpiryDate,
                    Type = EffectType.Positive,
                    Url = "/Caverns"
                });
            }
            if (settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Curse Protection",
                    ExpiryDate = settings.SorceressStadiumCurseProtectExpiryDate,
                    Type = EffectType.Positive
                });
            }
            if (settings.LavenderStopExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Unlimited Enhancer Uses",
                    ExpiryDate = settings.LavenderStopExpiryDate,
                    Type = EffectType.Positive,
                    Url = "/Stadium"
                });
            }
            if (settings.Crystal4thExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Break the Fourth Wall",
                    ExpiryDate = settings.Crystal4thExpiryDate,
                    Type = EffectType.Positive,
                    Url = "/Wall"
                });
            }
            if (settings.HasBetterSwapDeckLuck)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Better Swap Deck Luck",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/SwapDeck"
                });
            }
            if (settings.BagProtectExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Bag Stealth Protection",
                    ExpiryDate = settings.BagProtectExpiryDate,
                    Type = EffectType.Positive,
                    Url = "/Items"
                });
            }
            if (settings.CraftingLuckExpiryDate > DateTime.UtcNow)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Crafting Luck",
                    ExpiryDate = settings.CraftingLuckExpiryDate,
                    Type = EffectType.Positive,
                    Url = "/Crafting"
                });
            }
            if (settings.HasBetterSpinLuck)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Better Spin Luck",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/SpinSuccess"
                });
            }
            if (settings.HasBetterTreasureDiveLuck)
            {
                activeEffects.Add(new Effect
                {
                    Name = "Treasure Dive Hint",
                    Uses = 1,
                    Type = EffectType.Positive,
                    Url = "/TreasureDive"
                });
            }

            return activeEffects.OrderByDescending(x => x.Type).ThenBy(x => x.Name).ToList();
        }

        public void RemoveAllActiveEffects(User user)
        {
            Settings settings = _settingsRepository.GetSettings(user, false);

            if (settings.CandyExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.CandyExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.CurseExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.CurseExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.JunkyardCouponPercent > 0)
            {
                _settingsRepository.SetSetting(user, nameof(settings.JunkyardCouponPercent), 0.ToString());
            }
            if (settings.RepairCouponPercent > 0)
            {
                _settingsRepository.SetSetting(user, nameof(settings.RepairCouponPercent), 0.ToString());
            }
            if (settings.GeneralStoreCouponPercent > 0)
            {
                _settingsRepository.SetSetting(user, nameof(settings.GeneralStoreCouponPercent), 0.ToString());
            }
            if (settings.HasWizardBetterLuck)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasWizardBetterLuck), false.ToString());
            }
            if (settings.HasDoubleQuarryHaul)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleQuarryHaul), false.ToString());
            }
            if (settings.HasDoubleBankInterest)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleBankInterest), false.ToString());
            }
            if (settings.HasDoubleStadiumCPUReward)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasDoubleStadiumCPUReward), false.ToString());
            }
            if (settings.StadiumBonusExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.StadiumBonusExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.StealthProtectExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.StealthProtectExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.HoroscopeExpiryDate > DateTime.UtcNow && settings.ActiveHoroscope != HoroscopeType.None)
            {
                _settingsRepository.SetSetting(user, nameof(settings.ActiveHoroscope), HoroscopeType.None.ToString());
            }
            if (settings.DeepCavernsExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.DeepCavernsExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.SorceressStadiumCurseProtectExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.SorceressStadiumCurseProtectExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.LavenderStopExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.LavenderStopExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.Crystal4thExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.Crystal4thExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.HasBetterSwapDeckLuck)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSwapDeckLuck), false.ToString());
            }
            if (settings.BagProtectExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.BagProtectExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.CraftingLuckExpiryDate > DateTime.UtcNow)
            {
                _settingsRepository.SetSetting(user, nameof(settings.CraftingLuckExpiryDate), DateTime.MinValue.ToString());
            }
            if (settings.HasBetterSpinLuck)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterSpinLuck), false.ToString());
            }
            if (settings.HasBetterTreasureDiveLuck)
            {
                _settingsRepository.SetSetting(user, nameof(settings.HasBetterTreasureDiveLuck), false.ToString());
            }
        }
    }
}
