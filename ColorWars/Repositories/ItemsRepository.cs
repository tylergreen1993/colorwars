﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class ItemsRepository : IItemsRepository
    {
        private IItemsPersistence _itemsPersistence;
        private ISettingsRepository _settingsRepository;
        private IUserRepository _userRepository;

        public ItemsRepository(IItemsPersistence itemsPersistence, ISettingsRepository settingsRepository, IUserRepository userRepository)
        {
            _itemsPersistence = itemsPersistence;
            _settingsRepository = settingsRepository;
            _userRepository = userRepository;
        }

        public List<Item> GetItems(User user, bool onlySellable = false, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Item> items = _itemsPersistence.GetAllItemsWithUsername(user.Username, isCached);
            if (items.Count > 0)
            {
                Settings settings = _settingsRepository.GetSettings(user, isCached);
                Item[] itemsArray = items.ToArray();
                SortItems(itemsArray, settings.ItemSort);
                items = itemsArray.ToList();
            }

            if (onlySellable)
            {
                return items.FindAll(x => x.DeckType == DeckType.None && x.Condition != ItemCondition.Unusable);
            }

            return items;
        }

        public List<Card> GetCards(User user, bool onlyInDeck = false, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            Settings settings = _settingsRepository.GetSettings(user, isCached);

            List<Card> cards = _itemsPersistence.GetAllCardsWithUsername(user.Username, isCached);
            if (cards.Count > 0)
            {
                Card[] cardsArray = cards.ToArray();
                SortItems(cardsArray, settings.ItemSort);
                cards = cardsArray.ToList();
            }

            if (onlyInDeck)
            {
                return cards?.FindAll(x => x.DeckType == settings.DefaultDeckType);
            }

            return cards.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Enhancer> GetEnhancers(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            List<Enhancer> enhancers = _itemsPersistence.GetAllEnhancersWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
            if (enhancers.Count > 0)
            {
                Settings settings = _settingsRepository.GetSettings(user, isCached);
                Enhancer[] enhancersArray = enhancers.ToArray();
                SortItems(enhancersArray, settings.ItemSort);
                enhancers = enhancersArray.ToList();
            }

            return enhancers;
        }

        public List<CardPack> GetCardPacks(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllCardPacksWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Crafting> GetCrafting(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllCraftingWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Candy> GetCandy(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllCandyWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Coupon> GetCoupons(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllCouponsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Egg> GetEggCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllEggCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<LibraryCard> GetLibraryCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllLibraryCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Theme> GetThemeCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllThemesWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Sleeve> GetSleeves(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllSleevesWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Upgrader> GetUpgraders(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllUpgradersWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Jewel> GetJewels(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllJewelsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Stealth> GetStealthCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllStealthCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Frozen> GetFrozenItems(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllFrozenItemsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<CompanionItem> GetCompanions(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllCompanionsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Seed> GetSeedCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllSeedCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Art> GetArtCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllArtCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Fairy> GetFairyCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllFairyCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Beyond> GetBeyondCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllBeyondCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<KeyCard> GetKeyCards(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _itemsPersistence.GetAllKeyCardsWithUsername(user.Username, isCached)?.FindAll(x => x.Condition != ItemCondition.Unusable);
        }

        public List<Item> GetAllItems(ItemCategory onlyItemCategory = ItemCategory.All, ItemCategory[] excludeItems = null, bool includeExclusiveItems = false)
        {
            List<Item> allItems = _itemsPersistence.GetAllItems();
            if (!includeExclusiveItems)
            {
                allItems = allItems.FindAll(x => !x.IsExclusive);
            }

            if (onlyItemCategory != ItemCategory.All)
            {
                return allItems.FindAll(x => x.Category == onlyItemCategory);
            }

            if (excludeItems == null)
            {
                return allItems;
            }

            foreach (ItemCategory itemCategory in excludeItems)
            {
                allItems.RemoveAll(x => x.Category == itemCategory);
            }

            return allItems;
        }

        public List<Card> GetAllCards(bool includeExclusiveItems = false)
        {
            List<Card> allCards = _itemsPersistence.GetAllCards();
            if (!includeExclusiveItems)
            {
                allCards = allCards.FindAll(x => !x.IsExclusive);
            }

            return allCards;

        }

        public List<Enhancer> GetAllEnhancers(bool includeExclusiveItems = false)
        {
            List<Enhancer> allEnhancers = _itemsPersistence.GetAllEnhancers();
            if (!includeExclusiveItems)
            {
                allEnhancers = allEnhancers.FindAll(x => !x.IsExclusive);
            }

            return allEnhancers;
        }

        public List<Candy> GetAllCandyCards(bool includeExclusiveItems = false)
        {
            List<Candy> allCandyCards = _itemsPersistence.GetAllCandyCards();
            if (!includeExclusiveItems)
            {
                allCandyCards = allCandyCards.FindAll(x => !x.IsExclusive);
            }

            return allCandyCards;
        }

        public List<Item> GetTopOldestItems(bool isCached = true)
        {
            return _itemsPersistence.GetTopOldestItems(isCached);
        }

        public List<Item> GetTopLongestOwnedItems(bool isCached = true)
        {
            return _itemsPersistence.GetTopLongestOwnedItems(isCached);
        }

        public bool AddItem(User user, Item item, bool isUser = true)
        {
            if (user == null || item == null)
            {
                return false;
            }

            if (item.Uses > 0)
            {
                item.TotalUses = item.Uses;
            }

            if (item.Theme != ItemTheme.Default && item.Category != ItemCategory.Deck)
            {
                throw new Exception($"A theme cannot be added to a non-{Resources.DeckCard}.");
            }

            if (_itemsPersistence.AddItemForUsername(user.Username, item))
            {
                if (!isUser)
                {
                    user.FlushSession = true;
                    _userRepository.UpdateUser(user);
                }

                return true;
            }

            return false;
        }

        public bool RemoveItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            return _itemsPersistence.RemoveItemForUsername(user.Username, item);
        }

        public bool UpdateItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            if (item.Theme != ItemTheme.Default && item.Category != ItemCategory.Deck)
            {
                throw new Exception($"A theme cannot be added to a non-{Resources.DeckCard}.");
            }

            return _itemsPersistence.UpdateItemForSelectionId(user.Username, item);
        }

        public bool RepairAllItems(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _itemsPersistence.RepairAllItems(user.Username);
        }

        public bool RemoveUsedItems(User user)
        {
            if (user == null)
            {
                return false;
            }

            return _itemsPersistence.RemoveUsedItems(user.Username);
        }

        public Card SwapCard(User user, Card card, int amount)
        {
            int newAmount = card.Amount + amount;

            List<Card> allCards = GetAllCards();
            Card newCard = allCards.Find(x => x.Amount == newAmount);
            if (newCard == null)
            {
                return null;
            }

            newCard.CreatedDate = card.CreatedDate;
            newCard.RepairedDate = card.RepairedDate;
            newCard.Theme = card.Theme;
            newCard.DeckType = card.DeckType;

            if (RemoveItem(user, card))
            {
                if (AddItem(user, newCard))
                {
                    return GetCards(user).Find(x => x.Id == newCard.Id && x.CreatedDate == newCard.CreatedDate);
                }
            }

            return null;
        }

        private void SortItems(Item[] items, ItemSort sort)
        {
            if (sort == ItemSort.HighestValue)
            {
                Array.Sort(items, (a, b) =>
                {
                    var comparison = b.Points.CompareTo(a.Points);
                    return comparison == 0 ? b.Category.CompareTo(a.Category) : comparison;
                });
            }
            else if (sort == ItemSort.LowestValue)
            {
                Array.Sort(items, (a, b) =>
                {
                    var comparison = a.Points.CompareTo(b.Points);
                    return comparison == 0 ? b.Category.CompareTo(a.Category) : comparison;
                });
                items = items.OrderBy(x => x.Points).ThenByDescending(x => x.Category).ToArray();
            }
            else if (sort == ItemSort.Newest)
            {
                Array.Sort(items, (a, b) =>
                {
                    var comparison = b.AddedDate.CompareTo(a.AddedDate);
                    return comparison == 0 ? b.Category.CompareTo(a.Category) : comparison;
                });
            }
            else if (sort == ItemSort.Category)
            {
                Array.Sort(items, (a, b) =>
                {
                    var comparison = a.Category.CompareTo(b.Category);
                    return comparison == 0 ? a.Points.CompareTo(b.Points) : comparison;
                });
            }
            else if (sort == ItemSort.Name)
            {
                Array.Sort(items, (a, b) =>
                {
                    var comparison = string.Compare(a.Name, b.Name, StringComparison.InvariantCultureIgnoreCase);
                    return comparison == 0 ? a.Points.CompareTo(b.Points) : comparison;
                });
            }
        }
    }
}
