﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class BlockedRepository : IBlockedRepository
    {
        private IBlockedPersistence _blockedPersistence;
        private IUserRepository _userRepository;

        public BlockedRepository(IBlockedPersistence blockedPersistence, IUserRepository userRepository)
        {
            _blockedPersistence = blockedPersistence;
            _userRepository = userRepository;
        }

        public List<Blocked> GetBlocked(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _blockedPersistence.GetBlockedWithUsername(user.Username, isCached);
        }

        public bool AddBlocked(User user, User blockedUser)
        {
            if (user == null || blockedUser == null)
            {
                return false;
            }

            return _blockedPersistence.AddBlocked(user.Username, blockedUser.Username);
        }

        public bool DeleteFromBlocked(User user, User blockedUser)
        {
            if (user == null || blockedUser == null)
            {
                return false;
            }

            if (_blockedPersistence.DeleteFromBlocked(user.Username, blockedUser.Username))
            {
                Task.Run(() =>
                {
                    blockedUser.FlushSession = true;
                    _userRepository.UpdateUser(blockedUser);

                });

                return true;
            }

            return false;
        }

        public bool HasBlockedUser(User user, User blockedUser, bool isCached = true)
        {
            if (user == null || blockedUser == null)
            {
                return false;
            }

            List<Blocked> blockedUsers = GetBlocked(user, isCached);
            return blockedUsers.Any(x => x.BlockedUser == blockedUser.Username);
        }
    }
}
