﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorWars.Models;
using ColorWars.Persistences;

namespace ColorWars.Repositories
{
    public class CavernsRepository : ICavernsRepository
    {
        private ICavernsPersistence _cavernsPersistence;
        private IItemsRepository _itemsRepository;
        private List<SorceressEffect> _sorceressEffects;

        public CavernsRepository(ICavernsPersistence cavernsPersistence, IItemsRepository itemsRepository)
        {
            _cavernsPersistence = cavernsPersistence;
            _itemsRepository = itemsRepository;
            _sorceressEffects = new List<SorceressEffect>();
            PopulateEffects();
        }

        public List<SorceressEffect> GetAllSorceressEffects(Settings settings)
        {
            List<SorceressEffect> effects = _sorceressEffects.OrderBy(x => x.Points).ThenBy(x => x.Name).ToList();
            if (settings.HasLearntTranslation || settings.PhantomDoorLevel == 0)
            {
                effects.RemoveAll(x => x.Id == SorceressEffectId.LearnTranslation);
            }

            return effects;
        }

        public Item GetPlantedItem(User user, bool isCached = true)
        {
            if (user == null)
            {
                return null;
            }

            return _cavernsPersistence.GetPlantedItemWithUsername(user.Username, isCached);
        }

        public bool AddPlantedItem(User user, Item item)
        {
            if (user == null || item == null)
            {
                return false;
            }

            return _cavernsPersistence.AddPlantedItem(item.SelectionId, item.Id, user.Username, item.Uses, item.Theme, item.CreatedDate, item.RepairedDate);
        }

        public bool DeletePlantedItem(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return _cavernsPersistence.DeletePlantedItemWithSelectionId(item.SelectionId);
        }

        public Item GetItem(User user, Item item, CardColor color)
        {
            if (item.Category != ItemCategory.Seed)
            {
                return item;
            }

            Random rnd = new Random();
            Item harvestedItem;

            if (rnd.Next(100) == 1)
            {
                harvestedItem = _itemsRepository.GetAllItems(ItemCategory.Art, null, true).Find(x => x.IsExclusive && x.Name.Equals("Splatter", StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                List<Card> cards = _itemsRepository.GetAllCards().FindAll(x => x.Color == color);
                harvestedItem = cards[rnd.Next(cards.Count)];

                if (rnd.Next(50) == 1)
                {
                    ItemTheme[] themes = { ItemTheme.Outline, ItemTheme.Striped, ItemTheme.Retro, ItemTheme.Squared, ItemTheme.Neon, ItemTheme.Astral };
                    ItemTheme theme = themes[rnd.Next(themes.Length)];
                    harvestedItem.Theme = theme;
                }
            }

            harvestedItem.CreatedDate = item.CreatedDate;
            harvestedItem.RepairedDate = item.RepairedDate;

            if (DeletePlantedItem(item))
            {
                AddPlantedItem(user, harvestedItem);
            }

            return harvestedItem;
        }

        private void PopulateEffects()
        {
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.PositiveHoroscope,
                Name = $"Good Fortune",
                Description = $"Turn your negative horoscope into a positive one.",
                ImageUrl = "Sorceress/Yellow_Effect.png",
                Points = 1000
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.CompanionHappiness,
                Name = $"Happy Companions",
                Description = $"Restore all your Companions to 100% Happiness.",
                ImageUrl = "Sorceress/Blue_Effect.png",
                Points = 3500
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.CurseUser,
                Name = $"Stadium Curse",
                Description = $"Curse another user at the Stadium for CPU matches for the next 10 minutes.",
                ImageUrl = "Sorceress/Purple_Effect.png",
                Points = 5000
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.ProtectUser,
                Name = $"Curse Protection",
                Description = $"Protect yourself for a week from the Sorceress' Stadium Curse.",
                ImageUrl = "Sorceress/Red_Effect.png",
                Points = 250
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.RenameCompanion,
                Name = $"Rename Companion",
                Description = $"Rename one of your Companions.",
                ImageUrl = "Sorceress/Orange_Effect.png",
                Points = 250
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.CrystalBall,
                Name = $"Crystal Ball",
                Description = $"Visit the Crystal Ball again if you already have recently.",
                ImageUrl = "Sorceress/Green_Effect.png",
                Points = 1000
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.ReduceFurnace,
                Name = $"Reduce Furnace",
                Description = $"Reduce the waiting time for an item currently in the Furnace by 1 day.",
                ImageUrl = "Sorceress/Pink_Effect.png",
                Points = 7500
            });
            _sorceressEffects.Add(new SorceressEffect
            {
                Id = SorceressEffectId.LearnTranslation,
                Name = $"Learn Translation",
                Description = $"Learn how to read notes from strange, ethereal creatures.",
                ImageUrl = "Sorceress/Smoke_Effect.png",
                Points = 5000
            });
        }
    }
}
