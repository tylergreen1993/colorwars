﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class TourneyPersistence : ITourneyPersistence
    {
        private ISQLReader _sqlReader;

        public TourneyPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<Tourney> GetAllTourneys()
        {
            List<Tourney> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAllTourneys";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Tourney>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Tourney> GetTourneysWithUsername(string username)
        {
            List<Tourney> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTourneysWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Tourney>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<TourneyMatch> GetTourneyMatchesWithId(int tourneyId)
        {
            List<TourneyMatch> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTourneyMatchesWithId";
                    command.Parameters.Add(new SqlParameter("@TourneyId", tourneyId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<TourneyMatch>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Tourney GetTourneyWithId(int id)
        {
            List<Tourney> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTourneyWithId";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Tourney>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public Tourney AddTourney(int entranceCost, int matchesPerRound, int requiredParticipants, bool isSponsored, string creator, string tourneyName, TourneyCaliber tourneyCaliber)
        {
            List<Tourney> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddTourney";
                    command.Parameters.Add(new SqlParameter("@EntranceCost", entranceCost));
                    command.Parameters.Add(new SqlParameter("@MatchesPerRound", matchesPerRound));
                    command.Parameters.Add(new SqlParameter("@RequiredParticipants", requiredParticipants));
                    command.Parameters.Add(new SqlParameter("@IsSponsored", isSponsored));
                    command.Parameters.Add(new SqlParameter("@Creator", creator));
                    command.Parameters.Add(new SqlParameter("@Name", tourneyName));
                    command.Parameters.Add(new SqlParameter("@TourneyCaliber", tourneyCaliber));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Tourney>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }


            return results?.FirstOrDefault();
        }

        public bool AddTourneyMatch(int tourneyId, int round, string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddTourneyMatch";
                    command.Parameters.Add(new SqlParameter("@TourneyId", tourneyId));
                    command.Parameters.Add(new SqlParameter("@Round", round));
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateTourneyMatch(TourneyMatch tourneyMatch)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateTourneyMatch";
                    command.Parameters.Add(new SqlParameter("@TourneyMatchId", tourneyMatch.Id));
                    command.Parameters.Add(new SqlParameter("@Winner", tourneyMatch.Winner));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteTourney(int tourneyId)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteTourney";
                    command.Parameters.Add(new SqlParameter("@Id", tourneyId));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


