﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ILocationsPersistence
    {
        List<Location> GetFavoritedLocations(string username, bool isCached = true);
        bool AddFavoritedLocation(string username, LocationId locationId, int position);
        bool UpdateFavoritedLocation(string username, Location location);
        bool RemoveFavoritedLocation(string username, LocationId locationId);
    }
}
