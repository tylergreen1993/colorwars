﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IMatchHistoryPersistence
    {
        List<MatchHistory> GetMatchHistoryWithUsername(string username, bool isCached = true);
        MatchRanking GetMatchRanking(string username);
        List<MatchRanking> GetTopMatchRankings();
        bool AddMatchHistory(string username, string opponent, int points, int level, int intensity, CPUType cpuType, Guid tourneyMatchId, MatchHistoryResult result);
    }
}
