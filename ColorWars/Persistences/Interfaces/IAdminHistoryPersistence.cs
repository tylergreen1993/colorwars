﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IAdminHistoryPersistence
    {
        List<AdminHistory> GetAdminHistoryWithUsername(string username);
        List<TransferHistory> GetTransferHistory();
        List<TransferHistory> GetTransferHistoryWithUsername(string username);
        bool AddAdminHistory(string username, string adminUser, string reason);
        bool AddTransferHistory(int transferId, TransferType type, string transferer, string transferee, int differenceAmount);
    }
}
