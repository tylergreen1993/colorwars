﻿using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IAdminSettingsPersistence
    {
        AdminSettings GetAdminSettings(bool isCached = true);
        bool AddAdminSetting(string name, string value);
    }
}
