﻿using System;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface INoticeBoardTasksPersistence
    {
        NoticeBoardTask GetNoticeBoardTaskWithUsername(string username, bool isCached = true);
        bool AddNoticeBoardTask(string username, Guid itemId, ItemCondition itemCondition, bool IsExactCondition, bool hasTotalUses, int reward, DateTime itemCreatedDate);
        bool DeleteNoticeBoardTasksWithUsername(string username);
    }
}
