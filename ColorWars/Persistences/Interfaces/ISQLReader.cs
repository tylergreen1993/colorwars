﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace ColorWars.Persistences
{
    public interface ISQLReader
    {
        List<T> GetResults<T>(SqlCommand command) where T : class;
        T GetSettingsResults<T>(SqlCommand command) where T : class;
    }
}
