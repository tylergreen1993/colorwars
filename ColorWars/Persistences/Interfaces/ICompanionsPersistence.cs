﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ICompanionsPersistence
    {
        List<Companion> GetUserCompanionsWithUsername(string username, bool isCached = true);
        List<Companion> GetAdoptionCompanions();
        Companion AddUserCompanion(string username, CompanionType type, string name, CompanionColor color, int position);
        bool AddAdoptionCompanion(Guid id, CompanionType type, string name, CompanionColor color, int level, DateTime createdDate);
        bool UpdateUserCompanionWithId(Companion companion);
        bool DeleteUserCompanionWithId(Guid id);
        bool DeleteAdoptionCompanion(Guid id);
    }
}
