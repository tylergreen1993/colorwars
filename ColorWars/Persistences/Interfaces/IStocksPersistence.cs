﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IStocksPersistence
    {
        List<Stock> GetStocksWithUsername(string username);
        List<Stock> GetAllStocks();
        List<Stock> GetLastTransactionsForStockId(StockTicker id);
        List<StockHistory> GetStockHistory(StockTicker id);
        List<User> GetUsersAndUpdateCrashedStock(StockTicker id);
        Stock RemoveStock(Guid selectionId, string username, int amount);
        bool AddStock(StockTicker id, string username, int purchaseCost, int amount);
        bool AddStockHistory(StockTicker id, string username, int cost, int amount, StockHistoryType type);
    }
}
