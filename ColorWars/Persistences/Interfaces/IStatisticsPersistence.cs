﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IStatisticsPersistence
    {
        Statistics GetStatistics(bool isCached = true);
    }
}
