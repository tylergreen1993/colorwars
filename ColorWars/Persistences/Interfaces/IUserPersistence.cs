﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IUserPersistence
    {
        User GetUserWithUsernameAndPassword(string username, string password, string ipAddress, string userAgent, bool performUpdate = true);
        User GetUserWithAccessToken(string username, Guid accessToken);
        User CreateNewUser(string username, string password, string emailAddress, bool allowNotifications, string referral, string ipAddress, string userAgent);
        User GetUserWithUsername(string username);
        User GetUserWithEmail(string email);
        Count GetDAUMAUStatistic();
        List<User> GetUsersByIPAddress(string ipAddress, bool includeSessions);
        List<User> GetUsersWithAdminRoles();
        List<User> GetRecentUsers(Gender gender);
        List<User> GetReturningUsers(Gender gender);
        List<User> GetAllUsers(Gender gender, int days);
        List<User> GetActiveUsers(int minutes);
        List<User> GetRecentlyReturningUsers();
        List<User> GetNonReturningUsers();
        List<ActiveSession> GetAccessTokensWithUsername(string username);
        List<BannedIPAddress> GetBannedIPAddresses();
        bool AddBannedIPAddress(string ipAddress);
        bool UpdateUserWithUsername(User user, string password);
        bool LogoutUserWithAccessToken(User user);
        bool DeleteAccessTokens(User user);
        bool RemoveBannedIPAddress(string ipAddress);
        bool DeleteInformationWithUsername(string username);
    }
}
