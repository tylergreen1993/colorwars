﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IReportsPersistence
    {
        List<Report> GetAllClaimedReports();
        List<Report> GetAllUnclaimedReports();
        List<Report> GetClaimedReportsWithUsername(string username);
        List<Report> GetReportsWithUsername(string username, bool isReported);
        bool AddReport(string username, string reportedUser, string ipAddress, string reason, int attentionHallPostId = 0);
        bool UpdateReport(Report report);
    }
}
