﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IReferralsPersistence
    {
        List<Referral> GetReferralsWithUsername(string username);
        List<TopReferrer> GetTopReferrers(int days);
    }
}
