﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IWishingStonePersistence
    {
        List<Wish> GetWishes();
        bool AddWish(string username, string content, bool isGranted);
        bool DeleteWishesWithUsername(string username);
    }
}
