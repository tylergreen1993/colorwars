﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IWizardPersistence
    {
        List<WizardHistory> GetWizardHistoryWithUsername(string username, bool isCached = true);
        List<WizardRecord> GetBiggestWizardLossesInLastDays(int days);
        bool AddWizardHistory(string username, WizardType type, string message);
        bool AddWizardRecord(string username, int amount, WizardRecordType type);
    }
}
