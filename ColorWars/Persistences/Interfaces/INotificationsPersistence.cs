﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface INotificationsPersistence
    {
        List<Notification> GetNotificationsWithUsername(string username, bool markAsRead);
        Count GetUnreadNotificationsCountWithUsername(string username);
        UnreadCount GetUnreadCountWithUsername(string username);
        Notification AddNotification(string username, string message, NotificationLocation location, string href, string imageUrl);
    }
}
