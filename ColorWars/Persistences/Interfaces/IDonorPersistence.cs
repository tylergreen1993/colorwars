﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IDonorPersistence
    {
        List<Donor> GetDonorsWithDays(int days, bool isCached = true);
        bool AddDonor(string username, int amount);
    }
}
