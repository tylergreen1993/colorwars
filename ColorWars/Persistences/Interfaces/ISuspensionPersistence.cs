﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISuspensionPersistence
    {
        List<SuspensionHistory> GetSuspensionHistoryWithUsername(string username);
        bool AddSuspensionHistory(string username, string suspendingUser, string reason, DateTime suspendedDate);
    }
}
