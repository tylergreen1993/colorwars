﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IBuyersClubPersistence
    {
        List<StoreItem> GetBuyersClubItems(bool overrideCache = false);
        bool AddBuyersClubItem(Guid itemId, int cost);
        bool DeleteBuyersClubItems();
    }
}
