﻿using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ITreasureDivePersistence
    {
		TreasureDive GetTreasureDiveWithUsername(string username, bool isCached = true);
        bool AddTreasureDive(string username, string treasureCoordinates);
        bool UpdateTreasureDive(string username, int attempts, string lastGuessCoordinates);
        bool RemoveTreasureDiveWithUsername(string username);
    }
}
