﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IStorePersistence
    {
        List<StoreItem> GetStoreItems(bool overrideCache = false);
        bool AddStoreItem(Guid itemId, int cost, int minCost);
        bool DeleteStoreItems();
    }
}
