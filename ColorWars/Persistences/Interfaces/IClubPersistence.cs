﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IClubPersistence
    {
        List<ClubMembership> GetAllClubMembershipsWithUsername(string username, bool isCached = true);
        Item GetIncubatorItemWithUsername(string username, bool isCached = true);
        bool AddClubMembership(string username, DateTime startDate, DateTime endDate);
        bool AddIncubatorItem(Guid selectionId, Guid itemId, string username, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate);
        bool UpdateClubMembershipWithId(ClubMembership clubMembership);
        bool DeleteIncubatorItemWithSelectionId(Guid id);
    }
}
