﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISurveyPersistence
    {
        List<SurveyAnswer> GetSurveyAnswers();
        List<SurveyAnswer> GetSurveyAnswersWithUsername(string username);
        bool AddSurveyAnswer(string username, SurveyQuestionId surveyQuestionId, string answer);
    }
}