﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IBlockedPersistence
    {
        List<Blocked> GetBlockedWithUsername(string username, bool isCached = true);
        bool AddBlocked(string username, string blockedUser);
        bool DeleteFromBlocked(string username, string blockedUser);
    }
}
