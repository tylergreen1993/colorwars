﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IJobsPersistence
    {
        List<JobSubmission> GetJobSubmissions();
        List<JobSubmission> GetJobSubmissionsWithUsername(string username);
        JobSubmission GetJobSubmissionWithId(Guid id);
        bool AddJobSubmission(Guid jobSubmissionId, string username, JobPosition position, string title, string content, int attentionHallPostId);
        bool UpdateJobSubmission(JobSubmission job);
        bool DeleteHelperSubmissionWithUsername(string username);
    }
}
