﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISettingsPersistence
    {
        Settings GetSettingsWithUsername(string username, bool isCached = true);
        List<User> GetUsersBySettingNameAndValue(string name, string value);
        bool AddSettingForUsername(User user, string name, string value);
        bool DeleteSettingForUsername(User user, string name);
    }
}
