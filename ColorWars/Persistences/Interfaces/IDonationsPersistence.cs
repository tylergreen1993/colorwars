﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IDonationsPersistence
    {
        List<Item> GetDonationItems();
        bool AddDonationItem(string username, Guid itemId, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate);
        bool DeleteDonationItem(Guid id);
    }
}
