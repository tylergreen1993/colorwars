﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ITradePersistence
    {
        List<Trade> GetTrades();
        List<Trade> GetTradesWithSearch(string query, ItemTheme theme);
        List<Trade> GetTradesWithUsername(string username);
        List<Trade> GetTradesUserLeftOfferWithUsername(string username);
        Trade GetTradeWithId(int id);
        List<TradeOffer> GetTradeOffersWithTradeId(int tradeId);
        Item GetTradeItemWithSelectionId(Guid selectionId);
        Trade AddTrade(string username, Guid? item1Id, Guid? item2Id, Guid? item3Id, string request);
        bool AddTradeOffer(string username, int tradeId, Guid? item1Id, Guid? item2Id, Guid? item3Id);
        bool AddTradeItem(Item item);
        bool CompleteTrade(int tradeId, Guid tradeOfferId);
        bool UpdateTrade(Trade trade);
        bool DeleteTrade(int id);
        bool DeleteTradeOffer(Guid id);
    }
}
