﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IGroupsPersistence
    {
        List<Group> GetTopGroups(bool isCached = true);
        List<Group> GetGroupsWithSearchQuery(string searchQuery);
        List<Group> GetGroupsWithUsername(string username, bool isCached = true);
        Group GetGroupWithId(int id);
        List<GroupMember> GetGroupMembersWithGroupId(int id);
        List<GroupMessage> GetGroupMessagesWithGroupId(int groupId);
        List<GroupRequest> GetGroupRequestsWithGroupId(int groupId);
        Group AddGroup(string username, string name, string displayPicUrl, GroupPower power, string description, bool isPrivateGroup);
        bool AddGroupMember(string username, int groupId, GroupRole role, bool isPrimary);
        bool AddGroupMessage(int groupId, string sender, string content);
        bool AddGroupRequest(string username, int groupId);
        bool UpdateGroup(Group group);
        bool UpdateGroupMember(GroupMember groupMember);
        bool UpdateGroupMessage(GroupMessage groupMessage);
        bool UpdateGroupRequest(GroupRequest groupRequest);
        bool DeleteGroup(int groupId);
        bool DeleteGroupMember(int groupId, string username);
        bool DeleteGroupRequest(string username, int groupId);
    }
}
