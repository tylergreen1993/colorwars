﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IDoubtItPersistence
    {
		List<DoubtItCard> GetDoubtItCards(string username, bool isCached = true);
        bool AddDoubtItCards(string username, string amounts, DoubtItOwner owner);
        bool UpdateDoubtItCard(Guid id, string username, DoubtItOwner owner, DateTime addedDate);
        bool DeleteDoubtItCards(string username);
    }
}
