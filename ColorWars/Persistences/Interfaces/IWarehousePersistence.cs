﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IWarehousePersistence
    {
        List<StoreItem> GetBulkItemsWithUsername(string username, bool overrideCache = false);
        bool AddBulkItem(string username, Guid itemId, int cost);
        bool DeleteBulkItemsWithUsername(string username);
    }
}
