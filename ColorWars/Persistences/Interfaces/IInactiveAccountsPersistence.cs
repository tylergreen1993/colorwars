﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IInactiveAccountsPersistence
    {
        List<InactiveAccount> GetInactiveAccounts(InactiveAccountType type);
        bool AddInactiveAccount(string username, InactiveAccountReason reason, InactiveAccountType type, string info);
        bool RemoveInactiveAccount(string username);
    }
}