﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IJunkyardPersistence
    {
        List<Item> GetJunkyardItems();
        bool AddJunkyardItem(Guid itemId, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate);
        bool DeleteJunkyardItem(Guid id);
    }
}
