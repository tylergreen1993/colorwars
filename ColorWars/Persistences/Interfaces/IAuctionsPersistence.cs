﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IAuctionsPersistence
    {
        List<Auction> GetAuctions();
        List<Auction> GetPastAuctions();
        List<Auction> GetAuctionsWithSearch(string query, ItemTheme theme);
        List<Auction> GetAuctionsWithUsername(string username);
        Auction GetAuctionWithAuctionId(int auctionId);
        List<Bid> GetBidsWithAuctionId(int auctionId);
        Item GetAuctionItemWithAuctionId(int auctionId);
        bool AddBid(int auctionId, string username, int points);
        Auction AddAuction(string username, int startPoints, int minBidIncrement, Guid itemId, DateTime endDate);
        bool AddAuctionItem(Item item);
        bool UpdateAuction(int id, int startPoints, bool creatorCollected, bool winnerCollected, bool sentNotification, DateTime endDate);
    }
}
