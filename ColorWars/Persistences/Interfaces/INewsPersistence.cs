﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface INewsPersistence
    {
        List<News> GetNews();
        List<NewsComment> GetNewsCommentsWithId(int id, string username);
        News AddNews(string username, string title, string content);
        NewsComment AddNewsComment(int newsId, Guid? parentCommentId, string username, string message);
        bool AddOrDeleteNewsCommentLike(int id, Guid commentId, string username);
        bool UpdateNews(News news);
        bool DeleteNews(int id);
        bool DeleteNewsComment(int newsId, Guid id);
    }
}
