﻿using System;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ICraftingPersistence
    {
        Item GetFurnaceItemWithUsername(string username, bool isCached = true);
        bool AddFurnaceItem(Guid selectionId, Guid itemId, string username, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate);
        bool DeleteFurnaceItemWithSelectionId(Guid id);
    }
}
