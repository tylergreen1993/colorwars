﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ILottoPersistence
    {
        List<Lotto> GetLottos();
        bool AddLotto(int currentPot, int winningNumber);
        bool UpdateLotto(int id, int currentPot, string winnerUsername, DateTime wonDate);
    }
}
