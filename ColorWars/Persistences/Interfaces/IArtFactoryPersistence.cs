﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IArtFactoryPersistence
    {
		List<Custom> GetRecentUserCustomCards();
		bool AddUserCustomCard(string username, string name, string imageUrl);
	}
}
