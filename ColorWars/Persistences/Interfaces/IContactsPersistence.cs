﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IContactsPersistence
    {
        List<Contact> GetAllContactSubmissionsWithUsername(string username);
        List<Contact> GetAllClaimedContacts();
        List<Contact> GetAllUnclaimedContacts();
        List<Contact> GetClaimedContactsWithUsername(string username);
        List<Contact> GetContactsWithUsername(string username);
        bool AddContact(string username, string emailAddress, string ipAddress, string message, ContactCategory category, bool replyViaMessages);
        bool UpdateContact(Contact contact);
    }
}
