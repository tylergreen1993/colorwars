﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISecretGifterPersistence
    {
        List<Item> GetSecretGiftItemsWithUsername(string username);
        SecretGiftUser GetSecretGiftUsername(string username);
        bool AddSecretGiftItem(Item item);
        bool DeleteSecretGiftItemWithSelectionId(Guid id);
    }
}
