﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IAccomplishmentsPersistence
    {
        List<Accomplishment> GetAllAccomplishments(bool isCached = true);
        List<Accomplishment> GetAccomplishmentsWithUsername(string username, bool isCached = true);
        List<Accomplishment> GetAccomplishmentsRankings();
        bool AddAccomplishment(string username, AccomplishmentId accomplishmentId);
    }
}
