﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IStreakCardsPersistence
    {
        List<StreakCard> GetStreakCardsWithUsername(string username, bool isCached = true);
        bool AddStreakCard(string username, int position, int points);
        bool DeleteStreakCards(string username);
    }
}
