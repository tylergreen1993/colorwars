﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISafetyDepositPersistence
    {
        List<Item> GetSafetyDepositItemsWithUsername(string username, bool isCached = true);
        bool AddSafetyDepositItem(Item item, int position);
        bool UpdateSafetyDepositItemWithSelectionId(Guid selectionId, int position);
        bool DeleteSafetyDepositItem(Guid id, string username);
        bool RepairAllSafetyDepositItems(string username);
    }
}
