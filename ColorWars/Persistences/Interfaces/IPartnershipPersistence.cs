﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IPartnershipPersistence
    {
        List<PartnershipRequest> GetPartnershipRequests(string username);
        List<PartnershipEarning> GetPartnershipEarnings(string username, string partnershipUsername, bool isCached = true);
        bool AddPartnershipRequest(string username, string requestUsername);
        bool AddPartnershipEarning(string username, string partnershipUsername, int amount);
        bool DeletePartnershipRequest(string username, string requestUsername);
    }
}
