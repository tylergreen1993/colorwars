﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ITakeOfferPersistence
    {
        List<OfferCard> GetOfferCardsWithUsername(string username, bool isCached = true);
        bool AddOfferCard(string username, int position, int points);
        bool DeleteOfferCards(string username);
    }
}
