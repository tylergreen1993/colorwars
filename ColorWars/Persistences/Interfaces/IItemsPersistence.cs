﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IItemsPersistence
    {
        List<Item> GetAllItemsWithUsername(string username, bool isCached = true);
        List<Item> GetAllItems();
        List<Card> GetAllCards();
        List<Enhancer> GetAllEnhancers();
        List<Candy> GetAllCandyCards();
        List<Card> GetAllCardsWithUsername(string username, bool isCached = true);
        List<Enhancer> GetAllEnhancersWithUsername(string username, bool isCached = true);
        List<CardPack> GetAllCardPacksWithUsername(string username, bool isCached = true);
        List<Crafting> GetAllCraftingWithUsername(string username, bool isCached = true);
        List<Candy> GetAllCandyWithUsername(string username, bool isCached = true);
        List<Coupon> GetAllCouponsWithUsername(string username, bool isCached = true);
        List<Egg> GetAllEggCardsWithUsername(string username, bool isCached = true);
        List<LibraryCard> GetAllLibraryCardsWithUsername(string username, bool isCached = true);
        List<Theme> GetAllThemesWithUsername(string username, bool isCached = true);
        List<Sleeve> GetAllSleevesWithUsername(string username, bool isCached = true);
        List<Upgrader> GetAllUpgradersWithUsername(string username, bool isCached = true);
        List<Jewel> GetAllJewelsWithUsername(string username, bool isCached = true);
        List<Stealth> GetAllStealthCardsWithUsername(string username, bool isCached = true);
        List<Frozen> GetAllFrozenItemsWithUsername(string username, bool isCached = true);
        List<CompanionItem> GetAllCompanionsWithUsername(string username, bool isCached = true);
        List<Seed> GetAllSeedCardsWithUsername(string username, bool isCached = true);
        List<Art> GetAllArtCardsWithUsername(string username, bool isCached = true);
        List<Fairy> GetAllFairyCardsWithUsername(string username, bool isCached = true);
        List<Beyond> GetAllBeyondCardsWithUsername(string username, bool isCached = true);
        List<KeyCard> GetAllKeyCardsWithUsername(string username, bool isCached = true);
        List<Item> GetTopOldestItems(bool isCached = true);
        List<Item> GetTopLongestOwnedItems(bool isCached = true);
        bool AddItemForUsername(string username, Item item);
        bool RemoveItemForUsername(string username, Item item);
        bool UpdateItemForSelectionId(string username, Item item);
        bool RepairAllItems(string username);
        bool RemoveUsedItems(string username);
    }
}
