﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ITourneyPersistence
    {
        List<Tourney> GetAllTourneys();
        List<Tourney> GetTourneysWithUsername(string username);
        List<TourneyMatch> GetTourneyMatchesWithId(int tourneyId);
        Tourney GetTourneyWithId(int id);
        Tourney AddTourney(int entranceCost, int matchesPerRound, int requiredParticipants, bool isSponsored, string creator, string tourneyName, TourneyCaliber tourneyCaliber);
        bool AddTourneyMatch(int tourneyId, int round, string username);
        bool UpdateTourneyMatch(TourneyMatch tourneyMatch);
        bool DeleteTourney(int tourneyId);
    }
}
