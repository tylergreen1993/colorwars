﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ILibraryPersistence
    {
        List<LibraryHistory> GetLibraryHistoryWithUsername(string username, bool isCached = true);
        List<VoidMessage> GetVoidMessages();
        bool AddLibraryHistory(string username, LibraryKnowledgeType type, string message);
        bool AddVoidMessage(string username, string message);
    }
}
