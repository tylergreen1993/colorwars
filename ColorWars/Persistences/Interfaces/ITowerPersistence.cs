﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ITowerPersistence
    {
        List<TowerRanking> GetTowerRankingsInPastDays(int days);
        List<TowerRanking> GetTopTowerRankings();
        bool AddTowerRanking(string username, int topFloorLevel);
    }
}