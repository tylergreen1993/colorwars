﻿using System;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ICavernsPersistence
    {
        Item GetPlantedItemWithUsername(string username, bool isCached = true);
        bool AddPlantedItem(Guid selectionId, Guid itemId, string username, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate);
        bool DeletePlantedItemWithSelectionId(Guid id);
    }
}
