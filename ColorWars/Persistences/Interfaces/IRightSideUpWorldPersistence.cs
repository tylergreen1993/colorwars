﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IRightSideUpWorldPersistence
    {
        List<HallOfFameEntry> GetRightSideUpGuardHallOfFame();
        bool AddRightSideUpGuardHallOfFame(string username);
    }
}
