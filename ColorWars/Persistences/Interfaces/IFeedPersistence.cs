﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IFeedPersistence
    {
        List<HiddenFeedCategory> GetHiddenFeedCategories(string username, bool isCached = true);
        bool AddHiddenFeedCategory(string username, FeedCategory feedId);
        bool DeleteHiddenFeedCategory(string username, FeedCategory feedId);
    }
}
