﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IDisposerPersistence
    {
        List<DisposerRecord> GetDisposerRecordsInLastDays(int days);
        bool AddDisposerRecord(string username);
    }
}
