﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IMessagesPersistence
    {
        List<Message> GetAllMessagesWithUsername(string username, int topCount);
        List<Message> GetMessagesBetweenUsernames(string username, string correspondent, bool updateRead = true);
        int GetUnreadMessagesCountWithUsername(string username, string correspondent);
        Message AddMessage(string sender, string recipient, string content);
        bool DeleteMessagesBetweenUsernames(string username, string correspondent);
    }
}
