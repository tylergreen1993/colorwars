﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IPowerMovesPersistence
    {
		List<PowerMove> GetPowerMovesWithUsername(string username, bool isCached = true);
		bool AddPowerMove(string username, PowerMoveType powerMoveType);
        bool UpdatePowerMove(string username, PowerMoveType powerMoveType, bool isActive);
    }
}
