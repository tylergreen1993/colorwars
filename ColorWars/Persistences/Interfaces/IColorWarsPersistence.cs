﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IColorWarsPersistence
    {
        List<ColorWarsSubmission> GetAllColorWarsSubmissionsWithTeamColor(ColorWarsColor teamColor);
        List<ColorWarsSubmission> GetAllColorWarsSubmissionsWithUsername(string username);
        List<ColorWarsHistory> GetAllColorWarsHistory();
        bool AddColorWarsSubmission(string username, int amount, ColorWarsColor teamColor);
        bool AddColorWarsHistory(ColorWarsColor teamColor, int amount);
        bool CompleteColorWars(ColorWarsColor winningTeamColor, int amountEarned);
    }
}
