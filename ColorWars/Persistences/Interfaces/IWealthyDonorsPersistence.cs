﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IWealthyDonorsPersistence
    {
		List<WealthyDonor> GetWealthyDonors();
		bool AddWealthyDonor(string username, int amount);
	}
}