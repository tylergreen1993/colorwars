﻿using System;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IShowcasePersistence
    {
        Item GetTimedShowcase(bool overrideCache = false);
        Item GetAgerItemWithUsername(string username, bool isCached = true);
        bool AddTimedShowcase(Guid itemId);
        bool AddAgerItem(Item item);
        bool DeleteTimedShowcase();
        bool DeleteAgerItemWithSelectionId(Guid id);
    }
}
