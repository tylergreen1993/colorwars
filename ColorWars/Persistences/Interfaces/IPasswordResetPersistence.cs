﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IPasswordResetPersistence
    {
        PasswordReset GetPasswordReset(Guid id, string username);
        PasswordReset CreateOrUpdatePasswordReset(string username);
    }
}
