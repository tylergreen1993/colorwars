﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IAttentionHallPersistence
    {
        List<AttentionHallPost> GetAllAttentionHallPosts(AttentionHallPostsFilter filter, AttentionHallPostType type, string username);
        AttentionHallPost GetAttentionHallPost(int postId, string username);
        List<AttentionHallComment> GetAttentionHallComments(int postId, string username);
        List<AttentionHallComment> GetAttentionHallCommentsFromPostsWithUsername(string username);
        AttentionHallPoint GetTotalAttentionHallPointsWithUsername(string username);
        List<UserStatistic> GetTopAttentionHallPosters(bool isCached = true);
        List<UserStatistic> GetTopAttentionHallCommenters(bool isCached = true);
        AttentionHallPost AddAttentionHallPost(string username, string title, string content, AccomplishmentId? accomplishmentId, Item item, string matchTagUsername, Guid jobSubmissionId, AttentionHallFlair? attentionHallFlair);
        AttentionHallComment AddAttentionHallComment(Guid? parentCommentId, int postId, string username, string comment);
        bool AddAttentionHallPoint(int postId, string username, AttentionHallPointType points);
        bool AddOrDeleteAttentionHallCommentLike(int postId, Guid commentId, string username);
        bool UpdateAttentionHallPost(int id, string title, string content);
        bool UpdateAttentionHallPoint(int postId, string username, AttentionHallPointType points);
        bool DeleteAttentionHallPost(int postId);
        bool DeleteAttentionHallComment(int postId, Guid commentId);
    }
}
