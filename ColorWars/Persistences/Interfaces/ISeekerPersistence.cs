﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISeekerPersistence
    {
        List<SeekerChallenge> GetSeekerChallengesWithUsername(string username, bool isCached = true);
        bool AddSeekerChallenge(string username, Guid itemId, ItemTheme itemTheme, Guid rewardId, ItemTheme rewardTheme);
        bool DeleteSeekerChallengesWithUsername(string username);
    }
}
