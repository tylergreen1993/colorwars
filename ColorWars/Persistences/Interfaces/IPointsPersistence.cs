﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IPointsPersistence
    {
        bool UpdatePoints(User user);
        WealthRanking GetWealthRanking(string username);
        List<WealthRanking> GetTopWealthRankings();
    }
}
