﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISpinSuccessPersistence
    {
        List<SpinSuccessHistory> GetSpinSuccessHistoryWithUsername(string username, bool isCached = true);
        bool AddSpinSuccessHistory(string username, SpinSuccessType type, string message);
    }
}
