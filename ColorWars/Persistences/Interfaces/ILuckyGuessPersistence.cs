﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ILuckyGuessPersistence
    {
        List<LuckyGuess> GetLuckyGuesses();
        List<LuckyGuess> GetLuckyGuessTreasures();
        bool AddLuckyGuess(int position, string username);
        bool AddLuckyGuessWinner(string username);
        bool AddLuckyGuessTreasure(int position);
        bool DeleteLuckyGuesses();
    }
}
