﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IMarketStallPersistence
    {
        List<MarketStall> GetNewMarketStalls();
        MarketStall GetMarketStallWithUsername(string username);
        MarketStall GetMarketStallWithId(int id);
        List<MarketStallItem> GetMarketStallItemsWithSearchQuery(string query, ItemTheme theme);
        List<MarketStallItem> GetMarketStallItemsWithMarketStallId(int id, int discount);
        List<MarketStallHistory> GetMarketStallsHistoryWithMarketStallId(int id);
        MarketStall AddMarketStall(string username, string name, string description, string color);
        bool AddMarketStallItem(Item item, int marketStallId, int cost);
        bool AddMarketStallsHistory(int marketStallId, string username, MarketStallItem item);
        bool UpdateMarketStallWithId(MarketStall marketStall);
        bool RepairAllMarketStallItems(MarketStall marketStall);
        bool DeleteMarketStallWithId(int id);
        bool DeleteMarketStallItemWithSelectionId(Guid selectionId);
    }
}
