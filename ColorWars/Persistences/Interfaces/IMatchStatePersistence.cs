﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IMatchStatePersistence
    {
        List<MatchState> GetAllMatchStatesWithUsername(string username, bool getAll = false, bool isCached = true);
        bool UpdateMatchStateForId(MatchState matchState);
        bool AddMatchState(MatchState matchState);
        bool DeleteFromMatchStateWithUsername(string username, string opponent, bool isCPU, bool isExpired);
        MatchWaitlist GetMatchWaitlistWithUsername(string username);
        List<MatchWaitlist> GetMatchWaitlists(string username, bool isElite);
        MatchWaitlist AddOrUpdateMatchWaitlist(string username, int points, bool isElite, string challengeUser, Guid tourneyMatchId);
        bool DeleteFromMatchWaitlistWithUsername(string username);
    }
}
