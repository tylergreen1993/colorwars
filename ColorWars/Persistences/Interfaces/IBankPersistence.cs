﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IBankPersistence
    {
        Bank GetBankInfoWithUsername(string username, bool isCached = true);
        List<BankTransaction> GetBankTransactionsWithUsername(string username, bool isCached = true);
        bool UpdateBank(string username, int points);
        bool AddBankTransaction(string username, BankTransactionType type, int amount);
    }
}
