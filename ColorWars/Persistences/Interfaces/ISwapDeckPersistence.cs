﻿using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface ISwapDeckPersistence
    {
        List<SwapDeckHistory> GetSwapDeckHistoryWithUsername(string username, bool isCached = true);
        bool AddSwapDeckHistory(string username, SwapDeckType type, string message);
    }
}
