﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IPromoCodesPersistence
    {
        PromoCode GetPromoCode(string code);
        List<PromoCode> GetPreviousPromoCodes(string username, bool isCached = true);
        List<PromoCode> GetAllUnexpiredGlobalPromoCodes(bool isCached = true);
        List<PromoCodeHistory> GetPromoCodeHistoryWithUsername(string username, bool isCached = true);
        PromoCode RedeemPromoCode(string username, string code);
        bool AddPromoCode(string code, string creator, PromoEffect effect, bool oneUserUse, bool isSponsor, Guid itemId, DateTime expiryDate);
        bool AddPromoCodeHistory(string username, string code, string message);
    }
}
