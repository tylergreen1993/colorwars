﻿using System;
using System.Collections.Generic;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public interface IProfilePersistence
    {
        List<Item> GetShowroomItemsWithUsername(string username, bool isCached = true);
        List<ShowroomLike> GetShowroomLikes(string showroomUsername);
        bool AddShowroomItem(Item item, int position);
        bool AddOrDeleteShowroomLike(string username, string showroomUsername);
        bool UpdateShowroomItemWithSelectionId(Guid selectionId, int position, int uses, DateTime repairedDate);
        bool DeleteShowroomItemWithSelectionId(Guid selectionId);
    }
}
