﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class TradePersistence : ITradePersistence
    {
        private ISQLReader _sqlReader;

        public TradePersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<Trade> GetTrades()
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTrades";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Trade> GetTradesWithSearch(string query, ItemTheme theme)
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradesWithSearch";
                    command.Parameters.Add(new SqlParameter("@Query", query));
                    command.Parameters.Add(new SqlParameter("@Theme", theme));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Trade> GetTradesWithUsername(string username)
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradesWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Trade> GetTradesUserLeftOfferWithUsername(string username)
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradesUserLeftOfferWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Trade GetTradeWithId(int id)
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradeWithId";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<TradeOffer> GetTradeOffersWithTradeId(int tradeId)
        {
            List<TradeOffer> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradeOffersWithTradeId";
                    command.Parameters.Add(new SqlParameter("@TradeId", tradeId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<TradeOffer>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Item GetTradeItemWithSelectionId(Guid selectionId)
        {
            List<Item> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTradeItemWithSelectionId";
                    command.Parameters.Add(new SqlParameter("@SelectionId", selectionId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Item>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public Trade AddTrade(string username, Guid? item1Id, Guid? item2Id, Guid? item3Id, string request)
        {
            List<Trade> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddTrade";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Item1Id", item1Id));
                    command.Parameters.Add(new SqlParameter("@Item2Id", item2Id));
                    command.Parameters.Add(new SqlParameter("@Item3Id", item3Id));
                    command.Parameters.Add(new SqlParameter("@Request", request));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Trade>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public bool AddTradeOffer(string username, int tradeId, Guid? item1Id, Guid? item2Id, Guid? item3Id)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddTradeOffer";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@TradeId", tradeId));
                    command.Parameters.Add(new SqlParameter("@Item1Id", item1Id));
                    command.Parameters.Add(new SqlParameter("@Item2Id", item2Id));
                    command.Parameters.Add(new SqlParameter("@Item3Id", item3Id));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool AddTradeItem(Item item)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddTradeItem";
                    command.Parameters.Add(new SqlParameter("@SelectionId", item.SelectionId));
                    command.Parameters.Add(new SqlParameter("@ItemId", item.Id));
                    if (item.Category == ItemCategory.Custom)
                    {
                        command.Parameters.Add(new SqlParameter("@Name", item.Name));
                        command.Parameters.Add(new SqlParameter("@ImageUrl", item.ImageUrl));
                    }
                    command.Parameters.Add(new SqlParameter("@Uses", item.Uses));
                    command.Parameters.Add(new SqlParameter("@Theme", item.Theme));
                    command.Parameters.Add(new SqlParameter("@CreatedDate", item.CreatedDate));
                    command.Parameters.Add(new SqlParameter("@RepairedDate", item.RepairedDate));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool CompleteTrade(int tradeId, Guid tradeOfferId)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "CompleteTrade";
                    command.Parameters.Add(new SqlParameter("@TradeId", tradeId));
                    command.Parameters.Add(new SqlParameter("@TradeOfferId", tradeOfferId));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateTrade(Trade trade)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateTrade";
                    command.Parameters.Add(new SqlParameter("@Id", trade.Id));
                    command.Parameters.Add(new SqlParameter("@Request", trade.Request));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteTrade(int id)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteTrade";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteTradeOffer(Guid id)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteTradeOffer";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


