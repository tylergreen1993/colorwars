﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class MatchStatePersistence : IMatchStatePersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public MatchStatePersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<MatchState> GetAllMatchStatesWithUsername(string username, bool getInactive = false, bool isCached = true)
        {
            List<MatchState> results = isCached ? _session.GetObject<List<MatchState>>("MatchStates") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAllMatchStatesWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@GetInactive", getInactive));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<MatchState>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("MatchStates", results);
            }
            return results;
        }

        public bool UpdateMatchStateForId(MatchState matchState)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateMatchStateForId";
                    command.Parameters.Add(new SqlParameter("@Id", matchState.Id));
                    command.Parameters.Add(new SqlParameter("@CardId", matchState.CardId));
                    command.Parameters.Add(new SqlParameter("@EnhancerId", matchState.EnhancerId));
                    command.Parameters.Add(new SqlParameter("@PowerMove", matchState.PowerMove));
                    command.Parameters.Add(new SqlParameter("@CPUCardId", matchState.CPUCardId));
                    command.Parameters.Add(new SqlParameter("@CPUEnhancerId", matchState.CPUEnhancerId));
                    command.Parameters.Add(new SqlParameter("@CPUPowerMove", matchState.CPUPowerMove));
                    command.Parameters.Add(new SqlParameter("@Position", matchState.Position));
                    command.Parameters.Add(new SqlParameter("@Points", matchState.Points));
                    command.Parameters.Add(new SqlParameter("@Status", matchState.Status));
                    command.Parameters.Add(new SqlParameter("@IsInSelection", matchState.IsInSelection));
                    command.Parameters.Add(new SqlParameter("@UpdatedDate", DateTime.UtcNow));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool AddMatchState(MatchState matchState)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddMatchState";
                    command.Parameters.Add(new SqlParameter("@Username", matchState.Username));
                    command.Parameters.Add(new SqlParameter("@Opponent", matchState.Opponent));
                    command.Parameters.Add(new SqlParameter("@CardId", matchState.CardId));
                    command.Parameters.Add(new SqlParameter("@EnhancerId", matchState.EnhancerId));
                    command.Parameters.Add(new SqlParameter("@PowerMove", matchState.PowerMove));
                    command.Parameters.Add(new SqlParameter("@CPUCardId", matchState.CPUCardId));
                    command.Parameters.Add(new SqlParameter("@CPUEnhancerId", matchState.CPUEnhancerId));
                    command.Parameters.Add(new SqlParameter("@CPUCardTheme", matchState.CPUCardTheme));
                    command.Parameters.Add(new SqlParameter("@CPUPowerMove", matchState.CPUPowerMove));
                    command.Parameters.Add(new SqlParameter("@Position", matchState.Position));
                    command.Parameters.Add(new SqlParameter("@Points", matchState.Points));
                    command.Parameters.Add(new SqlParameter("@TourneyMatchId", matchState.TourneyMatchId));
                    command.Parameters.Add(new SqlParameter("@Status", matchState.Status));
                    command.Parameters.Add(new SqlParameter("@ImageUrl", matchState.ImageUrl));
                    command.Parameters.Add(new SqlParameter("@IsInSelection", matchState.IsInSelection));
                    command.Parameters.Add(new SqlParameter("@IsCPU", matchState.IsCPU));
                    command.Parameters.Add(new SqlParameter("@IsCPUChallenge", matchState.IsCPUChallenge));
                    command.Parameters.Add(new SqlParameter("@IsSpecialCPU", matchState.IsSpecialCPU));
                    command.Parameters.Add(new SqlParameter("@UpdatedDate", DateTime.UtcNow));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool DeleteFromMatchStateWithUsername(string username, string opponent, bool isCPU, bool isExpired)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteFromMatchStateWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Opponent", opponent));
                    command.Parameters.Add(new SqlParameter("@IsCPU", isCPU));
                    command.Parameters.Add(new SqlParameter("@IsExpired", isExpired));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public MatchWaitlist GetMatchWaitlistWithUsername(string username)
        {
            List<MatchWaitlist> results;

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetMatchWaitlistWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<MatchWaitlist>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<MatchWaitlist> GetMatchWaitlists(string username, bool isElite)
        {
            List<MatchWaitlist> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetMatchWaitlists";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@IsElite", isElite));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<MatchWaitlist>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public MatchWaitlist AddOrUpdateMatchWaitlist(string username, int points, bool isElite, string challengeUser, Guid tourneyMatchId)
        {
            List<MatchWaitlist> results;

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddOrUpdateMatchWaitlist";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Points", points));
                    command.Parameters.Add(new SqlParameter("@IsElite", isElite));
                    command.Parameters.Add(new SqlParameter("@ChallengeUser", challengeUser));
                    command.Parameters.Add(new SqlParameter("@TourneyMatchId", tourneyMatchId));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<MatchWaitlist>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }


        public bool DeleteFromMatchWaitlistWithUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteFromMatchWaitlistWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("MatchStates");
        }
    }
}
