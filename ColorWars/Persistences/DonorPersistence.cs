﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class DonorPersistence : IDonorPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public DonorPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<Donor> GetDonorsWithDays(int days, bool isCached = true)
        {
            List<Donor> results = isCached ? _session.GetObject<List<Donor>>("RecentDonors") : null;
            if (results != null)
            {
                return results;
            }
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetDonorsWithDays";
                    command.Parameters.Add(new SqlParameter("@Days", days));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Donor>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            if (isCached)
            {
                _session.SetObject("RecentDonors", results);
            }
            return results;
        }

        public bool AddDonor(string username, int amount)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddDonor";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Amount", amount));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    _session.Remove("RecentDonors");
                    return true;
                }
            }
        }
    }
}


