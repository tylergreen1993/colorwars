﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class SuspensionPersistence : ISuspensionPersistence
    {
        private ISQLReader _sqlReader;

        public SuspensionPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<SuspensionHistory> GetSuspensionHistoryWithUsername(string username)
        {
            List<SuspensionHistory> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetSuspensionHistoryWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<SuspensionHistory>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public bool AddSuspensionHistory(string username, string suspendingUser, string reason, DateTime suspendedDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddSuspensionHistory";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@SuspendingUser", suspendingUser));
                    command.Parameters.Add(new SqlParameter("@Reason", reason));
                    command.Parameters.Add(new SqlParameter("@SuspendedDate", suspendedDate));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


