﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class DonationsPersistence : IDonationsPersistence
    {
        private ISQLReader _sqlReader;

        public DonationsPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<Item> GetDonationItems()
        {
            List<Item> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetDonationItems";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Item>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public bool AddDonationItem(string username, Guid itemId, int uses, ItemTheme theme, DateTime createdDate, DateTime repairedDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddDonationItem";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@ItemId", itemId));
                    command.Parameters.Add(new SqlParameter("@Uses", uses));
                    command.Parameters.Add(new SqlParameter("@Theme", theme));
                    command.Parameters.Add(new SqlParameter("@CreatedDate", createdDate));
                    command.Parameters.Add(new SqlParameter("@RepairedDate", repairedDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteDonationItem(Guid id)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteDonationItem";
                    command.Parameters.Add(new SqlParameter("@Id", id));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


