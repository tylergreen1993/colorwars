﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class NoticeBoardTasksPersistence : INoticeBoardTasksPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public NoticeBoardTasksPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public NoticeBoardTask GetNoticeBoardTaskWithUsername(string username, bool isCached = true)
        {
            List<NoticeBoardTask> results = isCached ? _session.GetObject<List<NoticeBoardTask>>("NoticeBoardTask") : null;
            if (results != null)
            {
                return results?.FirstOrDefault();
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetNoticeBoardTaskWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<NoticeBoardTask>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("NoticeBoardTask", results);
            }
            return results?.FirstOrDefault();
        }

        public bool AddNoticeBoardTask(string username, Guid itemId, ItemCondition itemCondition, bool IsExactCondition, bool hasTotalUses, int reward, DateTime itemCreatedDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddNoticeBoardTask";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@ItemId", itemId));
                    command.Parameters.Add(new SqlParameter("@ItemCondition", itemCondition));
                    command.Parameters.Add(new SqlParameter("@IsExactCondition", IsExactCondition));
                    command.Parameters.Add(new SqlParameter("@HasTotalUses", hasTotalUses));
                    command.Parameters.Add(new SqlParameter("@Reward", reward));
                    command.Parameters.Add(new SqlParameter("@ItemCreatedDate", itemCreatedDate));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool DeleteNoticeBoardTasksWithUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteNoticeBoardTasksWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("NoticeBoardTask");
        }
    }
}


