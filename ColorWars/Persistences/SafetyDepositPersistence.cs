﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class SafetyDepositPersistence : ISafetyDepositPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public SafetyDepositPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<Item> GetSafetyDepositItemsWithUsername(string username, bool isCached = true)
        {
            List<Item> results = isCached ? _session.GetObject<List<Item>>("SafetyDepositItems") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetSafetyDepositItemsWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Item>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("SafetyDepositItems", results);
            }
            return results;
        }

        public bool AddSafetyDepositItem(Item item, int position)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddSafetyDepositItem";
                    command.Parameters.Add(new SqlParameter("@Username", item.Username));
                    command.Parameters.Add(new SqlParameter("@ItemId", item.Id));
                    if (item.Category == ItemCategory.Custom)
                    {
                        command.Parameters.Add(new SqlParameter("@Name", item.Name));
                        command.Parameters.Add(new SqlParameter("@ImageUrl", item.ImageUrl));
                    }
                    command.Parameters.Add(new SqlParameter("@Uses", item.Uses));
                    command.Parameters.Add(new SqlParameter("@Position", position));
                    command.Parameters.Add(new SqlParameter("@Theme", item.Theme));
                    command.Parameters.Add(new SqlParameter("@CreatedDate", item.CreatedDate));
                    command.Parameters.Add(new SqlParameter("@RepairedDate", item.RepairedDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool UpdateSafetyDepositItemWithSelectionId(Guid selectionId, int position)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateSafetyDepositItemWithSelectionId";
                    command.Parameters.Add(new SqlParameter("@SelectionId", selectionId));
                    command.Parameters.Add(new SqlParameter("@Position", position));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool DeleteSafetyDepositItem(Guid id, string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteSafetyDepositItem";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool RepairAllSafetyDepositItems(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "RepairAllSafetyDepositItems";
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("SafetyDepositItems");
        }
    }
}


