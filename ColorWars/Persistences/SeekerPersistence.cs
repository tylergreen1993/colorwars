﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class SeekerPersistence : ISeekerPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public SeekerPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<SeekerChallenge> GetSeekerChallengesWithUsername(string username, bool isCached = true)
        {
            List<SeekerChallenge> results = isCached ? _session.GetObject<List<SeekerChallenge>>("SeekerChallenges") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetSeekerChallengesWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<SeekerChallenge>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("SeekerChallenges", results);
            }
            return results;
        }

        public bool AddSeekerChallenge(string username, Guid itemId, ItemTheme itemTheme, Guid rewardId, ItemTheme rewardTheme)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddSeekerChallenge";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@ItemId", itemId));
                    command.Parameters.Add(new SqlParameter("@ItemTheme", itemTheme));
                    command.Parameters.Add(new SqlParameter("@RewardId", rewardId));
                    command.Parameters.Add(new SqlParameter("@RewardTheme", rewardTheme));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool DeleteSeekerChallengesWithUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteSeekerChallengesWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("SeekerChallenges");
        }
    }
}


