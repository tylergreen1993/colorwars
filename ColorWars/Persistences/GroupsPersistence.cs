﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class GroupsPersistence : IGroupsPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public GroupsPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<Group> GetTopGroups(bool isCached = true)
        {
            List<Group> results = isCached ? _session.GetObject<List<Group>>("TopGroups") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetTopGroups";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Group>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("TopGroups", results);
            }
            return results;
        }

        public List<Group> GetGroupsWithSearchQuery(string searchQuery)
        {
            List<Group> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupsWithSearchQuery";
                    command.Parameters.Add(new SqlParameter("@SearchQuery", searchQuery));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Group>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Group> GetGroupsWithUsername(string username, bool isCached = true)
        {
            List<Group> results = isCached ? _session.GetObject<List<Group>>("UserGroups") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupsWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Group>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("UserGroups", results);
            }
            return results;
        }

        public Group GetGroupWithId(int id)
        {
            List<Group> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupWithId";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Group>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<GroupMember> GetGroupMembersWithGroupId(int id)
        {
            List<GroupMember> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupMembersWithGroupId";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<GroupMember>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<GroupMessage> GetGroupMessagesWithGroupId(int groupId)
        {
            List<GroupMessage> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupMessagesWithGroupId";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<GroupMessage>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<GroupRequest> GetGroupRequestsWithGroupId(int groupId)
        {
            List<GroupRequest> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetGroupRequestsWithGroupId";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<GroupRequest>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Group AddGroup(string username, string name, string displayPicUrl, GroupPower power, string description, bool isPrivateGroup)
        {
            List<Group> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddGroup";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Name", name));
                    command.Parameters.Add(new SqlParameter("@DisplayPicUrl", displayPicUrl));
                    command.Parameters.Add(new SqlParameter("@Power", power));
                    command.Parameters.Add(new SqlParameter("@Description", description));
                    command.Parameters.Add(new SqlParameter("@IsPrivate", isPrivateGroup));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Group>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            _session.Remove("UserGroups");

            return results?.FirstOrDefault();
        }

        public bool AddGroupMember(string username, int groupId, GroupRole role, bool isPrimary)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddGroupMember";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    command.Parameters.Add(new SqlParameter("@Role", role));
                    command.Parameters.Add(new SqlParameter("@IsPrimary", isPrimary));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UserGroups");

                    return true;
                }
            }
        }

        public bool AddGroupMessage(int groupId, string sender, string content)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddGroupMessage";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    command.Parameters.Add(new SqlParameter("@Sender", sender));
                    command.Parameters.Add(new SqlParameter("@Content", content));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool AddGroupRequest(string username, int groupId)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddGroupRequest";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateGroup(Group group)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateGroup";
                    command.Parameters.Add(new SqlParameter("@GroupId", group.Id));
                    command.Parameters.Add(new SqlParameter("@Name", group.Name));
                    command.Parameters.Add(new SqlParameter("@DisplayPicUrl", group.DisplayPicUrl));
                    command.Parameters.Add(new SqlParameter("@Description", group.Description));
                    command.Parameters.Add(new SqlParameter("@Power", group.Power));
                    command.Parameters.Add(new SqlParameter("@IsPrivate", group.IsPrivate));
                    command.Parameters.Add(new SqlParameter("@Whiteboard", group.Whiteboard));
                    command.Parameters.Add(new SqlParameter("@LastPrizeDate", group.LastPrizeDate));
                    command.Parameters.Add(new SqlParameter("@LastPowerChangeDate", group.LastPowerChangeDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UserGroups");

                    return true;
                }
            }
        }

        public bool UpdateGroupMember(GroupMember groupMember)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateGroupMember";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupMember.GroupId));
                    command.Parameters.Add(new SqlParameter("@Username", groupMember.Username));
                    command.Parameters.Add(new SqlParameter("@Role", groupMember.Role));
                    command.Parameters.Add(new SqlParameter("@IsPrizeMember", groupMember.IsPrizeMember));
                    command.Parameters.Add(new SqlParameter("@IsPrimary", groupMember.IsPrimary));
                    command.Parameters.Add(new SqlParameter("@LastReadMessageDate", groupMember.LastReadMessageDate));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UserGroups");

                    return true;
                }
            }
        }

        public bool UpdateGroupMessage(GroupMessage groupMessage)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateGroupMessage";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupMessage.GroupId));
                    command.Parameters.Add(new SqlParameter("@MessageId", groupMessage.Id));
                    command.Parameters.Add(new SqlParameter("@Content", groupMessage.Content));
                    command.Parameters.Add(new SqlParameter("@IsDeleted", groupMessage.IsDeleted));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateGroupRequest(GroupRequest groupRequest)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateGroupRequest";
                    command.Parameters.Add(new SqlParameter("@Username", groupRequest.Username));
                    command.Parameters.Add(new SqlParameter("@GroupId", groupRequest.GroupId));
                    command.Parameters.Add(new SqlParameter("@Accepted", groupRequest.Accepted));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteGroup(int groupId)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteGroup";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UserGroups");

                    return true;
                }
            }
        }

        public bool DeleteGroupMember(int groupId, string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteGroupMember";
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UserGroups");

                    return true;
                }
            }
        }

        public bool DeleteGroupRequest(string username, int groupId)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteGroupRequest";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@GroupId", groupId));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


