﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class DoubtItPersistence : IDoubtItPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public DoubtItPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<DoubtItCard> GetDoubtItCards(string username, bool isCached = true)
        {
            List<DoubtItCard> results = isCached ? _session.GetObject<List<DoubtItCard>>("DoubtItCards") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetDoubtItCards";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<DoubtItCard>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("DoubtItCards", results);
            }
            return results;
        }

        public bool AddDoubtItCards(string username, string amounts, DoubtItOwner owner)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddDoubtItCards";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Amounts", amounts));
                    command.Parameters.Add(new SqlParameter("@Owner", owner));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool UpdateDoubtItCard(Guid id, string username, DoubtItOwner owner, DateTime addedDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateDoubtItCard";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Owner", owner));
                    command.Parameters.Add(new SqlParameter("@AddedDate", addedDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        public bool DeleteDoubtItCards(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteDoubtItCards";
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("DoubtItCards");
        }
    }
}


