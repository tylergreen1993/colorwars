﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class PromoCodesPersistence : IPromoCodesPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public PromoCodesPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public PromoCode GetPromoCode(string code)
        {
            List<PromoCode> results;

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPromoCode";
                    command.Parameters.Add(new SqlParameter("@Code", code));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PromoCode>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<PromoCode> GetPreviousPromoCodes(string username, bool isCached = true)
        {
            List<PromoCode> results = isCached ? _session.GetObject<List<PromoCode>>("PreviousPromoCodes") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPreviousPromoCodes";
                    command.Parameters.Add(new SqlParameter("@Username", username));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PromoCode>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("PreviousPromoCodes", results);
            }

            return results;
        }

        public List<PromoCode> GetAllUnexpiredGlobalPromoCodes(bool isCached = true)
        {
            List<PromoCode> results = isCached ? _session.GetObject<List<PromoCode>>("UnexpiredPromoCodes") : null;
            if (results != null)
            {
                return results;
            }
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAllUnexpiredGlobalPromoCodes";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PromoCode>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            if (isCached)
            {
                _session.SetObject("UnexpiredPromoCodes", results);
            }
            return results;
        }

        public List<PromoCodeHistory> GetPromoCodeHistoryWithUsername(string username, bool isCached = true)
        {
            List<PromoCodeHistory> results = isCached ? _session.GetObject<List<PromoCodeHistory>>("PromoCodeHistory") : null;
            if (results != null)
            {
                return results;
            }

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPromoCodeHistoryWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PromoCodeHistory>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            if (isCached)
            {
                _session.SetObject("PromoCodeHistory", results);
            }
            return results;
        }

        public PromoCode RedeemPromoCode(string username, string code)
        {
            List<PromoCode> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "RedeemPromoCode";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Code", code));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PromoCode>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            ClearCache();
            return results?.FirstOrDefault();
        }

        public bool AddPromoCode(string code, string creator, PromoEffect effect, bool oneUserUse, bool isSponsor, Guid itemId, DateTime expiryDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddPromoCode";
                    command.Parameters.Add(new SqlParameter("@Code", code));
                    command.Parameters.Add(new SqlParameter("@Creator", creator));
                    command.Parameters.Add(new SqlParameter("@Effect", effect));
                    command.Parameters.Add(new SqlParameter("@OneUserUse", oneUserUse));
                    command.Parameters.Add(new SqlParameter("@IsSponsor", isSponsor));
                    command.Parameters.Add(new SqlParameter("@ItemId", itemId));
                    command.Parameters.Add(new SqlParameter("@ExpiryDate", expiryDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    _session.Remove("UnexpiredPromoCodes");

                    return true;
                }
            }
        }

        public bool AddPromoCodeHistory(string username, string code, string message)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddPromoCodeHistory";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Code", code));
                    command.Parameters.Add(new SqlParameter("@Message", message));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    ClearCache();
                    return true;
                }
            }
        }

        private void ClearCache()
        {
            _session.Remove("PreviousPromoCodes");
            _session.Remove("PromoCodeHistory");
        }
    }
}


