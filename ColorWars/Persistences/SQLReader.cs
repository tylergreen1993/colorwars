﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using ColorWars.Models;
using System.Reflection;
using System.Linq;

namespace ColorWars.Persistences
{
    public class SQLReader: ISQLReader
    {
       public List<T> GetResults<T>(SqlCommand command) 
       where T : class
       {
            List<T> results = new List<T>();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Result result = new Result();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        result[reader.GetName(i)] = reader.GetValue(i);
                    }
                    results.Add(ObjectFromResult<T>(result, false));
                }
            }
            return results;
        }

        public T GetSettingsResults<T>(SqlCommand command)
        where T : class
        {
            Result result = new Result();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    string name = string.Empty;
                    object value = null;

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (reader.GetName(i) == "Name")
                        {
                            name = reader.GetValue(i).ToString();
                        }
                        if (reader.GetName(i) == "Value")
                        {
                            value = reader.GetValue(i);
                        }
                    }
                    result[name] = value;
                }
            }

            return ObjectFromResult<T>(result, true);
        }

        private T ObjectFromResult<T>(Result result, bool fromSetting)
        where T : class
        {
            Type type = typeof(T);
            T obj = (T)Activator.CreateInstance(type);
            foreach (KeyValuePair<string, object> item in result)
            {
                PropertyInfo property = type.GetProperty(item.Key);
                if (property != null && item.Value != DBNull.Value)
                {
                    Type propertyType = property.PropertyType;
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        propertyType = propertyType.GetGenericArguments().First();
                    }
                    object value = fromSetting ?  (propertyType.IsEnum ? Enum.Parse(propertyType, item.Value.ToString()) : Convert.ChangeType(item.Value, propertyType)) : item.Value;
                    if (propertyType.IsEnum)
                    {
                        value = Enum.ToObject(propertyType, value);
                    }
                    property.SetValue(obj, value, null);
                }
            }
            return obj;
        }
    }
}