﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class UserPersistence : IUserPersistence
    {
        private ISQLReader _sqlReader;

        public UserPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public User GetUserWithUsernameAndPassword(string username, string password, string ipAddress, string userAgent, bool performUpdate = true)
        {
            List<User> results;

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUserWithUsernameAndPassword";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Password", Helper.Hash(password)));
                    command.Parameters.Add(new SqlParameter("@IPAddress", ipAddress));
                    command.Parameters.Add(new SqlParameter("@UserAgent", userAgent));
                    command.Parameters.Add(new SqlParameter("@PerformUpdate", performUpdate));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public User GetUserWithAccessToken(string username, Guid accessToken)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUserWithAccessToken";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@AccessToken", accessToken));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public User CreateNewUser(string username, string password, string emailAddress, bool allowNotifications, string referral, string ipAddress, string userAgent)
        {
            User user = new User
            {
                Username = username,
                AccessToken = Guid.NewGuid(),
                EmailAddress = emailAddress,
                AllowNotifications = allowNotifications,
                Referral = referral,
                IPAddress = ipAddress
            };

            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "CreateNewUser";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Password", Helper.Hash(password)));
                    command.Parameters.Add(new SqlParameter("@AccessToken", user.AccessToken));
                    command.Parameters.Add(new SqlParameter("@EmailAddress", emailAddress));
                    command.Parameters.Add(new SqlParameter("@AllowNotifications", allowNotifications));
                    command.Parameters.Add(new SqlParameter("@Referral", referral));
                    command.Parameters.Add(new SqlParameter("@IPAddress", ipAddress));
                    command.Parameters.Add(new SqlParameter("@UserAgent", userAgent));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return user;
                }
            }
        }

        public User GetUserWithUsername(string username)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUserWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public User GetUserWithEmail(string email)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUserWithEmail";
                    command.Parameters.Add(new SqlParameter("@EmailAddress", email));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }


        public Count GetDAUMAUStatistic()
        {
            List<Count> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetDAUMAUStatistic";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Count>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<User> GetUsersByIPAddress(string ipAddress, bool includeSessions)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUsersByIPAddress";
                    command.Parameters.Add(new SqlParameter("@IPAddress", ipAddress));
                    command.Parameters.Add(new SqlParameter("@IncludeSessions", includeSessions));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetUsersWithAdminRoles()
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUsersWithAdminRoles";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetRecentUsers(Gender gender)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetRecentUsers";
                    command.Parameters.Add(new SqlParameter("@Gender", gender));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetReturningUsers(Gender gender)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetReturningUsers";
                    command.Parameters.Add(new SqlParameter("@Gender", gender));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }


        public List<User> GetAllUsers(Gender gender, int days)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAllUsers";
                    command.Parameters.Add(new SqlParameter("@Gender", gender));
                    command.Parameters.Add(new SqlParameter("@Days", days));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetActiveUsers(int minutes)
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetActiveUsers";
                    command.Parameters.Add(new SqlParameter("@Minutes", minutes));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetRecentlyReturningUsers()
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetRecentlyReturningUsers";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<User> GetNonReturningUsers()
        {
            List<User> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetNonReturningUsers";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<User>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<ActiveSession> GetAccessTokensWithUsername(string username)
        {
            List<ActiveSession> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAccessTokensWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<ActiveSession>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<BannedIPAddress> GetBannedIPAddresses()
        {
            List<BannedIPAddress> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetBannedIPAddresses";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<BannedIPAddress>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public bool AddBannedIPAddress(string ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddBannedIPAddress";
                    command.Parameters.Add(new SqlParameter("@IPAddress", ipAddress));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateUserWithUsername(User user, string password)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateUserWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", user.Username));
                    command.Parameters.Add(new SqlParameter("@EmailAddress", user.EmailAddress));
                    command.Parameters.Add(new SqlParameter("@Gender", user.Gender));
                    command.Parameters.Add(new SqlParameter("@FirstName", user.FirstName));
                    command.Parameters.Add(new SqlParameter("@Location", user.Location));
                    command.Parameters.Add(new SqlParameter("@Bio", user.Bio));
                    command.Parameters.Add(new SqlParameter("@Color", user.Color));
                    command.Parameters.Add(new SqlParameter("@DisplayPicUrl", user.DisplayPicUrl));
                    command.Parameters.Add(new SqlParameter("@AllowNotifications", user.AllowNotifications));
                    command.Parameters.Add(new SqlParameter("@FlushSession", user.FlushSession));
                    command.Parameters.Add(new SqlParameter("@LoginAttempts", user.LoginAttempts));
                    command.Parameters.Add(new SqlParameter("@LoginAttemptDate", user.LoginAttemptDate));
                    command.Parameters.Add(new SqlParameter("@Role", user.Role));
                    command.Parameters.Add(new SqlParameter("@InactiveType", user.InactiveType));
                    command.Parameters.Add(new SqlParameter("@ForgotEmailDate", user.ForgotEmailDate));
                    command.Parameters.Add(new SqlParameter("@SuspendedDate", user.SuspendedDate));
                    command.Parameters.Add(new SqlParameter("@PremiumExpiryDate", user.PremiumExpiryDate));
                    command.Parameters.Add(new SqlParameter("@AdFreeExpiryDate", user.AdFreeExpiryDate));
                    command.Parameters.Add(new SqlParameter("@Password", string.IsNullOrEmpty(password) ? string.Empty : Helper.Hash(password)));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool LogoutUserWithAccessToken(User user)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "LogoutUserWithAccessToken";
                    command.Parameters.Add(new SqlParameter("@Username", user.Username));
                    command.Parameters.Add(new SqlParameter("@AccessToken", user.AccessToken));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteAccessTokens(User user)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteAccessTokens";
                    command.Parameters.Add(new SqlParameter("@Username", user.Username));
                    command.Parameters.Add(new SqlParameter("@AccessToken", user.AccessToken));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool RemoveBannedIPAddress(string ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "RemoveBannedIPAddress";
                    command.Parameters.Add(new SqlParameter("@IPAddress", ipAddress));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeleteInformationWithUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteInformationWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}
