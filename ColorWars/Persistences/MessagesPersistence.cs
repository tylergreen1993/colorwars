﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class MessagesPersistence : IMessagesPersistence
    {
        private ISQLReader _sqlReader;

        public MessagesPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<Message> GetAllMessagesWithUsername(string username, int topCount)
        {
            List<Message> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAllMessagesWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@TopCount", topCount));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Message>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Message> GetMessagesBetweenUsernames(string username, string correspondent, bool updateRead = true)
        {
            List<Message> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetMessagesBetweenUsernames";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Correspondent", correspondent));
                    command.Parameters.Add(new SqlParameter("@UpdateRead", updateRead));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Message>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public int GetUnreadMessagesCountWithUsername(string username, string correspondent)
        {
            List<object> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetUnreadMessagesCountWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Correspondent", correspondent));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<object>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return 0;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results.Count;
        }

        public Message AddMessage(string sender, string recipient, string content)
        {
            List<Message> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddMessage";
                    command.Parameters.Add(new SqlParameter("@Sender", sender));
                    command.Parameters.Add(new SqlParameter("@Recipient", recipient));
                    command.Parameters.Add(new SqlParameter("@Content", content));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Message>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public bool DeleteMessagesBetweenUsernames(string username, string correspondent)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeleteMessagesBetweenUsernames";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Correspondent", correspondent));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return true;
                }
            }
        }
    }
}
