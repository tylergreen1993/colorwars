﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class AuctionsPersistence : IAuctionsPersistence
    {
        private ISQLReader _sqlReader;

        public AuctionsPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<Auction> GetAuctions()
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAuctions";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Auction> GetPastAuctions()
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPastAuctions";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Auction> GetAuctionsWithSearch(string query, ItemTheme theme)
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAuctionsWithSearch";
                    command.Parameters.Add(new SqlParameter("@Query", query));
                    command.Parameters.Add(new SqlParameter("@Theme", theme));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<Auction> GetAuctionsWithUsername(string username)
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAuctionsWithUsername";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Auction GetAuctionWithAuctionId(int auctionId)
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAuctionWithAuctionId";
                    command.Parameters.Add(new SqlParameter("@AuctionId", auctionId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public List<Bid> GetBidsWithAuctionId(int auctionId)
        {
            List<Bid> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetBidsWithAuctionId";
                    command.Parameters.Add(new SqlParameter("@AuctionId", auctionId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Bid>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public Item GetAuctionItemWithAuctionId(int auctionId)
        {
            List<Item> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetAuctionItemWithAuctionId";
                    command.Parameters.Add(new SqlParameter("@AuctionId", auctionId));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Item>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public bool AddBid(int auctionId, string username, int points)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddBid";
                    command.Parameters.Add(new SqlParameter("@AuctionId", auctionId));
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Points", points));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public Auction AddAuction(string username, int startPoints, int minBidIncrement, Guid itemId, DateTime endDate)
        {
            List<Auction> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddAuction";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@StartPoints", startPoints));
                    command.Parameters.Add(new SqlParameter("@MinBidIncrement", minBidIncrement));
                    command.Parameters.Add(new SqlParameter("@ItemId", itemId));
                    command.Parameters.Add(new SqlParameter("@EndDate", endDate));

                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<Auction>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results?.FirstOrDefault();
        }

        public bool AddAuctionItem(Item item)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddAuctionItem";
                    command.Parameters.Add(new SqlParameter("@SelectionId", item.SelectionId));
                    command.Parameters.Add(new SqlParameter("@ItemId", item.Id));
                    if (item.Category == ItemCategory.Custom)
                    {
                        command.Parameters.Add(new SqlParameter("@Name", item.Name));
                        command.Parameters.Add(new SqlParameter("@ImageUrl", item.ImageUrl));
                    }
                    command.Parameters.Add(new SqlParameter("@Uses", item.Uses));
                    command.Parameters.Add(new SqlParameter("@Theme", item.Theme));
                    command.Parameters.Add(new SqlParameter("@CreatedDate", item.CreatedDate));
                    command.Parameters.Add(new SqlParameter("@RepairedDate", item.RepairedDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool UpdateAuction(int id, int startPoints, bool creatorCollected, bool winnerCollected, bool sentNotification, DateTime endDate)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "UpdateAuction";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    command.Parameters.Add(new SqlParameter("@StartPoints", startPoints));
                    command.Parameters.Add(new SqlParameter("@CreatorCollected", creatorCollected));
                    command.Parameters.Add(new SqlParameter("@WinnerCollected", winnerCollected));
                    command.Parameters.Add(new SqlParameter("@SentNotification", sentNotification));
                    command.Parameters.Add(new SqlParameter("@EndDate", endDate));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


