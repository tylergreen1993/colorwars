﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class RightSideUpWorldPersistence : IRightSideUpWorldPersistence
    {
        private ISQLReader _sqlReader;

        public RightSideUpWorldPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<HallOfFameEntry> GetRightSideUpGuardHallOfFame()
        {
            List<HallOfFameEntry> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetRightSideUpGuardHallOfFame";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<HallOfFameEntry>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public bool AddRightSideUpGuardHallOfFame(string username)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddRightSideUpGuardHallOfFame";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return true;
                }
            }
        }
    }
}


