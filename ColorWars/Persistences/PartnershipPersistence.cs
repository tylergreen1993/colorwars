﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;
using Microsoft.AspNetCore.Http;

namespace ColorWars.Persistences
{
    public class PartnershipPersistence : IPartnershipPersistence
    {
        private IHttpContextAccessor _httpContextAccessor;
        private ISQLReader _sqlReader;
        private ISession _session;

        public PartnershipPersistence(IHttpContextAccessor httpContextAccessor, ISQLReader sqlReader)
        {
            _httpContextAccessor = httpContextAccessor;
            _sqlReader = sqlReader;
            _session = _httpContextAccessor.HttpContext.Session;
        }

        public List<PartnershipRequest> GetPartnershipRequests(string username)
        {
            List<PartnershipRequest> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPartnershipRequests";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PartnershipRequest>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public List<PartnershipEarning> GetPartnershipEarnings(string username, string partnershipUsername, bool isCached = true)
        {
            List<PartnershipEarning> results = isCached ? _session.GetObject<List<PartnershipEarning>>("PartnershipEarnings") : null;
            if (results != null)
            {
                return results;
            }
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetPartnershipEarnings";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@PartnershipUsername", partnershipUsername));
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<PartnershipEarning>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            if (isCached)
            {
                _session.SetObject("PartnershipEarnings", results);
            }
            return results;
        }

        public bool AddPartnershipRequest(string username, string requestUsername)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddPartnershipRequest";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@RequestUsername", requestUsername));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool AddPartnershipEarning(string username, string partnershipUsername, int amount)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddPartnershipEarning";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@PartnershipUsername", partnershipUsername));
                    command.Parameters.Add(new SqlParameter("@Amount", amount));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }

        public bool DeletePartnershipRequest(string username, string requestUsername)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "DeletePartnershipRequest";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@RequestUsername", requestUsername));
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


