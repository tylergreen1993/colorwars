﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace ColorWars.Persistences
{
    public static class SQLAuthenticate
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder;

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                builder = new SqlConnectionStringBuilder
                {
                    DataSource = ConfigurationManager.AppSettings.Get("DevelopmentDbDataSource"),
                    UserID = ConfigurationManager.AppSettings.Get("DevelopmentDbUsername"),
                    Password = ConfigurationManager.AppSettings.Get("DevelopmentDbPassword"),
                    InitialCatalog = ConfigurationManager.AppSettings.Get("DevelopmentDbCatalog")
                };
            }
            else
            {
                builder = new SqlConnectionStringBuilder
                {
                    DataSource = ConfigurationManager.AppSettings.Get("ProductionDbDataSource"),
                    UserID = ConfigurationManager.AppSettings.Get("ProductionDbUsername"),
                    Password = ConfigurationManager.AppSettings.Get("ProductionDbPassword"),
                    InitialCatalog = ConfigurationManager.AppSettings.Get("ProductionDbCatalog")
                };
            }

            return builder.ConnectionString;
        }
    }
}
