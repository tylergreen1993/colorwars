﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ColorWars.Classes;
using ColorWars.Models;

namespace ColorWars.Persistences
{
    public class WealthyDonorsPersistence : IWealthyDonorsPersistence
	{
        private ISQLReader _sqlReader;

		public WealthyDonorsPersistence(ISQLReader sqlReader)
        {
            _sqlReader = sqlReader;
        }

        public List<WealthyDonor> GetWealthyDonors()
        {
            List<WealthyDonor> results;
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetWealthyDonors";
                    try
                    {
                        connection.Open();
                        results = _sqlReader.GetResults<WealthyDonor>(command);
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return null;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return results;
        }

        public bool AddWealthyDonor(string username, int amount)
        {
            using (SqlConnection connection = new SqlConnection(SQLAuthenticate.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "AddWealthyDonor";
                    command.Parameters.Add(new SqlParameter("@Username", username));
                    command.Parameters.Add(new SqlParameter("@Amount", amount));

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        Log.Debug(e.ToString());
                        Messages.AddMessage(Resources.CannotConnectToDatabase);
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return true;
                }
            }
        }
    }
}


