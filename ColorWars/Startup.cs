﻿using System;
using System.Configuration;
using ColorWars.Classes;
using ColorWars.Persistences;
using ColorWars.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Stripe;

namespace ColorWars
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(12);
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.Secure = CookieSecurePolicy.Always;
            });

            services.AddApplicationInsightsTelemetry();
            services.AddMvc(option => option.EnableEndpointRouting = false);
            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddHttpContextAccessor();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            RegisterServices(services);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings.Get("DevelopmentStripeSecretKey");
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
                StripeConfiguration.ApiKey = ConfigurationManager.AppSettings.Get("ProductionStripeSecretKey");
            }

            int maxAgeSeconds = 60 * 60 * 2;
            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                context.Context.Response.Headers.Add("Cache-Control", $"public, max-age={maxAgeSeconds}")
            });
            app.UseCookiePolicy();
            app.UseSession();
            app.UseStatusCodePagesWithRedirects("/Error/{0}");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    "Profile",
                    "User/{id}",
                    new { controller = "Profile", action = "Index" });
                routes.MapRoute(
                    "Locations",
                    "{id}",
                    new { controller = "Locations", action = "Index" });
            });

            GlobalHttpContext.Services = app.ApplicationServices;
            ServicesLocator.Services = app.ApplicationServices;
        }

        private void RegisterServices(IServiceCollection services)
        {
            //Repositories
            services.AddTransient<ILoginRepository, LoginRepository>();
            services.AddTransient<IItemsRepository, ItemsRepository>();
            services.AddTransient<ILogoutRepository, LogoutRepository>();
            services.AddTransient<IMatchStateRepository, MatchStateRepository>();
            services.AddTransient<IMatchHistoryRepository, MatchHistoryRepository>();
            services.AddTransient<IPointsRepository, PointsRepository>();
            services.AddTransient<IRegisterRepository, RegisterRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ISettingsRepository, SettingsRepository>();
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IStoreRepository, StoreRepository>();
            services.AddTransient<IJunkyardRepository, JunkyardRepository>();
            services.AddTransient<IAuctionsRepository, AuctionsRepository>();
            services.AddTransient<IAccomplishmentsRepository, AccomplishmentsRepository>();
            services.AddTransient<ISeekerRepository, SeekerRepository>();
            services.AddTransient<IDonationsRepository, DonationsRepository>();
            services.AddTransient<IPasswordResetRepository, PasswordResetRepository>();
            services.AddTransient<IEmailRepository, EmailRepository>();
            services.AddTransient<IShowcaseRepository, ShowcaseRepository>();
            services.AddTransient<ISafetyDepositRepository, SafetyDepositRepository>();
            services.AddTransient<IMessagesRepository, MessagesRepository>();
            services.AddTransient<IWishingStoneRepository, WishingStoneRepository>();
            services.AddTransient<IBlockedRepository, BlockedRepository>();
            services.AddTransient<IReportsRepository, ReportsRepository>();
            services.AddTransient<IBuyersClubRepository, BuyersClubRepository>();
            services.AddTransient<IStocksRepository, StocksRepository>();
            services.AddTransient<IEffectsRepository, EffectsRepository>();
            services.AddTransient<IPromoCodesRepository, PromoCodesRepository>();
            services.AddTransient<IReferralsRepository, ReferralsRepository>();
            services.AddTransient<IWizardRepository, WizardRepository>();
            services.AddTransient<INewsRepository, NewsRepository>();
            services.AddTransient<IJobsRepository, JobsRepository>();
            services.AddTransient<ITradeRepository, TradeRepository>();
            services.AddTransient<IContactsRepository, ContactsRepository>();
            services.AddTransient<INotificationsRepository, NotificationsRepository>();
            services.AddTransient<IStatisticsRepository, StatisticsRepository>();
            services.AddTransient<IGroupsRepository, GroupsRepository>();
            services.AddTransient<IFeedRepository, FeedRepository>();
            services.AddTransient<IChecklistRepository, ChecklistRepository>();
            services.AddTransient<ILottoRepository, LottoRepository>();
            services.AddTransient<ISuspensionRepository, SuspensionRepository>();
            services.AddTransient<IAdminHistoryRepository, AdminHistoryRepository>();
            services.AddTransient<IStreakCardsRepository, StreakCardsRepository>();
            services.AddTransient<ISwapDeckRepository, SwapDeckRepository>();
            services.AddTransient<IMarketStallRepository, MarketStallRepository>();
            services.AddTransient<ILocationsRepository, LocationsRepository>();
            services.AddTransient<ILibraryRepository, LibraryRepository>();
            services.AddTransient<IAttentionHallRepository, AttentionHallRepository>();
            services.AddTransient<IClubRepository, ClubRepository>();
            services.AddTransient<IRewardsStoreRepository, RewardsStoreRepository>();
            services.AddTransient<INoticeBoardTasksRepository, NoticeBoardTasksRepository>();
            services.AddTransient<IDisposerRepository, DisposerRepository>();
            services.AddTransient<ICraftingRepository, CraftingRepository>();
            services.AddTransient<IStoreDiscountRepository, StoreDiscountRepository>();
            services.AddTransient<IWarehouseRepository, WarehouseRepository>();
            services.AddTransient<ICompanionsRepository, CompanionsRepository>();
            services.AddTransient<ICavernsRepository, CavernsRepository>();
            services.AddTransient<IRightSideUpWorldRepository, RightSideUpWorldRepository>();
            services.AddTransient<ISecretGifterRepository, SecretGifterRepository>();
            services.AddTransient<IProfileRepository, ProfileRepository>();
            services.AddTransient<ISurveyRepository, SurveyRepository>();
            services.AddTransient<IMuseumRepository, MuseumRepository>();
            services.AddTransient<ICaptchaRepository, CaptchaRepository>();
            services.AddTransient<IAdminSettingsRepository, AdminSettingsRepository>();
            services.AddTransient<IPartnershipRepository, PartnershipRepository>();
            services.AddTransient<IGuideRepository, GuideRepository>();
            services.AddTransient<IRepairRepository, RepairRepository>();
            services.AddTransient<IColorWarsRepository, ColorWarsRepository>();
            services.AddTransient<ISpinSuccessRepository, SpinSuccessRepository>();
            services.AddTransient<ITakeOfferRepository, TakeOfferRepository>();
            services.AddTransient<ITreasureDiveRepository, TreasureDiveRepository>();
            services.AddTransient<ITowerRepository, TowerRepository>();
            services.AddTransient<IDonorRepository, DonorRepository>();
            services.AddTransient<IWealthyDonorsRepository, WealthyDonorsRepository>();
            services.AddTransient<ICPURepository, CPURepository>();
            services.AddTransient<IPowerMovesRepository, PowerMovesRepository>();
            services.AddTransient<ITrainingCenterRepository, TrainingCenterRepository>();
            services.AddTransient<ITourneyRepository, TourneyRepository>();
            services.AddTransient<ISoundsRepository, SoundsRepository>();
            services.AddTransient<IRelicsRepository, RelicsRepository>();
            services.AddTransient<IDoubtItRepository, DoubtItRepository>();
            services.AddTransient<ICellarStoreRepository, CellarStoreRepository>();
            services.AddTransient<ILuckyGuessRepository, LuckyGuessRepository>();
            services.AddTransient<IArtFactoryRepository, ArtFactoryRepository>();
            services.AddTransient<IBadgeRepository, BadgeRepository>();
            services.AddTransient<IGoogleStorageRepository, GoogleStorageRepository>();

            //Persistences
            services.AddTransient<IItemsPersistence, ItemsPersistence>();
            services.AddTransient<IMatchStatePersistence, MatchStatePersistence>();
            services.AddTransient<IPointsPersistence, PointsPersistence>();
            services.AddTransient<IUserPersistence, UserPersistence>();
            services.AddTransient<ISettingsPersistence, SettingsPersistence>();
            services.AddTransient<IMatchHistoryPersistence, MatchHistoryPersistence>();
            services.AddTransient<IBankPersistence, BankPersistence>();
            services.AddTransient<IStorePersistence, StorePersistence>();
            services.AddTransient<IJunkyardPersistence, JunkyardPersistence>();
            services.AddTransient<IAuctionsPersistence, AuctionsPersistence>();
            services.AddTransient<IAccomplishmentsPersistence, AccomplishmentsPersistence>();
            services.AddTransient<ISeekerPersistence, SeekerPersistence>();
            services.AddTransient<IDonationsPersistence, DonationsPersistence>();
            services.AddTransient<IPasswordResetPersistence, PasswordResetPersistence>();
            services.AddTransient<IShowcasePersistence, ShowcasePersistence>();
            services.AddTransient<ISafetyDepositPersistence, SafetyDepositPersistence>();
            services.AddTransient<IMessagesPersistence, MessagesPersistence>();
            services.AddTransient<IWishingStonePersistence, WishingStonePersistence>();
            services.AddTransient<IBlockedPersistence, BlockedPersistence>();
            services.AddTransient<IReportsPersistence, ReportsPersistence>();
            services.AddTransient<IBuyersClubPersistence, BuyersClubPersistence>();
            services.AddTransient<IStocksPersistence, StocksPersistence>();
            services.AddTransient<IPromoCodesPersistence, PromoCodesPersistence>();
            services.AddTransient<IReferralsPersistence, ReferralsPersistence>();
            services.AddTransient<IWizardPersistence, WizardPersistence>();
            services.AddTransient<INewsPersistence, NewsPersistence>();
            services.AddTransient<IJobsPersistence, JobsPersistence>();
            services.AddTransient<ITradePersistence, TradePersistence>();
            services.AddTransient<IContactsPersistence, ContactsPersistence>();
            services.AddTransient<INotificationsPersistence, NotificationsPersistence>();
            services.AddTransient<IStatisticsPersistence, StatisticsPersistence>();
            services.AddTransient<IGroupsPersistence, GroupsPersistence>();
            services.AddTransient<ILottoPersistence, LottoPersistence>();
            services.AddTransient<ISuspensionPersistence, SuspensionPersistence>();
            services.AddTransient<IAdminHistoryPersistence, AdminHistoryPersistence>();
            services.AddTransient<IStreakCardsPersistence, StreakCardsPersistence>();
            services.AddTransient<IInactiveAccountsPersistence, InactiveAccountsPersistence>();
            services.AddTransient<ISwapDeckPersistence, SwapDeckPersistence>();
            services.AddTransient<IMarketStallPersistence, MarketStallPersistence>();
            services.AddTransient<ILocationsPersistence, LocationsPersistence>();
            services.AddTransient<ILibraryPersistence, LibraryPersistence>();
            services.AddTransient<IAttentionHallPersistence, AttentionHallPersistence>();
            services.AddTransient<IClubPersistence, ClubPersistence>();
            services.AddTransient<INoticeBoardTasksPersistence, NoticeBoardTasksPersistence>();
            services.AddTransient<IDisposerPersistence, DisposerPersistence>();
            services.AddTransient<ICraftingPersistence, CraftingPersistence>();
            services.AddTransient<IWarehousePersistence, WarehousePersistence>();
            services.AddTransient<ICompanionsPersistence, CompanionsPersistence>();
            services.AddTransient<ICavernsPersistence, CavernsPersistence>();
            services.AddTransient<IRightSideUpWorldPersistence, RightSideUpWorldPersistence>();
            services.AddTransient<ISecretGifterPersistence, SecretGifterPersistence>();
            services.AddTransient<IProfilePersistence, ProfilePersistence>();
            services.AddTransient<ISurveyPersistence, SurveyPersistence>();
            services.AddTransient<IAdminSettingsPersistence, AdminSettingsPersistence>();
            services.AddTransient<IPartnershipPersistence, PartnershipPersistence>();
            services.AddTransient<IColorWarsPersistence, ColorWarsPersistence>();
            services.AddTransient<ISpinSuccessPersistence, SpinSuccessPersistence>();
            services.AddTransient<ITakeOfferPersistence, TakeOfferPersistence>();
            services.AddTransient<ITreasureDivePersistence, TreasureDivePersistence>();
            services.AddTransient<IPowerMovesPersistence, PowerMovesPersistence>();
            services.AddTransient<ITowerPersistence, TowerPersistence>();
            services.AddTransient<IDonorPersistence, DonorPersistence>();
            services.AddTransient<IWealthyDonorsPersistence, WealthyDonorsPersistence>();
            services.AddTransient<ITourneyPersistence, TourneyPersistence>();
            services.AddTransient<IDoubtItPersistence, DoubtItPersistence>();
            services.AddTransient<ILuckyGuessPersistence, LuckyGuessPersistence>();
            services.AddTransient<IArtFactoryPersistence, ArtFactoryPersistence>();
            services.AddTransient<IFeedPersistence, FeedPersistence>();
            services.AddTransient<ISQLReader, SQLReader>();
        }
    }
}
