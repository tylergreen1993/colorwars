using ColorWars.Models;
using ColorWars.Persistences;
using ColorWars.Repositories;
using Moq;
using NUnit.Framework;

namespace Tests.Unit.Repositories
{
    public class UserRepositoryTests
    {
        private IUserRepository _userRepository;
        private Mock<IUserPersistence> _userPersistenceMock;
        private Mock<IInactiveAccountsPersistence> _inactiveAccountsPersistenceMock;
        private Mock<ILogoutRepository> _logoutRepositoryMock;

        [SetUp]
        public void Setup()
        {
            _userPersistenceMock = new Mock<IUserPersistence>();
            _inactiveAccountsPersistenceMock = new Mock<IInactiveAccountsPersistence>();
            _logoutRepositoryMock = new Mock<ILogoutRepository>();
            _userRepository = new UserRepository(_userPersistenceMock.Object, _inactiveAccountsPersistenceMock.Object, _logoutRepositoryMock.Object);
        }

        [Test]
        public void TestGetUser()
        {
            User user = new User
            {
                Username = "User",
                InactiveType = InactiveAccountType.Deleted
            };

            _userPersistenceMock.Setup(x => x.GetUserWithUsername(user.Username)).Returns(user);

            Assert.That(_userRepository.GetUser(string.Empty, true), Is.EqualTo(null));
            Assert.That(_userRepository.GetUser(user.Username, true), Is.EqualTo(null));
            Assert.That(_userRepository.GetUser(user.Username, false), Is.EqualTo(user));

            user.InactiveType = InactiveAccountType.None;
            Assert.That(_userRepository.GetUser(user.Username, true), Is.EqualTo(user));
        }

        [Test]
        public void TestGetUserWithEmail()
        {
            User user = new User
            {
                Username = "User",
                EmailAddress = "user@user.com"
            };

            _userPersistenceMock.Setup(x => x.GetUserWithEmail(user.EmailAddress)).Returns(user);

            Assert.That(_userRepository.GetUserWithEmail(user.EmailAddress, true), Is.EqualTo(user));
        }

        [Test]
        public void TestUpdateUser()
        {
            User user = new User
            {
                Username = "User"
            };

            _userPersistenceMock.Setup(x => x.UpdateUserWithUsername(user, It.IsAny<string>())).Returns(true);

            Assert.That(_userRepository.UpdateUser(null), Is.EqualTo(false));
            Assert.That(_userRepository.UpdateUser(user), Is.EqualTo(true));
        }
    }
}