CREATE PROCEDURE UpdateGroupRequest
    @Username varchar(255),
    @GroupId int,
    @Accepted bit
AS  
    UPDATE GroupRequests
    SET Accepted = @Accepted
    WHERE Username = @Username
    AND GroupId = @GroupId