CREATE PROCEDURE GetGroupMessagesWithGroupId
    @GroupId int
AS  
    SELECT TOP 150 gm.*, u.Color, u.DisplayPicUrl FROM GroupMessages gm
    JOIN Users u ON
    gm.Sender = u.Username
    WHERE GroupId = @GroupId
    ORDER BY CreatedDate DESC