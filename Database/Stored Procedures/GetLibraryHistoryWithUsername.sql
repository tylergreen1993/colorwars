CREATE PROCEDURE GetLibraryHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM LibraryHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC