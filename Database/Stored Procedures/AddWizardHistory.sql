CREATE PROCEDURE AddWizardHistory
    @Username varchar(255),
    @Type int,
    @Message varchar(MAX)
AS  
    INSERT INTO WizardHistory(Id, Username, Type, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Type, @Message, GETDATE())