CREATE PROCEDURE UpdateBank
    @Username nvarchar(255),   
    @Points int
AS    
IF EXISTS(SELECT * FROM Bank WHERE Username = @Username)
BEGIN
    UPDATE Bank
    SET Points = @Points,
    UpdatedDate = GETDATE()
    WHERE Username = @Username
END
ELSE
BEGIN
    INSERT INTO Bank(Username, Points, UpdatedDate)
    VALUES (@Username, @Points, GETDATE())
END