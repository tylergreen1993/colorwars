CREATE PROCEDURE GetAllColorWarsSubmissionsWithTeamColor
    @TeamColor int
AS  
    SELECT * FROM ColorWarsSubmissions
    WHERE TeamColor = @TeamColor
    ORDER BY CreatedDate DESC