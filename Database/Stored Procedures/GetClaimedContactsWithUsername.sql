CREATE PROCEDURE GetClaimedContactsWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM Contacts
    WHERE ClaimedUser = @Username
    AND IsCompleted = 0
    ORDER BY CreatedDate DESC