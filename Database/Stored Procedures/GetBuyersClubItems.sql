CREATE PROCEDURE GetBuyersClubItems
AS  
    SELECT *
    FROM BuyersClubItems b
    JOIN Items i
    ON b.Id = i.Id
    ORDER BY b.Cost, i.Category, i.Name ASC