CREATE PROCEDURE RemoveStock
    @SelectionId uniqueidentifier,
    @Username varchar(255),
    @Amount int
AS  
    DECLARE @Id int
    SET @Id = (SELECT ItemId FROM UserStocks WHERE Username = @Username AND SelectionId = @SelectionId)
    IF @Id IS NOT NULL
    BEGIN
        UPDATE UserStocks
        SET UserAmount = UserAmount - @Amount
        WHERE Username = @Username
        AND SelectionId = @SelectionId

        DELETE FROM UserStocks
        Where UserAmount <= 0
        
        UPDATE Stocks
        SET CurrentCost = CurrentCost - CAST(@Amount AS float)/7
        WHERE Id = @Id

        SELECT * FROM Stocks WHERE Id = @Id
    END