CREATE PROCEDURE AddInactiveAccount
    @Username varchar(255),
    @Reason int,
    @Type int,
    @Info varchar(max) = ''
AS  
	INSERT INTO InactiveAccounts(Username, Reason, Type, Info, InactiveDate)
    VALUES (@Username, @Reason, @Type, @Info, GETDATE())