CREATE PROCEDURE AddPartnershipRequest
    @RequestUsername varchar(255),
    @Username varchar(255)
AS  
    INSERT INTO PartnershipRequests(RequestUsername, Username, CreatedDate)
    VALUES(@RequestUsername, @Username, GETDATE())