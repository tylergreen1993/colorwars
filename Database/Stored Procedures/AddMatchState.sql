CREATE PROCEDURE AddMatchState
    @Username nvarchar(255),
    @Opponent nvarchar(255),
    @CardId uniqueidentifier,
    @EnhancerId uniqueidentifier,
    @PowerMove int,
    @CPUCardId uniqueidentifier,
    @CPUEnhancerId uniqueidentifier,
    @CPUCardTheme int,
    @CPUPowerMove int,
    @Position int,
    @Points int,
    @Status int,
    @ImageUrl varchar(MAX) = '',
    @IsInSelection bit,
    @IsCPU bit,
    @IsCPUChallenge bit,
    @IsSpecialCPU bit,
    @UpdatedDate datetime,
    @TourneyMatchId uniqueidentifier = NULL
AS  
    INSERT INTO MatchState(Id, Username, Opponent, CardId, EnhancerId, PowerMove, CPUCardId, CPUEnhancerId, TourneyMatchId, CPUCardTheme, CPUPowerMove, Position, Points, Status, ImageUrl, IsInSelection, IsCPU, IsCPUChallenge, IsSpecialCPU, IsDone, UpdatedDate)
    VALUES(NewID(), @Username, @Opponent, @CardId, @EnhancerId, @PowerMove, @CPUCardId, @CPUEnhancerId, @TourneyMatchId, @CPUCardTheme, @CPUPowerMove, @Position, @Points, @Status, @ImageUrl, @IsInSelection, @IsCPU, @IsCPUChallenge, @IsSpecialCPU, 0, @UpdatedDate)