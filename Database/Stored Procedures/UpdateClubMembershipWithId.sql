CREATE PROCEDURE UpdateClubMembershipWithId
    @Id uniqueidentifier,
    @ClaimedItem bit
AS  
    UPDATE ClubMemberships
    SET ClaimedItem = @ClaimedItem
    WHERE Id = @Id