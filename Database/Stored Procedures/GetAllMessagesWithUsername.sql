CREATE PROCEDURE GetAllMessagesWithUsername
    @Username nvarchar(255),
    @TopCount int = 50
AS  
    SELECT TOP (@TopCount) * 
    FROM Messages m
    JOIN 
    (
        SELECT u.Username, u.Color, u.DisplayPicUrl, MAX(u.ModifiedDate) AS LastActive, MAX(m.CreatedDate) AS CreatedDate
        FROM Messages m
        JOIN Users u ON
        m.Sender = u.Username 
        OR m.Recipient = u.Username
        WHERE (m.Sender = @Username OR m.Recipient = @Username)
        AND u.Username != @Username
        AND u.InactiveType = 0
        GROUP BY u.Username, u.Color, u.DisplayPicUrl
    ) md
    ON md.CreatedDate = m.CreatedDate
    AND 
    (
        (m.Sender = md.Username AND m.Recipient = @Username) 
        OR (m.Sender = @Username AND m.Recipient = md.Username)
    )
    ORDER BY m.CreatedDate DESC