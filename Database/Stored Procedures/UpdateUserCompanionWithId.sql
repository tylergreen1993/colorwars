CREATE PROCEDURE UpdateUserCompanionWithId
    @Id uniqueidentifier,
    @Username varchar(255),
    @Type int,
    @Name varchar(255),
    @Color int,
    @Level int = 0,
    @Position int = 0,
    @LastInteractedDate datetime = NULL,
    @HappinessDate datetime = NULL,
    @LastGiftGivenDate datetime = NULL,
    @CreatedDate datetime = NULL
AS  
    UPDATE UserCompanions
    SET Username = @Username,
    Type = @Type,
    Name = @Name,
    Color = @Color,
    Level = @Level,
    Position = @Position,
    LastInteractedDate = @LastInteractedDate,
    HappinessDate = @HappinessDate,
    LastGiftGivenDate = @LastGiftGivenDate,
    CreatedDate =  @CreatedDate
    WHERE Id = @Id