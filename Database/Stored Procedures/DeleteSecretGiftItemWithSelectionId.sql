CREATE PROCEDURE DeleteSecretGiftItemWithSelectionId
    @Id uniqueidentifier
AS  
	DELETE FROM SecretGiftItems
    WHERE SelectionId = @Id