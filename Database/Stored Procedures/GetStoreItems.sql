CREATE PROCEDURE GetStoreItems
AS  
    SELECT *
    FROM StoreItems s
    JOIN Items i
    ON s.ItemId = i.Id
    ORDER BY s.Cost, i.Category, i.Name ASC