CREATE PROCEDURE GetUserWithUsername
    @Username nvarchar(255)
AS    
    SELECT u.*, ISNULL(Points, 0) as Points
    FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.Username = @Username
