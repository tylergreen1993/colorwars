CREATE PROCEDURE GetAllEnhancersWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Enhancers e
    ON u.ItemId = e.Id
    WHERE u.Username = @Username
    AND i.Category = 1
    ORDER BY i.Points, i.Name ASC