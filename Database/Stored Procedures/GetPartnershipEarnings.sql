CREATE PROCEDURE GetPartnershipEarnings
    @Username varchar(255),
    @PartnershipUsername varchar(255)
AS  
    SELECT * FROM PartnershipEarnings
    WHERE Username = @Username
    AND PartnershipUsername = @PartnershipUsername
    ORDER BY CreatedDate DESC