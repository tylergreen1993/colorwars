CREATE PROCEDURE AddLuckyGuessWinner
    @Username varchar(255)
AS  
    UPDATE LuckyGuessTreasure
    SET Username = @Username,
    CreatedDate  = GETDATE()
    WHERE Username IS NULL