CREATE PROCEDURE UpdateAttentionHallPoint
    @PostId int,
    @Username varchar(255),
    @Points int
AS  
    IF @Points = 0
    BEGIN
        DELETE FROM AttentionHallPoints
        WHERE PostId = @PostId
        AND Username = @Username
    END
    ELSE
    BEGIN
        UPDATE AttentionHallPoints
        SET Points = @Points
        WHERE PostId = @PostId
        AND Username = @Username
    END
    