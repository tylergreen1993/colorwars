CREATE PROCEDURE GetPreviousPromoCodes
    @Username varchar(255)
AS  
    SELECT * FROM PromoCodes p
    JOIN UserPromoCodes u 
    ON u.Code = p.Code
    WHERE u.Username = @Username
    ORDER BY RedeemedDate DESC