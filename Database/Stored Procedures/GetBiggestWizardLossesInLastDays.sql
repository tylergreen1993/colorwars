CREATE PROCEDURE GetBiggestWizardLossesInLastDays
    @Days int
AS  
    SELECT TOP 100 CAST(RANK() OVER (ORDER BY Amount DESC) AS INT) as Rank, *
    FROM WizardRecords
    WHERE Type = 1
    AND CreatedDate >= DATEADD(DAY, -@Days, GETDATE())