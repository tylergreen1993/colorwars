CREATE PROCEDURE AddAccomplishment
    @AccomplishmentId int,
    @Username varchar(255)
AS  
    INSERT INTO UserAccomplishments(SelectionId, AccomplishmentId, Username, ReceivedDate)
    VALUES (NEWID(), @AccomplishmentId, @Username, GETDATE())
