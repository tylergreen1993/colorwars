CREATE PROCEDURE GetUsersWithAdminRoles
AS  
    SELECT u.*, ISNULL(Points, 0) as Points
    FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.Role > 0
    AND u.InactiveType = 0
    ORDER BY u.Role DESC, u.CreatedDate ASC

