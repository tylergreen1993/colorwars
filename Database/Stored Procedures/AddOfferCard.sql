CREATE PROCEDURE AddOfferCard
    @Username nvarchar(255),
    @Position nvarchar(255),
    @Points varchar(MAX)
AS  
    DELETE FROM OfferCards
    WHERE Username = @Username
    AND FlipDate < DATEADD(HOUR, -12, GETDATE())
    INSERT INTO OfferCards(Id, Username, Position, Points, FlipDate)
    VALUES (NEWID(), @Username, @Position, @Points, GETDATE())