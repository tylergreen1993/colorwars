CREATE PROCEDURE GetMarketStallItemsWithMarketStallId
    @Id int,
    @Discount int
AS  
    SELECT *,
    ISNULL(m.Name, i.Name) as Name,
    ISNULL(m.ImageUrl, i.ImageUrl) as ImageUrl,
    CAST(ROUND(m.Cost * (100 - @Discount)/100.0, 0) AS INT) as Cost 
    FROM
    MarketStallItems m
    JOIN Items i
    ON i.Id = m.ItemId
    WHERE m.MarketStallId = @Id
    ORDER BY m.Cost, i.Category, i.Name