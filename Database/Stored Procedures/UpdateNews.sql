CREATE PROCEDURE UpdateNews
    @Id int,
    @Username varchar(255),
    @Title varchar(255),
    @Content varchar(MAX)
AS  
    UPDATE News
    SET Username = @Username,
    Title = @Title,
    Content = @Content
    WHERE Id = @Id