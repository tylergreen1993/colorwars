CREATE PROCEDURE AddSuspensionHistory
    @Username nvarchar(255),
    @SuspendingUser nvarchar(255),
    @Reason varchar(MAX),
    @SuspendedDate datetime
AS  
    INSERT INTO SuspensionHistory(Id, Username, SuspendingUser, Reason, SuspendedDate, CreatedDate)
    VALUES (NEWID(), @Username, @SuspendingUser, @Reason, @SuspendedDate, GETDATE())