CREATE PROCEDURE AddTradeItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO TradeItems(SelectionId, ItemId, Name, ImageUrl, Uses, Theme, CreatedDate, RepairedDate)
    VALUES (@SelectionId, @ItemId, @Name, @ImageUrl, @Uses, @Theme, @CreatedDate, @RepairedDate)