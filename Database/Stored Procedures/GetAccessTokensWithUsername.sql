CREATE PROCEDURE GetAccessTokensWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM AccessTokens
    WHERE Username = @Username
    ORDER BY CreatedDate DESC