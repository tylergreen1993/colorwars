CREATE PROCEDURE DeleteFromMatchStateWithUsername
    @Username nvarchar(255),
    @Opponent nvarchar(255),
    @IsCPU bit,
    @IsExpired bit
AS
    IF @IsCPU = 1
    BEGIN
        DELETE FROM MatchState
        WHERE Username = @Username
    END
    ELSE IF @IsExpired = 1
    BEGIN
        DELETE FROM MatchState
        WHERE Username = @Username
        OR Username = @Opponent
    END
    ELSE
    BEGIN
        DECLARE @IsDone bit
        SET @IsDone = 
        (Select TOP 1 IsDone FROM MatchState
        WHERE USERNAME = @Opponent)
        IF @IsDone = 1
        BEGIN
            DELETE FROM MatchState
            WHERE Username = @Username
            OR Username = @Opponent
        END
        ELSE
        BEGIN
            UPDATE MatchState
            SET IsDone = 1
            WHERE Username = @Username
        END
    END