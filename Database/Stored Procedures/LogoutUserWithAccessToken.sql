CREATE PROCEDURE LogoutUserWithAccessToken
    @Username nvarchar(255),
    @AccessToken uniqueidentifier
AS  
    DELETE FROM AccessTokens
    WHERE Username = @Username
    AND AccessToken = @AccessToken