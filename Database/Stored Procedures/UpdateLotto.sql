CREATE PROCEDURE UpdateLotto
    @Id int,
    @CurrentPot int,
    @WinnerUsername varchar(255) = NULL,
    @WonDate datetime
AS  
    UPDATE Lotto
    SET CurrentPot = @CurrentPot,
    WinnerUsername = @WinnerUsername,
    WonDate = @WonDate
    WHERE Id = @Id