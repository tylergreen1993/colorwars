CREATE PROCEDURE GetAllEggCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN EggCards e
    ON u.ItemId = e.Id
    WHERE u.Username = @Username
    AND i.Category = 6
    ORDER BY i.Points, i.Name ASC