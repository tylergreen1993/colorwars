CREATE PROCEDURE GetJunkyardItems
AS  
    SELECT TOP 100 *
    FROM JunkyardItems j
    JOIN Items i
    ON j.ItemId = i.Id
    ORDER BY j.AddedDate DESC