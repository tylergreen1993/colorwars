CREATE PROCEDURE GetStockHistory
    @Id int
AS  
    SELECT * FROM StockHistory
    WHERE Id = @Id
    ORDER BY CreatedDate DESC