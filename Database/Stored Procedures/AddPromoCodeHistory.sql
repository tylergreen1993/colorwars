CREATE PROCEDURE AddPromoCodeHistory
    @Username varchar(255),
    @Code varchar(255),
    @Message varchar(MAX)
AS  
    INSERT INTO PromoCodeHistory(Id, Username, Code, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Code, @Message, GETDATE())