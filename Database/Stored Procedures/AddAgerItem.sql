CREATE PROCEDURE AddAgerItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Username varchar(255),
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO AgerItems(SelectionId, ItemId, Username, Name, ImageUrl, Uses, Theme, CreatedDate, RepairedDate, AddedDate)
    VALUES (@SelectionId, @ItemId, @Username, @Name, @ImageUrl, @Uses, @Theme, @CreatedDate, @RepairedDate, GETDATE())