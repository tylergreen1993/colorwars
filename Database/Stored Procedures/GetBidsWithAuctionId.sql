CREATE PROCEDURE GetBidsWithAuctionId
    @AuctionId int
AS  
    SELECT TOP 500 * FROM Bids
    WHERE AuctionId = @AuctionId
    ORDER BY BidDate DESC