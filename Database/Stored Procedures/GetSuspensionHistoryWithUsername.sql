CREATE PROCEDURE GetSuspensionHistoryWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM SuspensionHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC