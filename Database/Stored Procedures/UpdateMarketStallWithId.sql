CREATE PROCEDURE UpdateMarketStallWithId
    @Id int,
    @Name varchar(255),
    @Description varchar(MAX) = NULL,
    @Color varchar(255) = NULL,
    @Discount int
AS  
    UPDATE MarketStalls
    SET Name = @Name,
    Description = @Description,
    Color = @Color,
    Discount = @Discount
    WHERE Id = @Id