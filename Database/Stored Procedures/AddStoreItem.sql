CREATE PROCEDURE AddStoreItem
    @ItemId uniqueidentifier,
    @Cost int,
    @MinCost int
AS  
    IF ((SELECT COUNT(*) FROM StoreItems) <= 25)
    BEGIN
        INSERT INTO StoreItems(Id, ItemId, Cost, MinCost, CreatedDate)
        VALUES(NEWID(), @ItemId, @Cost, @MinCost, GETDATE())
    END