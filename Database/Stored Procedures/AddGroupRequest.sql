CREATE PROCEDURE AddGroupRequest
    @Username varchar(255),
    @GroupId int
AS  
    INSERT INTO GroupRequests(Username, GroupId, Accepted, CreatedDate)
    VALUES (@Username, @GroupId, 0, GETDATE())