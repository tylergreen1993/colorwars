CREATE PROCEDURE AddSettingForUsername
    @Username nvarchar(255),
    @Name nvarchar(255),
    @Value nvarchar(max)
AS  
    BEGIN TRAN
        IF EXISTS (SELECT * FROM Settings 
            WHERE Username = @Username
            AND Name = @Name)
            BEGIN
                UPDATE Settings
                SET Value = @Value,
                UpdatedDate = GETDATE()
                WHERE Username = @Username
                AND Name = @Name
            END
        ELSE
            BEGIN
                INSERT INTO Settings(Id, Username, Name, Value, CreatedDate, UpdatedDate)
                VALUES(NEWID(), @Username, @Name, @Value, GETDATE(), GETDATE())
            END
    COMMIT TRAN