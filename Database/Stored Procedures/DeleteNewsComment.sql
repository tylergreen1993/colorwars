CREATE PROCEDURE DeleteNewsComment
    @NewsId int,
    @Id uniqueidentifier
AS  
    DECLARE @CommentIds TABLE(
        Id uniqueidentifier
    );
    WITH CommentIds AS (
        SELECT * 
        FROM NewsComments
        WHERE Id = @Id
        AND NewsId = @NewsId
        UNION ALL
        SELECT n.*
        FROM NewsComments n
        JOIN CommentIds c 
        ON n.ParentCommentId = c.Id
    )
    INSERT INTO @CommentIds(id) 
    SELECT Id FROM CommentIds
    DELETE
    FROM NewsCommentLikes
    WHERE CommentId IN (SELECT Id FROM @CommentIds)
    AND NewsId = @NewsId
    DELETE
    FROM NewsComments
    WHERE Id IN (SELECT Id FROM @CommentIds)
    AND NewsId = @NewsId