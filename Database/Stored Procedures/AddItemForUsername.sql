CREATE PROCEDURE AddItemForUsername
    @Username nvarchar(255),
    @ItemId uniqueidentifier,
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @TotalUses int,
    @DeckType int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
INSERT into UserItems (SelectionId, Username, ItemId, Name, ImageUrl, Uses, DeckType, Theme, CreatedDate, RepairedDate, AddedDate) 
VALUES (NEWID(), @Username, @ItemId, @Name, @ImageUrl, @TotalUses, @DeckType, @Theme, @CreatedDate, @RepairedDate, GETDATE())