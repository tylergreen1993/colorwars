CREATE PROCEDURE GetBulkItemsWithUsername
    @Username varchar(255)
AS  
    SELECT *
    FROM BulkItems b
    JOIN Items i
    ON b.ItemId = i.Id
    WHERE b.Username = @Username
    ORDER BY b.Cost, i.Category, i.Name ASC