CREATE PROCEDURE GetShowroomLikes
    @ShowroomUsername varchar(255)
AS  
    SELECT * FROM ShowroomLikes
    WHERE ShowroomUsername = @ShowroomUsername
    ORDER BY CreatedDate DESC