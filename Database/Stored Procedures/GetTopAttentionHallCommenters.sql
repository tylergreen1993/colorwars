CREATE PROCEDURE GetTopAttentionHallCommenters
AS  
    SELECT TOP 100 u.Username, u.Color, u.DisplayPicUrl, COUNT(*) as Amount FROM AttentionHallComments a
    JOIN Users u ON a.Username = u.Username
    WHERE u.InactiveType = 0
    AND u.ModifiedDate > DateADD(DD, -30, GETDATE())
    GROUP BY u.Username, u.Color, u.DisplayPicUrl
    ORDER BY Amount DESC