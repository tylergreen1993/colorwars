CREATE PROCEDURE AddPartnershipEarning
    @Username varchar(255),
    @PartnershipUsername varchar(255),
    @Amount int
AS  
    INSERT INTO PartnershipEarnings(Id, Username, PartnershipUsername, Amount, CreatedDate)
    VALUES(NEWID(), @Username, @PartnershipUsername, @Amount, GETDATE())