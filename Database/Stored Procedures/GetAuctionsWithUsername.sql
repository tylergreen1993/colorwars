CREATE PROCEDURE GetAuctionsWithUsername
    @Username varchar(255)
AS  
    SELECT DISTINCT 
    a.*,
    (SELECT COUNT(*) FROM Bids WHERE AuctionId = a.Id) as BidCount,
    (SELECT TOP 1 Username FROM Bids WHERE AuctionId = a.Id ORDER BY Points DESC) as TopBidder,
    (SELECT TOP 1 Points FROM Bids WHERE AuctionId = a.Id ORDER BY Points DESC) as HighestBid
    FROM
    Auctions a
    LEFT JOIN Bids b on a.Id = b.AuctionId
    WHERE a.Username = @Username
    OR b.Username = @Username
    ORDER BY a.EndDate DESC