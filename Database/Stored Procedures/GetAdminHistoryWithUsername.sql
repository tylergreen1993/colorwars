CREATE PROCEDURE GetAdminHistoryWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM AdminHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC
