CREATE PROCEDURE GetMarketStallItemsWithSearchQuery
    @Query varchar(255),
    @Theme int
AS  
    SELECT TOP 150 mi.*, 
    CAST(ROUND(mi.Cost * (100 - m.Discount)/100.0, 0) AS INT) as Cost, 
    i.*, 
    ISNULL(mi.Name, i.Name) as Name,
    ISNULL(mi.ImageUrl, i.ImageUrl) as ImageUrl,
    m.Username 
    FROM MarketStallItems mi
    JOIN MarketStalls m
    ON m.Id = mi.MarketStallId
    JOIN Items i
    ON i.Id = mi.ItemId
    WHERE ISNULL(mi.Name, i.Name) like '%' + @Query + '%'
    AND (@Theme = 0 OR mi.Theme = @Theme)
    ORDER BY mi.Cost, i.Category, i.Name