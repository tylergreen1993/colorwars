CREATE PROCEDURE GetSettingsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM Settings s
    WHERE s.Username = @Username