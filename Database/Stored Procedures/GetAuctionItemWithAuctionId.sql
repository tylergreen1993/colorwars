CREATE PROCEDURE GetAuctionItemWithAuctionId
    @AuctionId int
AS  
    SELECT *, ISNULL(ai.Name, i.Name) as Name, ISNULL(ai.ImageUrl, i.ImageUrl) as ImageUrl
    FROM Auctions a
    JOIN AuctionItems ai
    ON a.ItemId = ai.Selectionid
    JOIN Items i
    ON ai.ItemId = i.Id
    WHERE a.Id = @AuctionId