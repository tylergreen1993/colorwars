CREATE PROCEDURE UpdateReport
    @Id uniqueidentifier,
    @Username nvarchar(255) = NULL,
    @Outcome nvarchar(MAX) = NULL,
    @IsCompleted bit
AS  
    UPDATE Reports
    SET ClaimedUser = @Username,
    Outcome = @Outcome,
    IsCompleted = @IsCompleted
    WHERE Id = @Id