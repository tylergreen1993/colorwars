CREATE PROCEDURE GetAllCards
AS  
    SELECT *
    FROM Items i
    JOIN Cards c
    ON i.Id = c.Id
    WHERE i.Category = 0
    ORDER BY i.Points ASC