CREATE PROCEDURE CreateOrUpdatePasswordReset
    @Username varchar(255)
AS  
    IF EXISTS (SELECT 1 FROM PasswordResets WHERE Username = @Username)
    BEGIN
        IF EXISTS (SELECT 1 FROM PasswordResets WHERE Username = @Username AND CreatedDate < DATEADD(minute, -30, GETDATE()))
        BEGIN
            UPDATE PasswordResets
            SET Id = NEWID(), CreatedDate = GETDATE()
            WHERE Username = @Username
        END
    END
    ELSE
    BEGIN
        INSERT INTO PasswordResets(Id, Username, CreatedDate)
        VALUES(NEWID(), @Username, GETDATE())
    END
    SELECT * FROM PasswordResets
    WHERE Username = @Username