CREATE PROCEDURE GetPartnershipRequests
    @Username varchar(255)
AS  
    SELECT * FROM PartnershipRequests
    WHERE Username = @Username
    ORDER BY CreatedDate DESC