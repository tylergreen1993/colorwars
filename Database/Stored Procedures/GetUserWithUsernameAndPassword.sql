CREATE PROCEDURE GetUserWithUsernameAndPassword
    @Username nvarchar(255),   
    @Password nvarchar(255),
    @IPAddress nvarchar(255),
    @UserAgent nvarchar(MAX),
    @PerformUpdate bit = 1
AS  
    IF @PerformUpdate = 1
    BEGIN
        DECLARE @AccessToken uniqueidentifier = NEWID()
        UPDATE Users
        SET ActiveAccessToken = @AccessToken, ModifiedDate = GETDATE(), LoginAttempts = 0, IPAddress = @IPAddress
        WHERE Username = @Username AND Password = @Password
        IF EXISTS(SELECT 1 FROM Users WHERE Username = @Username AND Password = @Password)
        BEGIN
            INSERT INTO AccessTokens(Username, AccessToken, IPAddress, UserAgent, CreatedDate)
            VALUES(@Username, @AccessToken, @IPAddress, @UserAgent, GETDATE())
        END
    END
    SELECT u.*, a.AccessToken, ISNULL(Points, 0) as Points
    FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    LEFT JOIN AccessTokens a
    ON u.Username  = a.username
    WHERE u.Username = @Username
    AND u.Password = @Password
    AND a.AccessToken = u.ActiveAccessToken