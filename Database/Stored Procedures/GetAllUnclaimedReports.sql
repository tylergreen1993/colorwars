CREATE PROCEDURE GetAllUnclaimedReports
AS  
    SELECT * FROM Reports
    WHERE ClaimedUser IS NULL
    ORDER BY CreatedDate DESC