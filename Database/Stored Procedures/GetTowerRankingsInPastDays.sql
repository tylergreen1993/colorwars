CREATE PROCEDURE GetTowerRankingsInPastDays
    @Days int
AS  
    SELECT CAST(RANK() OVER (ORDER BY TopFloorLevel DESC) AS INT) as Rank, PERCENT_RANK() OVER (ORDER BY TopFloorLevel DESC) as Percentile, * 
    FROM TowerRankings
    WHERE CreatedDate >= DATEADD(DAY, -@Days, GETDATE())

