CREATE PROCEDURE GetClaimedReportsWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM Reports
    WHERE ClaimedUser = @Username
    AND IsCompleted = 0
    ORDER BY CreatedDate DESC