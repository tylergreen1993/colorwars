CREATE PROCEDURE UpdateGroupMessage
    @GroupId int,
    @MessageId uniqueidentifier,
    @Content nvarchar(max),
    @IsDeleted bit
AS  
    UPDATE GroupMessages
    SET Content = @Content,
    IsDeleted = @IsDeleted
    WHERE GroupId = @GroupId
    AND Id = @MessageId