CREATE PROCEDURE RepairAllSafetyDepositItems
    @Username varchar(255)
AS  
    UPDATE SafetyDepositItems
    SET RepairedDate = GETDATE()
    WHERE Username = @Username