CREATE PROCEDURE GetAllColorWarsSubmissionsWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM ColorWarsSubmissions
    WHERE Username = @Username
    ORDER BY CreatedDate DESC