CREATE PROCEDURE GetAllSeedCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN SeedCards s
    ON u.ItemId = s.Id
    WHERE u.Username = @Username
    AND i.Category = 15
    ORDER BY i.Points, i.Name ASC