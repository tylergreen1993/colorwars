CREATE PROCEDURE DeleteFromMatchWaitlistWithUsername
    @Username nvarchar(255)
AS  
    DELETE FROM MatchWaitlist
    WHERE Username = @Username
    OR Username2 = @Username