CREATE PROCEDURE GetUserWithAccessToken
    @Username nvarchar(255),   
    @AccessToken uniqueidentifier 
AS  
    IF EXISTS(SELECT 1 FROM AccessTokens WHERE Username = @Username AND AccessToken = @AccessToken)
    BEGIN
        UPDATE Users 
        SET ModifiedDate = GETDATE(),
        FlushSession =  CASE
                            WHEN EXISTS(SELECT 1 FROM Users WHERE ActiveAccessToken = @AccessToken)
                                THEN FlushSession
                            ELSE 1
                        END,
        ActiveAccessToken = @AccessToken
        WHERE Username = @Username 
    END
    SELECT u.*, a.AccessToken, ISNULL(Points, 0) as Points
    FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    LEFT JOIN AccessTokens a
    ON u.Username  = a.username
    WHERE u.Username = @Username 
    AND u.ActiveAccessToken = @AccessToken 
    AND a.AccessToken = @AccessToken