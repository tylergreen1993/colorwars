CREATE PROCEDURE GetUnreadMessagesWithUsername
    @Username nvarchar(255)
AS  
    SELECT * 
    FROM Messages m
    JOIN Users u on u.Username = m.Sender
    WHERE Recipient = @Username
    AND u.InactiveType = 0
    And IsRead = 0