CREATE PROCEDURE GetAllCompanionsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Companions c
    ON u.ItemId = c.Id
    WHERE u.Username = @Username
    AND i.Category = 14
    ORDER BY i.Points, i.Name ASC