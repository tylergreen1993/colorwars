CREATE PROCEDURE UpdateMatchStateForId
    @Id uniqueidentifier,
    @CardId uniqueidentifier,
    @EnhancerId uniqueidentifier,
    @PowerMove int,
    @CPUCardId uniqueidentifier,
    @CPUEnhancerId uniqueidentifier,
    @CPUPowerMove int,
    @Position int,
    @Points int,
    @Status int,
    @IsInSelection bit,
    @UpdatedDate datetime
AS  
    UPDATE MatchState 
    SET CardId = @CardId, 
    EnhancerId = @EnhancerId,
    PowerMove = @PowerMove,
    CPUEnhancerId = @CPUEnhancerId,
    CPUCardId = @CPUCardId,
    CPUPowerMove = @CPUPowerMove,
    Position = @Position, 
    Points = @Points, 
    Status = @Status, 
    IsInSelection = @IsInSelection,
    UpdatedDate = @UpdatedDate
    WHERE Id = @Id