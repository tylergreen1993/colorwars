CREATE PROCEDURE CompleteTrade
    @TradeId int,
    @TradeOfferId uniqueidentifier
AS  
    UPDATE Trades
    SET IsAccepted = 1,
    AcceptedDate = GETDATE()
    WHERE Id = @TradeId
    
    UPDATE TradeOffers
    SET IsAccepted = 1
    WHERE Id = @TradeOfferId