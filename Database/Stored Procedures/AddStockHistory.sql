CREATE PROCEDURE AddStockHistory
    @Id int,
    @Username varchar(255),
    @Cost int,
    @Amount int,
    @Type int
AS  
    INSERT INTO StockHistory(SelectionId, Id, Username, UserCost, UserAmount, HistoryType, CreatedDate)
    VALUES(NEWID(), @Id, @Username, @Cost, @Amount, @Type, GETDATE())