CREATE PROCEDURE RemoveFavoritedLocation
    @Username varchar(255),
    @LocationId int
AS  
    DELETE FROM FavoritedLocations
    WHERE Username = @Username
    AND LocationId = @LocationId