CREATE PROCEDURE GetAllThemesWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Themes t
    ON u.ItemId = t.Id
    WHERE u.Username = @Username
    AND i.Category = 8
    ORDER BY i.Points, i.Name ASC