CREATE PROCEDURE GetAllItemsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *, ISNULL(u.Name, i.Name) as Name, ISNULL(u.ImageUrl, i.ImageUrl) as ImageUrl
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    WHERE u.Username = @Username
    ORDER BY i.Category, i.Points, i.Name, u.CreatedDate ASC