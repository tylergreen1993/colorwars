CREATE PROCEDURE GetSwapDeckHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM SwapDeckHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC