CREATE PROCEDURE AddReport
    @Username varchar(255),
    @ReportedUser varchar(255),
    @IPAddress varchar(255),
    @AttentionHallPostId int,
    @Reason varchar(max)
AS  
    INSERT INTO Reports(Id, Username, ReportedUser, IPAddress, ClaimedUser, AttentionHallPostId, Reason, IsCompleted, CreatedDate)
    VALUES (NEWID(), @Username, @ReportedUser, @IPAddress, NULL, @AttentionHallPostId, @Reason, 0, GETDATE())