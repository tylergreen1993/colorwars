CREATE PROCEDURE GetPromoCodeHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM PromoCodeHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC