CREATE PROCEDURE GetAccomplishmentsWithUsername
    @Username varchar(255)
AS  
	DECLARE @UserCount float = (SELECT COUNT(*) FROM Users WHERE InactiveType = 0)
    SELECT *, (CAST((SELECT COUNT(*) FROM UserAccomplishments WHERE Id = AccomplishmentId) AS float)/@UserCount * 100) as PercentOwned
    FROM UserAccomplishments u
    JOIN Accomplishments a 
    ON u.AccomplishmentId = a.Id
    WHERE u.Username = @Username
    ORDER BY a.Category, a.Name