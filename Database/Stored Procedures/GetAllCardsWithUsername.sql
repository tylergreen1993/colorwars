CREATE PROCEDURE GetAllCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Cards c
    ON u.ItemId = c.Id
    WHERE u.Username = @Username
    AND i.Category = 0
    ORDER BY i.Points, i.Name ASC