CREATE PROCEDURE AddAdminHistory
    @Username nvarchar(255),
    @AdminUser nvarchar(255),
    @Reason varchar(MAX)
AS  
    INSERT INTO AdminHistory(Id, Username, AdminUser, Reason, CreatedDate)
    VALUES (NEWID(), @Username, @AdminUser, @Reason, GETDATE())