CREATE PROCEDURE AddTreasureDive
    @Username varchar(255),
    @TreasureCoordinates varchar(MAX)
AS  
    INSERT INTO TreasureDives(Username, Attempts, TreasureCoordinates, LastGuessCoordinates)
    VALUES (@Username, 0, @TreasureCoordinates, '')