CREATE PROCEDURE UpdateSafetyDepositItemWithSelectionId
    @SelectionId uniqueidentifier,
    @Position int
AS  
    UPDATE SafetyDepositItems
    SET Position = @Position
    WHERE SelectionId = @SelectionId