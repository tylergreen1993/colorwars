CREATE PROCEDURE AddTrade
    @Username varchar(255),
    @Item1Id uniqueidentifier = NULL,
    @Item2Id uniqueidentifier = NULL,
    @Item3Id uniqueidentifier = NULL,
    @Request varchar(MAX) = NULL
AS  
    DECLARE @OutputId TABLE (Id int)

    INSERT INTO Trades(Username, Item1Id, Item2Id, Item3Id, Request, IsAccepted, CreatedDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES (@Username, @Item1Id, @Item2Id, @Item3Id, @Request, 0, GETDATE())

    SELECT * FROM Trades
    WHERE Id in (SELECT Id FROM @OutputId)