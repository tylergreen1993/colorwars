CREATE PROCEDURE GetBankTransactionsWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM BankTransactions
    WHERE Username = @Username
    ORDER BY TransactionDate DESC