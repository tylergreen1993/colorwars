CREATE PROCEDURE AddMysteryShowcase
    @ItemId uniqueidentifier
AS  
    IF ((SELECT COUNT(*) FROM MysteryShowcase) = 0)
    BEGIN
        INSERT INTO MysteryShowcase(Id, CreatedDate)
        VALUES(@ItemId, GETDATE())
    END