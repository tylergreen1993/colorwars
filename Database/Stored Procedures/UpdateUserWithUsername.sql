CREATE PROCEDURE UpdateUserWithUsername
    @Username nvarchar(255), 
    @EmailAddress nvarchar(255),
    @Gender int,
    @FirstName nvarchar(255),
    @Location nvarchar(255),
    @Bio nvarchar(max),
    @Color nvarchar(255),
    @DisplayPicUrl nvarchar(max) = '',
    @AllowNotifications bit,
    @FlushSession bit,
    @LoginAttempts int,
    @LoginAttemptDate datetime,
    @Role int,
    @InactiveType int,
    @ForgotEmailDate datetime,
    @SuspendedDate datetime,
    @PremiumExpiryDate datetime,
    @AdFreeExpiryDate datetime,
    @Password nvarchar(255)
AS  
    UPDATE Users
    SET EmailAddress = @EmailAddress, 
    Gender = @Gender,
    FirstName = @FirstName,
    Location = @Location,
    Bio = @Bio,
    Color = @Color,
    DisplayPicUrl = @DisplayPicUrl,
    AllowNotifications = @AllowNotifications,
    FlushSession = @FlushSession,
    LoginAttempts = @LoginAttempts,
    LoginAttemptDate = @LoginAttemptDate,
    Role = @Role,
    ForgotEmailDate = @ForgotEmailDate,
    SuspendedDate = @SuspendedDate,
    PremiumExpiryDate = @PremiumExpiryDate,
    AdFreeExpiryDate = @AdFreeExpiryDate,
    InactiveType = @InactiveType
    WHERE Username = @Username
    IF NULLIF(@Password, '') != ''
    BEGIN
        UPDATE Users
        SET Password = @Password
        WHERE Username = @Username
        UPDATE PasswordResets
        SET Id = NEWID()
        WHERE Username = @Username
        DELETE FROM AccessTokens
        WHERE Username = @Username
        AND AccessToken NOT IN (SELECT ActiveAccessToken FROM Users WHERE Username = @Username)
    END