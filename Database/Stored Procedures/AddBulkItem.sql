CREATE PROCEDURE AddBulkItem
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @Cost int
AS  
    INSERT INTO BulkItems(Id, Username, ItemId, Cost, CreatedDate)
    VALUES(NEWID(), @Username, @ItemId, @Cost, GETDATE())