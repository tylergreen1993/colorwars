CREATE PROCEDURE AddOrUpdateMatchWaitlist
    @Username nvarchar(255),
    @IsElite bit,
    @Points int,
    @ChallengeUser nvarchar(255) = NULL,
    @TourneyMatchId uniqueidentifier = NULL
AS  
    DECLARE @MatchUser nvarchar(255)

    DELETE FROM MatchWaitList
	WHERE CreatedDate < DATEADD(SECOND, -90, GETDATE())
    
    IF @ChallengeUser IS NULL
    BEGIN
    	SET @MatchUser = 
	    (SELECT TOP 1 m.Username FROM MatchWaitlist m
	    WHERE m.Username2 IS NULL
	    AND m.IsElite = @IsElite
	    AND m.ChallengeUser IS NULL
        AND m.CreatedDate >= DATEADD(SECOND, -90, GETDATE())
	    ORDER BY m.CreatedDate ASC)
    END
    ELSE
    BEGIN
    	SET @MatchUser = 
    	(SELECT Top 1 m.Username FROM MatchWaitlist m
    	WHERE m.Username = @ChallengeUser
    	AND ChallengeUser = @Username
    	AND TourneyMatchId = @TourneyMatchId)
    END
    
    IF @MatchUser IS NULL
    BEGIN
        INSERT INTO MatchWaitlist(Id, Username, Username2, ChallengeUser, IsElite, Points, TourneyMatchId, CreatedDate)
        VALUES (NEWID(), @Username, NULL, @ChallengeUser, @IsElite, @Points, @TourneyMatchId, GETDATE())
        SELECT * FROM MatchWaitlist WHERE Username = @Username 
    END
    ELSE
    BEGIN
        UPDATE MatchWaitlist
        SET Username2 = @Username
        WHERE Username = @MatchUser
        AND TourneyMatchId = @TourneyMatchId
        SELECT * FROM MatchWaitlist WHERE Username2 = @Username AND TourneyMatchId = @TourneyMatchId
    END