CREATE PROCEDURE AddBannedIPAddress
    @IPAddress nvarchar(255)
AS  
	INSERT INTO BannedIPAddresses(IPAddress, CreatedDate)
    VALUES(@IPAddress, GETDATE())