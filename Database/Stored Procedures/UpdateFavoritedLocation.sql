CREATE PROCEDURE UpdateFavoritedLocation
    @Username varchar(255),
    @LocationId int,
    @Position int
AS  
    UPDATE FavoritedLocations
    SET Position = @Position
    WHERE Username = @Username
    AND LocationId = @LocationId