CREATE PROCEDURE DeleteTradeOffer
    @Id uniqueidentifier
AS  
    DECLARE @Item1Id uniqueidentifier = (SELECT Item1Id FROM TradeOffers WHERE Id = @Id)
    DECLARE @Item2Id uniqueidentifier = (SELECT Item2Id FROM TradeOffers WHERE Id = @Id)
    DECLARE @Item3Id uniqueidentifier = (SELECT Item3Id FROM TradeOffers WHERE Id = @Id)

    DELETE FROM TradeOffers
    WHERE Id = @Id
    DELETE FROM TradeItems
    WHERE SelectionId = @Item1Id
    OR SelectionId = @Item2Id
    OR SelectionId = @Item3Id