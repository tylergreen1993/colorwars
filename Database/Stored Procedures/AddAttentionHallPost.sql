CREATE PROCEDURE AddAttentionHallPost
    @Username varchar(255),
    @Title varchar(255),
    @Content varchar(MAX),
    @AccomplishmentId int = NULL,
    @ItemId uniqueidentifier = NULL,
    @ItemName varchar(255) = NULL,
    @ItemImageUrl varchar(255) = NULL,
    @ItemTheme int = NULL,
    @MatchTagUsername varchar(255) = NULL,
    @Flair int = NULL,
    @JobSubmissionId uniqueidentifier
AS  
    DECLARE @OutputId TABLE (Id int)
    DECLARE @CurrentDate datetime = GETDATE()

    INSERT INTO AttentionHallPosts(Username, Title, Content, AccomplishmentId, ItemId, ItemTheme, ItemName, ItemImageUrl, MatchTagUsername, JobSubmissionId, Flair, CreatedDate, ModifiedDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES(@Username, @Title, @Content, @AccomplishmentId, @ItemId, @ItemTheme, @ItemName, @ItemImageUrl, @MatchTagUsername, @JobSubmissionId, @Flair, @CurrentDate, @CurrentDate)

    SELECT * FROM AttentionHallPosts
    WHERE Id in (SELECT Id FROM @OutputId)