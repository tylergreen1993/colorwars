CREATE PROCEDURE GetLastTransactionsForStockId
    @Id int
AS  
    SELECT TOP 100 * FROM UserStocks u
    JOIN Stocks s
    ON u.ItemId = s.Id
    WHERE ItemId = @Id
    ORDER BY u.PurchaseDate DESC