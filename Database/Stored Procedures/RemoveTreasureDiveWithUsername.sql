CREATE PROCEDURE RemoveTreasureDiveWithUsername
    @Username varchar(255)
AS  
    DELETE FROM TreasureDives
    WHERE Username = @Username