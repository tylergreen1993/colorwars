CREATE PROCEDURE AddGroup
    @Username varchar(255),
    @Name varchar(255),
    @DisplayPicUrl varchar(MAX) = NULL,
    @Power int,
    @Description varchar(MAX),
    @IsPrivate bit
AS  
    DECLARE @OutputId TABLE (Id int)

    INSERT INTO Groups(Name, DisplayPicUrl, Power, Description, IsPrivate, Whiteboard, CreatedBy, LastPrizeDate, LastPowerChangeDate, CreatedDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES(@Name, @DisplayPicUrl, @Power, @Description, @IsPrivate, '', @Username, GETDATE(), GETDATE(), GETDATE())

    INSERT INTO GroupMembers(GroupId, Username, Role, IsPrizeMember, JoinDate, LastReadMessageDate)
    VALUES((SELECT Id from @OutputId), @Username, 2, 1, GETDATE(), GETDATE())

    SELECT * FROM Groups
    WHERE Id in (SELECT Id FROM @OutputId)