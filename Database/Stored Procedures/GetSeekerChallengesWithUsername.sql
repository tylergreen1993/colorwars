CREATE PROCEDURE GetSeekerChallengesWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM SeekerChallenges
    WHERE Username = @Username
