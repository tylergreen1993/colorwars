CREATE PROCEDURE UpdateTrade
    @Id int,
    @Request varchar(MAX)
AS  
    UPDATE Trades
    SET Request = @Request
    WHERE Id = @Id