CREATE PROCEDURE DeleteTourney
    @Id int
AS  
    DELETE FROM TourneyMatches
    WHERE TourneyId = @Id
    DELETE FROM Tourneys
    WHERE Id = @Id