CREATE PROCEDURE AddJobSubmission
	@Id uniqueidentifier,
    @Username varchar(255),
    @Position int,
    @Title varchar(255),
    @Content varchar(MAX),
    @AttentionHallPostId int
AS  
    INSERT INTO JobSubmissions(Id, Username, Position, Title, Content, State, AttentionHallPostId, CreatedDate)
    VALUES(@Id, @Username, @Position, @Title, @Content, 0, @AttentionHallPostId, GETDATE())