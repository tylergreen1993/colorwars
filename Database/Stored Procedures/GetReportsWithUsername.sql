CREATE PROCEDURE GetReportsWithUsername
    @Username varchar(255),
    @IsReported bit
AS  
    IF @IsReported = 1
    BEGIN
        SELECT * FROM Reports r
        WHERE r.ReportedUser = @Username
        ORDER BY CreatedDate DESC
    END
    ELSE
    BEGIN
        SELECT * FROM Reports r
        WHERE r.Username = @Username
        ORDER BY CreatedDate DESC
    END