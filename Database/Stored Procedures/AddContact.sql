CREATE PROCEDURE AddContact
    @Username varchar(255),
    @EmailAddress varchar(255),
    @IPAddress varchar(255),
    @Message varchar(max),
    @Category int,
    @ReplyViaMessages bit
AS  
    INSERT INTO Contacts(Id, Username, ClaimedUser, EmailAddress, IPAddress, Message, Category, ReplyViaMessages, IsCompleted, CreatedDate)
    VALUES (NEWID(), @Username, NULL, @EmailAddress, @IPAddress, @Message, @Category, @ReplyViaMessages, 0, GETDATE())