CREATE PROCEDURE GetAccomplishmentsRankings
AS  
    SELECT a.*, COUNT(*) AS Count FROM UserAccomplishments ua
    JOIN Accomplishments a
    ON ua.AccomplishmentId = a.Id
    GROUP BY a.Id, a.Name, a.Description, a.Category
    ORDER BY Count DESC