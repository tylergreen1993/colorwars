CREATE PROCEDURE AddSafetyDepositItem
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Theme int,
    @Position int = 0,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO SafetyDepositItems(Username, SelectionId, ItemId, Name, ImageUrl, Uses, Theme, Position, CreatedDate, RepairedDate, AddedDate)
    VALUES(@Username, NEWID(), @ItemId, @Name, @ImageUrl, @Uses, @Theme, @Position, @CreatedDate, @RepairedDate, GETDATE())