CREATE PROCEDURE GetMatchWaitlists
    @Username varchar(255),
    @IsElite bit
AS  
    SELECT * FROM MatchWaitlist
    WHERE Username2 IS NULL
    AND (ChallengeUser IS NULL OR ChallengeUser = @Username)
    AND (ChallengeUser = @Username OR IsElite = @IsElite)
    AND CreatedDate >= DATEADD(SECOND, -90, GETDATE())