CREATE PROCEDURE AddTourneyMatch
    @TourneyId int,
    @Round int,
    @Username varchar(255)

AS  
    IF EXISTS(SELECT TOP 1 * FROM TourneyMatches WHERE TourneyId = @TourneyId AND Round = @Round AND Username2 IS NULL)
    BEGIN
        UPDATE TourneyMatches
        SET Username2 = @Username,
        ModifiedDate = GETDATE()
        WHERE TourneyId = @TourneyId
        AND Round = @Round
        AND Username2 IS NULL
    END
    ELSE
    BEGIN
        INSERT INTO TourneyMatches(Id, TourneyId, Round, Username1, Username2, Winner, CreatedDate, ModifiedDate)
        VALUES(NEWID(), @TourneyId, @Round, @Username, NULL, NULL, GETDATE(), GETDATE())
    END