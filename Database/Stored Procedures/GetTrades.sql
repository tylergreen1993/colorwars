CREATE PROCEDURE GetTrades
AS  
    SELECT TOP 150 * 
    FROM Trades t
    WHERE t.IsAccepted = 0
    ORDER BY CreatedDate DESC
    