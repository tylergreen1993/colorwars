CREATE PROCEDURE AddColorWarsSubmission
    @Username varchar(255),
    @Amount int,
    @TeamColor int
AS  
    INSERT INTO ColorWarsSubmissions(Id, Username, Amount, TeamColor, CreatedDate)
    VALUES(NEWID(), @Username, @Amount, @TeamColor, GETDATE())