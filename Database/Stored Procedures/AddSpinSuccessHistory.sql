CREATE PROCEDURE AddSpinSuccessHistory
    @Username varchar(255),
    @Type int,
    @Message varchar(MAX)
AS  
    INSERT INTO SpinSuccessHistory(Id, Username, Type, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Type, @Message, GETDATE())