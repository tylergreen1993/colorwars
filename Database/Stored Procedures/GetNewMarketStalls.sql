CREATE PROCEDURE GetNewMarketStalls
AS  
    SELECT DISTINCT TOP 50 m.* FROM
    MarketStalls m
    JOIN MarketStallItems mi
    ON m.Id = mi.MarketStallId
    ORDER BY m.CreatedDate DESC