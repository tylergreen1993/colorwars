CREATE PROCEDURE GetPlantedItemWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM PlantedItems p
    JOIN Items i
    ON p.ItemId = i.Id
    WHERE p.Username = @Username