CREATE PROCEDURE DeleteAdoptionCompanion
    @Id uniqueidentifier
AS  
    DELETE FROM AdoptionCompanions
    WHERE Id = @Id