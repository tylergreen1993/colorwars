CREATE PROCEDURE AddFavoritedLocation
    @Username varchar(255),
    @LocationId int,
    @Position int = 0
AS  
    INSERT INTO FavoritedLocations(Username, LocationId, Position, CreatedDate)
    VALUES (@Username, @LocationId, @Position, GETDATE())