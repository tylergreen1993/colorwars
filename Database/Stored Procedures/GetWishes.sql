CREATE PROCEDURE GetWishes
AS  
    SELECT TOP 125 w.*, u.Color, u.DisplayPicUrl
    FROM Wishes w
    JOIN Users u ON
    w.Username = u.Username
    ORDER BY CreatedDate DESC