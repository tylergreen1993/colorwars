CREATE PROCEDURE DeleteFromBlocked
    @Username varchar(255),
    @BlockedUser varchar(255)
AS  
    DELETE FROM Blocked
    WHERE Username = @Username
    AND BlockedUser = @BlockedUser