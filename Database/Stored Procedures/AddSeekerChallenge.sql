CREATE PROCEDURE AddSeekerChallenge
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @ItemTheme int,
    @RewardId uniqueidentifier,
    @RewardTheme int
AS  
    INSERT INTO SeekerChallenges(Id, Username, ItemId, ItemTheme, RewardId, RewardTheme)
    VALUES (NEWID(), @Username, @ItemId, @ItemTheme, @RewardId, @RewardTheme)
