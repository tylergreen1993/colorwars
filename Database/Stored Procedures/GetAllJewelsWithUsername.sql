CREATE PROCEDURE GetAllJewelsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Jewels j
    ON u.ItemId = j.Id
    WHERE u.Username = @Username
    AND i.Category = 11
    ORDER BY i.Points, i.Name ASC