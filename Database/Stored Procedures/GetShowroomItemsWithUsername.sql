CREATE PROCEDURE GetShowroomItemsWithUsername
    @Username varchar(255)
AS  
    SELECT *, ISNULL(s.Name, i.Name) as Name, ISNULL(s.ImageUrl, i.ImageUrl) as ImageUrl
    FROM ShowroomItems s
    JOIN Items i ON
    s.ItemId = i.Id
    WHERE Username = @Username
    ORDER BY Position ASC