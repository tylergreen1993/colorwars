CREATE PROCEDURE GetBlockedWithUsername
    @Username nvarchar(255)
AS    
    SELECT *
    FROM Blocked b
    WHERE b.Username = @Username
