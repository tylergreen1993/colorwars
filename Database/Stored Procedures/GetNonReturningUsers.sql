CREATE PROCEDURE GetNonReturningUsers
AS  
    SELECT * FROM Users u
    WHERE u.AllowNotifications = 1
    AND u.InactiveType = 0
    AND DATEDIFF(hour, u.ModifiedDate, GETDATE()) >= 72
    AND (SELECT COUNT(*) FROM Settings s WHERE s.Username = u.Username AND Name = 'HasReceivedReturnMessage') = 0