CREATE PROCEDURE AddGroupMember
    @GroupId int,
    @Username varchar(255),
    @Role int,
    @IsPrimary bit
AS  
    INSERT INTO GroupMembers(GroupId, Username, Role, IsPrizeMember, IsPrimary, JoinDate, LastReadMessageDate)
    VALUES (@GroupId, @Username, @Role, 0, @IsPrimary, GETDATE(), GETDATE())