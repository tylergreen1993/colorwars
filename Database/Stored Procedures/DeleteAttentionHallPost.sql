CREATE PROCEDURE DeleteAttentionHallPost
    @PostId int
AS  
    DELETE FROM AttentionHallPoints
    WHERE PostId = @PostId

    DELETE FROM AttentionHallCommentLikes
    WHERE PostId = @PostId

    DELETE FROM AttentionHallComments
    WHERE PostId = @PostId

    DELETE FROM AttentionHallPosts
    WHERE Id = @PostId