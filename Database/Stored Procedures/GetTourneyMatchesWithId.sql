CREATE PROCEDURE GetTourneyMatchesWithId
    @TourneyId int
AS  
    SELECT Id
    INTO #TourneyMatchIds 
    FROM TourneyMatches 
    WHERE TourneyId = @TourneyId

    SELECT Result, Username, TourneyMatchId
    INTO #TourneyMatchHistory
    FROM MatchHistory
    WHERE TourneyMatchId IS NOT NULL
    AND TourneyMatchId IN (SELECT Id FROM #TourneyMatchIds)

    SELECT tm.*, t.MatchesPerRound,
    dbo.InlineMax(
        (SELECT COUNT(*) FROM #TourneyMatchHistory WHERE Result = 1 AND Username = tm.Username1 AND TourneyMatchId = tm.Id), 
        (SELECT COUNT(*) FROM #TourneyMatchHistory WHERE Result = -1 AND Username = tm.Username2 AND TourneyMatchId = tm.Id)
        ) as Wins,
    dbo.InlineMax(
        (SELECT COUNT(*) FROM  #TourneyMatchHistory WHERE Result = -1 AND Username = tm.Username1 AND TourneyMatchId = tm.Id),
        (SELECT COUNT(*) FROM #TourneyMatchHistory WHERE Result = 1 AND Username = tm.Username2 AND TourneyMatchId = tm.Id)
    ) as Losses
    FROM TourneyMatches tm
    JOIN Tourneys t on t.Id = tm.TourneyId
    WHERE TourneyId = @TourneyId
    ORDER BY Round ASC