CREATE PROCEDURE AddUserCustomCard
    @Username varchar(255),
    @Name varchar(255),
    @ImageUrl varchar(255)
AS  
    INSERT INTO UserCustomCards(Id, Username, Name, ImageUrl, CreatedDate)
    VALUES(NEWID(), @Username, @Name, @ImageUrl, GETDATE())