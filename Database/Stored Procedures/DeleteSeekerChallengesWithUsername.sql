CREATE PROCEDURE DeleteSeekerChallengesWithUsername
    @Username varchar(255)
AS  
    DELETE FROM SeekerChallenges
    WHERE Username = @Username
