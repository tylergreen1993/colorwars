CREATE PROCEDURE GetAllClaimedReports
AS  
    SELECT * FROM Reports
    WHERE IsCompleted = 1
    ORDER BY CreatedDate DESC