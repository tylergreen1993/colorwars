CREATE PROCEDURE GetAllLibraryCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN LibraryCards l
    ON u.ItemId = l.Id
    WHERE u.Username = @Username
    AND i.Category = 7
    ORDER BY i.Points, i.Name ASC