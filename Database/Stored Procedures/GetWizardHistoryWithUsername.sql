CREATE PROCEDURE GetWizardHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM WizardHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC