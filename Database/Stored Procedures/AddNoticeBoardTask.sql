CREATE PROCEDURE AddNoticeBoardTask
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @ItemCondition int,
    @IsExactCondition bit,
    @HasTotalUses bit,
    @Reward int,
    @ItemCreatedDate datetime
AS  
    DELETE FROM NoticeBoardTasks
    WHERE Username = @Username
    INSERT INTO NoticeBoardTasks(Username, ItemId, ItemCondition, IsExactCondition, HasTotalUses, Reward, ItemCreatedDate, CreatedDate)
    VALUES(@Username, @ItemId, @ItemCondition, @IsExactCondition, @HasTotalUses, @Reward, @ItemCreatedDate, GETDATE())