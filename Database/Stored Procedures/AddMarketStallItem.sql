CREATE PROCEDURE AddMarketStallItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @MarketStallId int,
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Cost int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO MarketStallItems(SelectionId, ItemId, MarketStallId, Name, ImageUrl, Uses, Cost, Theme, CreatedDate, RepairedDate)
    VALUES (@SelectionId, @ItemId, @MarketStallId, @Name, @ImageUrl, @Uses, @Cost, @Theme, @CreatedDate, @RepairedDate)