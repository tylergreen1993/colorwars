CREATE PROCEDURE DeleteWishesWithUsername
    @Username varchar(255)
AS  
    DELETE FROM Wishes
    WHERE Username = @Username