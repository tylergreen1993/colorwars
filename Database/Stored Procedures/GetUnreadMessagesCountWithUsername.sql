CREATE PROCEDURE GetUnreadMessagesCountWithUsername
    @Username nvarchar(255),
    @Correspondent nvarchar(255) = NULL
AS  
    SELECT COUNT(*)
    FROM Messages m
    JOIN Users u 
    ON u.Username = m.Sender
    WHERE u.InactiveType = 0
    And m.IsRead = 0
    AND Recipient = @Username
    AND (@Correspondent IS NULL OR Sender = @Correspondent)
    GROUP BY m.Sender