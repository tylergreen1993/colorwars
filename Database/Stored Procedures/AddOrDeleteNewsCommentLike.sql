CREATE PROCEDURE AddOrDeleteNewsCommentLike
    @Id int,
    @CommentId uniqueidentifier,
    @Username varchar(255)
AS  
    IF EXISTS(SELECT * FROM NewsCommentLikes WHERE NewsId = @Id AND CommentId = @CommentId AND Username = @Username)
    BEGIN
        DELETE FROM NewsCommentLikes
        WHERE NewsId = @Id
        AND CommentId = @CommentId
        AND Username = @Username
    END
    ELSE
    BEGIN
        INSERT INTO NewsCommentLikes(NewsId, CommentId, Username, CreatedDate)
        VALUES(@Id, @CommentId, @Username, GETDATE())
    END