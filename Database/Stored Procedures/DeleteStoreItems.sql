CREATE PROCEDURE DeleteStoreItems
AS  
	IF EXISTS(SELECT 1 from StoreItems WHERE CreatedDate <= DATEADD(mi, -30, GETDATE()))
	BEGIN
    	DELETE FROM StoreItems
   	END