CREATE PROCEDURE GetAllClubMembershipsWithUsername
    @Username varchar(255)
AS  
    SELECT CAST(ROW_NUMBER() OVER(ORDER BY StartDate) AS INT) AS Month, c.*
    FROM ClubMemberships c
    WHERE c.Username = @Username