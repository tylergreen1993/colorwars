CREATE PROCEDURE DeleteAttentionHallComment
    @PostId int,
    @CommentId uniqueidentifier
AS  
    DECLARE @CommentIds TABLE(
        Id uniqueidentifier
    );
    WITH CommentIds AS (
        SELECT * 
        FROM AttentionHallComments
        WHERE Id = @CommentId
        AND PostId = @PostId
        UNION ALL
        SELECT a.*
        FROM AttentionHallComments a 
        JOIN CommentIds c 
        ON a.ParentCommentId = c.Id
    )
    INSERT INTO @CommentIds(id) 
    SELECT Id FROM CommentIds
    DELETE 
    FROM AttentionHallCommentLikes
    WHERE CommentId IN (SELECT Id FROM @CommentIds)
    AND PostId = @PostId
    DELETE
    FROM AttentionHallComments
    WHERE Id IN (SELECT Id FROM @CommentIds)
    AND PostId = @PostId