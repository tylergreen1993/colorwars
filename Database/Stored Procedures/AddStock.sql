CREATE PROCEDURE AddStock
    @Id int,
    @Username varchar(255),
    @PurchaseCost int,
    @Amount int
AS  
    INSERT INTO UserStocks(SelectionId, ItemId, Username, PurchaseCost, UserAmount, PurchaseDate)
    VALUES(NEWID(), @Id, @Username, @PurchaseCost, @Amount, GETDATE())
    UPDATE Stocks
    SET CurrentCost = CurrentCost + CAST(@Amount AS float)/15
    WHERE Id = @Id