CREATE PROCEDURE GetAllStocks
AS  
    SELECT *, 
    (SELECT SUM(u.UserAmount) FROM UserStocks u WHERE u.ItemId = s.Id) AS TotalAmount, 
    (SELECT TOP 1 Username FROM UserStocks u WHERE u.ItemId = s.Id GROUP BY Username ORDER BY SUM(UserAmount) DESC) as TopOwner
    FROM Stocks s
    ORDER BY Name ASC