CREATE PROCEDURE DeleteAccessTokens
    @Username varchar(255),
    @AccessToken uniqueidentifier
AS  
    DELETE FROM AccessTokens
    WHERE Username = @Username
    AND AccessToken != @AccessToken