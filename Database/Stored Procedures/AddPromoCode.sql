CREATE PROCEDURE AddPromoCode
    @Code varchar(255),
    @Creator varchar(255) = NULL,
    @Effect int,
    @OneUserUse bit,
    @IsSponsor bit = 0,
    @ItemId uniqueidentifier,
    @ExpiryDate datetime
AS  
    INSERT INTO PromoCodes(Code, Creator, Effect, OneUserUse, IsSponsor, ItemId, ExpiryDate)
    VALUES (@Code, @Creator, @Effect, @OneUserUse, @IsSponsor, @ItemId, @ExpiryDate)