CREATE PROCEDURE UpdateTourneyMatch
    @TourneyMatchId uniqueidentifier,
    @Winner varchar(255)
AS  
    UPDATE TourneyMatches
    SET Winner = @Winner
    WHERE Id = @TourneyMatchId