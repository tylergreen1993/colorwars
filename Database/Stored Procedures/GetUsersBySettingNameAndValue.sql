CREATE PROCEDURE GetUsersBySettingNameAndValue
    @Name varchar(255),
    @Value varchar(MAX)
AS  
    SELECT Username FROM Settings
    WHERE Name = @Name
    and Value = @Value