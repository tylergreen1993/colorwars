CREATE PROCEDURE DeleteBulkItemsWithUsername
    @Username varchar(255)
AS  
	BEGIN
    	DELETE FROM BulkItems
        WHERE Username = @Username
   	END