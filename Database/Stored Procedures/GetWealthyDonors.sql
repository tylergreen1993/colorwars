CREATE PROCEDURE GetWealthyDonors
AS  
    SELECT CAST(RANK() OVER (ORDER BY Amount DESC) AS INT) as Rank, *
    FROM WealthyDonors