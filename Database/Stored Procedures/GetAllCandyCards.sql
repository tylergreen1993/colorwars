CREATE PROCEDURE GetAllCandyCards
AS  
    SELECT *
    FROM Items i
    JOIN Candy c
    ON i.Id = c.Id
    WHERE i.Category = 4
    ORDER BY i.Points ASC