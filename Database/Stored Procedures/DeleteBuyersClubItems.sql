CREATE PROCEDURE DeleteBuyersClubItems
AS  
	IF EXISTS(SELECT 1 from BuyersClubItems WHERE CreatedDate <= DATEADD(mi, -30, GETDATE()))
	BEGIN
    	DELETE FROM BuyersClubItems
   	END