CREATE PROCEDURE DeleteMessagesBetweenUsernames
    @Username nvarchar(255),
    @Correspondent nvarchar(255)
AS  
    DELETE FROM Messages
    WHERE (Sender = @Username AND Recipient = @Correspondent)
    OR (Sender = @Correspondent AND Recipient = @Username)