CREATE PROCEDURE GetReturningUsers
    @Gender int
AS  
    SELECT * FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.Gender = @Gender
    AND u.CreatedDate < DATEADD(day, -14, GETDATE())
    AND u.ModifiedDate > DATEADD(day, -7, GETDATE())
    ORDER BY u.CreatedDate DESC