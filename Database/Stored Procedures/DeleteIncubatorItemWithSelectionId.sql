CREATE PROCEDURE DeleteIncubatorItemWithSelectionId
    @Id uniqueidentifier
AS  
	DELETE FROM IncubatorItems
    WHERE SelectionId = @Id