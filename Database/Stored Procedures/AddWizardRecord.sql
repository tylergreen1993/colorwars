CREATE PROCEDURE AddWizardRecord
    @Username varchar(255),
    @Amount int,
    @Type int
AS  
    INSERT INTO WizardRecords(Id, Username, Amount, Type, CreatedDate)
    VALUES (NEWID(), @Username, @Amount, @Type, GETDATE())