CREATE PROCEDURE GetUnreadNotificationsCountWithUsername
    @Username varchar(255)
AS  
    SELECT COUNT(*) as Amount
    FROM Notifications
    WHERE Username = @Username
    AND SeenDate IS NULL