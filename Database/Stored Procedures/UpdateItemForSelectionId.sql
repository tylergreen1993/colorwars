CREATE PROCEDURE UpdateItemForSelectionId
    @SelectionId uniqueidentifier,
    @Username nvarchar(255),
    @Uses int,
    @DeckType int,
    @Theme int,
    @RepairedDate datetime
AS  
UPDATE UserItems 
SET Username = @Username, 
Uses = @Uses, 
DeckType = @DeckType, 
Theme = @Theme,
RepairedDate = @RepairedDate
WHERE SelectionId = @SelectionId