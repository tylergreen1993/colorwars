CREATE PROCEDURE GetStatistics
AS  
    SELECT (SELECT COUNT(*) FROM Users WHERE InactiveType = 0) as UsersCount, 
    (SELECT COUNT(*) FROM UserItems) as ItemsCount,
    (SELECT ((SELECT COUNT(*) FROM MatchHistory WHERE CPUType > 0) + (SELECT COUNT(*)/2 FROM MatchHistory WHERE CPUType = 0)))
    as StadiumMatchesCount,
    (SELECT (SELECT SUM(Points) FROM POINTS) + (SELECT SUM(Points) FROM Bank))
    as PointsAmount