CREATE PROCEDURE AddFurnaceItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Username varchar(255),
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO FurnaceItems(SelectionId, ItemId, Username, Uses, Theme, CreatedDate, RepairedDate)
    VALUES (@SelectionId, @ItemId, @Username, @Uses, @Theme, @CreatedDate, @RepairedDate)