CREATE PROCEDURE GetAuctions
AS  
    SELECT TOP 150 *,
    (SELECT COUNT(*) FROM Bids WHERE AuctionId = a.Id) as BidCount,
    (SELECT TOP 1 Points FROM Bids WHERE AuctionId = a.Id ORDER BY Points DESC) as HighestBid
    FROM Auctions a
    WHERE a.EndDate > GETDATE()
    ORDER BY EndDate ASC