CREATE PROCEDURE GetTotalAttentionHallPointsWithUsername
    @Username varchar(255)
AS  
	SELECT ISNULL(SUM(ap.Points), 0) as TotalPoints
	FROM AttentionHallPoints ap
	JOIN AttentionHallPosts a 
	ON a.Id = ap.PostId
	WHERE a.Username = @Username