CREATE PROCEDURE DeletePartnershipRequest
    @RequestUsername varchar(255),
    @Username varchar(255)
AS  
    DELETE FROM PartnershipRequests
    WHERE Username = @Username
    AND RequestUsername = @RequestUsername