CREATE PROCEDURE GetOfferCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM OfferCards
    WHERE Username = @Username
    ORDER BY FlipDate DESC