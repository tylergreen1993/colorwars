CREATE PROCEDURE GetTopLongestOwnedItems
AS  
    SELECT TOP 100 i.*, ui.*, ISNULL(ui.Name, i.Name) as Name, ISNULL(ui.ImageUrl, i.ImageUrl) as ImageUrl 
    FROM UserItems ui
    JOIN Items i ON i.Id = ui.ItemId
    JOIN Users u on u.Username = ui.Username
    WHERE u.InactiveType = 0
    AND u.ModifiedDate > DateADD(DD, -7, GETDATE())
    ORDER BY AddedDate