CREATE PROCEDURE AddClubMembership
    @Username varchar(255),
    @StartDate datetime,
    @EndDate datetime
AS  
    INSERT INTO ClubMemberships(Id, Username, ClaimedItem, StartDate, EndDate)
    VALUES(NEWID(), @Username, 0, @StartDate, @EndDate)