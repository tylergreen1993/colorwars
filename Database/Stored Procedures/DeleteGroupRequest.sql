CREATE PROCEDURE DeleteGroupRequest
    @Username varchar(255),
    @GroupId int
AS  
    DELETE FROM GroupRequests
    WHERE Username = @Username
    AND GroupId = @GroupId