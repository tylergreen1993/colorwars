CREATE PROCEDURE GetAllMatchStatesWithUsername
    @Username nvarchar(255),
    @GetInactive bit
AS  
    SELECT * FROM MatchState
    WHERE Username = @Username
    AND (@GetInactive = 1 OR IsDone = 0)
    ORDER BY Position ASC