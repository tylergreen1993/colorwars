CREATE PROCEDURE GetPasswordReset
    @Username varchar(255),
    @Id uniqueidentifier
AS  
    SELECT * 
    FROM PasswordResets 
    WHERE Username = @Username
    AND Id = @Id 
    AND CreatedDate >= DATEADD(minute, -30, GETDATE())