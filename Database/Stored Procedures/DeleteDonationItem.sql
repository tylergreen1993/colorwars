CREATE PROCEDURE DeleteDonationItem
    @Id uniqueidentifier
AS  
	DELETE FROM DonationItems
    WHERE SelectionId = @Id