CREATE PROCEDURE RemoveItemForUsername
    @Username nvarchar(255),
    @SelectionId uniqueidentifier
AS  
    DELETE FROM
    UserItems
    WHERE Username = @Username
    AND SelectionId = @SelectionId