CREATE PROCEDURE GetTradesWithSearch
    @Query varchar(255),
    @Theme int
AS  
    SELECT DISTINCT TOP 150 t.* 
    FROM Trades t
    JOIN TradeItems ti
    ON t.Item1Id = ti.SelectionId
    OR t.Item2Id = ti.SelectionId
    OR t.Item3Id = ti.SelectionId
    JOIN Items i ON
    ti.ItemId = i.Id
    WHERE t.IsAccepted = 0
    AND ISNULL(ti.Name, i.Name) like '%' + @Query + '%'
    AND (@Theme = 0 OR ti.Theme = @Theme)
    ORDER BY CreatedDate DESC