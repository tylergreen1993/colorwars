CREATE PROCEDURE AddWealthyDonor
    @Username varchar(255),
    @Amount int
AS  
    IF EXISTS (SELECT 1 FROM WealthyDonors WHERE Username = @Username)
    BEGIN
        UPDATE WealthyDonors
        SET Amount = Amount + @Amount
        WHERE Username = @Username
    END
    ELSE
    BEGIN
        INSERT INTO WealthyDonors(Username, Amount)
        VALUES(@Username, @Amount)
    END