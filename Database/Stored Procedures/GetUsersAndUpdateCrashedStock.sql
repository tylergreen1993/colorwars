CREATE PROCEDURE GetUsersAndUpdateCrashedStock
    @Id int
AS  
    DECLARE @CurrentCost float
    SET @CurrentCost = (SELECT CurrentCost FROM Stocks WHERE Id = @Id)
    IF @CurrentCost < 1
    BEGIN
        SELECT DISTINCT * FROM Users u
        JOIN UserStocks us
        ON us.Username = u.Username
        WHERE us.ItemId = @Id

        DELETE FROM UserStocks WHERE ItemId = @Id

        UPDATE Stocks
        SET CurrentCost = RAND() * 90 + 40 
        WHERE Id = @Id
    END