CREATE PROCEDURE UpdateDoubtItCard
    @Id uniqueidentifier,
    @Username varchar(255),
    @Owner int,
    @AddedDate datetime
AS  
    UPDATE DoubtItCards
    SET Owner = @Owner,
    AddedDate = @AddedDate
    WHERE Username = @Username
    AND Id = @Id