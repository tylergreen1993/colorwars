CREATE PROCEDURE DeleteMarketStallItemWithSelectionId
    @SelectionId uniqueidentifier
AS  
    DELETE FROM MarketStallItems
    WHERE SelectionId = @SelectionId