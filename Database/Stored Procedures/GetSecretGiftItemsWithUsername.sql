CREATE PROCEDURE GetSecretGiftItemsWithUsername
    @Username varchar(255)
AS  
    SELECT *, ISNULL(s.Name, i.Name) as Name, ISNULL(s.ImageUrl, i.ImageUrl) as ImageUrl
    FROM SecretGiftItems s
    JOIN Items i
    ON s.ItemId = i.Id
    WHERE s.Username = @Username