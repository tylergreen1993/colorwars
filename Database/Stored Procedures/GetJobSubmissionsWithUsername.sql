CREATE PROCEDURE GetJobSubmissionsWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM JobSubmissions
    WHERE Username = @Username
    ORDER BY CreatedDate DESC
