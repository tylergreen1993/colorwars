CREATE PROCEDURE GetJobSubmissionWithId
    @Id uniqueidentifier
AS  
    SELECT * FROM JobSubmissions
    WHERE Id = @Id
    ORDER BY CreatedDate DESC
