CREATE PROCEDURE AddSecretGiftItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Username varchar(255),
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO SecretGiftItems(SelectionId, ItemId, Username, Name, ImageUrl, Uses, Theme, CreatedDate, RepairedDate)
    VALUES (@SelectionId, @ItemId, @Username, @Name, @ImageUrl, @Uses, @Theme, @CreatedDate, @RepairedDate)