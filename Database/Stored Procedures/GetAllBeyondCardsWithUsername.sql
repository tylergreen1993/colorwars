CREATE PROCEDURE GetAllBeyondCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN BeyondCards b
    ON u.ItemId = b.Id
    WHERE u.Username = @Username
    AND i.Category = 18
    ORDER BY i.Points, i.Name ASC