CREATE PROCEDURE AddVoidMessage
    @Username nvarchar(255),
    @Message varchar(MAX)
AS  
    INSERT INTO VoidMessages(Id, Username, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Message, GETDATE())