CREATE PROCEDURE GetTradeItemWithSelectionId
    @SelectionId uniqueidentifier
AS  
    SELECT *, ISNULL(t.Name, i.Name) as Name, ISNULL(t.ImageUrl, i.ImageUrl) as ImageUrl
    FROM TradeItems t
    JOIN Items i
    ON t.ItemId = i.Id
    WHERE t.SelectionId = @SelectionId