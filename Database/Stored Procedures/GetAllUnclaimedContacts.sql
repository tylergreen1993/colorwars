CREATE PROCEDURE GetAllUnclaimedContacts
AS  
    SELECT * FROM Contacts
    WHERE ClaimedUser IS NULL
    ORDER BY CreatedDate DESC