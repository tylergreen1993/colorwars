CREATE PROCEDURE AddColorWarsHistory
    @TeamColor int,
    @Amount int
AS  
    INSERT INTO ColorWarsHistory(Id, TeamColor, Amount, CreatedDate)
    VALUES(NEWID(), @TeamColor, @Amount, GETDATE())