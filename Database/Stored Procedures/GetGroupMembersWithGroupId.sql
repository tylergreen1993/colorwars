CREATE PROCEDURE GetGroupMembersWithGroupId
    @Id int
AS  
    SELECT gm.*, u.ModifiedDate
    FROM GroupMembers gm
    JOIN Users u on gm.Username = u.Username
    WHERE GroupId = @Id
    ORDER BY Role DESC, JoinDate ASC, Username ASC