CREATE PROCEDURE CompleteColorWars
    @WinningTeamColor int,
    @AmountEarned int
AS  
    UPDATE Points
    SET Points = Points + @AmountEarned
    WHERE Username in (SELECT DISTINCT Username FROM ColorWarsSubmissions WHERE TeamColor = @WinningTeamColor)
    DELETE FROM ColorWarsSubmissions