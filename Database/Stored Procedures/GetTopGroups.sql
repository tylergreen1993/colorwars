CREATE PROCEDURE GetTopGroups
AS  
    SELECT TOP 100 g.*, MembersCount
    FROM Groups g
    JOIN
    (SELECT g.Id, COUNT(g.Id) AS MembersCount
    FROM Groups g
    JOIN GroupMembers gm
    ON g.Id = gm.GroupId
    WHERE gm.Role >= 0
    GROUP BY g.Id) g2
    ON g.Id = g2.Id
    ORDER BY MembersCount DESC
