CREATE PROCEDURE GetTransferHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM TransferHistory
    WHERE Transferer = @Username
    OR Transferee = @Username
    ORDER BY CreatedDate DESC