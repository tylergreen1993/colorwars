CREATE PROCEDURE GetTradesUserLeftOfferWithUsername
    @Username varchar(255)
AS  
    SELECT t.* FROM Trades t
    JOIN TradeOffers o
    ON t.Id = o.TradeId
    WHERE o.Username = @Username
    ORDER BY t.CreatedDate DESC