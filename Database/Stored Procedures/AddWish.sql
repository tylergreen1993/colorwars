CREATE PROCEDURE AddWish
    @Username nvarchar(255),
    @Content nvarchar(max),
    @IsGranted bit
AS  
    INSERT INTO Wishes(Id, Username, Content, IsGranted, CreatedDate)
    VALUES (NEWID(), @Username, @Content, @IsGranted, GETDATE())