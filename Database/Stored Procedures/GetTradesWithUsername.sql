CREATE PROCEDURE GetTradesWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM Trades
    WHERE Username = @Username
    ORDER BY CreatedDate DESC