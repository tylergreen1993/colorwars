CREATE PROCEDURE DeleteTrade
    @Id int
AS  
    DECLARE @Item1Id uniqueidentifier = (SELECT Item1Id FROM Trades WHERE Id = @Id)
    DECLARE @Item2Id uniqueidentifier = (SELECT Item2Id FROM Trades WHERE Id = @Id)
    DECLARE @Item3Id uniqueidentifier = (SELECT Item3Id FROM Trades WHERE Id = @Id)
    
    DECLARE @SelectionIds TABLE
    (
        Id uniqueidentifier
    )
    INSERT INTO @SelectionIds (Id) 
    SELECT Item1Id
    FROM TradeOffers 
    WHERE TradeId = @Id
    
    INSERT INTO @SelectionIds (Id) 
    SELECT Item2Id
    FROM TradeOffers 
    WHERE TradeId = @Id

    INSERT INTO @SelectionIds (Id) 
    SELECT Item3Id
    FROM TradeOffers 
    WHERE TradeId = @Id

    DELETE FROM TradeOffers
    WHERE TradeId = @Id
    
    DELETE FROM Trades
    WHERE Id = @Id

    DELETE FROM TradeItems
    WHERE SelectionId = @Item1Id
    OR SelectionId = @Item2Id
    OR SelectionId = @Item3Id
    
    DELETE FROM TradeItems
    WHERE SelectionId IN (SELECT Id FROM @SelectionIds)