CREATE PROCEDURE GetGroupRequestsWithGroupId
    @GroupId int
AS  
    SELECT * FROM GroupRequests
    WHERE GroupId = @GroupId
    ORDER BY CreatedDate DESC