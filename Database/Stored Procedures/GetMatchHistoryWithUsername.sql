CREATE PROCEDURE GetMatchHistoryWithUsername
    @Username nvarchar(255)
AS  
    SELECT m.*, ISNULL(u.Color, '') as OpponentColor
    FROM MatchHistory m
    LEFT JOIN Users u on m.Opponent = u.Username
    WHERE m.Username = @Username
    ORDER BY CreatedDate DESC