CREATE PROCEDURE DeleteSettingForUsername
    @Username nvarchar(255),
    @Name nvarchar(255)
AS  
    BEGIN TRAN
        IF EXISTS (SELECT * FROM Settings 
            WHERE Username = @Username
            AND Name = @Name)
            BEGIN
                DELETE FROM Settings
                WHERE Username = @Username
                AND Name = @Name
            END
    COMMIT TRAN