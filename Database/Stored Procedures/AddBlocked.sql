CREATE PROCEDURE AddBlocked
    @Username varchar(255),
    @BlockedUser varchar(255)
AS  
    INSERT INTO Blocked(Username, BlockedUser, CreatedDate)
    VALUES (@Username, @BlockedUser, GETDATE())