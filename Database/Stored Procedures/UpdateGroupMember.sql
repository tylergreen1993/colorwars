CREATE PROCEDURE UpdateGroupMember
    @GroupId int,
    @Username varchar(255),
    @Role int,
    @IsPrizeMember bit,
    @IsPrimary bit,
    @LastReadMessageDate datetime
AS  
    UPDATE GroupMembers
    SET Role = @Role,
    IsPrizeMember = @IsPrizeMember,
    IsPrimary =  @IsPrimary,
    LastReadMessageDate = @LastReadMessageDate
    WHERE GroupId = @GroupId
    AND Username = @Username