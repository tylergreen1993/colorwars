CREATE PROCEDURE GetRecentUsers
    @Gender int
AS  
    SELECT * FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.Gender = @Gender
    AND u.CreatedDate > DATEADD(day, -14, GETDATE())
    ORDER BY u.CreatedDate DESC
    