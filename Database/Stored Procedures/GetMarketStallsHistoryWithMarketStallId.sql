CREATE PROCEDURE GetMarketStallsHistoryWithMarketStallId
    @Id int
AS  
    SELECT m.*, ISNULL(m.Name, i.Name) as ItemName
    FROM MarketStallsHistory m
    JOIN Items i
    ON i.Id = m.ItemId
    WHERE m.MarketStallId = @Id
    ORDER BY m.CreatedDate DESC