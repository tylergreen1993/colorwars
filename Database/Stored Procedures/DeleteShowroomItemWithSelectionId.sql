CREATE PROCEDURE DeleteShowroomItemWithSelectionId
    @SelectionId uniqueidentifier
AS  
    DELETE FROM ShowroomItems
    WHERE SelectionId = @SelectionId