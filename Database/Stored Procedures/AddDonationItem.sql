CREATE PROCEDURE AddDonationItem
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO DonationItems(Username, SelectionId, ItemId, Uses, Theme, CreatedDate, RepairedDate, AddedDate)
    VALUES(@Username, NEWID(), @ItemId, @Uses, @Theme, @CreatedDate, @RepairedDate, GETDATE())