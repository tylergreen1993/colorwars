CREATE PROCEDURE AddMarketStallsHistory
    @MarketStallId int,
    @Username varchar(255),
    @ItemId uniqueidentifier,
    @Name varchar(255) = NULL,
    @Cost int,
    @Theme int
AS  
    INSERT INTO MarketStallsHistory(Id, MarketStallId, Username, ItemId, Name, Cost, Theme, CreatedDate)
    VALUES (NEWID(), @MarketStallId, @Username, @ItemId, @Name, @Cost, @Theme, GETDATE())