CREATE PROCEDURE GetStreakCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM StreakCards
    WHERE Username = @Username
    ORDER BY FlipDate DESC