CREATE PROCEDURE UpdateAttentionHallPost
    @Id int,
    @Title varchar(255),
    @Content varchar(MAX)
AS  
    UPDATE AttentionHallPosts
    SET Title = @Title,
    Content = @Content,
    ModifiedDate = GETDATE()
    WHERE Id = @Id
