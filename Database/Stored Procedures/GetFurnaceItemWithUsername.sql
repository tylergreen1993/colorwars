CREATE PROCEDURE GetFurnaceItemWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM FurnaceItems f
    JOIN Items i
    ON f.ItemId = i.Id
    WHERE f.Username = @Username