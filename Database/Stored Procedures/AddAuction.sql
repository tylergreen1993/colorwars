CREATE PROCEDURE AddAuction
    @Username varchar(255),
    @StartPoints int,
    @ItemId uniqueidentifier,
    @MinBidIncrement int = 1,
    @EndDate datetime
AS  
    DECLARE @OutputId TABLE (Id int)

    INSERT INTO Auctions(Username, StartPoints, ItemId, MinBidIncrement, CreatorCollected, WinnerCollected, SentNotification, EndDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES (@Username, @StartPoints, @ItemId, @MinBidIncrement, 0, 0, 0, @EndDate)
    
    SELECT * FROM Auctions
    WHERE Id in (SELECT Id FROM @OutputId)