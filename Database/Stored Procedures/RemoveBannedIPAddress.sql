CREATE PROCEDURE RemoveBannedIPAddress
    @IPAddress varchar(250)
AS  
	DELETE FROM BannedIPAddresses
    WHERE IPAddress = @IPAddress