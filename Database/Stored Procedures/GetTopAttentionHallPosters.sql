CREATE PROCEDURE GetTopAttentionHallPosters
AS  
    SELECT TOP 100 u.Username, u.Color, u.DisplayPicUrl, COUNT(*) as Amount FROM AttentionHallPosts p
    JOIN Users u ON p.Username = u.Username
    WHERE u.InactiveType = 0
    AND u.ModifiedDate > DateADD(DD, -30, GETDATE())
    GROUP BY u.Username, u.Color, u.DisplayPicUrl
    ORDER BY Amount DESC