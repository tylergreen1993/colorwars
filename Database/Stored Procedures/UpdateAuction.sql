CREATE PROCEDURE UpdateAuction
    @Id int,
    @StartPoints int,
    @CreatorCollected bit,
    @WinnerCollected bit,
    @SentNotification bit,
    @EndDate datetime
AS  
    Update Auctions
    SET StartPoints = @StartPoints,
    CreatorCollected = @CreatorCollected,
    WinnerCollected = @WinnerCollected,
    SentNotification = @SentNotification,
    EndDate = @EndDate
    WHERE Id = @Id