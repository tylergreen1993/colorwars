CREATE PROCEDURE GetNotificationsWithUsername
    @Username varchar(255),
    @MarkAsRead bit
AS  
    SELECT * FROM Notifications
    WHERE Username = @Username
    ORDER BY CreatedDate DESC

    IF @MarkAsRead = 1
    BEGIN
        UPDATE Notifications
        SET SeenDate = GETDATE()
        WHERE Username = @Username
        AND SeenDate IS NULL
    END