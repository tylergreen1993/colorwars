CREATE PROCEDURE AddJunkyardItem
    @ItemId uniqueidentifier,
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO JunkyardItems(SelectionId, ItemId, Uses, Theme, CreatedDate, RepairedDate, AddedDate)
    VALUES(NEWID(), @ItemId, @Uses, @Theme, @CreatedDate, @RepairedDate, GETDATE())