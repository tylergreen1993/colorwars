CREATE PROCEDURE RemoveInactiveAccount
    @Username varchar(255)
AS  
	DELETE FROM InactiveAccounts
    WHERE Username = @Username AND Type = 1