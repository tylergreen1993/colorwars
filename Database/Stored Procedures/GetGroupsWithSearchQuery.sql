CREATE PROCEDURE GetGroupsWithSearchQuery
    @SearchQuery varchar(255)
AS  
    SELECT g.*, MembersCount
    FROM Groups g
    JOIN
    (SELECT g.Id, COUNT(g.Id) AS MembersCount
    FROM Groups g
    JOIN GroupMembers gm
    ON g.Id = gm.GroupId
    WHERE g.Name like '%' + @SearchQuery + '%'
    OR g.Description like '%' + @SearchQuery + '%'
    AND gm.Role >= 0
    GROUP BY g.Id) g2
    ON g.Id = g2.Id
    ORDER BY MembersCount DESC
