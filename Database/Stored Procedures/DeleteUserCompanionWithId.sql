CREATE PROCEDURE DeleteUserCompanionWithId
    @Id uniqueidentifier
AS  
    DELETE FROM UserCompanions
    WHERE Id = @Id