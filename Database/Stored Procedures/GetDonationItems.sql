CREATE PROCEDURE GetDonationItems
AS  
    SELECT TOP 50 *
    FROM DonationItems d
    JOIN Items i
    ON d.ItemId = i.Id
    ORDER BY d.AddedDate DESC