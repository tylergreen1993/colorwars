CREATE PROCEDURE GetAttentionHallPost
    @PostId int,
    @Username varchar(255)
AS  
    SELECT a.*,
    (SELECT ISNULL(SUM(Points), 0) FROM AttentionHallPoints WHERE PostId = Id) AS Points, 
    ISNULL((SELECT TOP 1 Points FROM AttentionHallPoints WHERE PostId = Id AND Username = @Username), 0) AS UserVote,
    (SELECT COUNT(*) FROM AttentionHallComments WHERE PostId = a.Id) AS TotalComments
    FROM AttentionHallPosts a
    WHERE a.Id = @PostId