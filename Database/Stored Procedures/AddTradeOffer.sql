CREATE PROCEDURE AddTradeOffer
    @TradeId int,
    @Username varchar(255),
    @Item1Id uniqueidentifier = NULL,
    @Item2Id uniqueidentifier = NULL,
    @Item3Id uniqueidentifier = NULL
AS  
    INSERT INTO TradeOffers(Id, TradeId, Username, Item1Id, Item2Id, Item3Id, IsAccepted, CreatedDate)
    VALUES(NEWID(), @TradeId, @Username, @Item1Id, @Item2Id, @Item3Id, 0, GETDATE())