CREATE PROCEDURE AddLuckyGuess
    @Position int,
    @Username nvarchar(255)
AS  
    INSERT INTO LuckyGuesses(Position, Username, CreatedDate)
    VALUES(@Position, @Username, GETDATE())