CREATE PROCEDURE AddSurveyAnswer
    @Username varchar(255),
    @SurveyQuestionId int,
    @Answer varchar(MAX)
AS  
    INSERT INTO SurveyAnswers(Id, Username, SurveyQuestionId, Answer, CreatedDate)
    VALUES(NEWID(), @Username, @SurveyQuestionId, @Answer, GETDATE())