CREATE PROCEDURE AddGroupMessage
    @GroupId int,
    @Sender nvarchar(255),
    @Content nvarchar(max)
AS  
    INSERT INTO GroupMessages(Id, GroupId, Sender, Content, IsDeleted, CreatedDate)
    VALUES (NEWID(), @GroupId, @Sender, @Content, 0, GETDATE())