CREATE PROCEDURE GetFavoritedLocations
    @Username varchar(255)
AS  
    SELECT * FROM FavoritedLocations
    WHERE Username = @Username
    ORDER BY Position ASC