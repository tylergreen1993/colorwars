CREATE PROCEDURE AddRightSideUpGuardHallOfFame
    @Username nvarchar(255)
AS  
    INSERT INTO RightSideUpGuardHallOfFame(Username, CreatedDate)
    VALUES (@Username, GETDATE())