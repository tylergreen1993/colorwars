CREATE PROCEDURE GetSafetyDepositItemsWithUsername
    @Username varchar(255)
AS  
    SELECT *, ISNULL(s.Name, i.Name) as Name, ISNULL(s.ImageUrl, i.ImageUrl) as ImageUrl
    FROM SafetyDepositItems s
    JOIN Items i
    ON s.ItemId = i.Id
    WHERE s.Username = @Username
    ORDER BY i.Category, i.Points, i.Name, s.CreatedDate ASC