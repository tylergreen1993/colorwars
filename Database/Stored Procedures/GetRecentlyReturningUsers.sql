CREATE PROCEDURE GetRecentlyReturningUsers
AS 
    SELECT * FROM Users
    WHERE DATEDIFF(hour, CreatedDate, GETDATE()) >= 24
    AND DATEDIFF(hour, ModifiedDate, GETDATE()) < 24