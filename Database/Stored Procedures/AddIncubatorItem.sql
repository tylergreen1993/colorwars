CREATE PROCEDURE AddIncubatorItem
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Username varchar(255),
    @Uses int,
    @Theme int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO IncubatorItems(SelectionId, ItemId, Username, Uses, Theme, CreatedDate, RepairedDate)
    VALUES (@SelectionId, @ItemId, @Username, @Uses, @Theme, @CreatedDate, @RepairedDate)