CREATE PROCEDURE GetGroupsWithUsername
    @Username varchar(255)
AS  
    SELECT g.*, 
    gm.IsPrimary, 
    gm.LastReadMessageDate, 
    gm.IsPrizeMember,
    gm.Role,
    (SELECT COUNT(*) FROM GroupMessages WHERE GroupId = g.Id AND CreatedDate > gm.LastReadMessageDate AND CreatedDate > gm.JoinDate) as UnreadMessages,
    (SELECT TOP 1 CreatedDate FROM GroupMessages WHERE GroupId = g.Id ORDER BY CreatedDate DESC) as LastGroupMessageDate 
    FROM Groups g
    JOIN GroupMembers gm 
    ON g.Id = gm.GroupId
    WHERE gm.Username = @Username
    AND gm.Role >= 0