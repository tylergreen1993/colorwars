CREATE PROCEDURE GetDisposerRecordsInLastDays
    @Days int
AS  
    SELECT TOP 100 CAST(RANK() OVER (ORDER BY COUNT(*) DESC) AS INT) as Rank, COUNT(*) as Amount, Username
    FROM DisposerRecords
    WHERE CreatedDate >= DATEADD(DAY, -@Days, GETDATE())
    GROUP BY Username