CREATE PROCEDURE UpdateGroup
    @GroupId int,
    @Name varchar(255),
    @DisplayPicUrl varchar(MAX) = NULL,
    @Power int,
    @Description varchar(MAX),
    @IsPrivate bit,
    @Whiteboard varchar(MAX),
    @LastPrizeDate datetime,
    @LastPowerChangeDate datetime
AS  
    UPDATE Groups
    SET Name = @Name,
    DisplayPicUrl = @DisplayPicUrl,
    Power = @Power,
    Description = @Description,
    IsPrivate = @IsPrivate,
    Whiteboard = @Whiteboard,
    LastPrizeDate = @LastPrizeDate,
    LastPowerChangeDate = @LastPowerChangeDate
    WHERE Id = @GroupId