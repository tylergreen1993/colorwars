CREATE PROCEDURE GetAllClaimedContacts
AS  
    SELECT * FROM Contacts
    WHERE IsCompleted = 1
    ORDER BY CreatedDate DESC