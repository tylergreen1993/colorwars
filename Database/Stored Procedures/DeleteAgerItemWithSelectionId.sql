CREATE PROCEDURE DeleteAgerItemWithSelectionId
    @Id uniqueidentifier
AS  
	DELETE FROM AgerItems
    WHERE SelectionId = @Id