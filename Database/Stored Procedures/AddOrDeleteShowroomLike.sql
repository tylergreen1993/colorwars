CREATE PROCEDURE AddOrDeleteShowroomLike
    @Username varchar(255),
    @ShowroomUsername varchar(255)
AS  
    IF EXISTS(SELECT 1 FROM ShowroomLikes WHERE Username = @Username AND ShowroomUsername = @ShowroomUsername)
    BEGIN
        DELETE FROM ShowroomLikes
        WHERE Username = @Username
        AND ShowroomUsername = @ShowroomUsername
    END
    ELSE
    BEGIN
        INSERT INTO ShowroomLikes(Username, ShowroomUsername, CreatedDate)
        VALUES(@Username, @ShowroomUsername, GETDATE())
    END