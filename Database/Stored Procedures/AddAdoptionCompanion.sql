CREATE PROCEDURE AddAdoptionCompanion
    @Id uniqueidentifier,
    @Type int, 
    @Name varchar(255),
    @Color int,
    @Level int,
    @CreatedDate datetime
AS  
    INSERT INTO AdoptionCompanions(Id, Type, Name, Color, Level, CreatedDate, AddedDate)
    VALUES (@Id, @Type, @Name, @Color, @Level, @CreatedDate, GETDATE())