CREATE PROCEDURE AddBuyersClubItem
    @ItemId uniqueidentifier,
    @Cost int
AS  
    IF ((SELECT COUNT(*) FROM BuyersClubItems) <= 25)
    BEGIN
        INSERT INTO BuyersClubItems(SelectionId, Id, Cost, CreatedDate)
        VALUES(NEWID(), @ItemId, @Cost, GETDATE())
    END