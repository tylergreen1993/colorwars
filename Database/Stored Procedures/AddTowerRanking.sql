CREATE PROCEDURE AddTowerRanking
    @Username nvarchar(255),
    @TopFloorLevel int
AS  
    INSERT INTO TowerRankings(Id, Username, TopFloorLevel, CreatedDate)
    VALUES(NEWID(), @Username, @TopFloorLevel, GETDATE())