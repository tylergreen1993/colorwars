CREATE PROCEDURE GetAuctionsWithSearch
    @Query varchar(255),
    @Theme int
AS  
    SELECT TOP 150 a.*,
    (SELECT COUNT(*) FROM Bids WHERE AuctionId = a.Id) as BidCount,
    (SELECT TOP 1 Points FROM Bids WHERE AuctionId = a.Id ORDER BY Points DESC) as HighestBid
    FROM Auctions a
    JOIN AuctionItems ai
    ON a.ItemId = ai.SelectionId
    JOIN Items i ON
    ai.ItemId = i.Id
    WHERE a.EndDate > GETDATE()
    AND ISNULL(ai.Name, i.Name) like '%' + @Query + '%'
    AND (@Theme = 0 OR ai.Theme = @Theme)
    ORDER BY EndDate ASC