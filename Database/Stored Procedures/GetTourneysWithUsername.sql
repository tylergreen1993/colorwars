CREATE PROCEDURE GetTourneysWithUsername
    @Username varchar(255)
AS  
    SELECT DISTINCT TOP 150 t.* 
    FROM Tourneys t
    JOIN TourneyMatches tm on tm.TourneyId = t.Id
    WHERE tm.Round = 0
    AND (t.Creator = @Username OR tm.Username1 = @Username OR tm.Username2 = @Username)
    ORDER BY t.CreatedDate DESC