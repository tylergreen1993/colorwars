CREATE PROCEDURE AddAttentionHallPoint
    @PostId int,
    @Username varchar(255),
    @Points int
AS  
    INSERT INTO AttentionHallPoints(PostId, Username, Points, CreatedDate)
    VALUES(@PostId, @Username, @Points, GETDATE())