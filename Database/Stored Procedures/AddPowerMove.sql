CREATE PROCEDURE AddPowerMove
    @Username varchar(255),
    @Type int
AS  
    INSERT INTO UserPowerMoves(Type, Username, IsActive, CreatedDate)
    VALUES(@Type, @Username, 0, GETDATE())