CREATE PROCEDURE GetAllArtCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN ArtCards a
    ON u.ItemId = a.Id
    WHERE u.Username = @Username
    AND i.Category = 16
    ORDER BY i.Points, i.Name ASC