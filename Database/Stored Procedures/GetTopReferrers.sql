CREATE PROCEDURE GetTopReferrers
    @Days int
AS  
    SELECT TOP 100 CAST(RANK() OVER (ORDER BY COUNT(*) DESC) AS INT) as Rank, Referral as Referrer, COUNT(*) as Amount 
    FROM Users
    WHERE Referral != ''
    AND CreatedDate >= DATEADD(day, -@Days, GETDATE())
    GROUP BY Referral
    ORDER BY Amount DESC