CREATE PROCEDURE AddTransferHistory
    @TransferId int,
    @Type int,
    @Transferer varchar(255),
    @Transferee varchar(255),
    @DifferenceAmount int
AS  
    INSERT INTO TransferHistory(Id, TransferId, Type, Transferer, Transferee, DifferenceAmount, CreatedDate)
    VALUES(NEWID(), @TransferId, @Type, @Transferer, @Transferee, @DifferenceAmount, GETDATE())