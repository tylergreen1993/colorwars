CREATE PROCEDURE GetContactsWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM Contacts c
    WHERE c.Username = @Username
    ORDER BY CreatedDate DESC