CREATE PROCEDURE GetUsersByIPAddress
    @IPAddress nvarchar(255),
    @IncludeSessions bit
AS  
	SELECT u.*, ISNULL(Points, 0) as Points
    FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.IPAddress = @IPAddress
    OR (@IncludeSessions = 1 AND @IPAddress IN (SELECT IPAddress FROM AccessTokens WHERE Username = u.username))