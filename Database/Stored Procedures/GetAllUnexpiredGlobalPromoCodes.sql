CREATE PROCEDURE GetAllUnexpiredGlobalPromoCodes
AS  
    SELECT * FROM PromoCodes p
    WHERE ExpiryDate > GETDATE()
    AND OneUserUse = 0
    ORDER BY ExpiryDate DESC