CREATE PROCEDURE GetDAUMAUStatistic
AS  
    SELECT CAST((SELECT COUNT(*) FROM Users WHERE ModifiedDate >= DATEADD(day, -1, GETDATE())) AS FLOAT) / (SELECT COUNT(*) FROM Users WHERE ModifiedDate >= DATEADD(day, -30, GETDATE())) * 100 as TotalPercent