CREATE PROCEDURE GetAllUsers
    @Gender int,
    @Days int
AS  
    SELECT * FROM Users u
    LEFT JOIN Points p
    ON u.Username = p.Username
    WHERE u.Gender = @Gender
    AND u.CreatedDate < DATEADD(day, -@Days, GETDATE())
    ORDER BY u.CreatedDate DESC