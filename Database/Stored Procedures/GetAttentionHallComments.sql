CREATE PROCEDURE GetAttentionHallComments
    @PostId int,
    @Username varchar(255)
AS  
    SELECT a.*, 
    (SELECT COUNT(*) FROM AttentionHallCommentLikes WHERE CommentId = a.Id AND PostId = PostId) as TotalLikes, 
    (SELECT ISNULL(STRING_AGG(al.Username, ', '), '') FROM AttentionHallCommentLikes al WHERE al.CommentId = a.Id AND al.PostId = PostId) as LikedUsernames,
    CAST(ISNULL((SELECT 1 FROM AttentionHallCommentLikes WHERE CommentId = a.Id AND PostId = PostId AND Username = @Username), 0) AS BIT) AS UserLiked,
    u.Color, u.DisplayPicUrl
    FROM AttentionHallComments a
    JOIN Users u ON
    a.Username = u.Username
    WHERE PostId = @PostId
    ORDER BY TotalLikes DESC, a.CreatedDate DESC