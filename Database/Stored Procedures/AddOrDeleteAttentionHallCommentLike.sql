CREATE PROCEDURE AddOrDeleteAttentionHallCommentLike
    @PostId int,
    @CommentId uniqueidentifier,
    @Username varchar(255)
AS  
    IF EXISTS(SELECT * FROM AttentionHallCommentLikes WHERE PostId = @PostId AND CommentId = @CommentId AND Username = @Username)
    BEGIN
        DELETE FROM AttentionHallCommentLikes
        WHERE PostId = @PostId
        AND CommentId = @CommentId
        AND Username = @Username
    END
    ELSE
    BEGIN
        INSERT INTO AttentionHallCommentLikes(PostId, CommentId, Username, CreatedDate)
        VALUES(@PostId, @CommentId, @Username, GETDATE())
    END