CREATE PROCEDURE AddNewsComment
	@ParentCommentId uniqueidentifier = NULL,
    @NewsId int,
    @Username varchar(255),
    @Message varchar(MAX)
AS  
	DECLARE @Id uniqueidentifier = NEWID()

    INSERT INTO NewsComments(Id, ParentCommentId, NewsId, Username, Message, CommentDate)
    VALUES (@Id, @ParentCommentId, @NewsId, @Username, @Message, GETDATE())

    SELECT * FROM NewsComments
    WHERE Id = @Id