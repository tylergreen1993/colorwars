CREATE PROCEDURE GetIncubatorItemWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM IncubatorItems inc
    JOIN Items i
    ON inc.ItemId = i.Id
    WHERE inc.Username = @Username