CREATE PROCEDURE AddSwapDeckHistory
    @Username varchar(255),
    @Type int,
    @Message varchar(MAX)
AS  
    INSERT INTO SwapDeckHistory(Id, Username, Type, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Type, @Message, GETDATE())