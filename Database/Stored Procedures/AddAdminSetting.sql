CREATE PROCEDURE AddAdminSetting
    @Name nvarchar(255),
    @Value nvarchar(255)
AS  
    BEGIN TRAN
        IF EXISTS (SELECT * FROM AdminSettings WHERE Name = @Name)
            BEGIN
                UPDATE AdminSettings
                SET Value = @Value
                WHERE Name = @Name
            END
        ELSE
            BEGIN
                INSERT INTO AdminSettings(Id, Name, Value, CreatedDate)
                VALUES(NEWID(), @Name, @Value, GETDATE())
            END
    COMMIT TRAN