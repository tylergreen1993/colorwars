CREATE PROCEDURE AddLibraryHistory
    @Username varchar(255),
    @Type int,
    @Message varchar(MAX)
AS  
    INSERT INTO LibraryHistory(Id, Username, Type, Message, CreatedDate)
    VALUES (NEWID(), @Username, @Type, @Message, GETDATE())