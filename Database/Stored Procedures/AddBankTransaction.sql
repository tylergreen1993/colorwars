CREATE PROCEDURE AddBankTransaction
    @Username varchar(255),
    @Type int,
    @Amount int
AS  
    INSERT INTO BankTransactions(Username, Type, Amount, TransactionDate)
    VALUES (@Username, @Type, @Amount, GETDATE())