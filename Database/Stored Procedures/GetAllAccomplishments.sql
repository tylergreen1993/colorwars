CREATE PROCEDURE GetAllAccomplishments
AS  
	DECLARE @UserCount float = (SELECT COUNT(*) FROM Users WHERE InactiveType = 0)
    SELECT *, (CAST((SELECT COUNT(*) FROM UserAccomplishments WHERE AccomplishmentId = a.Id) AS float)/@UserCount * 100) as PercentOwned
    FROM Accomplishments a
    ORDER BY Category, Name
