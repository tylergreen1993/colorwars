CREATE PROCEDURE DeleteNews
    @Id int
AS  
    DELETE FROM NewsComments
    WHERE NewsId = @Id
    DELETE FROM News
    WHERE Id = @Id