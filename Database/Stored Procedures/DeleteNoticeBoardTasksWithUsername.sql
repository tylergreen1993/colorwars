CREATE PROCEDURE DeleteNoticeBoardTasksWithUsername
    @Username varchar(255)
AS  
    DELETE FROM NoticeBoardTasks
    WHERE Username = @Username