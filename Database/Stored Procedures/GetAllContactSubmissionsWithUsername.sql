CREATE PROCEDURE GetAllContactSubmissionsWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM Contacts
    WHERE Username = @Username
    ORDER BY CreatedDate DESC