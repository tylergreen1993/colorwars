CREATE PROCEDURE AddNotification
    @Username varchar(255),
    @Message varchar(MAX),
    @Location int,
    @Href varchar(255),
    @ImageUrl varchar(255)
AS  
	DECLARE @Id UNIQUEIDENTIFIER = NEWID()
    INSERT INTO Notifications(Id, Username, Message, Location, Href, ImageUrl, SeenDate, CreatedDate)
    VALUES (@Id, @Username, @Message, @Location, @Href, @ImageUrl, NULL, GETDATE())

    SELECT * FROM Notifications
    WHERE Id = @Id