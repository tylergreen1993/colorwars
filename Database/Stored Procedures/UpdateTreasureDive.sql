CREATE PROCEDURE UpdateTreasureDive
    @Username varchar(255),
    @Attempts int,
    @LastGuessCoordinates varchar(MAX)
AS  
    UPDATE TreasureDives
    SET Attempts = @Attempts,
    LastGuessCoordinates = @LastGuessCoordinates
    WHERE Username = @Username