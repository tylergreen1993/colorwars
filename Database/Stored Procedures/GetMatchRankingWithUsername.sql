CREATE PROCEDURE GetMatchRankingWithUsername
    @Username nvarchar(255)
AS  
    IF (SELECT COUNT(*) FROM MatchHistory WHERE Username = @Username AND CPUType = 0) != 0
    BEGIN
        SELECT * FROM GetMatchRankings()
        WHERE Username = @Username
    END