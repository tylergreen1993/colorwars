CREATE PROCEDURE UpdatePoints
    @Username nvarchar(255),   
    @Points int
AS    
UPDATE Points
SET Points = @Points
WHERE Username = @Username