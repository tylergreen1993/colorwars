CREATE PROCEDURE AddHiddenFeedCategory
    @Username varchar(255),
    @FeedId int
AS  
    INSERT INTO HiddenFeedCategories(Username, FeedId, CreatedDate)
    VALUES (@Username, @FeedId, GETDATE())
