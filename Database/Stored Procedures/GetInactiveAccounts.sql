CREATE PROCEDURE GetInactiveAccounts
    @Type int
AS  
	SELECT * FROM InactiveAccounts
    WHERE Type = @Type
    ORDER BY InactiveDate DESC