CREATE PROCEDURE GetMysteryShowcase
AS  
    SELECT *
    FROM MysteryShowcase m
    JOIN Items i
    ON m.Id = i.Id
    ORDER BY i.Category, i.Name ASC