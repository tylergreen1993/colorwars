CREATE PROCEDURE GetAllCouponsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Coupons c
    ON u.ItemId = c.Id
    WHERE u.Username = @Username
    AND i.Category = 5
    ORDER BY i.Points, i.Name ASC