CREATE PROCEDURE UpdateShowroomItemWithSelectionId
    @SelectionId uniqueidentifier,
    @Position int,
    @Uses int,
    @RepairedDate datetime
AS  
    UPDATE ShowroomItems
    SET Position = @Position,
    Uses = @Uses,
    RepairedDate = @RepairedDate
    WHERE SelectionId = @SelectionId