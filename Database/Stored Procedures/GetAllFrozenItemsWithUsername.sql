CREATE PROCEDURE GetAllFrozenItemsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN FrozenItems f
    ON u.ItemId = f.Id
    WHERE u.Username = @Username
    AND i.Category = 13
    ORDER BY i.Points, i.Name ASC