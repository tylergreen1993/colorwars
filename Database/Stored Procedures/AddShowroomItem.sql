CREATE PROCEDURE AddShowroomItem
    @Username varchar(255),
    @SelectionId uniqueidentifier,
    @ItemId uniqueidentifier,
    @Name varchar(255) = NULL,
    @ImageUrl varchar(255) = NULL,
    @Uses int,
    @Theme int,
    @Position int,
    @CreatedDate datetime,
    @RepairedDate datetime
AS  
    INSERT INTO ShowroomItems(Username, SelectionId, ItemId, Name, ImageUrl, Uses, Theme, Position, CreatedDate, RepairedDate, AddedDate)
    VALUES (@Username, @SelectionId, @ItemId, @Name, @ImageUrl, @Uses, @Theme, @Position, @CreatedDate, @RepairedDate, GETDATE())