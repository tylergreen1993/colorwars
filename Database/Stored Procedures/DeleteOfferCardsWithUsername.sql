CREATE PROCEDURE DeleteOfferCardsWithUsername
    @Username nvarchar(255)
AS  
    DELETE FROM OfferCards
    WHERE Username = @Username