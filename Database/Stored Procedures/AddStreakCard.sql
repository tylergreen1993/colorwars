CREATE PROCEDURE AddStreakCard
    @Username nvarchar(255),
    @Position nvarchar(255),
    @Points varchar(MAX)
AS  
    DELETE FROM StreakCards
    WHERE Username = @Username
    AND FlipDate < DATEADD(HOUR, -8, GETDATE())
    INSERT INTO StreakCards(Id, Username, Position, Points, FlipDate)
    VALUES (NEWID(), @Username, @Position, @Points, GETDATE())