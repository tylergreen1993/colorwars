CREATE PROCEDURE AddMessage
    @Sender nvarchar(255),
    @Recipient nvarchar(255),
    @Content nvarchar(max)
AS  
	DECLARE @Id UNIQUEIDENTIFIER = NEWID()
    INSERT INTO Messages(Id, Sender, Recipient, Content, IsRead, CreatedDate)
    VALUES (@Id, @Sender, @Recipient, @Content, 0, GETDATE())
    
    SELECT * FROM Messages
    WHERE Id = @Id