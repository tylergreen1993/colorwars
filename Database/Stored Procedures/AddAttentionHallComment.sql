CREATE PROCEDURE AddAttentionHallComment
	@ParentCommentId uniqueidentifier = NULL,
    @PostId int,
    @Username varchar(255),
    @Comment varchar(MAX)
AS  
	DECLARE @Id uniqueidentifier = NEWID()

    INSERT INTO AttentionHallComments(Id, ParentCommentId, PostId, Username, Comment, CreatedDate)
    VALUES(@Id, @ParentCommentId, @PostId, @Username, @Comment, GETDATE())

    SELECT * FROM AttentionHallComments
    WHERE Id = @Id