CREATE PROCEDURE GetSurveyAnswersWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM SurveyAnswers
    WHERE Username = @Username
    ORDER BY CreatedDate DESC