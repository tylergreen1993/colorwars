CREATE PROCEDURE GetAgerItemWithUsername
    @Username varchar(255)
AS  
    SELECT *, ISNULL(a.Name, i.Name) as Name, ISNULL(a.ImageUrl, i.ImageUrl) as ImageUrl
    FROM AgerItems a
    JOIN Items i
    ON a.ItemId = i.Id
    WHERE a.Username = @Username