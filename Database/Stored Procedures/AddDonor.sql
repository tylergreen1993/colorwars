CREATE PROCEDURE AddDonor
    @Username varchar(255),
    @Amount int
AS  
    INSERT INTO Donors(Id, Username, Amount, CreatedDate)
    VALUES (NEWID(), @Username, @Amount, GETDATE())