CREATE PROCEDURE GetAllKeyCardsWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN KeyCards k
    ON u.ItemId = k.Id
    WHERE u.Username = @Username
    AND i.Category = 19
    ORDER BY i.Points, i.Name ASC