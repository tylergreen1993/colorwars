CREATE PROCEDURE AddMarketStall
    @Username varchar(255),
    @Name varchar(255),
    @Description varchar(MAX) = NULL,
    @Color varchar(255) = NULL
AS  
    DECLARE @OutputId TABLE (Id int)

    INSERT INTO MarketStalls(Username, Name, Description, Color, Discount, CreatedDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES (@Username, @Name, @Description, @Color, 0, GETDATE())

    SELECT * FROM MarketStalls
    WHERE Id in (SELECT Id FROM @OutputId)