CREATE PROCEDURE GetAllUpgradersWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Upgraders up
    ON u.ItemId = up.Id
    WHERE u.Username = @Username
    AND i.Category = 10
    ORDER BY i.Points, i.Name ASC