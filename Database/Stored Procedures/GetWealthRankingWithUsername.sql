CREATE PROCEDURE GetWealthRankingWithUsername
    @Username nvarchar(255)
AS  
    SELECT * FROM GetWealthRankings()
    WHERE Username = @Username