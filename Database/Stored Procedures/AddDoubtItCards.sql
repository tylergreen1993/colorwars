CREATE PROCEDURE AddDoubtItCards
    @Username varchar(255),
    @Amounts varchar(MAX),
    @Owner int
AS  
    INSERT INTO DoubtItCards(Id, Username, Amount, Owner, AddedDate)
    SELECT NEWID(), @Username, CAST(value AS INT), @Owner, GETDATE()
    FROM  STRING_SPLIT(@Amounts, ',') 