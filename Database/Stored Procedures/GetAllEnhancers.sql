CREATE PROCEDURE GetAllEnhancers
AS  
    SELECT *
    FROM Items i
    JOIN Enhancers e
    ON i.Id = e.Id
    WHERE i.Category = 1
    ORDER BY i.Points ASC