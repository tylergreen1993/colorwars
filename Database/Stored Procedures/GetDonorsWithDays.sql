CREATE PROCEDURE GetDonorsWithDays
    @Days int
AS  
    SELECT * FROM Donors
    WHERE CreatedDate > DateADD(DD, -@Days, GETDATE())
    ORDER BY CreatedDate DESC