CREATE PROCEDURE CreateNewUser
    @Username nvarchar(255),   
    @Password nvarchar(255),
    @EmailAddress nvarchar(255),
    @AllowNotifications bit,
    @AccessToken uniqueidentifier,
    @Referral nvarchar(255),
    @IPAddress nvarchar(255),
    @UserAgent nvarchar(MAX)
AS    
	INSERT INTO Users (Username, Password, ActiveAccessToken, FirstName, EmailAddress, Location, Bio, Color, DisplayPicUrl, Gender, AllowNotifications, FlushSession, LoginAttempts, LoginAttemptDate, ForgotEmailDate, SuspendedDate, PremiumExpiryDate, AdFreeExpiryDate, IPAddress, Referral, Role, InactiveType) 
	VALUES (@Username, @Password, @AccessToken, '', @EmailAddress, '', '', '', '', 0, @AllowNotifications, 0, 0, GETDATE(), GETDATE(), GETDATE(), GETDATE(), GETDATE(), @IPAddress, @Referral, 0, 0)
	INSERT INTO Points(Username, Points)
	VALUES(@Username, 0)
	INSERT INTO AccessTokens(Username, AccessToken, IPAddress, UserAgent, CreatedDate)
	VALUES(@Username, @AccessToken, @IPAddress, @UserAgent, GETDATE())