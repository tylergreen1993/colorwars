CREATE PROCEDURE UpdateJobSubmission
    @Id uniqueidentifier,
    @State int,
    @UpdatedDate datetime
AS  
    UPDATE JobSubmissions
    SET State = @State,
    UpdatedDate = @UpdatedDate
    WHERE Id = @Id