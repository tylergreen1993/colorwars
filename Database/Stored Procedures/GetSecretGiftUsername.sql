CREATE PROCEDURE GetSecretGiftUsername
    @Username varchar(255)
AS  
    SELECT TOP 1 *, (ABS(CHECKSUM(NEWID()) % (SELECT COUNT(*) FROM USERS)) + 1) * (SecretGifterRank/25 + 1) as Score
    FROM
    (SELECT u.Username, CAST(ISNULL((SELECT VALUE FROM Settings WHERE Name = 'SecretGifterRank' AND Username = u.Username), 0) as int) as SecretGifterRank
    FROM Users u
    WHERE u.Username != @Username
    AND u.InactiveType = 0
    AND u.Role < 2
    AND u.SuspendedDate < GETDATE()
    AND u.CreatedDate < DATEADD(DAY, -1, GETDATE())) as AllUsers
    ORDER BY Score DESC