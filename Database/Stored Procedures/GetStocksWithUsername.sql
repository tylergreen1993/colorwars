CREATE PROCEDURE GetStocksWithUsername
    @Username varchar(255)
AS  
    SELECT *, (SELECT SUM(u.UserAmount) FROM UserStocks u WHERE u.ItemId = s.Id) AS TotalAmount
    FROM UserStocks u
    JOIN Stocks s
    ON  u.ItemId = s.Id
    WHERE u.Username = @Username
    ORDER BY u.PurchaseDate DESC