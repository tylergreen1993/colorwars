CREATE PROCEDURE AddBid
    @AuctionId int,
    @Username varchar(255),
    @Points int
AS  
    INSERT INTO Bids(Id, AuctionId, Username, Points, BidDate)
    VALUES (NEWID(), @AuctionId, @Username, @Points, GETDATE())