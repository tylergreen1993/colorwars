CREATE PROCEDURE GetNewsCommentsWithId
    @Id int,
    @Username varchar(255)
AS  
    SELECT n.*, (SELECT COUNT(*) FROM NewsCommentLikes WHERE CommentId = n.Id AND NewsId = NewsId) as TotalLikes, 
    (SELECT ISNULL(STRING_AGG(nl.Username, ', '), '') FROM NewsCommentLikes nl WHERE nl.CommentId = n.Id AND nl.NewsId = NewsId) as LikedUsernames,
    CAST(ISNULL((SELECT 1 FROM NewsCommentLikes WHERE CommentId = n.Id AND NewsId = NewsId AND Username = @Username), 0) AS BIT) AS UserLiked,
    u.Color, u.DisplayPicUrl
    FROM NewsComments n
    JOIN Users u ON
    u.Username = n.Username 
    WHERE NewsId = @Id
    ORDER BY TotalLikes DESC, CommentDate DESC