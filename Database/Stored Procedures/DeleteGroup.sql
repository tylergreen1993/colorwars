CREATE PROCEDURE DeleteGroup
    @GroupId int
AS  
    DELETE FROM GroupMembers
    WHERE GroupId = @GroupId
    DELETE FROM GroupMessages
    WHERE GroupId = @GroupId
    DELETE FROM Groups
    WHERE Id = @GroupId