CREATE PROCEDURE AddMatchHistory
    @Username nvarchar(255),
    @Opponent nvarchar(255),
    @Points int,
    @Level int,
    @Intensity int,
    @CPUType int,
    @Result int,
    @TourneyMatchId uniqueidentifier = NULL
AS  
    INSERT INTO MatchHistory(Id, Username, Opponent, Points, Level, Intensity, CPUType, TourneyMatchId, Result, CreatedDate)
    VALUES (NEWID(), @Username, @Opponent, @Points, @Level, @Intensity, @CPUType, @TourneyMatchId, @Result, GETDATE())