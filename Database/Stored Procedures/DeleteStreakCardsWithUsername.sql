CREATE PROCEDURE DeleteStreakCardsWithUsername
    @Username nvarchar(255)
AS  
    DELETE FROM StreakCards
    WHERE Username = @Username