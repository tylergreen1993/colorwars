CREATE PROCEDURE DeleteHelperSubmissionWithUsername
    @Username nvarchar(255)
AS  
    DELETE FROM JobSubmissions
    WHERE Username = @Username
    AND Position = 6