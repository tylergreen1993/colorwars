CREATE PROCEDURE AddNews
    @Username varchar(255),
    @Title varchar(255),
    @Content varchar(MAX)
AS  
    DECLARE @OutputId TABLE (Id int)

    INSERT INTO News(Username, Title, Content, PublishDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES (@Username, @Title, @Content, GETDATE())

    SELECT * FROM News
    WHERE Id in (SELECT Id FROM @OutputId)