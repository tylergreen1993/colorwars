CREATE PROCEDURE AddDisposerRecord
    @Username varchar(255)
AS  
    INSERT INTO DisposerRecords(Id, Username, CreatedDate)
    VALUES (NEWID(), @Username, GETDATE())