CREATE PROCEDURE AddLotto
    @CurrentPot int,
    @WinningNumber int
AS  
    INSERT INTO Lotto(CurrentPot, WinningNumber, WinnerUsername, WonDate)
    VALUES (@CurrentPot, @WinningNumber, NULL, GETDATE())