CREATE PROCEDURE RepairAllItems
    @Username varchar(255)
AS  
    UPDATE UserItems
    SET RepairedDate = GETDATE()
    WHERE Username = @Username