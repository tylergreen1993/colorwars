CREATE PROCEDURE RedeemPromoCode
    @Username varchar(255),
    @Code varchar(255)
AS  
    DECLARE @Redeemed bit = 0
    DECLARE @OneUserUse bit = 0
    IF EXISTS(SELECT 1 FROM PromoCodes WHERE Code = @Code AND ExpiryDate > GETDATE())
    BEGIN
        SET @OneUserUse = (SELECT OneUserUse FROM PromoCodes WHERE Code = @Code)
        IF @OneUserUse = 0
        BEGIN
            IF NOT EXISTS (SELECT 1 FROM UserPromoCodes WHERE Username = @Username AND Code = @Code)
            BEGIN
                SET @Redeemed = 1
            END
        END
        ELSE
        BEGIN
            SET @Redeemed = 1   
        END
    END
    IF @Redeemed = 1
    BEGIN
        IF @OneUserUse = 0
        BEGIN
            INSERT INTO UserPromoCodes(Username, Code, RedeemedDate)
            VALUES(@Username, @Code, GETDATE())
        END

        SELECT * FROM PromoCodes p
        WHERE Code = @Code

        IF @OneUserUse = 1
        BEGIN
            DELETE FROM PromoCodes WHERE Code = @Code
        END

        DELETE FROM UserPromoCodes WHERE Code IN (SELECT Code FROM PromoCodes WHERE ExpiryDate <= GETDATE())
        DELETE FROM PromoCodes WHERE  ExpiryDate <= GETDATE()
    END