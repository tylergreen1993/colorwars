CREATE PROCEDURE AddTourney
    @EntranceCost int,
    @MatchesPerRound int,
    @RequiredParticipants int,
    @Creator varchar(255),
    @Name varchar(255),
    @TourneyCaliber int,
    @IsSponsored bit
AS  
    DECLARE @OutputId TABLE (Id int)
    INSERT INTO Tourneys(EntranceCost, MatchesPerRound, RequiredParticipants, IsSponsored, Creator, Name, TourneyCaliber, CreatedDate)
    OUTPUT inserted.Id INTO @OutputId
    VALUES(@EntranceCost, @MatchesPerRound, @RequiredParticipants, @IsSponsored, @Creator, @Name, @TourneyCaliber, GETDATE())

    SELECT * FROM Tourneys
    WHERE Id in (SELECT Id FROM @OutputId)