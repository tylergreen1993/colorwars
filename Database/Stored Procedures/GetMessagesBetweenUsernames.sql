CREATE PROCEDURE GetMessagesBetweenUsernames
    @Username nvarchar(255),
    @Correspondent nvarchar(255),
    @UpdateRead bit
AS  
	IF @UpdateRead = 1 AND EXISTS(SELECT * FROM Messages WHERE Sender = @Correspondent AND Recipient = @Username AND IsRead = 0)
	BEGIN
	    UPDATE Messages
    	SET IsRead = 1
    	WHERE Sender = @Correspondent 
        AND Recipient = @Username
        AND IsRead = 0
	END
    SELECT TOP 100 * FROM Messages
    WHERE (Sender = @Username AND Recipient = @Correspondent)
    OR (Sender = @Correspondent AND Recipient = @Username)
    ORDER BY CreatedDate DESC