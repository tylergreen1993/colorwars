CREATE PROCEDURE GetUnreadCountWithUsername
    @Username varchar(255)
AS  
    SELECT 
    (SELECT COUNT(*)
    FROM Notifications
    WHERE SeenDate IS NULL
    AND Username = @Username) as Notifications,
    (SELECT COUNT(DISTINCT Sender)
    FROM Messages m
    JOIN Users u 
    ON u.Username = m.Sender
    WHERE u.InactiveType = 0
    AND m.IsRead = 0
    AND Recipient = @Username) as Messages