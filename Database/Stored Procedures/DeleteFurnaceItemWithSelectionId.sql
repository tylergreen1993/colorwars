CREATE PROCEDURE DeleteFurnaceItemWithSelectionId
    @Id uniqueidentifier
AS  
	DELETE FROM FurnaceItems
    WHERE SelectionId = @Id