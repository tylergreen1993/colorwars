CREATE PROCEDURE GetReferralsWithUsername
    @Username varchar(255)
AS  
    SELECT u.Username, MAX(u.CreatedDate) as CreatedDate, MAX(u.ModifiedDate) as ModifiedDate, MAX(b.TransactionDate) as TransactionDate, COUNT(b.TransactionDate) as InterestTimes 
    FROM Users u
    LEFT JOIN BankTransactions b
    ON u.Username = b.Username
    WHERE u.Referral = @Username
    AND (b.Username IS NULL 
    OR (b.Type = 2 AND DATEDIFF(day, u.CreatedDate, b.TransactionDate) <= 30))
    GROUP BY u.Username
    ORDER BY CreatedDate DESC