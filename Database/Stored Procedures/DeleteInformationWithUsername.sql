CREATE PROCEDURE DeleteInformationWithUsername
    @Username nvarchar(255)
AS  
    DELETE FROM Points
    WHERE Username = @Username
    DELETE FROM Bank
    WHERE Username = @Username
    DELETE FROM UserItems
    WHERE Username = @Username
    DELETE FROM UserStocks
    WHERE Username = @Username
    DELETE FROM GroupMembers
    WHERE Username = @Username
    DELETE FROM UserPromoCodes
    WHERE Username = @Username
    DELETE FROM WizardHistory
    WHERE Username = @Username
    DELETE FROM SwapDeckHistory
    WHERE Username = @Username
    DELETE FROM LibraryHistory
    WHERE Username = @Username
    DELETE FROM JobSubmissions
    WHERE Username = @Username
    DELETE FROM MarketStallsHistory
    WHERE MarketStallId IN (SELECT ID FROM MarketStalls WHERE Username = @Username)
    DELETE FROM MarketStallItems
    WHERE MarketStallId IN (SELECT ID FROM MarketStalls WHERE Username = @Username)
    DELETE FROM MarketStalls
    WHERE Username = @Username
    DELETE FROM NewsComments
    WHERE Username = @Username
    DELETE FROM BulkItems
    WHERE Username = @Username
    DELETE FROM SafetyDepositItems
    WHERE Username = @Username
    DELETE FROM ShowroomItems
    WHERE Username = @Username
    DELETE FROM UserCompanions
    WHERE Username = @Username
    DELETE FROM Messages
    WHERE Sender = @Username OR Recipient = @Username
    DELETE FROM Notifications
    WHERE Username = @Username
    DELETE FROM FavoritedLocations
    WHERE Username = @Username
    DELETE FROM TreasureDives
    WHERE Username = @Username
    DELETE FROM UserPowerMoves
    WHERE Username = @Username
    DELETE FROM UserAccomplishments
    WHERE Username = @Username
    DELETE FROM Settings
    WHERE Username = @Username
    DELETE FROM AccessTokens
    WHERE Username = @Username