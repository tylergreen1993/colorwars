CREATE PROCEDURE DeleteSafetyDepositItem
    @Id uniqueidentifier,
    @Username varchar(255)
AS  
	DELETE FROM SafetyDepositItems
    WHERE SelectionId = @Id
    AND Username = @Username