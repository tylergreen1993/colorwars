CREATE PROCEDURE GetAttentionHallCommentsFromPostsWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM AttentionHallComments
    WHERE PostId IN (SELECT Id FROM AttentionHallPosts WHERE Username = @Username)
    ORDER BY CreatedDate DESC