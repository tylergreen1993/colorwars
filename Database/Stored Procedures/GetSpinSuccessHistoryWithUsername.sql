CREATE PROCEDURE GetSpinSuccessHistoryWithUsername
    @Username varchar(255)
AS  
    SELECT * FROM SpinSuccessHistory
    WHERE Username = @Username
    ORDER BY CreatedDate DESC