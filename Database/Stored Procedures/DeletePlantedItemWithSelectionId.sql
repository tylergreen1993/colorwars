CREATE PROCEDURE DeletePlantedItemWithSelectionId
    @Id uniqueidentifier
AS  
	DELETE FROM PlantedItems
    WHERE SelectionId = @Id