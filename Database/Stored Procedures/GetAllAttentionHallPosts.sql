CREATE PROCEDURE GetAllAttentionHallPosts
    @Filter int,
    @Type int,
    @Username varchar(255)
AS  
    SELECT TOP 150 * FROM (
    SELECT a.*, 
    (SELECT ISNULL(SUM(Points), 0) FROM AttentionHallPoints WHERE PostId = Id) AS Points, 
    ISNULL((SELECT TOP 1 Points FROM AttentionHallPoints WHERE PostId = Id AND Username = @Username), 0) AS UserVote,
    (SELECT COUNT(*) FROM AttentionHallComments WHERE PostId = a.Id) AS TotalComments
    FROM AttentionHallPosts a
    WHERE (@Filter != 3 OR a.Username = @Username)
    AND 1 = CASE
        WHEN @Type = 0 THEN 1
        WHEN @Type = 1 AND AccomplishmentId IS NULL AND ItemId IS NULL AND MatchTagUsername IS NULL THEN 1
        WHEN @Type = 2 AND AccomplishmentId IS NOT NULL AND ItemId IS NULL AND MatchTagUsername IS NULL THEN 1
        WHEN @Type = 3 AND AccomplishmentId IS NULL AND ItemId IS NOT NULL AND MatchTagUsername IS NULL THEN 1
        WHEN @Type = 4 AND AccomplishmentId IS NULL AND ItemId IS NULL AND MatchTagUsername IS NOT NULL THEN 1
        ELSE 0
    END
    ) AS AttentionHallPosts 
    ORDER BY 
        CASE @Filter
            WHEN 0 THEN (1.0 * Points + 1.0)/(DATEDIFF(hour, CreatedDate, GETDATE()) + 1)
            WHEN 1 THEN CreatedDate
            WHEN 2 THEN Points
            ELSE CreatedDate
        END 
    DESC