CREATE PROCEDURE RepairAllMarketStallItems
    @MarketStallId int
AS  
    UPDATE MarketStallItems
    SET RepairedDate = GETDATE()
    WHERE MarketStallId = @MarketStallId