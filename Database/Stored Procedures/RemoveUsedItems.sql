CREATE PROCEDURE RemoveUsedItems
    @Username nvarchar(255)
AS
    DELETE FROM UserItems
    WHERE Username = @Username
    AND Uses = 0