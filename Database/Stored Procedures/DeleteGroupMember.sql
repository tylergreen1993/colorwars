CREATE PROCEDURE DeleteGroupMember
    @GroupId int,
    @Username varchar(255)
AS  
    DELETE FROM GroupMembers
    WHERE GroupId = @GroupId
    AND Username = @Username