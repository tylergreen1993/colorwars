CREATE PROCEDURE GetActiveUsers
    @Minutes int
AS  
    SELECT * FROM Users u
    INNER JOIN Points p
    ON u.Username = p.Username
    WHERE u.ModifiedDate > DateADD(mi, -@Minutes, GETDATE())