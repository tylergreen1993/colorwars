CREATE PROCEDURE DeleteMarketStallWithId
    @Id int
AS  

	DELETE FROM MarketStallsHistory
	WHERE MarketStallId = @Id
	
    DELETE FROM MarketStallItems
    WHERE MarketStallId = @Id
    
    DELETE FROM MarketStalls
    WHERE Id = @Id