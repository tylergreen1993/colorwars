CREATE PROCEDURE AddUserCompanion
    @Username varchar(255),
    @Type int, 
    @Name varchar(255),
    @Color int,
    @Position int = 0
AS  
	DECLARE @Id UNIQUEIDENTIFIER = NEWID();
    INSERT INTO UserCompanions(Id, Username, Type, Name, Color, Level, Position, LastInteractedDate, HappinessDate, LastGiftGivenDate, CreatedDate)
    VALUES (@Id, @Username, @Type, @Name, @Color, 0, @Position, GETDATE(), GETDATE(), NULL, GETDATE())

    SELECT * FROM UserCompanions
    WHERE Id = @Id
