CREATE PROCEDURE DeleteHiddenFeedCategory
    @Username varchar(255),
    @FeedId int
AS  
    DELETE FROM HiddenFeedCategories
    WHERE Username = @Username
    AND FeedId = @FeedId
