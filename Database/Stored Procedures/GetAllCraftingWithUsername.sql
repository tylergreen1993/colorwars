CREATE PROCEDURE GetAllCraftingWithUsername
    @Username nvarchar(255)
AS  
    SELECT *
    FROM UserItems u
    JOIN Items i
    ON u.ItemId = i.Id
    JOIN Crafting c
    ON u.ItemId = c.Id
    WHERE u.Username = @Username
    AND i.Category = 3
    ORDER BY i.Points, i.Name ASC