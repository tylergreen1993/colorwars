CREATE PROCEDURE UpdatePowerMove
    @Username varchar(255),
    @Type int,
    @IsActive bit
AS  
    IF EXISTS (SELECT 1 FROM UserPowerMoves WHERE Username = @Username AND Type = @Type)
    BEGIN
        UPDATE UserPowerMoves
        SET IsActive = @IsActive
        WHERE Username = @Username
        AND Type = @Type
    END
    ELSE
    BEGIN
        INSERT INTO UserPowerMoves(Type, Username, IsActive, CreatedDate)
        VALUES(@Type, @Username, 0, GETDATE())
    END