CREATE PROCEDURE UpdateContact
    @Id uniqueidentifier,
    @Username nvarchar(255) = NULL,
    @Outcome nvarchar(MAX) = NULL,
    @IsCompleted bit
AS  
    UPDATE Contacts
    SET ClaimedUser = @Username,
    Outcome = @Outcome,
    IsCompleted = @IsCompleted
    WHERE Id = @Id