CREATE FUNCTION GetTowerRankings()
RETURNS TABLE  
AS  
RETURN   
( 
    SELECT CAST(RANK() OVER (ORDER BY TopFloorLevel DESC) AS INT) as Rank, PERCENT_RANK() OVER (ORDER BY TopFloorLevel DESC) as Percentile, * FROM
    (
        SELECT * FROM (
            SELECT s.Username, ISNULL(CAST(Value AS INT) + 1, 0) as TopFloorLevel
            FROM Settings s
            WHERE Name = 'TowerFloorRecordLevel'
        ) as TowerFloorLevels
        WHERE TopFloorLevel > 0
    ) as TowerFloorLevelRankings
)