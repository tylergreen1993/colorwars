CREATE FUNCTION GetMatchRankings()
RETURNS TABLE  
AS  
RETURN   
(         
    SELECT Username, DisplayPicUrl, Color, Score, Rank, Percentile, TotalMatches, WonMatches FROM 
    (
        SELECT CAST(RANK() OVER (ORDER BY Score DESC, TotalMatches ASC) AS INT) as Rank, PERCENT_RANK() OVER (ORDER BY Score DESC, TotalMatches ASC) as Percentile, * FROM
        (
            SELECT *,  ((2 * WonMatches) - TotalMatches) as Score
            FROM
            (
                SELECT *, (SELECT COUNT(*) FROM MatchHistory m WHERE m.Username = UserMatches.Username AND m.Result = 1 AND m.CPUType = 0 AND m.Points > 0) AS WonMatches  
                FROM
                (
                    SELECT *,
                    (SELECT COUNT(*) FROM MatchHistory m WHERE m.Username = u.Username AND CPUType = 0 AND u.InactiveType = 0 AND Points > 0) AS TotalMatches
                    FROM Users u
                ) AS UserMatches
                WHERE TotalMatches > 0
            ) AS WinningMatches
        ) AS UserScores
    ) AS UserRankings
)