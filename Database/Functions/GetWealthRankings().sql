CREATE FUNCTION GetWealthRankings()
RETURNS TABLE  
AS  
RETURN   
(         
    SELECT CAST(RANK() OVER (ORDER BY TotalPoints DESC) AS INT) as Rank, PERCENT_RANK() OVER (ORDER BY TotalPoints DESC) as Percentile, * FROM
    (
        SELECT u.Username, u.DisplayPicUrl, u.Color, ISNULL(p.Points, 0) + ISNULL(b.Points, 0) as TotalPoints
        FROM Users u
        LEFT JOIN 
        Bank b ON
        b.Username = u.Username
        LEFT JOIN Points p ON
        p.Username = u.Username
        WHERE u.InactiveType = 0
    ) as UserWealth
)