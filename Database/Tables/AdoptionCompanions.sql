CREATE TABLE AdoptionCompanions(
    Id uniqueidentifier,
    Type int,
    Name varchar(255),
    Color int,
    Level int,
    CreatedDate datetime,
    AddedDate datetime,
    Primary Key(Id)
)