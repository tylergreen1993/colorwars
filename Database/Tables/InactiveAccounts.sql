CREATE TABLE InactiveAccounts(
	Username varchar(255),
    Reason int,
    Type int,
    Info varchar(MAX),
    InactiveDate datetime,
    Primary Key(Username)
)