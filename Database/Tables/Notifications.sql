CREATE TABLE Notifications(
	Id uniqueidentifier,
    Username varchar(255),
    Message varchar(MAX),
    Location int,
    Href varchar(255),
    ImageUrl varchar(255),
    SeenDate datetime,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users(Username)
)