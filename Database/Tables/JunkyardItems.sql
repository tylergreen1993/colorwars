CREATE TABLE JunkyardItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime
    Primary Key(SelectionId),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)