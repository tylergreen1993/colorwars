CREATE TABLE Messages(
	Id uniqueidentifier,
    Sender varchar(255),
    Recipient varchar(255),
    Content varchar(max),
    IsRead bit,
    CreatedDate datetime,
    Primary Key (Id, Sender, Recipient),
    Foreign Key (Sender) References Users (Username),
    Foreign Key (Recipient) References Users (Username)
)