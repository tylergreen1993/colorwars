CREATE TABLE DoubtItCards(
    Id uniqueidentifier,
    Username varchar(255),
    Amount int,
    Owner int,
    AddedDate datetime,
    Primary Key (Id),
    Foreign Key (Username) References Users(Username)
)