CREATE TABLE FurnaceItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Username varchar(255),
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime
    Primary Key(Username),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)