CREATE TABLE Bids(
    Id uniqueidentifier,
    AuctionId int,
    Username varchar(255),
    Points int,
    BidDate datetime,
    Primary Key (Id),
    Foreign Key (AuctionId) References Auctions (Id),
    Foreign Key (Username) References Users (Username)
)