CREATE TABLE StoreItems(
    Id uniqueidentifier,
    ItemId uniqueidentifier,
    Cost int,
    MinCost int,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)