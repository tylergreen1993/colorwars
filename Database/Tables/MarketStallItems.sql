CREATE TABLE MarketStallItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    MarketStallId int,
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Cost int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime
    Primary Key(SelectionId),
    FOREIGN KEY (ItemId) REFERENCES Items (Id),
    FOREIGN KEY (MarketStallId) REFERENCES MarketStalls (Id)
)