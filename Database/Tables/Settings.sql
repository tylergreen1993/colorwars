CREATE TABLE Settings(
    Id uniqueidentifier DEFAULT NEWID(),
    Username varchar(255) NOT NULL,
    Name varchar(255) NOT NULL,
    Value varchar(max),
    CreatedDate datetime,
    UpdatedDate datetime,
    Primary Key (Username, Name),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)