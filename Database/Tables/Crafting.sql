CREATE TABLE Crafting(
    Id uniqueidentifier,
    Color int,
    IsEnhanced bit,
    Type int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)