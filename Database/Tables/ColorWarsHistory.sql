CREATE TABLE ColorWarsHistory(
    Id uniqueidentifier,
    TeamColor int,
    Amount int,
    CreatedDate datetime,
    Primary Key(Id)
)