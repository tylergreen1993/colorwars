CREATE TABLE JobSubmissions(
	Id uniqueidentifier,
    Username varchar(255),
    Position int,
    Title varchar(255),
    Content varchar(MAX),
    State int,
    AttentionHallPostId int,
    CreatedDate datetime,
    UpdatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users(Username)
)