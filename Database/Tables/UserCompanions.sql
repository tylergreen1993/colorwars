CREATE TABLE UserCompanions(
    Id uniqueidentifier,
    Username varchar(255),
    Type int,
    Name varchar(255),
    Color int,
    Level int,
    Position int,
    LastInteractedDate datetime,
    HappinessDate datetime,
    LastGiftGivenDate datetime,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)