CREATE TABLE MarketStalls(
	Id int identity(1,1),
    Username varchar(255),
    Name varchar(255),
    Description varchar(MAX),
    Discount int,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users (Username)
)