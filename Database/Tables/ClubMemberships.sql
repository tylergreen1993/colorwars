CREATE TABLE ClubMemberships(
    Id uniqueidentifier,
    Username varchar(255),
    ClaimedItem bit,
    StartDate datetime,
    EndDate datetime,
    Primary Key (Id),
    Foreign Key (Username) References Users (Username)
)