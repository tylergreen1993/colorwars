CREATE TABLE AgerItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Username varchar(255),
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime,
    Primary Key(Username),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)