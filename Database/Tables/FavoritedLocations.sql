CREATE TABLE FavoritedLocations(
	Username varchar(255),
    LocationId int,
    Position int,
    CreatedDate datetime,
    Primary Key (Username, LocationId),
    Foreign Key(Username) References Users(Username)
)