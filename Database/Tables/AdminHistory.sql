CREATE TABLE AdminHistory(
    Id uniqueidentifier,
    Username varchar(255),
    AdminUser varchar(255),
    Reason varchar(MAX),
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users(Username),
    Foreign Key (AdminUser) References Users(Username),
)