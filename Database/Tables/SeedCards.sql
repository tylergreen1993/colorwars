CREATE TABLE SeedCards(
    Id uniqueidentifier,
    Color int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)