CREATE TABLE NoticeBoardTasks(
    Username varchar(255),
    ItemId uniqueidentifier,
    ItemCondition int,
    IsExactCondition bit,
    HasTotalUses bit,
    Reward int,
    ItemCreatedDate datetime,
    CreatedDate datetime,
    Primary Key(Username),
    Foreign Key(Username) References Users(Username),
    Foreign Key(ItemId) References Items(Id)
)