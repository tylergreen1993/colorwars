CREATE TABLE AttentionHallCommentLikes(
	PostId int,
    CommentId uniqueidentifier,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key(PostId, CommentId, Username),
    Foreign Key(PostId) References AttentionHallPosts (Id),
    Foreign Key(CommentId) References AttentionHallComments (Id),
    Foreign Key(Username) References Users (Username)
)