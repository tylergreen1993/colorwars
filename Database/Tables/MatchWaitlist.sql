CREATE TABLE MatchWaitlist(
    Id uniqueidentifier,
    Username varchar (255),
    Username2 varchar (255),
    ChallengeUser varchar(255),
    IsElite bit,
    Points int,
    TourneyMatchId uniqueidentifier,
    CreatedDate datetime

    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users(Username),
    FOREIGN KEY (Username2) REFERENCES Users(Username)
    FOREIGN KEY (ChallengeUser) REFERENCES Users(Username)
)