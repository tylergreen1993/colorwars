CREATE TABLE AttentionHallComments(
    Id uniqueidentifier,
    ParentCommentId uniqueidentifier,
	PostId int,
    Username varchar(255),
    Comment varchar(max),
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(ParentCommentId) References AttentionHallComments (Id),
    Foreign Key(PostId) References AttentionHallPosts (Id),
    Foreign Key(Username) References Users (Username)
)