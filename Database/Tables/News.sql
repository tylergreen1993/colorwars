CREATE TABLE News(
	Id int identity(1,1),
    Username varchar(255),
    Title varchar(255),
    Content varchar(MAX),
    PublishDate datetime,
    Primary Key(Id)
)