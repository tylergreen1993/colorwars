CREATE TABLE Companions(
    Id uniqueidentifier,
    Type int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)