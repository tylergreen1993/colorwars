CREATE TABLE GroupMembers(
	GroupId int,
    Username varchar(255),
    Role int,
    IsPrizeMember bit,
    IsPrimary bit,
    JoinDate datetime,
    LastReadMessageDate datetime,
    Primary Key (GroupId, Username),
    Foreign Key (GroupId) References Groups(Id),
    Foreign Key (Username) References Users (Username)
)