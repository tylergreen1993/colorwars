CREATE TABLE TourneyMatches (
    Id uniqueidentifier,
    TourneyId int,
    Round int,
    Username1 varchar(255),
    Username2 varchar(255),
    Winner varchar(255),
    CreatedDate datetime,
    ModifiedDate datetime,
    Primary Key (Id),
    Foreign Key (TourneyId) References Tourneys(Id),
    Foreign Key (Username1) References Users(Username),
    Foreign Key (Username2) References Users(Username)
)