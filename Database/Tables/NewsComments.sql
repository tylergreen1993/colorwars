CREATE TABLE NewsComments(
	Id uniqueidentifier,
	ParentCommentId uniqueidentifier,
    NewsId int,
    Username varchar(255),
    Message varchar(MAX),
    CommentDate datetime,
    Primary Key(Id),
    Foreign Key (NewsId) References News(Id),
    Foreign Key(ParentCommentId) References NewsComments(Id),
    Foreign Key (Username) References Users(Username)
)