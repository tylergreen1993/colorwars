CREATE TABLE UserAccomplishments(
    SelectionId uniqueidentifier,
    AccomplishmentId int,
    Username varchar(255),
    ReceivedDate datetime,
    Primary Key(AccomplishmentId, Username),
    Foreign Key(Username) references Users(Username),
    Foreign Key(AccomplishmentId) references Accomplishments(Id)
)