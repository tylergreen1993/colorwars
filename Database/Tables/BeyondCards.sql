CREATE TABLE BeyondCards(
    Id uniqueidentifier,
    Color int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)