CREATE TABLE PromoCodes(
    Code varchar(255),
    Creator varchar(255),
    Effect int,
    OneUserUse bit,
    IsSponsor bit,
    ItemId uniqueidentifier,
    ExpiryDate datetime
    Primary Key(Code)
)