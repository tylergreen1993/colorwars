CREATE TABLE TradeOffers(
	Id uniqueidentifier,
    TradeId int,
    Username varchar(255),
    Item1Id uniqueidentifier,
    Item2Id uniqueidentifier,
    Item3Id uniqueidentifier,
    IsAccepted bit,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users (Username),
    Foreign Key(TradeId) References Trades(Id),
    Foreign Key (Item1Id) References TradeItems(SelectionId),
    Foreign Key (Item2Id) References TradeItems(SelectionId),
    Foreign Key (Item3Id) References TradeItems(SelectionId)
)