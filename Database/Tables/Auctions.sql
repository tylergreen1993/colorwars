CREATE TABLE Auctions(
    Id int identity(1,1),
    Username varchar(255),
    StartPoints int,
    ItemId uniqueidentifier,
    MinBidIncrement int,
    CreatorCollected bit,
    WinnerCollected bit,
    SentNotification bit,
    EndDate datetime,
    Primary Key (Id),
    Foreign Key (Username) References Users (Username),
    Foreign Key (ItemId) References AuctionItems (SelectionId)
)