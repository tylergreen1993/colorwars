CREATE TABLE ShowroomItems(
    Username varchar(255),
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Theme int,
    Position int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime,
    Primary Key(SelectionId),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)