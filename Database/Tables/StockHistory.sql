CREATE TABLE StockHistory (
    SelectionId uniqueidentifier,
    Id int,
    Username varchar(255),
    UserCost int,
    UserAmount int,
    HistoryType int,
    CreatedDate datetime,
    Primary Key(SelectionId),
    Foreign Key(Id) References Stocks(Id),
    Foreign Key(Username) References Users(Username)
)