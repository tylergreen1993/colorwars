CREATE TABLE LuckyGuessTreasure (
    Position int,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key (CreatedDate),
    Foreign Key (Username) References Users(Username)
)