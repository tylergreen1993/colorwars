CREATE TABLE UserStocks(
    SelectionId uniqueidentifier,
    ItemId int,
    Username varchar(255),
    PurchaseCost int,
    UserAmount int,
    PurchaseDate datetime,
    Primary Key(SelectionId),
    Foreign Key(ItemId) References Stocks(Id),
    Foreign Key(Username) References Users(Username)
)