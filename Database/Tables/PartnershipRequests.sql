CREATE TABLE PartnershipRequests(
    RequestUsername varchar(255),
    Username varchar(255),
    CreatedDate datetime,
    Primary Key(RequestUsername, Username),
    Foreign Key (Username) References Users(Username),
    Foreign Key (RequestUsername) References Users(Username)
)