CREATE TABLE Lotto(
	Id int identity(1,1),
	CurrentPot int,
    WinningNumber int,
    WinnerUsername varchar(255),
    WonDate datetime,
    Primary Key(Id),
    Foreign Key (WinnerUsername) References Users(Username)
)