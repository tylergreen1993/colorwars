CREATE TABLE TradeItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime
    Primary Key(SelectionId),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)