CREATE TABLE Candy(
    Id uniqueidentifier,
    Amount int,
    TotalTime int,
    Type int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)