CREATE TABLE Tourneys (
    Id int identity(1,1),
    EntranceCost int,
    MatchesPerRound int,
    RequiredParticipants int,
    Creator varchar(255),
    Name varchar(255),
    TourneyCaliber int,
    IsSponsored bit,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Creator) References Users (Username)
)