CREATE TABLE Wishes(
	Id uniqueidentifier,
    Username varchar(255),
    Content varchar(max),
    IsGranted bit,
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (Username) References Users (Username)
)