CREATE TABLE RightSideUpGuardHallOfFame(
    Username varchar(255),
    CreatedDate datetime,
    Primary Key(Username),
    Foreign Key (Username) References Users(Username)
)