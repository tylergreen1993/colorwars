CREATE TABLE Blocked(
    Username varchar(255),
    BlockedUser varchar(255),
    CreatedDate datetime,
    Primary Key (Username, BlockedUser),
    Foreign Key (Username) References Users (Username),
    Foreign Key (BlockedUser) References Users (Username)
)