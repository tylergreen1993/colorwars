CREATE TABLE BulkItems(
    Id uniqueidentifier,
    Username varchar(255),
    ItemId uniqueidentifier,
    Cost int,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)