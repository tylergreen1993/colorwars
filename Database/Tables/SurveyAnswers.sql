CREATE TABLE SurveyAnswers(
    Id uniqueidentifier,
    Username varchar(255),
    SurveyQuestionId int,
    Answer varchar(MAX),
    CreatedDate datetime,
    Primary Key(Id)
)