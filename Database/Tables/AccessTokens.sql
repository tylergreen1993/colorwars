CREATE TABLE AccessTokens(
    Username varchar(255),
    AccessToken uniqueidentifier,
    IPAddress varchar(255),
    UserAgent varchar(MAX),
    CreatedDate datetime
    Primary Key (Username, AccessToken),
    Foreign Key (Username) References Users (Username)
)