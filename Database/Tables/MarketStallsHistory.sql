CREATE TABLE MarketStallsHistory(
    Id uniqueidentifier,
    MarketStallId int,
    Username varchar(255),
    ItemId uniqueidentifier,
    Name varchar(255),
    Cost int,
    Theme int,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (MarketStallId) REFERENCES MarketStalls (Id),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)