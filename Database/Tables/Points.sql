CREATE TABLE Points(
    Username varchar(255),
    Points int,
    Primary Key (Username),
    Foreign Key (Username) References Users (Username)
)