CREATE TABLE KeyCards(
    Id uniqueidentifier,
    Piece int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)