CREATE TABLE ColorWarsSubmissions(
    Id uniqueidentifier,
    Username varchar(255),
    Amount int,
    TeamColor int,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users (Username)
)