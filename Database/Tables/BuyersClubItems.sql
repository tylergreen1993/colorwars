CREATE TABLE BuyersClubItems(
    SelectionId uniqueidentifier,
    Id uniqueidentifier,
    Cost int,
    CreatedDate datetime,
    Primary Key(SelectionId),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)