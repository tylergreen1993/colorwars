CREATE TABLE AuctionItems(
    SelectionId uniqueidentifier,
    Name varchar(255),
    ImageUrl varchar(255),
    ItemId uniqueidentifier,
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime
    Primary Key(SelectionId),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)