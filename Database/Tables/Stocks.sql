CREATE TABLE Stocks(
    Id int,
    Name varchar(255),
    Description varchar(255),
    CurrentCost float,
    DividendsPercent float,
    Primary Key(Id)
)