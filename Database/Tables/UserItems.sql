CREATE TABLE UserItems(
    SelectionId uniqueidentifier DEFAULT NEWID(),
    Username varchar(255),
    ItemId uniqueidentifier,
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    IsInDeck bit,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime,
    Primary Key (SelectionId),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)