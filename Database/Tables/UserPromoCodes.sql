CREATE TABLE UserPromoCodes(
    Username varchar(255),
    Code varchar(255),
    RedeemedDate datetime,
    Primary Key(Username, Code),
    Foreign Key(Username) References Users(Username),
    Foreign Key(Code) References PromoCodes(Code)
)