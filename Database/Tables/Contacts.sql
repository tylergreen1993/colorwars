CREATE TABLE Contacts(
    Id uniqueidentifier,
    Username varchar(255),
    ClaimedUser varchar(255),
    EmailAddress varchar(255),
    IPAddress varchar(255),
    Message varchar(max),
    Category int,
    ReplyViaMessages bit,
    Outcome varchar(MAX),
    IsCompleted bit,
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (ClaimedUser) References Users (Username)
)