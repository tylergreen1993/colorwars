CREATE TABLE Enhancers(
    Id uniqueidentifier,
    Amount int,
    IsMultiplied bit,
    IsPercent bit,
    IsFiller bit,
    SpecialType int,
    Primary Key (Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)