CREATE TABLE TreasureDives(
    Username varchar(255),
    Attempts int,
    TreasureCoordinates varchar(MAX),
    LastGuessCoordinates varchar(MAX),
    Primary Key(Username),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)