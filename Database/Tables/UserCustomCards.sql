CREATE TABLE UserCustomCards(
    Id uniqueidentifier,
    Username varchar(255),
    Name varchar(255),
    ImageUrl varchar(255),
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)