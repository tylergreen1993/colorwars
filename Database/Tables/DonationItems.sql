CREATE TABLE DonationItems(
    Username varchar(255),
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime
    Primary Key(SelectionId),
    Foreign Key (Username) References Users (Username),
    Foreign Key (ItemId) References Items (Id)
)