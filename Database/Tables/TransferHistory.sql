CREATE TABLE TransferHistory(
    Id uniqueidentifier,
    TransferId int,
    Type int,
    Transferer varchar(255),
    Transferee varchar(255),
    DifferenceAmount int,
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (Transferer) References Users (Username),
    Foreign Key (Transferee) References Users (Username)
)