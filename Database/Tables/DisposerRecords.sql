CREATE TABLE DisposerRecords(
	Id uniqueidentifier,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users(Username)
)