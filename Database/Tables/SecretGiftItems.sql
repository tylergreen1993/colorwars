CREATE TABLE SecretGiftItems(
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Username varchar(255),
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Theme int,
    CreatedDate datetime,
    RepairedDate datetime
    Primary Key(SelectionId),
    FOREIGN KEY (Username) REFERENCES Users (Username),
    FOREIGN KEY (ItemId) REFERENCES Items (Id)
)