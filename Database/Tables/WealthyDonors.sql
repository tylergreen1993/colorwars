CREATE TABLE WealthyDonors(
    Username varchar(255),
    Amount int,
    Primary Key(Username),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)