CREATE TABLE SwapDeckHistory(
	Id uniqueidentifier,
    Username varchar(255),
    Type int,
    Message varchar(MAX),
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users(Username)
)