CREATE TABLE Bank(
    Username varchar(255),
    Points int,
    UpdatedDate datetime
    Primary Key (Username),
    Foreign Key (Username) References Users (Username)
)