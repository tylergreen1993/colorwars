CREATE TABLE BankTransactions(
    Username varchar(255),
    Type int,
    Amount int,
    TransactionDate datetime,
    Primary Key(Username, TransactionDate),
    Foreign Key(Username) References Users(Username)
)