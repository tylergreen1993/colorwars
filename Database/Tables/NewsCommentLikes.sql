CREATE TABLE NewsCommentLikes(
	NewsId int,
    CommentId uniqueidentifier,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key(NewsId, CommentId, Username),
    Foreign Key(NewsId) References News (Id),
    Foreign Key(CommentId) References NewsComments (Id),
    Foreign Key(Username) References Users (Username)
)