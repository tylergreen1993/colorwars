CREATE TABLE StealthCards(
    Id uniqueidentifier,
    Type int,
    Quality int,
    Primary Key(Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)