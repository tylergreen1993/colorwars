CREATE TABLE PasswordResets(
    Id uniqueidentifier,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key (Id, Username),
    Foreign Key (Username) references Users(Username)
)