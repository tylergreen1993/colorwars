CREATE TABLE AttentionHallPoints(
	PostId int,
    Username varchar(255),
    Points int,
    CreatedDate datetime,
    Primary Key(PostId, Username),
    Foreign Key(PostId) References AttentionHallPosts (Id),
    Foreign Key(Username) References Users (Username)
)