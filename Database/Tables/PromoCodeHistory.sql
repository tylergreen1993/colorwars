CREATE TABLE PromoCodeHistory(
	Id uniqueidentifier,
    Username varchar(255),
    Code varchar(255),
    Message varchar(MAX),
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users(Username)
)