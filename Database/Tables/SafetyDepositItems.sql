CREATE TABLE SafetyDepositItems(
	Username varchar(255),
    SelectionId uniqueidentifier,
    ItemId uniqueidentifier,
    Name varchar(255),
    ImageUrl varchar(255),
    Uses int,
    Theme int,
    Position int,
    CreatedDate datetime,
    RepairedDate datetime,
    AddedDate datetime
    Primary Key(SelectionId),
    Foreign Key (Username) References Users (Username),
    Foreign Key (ItemId) References Items (Id)
)