CREATE TABLE UserPowerMoves (
    Type int,
    Username varchar(255),
    IsActive bit,
    CreatedDate datetime,
    Primary Key(Username, Type),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)