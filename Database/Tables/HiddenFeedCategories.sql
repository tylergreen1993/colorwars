CREATE TABLE HiddenFeedCategories(
    Username varchar(255),
    FeedId int,
    CreatedDate datetime,
    Primary Key (Username, FeedId)
)