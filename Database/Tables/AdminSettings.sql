CREATE TABLE AdminSettings(
    Id uniqueidentifier DEFAULT NEWID(),
    Name varchar(255),
    Value varchar(255),
    CreatedDate datetime
    Primary Key (Id)
)