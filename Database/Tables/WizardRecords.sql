CREATE TABLE WizardRecords(
	Id uniqueidentifier,
    Username varchar(255),
    Amount int,
    Type int,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users(Username)
)