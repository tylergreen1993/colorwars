CREATE TABLE ShowroomLikes(
	Username varchar(255),
    ShowroomUsername varchar(255),
    CreatedDate datetime,
    Primary Key(Username, ShowroomUsername),
    Foreign Key(Username) References Users (Username),
    Foreign Key(ShowroomUsername) References Users (Username)
)