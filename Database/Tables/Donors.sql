CREATE TABLE Donors(
    Id uniqueidentifier,
    Username varchar(255),
    Amount int, 
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users(Username)
)