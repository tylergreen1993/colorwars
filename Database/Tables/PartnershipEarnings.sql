CREATE TABLE PartnershipEarnings(
    Id uniqueidentifier,
    Username varchar(255),
    PartnershipUsername varchar(255),
    Amount int,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users(Username),
    Foreign Key (PartnershipUsername) References Users(Username)
)