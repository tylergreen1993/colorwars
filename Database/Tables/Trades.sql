CREATE TABLE Trades(
	Id int identity(1,1),
    Username varchar(255),
    Item1Id uniqueidentifier,
    Item2Id uniqueidentifier,
    Item3Id uniqueidentifier,
    Request varchar(MAX),
    IsAccepted bit,
    CreatedDate datetime,
    AcceptedDate datetime,
    Primary Key(Id),
    Foreign Key(Username) References Users (Username),
    Foreign Key (Item1Id) References TradeItems(SelectionId),
    Foreign Key (Item2Id) References TradeItems(SelectionId),
    Foreign Key (Item3Id) References TradeItems(SelectionId)
)