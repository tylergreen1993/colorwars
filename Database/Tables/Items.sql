CREATE TABLE Items(
    Id uniqueidentifier DEFAULT NEWID(),
    Name varchar(255),
    ImageUrl varchar(255),
    Description varchar(255),
    Category int,
    TotalUses int,
    Points int,
    Tier int,
    IsExclusive bit,
    Primary Key (Id)
)