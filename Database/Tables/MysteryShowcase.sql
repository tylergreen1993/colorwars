CREATE TABLE MysteryShowcase(
    Id uniqueidentifier,
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (Id) references Items(Id)
)