CREATE TABLE GroupRequests(
    Username varchar(255),
    GroupId int,
    Accepted bit,
    CreatedDate datetime,
    Primary Key (Username, GroupId),
    Foreign Key (Username) References Users (Username),
    Foreign Key (GroupId) References Groups (Id)
)