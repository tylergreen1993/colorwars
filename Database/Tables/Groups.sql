CREATE TABLE Groups(
	Id int identity(1,1),
    Name varchar(255),
    Power int,
    Description varchar(MAX),
    DisplayPicUrl varchar(MAX),
    IsPrivate bit,
    Whiteboard varchar(MAX),
    CreatedBy varchar(255),
    LastPrizeDate datetime,
    LastPowerChangeDate datetime,
    CreatedDate datetime,
    Primary Key (Id)
)