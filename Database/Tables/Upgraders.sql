CREATE TABLE Upgraders(
    Id uniqueidentifier,
    Amount int,
    Type int,
    Primary Key (Id),
    FOREIGN KEY (Id) REFERENCES Items (Id)
)