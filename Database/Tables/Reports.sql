CREATE TABLE Reports(
    Id uniqueidentifier,
    Username varchar(255),
    ReportedUser varchar(255),
    IPAddress varchar(255),
    ClaimedUser varchar(255),
    AttentionHallPostId int,
    Reason varchar(max),
    IsCompleted bit,
    Outcome varchar(MAX),
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (ReportedUser) References Users (Username),
    Foreign Key (ClaimedUser) References Users (Username)
)