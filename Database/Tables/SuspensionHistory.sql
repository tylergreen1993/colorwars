CREATE TABLE SuspensionHistory(
    Id uniqueidentifier,
    Username varchar(255),
    SuspendingUser varchar(255),
    Reason varchar(MAX),
	SuspendedDate datetime,
    CreatedDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users(Username),
    Foreign Key (SuspendingUser) References Users(Username),
)