CREATE TABLE OfferCards(
    Id uniqueidentifier,
    Username varchar(255),
    Position int,
    Points int,
    FlipDate datetime,
    Primary Key(Id),
    Foreign Key (Username) References Users(Username)
)