CREATE TABLE SeekerChallenges(
    Id uniqueidentifier,
    Username varchar(255),
    ItemId uniqueidentifier,
    ItemTheme int,
    RewardId uniqueidentifier,
    RewardTheme int
    Primary Key (Id),
    Foreign Key (ItemId) references Items(Id),
    Foreign key (RewardId) references Items(Id)
)