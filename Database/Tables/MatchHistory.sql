CREATE TABLE MatchHistory(
    Id uniqueidentifier,
    Username varchar(255),
    Opponent varchar(255),
    Points int,
    Level int,
    Intensity int,
    CPUType int,
    TourneyMatchId uniqueidentifier,
    Result int,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)