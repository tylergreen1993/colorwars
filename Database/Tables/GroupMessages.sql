CREATE TABLE GroupMessages(
	Id uniqueidentifier,
    GroupId int,
    Sender varchar(255),
    Content varchar(max),
    IsDeleted bit,
    CreatedDate datetime,
    Primary Key (Id),
    Foreign Key (GroupId) References Groups (Id),
    Foreign Key (Sender) References Users (Username),
)