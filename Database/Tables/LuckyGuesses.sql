CREATE TABLE LuckyGuesses (
    Position int,
    Username varchar(255),
    CreatedDate datetime,
    Primary Key (Position),
    Foreign Key (Username) References Users(Username)
)