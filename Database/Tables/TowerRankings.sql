CREATE TABLE TowerRankings(
    Id uniqueidentifier,
    Username varchar(255),
    TopFloorLevel int,
    CreatedDate datetime,
    Primary Key(Id),
    FOREIGN KEY (Username) REFERENCES Users (Username)
)